
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:19:22 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6240.709666

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*128*92 grid
  Fine grid: 240*256*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*256*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 62.12 MiB
    Density: 35.50 MiB
      Arrays: 34.03 MiB
      Localized functions: 1.47 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 22.38 MiB
      Arrays: 22.26 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.23 MiB
      C [qnM]: 0.09 MiB
      S, T [2 x qmm]: 0.18 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.97 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 12
Number of atomic orbitals: 108
Number of bands in calculation: 108
Number of valence electrons: 30
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
             .--------------------------------------------.  
            /|                                            |  
           / |                                            |  
          /  |                                            |  
         /   |                                            |  
        /    |                                            |  
       /     |                                            |  
      /      |                                            |  
     /       |                                            |  
    /        |                                            |  
   /         |                                            |  
  /          |                                            |  
 *           |                                            |  
 |           |                 H                          |  
 |           |           H C  C  C  H                     |  
 |           |         H  C  C  C H                       |  
 |           |              H                             |  
 |           .--------------------------------------------.  
 |          /                                            /   
 |         /                                            /    
 |        /                                            /     
 |       /                                            /      
 |      /                                            /       
 |     /                                            /        
 |    /                                            /         
 |   /                                            /          
 |  /                                            /           
 | /                                            /            
 |/                                            /             
 *--------------------------------------------*              

Positions:
   0 C      9.149787   10.877608    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C     10.358107   10.179984    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 C     10.358107    8.784736    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 C      9.149787    8.087112    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 C      7.941467    8.784736    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 C      7.941467   10.179984    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      9.149787   11.964720    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H     11.299574   10.723540    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H     11.299574    8.241180    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      9.149787    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H      7.000000    8.241180    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.000000   10.723540    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.299574    0.000000    0.000000   120     0.1525
  2. axis:    no     0.000000   18.964720    0.000000   128     0.1482
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  18.299574  18.964720  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:19:26         -1.10   -73.750626    1      
iter:   2  11:19:27         -1.72   -73.791966    1      
iter:   3  11:19:28         -2.06   -73.851903    1      
iter:   4  11:19:29         -3.25   -73.852151    1      
iter:   5  11:19:29         -3.93   -73.852173    1      
iter:   6  11:19:30         -4.62   -73.852175    1      
iter:   7  11:19:31         -5.16   -73.852175    1      
iter:   8  11:19:31         -5.41   -73.852175    1      
iter:   9  11:19:32         -5.76   -73.852175    1      
iter:  10  11:19:33         -6.18   -73.852175    1      

Occupied states converged after 12 e/g evaluations

Converged after 10 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6240.709666)

Kinetic:        +57.900483
Potential:      -64.652880
External:        +0.000000
XC:             -67.281670
Entropy (-ST):   +0.000000
Local:           +0.181893
--------------------------
Free energy:    -73.852175
Extrapolated:   -73.852175

 Band  Eigenvalues  Occupancy
    0    -21.15130    2.00000
    1    -18.38552    2.00000
    2    -18.38551    2.00000
    3    -14.77110    2.00000
    4    -14.77045    2.00000
    5    -12.79153    2.00000
    6    -11.13783    2.00000
    7    -10.78100    2.00000
    8    -10.12224    2.00000
    9    -10.12223    2.00000
   10     -8.88187    2.00000
   11     -8.13636    2.00000
   12     -8.13614    2.00000
   13     -6.15825    2.00000
   14     -6.15815    2.00000
   15     -0.92801    0.00000
   16     -0.92792    0.00000
   17      1.63384    0.00000
   18      2.88779    0.00000
   19      2.88794    0.00000
   20      3.11605    0.00000
   21      3.85328    0.00000
   22      3.85436    0.00000
   23      4.52179    0.00000
   24      7.15027    0.00000
   25      7.15209    0.00000
   26      7.37462    0.00000
   27      7.37685    0.00000
   28      8.60733    0.00000
   29      8.62423    0.00000
   30      9.92674    0.00000
   31     10.92072    0.00000
   32     10.92095    0.00000
   33     11.46742    0.00000
   34     11.46912    0.00000
   35     11.56103    0.00000
   36     11.56114    0.00000
   37     11.92048    0.00000
   38     12.33510    0.00000
   39     12.77728    0.00000
   40     12.77743    0.00000
   41     14.59817    0.00000
   42     15.43938    0.00000
   43     15.91880    0.00000
   44     15.91967    0.00000
   45     16.08130    0.00000
   46     16.08190    0.00000
   47     16.54491    0.00000
   48     18.71197    0.00000
   49     20.43493    0.00000
   50     21.23252    0.00000
   51     21.57739    0.00000
   52     21.57740    0.00000
   53     22.11206    0.00000
   54     22.11292    0.00000
   55     23.57714    0.00000
   56     23.57726    0.00000
   57     24.23377    0.00000
   58     24.23467    0.00000
   59     25.64220    0.00000
   60     25.64976    0.00000
   61     25.69427    0.00000
   62     25.71783    0.00000
   63     26.63381    0.00000
   64     31.72132    0.00000
   65     32.33452    0.00000
   66     33.28879    0.00000
   67     33.37237    0.00000
   68     33.37255    0.00000
   69     33.46954    0.00000
   70     33.47171    0.00000
   71     34.80408    0.00000
   72     34.80902    0.00000
   73     35.19303    0.00000
   74     35.19305    0.00000
   75     35.71802    0.00000
   76     35.75228    0.00000
   77     35.75362    0.00000
   78     38.52452    0.00000
   79     38.53020    0.00000
   80     40.89251    0.00000
   81     40.90772    0.00000
   82     42.25079    0.00000
   83     42.25124    0.00000
   84     42.50295    0.00000
   85     42.50423    0.00000
   86     42.53822    0.00000
   87     42.53945    0.00000
   88     43.66261    0.00000
   89     43.66454    0.00000
   90     44.42182    0.00000
   91     44.42183    0.00000
   92     47.14336    0.00000
   93     47.43042    0.00000
   94     48.63722    0.00000
   95     52.73201    0.00000
   96     53.16075    0.00000
   97     53.54202    0.00000
   98     53.54579    0.00000
   99     61.15831    0.00000
  100     61.15861    0.00000
  101     64.20639    0.00000
  102     64.21269    0.00000
  103     70.24253    0.00000
  104     70.24566    0.00000
  105     73.93005    0.00000
  106     80.92834    0.00000
  107     90.49038    0.00000

Fermi level: -3.54308

Gap: 5.230 eV
Transition (v -> c):
  (s=0, k=0, n=14, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=15, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.008     0.008   0.1% |
LCAO WFS Initialize:                  0.684     0.025   0.2% |
 Hamiltonian:                         0.659     0.000   0.0% |
  Atomic:                             0.016     0.000   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.018     0.018   0.2% |
  Hartree integrate/restrict:         0.017     0.017   0.2% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.335     0.010   0.1% |
   Communicate from 1D:               0.068     0.068   0.6% |
   Communicate from 2D:               0.066     0.066   0.6% |
   Communicate to 1D:                 0.064     0.064   0.6% |
   Communicate to 2D:                 0.073     0.073   0.6% |
   FFT 1D:                            0.021     0.021   0.2% |
   FFT 2D:                            0.035     0.035   0.3% |
  XC 3D grid:                         0.266     0.266   2.3% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            9.604     0.002   0.0% |
 Density:                             0.050     0.000   0.0% |
  Atomic density matrices:            0.007     0.007   0.1% |
  Mix:                                0.022     0.022   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.021     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.020     0.020   0.2% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            8.837     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.324     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.038     0.038   0.3% |
   Potential matrix:                  0.280     0.280   2.5% ||
  Density:                            0.604     0.000   0.0% |
   Atomic density matrices:           0.062     0.062   0.5% |
   Mix:                               0.285     0.285   2.5% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.253     0.008   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.244     0.244   2.1% ||
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        7.898     0.001   0.0% |
   Atomic:                            0.193     0.007   0.1% |
    XC Correction:                    0.187     0.187   1.6% ||
   Calculate atomic Hamiltonians:     0.021     0.021   0.2% |
   Communicate:                       0.215     0.215   1.9% ||
   Hartree integrate/restrict:        0.208     0.208   1.8% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.036     0.090   0.8% |
    Communicate from 1D:              0.821     0.821   7.2% |--|
    Communicate from 2D:              0.794     0.794   7.0% |--|
    Communicate to 1D:                0.808     0.808   7.1% |--|
    Communicate to 2D:                0.861     0.861   7.6% |--|
    FFT 1D:                           0.246     0.246   2.2% ||
    FFT 2D:                           0.417     0.417   3.7% ||
   XC 3D grid:                        3.174     3.174  27.9% |----------|
   vbar:                              0.048     0.048   0.4% |
  Preconditioning::                   0.002     0.002   0.0% |
  Unitary rotation:                   0.005     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.028     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.003     0.003   0.0% |
  Potential matrix:                   0.023     0.023   0.2% |
 Hamiltonian:                         0.659     0.000   0.0% |
  Atomic:                             0.016     0.001   0.0% |
   XC Correction:                     0.015     0.015   0.1% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.018     0.018   0.2% |
  Hartree integrate/restrict:         0.018     0.018   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.337     0.007   0.1% |
   Communicate from 1D:               0.068     0.068   0.6% |
   Communicate from 2D:               0.066     0.066   0.6% |
   Communicate to 1D:                 0.068     0.068   0.6% |
   Communicate to 2D:                 0.072     0.072   0.6% |
   FFT 1D:                            0.020     0.020   0.2% |
   FFT 2D:                            0.035     0.035   0.3% |
  XC 3D grid:                         0.265     0.265   2.3% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.028     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.003     0.003   0.0% |
  Orbital Layouts:                    0.002     0.002   0.0% |
  Potential matrix:                   0.023     0.023   0.2% |
ST tci:                               0.003     0.003   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.073     0.073   0.6% |
mktci:                                0.002     0.002   0.0% |
Other:                                0.987     0.987   8.7% |--|
------------------------------------------------------------
Total:                                         11.363 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:19:33 2021
