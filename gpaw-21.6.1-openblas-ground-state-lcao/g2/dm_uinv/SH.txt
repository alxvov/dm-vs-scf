
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:34:09 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16.0
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/S.PBE.gz
  compensation charges: gauss, rc=0.24, lmax=2
  cutoffs: 1.77(filt), 1.66(core),
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -10870.110060

Spin-polarized calculation.
Magnetic moment: 1.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 50.99 MiB
    Density: 29.10 MiB
      Arrays: 28.39 MiB
      Localized functions: 0.71 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.16 MiB
      Arrays: 21.10 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 0.73 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.00 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.72 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 18
Number of bands in calculation: 18
Number of valence electrons: 7
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            S                    |  
 |        |                                 |  
 |        |            H                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 S      7.000000    7.000000    8.344413    ( 0.0000,  0.0000,  1.0000)
   1 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.344413   104     0.1475

  Lengths:  14.000000  14.000000  15.344413
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1506

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:34:13         -1.67    -5.387794    1        +1.0000
iter:   2  11:34:13         -1.98    -5.398765    1        +1.0000
iter:   3  11:34:14         -2.38    -5.401043    1        +1.0000
iter:   4  11:34:15         -3.08    -5.401116    1        +1.0000
iter:   5  11:34:15         -3.79    -5.401121    1        +1.0000
iter:   6  11:34:16         -4.35    -5.401122    1        +1.0000
iter:   7  11:34:17         -4.71    -5.401122    1        +1.0000
iter:   8  11:34:17         -4.92    -5.401122    1        +1.0000
iter:   9  11:34:18         -5.39    -5.401123    1        +1.0000
iter:  10  11:34:19         -5.01    -5.401123    1        +1.0000
iter:  11  11:34:20         -4.42    -5.401123    1        +1.0000
iter:  12  11:34:20         -4.00    -5.401123    1        +1.0000
iter:  13  11:34:21         -3.53    -5.401123    1        +1.0000
iter:  14  11:34:22         -3.05    -5.401123    1        +1.0000
iter:  15  11:34:22         -2.54    -5.401125    1        +1.0000
iter:  16  11:34:23         -2.02    -5.401128    1        +1.0000
iter:  17  11:34:24         -2.99    -5.401134    1        +1.0000
iter:  18  11:34:25         -2.04    -5.401153    1        +1.0000
iter:  19  11:34:26         -1.94    -5.401160    1        +1.0000
iter:  20  11:34:27         -3.98    -5.401164    1        +1.0000
iter:  21  11:34:28         -4.55    -5.401164    1        +1.0000
iter:  22  11:34:29         -4.97    -5.401164    1        +1.0000
iter:  23  11:34:29         -5.40    -5.401164    1        +1.0000
iter:  24  11:34:30         -5.43    -5.401164    1        +1.0000
iter:  25  11:34:31         -4.98    -5.401164    1        +1.0000
iter:  26  11:34:32         -4.52    -5.401164    1        +1.0000
iter:  27  11:34:32         -4.03    -5.401164    1        +1.0000
iter:  28  11:34:33         -3.58    -5.401164    1        +1.0000
iter:  29  11:34:34         -3.13    -5.401165    1        +1.0000
iter:  30  11:34:34         -2.72    -5.401166    1        +1.0000
iter:  31  11:34:35         -2.47    -5.401167    1        +1.0000
iter:  32  11:34:36         -2.65    -5.401168    1        +1.0000
iter:  33  11:34:37         -3.26    -5.401168    1        +1.0000
iter:  34  11:34:37         -3.43    -5.401168    1        +1.0000
iter:  35  11:34:38         -4.29    -5.401168    1        +1.0000
iter:  36  11:34:39         -5.10    -5.401168    1        +1.0000
iter:  37  11:34:39         -5.55    -5.401168    1        +1.0000
iter:  38  11:34:40         -6.46    -5.401168    1        +1.0000

Occupied states converged after 41 e/g evaluations

Converged after 38 iterations.

Dipole moment: (0.000000, 0.000000, -0.161783) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999999)
Local magnetic moments:
   0 S  ( 0.000000,  0.000000,  0.490772)
   1 H  ( 0.000000,  0.000000, -0.006408)

Energy contributions relative to reference atoms: (reference = -10870.110060)

Kinetic:         +9.115904
Potential:       -8.549488
External:        +0.000000
XC:              -5.962769
Entropy (-ST):   +0.000000
Local:           -0.004815
--------------------------
Free energy:     -5.401168
Extrapolated:    -5.401168

Spin contamination: 0.021971 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -17.96863    1.00000    -17.07428    1.00000
    1     -9.76838    1.00000     -9.41266    1.00000
    2     -7.36334    1.00000     -5.64166    1.00000
    3     -6.03103    1.00000     -5.33198    0.00000
    4      0.51664    0.00000      0.81407    0.00000
    5      6.23380    0.00000      6.54313    0.00000
    6     10.21577    0.00000     11.34368    0.00000
    7     10.55708    0.00000     11.44405    0.00000
    8     11.16757    0.00000     11.54063    0.00000
    9     11.32890    0.00000     11.69012    0.00000
   10     11.83769    0.00000     12.07160    0.00000
   11     12.28020    0.00000     13.64006    0.00000
   12     12.31736    0.00000     13.91677    0.00000
   13     18.66141    0.00000     19.07157    0.00000
   14     22.84859    0.00000     23.46423    0.00000
   15     26.84021    0.00000     27.31422    0.00000
   16     27.29833    0.00000     27.34352    0.00000
   17     43.36702    0.00000     43.51774    0.00000

Fermi level: -2.56251

Gap: 5.849 eV
Transition (v -> c):
  (s=1, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.002     0.002   0.0% |
LCAO WFS Initialize:                  0.708     0.025   0.1% |
 Hamiltonian:                         0.683     0.002   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.040     0.040   0.1% |
  Hartree integrate/restrict:         0.018     0.018   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.208     0.006   0.0% |
   Communicate from 1D:               0.042     0.042   0.1% |
   Communicate from 2D:               0.041     0.041   0.1% |
   Communicate to 1D:                 0.042     0.042   0.1% |
   Communicate to 2D:                 0.045     0.045   0.1% |
   FFT 1D:                            0.010     0.010   0.0% |
   FFT 2D:                            0.022     0.022   0.1% |
  XC 3D grid:                         0.412     0.412   1.3% ||
  vbar:                               0.003     0.003   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                           29.713     0.018   0.1% |
 Density:                             0.028     0.000   0.0% |
  Atomic density matrices:            0.002     0.002   0.0% |
  Mix:                                0.024     0.024   0.1% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.003     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.002     0.002   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           28.989     0.004   0.0% |
  Broadcast gradients:                0.001     0.001   0.0% |
  Calculate gradients:                0.152     0.005   0.0% |
   Construct Gradient Matrix:         0.007     0.007   0.0% |
   DenseAtomicCorrection:             0.002     0.002   0.0% |
   Distribute overlap matrix:         0.049     0.049   0.2% |
   Potential matrix:                  0.088     0.088   0.3% |
  Density:                            1.173     0.001   0.0% |
   Atomic density matrices:           0.063     0.063   0.2% |
   Mix:                               0.985     0.985   3.2% ||
   Multipole moments:                 0.004     0.004   0.0% |
   Normalize:                         0.008     0.008   0.0% |
   Pseudo density:                    0.112     0.026   0.1% |
    Calculate density matrix:         0.003     0.003   0.0% |
    Construct density:                0.083     0.083   0.3% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.007     0.007   0.0% |
  Hamiltonian:                       27.632     0.061   0.2% |
   Atomic:                            0.009     0.009   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.016     0.016   0.1% |
   Communicate:                       1.632     1.632   5.2% |-|
   Hartree integrate/restrict:        0.709     0.709   2.3% ||
   New Kinetic Energy:                0.004     0.002   0.0% |
    Pseudo part:                      0.002     0.002   0.0% |
   Poisson:                           8.669     0.181   0.6% |
    Communicate from 1D:              1.829     1.829   5.9% |-|
    Communicate from 2D:              1.732     1.732   5.6% |-|
    Communicate to 1D:                1.807     1.807   5.8% |-|
    Communicate to 2D:                1.830     1.830   5.9% |-|
    FFT 1D:                           0.421     0.421   1.4% ||
    FFT 2D:                           0.869     0.869   2.8% ||
   XC 3D grid:                       16.431    16.431  52.8% |--------------------|
   vbar:                              0.099     0.099   0.3% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.019     0.002   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.001     0.001   0.0% |
   Eigendecomposition:                0.016     0.016   0.1% |
 Get canonical representation:        0.004     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.002     0.002   0.0% |
 Hamiltonian:                         0.670     0.001   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.040     0.040   0.1% |
  Hartree integrate/restrict:         0.017     0.017   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.212     0.005   0.0% |
   Communicate from 1D:               0.044     0.044   0.1% |
   Communicate from 2D:               0.042     0.042   0.1% |
   Communicate to 1D:                 0.045     0.045   0.1% |
   Communicate to 2D:                 0.045     0.045   0.1% |
   FFT 1D:                            0.010     0.010   0.0% |
   FFT 2D:                            0.021     0.021   0.1% |
  XC 3D grid:                         0.396     0.396   1.3% ||
  vbar:                               0.002     0.002   0.0% |
 LCAO eigensolver:                    0.004     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.000     0.000   0.0% |
  Potential matrix:                   0.002     0.002   0.0% |
ST tci:                               0.000     0.000   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.074     0.074   0.2% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.637     0.637   2.0% ||
------------------------------------------------------------
Total:                                         31.135 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:34:40 2021
