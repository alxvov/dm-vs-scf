
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:15:43 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2092.726718

Spin-polarized calculation.
Magnetic moment: 1.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*112*92 grid
  Fine grid: 208*224*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*224*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 62.53 MiB
    Density: 35.20 MiB
      Arrays: 34.64 MiB
      Localized functions: 0.56 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 25.78 MiB
      Arrays: 25.74 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 1.55 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.03 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.51 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 41
Number of bands in calculation: 41
Number of valence electrons: 11
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |            H C                       |  
 |         |          H  C H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.926548    7.578367    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C      7.926548    8.865627    7.000000    ( 0.0000,  0.0000,  1.0000)
   2 H      7.000000    7.002795    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      8.845933    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      7.186737    9.652824    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.845933    0.000000    0.000000   104     0.1524
  2. axis:    no     0.000000   16.652824    0.000000   112     0.1487
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  15.845933  16.652824  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1511

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:15:48         -1.01   -24.836213    1        +1.0000
iter:   2  11:15:50         -1.67   -24.899605    1        +1.0000
iter:   3  11:15:51         -2.22   -24.911691    1        +1.0000
iter:   4  11:15:52         -2.96   -24.912492    1        +1.0000
iter:   5  11:15:53         -3.37   -24.912614    1        +1.0000
iter:   6  11:15:54         -3.91   -24.912626    1        +1.0000
iter:   7  11:15:54         -4.03   -24.912627    1        +1.0000
iter:   8  11:15:55         -4.22   -24.912628    1        +1.0000
iter:   9  11:15:56         -4.66   -24.912628    1        +1.0000
iter:  10  11:15:57         -5.15   -24.912628    1        +1.0000
iter:  11  11:15:58         -5.82   -24.912628    1        +1.0000
iter:  12  11:15:59         -6.08   -24.912628    1        +1.0000

Occupied states converged after 15 e/g evaluations

Converged after 12 iterations.

Dipole moment: (-0.106277, -0.058957, 0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999998)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000, -0.029548)
   1 C  ( 0.000000,  0.000000,  0.320285)
   2 H  ( 0.000000,  0.000000,  0.031933)
   3 H  ( 0.000000,  0.000000,  0.020337)
   4 H  ( 0.000000,  0.000000,  0.010515)

Energy contributions relative to reference atoms: (reference = -2092.726718)

Kinetic:        +22.852612
Potential:      -24.241910
External:        +0.000000
XC:             -23.582127
Entropy (-ST):   +0.000000
Local:           +0.058798
--------------------------
Free energy:    -24.912628
Extrapolated:   -24.912628

Spin contamination: 0.079734 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -19.06517    1.00000    -18.67651    1.00000
    1    -14.06179    1.00000    -13.59456    1.00000
    2    -11.27746    1.00000    -10.93627    1.00000
    3    -10.39118    1.00000    -10.06602    1.00000
    4     -6.93568    1.00000     -6.69812    1.00000
    5     -5.27380    1.00000     -2.82215    0.00000
    6     -0.54808    0.00000     -0.19094    0.00000
    7      2.59910    0.00000      2.76270    0.00000
    8      2.69646    0.00000      2.98156    0.00000
    9      4.38374    0.00000      4.59436    0.00000
   10      8.34710    0.00000      8.52485    0.00000
   11      8.83714    0.00000      9.18607    0.00000
   12      9.63121    0.00000     10.07104    0.00000
   13      9.94734    0.00000     10.53960    0.00000
   14     10.82530    0.00000     11.23729    0.00000
   15     12.29442    0.00000     12.54301    0.00000
   16     12.30926    0.00000     13.17541    0.00000
   17     15.47624    0.00000     15.84272    0.00000
   18     17.37594    0.00000     17.81342    0.00000
   19     18.28594    0.00000     18.53392    0.00000
   20     20.36890    0.00000     20.58917    0.00000
   21     21.06919    0.00000     21.22758    0.00000
   22     21.21838    0.00000     21.85140    0.00000
   23     24.62927    0.00000     25.24492    0.00000
   24     24.69913    0.00000     25.72605    0.00000
   25     27.84475    0.00000     28.50393    0.00000
   26     30.30977    0.00000     30.60629    0.00000
   27     31.25992    0.00000     31.55421    0.00000
   28     32.24311    0.00000     32.58760    0.00000
   29     33.74592    0.00000     34.61084    0.00000
   30     34.68639    0.00000     35.10173    0.00000
   31     35.30060    0.00000     35.51782    0.00000
   32     40.68843    0.00000     41.23142    0.00000
   33     42.07585    0.00000     42.44531    0.00000
   34     43.87342    0.00000     44.26309    0.00000
   35     44.58554    0.00000     45.50355    0.00000
   36     45.92711    0.00000     46.33001    0.00000
   37     50.85030    0.00000     51.11304    0.00000
   38     53.89988    0.00000     54.48222    0.00000
   39     58.70183    0.00000     59.13643    0.00000
   40     68.40564    0.00000     69.17141    0.00000

Fermi level: -2.91094

Gap: 2.452 eV
Transition (v -> c):
  (s=0, k=0, n=5, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.004     0.004   0.0% |
LCAO WFS Initialize:                  0.860     0.033   0.2% |
 Hamiltonian:                         0.827     0.003   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.033     0.033   0.2% |
  Hartree integrate/restrict:         0.020     0.020   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.263     0.007   0.0% |
   Communicate from 1D:               0.054     0.054   0.3% |
   Communicate from 2D:               0.050     0.050   0.3% |
   Communicate to 1D:                 0.054     0.054   0.3% |
   Communicate to 2D:                 0.054     0.054   0.3% |
   FFT 1D:                            0.016     0.016   0.1% |
   FFT 2D:                            0.029     0.029   0.2% |
  XC 3D grid:                         0.504     0.504   3.2% ||
  vbar:                               0.003     0.003   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                           13.982     0.007   0.0% |
 Density:                             0.044     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.030     0.030   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.012     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.011     0.011   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           13.088     0.002   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.191     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.001     0.001   0.0% |
   Distribute overlap matrix:         0.005     0.005   0.0% |
   Potential matrix:                  0.179     0.179   1.1% |
  Density:                            0.664     0.000   0.0% |
   Atomic density matrices:           0.023     0.023   0.1% |
   Mix:                               0.462     0.462   2.9% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.004     0.004   0.0% |
   Pseudo density:                    0.174     0.014   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.159     0.159   1.0% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                       12.219     0.027   0.2% |
   Atomic:                            0.016     0.015   0.1% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.007     0.007   0.0% |
   Communicate:                       0.475     0.475   3.0% ||
   Hartree integrate/restrict:        0.305     0.305   1.9% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.914     0.086   0.5% |
    Communicate from 1D:              0.812     0.812   5.2% |-|
    Communicate from 2D:              0.747     0.747   4.8% |-|
    Communicate to 1D:                0.806     0.806   5.1% |-|
    Communicate to 2D:                0.803     0.803   5.1% |-|
    FFT 1D:                           0.242     0.242   1.5% ||
    FFT 2D:                           0.418     0.418   2.7% ||
   XC 3D grid:                        7.429     7.429  47.3% |------------------|
   vbar:                              0.045     0.045   0.3% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.008     0.001   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.007     0.007   0.0% |
 Get canonical representation:        0.013     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Potential matrix:                   0.012     0.012   0.1% |
 Hamiltonian:                         0.816     0.002   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.032     0.032   0.2% |
  Hartree integrate/restrict:         0.021     0.021   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.261     0.006   0.0% |
   Communicate from 1D:               0.055     0.055   0.3% |
   Communicate from 2D:               0.050     0.050   0.3% |
   Communicate to 1D:                 0.053     0.053   0.3% |
   Communicate to 2D:                 0.053     0.053   0.3% |
   FFT 1D:                            0.016     0.016   0.1% |
   FFT 2D:                            0.029     0.029   0.2% |
  XC 3D grid:                         0.496     0.496   3.2% ||
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.014     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.012     0.012   0.1% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.075     0.075   0.5% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.767     0.767   4.9% |-|
------------------------------------------------------------
Total:                                         15.690 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:15:59 2021
