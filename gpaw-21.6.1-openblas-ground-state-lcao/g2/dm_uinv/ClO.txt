
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:28:47 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -14595.801697

Spin-polarized calculation.
Magnetic moment: 1.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 50.73 MiB
    Density: 28.94 MiB
      Arrays: 28.39 MiB
      Localized functions: 0.55 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.14 MiB
      Arrays: 21.10 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.64 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.63 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 26
Number of valence electrons: 13
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Cl                   |  
 |        |                                 |  
 |        |            O                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Cl     7.000000    7.000000    8.606787    ( 0.0000,  0.0000,  1.0000)
   1 O      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.606787   104     0.1501

  Lengths:  14.000000  14.000000  15.606787
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1515

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:28:51         -0.72    -4.364384    1        +1.0000
iter:   2  11:28:52         -1.43    -4.579227    1        +1.0000
iter:   3  11:28:53         -1.83    -4.678779    1        +1.0000
iter:   4  11:28:53         -2.47    -4.690690    1        +1.0000
iter:   5  11:28:54         -2.64    -4.693081    1        +1.0000
iter:   6  11:28:55         -3.42    -4.693187    1        +1.0000
iter:   7  11:28:56         -3.70    -4.693204    1        +1.0000
iter:   8  11:28:57         -3.88    -4.693208    1        +1.0000
iter:   9  11:28:58         -4.54    -4.693208    1        +1.0000
iter:  10  11:28:58         -4.89    -4.693209    1        +1.0000
iter:  11  11:28:59         -4.97    -4.693209    1        +1.0000
iter:  12  11:29:00         -4.68    -4.693209    1        +1.0000
iter:  13  11:29:00         -3.94    -4.693210    1        +1.0000
iter:  14  11:29:01         -3.46    -4.693211    1        +1.0000
iter:  15  11:29:02         -3.46    -4.693215    1        +1.0000
iter:  16  11:29:03         -2.42    -4.693254    1        +1.0000
iter:  17  11:29:04         -1.51    -4.693299    1        +1.0000
iter:  18  11:29:05         -1.70    -4.693320    1        +1.0000
iter:  19  11:29:06         -2.16    -4.693327    1        +1.0000
iter:  20  11:29:07         -3.22    -4.693453    1        +1.0000
iter:  21  11:29:09         -3.51    -4.693496    1        +1.0000
iter:  22  11:29:10         -3.61    -4.693508    1        +1.0000
iter:  23  11:29:10         -4.07    -4.693511    1        +1.0000
iter:  24  11:29:11         -4.90    -4.693512    1        +1.0000
iter:  25  11:29:12         -4.86    -4.693512    1        +1.0000
iter:  26  11:29:12         -4.73    -4.693512    1        +1.0000
iter:  27  11:29:13         -3.94    -4.693513    1        +1.0000
iter:  28  11:29:14         -3.77    -4.693514    1        +1.0000
iter:  29  11:29:15         -3.31    -4.693519    1        +1.0000
iter:  30  11:29:15         -2.59    -4.693542    1        +1.0000
iter:  31  11:29:16         -2.36    -4.693576    1        +1.0000
iter:  32  11:29:17         -2.25    -4.693618    1        +1.0000
iter:  33  11:29:17         -2.32    -4.693661    1        +1.0000
iter:  34  11:29:19         -3.38    -4.693673    1        +1.0000
iter:  35  11:29:19         -2.61    -4.693677    1        +1.0000
iter:  36  11:29:20         -3.20    -4.693680    1        +1.0000
iter:  37  11:29:21         -2.90    -4.693682    1        +1.0000
iter:  38  11:29:22         -2.94    -4.693683    1        +1.0000
iter:  39  11:29:23         -4.71    -4.693683    1        +1.0000
iter:  40  11:29:24         -4.85    -4.693683    1        +1.0000
iter:  41  11:29:24         -5.14    -4.693683    1        +1.0000
iter:  42  11:29:25         -5.99    -4.693683    1        +1.0000
iter:  43  11:29:26         -6.17    -4.693683    1        +1.0000

Occupied states converged after 52 e/g evaluations

Converged after 43 iterations.

Dipole moment: (-0.000000, -0.000000, 0.318884) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000008)
Local magnetic moments:
   0 Cl ( 0.000000,  0.000000,  0.129622)
   1 O  ( 0.000000,  0.000000,  0.476507)

Energy contributions relative to reference atoms: (reference = -14595.801697)

Kinetic:        +10.979221
Potential:       -8.098458
External:        +0.000000
XC:              -7.641399
Entropy (-ST):   +0.000000
Local:           +0.066953
--------------------------
Free energy:     -4.693683
Extrapolated:    -4.693683

Spin contamination: 0.021159 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -26.65960    1.00000    -25.95100    1.00000
    1    -19.34113    1.00000    -18.68129    1.00000
    2    -11.55100    1.00000    -11.11450    1.00000
    3    -10.92704    1.00000     -9.78752    1.00000
    4     -9.99944    1.00000     -9.77737    1.00000
    5     -7.14739    1.00000     -5.66435    1.00000
    6     -6.02360    1.00000     -5.38454    0.00000
    7     -1.95046    0.00000     -1.57560    0.00000
    8     12.07045    0.00000     12.22851    0.00000
    9     12.28153    0.00000     12.67444    0.00000
   10     12.70999    0.00000     12.75988    0.00000
   11     13.44590    0.00000     14.15863    0.00000
   12     13.87582    0.00000     14.17353    0.00000
   13     13.87750    0.00000     14.30451    0.00000
   14     14.04759    0.00000     14.32619    0.00000
   15     15.47412    0.00000     15.65217    0.00000
   16     18.06206    0.00000     18.36086    0.00000
   17     18.66426    0.00000     19.43182    0.00000
   18     19.26796    0.00000     19.56928    0.00000
   19     23.92832    0.00000     24.28419    0.00000
   20     36.82862    0.00000     37.45950    0.00000
   21     45.17479    0.00000     46.30907    0.00000
   22     45.18790    0.00000     46.45547    0.00000
   23     48.60710    0.00000     49.75960    0.00000
   24     49.56725    0.00000     49.79228    0.00000
   25     61.69030    0.00000     62.20943    0.00000

Fermi level: -3.80740

Gap: 3.434 eV
Transition (v -> c):
  (s=1, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.002     0.002   0.0% |
LCAO WFS Initialize:                  0.705     0.025   0.1% |
 Hamiltonian:                         0.680     0.002   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.039     0.039   0.1% |
  Hartree integrate/restrict:         0.018     0.018   0.0% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.207     0.006   0.0% |
   Communicate from 1D:               0.042     0.042   0.1% |
   Communicate from 2D:               0.041     0.041   0.1% |
   Communicate to 1D:                 0.042     0.042   0.1% |
   Communicate to 2D:                 0.045     0.045   0.1% |
   FFT 1D:                            0.010     0.010   0.0% |
   FFT 2D:                            0.022     0.022   0.1% |
  XC 3D grid:                         0.409     0.409   1.1% |
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                           37.294     0.021   0.1% |
 Density:                             0.028     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.023     0.023   0.1% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.003     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.002     0.002   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           36.570     0.006   0.0% |
  Broadcast gradients:                0.001     0.001   0.0% |
  Calculate gradients:                0.185     0.009   0.0% |
   Construct Gradient Matrix:         0.010     0.010   0.0% |
   DenseAtomicCorrection:             0.002     0.002   0.0% |
   Distribute overlap matrix:         0.037     0.037   0.1% |
   Potential matrix:                  0.127     0.127   0.3% |
  Density:                            1.461     0.001   0.0% |
   Atomic density matrices:           0.052     0.052   0.1% |
   Mix:                               1.241     1.241   3.2% ||
   Multipole moments:                 0.004     0.004   0.0% |
   Normalize:                         0.010     0.010   0.0% |
   Pseudo density:                    0.154     0.034   0.1% |
    Calculate density matrix:         0.004     0.004   0.0% |
    Construct density:                0.116     0.116   0.3% |
    Symmetrize density:               0.001     0.001   0.0% |
  Get Search Direction:               0.008     0.008   0.0% |
  Hamiltonian:                       34.882     0.077   0.2% |
   Atomic:                            0.012     0.012   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.020     0.020   0.1% |
   Communicate:                       2.019     2.019   5.2% |-|
   Hartree integrate/restrict:        0.858     0.858   2.2% ||
   New Kinetic Energy:                0.005     0.002   0.0% |
    Pseudo part:                      0.003     0.003   0.0% |
   Poisson:                          10.978     0.236   0.6% |
    Communicate from 1D:              2.239     2.239   5.8% |-|
    Communicate from 2D:              2.157     2.157   5.6% |-|
    Communicate to 1D:                2.384     2.384   6.1% |-|
    Communicate to 2D:                2.288     2.288   5.9% |-|
    FFT 1D:                           0.543     0.543   1.4% ||
    FFT 2D:                           1.131     1.131   2.9% ||
   XC 3D grid:                       20.788    20.788  53.6% |--------------------|
   vbar:                              0.125     0.125   0.3% |
  Preconditioning::                   0.002     0.002   0.0% |
  Unitary rotation:                   0.025     0.002   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.001     0.001   0.0% |
   Eigendecomposition:                0.021     0.021   0.1% |
 Get canonical representation:        0.004     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.002     0.002   0.0% |
 Hamiltonian:                         0.668     0.001   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.039     0.039   0.1% |
  Hartree integrate/restrict:         0.017     0.017   0.0% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.210     0.005   0.0% |
   Communicate from 1D:               0.044     0.044   0.1% |
   Communicate from 2D:               0.041     0.041   0.1% |
   Communicate to 1D:                 0.046     0.046   0.1% |
   Communicate to 2D:                 0.043     0.043   0.1% |
   FFT 1D:                            0.010     0.010   0.0% |
   FFT 2D:                            0.021     0.021   0.1% |
  XC 3D grid:                         0.398     0.398   1.0% |
  vbar:                               0.002     0.002   0.0% |
 LCAO eigensolver:                    0.004     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.002     0.002   0.0% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.136     0.136   0.4% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.654     0.654   1.7% ||
------------------------------------------------------------
Total:                                         38.792 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:29:26 2021
