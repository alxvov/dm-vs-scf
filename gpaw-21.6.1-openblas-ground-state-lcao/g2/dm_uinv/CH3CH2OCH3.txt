
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:10:03 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -5223.657946

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*124*104 grid
  Fine grid: 240*248*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*248*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 66.59 MiB
    Density: 38.65 MiB
      Arrays: 37.31 MiB
      Localized functions: 1.34 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 24.51 MiB
      Arrays: 24.40 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.10 MiB
    Wavefunctions: 3.43 MiB
      C [qnM]: 0.06 MiB
      S, T [2 x qmm]: 0.13 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.24 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 12
Number of atomic orbitals: 92
Number of bands in calculation: 92
Number of valence electrons: 26
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .-------------------------------------------.  
           /|                                           |  
          / |                                           |  
         /  |                                           |  
        /   |                                           |  
       /    |                                           |  
      /     |                                           |  
     /      |                                           |  
    /       |                                           |  
   /        |                                           |  
  /         |                                           |  
 *          |                                           |  
 |          |                                           |  
 |          |           H      H                        |  
 |          |             C      H                      |  
 |          |               OC H                        |  
 |          |           H     HC                        |  
 |          |                    H                      |  
 |          |                                           |  
 |          .-------------------------------------------.  
 |         /                                           /   
 |        /                                           /    
 |       /                                           /     
 |      /                                           /      
 |     /                                           /       
 |    /                                           /        
 |   /                                           /         
 |  /                                           /          
 | /                                           /           
 |/                                           /            
 *-------------------------------------------*             

Positions:
   0 O      8.961292    8.600584    7.891710    ( 0.0000,  0.0000,  0.0000)
   1 C      8.954863   10.019170    7.891710    ( 0.0000,  0.0000,  0.0000)
   2 C     10.279381    8.087296    7.891710    ( 0.0000,  0.0000,  0.0000)
   3 C      7.512694   10.473650    7.891710    ( 0.0000,  0.0000,  0.0000)
   4 H      9.485825   10.399809    8.778591    ( 0.0000,  0.0000,  0.0000)
   5 H      9.485825   10.399809    7.004829    ( 0.0000,  0.0000,  0.0000)
   6 H     10.196511    7.000000    7.891710    ( 0.0000,  0.0000,  0.0000)
   7 H     10.836192    8.407400    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H     10.836192    8.407400    8.783420    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000   10.093930    7.005855    ( 0.0000,  0.0000,  0.0000)
  10 H      7.000000   10.093930    8.777565    ( 0.0000,  0.0000,  0.0000)
  11 H      7.452838   11.565408    7.891710    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.836192    0.000000    0.000000   120     0.1486
  2. axis:    no     0.000000   18.565408    0.000000   124     0.1497
  3. axis:    no     0.000000    0.000000   15.783420   104     0.1518

  Lengths:  17.836192  18.565408  15.783420
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1500

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:10:08         -1.02   -61.189099    1      
iter:   2  11:10:09         -1.67   -61.216108    1      
iter:   3  11:10:10         -1.99   -61.296516    1      
iter:   4  11:10:11         -2.36   -61.299975    1      
iter:   5  11:10:11         -2.69   -61.302920    1      
iter:   6  11:10:12         -3.70   -61.302951    1      
iter:   7  11:10:13         -4.24   -61.302962    1      
iter:   8  11:10:14         -4.29   -61.302963    1      
iter:   9  11:10:15         -4.65   -61.302963    1      
iter:  10  11:10:15         -4.91   -61.302964    1      
iter:  11  11:10:16         -5.36   -61.302964    1      
iter:  12  11:10:17         -6.11   -61.302964    1      

Occupied states converged after 14 e/g evaluations

Converged after 12 iterations.

Dipole moment: (0.159044, 0.126479, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5223.657946)

Kinetic:        +52.160435
Potential:      -57.272829
External:        +0.000000
XC:             -56.332237
Entropy (-ST):   +0.000000
Local:           +0.141666
--------------------------
Free energy:    -61.302964
Extrapolated:   -61.302964

 Band  Eigenvalues  Occupancy
    0    -25.58243    2.00000
    1    -18.82514    2.00000
    2    -16.52207    2.00000
    3    -14.76787    2.00000
    4    -11.87017    2.00000
    5    -11.58089    2.00000
    6    -10.88563    2.00000
    7    -10.08374    2.00000
    8     -9.20767    2.00000
    9     -8.64970    2.00000
   10     -8.37103    2.00000
   11     -7.34559    2.00000
   12     -5.50702    2.00000
   13      1.76821    0.00000
   14      2.40553    0.00000
   15      2.89998    0.00000
   16      3.06200    0.00000
   17      3.49651    0.00000
   18      3.85144    0.00000
   19      4.07063    0.00000
   20      4.24601    0.00000
   21      4.85250    0.00000
   22      5.40173    0.00000
   23      6.03728    0.00000
   24      8.45783    0.00000
   25      8.71588    0.00000
   26      9.66593    0.00000
   27      9.94536    0.00000
   28     10.76627    0.00000
   29     11.39811    0.00000
   30     11.79904    0.00000
   31     12.50251    0.00000
   32     13.95673    0.00000
   33     14.71923    0.00000
   34     14.90463    0.00000
   35     15.22398    0.00000
   36     15.52256    0.00000
   37     15.58804    0.00000
   38     16.04466    0.00000
   39     16.51295    0.00000
   40     16.80270    0.00000
   41     16.82920    0.00000
   42     17.45213    0.00000
   43     21.20534    0.00000
   44     21.55661    0.00000
   45     21.77849    0.00000
   46     23.67665    0.00000
   47     24.71106    0.00000
   48     24.81225    0.00000
   49     26.26140    0.00000
   50     27.93364    0.00000
   51     28.31452    0.00000
   52     29.09942    0.00000
   53     30.57295    0.00000
   54     31.31361    0.00000
   55     32.17506    0.00000
   56     33.87025    0.00000
   57     34.09001    0.00000
   58     35.16633    0.00000
   59     35.19889    0.00000
   60     35.74461    0.00000
   61     35.75727    0.00000
   62     35.92545    0.00000
   63     36.69680    0.00000
   64     37.22304    0.00000
   65     37.39442    0.00000
   66     37.73626    0.00000
   67     39.48221    0.00000
   68     40.11092    0.00000
   69     40.62945    0.00000
   70     41.04844    0.00000
   71     42.57162    0.00000
   72     43.56088    0.00000
   73     44.61040    0.00000
   74     44.84299    0.00000
   75     45.90077    0.00000
   76     46.80543    0.00000
   77     46.99505    0.00000
   78     49.56817    0.00000
   79     50.99018    0.00000
   80     51.16559    0.00000
   81     51.68013    0.00000
   82     52.88913    0.00000
   83     53.54152    0.00000
   84     55.08186    0.00000
   85     55.16080    0.00000
   86     55.99592    0.00000
   87     60.74681    0.00000
   88     61.51738    0.00000
   89     64.74568    0.00000
   90     67.63834    0.00000
   91     69.65887    0.00000

Fermi level: -1.86941

Gap: 7.275 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.008     0.008   0.1% |
LCAO WFS Initialize:                  0.740     0.025   0.2% |
 Hamiltonian:                         0.715     0.000   0.0% |
  Atomic:                             0.016     0.000   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.005     0.005   0.0% |
  Communicate:                        0.006     0.006   0.0% |
  Hartree integrate/restrict:         0.021     0.021   0.2% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.370     0.010   0.1% |
   Communicate from 1D:               0.077     0.077   0.6% |
   Communicate from 2D:               0.071     0.071   0.5% |
   Communicate to 1D:                 0.068     0.068   0.5% |
   Communicate to 2D:                 0.078     0.078   0.6% |
   FFT 1D:                            0.026     0.026   0.2% |
   FFT 2D:                            0.039     0.039   0.3% |
  XC 3D grid:                         0.291     0.291   2.1% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                           11.701     0.003   0.0% |
 Density:                             0.046     0.000   0.0% |
  Atomic density matrices:            0.011     0.011   0.1% |
  Mix:                                0.023     0.023   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.012     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.011     0.011   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           10.886     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.335     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.147     0.147   1.1% |
   Potential matrix:                  0.182     0.182   1.3% ||
  Density:                            0.669     0.000   0.0% |
   Atomic density matrices:           0.156     0.156   1.1% |
   Mix:                               0.336     0.336   2.4% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.172     0.010   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.161     0.161   1.2% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        9.871     0.001   0.0% |
   Atomic:                            0.224     0.006   0.0% |
    XC Correction:                    0.219     0.219   1.6% ||
   Calculate atomic Hamiltonians:     0.053     0.053   0.4% |
   Communicate:                       0.095     0.095   0.7% |
   Hartree integrate/restrict:        0.273     0.273   2.0% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           5.116     0.118   0.9% |
    Communicate from 1D:              1.051     1.051   7.6% |--|
    Communicate from 2D:              0.994     0.994   7.2% |--|
    Communicate to 1D:                0.957     0.957   6.9% |--|
    Communicate to 2D:                1.093     1.093   7.9% |--|
    FFT 1D:                           0.364     0.364   2.6% ||
    FFT 2D:                           0.539     0.539   3.9% |-|
   XC 3D grid:                        4.045     4.045  29.4% |-----------|
   vbar:                              0.062     0.062   0.4% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.006     0.001   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.025     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.011     0.011   0.1% |
  Potential matrix:                   0.013     0.013   0.1% |
 Hamiltonian:                         0.716     0.000   0.0% |
  Atomic:                             0.016     0.000   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.003     0.003   0.0% |
  Communicate:                        0.007     0.007   0.0% |
  Hartree integrate/restrict:         0.023     0.023   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.373     0.008   0.1% |
   Communicate from 1D:               0.077     0.077   0.6% |
   Communicate from 2D:               0.074     0.074   0.5% |
   Communicate to 1D:                 0.069     0.069   0.5% |
   Communicate to 2D:                 0.081     0.081   0.6% |
   FFT 1D:                            0.026     0.026   0.2% |
   FFT 2D:                            0.039     0.039   0.3% |
  XC 3D grid:                         0.290     0.290   2.1% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.025     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.011     0.011   0.1% |
  Orbital Layouts:                    0.002     0.002   0.0% |
  Potential matrix:                   0.013     0.013   0.1% |
ST tci:                               0.002     0.002   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.201     0.201   1.5% ||
mktci:                                0.002     0.002   0.0% |
Other:                                1.119     1.119   8.1% |--|
------------------------------------------------------------
Total:                                         13.775 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:10:17 2021
