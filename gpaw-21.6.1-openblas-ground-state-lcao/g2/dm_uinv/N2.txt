
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:35:00 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7.0
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/N.PBE.gz
  compensation charges: gauss, rc=0.18, lmax=2
  cutoffs: 1.11(filt), 0.96(core),
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

Reference energy: -2963.443008

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*100 grid
  Fine grid: 184*184*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 34.56 MiB
    Density: 20.57 MiB
      Arrays: 20.28 MiB
      Localized functions: 0.29 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 13.29 MiB
      Arrays: 13.27 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.70 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.68 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 26
Number of valence electrons: 10
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 N      7.000000    7.000000    8.129980    ( 0.0000,  0.0000,  0.0000)
   1 N      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.129980   100     0.1513

  Lengths:  14.000000  14.000000  15.129980
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1519

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:35:02         -1.56   -15.567251    1      
iter:   2  11:35:02         -1.87   -15.608181    1      
iter:   3  11:35:03         -2.85   -15.608574    1      
iter:   4  11:35:03         -3.54   -15.608612    1      
iter:   5  11:35:04         -4.79   -15.608612    1      
iter:   6  11:35:04         -5.71   -15.608612    1      
iter:   7  11:35:04         -6.51   -15.608612    1      

Occupied states converged after 8 e/g evaluations

Converged after 7 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2963.443008)

Kinetic:         +9.512894
Potential:      -13.487615
External:        +0.000000
XC:             -11.804786
Entropy (-ST):   +0.000000
Local:           +0.170895
--------------------------
Free energy:    -15.608612
Extrapolated:   -15.608612

 Band  Eigenvalues  Occupancy
    0    -27.66845    2.00000
    1    -13.27453    2.00000
    2    -11.05368    2.00000
    3    -11.05368    2.00000
    4     -9.65340    2.00000
    5     -1.83718    0.00000
    6     -1.83718    0.00000
    7      9.26666    0.00000
    8     11.74945    0.00000
    9     11.74945    0.00000
   10     12.44544    0.00000
   11     15.20645    0.00000
   12     15.20645    0.00000
   13     17.52975    0.00000
   14     19.29174    0.00000
   15     29.53891    0.00000
   16     29.54310    0.00000
   17     33.91432    0.00000
   18     33.91432    0.00000
   19     36.91123    0.00000
   20     41.81809    0.00000
   21     41.82204    0.00000
   22     53.42419    0.00000
   23     56.43688    0.00000
   24     56.43688    0.00000
   25     58.44074    0.00000

Fermi level: -5.74529

Gap: 7.816 eV
Transition (v -> c):
  (s=0, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.002     0.002   0.0% |
LCAO WFS Initialize:                  0.395     0.014   0.3% |
 Hamiltonian:                         0.381     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.016     0.016   0.4% |
  Hartree integrate/restrict:         0.009     0.009   0.2% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.195     0.005   0.1% |
   Communicate from 1D:               0.042     0.042   0.9% |
   Communicate from 2D:               0.040     0.040   0.9% |
   Communicate to 1D:                 0.039     0.039   0.9% |
   Communicate to 2D:                 0.042     0.042   0.9% |
   FFT 1D:                            0.008     0.008   0.2% |
   FFT 2D:                            0.019     0.019   0.4% |
  XC 3D grid:                         0.158     0.158   3.5% ||
  vbar:                               0.002     0.002   0.1% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            3.472     0.002   0.0% |
 Density:                             0.016     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.013     0.013   0.3% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.002     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.001     0.001   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            3.082     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.016     0.001   0.0% |
   Construct Gradient Matrix:         0.001     0.001   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.004     0.004   0.1% |
   Potential matrix:                  0.011     0.011   0.2% |
  Density:                            0.123     0.000   0.0% |
   Atomic density matrices:           0.005     0.005   0.1% |
   Mix:                               0.103     0.103   2.3% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.001     0.001   0.0% |
   Pseudo density:                    0.013     0.003   0.1% |
    Calculate density matrix:         0.000     0.000   0.0% |
    Construct density:                0.010     0.010   0.2% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.001     0.001   0.0% |
  Hamiltonian:                        2.939     0.001   0.0% |
   Atomic:                            0.002     0.002   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.002     0.002   0.1% |
   Communicate:                       0.129     0.129   2.9% ||
   Hartree integrate/restrict:        0.074     0.074   1.7% ||
   New Kinetic Energy:                0.001     0.000   0.0% |
    Pseudo part:                      0.000     0.000   0.0% |
   Poisson:                           1.546     0.034   0.8% |
    Communicate from 1D:              0.333     0.333   7.4% |--|
    Communicate from 2D:              0.319     0.319   7.1% |--|
    Communicate to 1D:                0.314     0.314   7.0% |--|
    Communicate to 2D:                0.336     0.336   7.5% |--|
    FFT 1D:                           0.064     0.064   1.4% ||
    FFT 2D:                           0.146     0.146   3.3% ||
   XC 3D grid:                        1.166     1.166  26.1% |---------|
   vbar:                              0.019     0.019   0.4% |
  Preconditioning::                   0.000     0.000   0.0% |
  Unitary rotation:                   0.002     0.000   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.002     0.002   0.0% |
 Get canonical representation:        0.002     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Potential matrix:                   0.001     0.001   0.0% |
 Hamiltonian:                         0.368     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.016     0.016   0.4% |
  Hartree integrate/restrict:         0.009     0.009   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.193     0.004   0.1% |
   Communicate from 1D:               0.042     0.042   0.9% |
   Communicate from 2D:               0.040     0.040   0.9% |
   Communicate to 1D:                 0.039     0.039   0.9% |
   Communicate to 2D:                 0.042     0.042   0.9% |
   FFT 1D:                            0.008     0.008   0.2% |
   FFT 2D:                            0.018     0.018   0.4% |
  XC 3D grid:                         0.146     0.146   3.3% ||
  vbar:                               0.002     0.002   0.1% |
 LCAO eigensolver:                    0.002     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Orbital Layouts:                    0.000     0.000   0.0% |
  Potential matrix:                   0.001     0.001   0.0% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.036     0.036   0.8% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.567     0.567  12.7% |----|
------------------------------------------------------------
Total:                                          4.473 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:35:04 2021
