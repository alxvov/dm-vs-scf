
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:35:34 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3132.844995

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*116*108 grid
  Fine grid: 208*232*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*232*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 55.00 MiB
    Density: 32.17 MiB
      Arrays: 31.36 MiB
      Localized functions: 0.81 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.58 MiB
      Arrays: 20.51 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 2.25 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.17 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 59
Number of bands in calculation: 59
Number of valence electrons: 16
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .--------------------------------------.  
           /|                                      |  
          / |                                      |  
         /  |                                      |  
        /   |                                      |  
       /    |                                      |  
      /     |                                      |  
     /      |                                      |  
    /       |                                      |  
   /        |                                      |  
  /         |                                      |  
 *          |                                      |  
 |          |                                      |  
 |          |                                      |  
 |          |           H   H                      |  
 |          |             C                        |  
 |          |             CH                       |  
 |          |             C                        |  
 |          |            H                         |  
 |          |                                      |  
 |          .--------------------------------------.  
 |         /                                      /   
 |        /                                      /    
 |       /                                      /     
 |      /                                      /      
 |     /                                      /       
 |    /                                      /        
 |   /                                      /         
 |  /                                      /          
 | /                                      /           
 |/                                      /            
 *--------------------------------------*             

Positions:
   0 C      7.912438    8.584098    8.896768    ( 0.0000,  0.0000,  0.0000)
   1 C      7.912438    7.933553    7.539667    ( 0.0000,  0.0000,  0.0000)
   2 C      7.912438    9.234643    7.539667    ( 0.0000,  0.0000,  0.0000)
   3 H      8.824876    8.584098    9.494856    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    8.584098    9.494856    ( 0.0000,  0.0000,  0.0000)
   5 H      7.912438    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      7.912438   10.168196    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.824876    0.000000    0.000000   104     0.1522
  2. axis:    no     0.000000   17.168196    0.000000   116     0.1480
  3. axis:    no     0.000000    0.000000   16.494856   108     0.1527

  Lengths:  15.824876  17.168196  16.494856
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:35:38         -1.01   -37.616739    1      
iter:   2  11:35:39         -1.91   -37.677524    1      
iter:   3  11:35:40         -2.27   -37.682716    1      
iter:   4  11:35:40         -2.77   -37.684262    1      
iter:   5  11:35:41         -3.67   -37.684287    1      
iter:   6  11:35:42         -4.31   -37.684292    1      
iter:   7  11:35:42         -4.70   -37.684292    1      
iter:   8  11:35:43         -4.93   -37.684292    1      
iter:   9  11:35:44         -5.25   -37.684292    1      
iter:  10  11:35:44         -5.87   -37.684292    1      
iter:  11  11:35:45         -6.27   -37.684292    1      

Occupied states converged after 14 e/g evaluations

Converged after 11 iterations.

Dipole moment: (-0.000000, -0.000000, -0.102918) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3132.844995)

Kinetic:        +29.682980
Potential:      -31.982826
External:        +0.000000
XC:             -35.470307
Entropy (-ST):   +0.000000
Local:           +0.085860
--------------------------
Free energy:    -37.684292
Extrapolated:   -37.684292

 Band  Eigenvalues  Occupancy
    0    -21.69676    2.00000
    1    -14.64173    2.00000
    2    -13.84209    2.00000
    3    -12.27667    2.00000
    4    -10.54876    2.00000
    5     -8.28722    2.00000
    6     -7.07821    2.00000
    7     -5.95455    2.00000
    8     -0.72535    0.00000
    9      2.45694    0.00000
   10      2.66918    0.00000
   11      3.03279    0.00000
   12      3.81741    0.00000
   13      4.19678    0.00000
   14      5.59391    0.00000
   15      7.95131    0.00000
   16      8.14130    0.00000
   17      9.53589    0.00000
   18     10.12172    0.00000
   19     10.16408    0.00000
   20     11.51330    0.00000
   21     11.52044    0.00000
   22     12.02618    0.00000
   23     14.65955    0.00000
   24     15.52146    0.00000
   25     17.02911    0.00000
   26     18.40485    0.00000
   27     18.93052    0.00000
   28     19.07967    0.00000
   29     20.84563    0.00000
   30     21.16276    0.00000
   31     22.33801    0.00000
   32     23.44427    0.00000
   33     27.37254    0.00000
   34     27.55545    0.00000
   35     27.66902    0.00000
   36     28.31230    0.00000
   37     32.31847    0.00000
   38     32.88169    0.00000
   39     33.11117    0.00000
   40     33.71053    0.00000
   41     35.83376    0.00000
   42     35.90795    0.00000
   43     36.45324    0.00000
   44     36.77171    0.00000
   45     38.84400    0.00000
   46     40.83269    0.00000
   47     43.58441    0.00000
   48     44.04445    0.00000
   49     44.17188    0.00000
   50     44.51383    0.00000
   51     46.96959    0.00000
   52     50.63783    0.00000
   53     50.70244    0.00000
   54     53.90351    0.00000
   55     54.77682    0.00000
   56     57.47404    0.00000
   57     64.94516    0.00000
   58     75.75987    0.00000

Fermi level: -3.33995

Gap: 5.229 eV
Transition (v -> c):
  (s=0, k=0, n=7, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.005     0.005   0.0% |
LCAO WFS Initialize:                  0.622     0.022   0.2% |
 Hamiltonian:                         0.600     0.000   0.0% |
  Atomic:                             0.016     0.016   0.1% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.004     0.004   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.017     0.017   0.2% |
  Initialize Hamiltonian:             0.001     0.001   0.0% |
  Poisson:                            0.312     0.009   0.1% |
   Communicate from 1D:               0.063     0.063   0.6% |
   Communicate from 2D:               0.061     0.061   0.5% |
   Communicate to 1D:                 0.064     0.064   0.6% |
   Communicate to 2D:                 0.067     0.067   0.6% |
   FFT 1D:                            0.015     0.015   0.1% |
   FFT 2D:                            0.032     0.032   0.3% |
  XC 3D grid:                         0.245     0.245   2.2% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.004     0.004   0.0% |
SCF-cycle:                            9.604     0.002   0.0% |
 Density:                             0.031     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.020     0.020   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.009     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.009     0.009   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            8.951     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.167     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.002     0.002   0.0% |
   Distribute overlap matrix:         0.020     0.020   0.2% |
   Potential matrix:                  0.141     0.141   1.3% ||
  Density:                            0.462     0.000   0.0% |
   Atomic density matrices:           0.019     0.019   0.2% |
   Mix:                               0.306     0.306   2.7% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.134     0.008   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.124     0.124   1.1% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        8.313     0.001   0.0% |
   Atomic:                            0.230     0.229   2.0% ||
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.044     0.044   0.4% |
   Communicate:                       0.001     0.001   0.0% |
   Hartree integrate/restrict:        0.257     0.257   2.3% ||
   New Kinetic Energy:                0.001     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.344     0.097   0.9% |
    Communicate from 1D:              0.897     0.897   8.0% |--|
    Communicate from 2D:              0.853     0.853   7.6% |--|
    Communicate to 1D:                0.879     0.879   7.8% |--|
    Communicate to 2D:                0.951     0.951   8.5% |--|
    FFT 1D:                           0.215     0.215   1.9% ||
    FFT 2D:                           0.452     0.452   4.0% |-|
   XC 3D grid:                        3.384     3.384  30.2% |-----------|
   vbar:                              0.051     0.051   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.005     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.012     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.010     0.010   0.1% |
 Hamiltonian:                         0.595     0.000   0.0% |
  Atomic:                             0.016     0.016   0.1% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.019     0.019   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.314     0.007   0.1% |
   Communicate from 1D:               0.064     0.064   0.6% |
   Communicate from 2D:               0.061     0.061   0.5% |
   Communicate to 1D:                 0.066     0.066   0.6% |
   Communicate to 2D:                 0.068     0.068   0.6% |
   FFT 1D:                            0.016     0.016   0.1% |
   FFT 2D:                            0.032     0.032   0.3% |
  XC 3D grid:                         0.242     0.242   2.2% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.012     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.010     0.010   0.1% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.074     0.074   0.7% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.900     0.900   8.0% |--|
------------------------------------------------------------
Total:                                         11.211 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:35:45 2021
