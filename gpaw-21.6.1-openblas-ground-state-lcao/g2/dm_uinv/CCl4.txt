
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:24:24 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -51247.425698

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*108*108 grid
  Fine grid: 216*216*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 576.68 MiB
  Calculator: 54.09 MiB
    Density: 31.91 MiB
      Arrays: 30.31 MiB
      Localized functions: 1.60 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.95 MiB
      Arrays: 19.83 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 2.22 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.13 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 65
Number of bands in calculation: 65
Number of valence electrons: 32
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                 Cl                   |  
 |         |          Cl                          |  
 |         |             C                        |  
 |         |            Cl                        |  
 |         |               Cl                     |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      8.021340    8.021340    8.021340    ( 0.0000,  0.0000,  0.0000)
   1 Cl     9.042680    9.042680    9.042680    ( 0.0000,  0.0000,  0.0000)
   2 Cl     7.000000    7.000000    9.042680    ( 0.0000,  0.0000,  0.0000)
   3 Cl     7.000000    9.042680    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 Cl     9.042680    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.042680    0.000000    0.000000   108     0.1485
  2. axis:    no     0.000000   16.042680    0.000000   108     0.1485
  3. axis:    no     0.000000    0.000000   16.042680   108     0.1485

  Lengths:  16.042680  16.042680  16.042680
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1485

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:24:28         -1.33   -15.247322    1      
iter:   2  11:24:29         -2.09   -15.266384    1      
iter:   3  11:24:29         -2.50   -15.275735    1      
iter:   4  11:24:30         -4.00   -15.275831    1      
iter:   5  11:24:30         -4.07   -15.275852    1      
iter:   6  11:24:31         -4.51   -15.275853    1      
iter:   7  11:24:32         -4.71   -15.275852    1      
iter:   8  11:24:32         -4.82   -15.275853    1      
iter:   9  11:24:33         -5.32   -15.275853    1      
iter:  10  11:24:34         -6.10   -15.275853    1      

Occupied states converged after 12 e/g evaluations

Converged after 10 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -51247.425698)

Kinetic:        +35.298495
Potential:      -29.936234
External:        +0.000000
XC:             -20.696450
Entropy (-ST):   +0.000000
Local:           +0.058336
--------------------------
Free energy:    -15.275853
Extrapolated:   -15.275853

 Band  Eigenvalues  Occupancy
    0    -24.27035    2.00000
    1    -20.97338    2.00000
    2    -20.97338    2.00000
    3    -20.97338    2.00000
    4    -15.23975    2.00000
    5    -12.13449    2.00000
    6    -12.13449    2.00000
    7    -12.13449    2.00000
    8     -8.65025    2.00000
    9     -8.65025    2.00000
   10     -8.01548    2.00000
   11     -8.01548    2.00000
   12     -8.01548    2.00000
   13     -7.07668    2.00000
   14     -7.07668    2.00000
   15     -7.07668    2.00000
   16     -1.92295    0.00000
   17     -0.49796    0.00000
   18     -0.49796    0.00000
   19     -0.49796    0.00000
   20      7.91910    0.00000
   21      7.91910    0.00000
   22      7.91910    0.00000
   23     10.45888    0.00000
   24     10.45888    0.00000
   25     11.59310    0.00000
   26     11.59310    0.00000
   27     11.59310    0.00000
   28     12.43171    0.00000
   29     12.70520    0.00000
   30     12.70520    0.00000
   31     12.70520    0.00000
   32     12.84352    0.00000
   33     12.84352    0.00000
   34     13.43465    0.00000
   35     13.43465    0.00000
   36     13.43465    0.00000
   37     14.67651    0.00000
   38     14.67651    0.00000
   39     14.67651    0.00000
   40     15.06903    0.00000
   41     15.06903    0.00000
   42     15.06903    0.00000
   43     15.49522    0.00000
   44     16.30758    0.00000
   45     16.30758    0.00000
   46     18.49755    0.00000
   47     18.49755    0.00000
   48     18.49755    0.00000
   49     19.83661    0.00000
   50     19.83661    0.00000
   51     19.83661    0.00000
   52     21.83286    0.00000
   53     26.05937    0.00000
   54     26.05937    0.00000
   55     26.05937    0.00000
   56     31.96450    0.00000
   57     31.96450    0.00000
   58     31.96450    0.00000
   59     34.43214    0.00000
   60     34.43214    0.00000
   61     34.67164    0.00000
   62     39.65930    0.00000
   63     39.65930    0.00000
   64     39.65930    0.00000

Fermi level: -4.49982

Gap: 5.154 eV
Transition (v -> c):
  (s=0, k=0, n=15, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=16, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.004     0.004   0.0% |
LCAO WFS Initialize:                  0.604     0.022   0.2% |
 Hamiltonian:                         0.582     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.022     0.022   0.2% |
  Hartree integrate/restrict:         0.014     0.014   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.302     0.009   0.1% |
   Communicate from 1D:               0.063     0.063   0.7% |
   Communicate from 2D:               0.059     0.059   0.6% |
   Communicate to 1D:                 0.064     0.064   0.7% |
   Communicate to 2D:                 0.064     0.064   0.7% |
   FFT 1D:                            0.014     0.014   0.1% |
   FFT 2D:                            0.030     0.030   0.3% |
  XC 3D grid:                         0.238     0.238   2.5% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            8.006     0.002   0.0% |
 Density:                             0.028     0.000   0.0% |
  Atomic density matrices:            0.002     0.002   0.0% |
  Mix:                                0.019     0.019   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.007     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.007     0.007   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            7.383     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.110     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.016     0.016   0.2% |
   Potential matrix:                  0.090     0.090   0.9% |
  Density:                            0.369     0.000   0.0% |
   Atomic density matrices:           0.024     0.024   0.2% |
   Mix:                               0.257     0.257   2.7% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.085     0.006   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.078     0.078   0.8% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.001     0.001   0.0% |
  Hamiltonian:                        6.896     0.001   0.0% |
   Atomic:                            0.002     0.002   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.007     0.007   0.1% |
   Communicate:                       0.270     0.270   2.8% ||
   Hartree integrate/restrict:        0.178     0.178   1.8% ||
   New Kinetic Energy:                0.001     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.599     0.080   0.8% |
    Communicate from 1D:              0.752     0.752   7.8% |--|
    Communicate from 2D:              0.716     0.716   7.4% |--|
    Communicate to 1D:                0.759     0.759   7.8% |--|
    Communicate to 2D:                0.773     0.773   8.0% |--|
    FFT 1D:                           0.166     0.166   1.7% ||
    FFT 2D:                           0.354     0.354   3.7% ||
   XC 3D grid:                        2.796     2.796  28.9% |-----------|
   vbar:                              0.042     0.042   0.4% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.005     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.010     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.007     0.007   0.1% |
 Hamiltonian:                         0.573     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.022     0.022   0.2% |
  Hartree integrate/restrict:         0.015     0.015   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.300     0.006   0.1% |
   Communicate from 1D:               0.062     0.062   0.6% |
   Communicate from 2D:               0.060     0.060   0.6% |
   Communicate to 1D:                 0.064     0.064   0.7% |
   Communicate to 2D:                 0.064     0.064   0.7% |
   FFT 1D:                            0.014     0.014   0.1% |
   FFT 2D:                            0.029     0.029   0.3% |
  XC 3D grid:                         0.231     0.231   2.4% ||
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.010     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.007     0.007   0.1% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.135     0.135   1.4% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.920     0.920   9.5% |---|
------------------------------------------------------------
Total:                                          9.671 100.0%

Memory usage: 576.68 MiB
Date: Fri Aug  6 11:24:34 2021
