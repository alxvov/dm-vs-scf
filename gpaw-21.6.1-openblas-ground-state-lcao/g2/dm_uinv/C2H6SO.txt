
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:32:28 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16.0
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/S.PBE.gz
  compensation charges: gauss, rc=0.24, lmax=2
  cutoffs: 1.77(filt), 1.66(core),
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -15028.669403

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 124*116*104 grid
  Fine grid: 248*232*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 248*232*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 1, 0]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 64.50 MiB
    Density: 37.72 MiB
      Arrays: 36.05 MiB
      Localized functions: 1.67 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 23.71 MiB
      Arrays: 23.58 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.13 MiB
    Wavefunctions: 3.07 MiB
      C [qnM]: 0.05 MiB
      S, T [2 x qmm]: 0.10 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.91 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 82
Number of bands in calculation: 82
Number of valence electrons: 26
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .---------------------------------------------.  
           /|                                             |  
          / |                                             |  
         /  |                                             |  
        /   |                                             |  
       /    |                                             |  
      /     |                                             |  
     /      |                                             |  
    /       |                                             |  
   /        |                                             |  
  /         |                                             |  
 *          |                                             |  
 |          |                                             |  
 |          |                                             |  
 |          |             H    OH                         |  
 |          |                                             |  
 |          |           H C   S C  H                      |  
 |          |            H      H                         |  
 |          |                                             |  
 |          .---------------------------------------------.  
 |         /                                             /   
 |        /                                             /    
 |       /                                             /     
 |      /                                             /      
 |     /                                             /       
 |    /                                             /        
 |   /                                             /         
 |  /                                             /          
 | /                                             /           
 |/                                             /            
 *---------------------------------------------*             

Positions:
   0 S      9.279406    9.025165    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 O      9.279424   10.294069    7.818462    ( 0.0000,  0.0000,  0.0000)
   2 C     10.618932    7.984305    7.619360    ( 0.0000,  0.0000,  0.0000)
   3 C      7.939856    7.984335    7.619361    ( 0.0000,  0.0000,  0.0000)
   4 H     10.535239    7.896942    8.705468    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    8.479403    7.369969    ( 0.0000,  0.0000,  0.0000)
   6 H     10.583811    7.000000    7.146054    ( 0.0000,  0.0000,  0.0000)
   7 H     11.558799    8.479353    7.369969    ( 0.0000,  0.0000,  0.0000)
   8 H      7.974957    7.000029    7.146056    ( 0.0000,  0.0000,  0.0000)
   9 H      8.023547    7.896972    8.705469    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.558799    0.000000    0.000000   124     0.1497
  2. axis:    no     0.000000   17.294069    0.000000   116     0.1491
  3. axis:    no     0.000000    0.000000   15.705469   104     0.1510

  Lengths:  18.558799  17.294069  15.705469
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1499

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:32:33         -0.71   -47.853662    1      
iter:   2  11:32:34         -1.62   -47.989654    1      
iter:   3  11:32:35         -2.18   -48.022371    1      
iter:   4  11:32:35         -2.87   -48.025453    1      
iter:   5  11:32:36         -3.07   -48.026063    1      
iter:   6  11:32:37         -3.37   -48.026183    1      
iter:   7  11:32:37         -3.73   -48.026216    1      
iter:   8  11:32:38         -3.93   -48.026221    1      
iter:   9  11:32:38         -4.30   -48.026223    1      
iter:  10  11:32:39         -4.82   -48.026223    1      
iter:  11  11:32:40         -5.27   -48.026223    1      
iter:  12  11:32:40         -5.34   -48.026223    1      
iter:  13  11:32:41         -5.61   -48.026223    1      
iter:  14  11:32:42         -6.33   -48.026223    1      

Occupied states converged after 17 e/g evaluations

Converged after 14 iterations.

Dipole moment: (-0.000010, -0.746488, -0.125533) |e|*Ang

Energy contributions relative to reference atoms: (reference = -15028.669403)

Kinetic:        +43.040757
Potential:      -44.006136
External:        +0.000000
XC:             -46.987927
Entropy (-ST):   +0.000000
Local:           -0.072918
--------------------------
Free energy:    -48.026223
Extrapolated:   -48.026223

 Band  Eigenvalues  Occupancy
    0    -24.82586    2.00000
    1    -19.60487    2.00000
    2    -17.80569    2.00000
    3    -14.74101    2.00000
    4    -11.36791    2.00000
    5    -11.15270    2.00000
    6    -11.13325    2.00000
    7    -10.25363    2.00000
    8     -9.47384    2.00000
    9     -9.39889    2.00000
   10     -8.15350    2.00000
   11     -5.92035    2.00000
   12     -4.84157    2.00000
   13      0.62668    0.00000
   14      0.62825    0.00000
   15      1.39741    0.00000
   16      1.90426    0.00000
   17      2.82969    0.00000
   18      3.05151    0.00000
   19      3.08969    0.00000
   20      3.86452    0.00000
   21      4.23375    0.00000
   22      6.89648    0.00000
   23      7.44401    0.00000
   24      8.38473    0.00000
   25      8.67536    0.00000
   26     10.15023    0.00000
   27     10.40821    0.00000
   28     11.43494    0.00000
   29     11.72275    0.00000
   30     12.39989    0.00000
   31     12.64079    0.00000
   32     13.77739    0.00000
   33     14.01811    0.00000
   34     14.28344    0.00000
   35     14.95438    0.00000
   36     15.15923    0.00000
   37     15.42827    0.00000
   38     16.03462    0.00000
   39     16.70411    0.00000
   40     17.52053    0.00000
   41     20.05402    0.00000
   42     20.30921    0.00000
   43     21.37090    0.00000
   44     21.83344    0.00000
   45     22.10961    0.00000
   46     22.64304    0.00000
   47     23.85357    0.00000
   48     24.28556    0.00000
   49     25.22733    0.00000
   50     26.93336    0.00000
   51     27.39736    0.00000
   52     27.97381    0.00000
   53     31.27549    0.00000
   54     34.22007    0.00000
   55     34.25404    0.00000
   56     34.36050    0.00000
   57     35.00262    0.00000
   58     35.30927    0.00000
   59     35.41566    0.00000
   60     36.15289    0.00000
   61     37.27316    0.00000
   62     38.06605    0.00000
   63     38.62633    0.00000
   64     40.04950    0.00000
   65     40.85059    0.00000
   66     43.79895    0.00000
   67     43.96610    0.00000
   68     44.24710    0.00000
   69     44.36858    0.00000
   70     45.34613    0.00000
   71     48.54662    0.00000
   72     49.36841    0.00000
   73     50.47304    0.00000
   74     50.55753    0.00000
   75     51.01117    0.00000
   76     51.31363    0.00000
   77     52.07037    0.00000
   78     53.05982    0.00000
   79     54.98738    0.00000
   80     56.43151    0.00000
   81     66.58282    0.00000

Fermi level: -2.10745

Gap: 5.468 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.007     0.007   0.1% |
LCAO WFS Initialize:                  0.600     0.024   0.2% |
 Hamiltonian:                         0.575     0.000   0.0% |
  Atomic:                             0.020     0.000   0.0% |
   XC Correction:                     0.020     0.020   0.1% |
  Calculate atomic Hamiltonians:      0.003     0.003   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.017     0.017   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.247     0.010   0.1% |
   Communicate from 1D:               0.020     0.020   0.1% |
   Communicate from 2D:               0.069     0.069   0.5% |
   Communicate to 1D:                 0.065     0.065   0.5% |
   Communicate to 2D:                 0.021     0.021   0.2% |
   FFT 1D:                            0.023     0.023   0.2% |
   FFT 2D:                            0.039     0.039   0.3% |
  XC 3D grid:                         0.282     0.282   2.1% ||
  vbar:                               0.005     0.005   0.0% |
P tci:                                0.007     0.007   0.0% |
SCF-cycle:                           11.551     0.003   0.0% |
 Density:                             0.041     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.022     0.022   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.017     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.016     0.016   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           10.894     0.002   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.337     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.002     0.002   0.0% |
   Distribute overlap matrix:         0.014     0.014   0.1% |
   Potential matrix:                  0.315     0.315   2.3% ||
  Density:                            0.737     0.000   0.0% |
   Atomic density matrices:           0.025     0.025   0.2% |
   Mix:                               0.413     0.413   3.0% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.003     0.003   0.0% |
   Pseudo density:                    0.294     0.012   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.281     0.281   2.1% ||
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        9.806     0.001   0.0% |
   Atomic:                            0.353     0.017   0.1% |
    XC Correction:                    0.336     0.336   2.5% ||
   Calculate atomic Hamiltonians:     0.065     0.065   0.5% |
   Communicate:                       0.000     0.000   0.0% |
   Hartree integrate/restrict:        0.287     0.287   2.1% ||
   New Kinetic Energy:                0.002     0.002   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.258     0.145   1.1% |
    Communicate from 1D:              0.327     0.327   2.4% ||
    Communicate from 2D:              1.177     1.177   8.6% |--|
    Communicate to 1D:                1.145     1.145   8.4% |--|
    Communicate to 2D:                0.396     0.396   2.9% ||
    FFT 1D:                           0.401     0.401   2.9% ||
    FFT 2D:                           0.666     0.666   4.9% |-|
   XC 3D grid:                        4.767     4.767  34.8% |-------------|
   vbar:                              0.073     0.073   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.008     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.001     0.001   0.0% |
   Eigendecomposition:                0.005     0.005   0.0% |
 Get canonical representation:        0.021     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.018     0.018   0.1% |
 Hamiltonian:                         0.572     0.000   0.0% |
  Atomic:                             0.020     0.000   0.0% |
   XC Correction:                     0.020     0.020   0.1% |
  Calculate atomic Hamiltonians:      0.004     0.004   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.017     0.017   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.248     0.009   0.1% |
   Communicate from 1D:               0.019     0.019   0.1% |
   Communicate from 2D:               0.069     0.069   0.5% |
   Communicate to 1D:                 0.066     0.066   0.5% |
   Communicate to 2D:                 0.023     0.023   0.2% |
   FFT 1D:                            0.023     0.023   0.2% |
   FFT 2D:                            0.039     0.039   0.3% |
  XC 3D grid:                         0.279     0.279   2.0% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.021     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.018     0.018   0.1% |
ST tci:                               0.002     0.002   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.407     0.407   3.0% ||
mktci:                                0.002     0.002   0.0% |
Other:                                1.116     1.116   8.1% |--|
------------------------------------------------------------
Total:                                         13.691 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:32:42 2021
