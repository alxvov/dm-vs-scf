
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:32:54 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6480.770010

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*108*104 grid
  Fine grid: 208*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 48.47 MiB
    Density: 28.76 MiB
      Arrays: 28.09 MiB
      Localized functions: 0.67 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 18.43 MiB
      Arrays: 18.37 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.29 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.23 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 49
Number of bands in calculation: 49
Number of valence electrons: 20
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |           H  C H                     |  
 |         |              F                       |  
 |         |             F                        |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.908369    8.109716    7.793504    ( 0.0000,  0.0000,  0.0000)
   1 F      7.908369    9.219432    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 F      7.908369    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.109716    8.397300    ( 0.0000,  0.0000,  0.0000)
   4 H      8.816738    8.109716    8.397300    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.816738    0.000000    0.000000   104     0.1521
  2. axis:    no     0.000000   16.219432    0.000000   108     0.1502
  3. axis:    no     0.000000    0.000000   15.397300   104     0.1481

  Lengths:  15.816738  16.219432  15.397300
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1501

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:32:58         -1.52   -22.934575    1      
iter:   2  11:32:59         -2.28   -22.969167    1      
iter:   3  11:33:00         -3.04   -22.970087    1      
iter:   4  11:33:00         -3.69   -22.970174    1      
iter:   5  11:33:01         -4.16   -22.970182    1      
iter:   6  11:33:01         -4.52   -22.970183    1      
iter:   7  11:33:02         -4.89   -22.970183    1      
iter:   8  11:33:03         -5.15   -22.970183    1      
iter:   9  11:33:03         -5.65   -22.970183    1      
iter:  10  11:33:04         -6.44   -22.970183    1      

Occupied states converged after 13 e/g evaluations

Converged after 10 iterations.

Dipole moment: (0.000000, -0.000000, 0.324462) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6480.770010)

Kinetic:        +27.728210
Potential:      -26.943257
External:        +0.000000
XC:             -23.977734
Entropy (-ST):   +0.000000
Local:           +0.222599
--------------------------
Free energy:    -22.970183
Extrapolated:   -22.970183

 Band  Eigenvalues  Occupancy
    0    -31.03287    2.00000
    1    -29.54498    2.00000
    2    -17.29483    2.00000
    3    -13.07903    2.00000
    4    -12.99627    2.00000
    5    -12.94384    2.00000
    6     -9.39251    2.00000
    7     -9.37689    2.00000
    8     -8.96368    2.00000
    9     -7.70207    2.00000
   10      2.08812    0.00000
   11      2.38647    0.00000
   12      3.18985    0.00000
   13      3.26027    0.00000
   14     10.19768    0.00000
   15     10.48751    0.00000
   16     10.81955    0.00000
   17     11.69561    0.00000
   18     14.81659    0.00000
   19     17.37137    0.00000
   20     18.33385    0.00000
   21     20.11155    0.00000
   22     20.12387    0.00000
   23     22.38836    0.00000
   24     23.94672    0.00000
   25     24.69221    0.00000
   26     24.99083    0.00000
   27     27.35264    0.00000
   28     31.03653    0.00000
   29     32.91068    0.00000
   30     35.42603    0.00000
   31     35.46939    0.00000
   32     37.02422    0.00000
   33     39.23778    0.00000
   34     41.07536    0.00000
   35     48.46563    0.00000
   36     48.51792    0.00000
   37     49.51456    0.00000
   38     54.94642    0.00000
   39     58.24576    0.00000
   40     58.98063    0.00000
   41     59.74542    0.00000
   42     62.75635    0.00000
   43     62.83343    0.00000
   44     70.76892    0.00000
   45     72.81594    0.00000
   46     74.51379    0.00000
   47     74.74354    0.00000
   48     76.87663    0.00000

Fermi level: -2.80697

Gap: 9.790 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.003     0.003   0.0% |
LCAO WFS Initialize:                  0.556     0.018   0.2% |
 Hamiltonian:                         0.538     0.000   0.0% |
  Atomic:                             0.016     0.016   0.2% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.001     0.001   0.0% |
  Hartree integrate/restrict:         0.013     0.013   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.282     0.008   0.1% |
   Communicate from 1D:               0.059     0.059   0.6% |
   Communicate from 2D:               0.054     0.054   0.6% |
   Communicate to 1D:                 0.058     0.058   0.6% |
   Communicate to 2D:                 0.060     0.060   0.6% |
   FFT 1D:                            0.013     0.013   0.1% |
   FFT 2D:                            0.030     0.030   0.3% |
  XC 3D grid:                         0.221     0.221   2.4% ||
  vbar:                               0.003     0.003   0.0% |
P tci:                                0.002     0.002   0.0% |
SCF-cycle:                            7.789     0.002   0.0% |
 Density:                             0.023     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.017     0.017   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.004     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.004     0.004   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            7.241     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.070     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.001     0.001   0.0% |
   Distribute overlap matrix:         0.008     0.008   0.1% |
   Potential matrix:                  0.058     0.058   0.6% |
  Density:                            0.326     0.000   0.0% |
   Atomic density matrices:           0.013     0.013   0.1% |
   Mix:                               0.251     0.251   2.7% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.059     0.007   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.052     0.052   0.6% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.001     0.001   0.0% |
  Hamiltonian:                        6.836     0.001   0.0% |
   Atomic:                            0.204     0.204   2.2% ||
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.020     0.020   0.2% |
   Communicate:                       0.018     0.018   0.2% |
   Hartree integrate/restrict:        0.164     0.164   1.7% ||
   New Kinetic Energy:                0.001     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.587     0.088   0.9% |
    Communicate from 1D:              0.751     0.751   8.0% |--|
    Communicate from 2D:              0.708     0.708   7.5% |--|
    Communicate to 1D:                0.721     0.721   7.7% |--|
    Communicate to 2D:                0.756     0.756   8.0% |--|
    FFT 1D:                           0.163     0.163   1.7% ||
    FFT 2D:                           0.399     0.399   4.2% |-|
   XC 3D grid:                        2.797     2.797  29.8% |-----------|
   vbar:                              0.043     0.043   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.004     0.000   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.003     0.003   0.0% |
 Get canonical representation:        0.006     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.004     0.004   0.0% |
 Hamiltonian:                         0.511     0.000   0.0% |
  Atomic:                             0.016     0.016   0.2% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.001     0.001   0.0% |
  Hartree integrate/restrict:         0.013     0.013   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.269     0.006   0.1% |
   Communicate from 1D:               0.057     0.057   0.6% |
   Communicate from 2D:               0.054     0.054   0.6% |
   Communicate to 1D:                 0.053     0.053   0.6% |
   Communicate to 2D:                 0.057     0.057   0.6% |
   FFT 1D:                            0.013     0.013   0.1% |
   FFT 2D:                            0.031     0.031   0.3% |
  XC 3D grid:                         0.207     0.207   2.2% ||
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.006     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.004     0.004   0.0% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.202     0.202   2.1% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.846     0.846   9.0% |---|
------------------------------------------------------------
Total:                                          9.401 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:33:04 2021
