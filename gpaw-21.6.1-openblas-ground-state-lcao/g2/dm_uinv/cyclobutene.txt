
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:16:39 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4185.453435

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*112*112 grid
  Fine grid: 208*224*224 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*224*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 56.37 MiB
    Density: 32.54 MiB
      Arrays: 31.40 MiB
      Localized functions: 1.14 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.63 MiB
      Arrays: 20.54 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 3.20 MiB
      C [qnM]: 0.05 MiB
      S, T [2 x qmm]: 0.10 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.05 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 82
Number of bands in calculation: 82
Number of valence electrons: 22
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |              H                       |  
 |         |              C                       |  
 |         |             H                        |  
 |         |            H C H                     |  
 |         |             C                        |  
 |         |           H   H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.889310    7.749631    8.953808    ( 0.0000,  0.0000,  0.0000)
   1 C      7.889310    9.095155    8.953808    ( 0.0000,  0.0000,  0.0000)
   2 C      7.889310    7.640413    7.445943    ( 0.0000,  0.0000,  0.0000)
   3 C      7.889310    9.204373    7.445943    ( 0.0000,  0.0000,  0.0000)
   4 H      7.889310    7.000000    9.740354    ( 0.0000,  0.0000,  0.0000)
   5 H      7.889310    9.844786    9.740354    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.183151    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H      8.778620    7.183151    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      8.778620    9.661635    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    9.661635    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.778620    0.000000    0.000000   104     0.1517
  2. axis:    no     0.000000   16.844786    0.000000   112     0.1504
  3. axis:    no     0.000000    0.000000   16.740354   112     0.1495

  Lengths:  15.778620  16.844786  16.740354
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1505

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:16:43         -0.98   -54.986605    1      
iter:   2  11:16:44         -1.62   -55.038612    1      
iter:   3  11:16:44         -1.99   -55.092795    1      
iter:   4  11:16:45         -2.79   -55.093188    1      
iter:   5  11:16:46         -3.14   -55.093559    1      
iter:   6  11:16:46         -4.18   -55.093566    1      
iter:   7  11:16:47         -4.54   -55.093568    1      
iter:   8  11:16:48         -4.87   -55.093568    1      
iter:   9  11:16:48         -5.18   -55.093568    1      
iter:  10  11:16:49         -5.77   -55.093568    1      
iter:  11  11:16:50         -6.22   -55.093568    1      

Occupied states converged after 13 e/g evaluations

Converged after 11 iterations.

Dipole moment: (-0.000000, -0.000000, -0.034092) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4185.453435)

Kinetic:        +44.336320
Potential:      -48.823124
External:        +0.000000
XC:             -50.716775
Entropy (-ST):   +0.000000
Local:           +0.110011
--------------------------
Free energy:    -55.093568
Extrapolated:   -55.093568

 Band  Eigenvalues  Occupancy
    0    -21.27480    2.00000
    1    -16.05423    2.00000
    2    -15.77850    2.00000
    3    -12.50517    2.00000
    4    -12.15468    2.00000
    5    -11.12956    2.00000
    6     -9.13856    2.00000
    7     -8.59402    2.00000
    8     -7.80200    2.00000
    9     -7.54346    2.00000
   10     -5.89523    2.00000
   11     -0.36837    0.00000
   12      2.21030    0.00000
   13      2.85114    0.00000
   14      2.95228    0.00000
   15      3.00131    0.00000
   16      3.64515    0.00000
   17      4.20606    0.00000
   18      4.70713    0.00000
   19      5.18879    0.00000
   20      7.36629    0.00000
   21      8.58754    0.00000
   22      8.85517    0.00000
   23      9.14347    0.00000
   24      9.41305    0.00000
   25     10.19493    0.00000
   26     10.52533    0.00000
   27     11.60560    0.00000
   28     12.27040    0.00000
   29     12.65677    0.00000
   30     13.63097    0.00000
   31     14.08283    0.00000
   32     14.90095    0.00000
   33     15.97378    0.00000
   34     15.99904    0.00000
   35     17.07984    0.00000
   36     17.38752    0.00000
   37     17.85909    0.00000
   38     18.71257    0.00000
   39     19.11061    0.00000
   40     20.83623    0.00000
   41     21.67122    0.00000
   42     23.49629    0.00000
   43     23.54265    0.00000
   44     24.89552    0.00000
   45     28.20777    0.00000
   46     28.33707    0.00000
   47     28.35123    0.00000
   48     29.65128    0.00000
   49     29.83540    0.00000
   50     30.99435    0.00000
   51     31.43918    0.00000
   52     32.63056    0.00000
   53     34.94641    0.00000
   54     35.01580    0.00000
   55     35.06533    0.00000
   56     36.04207    0.00000
   57     37.49027    0.00000
   58     38.17995    0.00000
   59     38.29809    0.00000
   60     38.65049    0.00000
   61     38.79216    0.00000
   62     39.35846    0.00000
   63     41.24466    0.00000
   64     41.30095    0.00000
   65     41.90375    0.00000
   66     42.58180    0.00000
   67     42.81941    0.00000
   68     45.77925    0.00000
   69     46.36082    0.00000
   70     47.41055    0.00000
   71     48.04125    0.00000
   72     48.92930    0.00000
   73     50.83031    0.00000
   74     51.65513    0.00000
   75     52.71056    0.00000
   76     54.84490    0.00000
   77     58.55207    0.00000
   78     59.32635    0.00000
   79     61.46420    0.00000
   80     62.36576    0.00000
   81     76.58280    0.00000

Fermi level: -3.13180

Gap: 5.527 eV
Transition (v -> c):
  (s=0, k=0, n=10, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=11, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.007     0.007   0.1% |
LCAO WFS Initialize:                  0.619     0.020   0.2% |
 Hamiltonian:                         0.599     0.000   0.0% |
  Atomic:                             0.020     0.004   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.015     0.015   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.313     0.009   0.1% |
   Communicate from 1D:               0.064     0.064   0.6% |
   Communicate from 2D:               0.061     0.061   0.6% |
   Communicate to 1D:                 0.064     0.064   0.6% |
   Communicate to 2D:                 0.067     0.067   0.6% |
   FFT 1D:                            0.015     0.015   0.1% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.246     0.246   2.3% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.002     0.002   0.0% |
SCF-cycle:                            9.211     0.002   0.0% |
 Density:                             0.039     0.000   0.0% |
  Atomic density matrices:            0.003     0.003   0.0% |
  Mix:                                0.018     0.018   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.017     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.017     0.017   0.2% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            8.528     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.289     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.001     0.001   0.0% |
   Distribute overlap matrix:         0.033     0.033   0.3% |
   Potential matrix:                  0.250     0.250   2.3% ||
  Density:                            0.518     0.000   0.0% |
   Atomic density matrices:           0.040     0.040   0.4% |
   Mix:                               0.249     0.249   2.3% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.226     0.008   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.217     0.217   2.0% ||
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        7.712     0.001   0.0% |
   Atomic:                            0.256     0.053   0.5% |
    XC Correction:                    0.203     0.203   1.9% ||
   Calculate atomic Hamiltonians:     0.020     0.020   0.2% |
   Communicate:                       0.004     0.004   0.0% |
   Hartree integrate/restrict:        0.183     0.183   1.7% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.055     0.093   0.9% |
    Communicate from 1D:              0.835     0.835   7.7% |--|
    Communicate from 2D:              0.798     0.798   7.4% |--|
    Communicate to 1D:                0.837     0.837   7.7% |--|
    Communicate to 2D:                0.871     0.871   8.0% |--|
    FFT 1D:                           0.196     0.196   1.8% ||
    FFT 2D:                           0.425     0.425   3.9% |-|
   XC 3D grid:                        3.143     3.143  29.0% |-----------|
   vbar:                              0.048     0.048   0.4% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.005     0.001   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.023     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.003     0.003   0.0% |
  Potential matrix:                   0.019     0.019   0.2% |
 Hamiltonian:                         0.596     0.000   0.0% |
  Atomic:                             0.020     0.004   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.015     0.015   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.314     0.007   0.1% |
   Communicate from 1D:               0.066     0.066   0.6% |
   Communicate from 2D:               0.061     0.061   0.6% |
   Communicate to 1D:                 0.065     0.065   0.6% |
   Communicate to 2D:                 0.067     0.067   0.6% |
   FFT 1D:                            0.015     0.015   0.1% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.242     0.242   2.2% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.023     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.003     0.003   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.019     0.019   0.2% |
ST tci:                               0.002     0.002   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.074     0.074   0.7% |
mktci:                                0.002     0.002   0.0% |
Other:                                0.907     0.907   8.4% |--|
------------------------------------------------------------
Total:                                         10.824 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:16:50 2021
