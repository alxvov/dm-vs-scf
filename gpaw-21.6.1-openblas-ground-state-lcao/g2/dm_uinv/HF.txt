
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:29:37 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2726.570947

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*100 grid
  Fine grid: 184*184*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 34.18 MiB
    Density: 20.52 MiB
      Arrays: 20.28 MiB
      Localized functions: 0.24 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 13.29 MiB
      Arrays: 13.27 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.02 MiB
    Wavefunctions: 0.38 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.00 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.37 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 18
Number of bands in calculation: 18
Number of valence electrons: 8
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            F                    |  
 |        |            H                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 F      7.000000    7.000000    7.933891    ( 0.0000,  0.0000,  0.0000)
   1 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   14.933891   100     0.1493

  Lengths:  14.000000  14.000000  14.933891
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1512

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:29:40         -1.14    -6.997294    1      
iter:   2  11:29:40         -2.21    -7.000781    1      
iter:   3  11:29:41         -2.62    -7.002934    1      
iter:   4  11:29:41         -3.99    -7.002946    1      
iter:   5  11:29:41         -4.43    -7.002947    1      
iter:   6  11:29:42         -5.11    -7.002947    1      
iter:   7  11:29:42         -5.20    -7.002947    1      
iter:   8  11:29:43         -5.58    -7.002947    1      
iter:   9  11:29:43         -7.03    -7.002947    1      

Occupied states converged after 11 e/g evaluations

Converged after 9 iterations.

Dipole moment: (-0.000000, -0.000000, -0.351313) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2726.570947)

Kinetic:        +11.396903
Potential:      -10.467082
External:        +0.000000
XC:              -8.035153
Entropy (-ST):   +0.000000
Local:           +0.102386
--------------------------
Free energy:     -7.002947
Extrapolated:    -7.002947

 Band  Eigenvalues  Occupancy
    0    -29.16788    2.00000
    1    -12.55604    2.00000
    2     -8.64773    2.00000
    3     -8.64773    2.00000
    4      1.03192    0.00000
    5     11.79325    0.00000
    6     19.75028    0.00000
    7     19.75028    0.00000
    8     21.81873    0.00000
    9     25.17190    0.00000
   10     25.17190    0.00000
   11     39.34725    0.00000
   12     47.46273    0.00000
   13     58.95193    0.00000
   14     58.95271    0.00000
   15     67.30431    0.00000
   16     67.30431    0.00000
   17     83.50862    0.00000

Fermi level: -3.80790

Gap: 9.680 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.001     0.001   0.0% |
LCAO WFS Initialize:                  0.395     0.014   0.2% |
 Hamiltonian:                         0.381     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.016     0.016   0.3% |
  Hartree integrate/restrict:         0.009     0.009   0.2% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.195     0.005   0.1% |
   Communicate from 1D:               0.042     0.042   0.7% |
   Communicate from 2D:               0.040     0.040   0.7% |
   Communicate to 1D:                 0.039     0.039   0.7% |
   Communicate to 2D:                 0.042     0.042   0.7% |
   FFT 1D:                            0.008     0.008   0.1% |
   FFT 2D:                            0.019     0.019   0.3% |
  XC 3D grid:                         0.158     0.158   2.8% ||
  vbar:                               0.002     0.002   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            4.611     0.002   0.0% |
 Density:                             0.015     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.013     0.013   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.001     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.001     0.001   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            4.224     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.013     0.001   0.0% |
   Construct Gradient Matrix:         0.001     0.001   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.004     0.004   0.1% |
   Potential matrix:                  0.006     0.006   0.1% |
  Density:                            0.162     0.000   0.0% |
   Atomic density matrices:           0.008     0.008   0.1% |
   Mix:                               0.142     0.142   2.5% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.001     0.001   0.0% |
   Pseudo density:                    0.010     0.003   0.1% |
    Calculate density matrix:         0.000     0.000   0.0% |
    Construct density:                0.006     0.006   0.1% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.001     0.001   0.0% |
  Hamiltonian:                        4.044     0.001   0.0% |
   Atomic:                            0.002     0.002   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.003     0.003   0.1% |
   Communicate:                       0.172     0.172   3.0% ||
   Hartree integrate/restrict:        0.101     0.101   1.8% ||
   New Kinetic Energy:                0.001     0.000   0.0% |
    Pseudo part:                      0.000     0.000   0.0% |
   Poisson:                           2.134     0.047   0.8% |
    Communicate from 1D:              0.459     0.459   8.1% |--|
    Communicate from 2D:              0.441     0.441   7.8% |--|
    Communicate to 1D:                0.432     0.432   7.6% |--|
    Communicate to 2D:                0.463     0.463   8.2% |--|
    FFT 1D:                           0.087     0.087   1.5% ||
    FFT 2D:                           0.204     0.204   3.6% ||
   XC 3D grid:                        1.603     1.603  28.3% |----------|
   vbar:                              0.026     0.026   0.5% |
  Preconditioning::                   0.000     0.000   0.0% |
  Unitary rotation:                   0.003     0.000   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.003     0.003   0.0% |
 Get canonical representation:        0.001     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Potential matrix:                   0.001     0.001   0.0% |
 Hamiltonian:                         0.368     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.016     0.016   0.3% |
  Hartree integrate/restrict:         0.009     0.009   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.194     0.004   0.1% |
   Communicate from 1D:               0.042     0.042   0.7% |
   Communicate from 2D:               0.040     0.040   0.7% |
   Communicate to 1D:                 0.039     0.039   0.7% |
   Communicate to 2D:                 0.042     0.042   0.7% |
   FFT 1D:                            0.008     0.008   0.1% |
   FFT 2D:                            0.019     0.019   0.3% |
  XC 3D grid:                         0.146     0.146   2.6% ||
  vbar:                               0.002     0.002   0.0% |
 LCAO eigensolver:                    0.001     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Orbital Layouts:                    0.000     0.000   0.0% |
  Potential matrix:                   0.001     0.001   0.0% |
ST tci:                               0.000     0.000   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.075     0.075   1.3% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.583     0.583  10.3% |---|
------------------------------------------------------------
Total:                                          5.667 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:29:43 2021
