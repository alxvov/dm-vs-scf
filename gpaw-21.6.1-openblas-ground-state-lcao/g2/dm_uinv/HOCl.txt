
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:10:39 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -14608.291859

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 100*108*92 grid
  Fine grid: 200*216*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 200*216*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 41.01 MiB
    Density: 24.49 MiB
      Arrays: 23.85 MiB
      Localized functions: 0.64 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 15.65 MiB
      Arrays: 15.60 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 0.87 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.84 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 31
Number of bands in calculation: 31
Number of valence electrons: 14
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .------------------------------------.  
          /|                                    |  
         / |                                    |  
        /  |                                    |  
       /   |                                    |  
      /    |                                    |  
     /     |                                    |  
    /      |                                    |  
   /       |                                    |  
  /        |                                    |  
 *         |                                    |  
 |         |                                    |  
 |         |                                    |  
 |         |            H O                     |  
 |         |             Cl                     |  
 |         |                                    |  
 |         |                                    |  
 |         .------------------------------------.  
 |        /                                    /   
 |       /                                    /    
 |      /                                    /     
 |     /                                    /      
 |    /                                    /       
 |   /                                    /        
 |  /                                    /         
 | /                                    /          
 |/                                    /           
 *------------------------------------*            

Positions:
   0 O      7.954250    8.715694    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 H      7.000000    8.931056    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 Cl     7.954250    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.954250    0.000000    0.000000   100     0.1495
  2. axis:    no     0.000000   15.931056    0.000000   108     0.1475
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  14.954250  15.931056  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1497

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:10:42         -0.94    -9.710556    1      
iter:   2  11:10:43         -2.01    -9.759989    1      
iter:   3  11:10:43         -2.57    -9.763432    1      
iter:   4  11:10:44         -3.37    -9.763646    1      
iter:   5  11:10:44         -3.60    -9.763710    1      
iter:   6  11:10:45         -3.87    -9.763718    1      
iter:   7  11:10:45         -4.03    -9.763720    1      
iter:   8  11:10:46         -4.32    -9.763721    1      
iter:   9  11:10:46         -4.77    -9.763721    1      
iter:  10  11:10:47         -5.41    -9.763721    1      
iter:  11  11:10:47         -5.76    -9.763721    1      
iter:  12  11:10:49                  -9.763721    1      

Occupied states converged after 19 e/g evaluations

Converged after 12 iterations.

Dipole moment: (-0.304630, 0.029861, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -14608.291859)

Kinetic:        +15.703641
Potential:      -13.615372
External:        +0.000000
XC:             -11.926986
Entropy (-ST):   +0.000000
Local:           +0.074995
--------------------------
Free energy:     -9.763721
Extrapolated:    -9.763721

 Band  Eigenvalues  Occupancy
    0    -26.28592    2.00000
    1    -19.54522    2.00000
    2    -12.73670    2.00000
    3    -10.40837    2.00000
    4     -9.16879    2.00000
    5     -7.08154    2.00000
    6     -6.03336    2.00000
    7     -2.70650    0.00000
    8      1.51789    0.00000
    9     10.68897    0.00000
   10     12.44482    0.00000
   11     13.36294    0.00000
   12     13.45814    0.00000
   13     14.10796    0.00000
   14     14.19131    0.00000
   15     15.11267    0.00000
   16     15.19088    0.00000
   17     16.05428    0.00000
   18     18.68472    0.00000
   19     19.43177    0.00000
   20     19.89019    0.00000
   21     24.36722    0.00000
   22     26.61712    0.00000
   23     28.15722    0.00000
   24     38.19699    0.00000
   25     42.54111    0.00000
   26     48.06863    0.00000
   27     54.59311    0.00000
   28     55.04991    0.00000
   29     58.12266    0.00000
   30     69.20145    0.00000

Fermi level: -4.36993

Gap: 3.327 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.002     0.002   0.0% |
LCAO WFS Initialize:                  0.464     0.016   0.2% |
 Hamiltonian:                         0.448     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.020     0.020   0.2% |
  Hartree integrate/restrict:         0.011     0.011   0.1% |
  Initialize Hamiltonian:             0.001     0.001   0.0% |
  Poisson:                            0.228     0.005   0.0% |
   Communicate from 1D:               0.048     0.048   0.5% |
   Communicate from 2D:               0.046     0.046   0.4% |
   Communicate to 1D:                 0.045     0.045   0.4% |
   Communicate to 2D:                 0.048     0.048   0.5% |
   FFT 1D:                            0.014     0.014   0.1% |
   FFT 2D:                            0.022     0.022   0.2% |
  XC 3D grid:                         0.184     0.184   1.7% ||
  vbar:                               0.003     0.003   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            9.193     0.002   0.0% |
 Density:                             0.019     0.000   0.0% |
  Atomic density matrices:            0.002     0.002   0.0% |
  Mix:                                0.015     0.015   0.1% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.002     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.001     0.001   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            8.729     0.002   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.064     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.001     0.001   0.0% |
   Distribute overlap matrix:         0.029     0.029   0.3% |
   Potential matrix:                  0.031     0.031   0.3% |
  Density:                            0.361     0.000   0.0% |
   Atomic density matrices:           0.034     0.034   0.3% |
   Mix:                               0.287     0.287   2.7% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.037     0.008   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.028     0.028   0.3% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        8.293     0.002   0.0% |
   Atomic:                            0.004     0.004   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.007     0.007   0.1% |
   Communicate:                       0.382     0.382   3.6% ||
   Hartree integrate/restrict:        0.207     0.207   2.0% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.352     0.096   0.9% |
    Communicate from 1D:              0.920     0.920   8.7% |--|
    Communicate from 2D:              0.888     0.888   8.4% |--|
    Communicate to 1D:                0.861     0.861   8.1% |--|
    Communicate to 2D:                0.913     0.913   8.6% |--|
    FFT 1D:                           0.257     0.257   2.4% ||
    FFT 2D:                           0.418     0.418   3.9% |-|
   XC 3D grid:                        3.284     3.284  31.0% |-----------|
   vbar:                              0.054     0.054   0.5% |
  Preconditioning::                   0.000     0.000   0.0% |
  Unitary rotation:                   0.006     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.005     0.005   0.0% |
 Get canonical representation:        0.004     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.002     0.002   0.0% |
 Hamiltonian:                         0.435     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.020     0.020   0.2% |
  Hartree integrate/restrict:         0.011     0.011   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.228     0.005   0.0% |
   Communicate from 1D:               0.048     0.048   0.5% |
   Communicate from 2D:               0.046     0.046   0.4% |
   Communicate to 1D:                 0.045     0.045   0.4% |
   Communicate to 2D:                 0.048     0.048   0.5% |
   FFT 1D:                            0.014     0.014   0.1% |
   FFT 2D:                            0.022     0.022   0.2% |
  XC 3D grid:                         0.173     0.173   1.6% ||
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.004     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.000     0.000   0.0% |
  Potential matrix:                   0.002     0.002   0.0% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.199     0.199   1.9% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.718     0.718   6.8% |--|
------------------------------------------------------------
Total:                                         10.578 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:10:49 2021
