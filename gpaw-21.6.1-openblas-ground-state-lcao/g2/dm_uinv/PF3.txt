
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:16:06 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

P-setup:
  name: Phosphorus
  id: 03b4a34d18bb161274a4ee27145ba70a
  Z: 15.0
  valence: 5
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/P.PBE.gz
  compensation charges: gauss, rc=0.30, lmax=2
  cutoffs: 1.69(filt), 1.81(core),
  valence states:
                energy  radius
    3s(2.00)   -13.968   0.953
    3p(3.00)    -5.506   0.953
    *s          13.244   0.953
    *p          21.705   0.953
    *d           0.000   0.953

  LCAO basis set for P:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/P.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.0938 Bohr: 3s-sz confined orbital
      l=1, rc=7.7031 Bohr: 3p-sz confined orbital
      l=0, rc=3.4375 Bohr: 3s-dz split-valence wave
      l=1, rc=4.4688 Bohr: 3p-dz split-valence wave
      l=2, rc=7.7031 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -17445.156014

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*108*100 grid
  Fine grid: 216*216*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 48.97 MiB
    Density: 29.18 MiB
      Arrays: 28.04 MiB
      Localized functions: 1.13 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 18.43 MiB
      Arrays: 18.35 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 1.36 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.30 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 52
Number of valence electrons: 26
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |              PF                       |  
 |         |          F     F                      |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 P      8.198459    7.691931    7.788304    ( 0.0000,  0.0000,  0.0000)
   1 F      8.198459    9.075792    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 F      9.396918    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.396918    0.000000    0.000000   108     0.1518
  2. axis:    no     0.000000   16.075792    0.000000   108     0.1488
  3. axis:    no     0.000000    0.000000   14.788304   100     0.1479

  Lengths:  16.396918  16.075792  14.788304
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1495

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:16:10         -1.14   -17.573532    1      
iter:   2  11:16:11         -2.18   -17.618363    1      
iter:   3  11:16:11         -3.04   -17.619800    1      
iter:   4  11:16:12         -3.57   -17.620012    1      
iter:   5  11:16:12         -4.13   -17.620039    1      
iter:   6  11:16:13         -4.58   -17.620041    1      
iter:   7  11:16:14         -4.99   -17.620041    1      
iter:   8  11:16:15         -5.30   -17.620041    1      
iter:   9  11:16:15         -5.59   -17.620041    1      
iter:  10  11:16:16         -6.68   -17.620041    1      

Occupied states converged after 14 e/g evaluations

Converged after 10 iterations.

Dipole moment: (0.000000, -0.000021, 0.202312) |e|*Ang

Energy contributions relative to reference atoms: (reference = -17445.156014)

Kinetic:        +30.953897
Potential:      -27.143462
External:        +0.000000
XC:             -21.581129
Entropy (-ST):   +0.000000
Local:           +0.150654
--------------------------
Free energy:    -17.620041
Extrapolated:   -17.620041

 Band  Eigenvalues  Occupancy
    0    -31.54060    2.00000
    1    -30.19251    2.00000
    2    -30.19213    2.00000
    3    -16.27356    2.00000
    4    -13.16020    2.00000
    5    -13.16016    2.00000
    6    -12.17037    2.00000
    7    -11.07910    2.00000
    8    -11.07903    2.00000
    9    -10.08408    2.00000
   10    -10.08382    2.00000
   11     -9.72739    2.00000
   12     -7.31940    2.00000
   13     -0.64134    0.00000
   14     -0.64119    0.00000
   15      2.35523    0.00000
   16      8.22468    0.00000
   17      8.94799    0.00000
   18      8.94809    0.00000
   19     11.25127    0.00000
   20     11.25140    0.00000
   21     11.71330    0.00000
   22     12.66366    0.00000
   23     17.35523    0.00000
   24     17.35594    0.00000
   25     19.76056    0.00000
   26     20.09212    0.00000
   27     20.09218    0.00000
   28     21.79950    0.00000
   29     21.79979    0.00000
   30     22.39816    0.00000
   31     23.40536    0.00000
   32     25.49766    0.00000
   33     25.49782    0.00000
   34     46.02422    0.00000
   35     46.02453    0.00000
   36     53.54988    0.00000
   37     56.15864    0.00000
   38     56.94307    0.00000
   39     57.00676    0.00000
   40     57.00732    0.00000
   41     57.61225    0.00000
   42     57.61227    0.00000
   43     58.01423    0.00000
   44     58.11604    0.00000
   45     58.11622    0.00000
   46     64.56982    0.00000
   47     64.57072    0.00000
   48     65.82795    0.00000
   49     71.88845    0.00000
   50     74.99906    0.00000
   51     74.99996    0.00000

Fermi level: -3.98037

Gap: 6.678 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.003     0.003   0.0% |
LCAO WFS Initialize:                  0.563     0.019   0.2% |
 Hamiltonian:                         0.543     0.000   0.0% |
  Atomic:                             0.016     0.016   0.2% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.005     0.005   0.1% |
  Hartree integrate/restrict:         0.014     0.014   0.1% |
  Initialize Hamiltonian:             0.001     0.001   0.0% |
  Poisson:                            0.282     0.007   0.1% |
   Communicate from 1D:               0.060     0.060   0.6% |
   Communicate from 2D:               0.054     0.054   0.5% |
   Communicate to 1D:                 0.058     0.058   0.6% |
   Communicate to 2D:                 0.060     0.060   0.6% |
   FFT 1D:                            0.017     0.017   0.2% |
   FFT 2D:                            0.026     0.026   0.3% |
  XC 3D grid:                         0.220     0.220   2.2% ||
  vbar:                               0.003     0.003   0.0% |
P tci:                                0.003     0.003   0.0% |
SCF-cycle:                            8.560     0.002   0.0% |
 Density:                             0.024     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.018     0.018   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.005     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.004     0.004   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            7.988     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.071     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.001     0.001   0.0% |
   Distribute overlap matrix:         0.005     0.005   0.0% |
   Potential matrix:                  0.061     0.061   0.6% |
  Density:                            0.357     0.000   0.0% |
   Atomic density matrices:           0.009     0.009   0.1% |
   Mix:                               0.270     0.270   2.7% ||
   Multipole moments:                 0.012     0.012   0.1% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.064     0.007   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.056     0.056   0.6% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.001     0.001   0.0% |
  Hamiltonian:                        7.550     0.001   0.0% |
   Atomic:                            0.230     0.230   2.3% ||
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.013     0.013   0.1% |
   Communicate:                       0.077     0.077   0.8% |
   Hartree integrate/restrict:        0.190     0.190   1.9% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.961     0.086   0.9% |
    Communicate from 1D:              0.832     0.832   8.2% |--|
    Communicate from 2D:              0.771     0.771   7.6% |--|
    Communicate to 1D:                0.819     0.819   8.1% |--|
    Communicate to 2D:                0.842     0.842   8.3% |--|
    FFT 1D:                           0.239     0.239   2.4% ||
    FFT 2D:                           0.372     0.372   3.7% ||
   XC 3D grid:                        3.030     3.030  30.0% |-----------|
   vbar:                              0.047     0.047   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.005     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.005     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Potential matrix:                   0.004     0.004   0.0% |
 Hamiltonian:                         0.536     0.000   0.0% |
  Atomic:                             0.016     0.016   0.2% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.005     0.005   0.1% |
  Hartree integrate/restrict:         0.013     0.013   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.282     0.006   0.1% |
   Communicate from 1D:               0.059     0.059   0.6% |
   Communicate from 2D:               0.054     0.054   0.5% |
   Communicate to 1D:                 0.058     0.058   0.6% |
   Communicate to 2D:                 0.060     0.060   0.6% |
   FFT 1D:                            0.017     0.017   0.2% |
   FFT 2D:                            0.026     0.026   0.3% |
  XC 3D grid:                         0.215     0.215   2.1% ||
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.006     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.004     0.004   0.0% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.135     0.135   1.3% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.832     0.832   8.2% |--|
------------------------------------------------------------
Total:                                         10.098 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:16:16 2021
