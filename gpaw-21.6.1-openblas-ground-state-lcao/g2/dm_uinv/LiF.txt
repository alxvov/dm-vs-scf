
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:13:44 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Li-setup:
  name: Lithium
  id: 830b4218f175192f93f959cfc0aad614
  Z: 3.0
  valence: 1
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Li.PBE.gz
  compensation charges: gauss, rc=0.33, lmax=2
  cutoffs: 1.91(filt), 2.55(core),
  valence states:
                energy  radius
    2s(1.00)    -2.874   1.058
    2p(0.00)    -1.090   1.058
    *s          24.337   1.058

  LCAO basis set for Li:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Li.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=10.8906 Bohr: 2s-sz confined orbital
      l=0, rc=6.6719 Bohr: 2s-dz split-valence wave
      l=1, rc=10.8906 Bohr: p-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -2916.868605

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 37.07 MiB
    Density: 22.07 MiB
      Arrays: 21.10 MiB
      Localized functions: 0.98 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 13.88 MiB
      Arrays: 13.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 1.12 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.00 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.11 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 18
Number of bands in calculation: 18
Number of valence electrons: 8
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            F                    |  
 |        |                                 |  
 |        |            Li                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Li     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 F      7.000000    7.000000    8.566620    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.566620   104     0.1497

  Lengths:  14.000000  14.000000  15.566620
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1513

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:13:46         -0.81    -5.751943    1      
iter:   2  11:13:47         -1.86    -5.815481    1      
iter:   3  11:13:47         -2.73    -5.817542    1      
iter:   4  11:13:48         -3.40    -5.817766    1      
iter:   5  11:13:48         -4.05    -5.817793    1      
iter:   6  11:13:48         -4.27    -5.817801    1      
iter:   7  11:13:49         -4.16    -5.817805    1      
iter:   8  11:13:49         -4.18    -5.817805    1      
iter:   9  11:13:50         -4.46    -5.817805    1      
iter:  10  11:13:50         -5.34    -5.817805    1      
iter:  11  11:13:50         -6.00    -5.817805    1      
iter:  12  11:13:51         -6.56    -5.817805    1      

Occupied states converged after 14 e/g evaluations

Converged after 12 iterations.

Dipole moment: (-0.000000, -0.000000, -1.210939) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2916.868605)

Kinetic:        +15.024847
Potential:      -10.957405
External:        +0.000000
XC:              -9.989710
Entropy (-ST):   +0.000000
Local:           +0.104463
--------------------------
Free energy:     -5.817805
Extrapolated:    -5.817805

 Band  Eigenvalues  Occupancy
    0    -23.30432    2.00000
    1     -5.12413    2.00000
    2     -4.79217    2.00000
    3     -4.79217    2.00000
    4     -0.88371    0.00000
    5      4.41150    0.00000
    6      4.41150    0.00000
    7      4.91208    0.00000
    8      9.37907    0.00000
    9     24.28911    0.00000
   10     24.28911    0.00000
   11     25.93416    0.00000
   12     47.34792    0.00000
   13     61.60735    0.00000
   14     62.58729    0.00000
   15     62.58908    0.00000
   16     62.92913    0.00000
   17     62.92913    0.00000

Fermi level: -2.83794

Gap: 3.908 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.003     0.003   0.0% |
LCAO WFS Initialize:                  0.410     0.013   0.2% |
 Hamiltonian:                         0.397     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.017     0.017   0.2% |
  Hartree integrate/restrict:         0.009     0.009   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.204     0.006   0.1% |
   Communicate from 1D:               0.042     0.042   0.6% |
   Communicate from 2D:               0.042     0.042   0.6% |
   Communicate to 1D:                 0.041     0.041   0.6% |
   Communicate to 2D:                 0.042     0.042   0.6% |
   FFT 1D:                            0.010     0.010   0.1% |
   FFT 2D:                            0.022     0.022   0.3% |
  XC 3D grid:                         0.164     0.164   2.3% ||
  vbar:                               0.003     0.003   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            6.017     0.002   0.0% |
 Density:                             0.015     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.013     0.013   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.002     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.001     0.001   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            5.607     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.029     0.001   0.0% |
   Construct Gradient Matrix:         0.001     0.001   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.007     0.007   0.1% |
   Potential matrix:                  0.019     0.019   0.3% |
  Density:                            0.217     0.000   0.0% |
   Atomic density matrices:           0.009     0.009   0.1% |
   Mix:                               0.180     0.180   2.5% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.024     0.005   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.019     0.019   0.3% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        5.354     0.001   0.0% |
   Atomic:                            0.003     0.003   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.007     0.007   0.1% |
   Communicate:                       0.224     0.224   3.2% ||
   Hartree integrate/restrict:        0.135     0.135   1.9% ||
   New Kinetic Energy:                0.001     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           2.830     0.062   0.9% |
    Communicate from 1D:              0.587     0.587   8.2% |--|
    Communicate from 2D:              0.580     0.580   8.1% |--|
    Communicate to 1D:                0.571     0.571   8.0% |--|
    Communicate to 2D:                0.584     0.584   8.2% |--|
    FFT 1D:                           0.143     0.143   2.0% ||
    FFT 2D:                           0.303     0.303   4.3% |-|
   XC 3D grid:                        2.117     2.117  29.8% |-----------|
   vbar:                              0.035     0.035   0.5% |
  Preconditioning::                   0.000     0.000   0.0% |
  Unitary rotation:                   0.004     0.000   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.003     0.003   0.0% |
 Get canonical representation:        0.002     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Potential matrix:                   0.001     0.001   0.0% |
 Hamiltonian:                         0.388     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.016     0.016   0.2% |
  Hartree integrate/restrict:         0.011     0.011   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.206     0.004   0.1% |
   Communicate from 1D:               0.043     0.043   0.6% |
   Communicate from 2D:               0.043     0.043   0.6% |
   Communicate to 1D:                 0.041     0.041   0.6% |
   Communicate to 2D:                 0.043     0.043   0.6% |
   FFT 1D:                            0.010     0.010   0.1% |
   FFT 2D:                            0.022     0.022   0.3% |
  XC 3D grid:                         0.152     0.152   2.1% ||
  vbar:                               0.002     0.002   0.0% |
 LCAO eigensolver:                    0.002     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Orbital Layouts:                    0.000     0.000   0.0% |
  Potential matrix:                   0.001     0.001   0.0% |
ST tci:                               0.000     0.000   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.074     0.074   1.0% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.608     0.608   8.5% |--|
------------------------------------------------------------
Total:                                          7.114 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:13:51 2021
