from ase.collections import g2
from gpaw import GPAW, LCAO, ConvergenceError
from ase.parallel import parprint
from gpaw.utilities.memory import maxrss
import time
from gpaw.mpi import world
from gpaw.directmin.lcao.directmin_lcao import DirectMinLCAO

xc = 'PBE'
mode = LCAO()

file2write = open('dm-g2-results.txt', 'w')

for name in g2.names:

    atoms = g2[name]
    if len(atoms) == 1:
        continue
    atoms.center(vacuum=7.0)
    calc = GPAW(xc=xc, h=0.15,
                convergence={'density': 1.0e-6, 'eigenstates': 1.0e10}, # ignore eigenstates convergence as it is done in scf
                basis='dzp',
                mode=mode,
                txt=name + '.txt',
                eigensolver=DirectMinLCAO(matrix_exp='egdecomp2',
                                          representation='u_invar'
                                          ),
                mixer={'backend': 'no-mixing'},
                nbands='nao',
                occupations={'name': 'fixed-uniform'},
                symmetry='off'
                )

    atoms.calc = calc

    try:
        t1 = time.time()
        e = atoms.get_potential_energy()
        t2 = time.time()
        steps = atoms.calc.get_number_of_iterations()
        iters = atoms.calc.wfs.eigensolver.eg_count
        memory = maxrss() / 1024.0 ** 2
        parprint(name +
                 "\t{}\t{}\t{}\t{}\t{:.3f}".format(steps, iters, e,
                                                   t2-t1, memory), file=file2write, flush=True)  # s,MB
    except ConvergenceError:
        parprint(name +
                 "\t{}\t{}\t{}\t{}\t{}".format(None, None, None, None, None), file=file2write, flush=True)
    calc = None
    atoms = None

file2write.close()
