
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:08:45 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4146.069181

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*112*104 grid
  Fine grid: 224*224*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 341.03 MiB
  Calculator: 54.86 MiB
    Density: 32.25 MiB
      Arrays: 31.40 MiB
      Localized functions: 0.85 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.61 MiB
      Arrays: 20.54 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 2.01 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.93 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 59
Number of bands in calculation: 59
Number of valence electrons: 18
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .----------------------------------------.  
           /|                                        |  
          / |                                        |  
         /  |                                        |  
        /   |                                        |  
       /    |                                        |  
      /     |                                        |  
     /      |                                        |  
    /       |                                        |  
   /        |                                        |  
  /         |                                        |  
 *          |                                        |  
 |          |                                        |  
 |          |                                        |  
 |          |          H   H                         |  
 |          |            C  C  O                     |  
 |          |          H  H                          |  
 |          |                                        |  
 |          |                                        |  
 |          .----------------------------------------.  
 |         /                                        /   
 |        /                                        /    
 |       /                                        /     
 |      /                                        /      
 |     /                                        /       
 |    /                                        /        
 |   /                                        /         
 |  /                                        /          
 | /                                        /           
 |/                                        /            
 *----------------------------------------*             

Positions:
   0 O      9.814376    8.995476    7.880946    ( 0.0000,  0.0000,  0.0000)
   1 C      8.596321    9.098369    7.880946    ( 0.0000,  0.0000,  0.0000)
   2 H      8.119080   10.099531    7.880946    ( 0.0000,  0.0000,  0.0000)
   3 C      7.648219    7.934098    7.880946    ( 0.0000,  0.0000,  0.0000)
   4 H      8.210375    7.000000    7.880946    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.981761    8.761892    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.981761    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.814376    0.000000    0.000000   112     0.1501
  2. axis:    no     0.000000   17.099531    0.000000   112     0.1527
  3. axis:    no     0.000000    0.000000   15.761892   104     0.1516

  Lengths:  16.814376  17.099531  15.761892
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1514

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:08:49         -0.83   -37.597018    1      
iter:   2  11:08:50         -1.50   -37.731125    1      
iter:   3  11:08:51         -2.21   -37.750099    1      
iter:   4  11:08:52         -2.80   -37.751758    1      
iter:   5  11:08:52         -3.23   -37.751989    1      
iter:   6  11:08:53         -3.56   -37.752033    1      
iter:   7  11:08:54         -3.81   -37.752044    1      
iter:   8  11:08:54         -4.08   -37.752048    1      
iter:   9  11:08:55         -4.61   -37.752049    1      
iter:  10  11:08:55         -5.05   -37.752049    1      
iter:  11  11:08:56         -5.45   -37.752049    1      
iter:  12  11:08:57         -5.70   -37.752049    1      
iter:  13  11:08:57         -5.90   -37.752049    1      
iter:  14  11:08:58         -6.36   -37.752049    1      

Occupied states converged after 17 e/g evaluations

Converged after 14 iterations.

Dipole moment: (-0.504264, -0.060489, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4146.069181)

Kinetic:        +30.870558
Potential:      -34.458917
External:        +0.000000
XC:             -34.277405
Entropy (-ST):   +0.000000
Local:           +0.113715
--------------------------
Free energy:    -37.752049
Extrapolated:   -37.752049

 Band  Eigenvalues  Occupancy
    0    -25.93073    2.00000
    1    -18.68687    2.00000
    2    -14.21461    2.00000
    3    -11.53435    2.00000
    4    -10.88448    2.00000
    5    -10.33665    2.00000
    6     -9.49896    2.00000
    7     -8.77921    2.00000
    8     -5.59630    2.00000
    9     -1.75769    0.00000
   10      2.00738    0.00000
   11      2.73161    0.00000
   12      3.48236    0.00000
   13      3.61911    0.00000
   14      5.82835    0.00000
   15      6.49698    0.00000
   16      9.11611    0.00000
   17      9.52719    0.00000
   18     10.47802    0.00000
   19     10.48696    0.00000
   20     12.17363    0.00000
   21     14.03290    0.00000
   22     14.33357    0.00000
   23     14.91410    0.00000
   24     15.29646    0.00000
   25     16.02332    0.00000
   26     17.39958    0.00000
   27     17.85892    0.00000
   28     19.02439    0.00000
   29     20.38247    0.00000
   30     22.35993    0.00000
   31     24.47684    0.00000
   32     25.57522    0.00000
   33     27.05004    0.00000
   34     27.43444    0.00000
   35     29.93616    0.00000
   36     30.42148    0.00000
   37     33.34395    0.00000
   38     33.92344    0.00000
   39     35.30166    0.00000
   40     35.80047    0.00000
   41     35.94016    0.00000
   42     37.16095    0.00000
   43     38.98403    0.00000
   44     39.73444    0.00000
   45     42.44319    0.00000
   46     43.81734    0.00000
   47     45.39970    0.00000
   48     47.39340    0.00000
   49     48.12536    0.00000
   50     49.59117    0.00000
   51     49.62705    0.00000
   52     50.79219    0.00000
   53     54.89404    0.00000
   54     55.17573    0.00000
   55     60.85172    0.00000
   56     61.49716    0.00000
   57     68.24303    0.00000
   58     70.30885    0.00000

Fermi level: -3.67699

Gap: 3.839 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.005     0.005   0.0% |
LCAO WFS Initialize:                  0.620     0.019   0.1% |
 Hamiltonian:                         0.601     0.000   0.0% |
  Atomic:                             0.017     0.017   0.1% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.003     0.003   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.017     0.017   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.314     0.009   0.1% |
   Communicate from 1D:               0.064     0.064   0.5% |
   Communicate from 2D:               0.060     0.060   0.5% |
   Communicate to 1D:                 0.061     0.061   0.5% |
   Communicate to 2D:                 0.068     0.068   0.5% |
   FFT 1D:                            0.020     0.020   0.2% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.246     0.246   1.8% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.005     0.005   0.0% |
SCF-cycle:                           11.517     0.003   0.0% |
 Density:                             0.028     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.018     0.018   0.1% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.009     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.008     0.008   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           10.869     0.002   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.175     0.003   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.003     0.003   0.0% |
   Distribute overlap matrix:         0.004     0.004   0.0% |
   Potential matrix:                  0.164     0.164   1.2% |
  Density:                            0.516     0.000   0.0% |
   Atomic density matrices:           0.028     0.028   0.2% |
   Mix:                               0.327     0.327   2.5% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.003     0.003   0.0% |
   Pseudo density:                    0.155     0.010   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.144     0.144   1.1% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                       10.168     0.001   0.0% |
   Atomic:                            0.297     0.297   2.2% ||
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.059     0.059   0.4% |
   Communicate:                       0.000     0.000   0.0% |
   Hartree integrate/restrict:        0.247     0.247   1.9% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           5.352     0.118   0.9% |
    Communicate from 1D:              1.091     1.091   8.2% |--|
    Communicate from 2D:              1.020     1.020   7.7% |--|
    Communicate to 1D:                1.073     1.073   8.1% |--|
    Communicate to 2D:                1.132     1.132   8.5% |--|
    FFT 1D:                           0.344     0.344   2.6% ||
    FFT 2D:                           0.575     0.575   4.3% |-|
   XC 3D grid:                        4.145     4.145  31.2% |-----------|
   vbar:                              0.063     0.063   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.007     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.001     0.001   0.0% |
   Eigendecomposition:                0.005     0.005   0.0% |
 Get canonical representation:        0.011     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Potential matrix:                   0.010     0.010   0.1% |
 Hamiltonian:                         0.595     0.000   0.0% |
  Atomic:                             0.017     0.017   0.1% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.003     0.003   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.014     0.014   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.314     0.007   0.0% |
   Communicate from 1D:               0.064     0.064   0.5% |
   Communicate from 2D:               0.061     0.061   0.5% |
   Communicate to 1D:                 0.064     0.064   0.5% |
   Communicate to 2D:                 0.065     0.065   0.5% |
   FFT 1D:                            0.020     0.020   0.1% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.243     0.243   1.8% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.011     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.010     0.010   0.1% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.202     0.202   1.5% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.946     0.946   7.1% |--|
------------------------------------------------------------
Total:                                         13.298 100.0%

Memory usage: 394.84 MiB
Date: Fri Aug  6 11:08:58 2021
