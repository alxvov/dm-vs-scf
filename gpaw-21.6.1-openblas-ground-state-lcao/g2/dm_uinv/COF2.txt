
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:26:46 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -8496.641987

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*108*108 grid
  Fine grid: 184*216*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 44.61 MiB
    Density: 26.53 MiB
      Arrays: 25.78 MiB
      Localized functions: 0.75 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 16.92 MiB
      Arrays: 16.86 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.16 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.10 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 52
Number of valence electrons: 24
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           O                     |  
 |         |           C                     |  
 |         |            F                    |  
 |         |          F                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 O      7.000000    8.069490    8.970263    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000    8.069490    7.783906    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000    9.138980    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.138980    0.000000   108     0.1494
  3. axis:    no     0.000000    0.000000   15.970263   108     0.1479

  Lengths:  14.000000  16.138980  15.970263
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1498

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:26:49         -0.99   -22.082682    1      
iter:   2  11:26:50         -2.14   -22.132008    1      
iter:   3  11:26:51         -2.62   -22.139485    1      
iter:   4  11:26:51         -3.00   -22.141159    1      
iter:   5  11:26:52         -3.43   -22.141235    1      
iter:   6  11:26:52         -3.80   -22.141281    1      
iter:   7  11:26:53         -4.47   -22.141285    1      
iter:   8  11:26:53         -4.69   -22.141286    1      
iter:   9  11:26:54         -4.99   -22.141286    1      
iter:  10  11:26:54         -5.32   -22.141286    1      
iter:  11  11:26:55         -5.74   -22.141286    1      
iter:  12  11:26:55         -6.06   -22.141286    1      

Occupied states converged after 15 e/g evaluations

Converged after 12 iterations.

Dipole moment: (0.000000, 0.000000, -0.214942) |e|*Ang

Energy contributions relative to reference atoms: (reference = -8496.641987)

Kinetic:        +24.450276
Potential:      -23.106294
External:        +0.000000
XC:             -23.752095
Entropy (-ST):   +0.000000
Local:           +0.266827
--------------------------
Free energy:    -22.141286
Extrapolated:   -22.141286

 Band  Eigenvalues  Occupancy
    0    -33.21784    2.00000
    1    -31.35826    2.00000
    2    -27.55076    2.00000
    3    -16.83394    2.00000
    4    -14.97664    2.00000
    5    -13.69812    2.00000
    6    -12.81815    2.00000
    7    -11.02198    2.00000
    8    -10.95955    2.00000
    9    -10.45790    2.00000
   10     -9.37206    2.00000
   11     -8.03165    2.00000
   12     -1.51588    0.00000
   13      1.68889    0.00000
   14      5.31682    0.00000
   15      9.17279    0.00000
   16      9.40575    0.00000
   17     10.39528    0.00000
   18     11.42106    0.00000
   19     14.57110    0.00000
   20     16.93686    0.00000
   21     18.27284    0.00000
   22     19.06707    0.00000
   23     19.73157    0.00000
   24     20.03611    0.00000
   25     20.74405    0.00000
   26     22.23324    0.00000
   27     24.35771    0.00000
   28     24.47463    0.00000
   29     25.51523    0.00000
   30     27.57528    0.00000
   31     29.07589    0.00000
   32     38.37265    0.00000
   33     38.90877    0.00000
   34     45.40561    0.00000
   35     46.84256    0.00000
   36     49.02918    0.00000
   37     50.13765    0.00000
   38     54.22333    0.00000
   39     54.46967    0.00000
   40     56.00635    0.00000
   41     56.87349    0.00000
   42     59.46137    0.00000
   43     59.82893    0.00000
   44     60.10580    0.00000
   45     67.39215    0.00000
   46     69.52324    0.00000
   47     70.79339    0.00000
   48     71.65075    0.00000
   49     75.79558    0.00000
   50     77.96027    0.00000
   51     82.71072    0.00000

Fermi level: -4.77376

Gap: 6.516 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.003     0.003   0.0% |
LCAO WFS Initialize:                  0.510     0.018   0.2% |
 Hamiltonian:                         0.491     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.017     0.017   0.2% |
  Hartree integrate/restrict:         0.012     0.012   0.1% |
  Initialize Hamiltonian:             0.001     0.001   0.0% |
  Poisson:                            0.256     0.007   0.1% |
   Communicate from 1D:               0.054     0.054   0.6% |
   Communicate from 2D:               0.051     0.051   0.5% |
   Communicate to 1D:                 0.054     0.054   0.6% |
   Communicate to 2D:                 0.053     0.053   0.5% |
   FFT 1D:                            0.012     0.012   0.1% |
   FFT 2D:                            0.025     0.025   0.3% |
  XC 3D grid:                         0.201     0.201   2.1% ||
  vbar:                               0.003     0.003   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            8.116     0.003   0.0% |
 Density:                             0.022     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.017     0.017   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.004     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.003     0.003   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            7.612     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.075     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.011     0.011   0.1% |
   Potential matrix:                  0.059     0.059   0.6% |
  Density:                            0.352     0.000   0.0% |
   Atomic density matrices:           0.016     0.016   0.2% |
   Mix:                               0.274     0.274   2.8% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.059     0.007   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.051     0.051   0.5% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        7.175     0.001   0.0% |
   Atomic:                            0.003     0.003   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.007     0.007   0.1% |
   Communicate:                       0.259     0.259   2.7% ||
   Hartree integrate/restrict:        0.173     0.173   1.8% ||
   New Kinetic Energy:                0.001     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.735     0.089   0.9% |
    Communicate from 1D:              0.789     0.789   8.1% |--|
    Communicate from 2D:              0.756     0.756   7.8% |--|
    Communicate to 1D:                0.771     0.771   7.9% |--|
    Communicate to 2D:                0.781     0.781   8.0% |--|
    FFT 1D:                           0.174     0.174   1.8% ||
    FFT 2D:                           0.375     0.375   3.9% |-|
   XC 3D grid:                        2.950     2.950  30.3% |-----------|
   vbar:                              0.046     0.046   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.005     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.005     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.004     0.004   0.0% |
 Hamiltonian:                         0.469     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.018     0.018   0.2% |
  Hartree integrate/restrict:         0.011     0.011   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.246     0.006   0.1% |
   Communicate from 1D:               0.052     0.052   0.5% |
   Communicate from 2D:               0.051     0.051   0.5% |
   Communicate to 1D:                 0.050     0.050   0.5% |
   Communicate to 2D:                 0.051     0.051   0.5% |
   FFT 1D:                            0.012     0.012   0.1% |
   FFT 2D:                            0.025     0.025   0.3% |
  XC 3D grid:                         0.190     0.190   1.9% ||
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.005     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.004     0.004   0.0% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.294     0.294   3.0% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.807     0.807   8.3% |--|
------------------------------------------------------------
Total:                                          9.732 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:26:55 2021
