
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:22:05 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4171.049506

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*120*104 grid
  Fine grid: 208*240*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*240*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 576.68 MiB
  Calculator: 55.24 MiB
    Density: 32.25 MiB
      Arrays: 31.24 MiB
      Localized functions: 1.01 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.51 MiB
      Arrays: 20.43 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.47 MiB
      C [qnM]: 0.04 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.36 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 9
Number of atomic orbitals: 69
Number of bands in calculation: 69
Number of valence electrons: 20
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .--------------------------------------.  
           /|                                      |  
          / |                                      |  
         /  |                                      |  
        /   |                                      |  
       /    |                                      |  
      /     |                                      |  
     /      |                                      |  
    /       |                                      |  
   /        |                                      |  
  /         |                                      |  
 *          |                                      |  
 |          |                                      |  
 |          |              H                       |  
 |          |             OC                       |  
 |          |            H   H                     |  
 |          |            C                         |  
 |          |          H   H                       |  
 |          |                                      |  
 |          .--------------------------------------.  
 |         /                                      /   
 |        /                                      /    
 |       /                                      /     
 |      /                                      /      
 |     /                                      /       
 |    /                                      /        
 |   /                                      /         
 |  /                                      /          
 | /                                      /           
 |/                                      /            
 *--------------------------------------*             

Positions:
   0 C      7.891784   10.183494    7.640524    ( 0.0000,  0.0000,  0.0000)
   1 O      7.891784    9.017769    8.440584    ( 0.0000,  0.0000,  0.0000)
   2 C      7.891784    7.852044    7.640524    ( 0.0000,  0.0000,  0.0000)
   3 H      7.891784   11.035538    8.320677    ( 0.0000,  0.0000,  0.0000)
   4 H      8.783568   10.232089    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000   10.232089    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      7.891784    7.000000    8.320677    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000    7.803449    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      8.783568    7.803449    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.783568    0.000000    0.000000   104     0.1518
  2. axis:    no     0.000000   18.035538    0.000000   120     0.1503
  3. axis:    no     0.000000    0.000000   15.440584   104     0.1485

  Lengths:  15.783568  18.035538  15.440584
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1502

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:22:09         -1.08   -44.914443    1      
iter:   2  11:22:10         -2.03   -44.985771    1      
iter:   3  11:22:11         -2.23   -44.990130    1      
iter:   4  11:22:12         -2.63   -44.994256    1      
iter:   5  11:22:12         -3.37   -44.994385    1      
iter:   6  11:22:13         -4.10   -44.994408    1      
iter:   7  11:22:14         -4.25   -44.994414    1      
iter:   8  11:22:14         -4.84   -44.994414    1      
iter:   9  11:22:16         -5.04   -44.994414    1      
iter:  10  11:22:16         -5.30   -44.994414    1      
iter:  11  11:22:17         -5.86   -44.994414    1      
iter:  12  11:22:17         -6.25   -44.994414    1      

Occupied states converged after 16 e/g evaluations

Converged after 12 iterations.

Dipole moment: (-0.000000, 0.000000, -0.220454) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4171.049506)

Kinetic:        +38.941803
Potential:      -42.468227
External:        +0.000000
XC:             -41.585935
Entropy (-ST):   +0.000000
Local:           +0.117945
--------------------------
Free energy:    -44.994414
Extrapolated:   -44.994414

 Band  Eigenvalues  Occupancy
    0    -25.63742    2.00000
    1    -17.78242    2.00000
    2    -15.60403    2.00000
    3    -11.61611    2.00000
    4    -11.37413    2.00000
    5    -11.28116    2.00000
    6     -9.45580    2.00000
    7     -8.98200    2.00000
    8     -7.39108    2.00000
    9     -5.55679    2.00000
   10      1.75386    0.00000
   11      2.81132    0.00000
   12      2.99254    0.00000
   13      3.38695    0.00000
   14      3.43175    0.00000
   15      4.05883    0.00000
   16      4.58107    0.00000
   17      5.68468    0.00000
   18      8.60692    0.00000
   19      8.98965    0.00000
   20     10.28027    0.00000
   21     10.33446    0.00000
   22     11.32948    0.00000
   23     12.43866    0.00000
   24     13.96619    0.00000
   25     14.63835    0.00000
   26     14.95086    0.00000
   27     15.27642    0.00000
   28     15.90810    0.00000
   29     16.08539    0.00000
   30     16.18138    0.00000
   31     16.64343    0.00000
   32     21.26159    0.00000
   33     21.28674    0.00000
   34     21.41434    0.00000
   35     23.54554    0.00000
   36     24.75586    0.00000
   37     27.29534    0.00000
   38     27.91532    0.00000
   39     28.39994    0.00000
   40     31.29428    0.00000
   41     31.52023    0.00000
   42     32.21682    0.00000
   43     33.70773    0.00000
   44     35.05126    0.00000
   45     35.62712    0.00000
   46     35.63268    0.00000
   47     36.53092    0.00000
   48     36.78053    0.00000
   49     37.24979    0.00000
   50     40.09485    0.00000
   51     40.27370    0.00000
   52     41.06088    0.00000
   53     42.00903    0.00000
   54     42.90134    0.00000
   55     44.80075    0.00000
   56     45.62979    0.00000
   57     45.79650    0.00000
   58     49.49537    0.00000
   59     50.74065    0.00000
   60     51.38531    0.00000
   61     51.64259    0.00000
   62     52.11710    0.00000
   63     55.75579    0.00000
   64     59.33074    0.00000
   65     61.12876    0.00000
   66     64.11474    0.00000
   67     67.12173    0.00000
   68     68.13155    0.00000

Fermi level: -1.90147

Gap: 7.311 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.006     0.006   0.0% |
LCAO WFS Initialize:                  0.615     0.018   0.1% |
 Hamiltonian:                         0.597     0.000   0.0% |
  Atomic:                             0.020     0.005   0.0% |
   XC Correction:                     0.015     0.015   0.1% |
  Calculate atomic Hamiltonians:      0.003     0.003   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.014     0.014   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.311     0.009   0.1% |
   Communicate from 1D:               0.064     0.064   0.5% |
   Communicate from 2D:               0.061     0.061   0.5% |
   Communicate to 1D:                 0.064     0.064   0.5% |
   Communicate to 2D:                 0.066     0.066   0.5% |
   FFT 1D:                            0.014     0.014   0.1% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.244     0.244   1.9% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.002     0.002   0.0% |
SCF-cycle:                           10.848     0.003   0.0% |
 Density:                             0.030     0.000   0.0% |
  Atomic density matrices:            0.003     0.003   0.0% |
  Mix:                                0.017     0.017   0.1% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.011     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.010     0.010   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           10.199     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.216     0.003   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.002     0.002   0.0% |
   Distribute overlap matrix:         0.027     0.027   0.2% |
   Potential matrix:                  0.183     0.183   1.4% ||
  Density:                            0.517     0.000   0.0% |
   Atomic density matrices:           0.042     0.042   0.3% |
   Mix:                               0.300     0.300   2.4% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.171     0.009   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.160     0.160   1.3% ||
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        9.455     0.001   0.0% |
   Atomic:                            0.319     0.071   0.6% |
    XC Correction:                    0.248     0.248   2.0% ||
   Calculate atomic Hamiltonians:     0.045     0.045   0.4% |
   Communicate:                       0.001     0.001   0.0% |
   Hartree integrate/restrict:        0.228     0.228   1.8% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.954     0.111   0.9% |
    Communicate from 1D:              1.027     1.027   8.1% |--|
    Communicate from 2D:              0.969     0.969   7.7% |--|
    Communicate to 1D:                1.009     1.009   8.0% |--|
    Communicate to 2D:                1.077     1.077   8.5% |--|
    FFT 1D:                           0.230     0.230   1.8% ||
    FFT 2D:                           0.531     0.531   4.2% |-|
   XC 3D grid:                        3.846     3.846  30.5% |-----------|
   vbar:                              0.060     0.060   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.006     0.001   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.014     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.002     0.002   0.0% |
  Potential matrix:                   0.011     0.011   0.1% |
 Hamiltonian:                         0.588     0.000   0.0% |
  Atomic:                             0.020     0.005   0.0% |
   XC Correction:                     0.015     0.015   0.1% |
  Calculate atomic Hamiltonians:      0.003     0.003   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.013     0.013   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.309     0.007   0.1% |
   Communicate from 1D:               0.064     0.064   0.5% |
   Communicate from 2D:               0.060     0.060   0.5% |
   Communicate to 1D:                 0.064     0.064   0.5% |
   Communicate to 2D:                 0.067     0.067   0.5% |
   FFT 1D:                            0.014     0.014   0.1% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.239     0.239   1.9% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.014     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.002     0.002   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.011     0.011   0.1% |
ST tci:                               0.002     0.002   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.200     0.200   1.6% ||
mktci:                                0.002     0.002   0.0% |
Other:                                0.941     0.941   7.5% |--|
------------------------------------------------------------
Total:                                         12.615 100.0%

Memory usage: 576.68 MiB
Date: Fri Aug  6 11:22:18 2021
