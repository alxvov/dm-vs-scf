
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:11:29 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14.0
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Si.PBE.gz
  compensation charges: gauss, rc=0.33, lmax=2
  cutoffs: 1.86(filt), 2.06(core),
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -7911.324575

Spin-polarized calculation.
Magnetic moment: 2.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*112*100 grid
  Fine grid: 184*224*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*224*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 60.70 MiB
    Density: 34.28 MiB
      Arrays: 33.29 MiB
      Localized functions: 0.98 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 24.81 MiB
      Arrays: 24.74 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.61 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.60 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 23
Number of valence electrons: 6
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           Si                    |  
 |         |          H                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 Si     7.000000    8.271862    7.758952    ( 0.0000,  0.0000,  2.0000)
   1 H      7.000000    9.543724    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.543724    0.000000   112     0.1477
  3. axis:    no     0.000000    0.000000   14.758952   100     0.1476

  Lengths:  14.000000  16.543724  14.758952
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1491

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:11:33         -1.06    -8.301835    1        +2.0000
iter:   2  11:11:34         -1.39    -8.438526    1        +2.0000
iter:   3  11:11:35         -2.33    -8.439629    1        +2.0000
iter:   4  11:11:36         -3.12    -8.439691    1        +2.0000
iter:   5  11:11:36         -3.99    -8.439696    1        +2.0000
iter:   6  11:11:37         -4.33    -8.439696    1        +2.0000
iter:   7  11:11:38         -4.83    -8.439696    1        +2.0000
iter:   8  11:11:39         -5.20    -8.439696    1        +2.0000
iter:   9  11:11:40         -5.60    -8.439696    1        +2.0000
iter:  10  11:11:41         -6.21    -8.439696    1        +2.0000

Occupied states converged after 11 e/g evaluations

Converged after 10 iterations.

Dipole moment: (0.000000, 0.000000, 0.012657) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.999999)
Local magnetic moments:
   0 Si ( 0.000000,  0.000000,  0.598758)
   1 H  ( 0.000000,  0.000000,  0.020619)
   2 H  ( 0.000000,  0.000000,  0.020619)

Energy contributions relative to reference atoms: (reference = -7911.324575)

Kinetic:         +9.377859
Potential:       -8.828342
External:        +0.000000
XC:              -8.973142
Entropy (-ST):   +0.000000
Local:           -0.016071
--------------------------
Free energy:     -8.439696
Extrapolated:    -8.439696

Spin contamination: 0.003905 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -13.05856    1.00000    -12.01611    1.00000
    1     -9.07711    1.00000     -8.63280    1.00000
    2     -6.18877    1.00000     -4.02348    0.00000
    3     -4.56660    1.00000     -2.39180    0.00000
    4      0.36214    0.00000      1.28257    0.00000
    5      0.92003    0.00000      2.13072    0.00000
    6      5.03742    0.00000      5.42333    0.00000
    7      5.25770    0.00000      6.08466    0.00000
    8      5.76428    0.00000      7.24402    0.00000
    9      6.55834    0.00000      7.64564    0.00000
   10      7.41932    0.00000      8.87352    0.00000
   11      7.56654    0.00000      9.91331    0.00000
   12      7.71882    0.00000     10.26328    0.00000
   13     10.94602    0.00000     11.90045    0.00000
   14     13.62361    0.00000     14.65280    0.00000
   15     16.08044    0.00000     17.39898    0.00000
   16     18.09895    0.00000     18.76897    0.00000
   17     23.32677    0.00000     24.03329    0.00000
   18     25.55999    0.00000     26.27961    0.00000
   19     26.40315    0.00000     27.31045    0.00000
   20     30.30774    0.00000     30.96430    0.00000
   21     38.63610    0.00000     39.27834    0.00000
   22     42.70166    0.00000     43.40970    0.00000

Fermi level: -2.10223

Gap: 2.754 eV
Transition (v -> c):
  (s=1, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.003     0.003   0.0% |
LCAO WFS Initialize:                  0.827     0.032   0.3% |
 Hamiltonian:                         0.795     0.002   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.040     0.040   0.3% |
  Hartree integrate/restrict:         0.020     0.020   0.2% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.244     0.006   0.1% |
   Communicate from 1D:               0.051     0.051   0.4% |
   Communicate from 2D:               0.048     0.048   0.4% |
   Communicate to 1D:                 0.052     0.052   0.4% |
   Communicate to 2D:                 0.051     0.051   0.4% |
   FFT 1D:                            0.012     0.012   0.1% |
   FFT 2D:                            0.024     0.024   0.2% |
  XC 3D grid:                         0.483     0.483   4.2% |-|
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            9.983     0.006   0.0% |
 Density:                             0.038     0.000   0.0% |
  Atomic density matrices:            0.002     0.002   0.0% |
  Mix:                                0.030     0.030   0.3% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.007     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.006     0.006   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            9.142     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.083     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.001     0.001   0.0% |
   Distribute overlap matrix:         0.012     0.012   0.1% |
   Potential matrix:                  0.066     0.066   0.6% |
  Density:                            0.430     0.000   0.0% |
   Atomic density matrices:           0.017     0.017   0.1% |
   Mix:                               0.336     0.336   2.9% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.003     0.003   0.0% |
   Pseudo density:                    0.074     0.009   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.064     0.064   0.5% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        8.620     0.019   0.2% |
   Atomic:                            0.002     0.002   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.006     0.006   0.0% |
   Communicate:                       0.438     0.438   3.8% |-|
   Hartree integrate/restrict:        0.211     0.211   1.8% ||
   New Kinetic Energy:                0.001     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           2.681     0.060   0.5% |
    Communicate from 1D:              0.565     0.565   4.9% |-|
    Communicate from 2D:              0.529     0.529   4.5% |-|
    Communicate to 1D:                0.559     0.559   4.8% |-|
    Communicate to 2D:                0.568     0.568   4.9% |-|
    FFT 1D:                           0.132     0.132   1.1% |
    FFT 2D:                           0.267     0.267   2.3% ||
   XC 3D grid:                        5.232     5.232  45.0% |-----------------|
   vbar:                              0.031     0.031   0.3% |
  Preconditioning::                   0.000     0.000   0.0% |
  Unitary rotation:                   0.005     0.000   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.008     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Potential matrix:                   0.006     0.006   0.1% |
 Hamiltonian:                         0.782     0.002   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.040     0.040   0.3% |
  Hartree integrate/restrict:         0.019     0.019   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.243     0.005   0.0% |
   Communicate from 1D:               0.051     0.051   0.4% |
   Communicate from 2D:               0.048     0.048   0.4% |
   Communicate to 1D:                 0.051     0.051   0.4% |
   Communicate to 2D:                 0.051     0.051   0.4% |
   FFT 1D:                            0.012     0.012   0.1% |
   FFT 2D:                            0.024     0.024   0.2% |
  XC 3D grid:                         0.474     0.474   4.1% |-|
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.008     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.001     0.001   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.006     0.006   0.1% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.073     0.073   0.6% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.748     0.748   6.4% |--|
------------------------------------------------------------
Total:                                         11.636 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:11:41 2021
