
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:30:51 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -16688.528415

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*112*104 grid
  Fine grid: 224*224*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 55.39 MiB
    Density: 32.52 MiB
      Arrays: 31.40 MiB
      Localized functions: 1.12 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.63 MiB
      Arrays: 20.54 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.25 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.14 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 67
Number of bands in calculation: 67
Number of valence electrons: 24
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |                  H                     |  
 |         |            OC   CH                     |  
 |         |           Cl                           |  
 |         |                  H                     |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      7.845539    8.741877    7.882679    ( 0.0000,  0.0000,  0.0000)
   1 C      9.331614    8.934376    7.882679    ( 0.0000,  0.0000,  0.0000)
   2 Cl     7.393253    7.000000    7.882679    ( 0.0000,  0.0000,  0.0000)
   3 O      7.000000    9.592939    7.882679    ( 0.0000,  0.0000,  0.0000)
   4 H      9.546566   10.002792    7.882679    ( 0.0000,  0.0000,  0.0000)
   5 H      9.763386    8.458066    8.765358    ( 0.0000,  0.0000,  0.0000)
   6 H      9.763386    8.458066    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.763386    0.000000    0.000000   112     0.1497
  2. axis:    no     0.000000   17.002792    0.000000   112     0.1518
  3. axis:    no     0.000000    0.000000   15.765358   104     0.1516

  Lengths:  16.763386  17.002792  15.765358
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1510

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:30:55         -0.86   -36.445611    1      
iter:   2  11:30:56         -1.69   -36.560788    1      
iter:   3  11:30:57         -2.33   -36.577530    1      
iter:   4  11:30:58         -3.02   -36.578975    1      
iter:   5  11:30:58         -3.24   -36.579206    1      
iter:   6  11:30:59         -3.52   -36.579285    1      
iter:   7  11:31:00         -3.88   -36.579292    1      
iter:   8  11:31:00         -4.05   -36.579298    1      
iter:   9  11:31:01         -4.46   -36.579299    1      
iter:  10  11:31:02         -5.10   -36.579299    1      
iter:  11  11:31:02         -5.40   -36.579299    1      
iter:  12  11:31:03         -5.74   -36.579299    1      
iter:  13  11:31:04         -5.90   -36.579299    1      
iter:  14  11:31:05         -6.03   -36.579299    1      

Occupied states converged after 18 e/g evaluations

Converged after 14 iterations.

Dipole moment: (0.496514, -0.005869, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -16688.528415)

Kinetic:        +34.984777
Potential:      -37.125377
External:        +0.000000
XC:             -34.564589
Entropy (-ST):   +0.000000
Local:           +0.125890
--------------------------
Free energy:    -36.579299
Extrapolated:   -36.579299

 Band  Eigenvalues  Occupancy
    0    -27.06564    2.00000
    1    -21.02206    2.00000
    2    -18.87792    2.00000
    3    -13.69197    2.00000
    4    -11.89920    2.00000
    5    -11.58764    2.00000
    6    -11.23854    2.00000
    7    -10.04747    2.00000
    8     -9.59259    2.00000
    9     -7.49913    2.00000
   10     -7.32842    2.00000
   11     -6.66574    2.00000
   12     -1.92137    0.00000
   13      0.15903    0.00000
   14      1.88583    0.00000
   15      3.21219    0.00000
   16      3.39426    0.00000
   17      5.21347    0.00000
   18      6.72633    0.00000
   19      8.72255    0.00000
   20      9.44898    0.00000
   21      9.82761    0.00000
   22     10.37057    0.00000
   23     11.06671    0.00000
   24     12.91219    0.00000
   25     13.28153    0.00000
   26     13.32628    0.00000
   27     13.89976    0.00000
   28     14.42647    0.00000
   29     14.50247    0.00000
   30     14.75951    0.00000
   31     15.40044    0.00000
   32     15.92638    0.00000
   33     16.47411    0.00000
   34     16.69300    0.00000
   35     18.15856    0.00000
   36     18.64609    0.00000
   37     19.42862    0.00000
   38     21.68925    0.00000
   39     22.26280    0.00000
   40     23.86909    0.00000
   41     24.17957    0.00000
   42     26.79087    0.00000
   43     27.13727    0.00000
   44     28.17247    0.00000
   45     30.93403    0.00000
   46     33.09778    0.00000
   47     34.81753    0.00000
   48     34.98233    0.00000
   49     35.37250    0.00000
   50     36.24895    0.00000
   51     37.19082    0.00000
   52     38.86706    0.00000
   53     40.21811    0.00000
   54     42.46138    0.00000
   55     44.35414    0.00000
   56     44.97365    0.00000
   57     47.25717    0.00000
   58     48.87245    0.00000
   59     48.87560    0.00000
   60     50.68592    0.00000
   61     52.81622    0.00000
   62     53.85024    0.00000
   63     57.09829    0.00000
   64     61.38314    0.00000
   65     67.10207    0.00000
   66     68.16396    0.00000

Fermi level: -4.29356

Gap: 4.744 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.005     0.005   0.0% |
LCAO WFS Initialize:                  0.623     0.018   0.1% |
 Hamiltonian:                         0.605     0.000   0.0% |
  Atomic:                             0.020     0.020   0.1% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.004     0.004   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.013     0.013   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.317     0.009   0.1% |
   Communicate from 1D:               0.064     0.064   0.5% |
   Communicate from 2D:               0.060     0.060   0.4% |
   Communicate to 1D:                 0.063     0.063   0.4% |
   Communicate to 2D:                 0.067     0.067   0.5% |
   FFT 1D:                            0.020     0.020   0.1% |
   FFT 2D:                            0.033     0.033   0.2% |
  XC 3D grid:                         0.245     0.245   1.7% ||
  vbar:                               0.005     0.005   0.0% |
P tci:                                0.002     0.002   0.0% |
SCF-cycle:                           12.221     0.003   0.0% |
 Density:                             0.029     0.000   0.0% |
  Atomic density matrices:            0.004     0.004   0.0% |
  Mix:                                0.017     0.017   0.1% |
  Multipole moments:                  0.001     0.001   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.008     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.007     0.007   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           11.564     0.002   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.218     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.002     0.002   0.0% |
   Distribute overlap matrix:         0.067     0.067   0.5% |
   Potential matrix:                  0.143     0.143   1.0% |
  Density:                            0.559     0.000   0.0% |
   Atomic density matrices:           0.068     0.068   0.5% |
   Mix:                               0.332     0.332   2.3% ||
   Multipole moments:                 0.020     0.020   0.1% |
   Normalize:                         0.003     0.003   0.0% |
   Pseudo density:                    0.136     0.010   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.125     0.125   0.9% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                       10.775     0.001   0.0% |
   Atomic:                            0.364     0.364   2.6% ||
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.055     0.055   0.4% |
   Communicate:                       0.000     0.000   0.0% |
   Hartree integrate/restrict:        0.249     0.249   1.7% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           5.683     0.127   0.9% |
    Communicate from 1D:              1.158     1.158   8.1% |--|
    Communicate from 2D:              1.094     1.094   7.7% |--|
    Communicate to 1D:                1.140     1.140   8.0% |--|
    Communicate to 2D:                1.205     1.205   8.5% |--|
    FFT 1D:                           0.359     0.359   2.5% ||
    FFT 2D:                           0.601     0.601   4.2% |-|
   XC 3D grid:                        4.353     4.353  30.6% |-----------|
   vbar:                              0.067     0.067   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.007     0.001   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.001     0.001   0.0% |
   Eigendecomposition:                0.005     0.005   0.0% |
 Get canonical representation:        0.013     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.004     0.004   0.0% |
  Potential matrix:                   0.008     0.008   0.1% |
 Hamiltonian:                         0.600     0.000   0.0% |
  Atomic:                             0.020     0.020   0.1% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.003     0.003   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.014     0.014   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.316     0.007   0.0% |
   Communicate from 1D:               0.065     0.065   0.5% |
   Communicate from 2D:               0.061     0.061   0.4% |
   Communicate to 1D:                 0.063     0.063   0.4% |
   Communicate to 2D:                 0.067     0.067   0.5% |
   FFT 1D:                            0.020     0.020   0.1% |
   FFT 2D:                            0.033     0.033   0.2% |
  XC 3D grid:                         0.242     0.242   1.7% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.013     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.004     0.004   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.008     0.008   0.1% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.391     0.391   2.7% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.989     0.989   6.9% |--|
------------------------------------------------------------
Total:                                         14.235 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:31:05 2021
