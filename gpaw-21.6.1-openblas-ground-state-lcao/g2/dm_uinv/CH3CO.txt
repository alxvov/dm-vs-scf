
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:11:50 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -4133.579019

Spin-polarized calculation.
Magnetic moment: 1.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*108*104 grid
  Fine grid: 224*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 1, 0]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 73.70 MiB
    Density: 41.53 MiB
      Arrays: 40.74 MiB
      Localized functions: 0.79 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 30.33 MiB
      Arrays: 30.27 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.85 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.78 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 54
Number of bands in calculation: 54
Number of valence electrons: 17
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |           H                            |  
 |         |                C  O                    |  
 |         |             H                          |  
 |         |           H                            |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      7.639335    7.960023    7.881061    ( 0.0000,  0.0000,  0.1000)
   1 C      8.617626    9.114120    7.881061    ( 0.0000,  0.0000,  0.6000)
   2 H      8.162075    7.000000    7.881061    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.044566    8.762122    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    8.044566    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 O      9.812695    9.055782    7.881061    ( 0.0000,  0.0000,  0.3000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.812695    0.000000    0.000000   112     0.1501
  2. axis:    no     0.000000   16.114120    0.000000   108     0.1492
  3. axis:    no     0.000000    0.000000   15.762122   104     0.1516

  Lengths:  16.812695  16.114120  15.762122
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1503

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:11:56         -0.93   -32.603009    1        +1.0000
iter:   2  11:11:58         -1.60   -32.707757    1        +1.0000
iter:   3  11:11:59         -2.24   -32.725339    1        +1.0000
iter:   4  11:12:00         -2.85   -32.727040    1        +1.0000
iter:   5  11:12:01         -3.37   -32.727253    1        +1.0000
iter:   6  11:12:02         -3.76   -32.727282    1        +1.0000
iter:   7  11:12:03         -3.88   -32.727285    1        +1.0000
iter:   8  11:12:04         -4.10   -32.727290    1        +1.0000
iter:   9  11:12:04         -4.46   -32.727290    1        +1.0000
iter:  10  11:12:05         -4.90   -32.727290    1        +1.0000
iter:  11  11:12:06         -5.37   -32.727290    1        +1.0000
iter:  12  11:12:07         -5.65   -32.727290    1        +1.0000
iter:  13  11:12:09         -6.05   -32.727290    1        +1.0000

Occupied states converged after 17 e/g evaluations

Converged after 13 iterations.

Dipole moment: (-0.373388, -0.254051, -0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999998)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.058752)
   1 C  ( 0.000000,  0.000000,  0.202791)
   2 H  ( 0.000000,  0.000000,  0.009243)
   3 H  ( 0.000000,  0.000000, -0.001407)
   4 H  ( 0.000000,  0.000000, -0.001407)
   5 O  ( 0.000000,  0.000000,  0.197084)

Energy contributions relative to reference atoms: (reference = -4133.579019)

Kinetic:        +27.343741
Potential:      -30.075604
External:        +0.000000
XC:             -30.100120
Entropy (-ST):   +0.000000
Local:           +0.104693
--------------------------
Free energy:    -32.727290
Extrapolated:   -32.727290

Spin contamination: 0.018634 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -26.94855    1.00000    -26.60714    1.00000
    1    -18.85509    1.00000    -18.61572    1.00000
    2    -13.21029    1.00000    -12.80812    1.00000
    3    -11.20202    1.00000    -11.02089    1.00000
    4    -11.14946    1.00000    -10.85369    1.00000
    5    -10.97086    1.00000    -10.58063    1.00000
    6     -9.36035    1.00000     -9.21009    1.00000
    7     -9.01207    1.00000     -8.25708    1.00000
    8     -4.03594    1.00000     -2.37565    0.00000
    9     -1.62937    0.00000     -1.17010    0.00000
   10      2.01331    0.00000      2.09582    0.00000
   11      3.30806    0.00000      3.49006    0.00000
   12      3.43901    0.00000      3.52859    0.00000
   13      5.08027    0.00000      5.32346    0.00000
   14      6.90872    0.00000      7.14492    0.00000
   15      8.97957    0.00000      9.07227    0.00000
   16      9.88014    0.00000     10.48582    0.00000
   17     10.41920    0.00000     10.79672    0.00000
   18     10.68107    0.00000     10.92123    0.00000
   19     11.06107    0.00000     11.50440    0.00000
   20     13.84999    0.00000     13.98029    0.00000
   21     14.44806    0.00000     14.69354    0.00000
   22     14.78012    0.00000     14.86281    0.00000
   23     15.25207    0.00000     15.53191    0.00000
   24     16.73129    0.00000     16.94415    0.00000
   25     17.96370    0.00000     18.03898    0.00000
   26     18.53883    0.00000     19.03382    0.00000
   27     19.97614    0.00000     20.21223    0.00000
   28     20.10168    0.00000     20.51668    0.00000
   29     22.45380    0.00000     22.75412    0.00000
   30     26.51386    0.00000     27.10487    0.00000
   31     27.19527    0.00000     27.46461    0.00000
   32     27.28003    0.00000     27.53753    0.00000
   33     30.65519    0.00000     30.88967    0.00000
   34     30.71781    0.00000     31.24295    0.00000
   35     33.87175    0.00000     34.11786    0.00000
   36     35.02485    0.00000     35.24356    0.00000
   37     35.51881    0.00000     35.58313    0.00000
   38     36.13208    0.00000     36.32579    0.00000
   39     37.76277    0.00000     38.15659    0.00000
   40     38.57428    0.00000     38.81873    0.00000
   41     42.24135    0.00000     42.51821    0.00000
   42     43.17941    0.00000     43.46633    0.00000
   43     44.94300    0.00000     45.17048    0.00000
   44     46.00215    0.00000     46.23429    0.00000
   45     48.17392    0.00000     48.55234    0.00000
   46     48.78965    0.00000     49.04615    0.00000
   47     49.83791    0.00000     50.29623    0.00000
   48     50.54194    0.00000     50.66156    0.00000
   49     53.43541    0.00000     53.71344    0.00000
   50     55.22320    0.00000     55.80456    0.00000
   51     61.47988    0.00000     61.72859    0.00000
   52     65.24456    0.00000     65.87989    0.00000
   53     68.06444    0.00000     68.39881    0.00000

Fermi level: -2.83265

Gap: 1.660 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.004     0.004   0.0% |
LCAO WFS Initialize:                  0.919     0.037   0.2% |
 Hamiltonian:                         0.881     0.003   0.0% |
  Atomic:                             0.018     0.018   0.1% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.013     0.013   0.1% |
  Hartree integrate/restrict:         0.026     0.026   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.216     0.009   0.0% |
   Communicate from 1D:               0.020     0.020   0.1% |
   Communicate from 2D:               0.056     0.056   0.3% |
   Communicate to 1D:                 0.061     0.061   0.3% |
   Communicate to 2D:                 0.019     0.019   0.1% |
   FFT 1D:                            0.019     0.019   0.1% |
   FFT 2D:                            0.032     0.032   0.2% |
  XC 3D grid:                         0.599     0.599   3.1% ||
  vbar:                               0.005     0.005   0.0% |
P tci:                                0.002     0.002   0.0% |
SCF-cycle:                           17.027     0.008   0.0% |
 Density:                             0.052     0.000   0.0% |
  Atomic density matrices:            0.005     0.005   0.0% |
  Mix:                                0.033     0.033   0.2% |
  Multipole moments:                  0.001     0.001   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.013     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.012     0.012   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           16.063     0.002   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.319     0.005   0.0% |
   Construct Gradient Matrix:         0.004     0.004   0.0% |
   DenseAtomicCorrection:             0.003     0.003   0.0% |
   Distribute overlap matrix:         0.079     0.079   0.4% |
   Potential matrix:                  0.228     0.228   1.2% |
  Density:                            0.929     0.000   0.0% |
   Atomic density matrices:           0.079     0.079   0.4% |
   Mix:                               0.611     0.611   3.2% ||
   Multipole moments:                 0.010     0.010   0.0% |
   Normalize:                         0.005     0.005   0.0% |
   Pseudo density:                    0.223     0.019   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.202     0.202   1.1% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.003     0.003   0.0% |
  Hamiltonian:                       14.799     0.036   0.2% |
   Atomic:                            0.303     0.303   1.6% ||
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.031     0.031   0.2% |
   Communicate:                       0.224     0.224   1.2% |
   Hartree integrate/restrict:        0.424     0.424   2.2% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.632     0.116   0.6% |
    Communicate from 1D:              0.332     0.332   1.7% ||
    Communicate from 2D:              0.974     0.974   5.1% |-|
    Communicate to 1D:                1.041     1.041   5.4% |-|
    Communicate to 2D:                0.312     0.312   1.6% ||
    FFT 1D:                           0.324     0.324   1.7% ||
    FFT 2D:                           0.534     0.534   2.8% ||
   XC 3D grid:                       10.085    10.085  52.7% |--------------------|
   vbar:                              0.061     0.061   0.3% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.010     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.001     0.001   0.0% |
   Eigendecomposition:                0.008     0.008   0.0% |
 Get canonical representation:        0.020     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.005     0.005   0.0% |
  Potential matrix:                   0.013     0.013   0.1% |
 Hamiltonian:                         0.865     0.002   0.0% |
  Atomic:                             0.020     0.020   0.1% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.011     0.011   0.1% |
  Hartree integrate/restrict:         0.024     0.024   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.212     0.006   0.0% |
   Communicate from 1D:               0.019     0.019   0.1% |
   Communicate from 2D:               0.057     0.057   0.3% |
   Communicate to 1D:                 0.061     0.061   0.3% |
   Communicate to 2D:                 0.018     0.018   0.1% |
   FFT 1D:                            0.019     0.019   0.1% |
   FFT 2D:                            0.031     0.031   0.2% |
  XC 3D grid:                         0.591     0.591   3.1% ||
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.019     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.005     0.005   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.013     0.013   0.1% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.216     0.216   1.1% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.948     0.948   5.0% |-|
------------------------------------------------------------
Total:                                         19.118 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:12:09 2021
