
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:24:12 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4235.414084

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 124*120*112 grid
  Fine grid: 248*240*224 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 248*240*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 1, 0]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 576.68 MiB
  Calculator: 72.37 MiB
    Density: 41.71 MiB
      Arrays: 40.21 MiB
      Localized functions: 1.50 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 26.42 MiB
      Arrays: 26.30 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.24 MiB
      C [qnM]: 0.08 MiB
      S, T [2 x qmm]: 0.16 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 4.00 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 14
Number of atomic orbitals: 102
Number of bands in calculation: 102
Number of valence electrons: 26
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .--------------------------------------------.  
           /|                                            |  
          / |                                            |  
         /  |                                            |  
        /   |                                            |  
       /    |                                            |  
      /     |                                            |  
     /      |                                            |  
    /       |                                            |  
   /        |                                            |  
  /         |                                            |  
 *          |                                            |  
 |          |                                            |  
 |          |                                            |  
 |          |               HH  H                        |  
 |          |                CC                          |  
 |          |          H C    H C H                      |  
 |          |            H     H                         |  
 |          |            H      H                        |  
 |          |                                            |  
 |          .--------------------------------------------.  
 |         /                                            /   
 |        /                                            /    
 |       /                                            /     
 |      /                                            /      
 |     /                                            /       
 |    /                                            /        
 |   /                                            /         
 |  /                                            /          
 | /                                            /           
 |/                                            /            
 *--------------------------------------------*             

Positions:
   0 C      9.161537    8.759198    8.567796    ( 0.0000,  0.0000,  0.0000)
   1 H      9.161537    8.759198    9.666116    ( 0.0000,  0.0000,  0.0000)
   2 C      9.161537   10.209488    8.094613    ( 0.0000,  0.0000,  0.0000)
   3 H      9.161537   10.253195    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      8.276055   10.743893    8.452144    ( 0.0000,  0.0000,  0.0000)
   5 H     10.047019   10.743893    8.452144    ( 0.0000,  0.0000,  0.0000)
   6 C     10.417525    8.034053    8.094613    ( 0.0000,  0.0000,  0.0000)
   7 H     10.455376    8.012200    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H     11.323074    8.533700    8.452144    ( 0.0000,  0.0000,  0.0000)
   9 H     10.437592    7.000000    8.452144    ( 0.0000,  0.0000,  0.0000)
  10 C      7.905549    8.034053    8.094613    ( 0.0000,  0.0000,  0.0000)
  11 H      7.867698    8.012200    7.000000    ( 0.0000,  0.0000,  0.0000)
  12 H      7.885482    7.000000    8.452144    ( 0.0000,  0.0000,  0.0000)
  13 H      7.000000    8.533700    8.452144    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.323074    0.000000    0.000000   124     0.1478
  2. axis:    no     0.000000   17.743893    0.000000   120     0.1479
  3. axis:    no     0.000000    0.000000   16.666116   112     0.1488

  Lengths:  18.323074  17.743893  16.666116
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1481

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:24:17         -0.86   -71.799566    1      
iter:   2  11:24:17         -1.61   -71.896734    1      
iter:   3  11:24:18         -2.04   -71.942149    1      
iter:   4  11:24:19         -2.98   -71.942514    1      
iter:   5  11:24:19         -3.39   -71.942648    1      
iter:   6  11:24:20         -3.87   -71.942658    1      
iter:   7  11:24:21         -4.42   -71.942660    1      
iter:   8  11:24:22         -5.06   -71.942661    1      
iter:   9  11:24:22         -5.64   -71.942661    1      
iter:  10  11:24:23         -5.75   -71.942661    1      
iter:  11  11:24:24         -6.06   -71.942661    1      

Occupied states converged after 13 e/g evaluations

Converged after 11 iterations.

Dipole moment: (-0.000000, 0.000011, 0.023028) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4235.414084)

Kinetic:        +58.376643
Potential:      -66.268414
External:        +0.000000
XC:             -64.145594
Entropy (-ST):   +0.000000
Local:           +0.094704
--------------------------
Free energy:    -71.942661
Extrapolated:   -71.942661

 Band  Eigenvalues  Occupancy
    0    -19.87795    2.00000
    1    -16.74068    2.00000
    2    -16.74068    2.00000
    3    -13.61701    2.00000
    4    -11.26517    2.00000
    5    -10.42219    2.00000
    6    -10.42204    2.00000
    7     -9.14457    2.00000
    8     -9.14441    2.00000
    9     -8.60790    2.00000
   10     -7.78170    2.00000
   11     -7.78151    2.00000
   12     -7.57989    2.00000
   13      1.62536    0.00000
   14      2.64504    0.00000
   15      3.13956    0.00000
   16      3.14053    0.00000
   17      3.41322    0.00000
   18      3.41378    0.00000
   19      3.91172    0.00000
   20      4.69475    0.00000
   21      4.69525    0.00000
   22      5.19107    0.00000
   23      5.35152    0.00000
   24      6.19471    0.00000
   25      6.19561    0.00000
   26      8.04760    0.00000
   27      8.04772    0.00000
   28      9.10085    0.00000
   29      9.22032    0.00000
   30      9.22118    0.00000
   31     11.85924    0.00000
   32     12.01557    0.00000
   33     12.01606    0.00000
   34     12.70170    0.00000
   35     14.38756    0.00000
   36     14.98079    0.00000
   37     14.98108    0.00000
   38     15.58168    0.00000
   39     15.74350    0.00000
   40     15.74635    0.00000
   41     15.86396    0.00000
   42     16.27638    0.00000
   43     16.52572    0.00000
   44     16.52687    0.00000
   45     17.13747    0.00000
   46     17.81393    0.00000
   47     17.81767    0.00000
   48     18.93127    0.00000
   49     18.93161    0.00000
   50     22.57921    0.00000
   51     23.82583    0.00000
   52     23.82637    0.00000
   53     24.56559    0.00000
   54     28.18294    0.00000
   55     28.18304    0.00000
   56     28.56795    0.00000
   57     28.56814    0.00000
   58     29.21590    0.00000
   59     30.54426    0.00000
   60     31.71278    0.00000
   61     31.71313    0.00000
   62     33.23273    0.00000
   63     33.23296    0.00000
   64     33.23390    0.00000
   65     33.64188    0.00000
   66     35.33239    0.00000
   67     35.33399    0.00000
   68     36.38148    0.00000
   69     36.83678    0.00000
   70     36.83713    0.00000
   71     37.56703    0.00000
   72     38.32449    0.00000
   73     38.90358    0.00000
   74     38.90388    0.00000
   75     40.29412    0.00000
   76     40.44130    0.00000
   77     40.44177    0.00000
   78     40.53708    0.00000
   79     41.35855    0.00000
   80     41.36039    0.00000
   81     43.22778    0.00000
   82     43.22880    0.00000
   83     44.64537    0.00000
   84     44.64594    0.00000
   85     45.06599    0.00000
   86     46.31900    0.00000
   87     48.19333    0.00000
   88     50.32996    0.00000
   89     50.33019    0.00000
   90     51.48683    0.00000
   91     51.52103    0.00000
   92     51.52299    0.00000
   93     53.59161    0.00000
   94     53.59206    0.00000
   95     54.38559    0.00000
   96     54.65924    0.00000
   97     55.53772    0.00000
   98     55.53832    0.00000
   99     59.10451    0.00000
  100     60.88005    0.00000
  101     60.88034    0.00000

Fermi level: -2.97727

Gap: 9.205 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.010     0.010   0.1% |
LCAO WFS Initialize:                  0.678     0.026   0.2% |
 Hamiltonian:                         0.652     0.000   0.0% |
  Atomic:                             0.025     0.009   0.1% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.004     0.004   0.0% |
  Communicate:                        0.003     0.003   0.0% |
  Hartree integrate/restrict:         0.018     0.018   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.283     0.013   0.1% |
   Communicate from 1D:               0.023     0.023   0.2% |
   Communicate from 2D:               0.074     0.074   0.6% |
   Communicate to 1D:                 0.075     0.075   0.6% |
   Communicate to 2D:                 0.028     0.028   0.2% |
   FFT 1D:                            0.028     0.028   0.2% |
   FFT 2D:                            0.043     0.043   0.3% |
  XC 3D grid:                         0.314     0.314   2.6% ||
  vbar:                               0.005     0.005   0.0% |
P tci:                                0.007     0.007   0.1% |
SCF-cycle:                           10.267     0.003   0.0% |
 Density:                             0.054     0.000   0.0% |
  Atomic density matrices:            0.002     0.002   0.0% |
  Mix:                                0.025     0.025   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.026     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.026     0.026   0.2% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            9.507     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.418     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.002     0.002   0.0% |
   Distribute overlap matrix:         0.021     0.021   0.2% |
   Potential matrix:                  0.389     0.389   3.2% ||
  Density:                            0.727     0.000   0.0% |
   Atomic density matrices:           0.012     0.012   0.1% |
   Mix:                               0.347     0.347   2.8% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.364     0.011   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.352     0.352   2.9% ||
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        8.350     0.001   0.0% |
   Atomic:                            0.326     0.124   1.0% |
    XC Correction:                    0.201     0.201   1.6% ||
   Calculate atomic Hamiltonians:     0.032     0.032   0.3% |
   Communicate:                       0.038     0.038   0.3% |
   Hartree integrate/restrict:        0.253     0.253   2.1% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.596     0.125   1.0% |
    Communicate from 1D:              0.290     0.290   2.4% ||
    Communicate from 2D:              0.968     0.968   7.9% |--|
    Communicate to 1D:                0.957     0.957   7.8% |--|
    Communicate to 2D:                0.342     0.342   2.8% ||
    FFT 1D:                           0.363     0.363   3.0% ||
    FFT 2D:                           0.551     0.551   4.5% |-|
   XC 3D grid:                        4.040     4.040  33.1% |------------|
   vbar:                              0.063     0.063   0.5% |
  Preconditioning::                   0.002     0.002   0.0% |
  Unitary rotation:                   0.007     0.001   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.001     0.001   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.033     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.002     0.002   0.0% |
  Potential matrix:                   0.030     0.030   0.2% |
 Hamiltonian:                         0.637     0.000   0.0% |
  Atomic:                             0.025     0.009   0.1% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.003     0.003   0.0% |
  Hartree integrate/restrict:         0.020     0.020   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.272     0.009   0.1% |
   Communicate from 1D:               0.022     0.022   0.2% |
   Communicate from 2D:               0.075     0.075   0.6% |
   Communicate to 1D:                 0.072     0.072   0.6% |
   Communicate to 2D:                 0.024     0.024   0.2% |
   FFT 1D:                            0.028     0.028   0.2% |
   FFT 2D:                            0.042     0.042   0.3% |
  XC 3D grid:                         0.310     0.310   2.5% ||
  vbar:                               0.005     0.005   0.0% |
 LCAO eigensolver:                    0.033     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.002     0.002   0.0% |
  Orbital Layouts:                    0.002     0.002   0.0% |
  Potential matrix:                   0.030     0.030   0.2% |
ST tci:                               0.003     0.003   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.076     0.076   0.6% |
mktci:                                0.002     0.002   0.0% |
Other:                                1.169     1.169   9.6% |---|
------------------------------------------------------------
Total:                                         12.212 100.0%

Memory usage: 576.68 MiB
Date: Fri Aug  6 11:24:24 2021
