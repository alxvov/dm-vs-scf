
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:12:15 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -20697.191751

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*116*104 grid
  Fine grid: 184*232*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*232*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 46.03 MiB
    Density: 27.52 MiB
      Arrays: 26.67 MiB
      Localized functions: 0.85 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 17.51 MiB
      Arrays: 17.45 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 0.99 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.93 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 52
Number of valence electrons: 28
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .---------------------------------.  
           /|                                 |  
          / |                                 |  
         /  |                                 |  
        /   |                                 |  
       /    |                                 |  
      /     |                                 |  
     /      |                                 |  
    /       |                                 |  
   /        |                                 |  
  /         |                                 |  
 *          |                                 |  
 |          |                                 |  
 |          |            F                    |  
 |          |           Cl                    |  
 |          |         F                       |  
 |          |           F                     |  
 |          |                                 |  
 |          |                                 |  
 |          .---------------------------------.  
 |         /                                 /   
 |        /                                 /    
 |       /                                 /     
 |      /                                 /      
 |     /                                 /       
 |    /                                 /        
 |   /                                 /         
 |  /                                 /          
 | /                                 /           
 |/                                 /            
 *---------------------------------*             

Positions:
   0 Cl     7.000000    8.714544    8.635142    ( 0.0000,  0.0000,  0.0000)
   1 F      7.000000    8.714544    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000   10.429088    8.531656    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    7.000000    8.531656    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   17.429088    0.000000   116     0.1503
  3. axis:    no     0.000000    0.000000   15.635142   104     0.1503

  Lengths:  14.000000  17.429088  15.635142
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:12:19         -0.94    -7.081180    1      
iter:   2  11:12:20         -1.86    -7.254222    1      
iter:   3  11:12:20         -2.36    -7.269579    1      
iter:   4  11:12:21         -2.70    -7.277586    1      
iter:   5  11:12:21         -3.14    -7.279076    1      
iter:   6  11:12:22         -3.38    -7.279761    1      
iter:   7  11:12:22         -3.64    -7.279888    1      
iter:   8  11:12:23         -4.22    -7.279901    1      
iter:   9  11:12:23         -4.40    -7.279905    1      
iter:  10  11:12:24         -4.62    -7.279906    1      
iter:  11  11:12:24         -4.93    -7.279907    1      
iter:  12  11:12:25         -5.17    -7.279907    1      
iter:  13  11:12:26         -5.47    -7.279907    1      
iter:  14  11:12:26         -5.90    -7.279907    1      
iter:  15  11:12:27         -6.27    -7.279907    1      

Occupied states converged after 18 e/g evaluations

Converged after 15 iterations.

Dipole moment: (-0.000000, -0.000000, 0.186796) |e|*Ang

Energy contributions relative to reference atoms: (reference = -20697.191751)

Kinetic:        +24.059542
Potential:      -15.703846
External:        +0.000000
XC:             -15.835481
Entropy (-ST):   +0.000000
Local:           +0.199878
--------------------------
Free energy:     -7.279907
Extrapolated:    -7.279907

 Band  Eigenvalues  Occupancy
    0    -32.20159    2.00000
    1    -29.30437    2.00000
    2    -29.27559    2.00000
    3    -22.01080    2.00000
    4    -14.70189    2.00000
    5    -14.55735    2.00000
    6    -12.84389    2.00000
    7    -11.19202    2.00000
    8    -10.01118    2.00000
    9     -9.90599    2.00000
   10     -9.51399    2.00000
   11     -9.00548    2.00000
   12     -8.86822    2.00000
   13     -7.45372    2.00000
   14     -4.64307    0.00000
   15     -2.07853    0.00000
   16     10.08348    0.00000
   17     10.79159    0.00000
   18     11.01881    0.00000
   19     11.14735    0.00000
   20     13.00469    0.00000
   21     13.19487    0.00000
   22     13.81404    0.00000
   23     17.56600    0.00000
   24     20.05295    0.00000
   25     20.60174    0.00000
   26     21.23779    0.00000
   27     21.66836    0.00000
   28     21.88151    0.00000
   29     21.93193    0.00000
   30     22.15069    0.00000
   31     22.33539    0.00000
   32     24.50082    0.00000
   33     25.35066    0.00000
   34     42.88826    0.00000
   35     45.95945    0.00000
   36     50.66445    0.00000
   37     56.67295    0.00000
   38     56.86340    0.00000
   39     57.35836    0.00000
   40     57.45617    0.00000
   41     58.15088    0.00000
   42     58.28375    0.00000
   43     58.50443    0.00000
   44     59.59412    0.00000
   45     59.86138    0.00000
   46     60.72255    0.00000
   47     60.98148    0.00000
   48     61.05879    0.00000
   49     64.64100    0.00000
   50     69.31687    0.00000
   51     74.23471    0.00000

Fermi level: -6.04839

Gap: 2.811 eV
Transition (v -> c):
  (s=0, k=0, n=13, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=14, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.003     0.003   0.0% |
LCAO WFS Initialize:                  0.534     0.018   0.2% |
 Hamiltonian:                         0.516     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.023     0.023   0.2% |
  Hartree integrate/restrict:         0.012     0.012   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.268     0.008   0.1% |
   Communicate from 1D:               0.056     0.056   0.5% |
   Communicate from 2D:               0.052     0.052   0.4% |
   Communicate to 1D:                 0.056     0.056   0.5% |
   Communicate to 2D:                 0.055     0.055   0.5% |
   FFT 1D:                            0.013     0.013   0.1% |
   FFT 2D:                            0.027     0.027   0.2% |
  XC 3D grid:                         0.209     0.209   1.8% ||
  vbar:                               0.003     0.003   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                           10.038     0.003   0.0% |
 Density:                             0.021     0.000   0.0% |
  Atomic density matrices:            0.002     0.002   0.0% |
  Mix:                                0.016     0.016   0.1% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.002     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.002     0.002   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            9.513     0.002   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.076     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.033     0.033   0.3% |
   Potential matrix:                  0.037     0.037   0.3% |
  Density:                            0.412     0.000   0.0% |
   Atomic density matrices:           0.039     0.039   0.3% |
   Mix:                               0.326     0.326   2.8% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.003     0.003   0.0% |
   Pseudo density:                    0.042     0.008   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.033     0.033   0.3% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        9.013     0.001   0.0% |
   Atomic:                            0.004     0.004   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.007     0.007   0.1% |
   Communicate:                       0.405     0.405   3.5% ||
   Hartree integrate/restrict:        0.215     0.215   1.9% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.659     0.111   1.0% |
    Communicate from 1D:              0.988     0.988   8.6% |--|
    Communicate from 2D:              0.919     0.919   8.0% |--|
    Communicate to 1D:                0.943     0.943   8.2% |--|
    Communicate to 2D:                0.971     0.971   8.4% |--|
    FFT 1D:                           0.238     0.238   2.1% ||
    FFT 2D:                           0.490     0.490   4.3% |-|
   XC 3D grid:                        3.664     3.664  31.8% |------------|
   vbar:                              0.056     0.056   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.007     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.005     0.005   0.0% |
 Get canonical representation:        0.005     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.002     0.002   0.0% |
  Potential matrix:                   0.002     0.002   0.0% |
 Hamiltonian:                         0.491     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.022     0.022   0.2% |
  Hartree integrate/restrict:         0.012     0.012   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.256     0.006   0.1% |
   Communicate from 1D:               0.054     0.054   0.5% |
   Communicate from 2D:               0.051     0.051   0.4% |
   Communicate to 1D:                 0.051     0.051   0.4% |
   Communicate to 2D:                 0.054     0.054   0.5% |
   FFT 1D:                            0.013     0.013   0.1% |
   FFT 2D:                            0.027     0.027   0.2% |
  XC 3D grid:                         0.197     0.197   1.7% ||
  vbar:                               0.003     0.003   0.0% |
 LCAO eigensolver:                    0.005     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.002     0.002   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.002     0.002   0.0% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.138     0.138   1.2% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.794     0.794   6.9% |--|
------------------------------------------------------------
Total:                                         11.508 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:12:27 2021
