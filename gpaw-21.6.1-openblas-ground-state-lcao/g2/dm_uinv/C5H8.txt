
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:32:42 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -5238.061875

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*112*116 grid
  Fine grid: 224*224*232 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 64.06 MiB
    Density: 36.61 MiB
      Arrays: 35.06 MiB
      Localized functions: 1.54 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 23.06 MiB
      Arrays: 22.94 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.40 MiB
      C [qnM]: 0.08 MiB
      S, T [2 x qmm]: 0.17 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 4.14 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 13
Number of atomic orbitals: 105
Number of bands in calculation: 105
Number of valence electrons: 28
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |             H C  H                     |  
 |         |           H  C H                       |  
 |         |               C                        |  
 |         |                                        |  
 |         |            HC  C H                     |  
 |         |           H     H                      |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      8.265075    8.265075    8.568090    ( 0.0000,  0.0000,  0.0000)
   1 C      8.265075    9.027089    9.833842    ( 0.0000,  0.0000,  0.0000)
   2 C      8.265075    7.503061    9.833842    ( 0.0000,  0.0000,  0.0000)
   3 C      9.027089    8.265075    7.302338    ( 0.0000,  0.0000,  0.0000)
   4 C      7.503061    8.265075    7.302338    ( 0.0000,  0.0000,  0.0000)
   5 H      7.351052    9.530150   10.136180    ( 0.0000,  0.0000,  0.0000)
   6 H      9.179098    9.530150   10.136180    ( 0.0000,  0.0000,  0.0000)
   7 H      7.351052    7.000000   10.136180    ( 0.0000,  0.0000,  0.0000)
   8 H      9.179098    7.000000   10.136180    ( 0.0000,  0.0000,  0.0000)
   9 H      9.530150    7.351052    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H      9.530150    9.179098    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.000000    7.351052    7.000000    ( 0.0000,  0.0000,  0.0000)
  12 H      7.000000    9.179098    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.530150    0.000000    0.000000   112     0.1476
  2. axis:    no     0.000000   16.530150    0.000000   112     0.1476
  3. axis:    no     0.000000    0.000000   17.136180   116     0.1477

  Lengths:  16.530150  16.530150  17.136180
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1476

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:32:47         -0.98   -70.808050    1      
iter:   2  11:32:47         -1.72   -70.880706    1      
iter:   3  11:32:48         -2.12   -70.916220    1      
iter:   4  11:32:49         -2.72   -70.916575    1      
iter:   5  11:32:50         -2.99   -70.917585    1      
iter:   6  11:32:50         -4.37   -70.917592    1      
iter:   7  11:32:51         -4.58   -70.917596    1      
iter:   8  11:32:52         -4.88   -70.917596    1      
iter:   9  11:32:53         -5.18   -70.917596    1      
iter:  10  11:32:53         -5.84   -70.917596    1      
iter:  11  11:32:54         -6.25   -70.917596    1      

Occupied states converged after 13 e/g evaluations

Converged after 11 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5238.061875)

Kinetic:        +57.094012
Potential:      -61.373228
External:        +0.000000
XC:             -66.767072
Entropy (-ST):   +0.000000
Local:           +0.128691
--------------------------
Free energy:    -70.917596
Extrapolated:   -70.917596

 Band  Eigenvalues  Occupancy
    0    -21.99254    2.00000
    1    -19.88214    2.00000
    2    -14.71397    2.00000
    3    -14.71397    2.00000
    4    -13.18966    2.00000
    5    -11.88491    2.00000
    6    -11.42973    2.00000
    7    -11.42973    2.00000
    8     -8.83310    2.00000
    9     -8.49463    2.00000
   10     -8.39442    2.00000
   11     -6.76878    2.00000
   12     -6.76878    2.00000
   13     -6.44580    2.00000
   14      1.84003    0.00000
   15      1.84003    0.00000
   16      2.22660    0.00000
   17      2.61393    0.00000
   18      3.22316    0.00000
   19      3.22316    0.00000
   20      4.49361    0.00000
   21      4.49361    0.00000
   22      4.63968    0.00000
   23      4.93367    0.00000
   24      5.51251    0.00000
   25      6.78252    0.00000
   26      6.78252    0.00000
   27      7.14846    0.00000
   28      8.80304    0.00000
   29      9.00015    0.00000
   30     10.01569    0.00000
   31     10.01569    0.00000
   32     10.06658    0.00000
   33     11.57329    0.00000
   34     11.72386    0.00000
   35     11.97572    0.00000
   36     11.97572    0.00000
   37     12.18736    0.00000
   38     12.99581    0.00000
   39     12.99581    0.00000
   40     15.54796    0.00000
   41     15.60379    0.00000
   42     15.60379    0.00000
   43     15.94273    0.00000
   44     15.94273    0.00000
   45     16.43681    0.00000
   46     16.80905    0.00000
   47     17.53861    0.00000
   48     17.53861    0.00000
   49     19.95789    0.00000
   50     20.91597    0.00000
   51     21.10000    0.00000
   52     23.20880    0.00000
   53     24.30086    0.00000
   54     24.30086    0.00000
   55     24.36486    0.00000
   56     24.79969    0.00000
   57     27.48649    0.00000
   58     29.65867    0.00000
   59     29.83680    0.00000
   60     30.49367    0.00000
   61     31.90912    0.00000
   62     31.90912    0.00000
   63     32.91483    0.00000
   64     33.13660    0.00000
   65     33.13660    0.00000
   66     34.06847    0.00000
   67     34.22158    0.00000
   68     34.22158    0.00000
   69     34.37694    0.00000
   70     34.37694    0.00000
   71     35.37029    0.00000
   72     36.31415    0.00000
   73     36.60077    0.00000
   74     36.67583    0.00000
   75     36.67583    0.00000
   76     37.41025    0.00000
   77     37.41025    0.00000
   78     38.46081    0.00000
   79     41.07027    0.00000
   80     41.23117    0.00000
   81     41.23117    0.00000
   82     42.03683    0.00000
   83     43.16781    0.00000
   84     43.34692    0.00000
   85     44.10624    0.00000
   86     44.93249    0.00000
   87     44.93249    0.00000
   88     46.13721    0.00000
   89     47.31220    0.00000
   90     47.90366    0.00000
   91     49.06729    0.00000
   92     49.06729    0.00000
   93     49.81872    0.00000
   94     49.81872    0.00000
   95     51.89301    0.00000
   96     53.90653    0.00000
   97     55.74520    0.00000
   98     59.11456    0.00000
   99     60.58335    0.00000
  100     60.59246    0.00000
  101     60.59246    0.00000
  102     61.74177    0.00000
  103     61.74177    0.00000
  104     65.51064    0.00000

Fermi level: -2.30289

Gap: 8.286 eV
Transition (v -> c):
  (s=0, k=0, n=13, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=14, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.009     0.009   0.1% |
LCAO WFS Initialize:                  0.699     0.026   0.2% |
 Hamiltonian:                         0.673     0.000   0.0% |
  Atomic:                             0.020     0.004   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.012     0.012   0.1% |
  Hartree integrate/restrict:         0.017     0.017   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.345     0.010   0.1% |
   Communicate from 1D:               0.073     0.073   0.6% |
   Communicate from 2D:               0.069     0.069   0.6% |
   Communicate to 1D:                 0.066     0.066   0.5% |
   Communicate to 2D:                 0.074     0.074   0.6% |
   FFT 1D:                            0.017     0.017   0.1% |
   FFT 2D:                            0.036     0.036   0.3% |
  XC 3D grid:                         0.273     0.273   2.2% ||
  vbar:                               0.005     0.005   0.0% |
P tci:                                0.003     0.003   0.0% |
SCF-cycle:                           10.656     0.003   0.0% |
 Density:                             0.053     0.000   0.0% |
  Atomic density matrices:            0.006     0.006   0.1% |
  Mix:                                0.023     0.023   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.024     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.023     0.023   0.2% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            9.857     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.404     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.001     0.001   0.0% |
   Distribute overlap matrix:         0.056     0.056   0.5% |
   Potential matrix:                  0.340     0.340   2.7% ||
  Density:                            0.707     0.000   0.0% |
   Atomic density matrices:           0.080     0.080   0.6% |
   Mix:                               0.316     0.316   2.5% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.307     0.009   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.297     0.297   2.4% ||
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        8.735     0.001   0.0% |
   Atomic:                            0.259     0.058   0.5% |
    XC Correction:                    0.202     0.202   1.6% ||
   Calculate atomic Hamiltonians:     0.021     0.021   0.2% |
   Communicate:                       0.157     0.157   1.3% ||
   Hartree integrate/restrict:        0.219     0.219   1.8% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.500     0.107   0.9% |
    Communicate from 1D:              0.930     0.930   7.4% |--|
    Communicate from 2D:              0.893     0.893   7.1% |--|
    Communicate to 1D:                0.883     0.883   7.1% |--|
    Communicate to 2D:                0.994     0.994   8.0% |--|
    FFT 1D:                           0.226     0.226   1.8% ||
    FFT 2D:                           0.467     0.467   3.7% ||
   XC 3D grid:                        3.521     3.521  28.2% |----------|
   vbar:                              0.055     0.055   0.4% |
  Preconditioning::                   0.002     0.002   0.0% |
  Unitary rotation:                   0.006     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.032     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.004     0.004   0.0% |
  Potential matrix:                   0.026     0.026   0.2% |
 Hamiltonian:                         0.679     0.000   0.0% |
  Atomic:                             0.020     0.004   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.012     0.012   0.1% |
  Hartree integrate/restrict:         0.019     0.019   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.352     0.008   0.1% |
   Communicate from 1D:               0.071     0.071   0.6% |
   Communicate from 2D:               0.071     0.071   0.6% |
   Communicate to 1D:                 0.068     0.068   0.5% |
   Communicate to 2D:                 0.079     0.079   0.6% |
   FFT 1D:                            0.017     0.017   0.1% |
   FFT 2D:                            0.037     0.037   0.3% |
  XC 3D grid:                         0.270     0.270   2.2% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.032     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.004     0.004   0.0% |
  Orbital Layouts:                    0.002     0.002   0.0% |
  Potential matrix:                   0.026     0.026   0.2% |
ST tci:                               0.003     0.003   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.075     0.075   0.6% |
mktci:                                0.002     0.002   0.0% |
Other:                                1.042     1.042   8.3% |--|
------------------------------------------------------------
Total:                                         12.489 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:32:54 2021
