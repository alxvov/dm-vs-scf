
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:15:59 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -15269.030181

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 563.82 MiB
  Calculator: 36.02 MiB
    Density: 21.60 MiB
      Arrays: 21.10 MiB
      Localized functions: 0.50 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 13.84 MiB
      Arrays: 13.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.58 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.56 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 26
Number of valence electrons: 14
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Cl                   |  
 |        |                                 |  
 |        |            F                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 Cl     7.000000    7.000000    8.659096    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.659096   104     0.1506

  Lengths:  14.000000  14.000000  15.659096
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1516

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:16:02         -1.22    -3.031138    1      
iter:   2  11:16:02         -1.94    -3.080879    1      
iter:   3  11:16:03         -2.64    -3.083862    1      
iter:   4  11:16:03         -3.50    -3.083945    1      
iter:   5  11:16:04         -4.08    -3.083951    1      
iter:   6  11:16:04         -4.49    -3.083951    1      
iter:   7  11:16:04         -4.83    -3.083951    1      
iter:   8  11:16:05         -5.24    -3.083951    1      
iter:   9  11:16:05         -5.76    -3.083951    1      
iter:  10  11:16:06         -6.76    -3.083951    1      

Occupied states converged after 13 e/g evaluations

Converged after 10 iterations.

Dipole moment: (-0.000000, -0.000000, 0.214007) |e|*Ang

Energy contributions relative to reference atoms: (reference = -15269.030181)

Kinetic:        +11.963424
Potential:       -8.662178
External:        +0.000000
XC:              -6.492112
Entropy (-ST):   +0.000000
Local:           +0.106915
--------------------------
Free energy:     -3.083951
Extrapolated:    -3.083951

 Band  Eigenvalues  Occupancy
    0    -30.38581    2.00000
    1    -20.44676    2.00000
    2    -12.85182    2.00000
    3    -10.83751    2.00000
    4    -10.83751    2.00000
    5     -7.27580    2.00000
    6     -7.27580    2.00000
    7     -4.01813    0.00000
    8     11.86421    0.00000
    9     12.88607    0.00000
   10     12.88607    0.00000
   11     14.03395    0.00000
   12     14.03395    0.00000
   13     14.60711    0.00000
   14     14.60810    0.00000
   15     16.38185    0.00000
   16     20.67451    0.00000
   17     22.49536    0.00000
   18     22.49536    0.00000
   19     23.01298    0.00000
   20     45.74008    0.00000
   21     57.81827    0.00000
   22     57.81865    0.00000
   23     59.45602    0.00000
   24     59.45602    0.00000
   25     69.73144    0.00000

Fermi level: -5.64697

Gap: 3.258 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.001     0.001   0.0% |
LCAO WFS Initialize:                  0.413     0.013   0.2% |
 Hamiltonian:                         0.400     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.021     0.021   0.3% |
  Hartree integrate/restrict:         0.009     0.009   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.206     0.006   0.1% |
   Communicate from 1D:               0.043     0.043   0.6% |
   Communicate from 2D:               0.041     0.041   0.6% |
   Communicate to 1D:                 0.041     0.041   0.6% |
   Communicate to 2D:                 0.043     0.043   0.6% |
   FFT 1D:                            0.010     0.010   0.1% |
   FFT 2D:                            0.022     0.022   0.3% |
  XC 3D grid:                         0.162     0.162   2.4% ||
  vbar:                               0.002     0.002   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            5.688     0.002   0.0% |
 Density:                             0.015     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.013     0.013   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.001     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.001     0.001   0.0% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            5.277     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.022     0.001   0.0% |
   Construct Gradient Matrix:         0.001     0.001   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.005     0.005   0.1% |
   Potential matrix:                  0.014     0.014   0.2% |
  Density:                            0.195     0.000   0.0% |
   Atomic density matrices:           0.009     0.009   0.1% |
   Mix:                               0.165     0.165   2.4% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.001     0.001   0.0% |
   Pseudo density:                    0.018     0.005   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.013     0.013   0.2% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.001     0.001   0.0% |
  Hamiltonian:                        5.054     0.001   0.0% |
   Atomic:                            0.003     0.003   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.004     0.004   0.1% |
   Communicate:                       0.264     0.264   3.8% |-|
   Hartree integrate/restrict:        0.122     0.122   1.8% ||
   New Kinetic Energy:                0.001     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           2.656     0.060   0.9% |
    Communicate from 1D:              0.556     0.556   8.1% |--|
    Communicate from 2D:              0.542     0.542   7.9% |--|
    Communicate to 1D:                0.535     0.535   7.8% |--|
    Communicate to 2D:                0.552     0.552   8.0% |--|
    FFT 1D:                           0.131     0.131   1.9% ||
    FFT 2D:                           0.280     0.280   4.1% |-|
   XC 3D grid:                        1.970     1.970  28.7% |----------|
   vbar:                              0.032     0.032   0.5% |
  Preconditioning::                   0.000     0.000   0.0% |
  Unitary rotation:                   0.004     0.000   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.003     0.003   0.0% |
 Get canonical representation:        0.002     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Potential matrix:                   0.001     0.001   0.0% |
 Hamiltonian:                         0.390     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
  Communicate:                        0.020     0.020   0.3% |
  Hartree integrate/restrict:         0.010     0.010   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.205     0.005   0.1% |
   Communicate from 1D:               0.043     0.043   0.6% |
   Communicate from 2D:               0.042     0.042   0.6% |
   Communicate to 1D:                 0.041     0.041   0.6% |
   Communicate to 2D:                 0.043     0.043   0.6% |
   FFT 1D:                            0.010     0.010   0.1% |
   FFT 2D:                            0.022     0.022   0.3% |
  XC 3D grid:                         0.152     0.152   2.2% ||
  vbar:                               0.002     0.002   0.0% |
 LCAO eigensolver:                    0.002     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Orbital Layouts:                    0.000     0.000   0.0% |
  Potential matrix:                   0.001     0.001   0.0% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.136     0.136   2.0% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.627     0.627   9.1% |---|
------------------------------------------------------------
Total:                                          6.866 100.0%

Memory usage: 563.82 MiB
Date: Fri Aug  6 11:16:06 2021
