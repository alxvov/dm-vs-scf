
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:33:04 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Li-setup:
  name: Lithium
  id: 830b4218f175192f93f959cfc0aad614
  Z: 3.0
  valence: 1
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Li.PBE.gz
  compensation charges: gauss, rc=0.33, lmax=2
  cutoffs: 1.91(filt), 2.55(core),
  valence states:
                energy  radius
    2s(1.00)    -2.874   1.058
    2p(0.00)    -1.090   1.058
    *s          24.337   1.058

  LCAO basis set for Li:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Li.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=10.8906 Bohr: 2s-sz confined orbital
      l=0, rc=6.6719 Bohr: 2s-dz split-valence wave
      l=1, rc=10.8906 Bohr: p-type Gaussian polarization

Reference energy: -405.575640

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*112 grid
  Fine grid: 184*184*224 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 41.22 MiB
    Density: 24.35 MiB
      Arrays: 22.74 MiB
      Localized functions: 1.61 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 15.00 MiB
      Arrays: 14.88 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.13 MiB
    Wavefunctions: 1.87 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.00 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.87 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 10
Number of bands in calculation: 10
Number of valence electrons: 2
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Li                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Li                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Li     7.000000    7.000000    9.773060    ( 0.0000,  0.0000,  0.0000)
   1 Li     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.773060   112     0.1498

  Lengths:  14.000000  14.000000  16.773060
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1514

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:33:06         -0.69    -1.306502    1      
iter:   2  11:33:06         -1.31    -1.318162    1      
iter:   3  11:33:07         -2.95    -1.318169    1      
iter:   4  11:33:07         -4.57    -1.318169    1      
iter:   5  11:33:08         -5.60    -1.318169    1      
iter:   6  11:33:08         -8.95    -1.318169    1      

Occupied states converged after 7 e/g evaluations

Converged after 6 iterations.

Dipole moment: (0.000000, -0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -405.575640)

Kinetic:         +3.188660
Potential:       -2.662855
External:        +0.000000
XC:              -1.842263
Entropy (-ST):   +0.000000
Local:           -0.001710
--------------------------
Free energy:     -1.318169
Extrapolated:    -1.318169

 Band  Eigenvalues  Occupancy
    0     -3.02527    2.00000
    1     -1.56867    0.00000
    2      1.21009    0.00000
    3      2.17044    0.00000
    4      2.17044    0.00000
    5      3.76906    0.00000
    6      3.76906    0.00000
    7      4.85363    0.00000
    8      5.01851    0.00000
    9      7.57243    0.00000

Fermi level: -2.29697

Gap: 1.457 eV
Transition (v -> c):
  (s=0, k=0, n=0, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=1, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.004     0.004   0.1% |
LCAO WFS Initialize:                  0.439     0.015   0.3% |
 Hamiltonian:                         0.424     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.014     0.014   0.3% |
  Hartree integrate/restrict:         0.012     0.012   0.3% |
  Initialize Hamiltonian:             0.001     0.001   0.0% |
  Poisson:                            0.219     0.005   0.1% |
   Communicate from 1D:               0.046     0.046   1.0% |
   Communicate from 2D:               0.045     0.045   1.0% |
   Communicate to 1D:                 0.044     0.044   1.0% |
   Communicate to 2D:                 0.045     0.045   1.0% |
   FFT 1D:                            0.011     0.011   0.2% |
   FFT 2D:                            0.023     0.023   0.5% |
  XC 3D grid:                         0.175     0.175   3.8% |-|
  vbar:                               0.003     0.003   0.1% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            3.470     0.001   0.0% |
 Density:                             0.018     0.000   0.0% |
  Atomic density matrices:            0.001     0.001   0.0% |
  Mix:                                0.014     0.014   0.3% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.003     0.000   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.002     0.002   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            3.033     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.022     0.000   0.0% |
   Construct Gradient Matrix:         0.001     0.001   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.002     0.002   0.1% |
   Potential matrix:                  0.018     0.018   0.4% |
  Density:                            0.125     0.000   0.0% |
   Atomic density matrices:           0.005     0.005   0.1% |
   Mix:                               0.098     0.098   2.2% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.001     0.001   0.0% |
   Pseudo density:                    0.021     0.003   0.1% |
    Calculate density matrix:         0.000     0.000   0.0% |
    Construct density:                0.018     0.018   0.4% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.001     0.001   0.0% |
  Hamiltonian:                        2.883     0.001   0.0% |
   Atomic:                            0.001     0.001   0.0% |
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.004     0.004   0.1% |
   Communicate:                       0.100     0.100   2.2% ||
   Hartree integrate/restrict:        0.075     0.075   1.6% ||
   New Kinetic Energy:                0.000     0.000   0.0% |
    Pseudo part:                      0.000     0.000   0.0% |
   Poisson:                           1.535     0.034   0.7% |
    Communicate from 1D:              0.321     0.321   7.1% |--|
    Communicate from 2D:              0.314     0.314   6.9% |--|
    Communicate to 1D:                0.309     0.309   6.8% |--|
    Communicate to 2D:                0.319     0.319   7.0% |--|
    FFT 1D:                           0.077     0.077   1.7% ||
    FFT 2D:                           0.161     0.161   3.5% ||
   XC 3D grid:                        1.148     1.148  25.3% |---------|
   vbar:                              0.019     0.019   0.4% |
  Preconditioning::                   0.000     0.000   0.0% |
  Unitary rotation:                   0.002     0.000   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.001     0.001   0.0% |
 Get canonical representation:        0.003     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Potential matrix:                   0.003     0.003   0.1% |
 Hamiltonian:                         0.411     0.000   0.0% |
  Atomic:                             0.000     0.000   0.0% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.014     0.014   0.3% |
  Hartree integrate/restrict:         0.010     0.010   0.2% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.219     0.005   0.1% |
   Communicate from 1D:               0.046     0.046   1.0% |
   Communicate from 2D:               0.045     0.045   1.0% |
   Communicate to 1D:                 0.044     0.044   1.0% |
   Communicate to 2D:                 0.045     0.045   1.0% |
   FFT 1D:                            0.011     0.011   0.2% |
   FFT 2D:                            0.023     0.023   0.5% |
  XC 3D grid:                         0.164     0.164   3.6% ||
  vbar:                               0.003     0.003   0.1% |
 LCAO eigensolver:                    0.003     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.000     0.000   0.0% |
  Orbital Layouts:                    0.000     0.000   0.0% |
  Potential matrix:                   0.003     0.003   0.1% |
ST tci:                               0.000     0.000   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.009     0.009   0.2% |
mktci:                                0.001     0.001   0.0% |
Other:                                0.618     0.618  13.6% |----|
------------------------------------------------------------
Total:                                          4.542 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:33:08 2021
