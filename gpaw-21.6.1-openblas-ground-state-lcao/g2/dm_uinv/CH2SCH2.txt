
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:33:08 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16.0
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/S.PBE.gz
  compensation charges: gauss, rc=0.24, lmax=2
  cutoffs: 1.77(filt), 1.66(core),
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -12962.836777

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*112*108 grid
  Fine grid: 208*224*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*224*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 53.89 MiB
    Density: 31.59 MiB
      Arrays: 30.27 MiB
      Localized functions: 1.32 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.91 MiB
      Arrays: 19.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 2.39 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.31 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 59
Number of bands in calculation: 59
Number of valence electrons: 18
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |             S                        |  
 |         |            H C  H                    |  
 |         |             C                        |  
 |         |          H    H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.913940    7.510423    7.284560    ( 0.0000,  0.0000,  0.0000)
   1 S      7.913940    8.250142    8.940368    ( 0.0000,  0.0000,  0.0000)
   2 C      7.913940    8.989861    7.284560    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      8.827880    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      8.827880    9.500284    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    9.500284    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.827880    0.000000    0.000000   104     0.1522
  2. axis:    no     0.000000   16.500284    0.000000   112     0.1473
  3. axis:    no     0.000000    0.000000   15.940368   108     0.1476

  Lengths:  15.827880  16.500284  15.940368
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1490

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:33:12         -0.84   -34.630111    1      
iter:   2  11:33:13         -1.57   -34.667126    1      
iter:   3  11:33:14         -1.92   -34.715053    1      
iter:   4  11:33:14         -3.07   -34.715275    1      
iter:   5  11:33:15         -3.40   -34.715354    1      
iter:   6  11:33:16         -4.06   -34.715358    1      
iter:   7  11:33:16         -4.47   -34.715359    1      
iter:   8  11:33:17         -4.68   -34.715359    1      
iter:   9  11:33:17         -5.36   -34.715359    1      
iter:  10  11:33:18         -5.84   -34.715359    1      
iter:  11  11:33:19         -6.20   -34.715359    1      

Occupied states converged after 13 e/g evaluations

Converged after 11 iterations.

Dipole moment: (0.000000, 0.000000, -0.326549) |e|*Ang

Energy contributions relative to reference atoms: (reference = -12962.836777)

Kinetic:        +33.141159
Potential:      -34.871961
External:        +0.000000
XC:             -33.015004
Entropy (-ST):   +0.000000
Local:           +0.030447
--------------------------
Free energy:    -34.715359
Extrapolated:   -34.715359

 Band  Eigenvalues  Occupancy
    0    -21.51641    2.00000
    1    -15.18631    2.00000
    2    -15.08975    2.00000
    3    -11.99474    2.00000
    4    -10.98968    2.00000
    5     -9.19820    2.00000
    6     -7.79580    2.00000
    7     -7.58602    2.00000
    8     -4.99845    2.00000
    9     -0.36021    0.00000
   10      1.13189    0.00000
   11      2.25456    0.00000
   12      2.42193    0.00000
   13      2.95283    0.00000
   14      4.97320    0.00000
   15      5.66255    0.00000
   16      7.36052    0.00000
   17      8.16539    0.00000
   18      8.35624    0.00000
   19     10.04443    0.00000
   20     10.63767    0.00000
   21     10.96685    0.00000
   22     11.29108    0.00000
   23     11.69887    0.00000
   24     12.24197    0.00000
   25     13.31569    0.00000
   26     13.80122    0.00000
   27     13.86435    0.00000
   28     15.68624    0.00000
   29     15.80499    0.00000
   30     16.34145    0.00000
   31     18.21346    0.00000
   32     19.80889    0.00000
   33     20.54300    0.00000
   34     22.14778    0.00000
   35     22.67196    0.00000
   36     24.23045    0.00000
   37     26.33646    0.00000
   38     28.08252    0.00000
   39     30.89340    0.00000
   40     32.41895    0.00000
   41     32.78555    0.00000
   42     32.88391    0.00000
   43     33.87885    0.00000
   44     34.35827    0.00000
   45     34.79935    0.00000
   46     35.82399    0.00000
   47     38.31439    0.00000
   48     40.21482    0.00000
   49     40.30620    0.00000
   50     41.64195    0.00000
   51     44.21794    0.00000
   52     45.55813    0.00000
   53     45.63134    0.00000
   54     48.74288    0.00000
   55     49.64938    0.00000
   56     50.74512    0.00000
   57     59.27132    0.00000
   58     61.39185    0.00000

Fermi level: -2.67933

Gap: 4.638 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.006     0.006   0.1% |
LCAO WFS Initialize:                  0.603     0.022   0.2% |
 Hamiltonian:                         0.580     0.000   0.0% |
  Atomic:                             0.016     0.016   0.2% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.005     0.005   0.0% |
  Hartree integrate/restrict:         0.015     0.015   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.302     0.009   0.1% |
   Communicate from 1D:               0.062     0.062   0.6% |
   Communicate from 2D:               0.058     0.058   0.6% |
   Communicate to 1D:                 0.062     0.062   0.6% |
   Communicate to 2D:                 0.065     0.065   0.6% |
   FFT 1D:                            0.015     0.015   0.1% |
   FFT 2D:                            0.031     0.031   0.3% |
  XC 3D grid:                         0.238     0.238   2.3% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.004     0.004   0.0% |
SCF-cycle:                            8.729     0.002   0.0% |
 Density:                             0.034     0.000   0.0% |
  Atomic density matrices:            0.003     0.003   0.0% |
  Mix:                                0.020     0.020   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.011     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.011     0.011   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            8.092     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.180     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.002     0.002   0.0% |
   Distribute overlap matrix:         0.020     0.020   0.2% |
   Potential matrix:                  0.154     0.154   1.5% ||
  Density:                            0.452     0.000   0.0% |
   Atomic density matrices:           0.025     0.025   0.2% |
   Mix:                               0.278     0.278   2.7% ||
   Multipole moments:                 0.001     0.001   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.145     0.007   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.137     0.137   1.3% ||
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        7.452     0.001   0.0% |
   Atomic:                            0.207     0.207   2.0% ||
    XC Correction:                    0.000     0.000   0.0% |
   Calculate atomic Hamiltonians:     0.018     0.018   0.2% |
   Communicate:                       0.061     0.061   0.6% |
   Hartree integrate/restrict:        0.190     0.190   1.8% ||
   New Kinetic Energy:                0.001     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.904     0.086   0.8% |
    Communicate from 1D:              0.807     0.807   7.7% |--|
    Communicate from 2D:              0.760     0.760   7.3% |--|
    Communicate to 1D:                0.799     0.799   7.6% |--|
    Communicate to 2D:                0.854     0.854   8.2% |--|
    FFT 1D:                           0.193     0.193   1.8% ||
    FFT 2D:                           0.407     0.407   3.9% |-|
   XC 3D grid:                        3.023     3.023  28.9% |-----------|
   vbar:                              0.046     0.046   0.4% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.005     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.014     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.002     0.002   0.0% |
  Potential matrix:                   0.012     0.012   0.1% |
 Hamiltonian:                         0.573     0.000   0.0% |
  Atomic:                             0.016     0.016   0.2% |
   XC Correction:                     0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.005     0.005   0.0% |
  Hartree integrate/restrict:         0.014     0.014   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.301     0.006   0.1% |
   Communicate from 1D:               0.063     0.063   0.6% |
   Communicate from 2D:               0.058     0.058   0.6% |
   Communicate to 1D:                 0.063     0.063   0.6% |
   Communicate to 2D:                 0.064     0.064   0.6% |
   FFT 1D:                            0.015     0.015   0.1% |
   FFT 2D:                            0.031     0.031   0.3% |
  XC 3D grid:                         0.232     0.232   2.2% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.014     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.002     0.002   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.012     0.012   0.1% |
ST tci:                               0.001     0.001   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.205     0.205   2.0% ||
mktci:                                0.001     0.001   0.0% |
Other:                                0.916     0.916   8.8% |---|
------------------------------------------------------------
Total:                                         10.465 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:33:19 2021
