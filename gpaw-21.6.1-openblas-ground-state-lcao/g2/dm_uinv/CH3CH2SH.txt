
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:22:50 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16.0
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/S.PBE.gz
  compensation charges: gauss, rc=0.24, lmax=2
  cutoffs: 1.77(filt), 1.66(core),
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -12987.817102

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*108*104 grid
  Fine grid: 240*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 1, 0]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 576.68 MiB
  Calculator: 57.88 MiB
    Density: 33.85 MiB
      Arrays: 32.45 MiB
      Localized functions: 1.40 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.34 MiB
      Arrays: 21.23 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 2.69 MiB
      C [qnM]: 0.04 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.58 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 9
Number of atomic orbitals: 69
Number of bands in calculation: 69
Number of valence electrons: 20
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------------.  
          /|                                           |  
         / |                                           |  
        /  |                                           |  
       /   |                                           |  
      /    |                                           |  
     /     |                                           |  
    /      |                                           |  
   /       |                                           |  
  /        |                                           |  
 *         |                                           |  
 |         |                                           |  
 |         |                                           |  
 |         |                H                          |  
 |         |                C   H H                    |  
 |         |           H S  H                          |  
 |         |                    H                      |  
 |         |                                           |  
 |         |                                           |  
 |         .-------------------------------------------.  
 |        /                                           /   
 |       /                                           /    
 |      /                                           /     
 |     /                                           /      
 |    /                                           /       
 |   /                                           /        
 |  /                                           /         
 | /                                           /          
 |/                                           /           
 *-------------------------------------------*            

Positions:
   0 C     10.549689    8.510696    7.885793    ( 0.0000,  0.0000,  0.0000)
   1 C      9.035346    8.657696    7.885793    ( 0.0000,  0.0000,  0.0000)
   2 S      8.279278    7.000000    7.885793    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.403546    7.885793    ( 0.0000,  0.0000,  0.0000)
   4 H      8.710376    9.207766    8.771586    ( 0.0000,  0.0000,  0.0000)
   5 H      8.710376    9.207766    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H     11.021849    9.496366    7.885793    ( 0.0000,  0.0000,  0.0000)
   7 H     10.890250    7.968929    8.771287    ( 0.0000,  0.0000,  0.0000)
   8 H     10.890250    7.968929    7.000299    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.021849    0.000000    0.000000   120     0.1502
  2. axis:    no     0.000000   16.496366    0.000000   108     0.1527
  3. axis:    no     0.000000    0.000000   15.771586   104     0.1516

  Lengths:  18.021849  16.496366  15.771586
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1515

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:22:54         -0.97   -42.679325    1      
iter:   2  11:22:55         -1.88   -42.763262    1      
iter:   3  11:22:55         -2.26   -42.770209    1      
iter:   4  11:22:56         -2.75   -42.771696    1      
iter:   5  11:22:57         -3.23   -42.771818    1      
iter:   6  11:22:57         -3.89   -42.771828    1      
iter:   7  11:22:58         -4.50   -42.771829    1      
iter:   8  11:22:58         -4.72   -42.771829    1      
iter:   9  11:22:59         -5.08   -42.771829    1      
iter:  10  11:22:59         -5.43   -42.771829    1      
iter:  11  11:23:00         -5.86   -42.771829    1      
iter:  12  11:23:01         -6.45   -42.771829    1      

Occupied states converged after 15 e/g evaluations

Converged after 12 iterations.

Dipole moment: (0.003420, 0.283328, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -12987.817102)

Kinetic:        +39.319172
Potential:      -43.012683
External:        +0.000000
XC:             -39.106583
Entropy (-ST):   +0.000000
Local:           +0.028266
--------------------------
Free energy:    -42.771829
Extrapolated:   -42.771829

 Band  Eigenvalues  Occupancy
    0    -19.82060    2.00000
    1    -17.31107    2.00000
    2    -14.68336    2.00000
    3    -11.19223    2.00000
    4    -11.18747    2.00000
    5     -9.43120    2.00000
    6     -8.76025    2.00000
    7     -8.68192    2.00000
    8     -7.65703    2.00000
    9     -5.14958    2.00000
   10      0.25313    0.00000
   11      1.40174    0.00000
   12      2.40393    0.00000
   13      3.04761    0.00000
   14      3.14899    0.00000
   15      3.86428    0.00000
   16      4.30847    0.00000
   17      4.84178    0.00000
   18      7.32801    0.00000
   19      8.13345    0.00000
   20      8.33759    0.00000
   21      9.06761    0.00000
   22      9.70433    0.00000
   23      9.74827    0.00000
   24     11.32946    0.00000
   25     11.57005    0.00000
   26     11.85923    0.00000
   27     12.91244    0.00000
   28     14.42246    0.00000
   29     14.50377    0.00000
   30     15.14233    0.00000
   31     15.32793    0.00000
   32     15.84073    0.00000
   33     16.18346    0.00000
   34     16.31605    0.00000
   35     16.60218    0.00000
   36     17.47463    0.00000
   37     20.93502    0.00000
   38     22.28618    0.00000
   39     22.51197    0.00000
   40     23.04446    0.00000
   41     24.70875    0.00000
   42     27.91329    0.00000
   43     27.91582    0.00000
   44     28.18514    0.00000
   45     28.99975    0.00000
   46     30.38270    0.00000
   47     31.97244    0.00000
   48     34.18747    0.00000
   49     34.25538    0.00000
   50     34.62074    0.00000
   51     36.14092    0.00000
   52     36.27003    0.00000
   53     36.94544    0.00000
   54     37.10968    0.00000
   55     37.90680    0.00000
   56     39.46300    0.00000
   57     42.43344    0.00000
   58     42.88275    0.00000
   59     43.55451    0.00000
   60     44.68701    0.00000
   61     46.06323    0.00000
   62     47.56917    0.00000
   63     49.50648    0.00000
   64     50.96053    0.00000
   65     52.82416    0.00000
   66     54.32980    0.00000
   67     54.69458    0.00000
   68     55.16461    0.00000

Fermi level: -2.44823

Gap: 5.403 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.007     0.007   0.1% |
LCAO WFS Initialize:                  0.548     0.022   0.2% |
 Hamiltonian:                         0.527     0.000   0.0% |
  Atomic:                             0.016     0.000   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.004     0.004   0.0% |
  Communicate:                        0.005     0.005   0.0% |
  Hartree integrate/restrict:         0.015     0.015   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.229     0.009   0.1% |
   Communicate from 1D:               0.020     0.020   0.2% |
   Communicate from 2D:               0.061     0.061   0.6% |
   Communicate to 1D:                 0.063     0.063   0.6% |
   Communicate to 2D:                 0.024     0.024   0.2% |
   FFT 1D:                            0.019     0.019   0.2% |
   FFT 2D:                            0.034     0.034   0.3% |
  XC 3D grid:                         0.254     0.254   2.3% ||
  vbar:                               0.004     0.004   0.0% |
P tci:                                0.000     0.000   0.0% |
SCF-cycle:                            9.216     0.003   0.0% |
 Density:                             0.039     0.000   0.0% |
  Atomic density matrices:            0.009     0.009   0.1% |
  Mix:                                0.020     0.020   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.009     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.009     0.009   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:            8.620     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.277     0.002   0.0% |
   Construct Gradient Matrix:         0.002     0.002   0.0% |
   DenseAtomicCorrection:             0.000     0.000   0.0% |
   Distribute overlap matrix:         0.128     0.128   1.2% |
   Potential matrix:                  0.145     0.145   1.3% ||
  Density:                            0.603     0.000   0.0% |
   Atomic density matrices:           0.129     0.129   1.2% |
   Mix:                               0.326     0.326   3.0% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.144     0.009   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.134     0.134   1.2% |
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        7.730     0.001   0.0% |
   Atomic:                            0.239     0.006   0.1% |
    XC Correction:                    0.233     0.233   2.1% ||
   Calculate atomic Hamiltonians:     0.044     0.044   0.4% |
   Communicate:                       0.069     0.069   0.6% |
   Hartree integrate/restrict:        0.222     0.222   2.0% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           3.345     0.109   1.0% |
    Communicate from 1D:              0.270     0.270   2.5% ||
    Communicate from 2D:              0.912     0.912   8.3% |--|
    Communicate to 1D:                0.936     0.936   8.5% |--|
    Communicate to 2D:                0.320     0.320   2.9% ||
    FFT 1D:                           0.293     0.293   2.7% ||
    FFT 2D:                           0.504     0.504   4.6% |-|
   XC 3D grid:                        3.750     3.750  34.2% |-------------|
   vbar:                              0.058     0.058   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.005     0.001   0.0% |
   Broadcast coefficients:            0.001     0.001   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.019     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.009     0.009   0.1% |
  Potential matrix:                   0.010     0.010   0.1% |
 Hamiltonian:                         0.517     0.000   0.0% |
  Atomic:                             0.016     0.000   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.003     0.003   0.0% |
  Communicate:                        0.005     0.005   0.0% |
  Hartree integrate/restrict:         0.014     0.014   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.227     0.007   0.1% |
   Communicate from 1D:               0.020     0.020   0.2% |
   Communicate from 2D:               0.061     0.061   0.6% |
   Communicate to 1D:                 0.063     0.063   0.6% |
   Communicate to 2D:                 0.023     0.023   0.2% |
   FFT 1D:                            0.019     0.019   0.2% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.248     0.248   2.3% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.019     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.009     0.009   0.1% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.010     0.010   0.1% |
ST tci:                               0.002     0.002   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.200     0.200   1.8% ||
mktci:                                0.002     0.002   0.0% |
Other:                                0.988     0.988   9.0% |---|
------------------------------------------------------------
Total:                                         10.963 100.0%

Memory usage: 576.68 MiB
Date: Fri Aug  6 11:23:01 2021
