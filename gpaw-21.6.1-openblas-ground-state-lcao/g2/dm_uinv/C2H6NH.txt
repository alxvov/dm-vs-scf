
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-1
Date:   Fri Aug  6 11:28:26 2021
Arch:   x86_64
Pid:    1345498
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  eigensolver: {functional: {'name': 'ks'},
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                localizationtype: None,
                matrix_exp: egdecomp2,
                name: direct-min-lcao,
                orthonormalization: {'name': 'gramschmidt'},
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {backend: no-mixing}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fixed-uniform}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7.0
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/N.PBE.gz
  compensation charges: gauss, rc=0.18, lmax=2
  cutoffs: 1.11(filt), 0.96(core),
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3624.408871

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: Uniform distribution of occupation numbers 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*108*120 grid
  Fine grid: 208*216*240 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: no-mixing
  Linear mixing parameter: 0
  Mixing with 0 old densities
  Damping of long wave oscillations: 0 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 628.71 MiB
  Calculator: 57.52 MiB
    Density: 33.45 MiB
      Arrays: 32.45 MiB
      Localized functions: 1.00 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.31 MiB
      Arrays: 21.23 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.75 MiB
      C [qnM]: 0.04 MiB
      S, T [2 x qmm]: 0.08 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.63 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 74
Number of bands in calculation: 74
Number of valence electrons: 20
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |              H                       |  
 |         |                                      |  
 |         |           H C HH                     |  
 |         |              N                       |  
 |         |                                      |  
 |         |           H CHH                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.944690    7.738040   10.288285    ( 0.0000,  0.0000,  0.0000)
   1 N      7.944690    8.555212    9.083405    ( 0.0000,  0.0000,  0.0000)
   2 C      7.944690    7.738040    7.878525    ( 0.0000,  0.0000,  0.0000)
   3 H      8.763721    7.000000   10.331911    ( 0.0000,  0.0000,  0.0000)
   4 H      8.011818    8.383924   11.166810    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.189755   10.345155    ( 0.0000,  0.0000,  0.0000)
   6 H      8.777523    9.140962    9.083405    ( 0.0000,  0.0000,  0.0000)
   7 H      8.763721    7.000000    7.834899    ( 0.0000,  0.0000,  0.0000)
   8 H      8.011818    8.383924    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.189755    7.821655    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.777523    0.000000    0.000000   104     0.1517
  2. axis:    no     0.000000   16.140962    0.000000   108     0.1495
  3. axis:    no     0.000000    0.000000   18.166810   120     0.1514

  Lengths:  15.777523  16.140962  18.166810
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1508

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:28:31         -0.90   -50.414838    1      
iter:   2  11:28:31         -1.60   -50.438466    1      
iter:   3  11:28:32         -1.91   -50.522575    1      
iter:   4  11:28:33         -2.39   -50.524656    1      
iter:   5  11:28:33         -2.76   -50.526254    1      
iter:   6  11:28:34         -4.01   -50.526287    1      
iter:   7  11:28:35         -4.01   -50.526307    1      
iter:   8  11:28:35         -4.29   -50.526308    1      
iter:   9  11:28:36         -4.58   -50.526309    1      
iter:  10  11:28:37         -5.00   -50.526309    1      
iter:  11  11:28:37         -5.35   -50.526309    1      
iter:  12  11:28:38         -5.89   -50.526309    1      
iter:  13  11:28:39         -6.37   -50.526309    1      

Occupied states converged after 15 e/g evaluations

Converged after 13 iterations.

Dipole moment: (0.169182, -0.055418, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3624.408871)

Kinetic:        +41.852206
Potential:      -46.389158
External:        +0.000000
XC:             -46.142422
Entropy (-ST):   +0.000000
Local:           +0.153065
--------------------------
Free energy:    -50.526309
Extrapolated:   -50.526309

 Band  Eigenvalues  Occupancy
    0    -22.26202    2.00000
    1    -17.19327    2.00000
    2    -15.31659    2.00000
    3    -11.86933    2.00000
    4    -10.60593    2.00000
    5    -10.52990    2.00000
    6     -9.20686    2.00000
    7     -8.65627    2.00000
    8     -8.57422    2.00000
    9     -4.80086    2.00000
   10      1.66283    0.00000
   11      2.69502    0.00000
   12      3.04046    0.00000
   13      3.45702    0.00000
   14      3.56459    0.00000
   15      4.23018    0.00000
   16      4.66590    0.00000
   17      5.02810    0.00000
   18      6.32308    0.00000
   19      8.71027    0.00000
   20      8.88464    0.00000
   21      9.82398    0.00000
   22     10.26653    0.00000
   23     10.85745    0.00000
   24     12.60105    0.00000
   25     12.84282    0.00000
   26     14.65610    0.00000
   27     14.94904    0.00000
   28     15.01160    0.00000
   29     15.52036    0.00000
   30     15.82523    0.00000
   31     16.09489    0.00000
   32     16.24859    0.00000
   33     17.66136    0.00000
   34     18.99920    0.00000
   35     19.45542    0.00000
   36     20.31340    0.00000
   37     22.36702    0.00000
   38     25.39260    0.00000
   39     25.49650    0.00000
   40     27.42327    0.00000
   41     27.89428    0.00000
   42     29.20818    0.00000
   43     29.55906    0.00000
   44     29.92992    0.00000
   45     32.57708    0.00000
   46     34.70669    0.00000
   47     35.29885    0.00000
   48     35.34735    0.00000
   49     35.73604    0.00000
   50     36.55852    0.00000
   51     37.46650    0.00000
   52     38.58172    0.00000
   53     39.36988    0.00000
   54     39.42793    0.00000
   55     40.14439    0.00000
   56     40.23870    0.00000
   57     42.26724    0.00000
   58     42.72628    0.00000
   59     43.71589    0.00000
   60     44.41199    0.00000
   61     45.23600    0.00000
   62     46.60954    0.00000
   63     49.15658    0.00000
   64     51.12953    0.00000
   65     51.19939    0.00000
   66     51.43552    0.00000
   67     52.30028    0.00000
   68     55.06790    0.00000
   69     56.69669    0.00000
   70     58.08047    0.00000
   71     59.61577    0.00000
   72     61.00919    0.00000
   73     65.22925    0.00000

Fermi level: -1.56901

Gap: 6.464 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                               incl.     excl.
------------------------------------------------------------
Basic WFS set positions:              0.000     0.000   0.0% |
Basis functions set positions:        0.007     0.007   0.1% |
LCAO WFS Initialize:                  0.640     0.023   0.2% |
 Hamiltonian:                         0.617     0.000   0.0% |
  Atomic:                             0.020     0.004   0.0% |
   XC Correction:                     0.015     0.015   0.1% |
  Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.016     0.016   0.1% |
  Initialize Hamiltonian:             0.000     0.000   0.0% |
  Poisson:                            0.321     0.009   0.1% |
   Communicate from 1D:               0.065     0.065   0.5% |
   Communicate from 2D:               0.063     0.063   0.5% |
   Communicate to 1D:                 0.066     0.066   0.5% |
   Communicate to 2D:                 0.070     0.070   0.6% |
   FFT 1D:                            0.015     0.015   0.1% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.253     0.253   2.0% ||
  vbar:                               0.005     0.005   0.0% |
P tci:                                0.002     0.002   0.0% |
SCF-cycle:                           10.716     0.003   0.0% |
 Density:                             0.038     0.000   0.0% |
  Atomic density matrices:            0.004     0.004   0.0% |
  Mix:                                0.020     0.020   0.2% |
  Multipole moments:                  0.000     0.000   0.0% |
  Normalize:                          0.000     0.000   0.0% |
  Pseudo density:                     0.013     0.001   0.0% |
   Calculate density matrix:          0.000     0.000   0.0% |
   Construct density:                 0.013     0.013   0.1% |
   Symmetrize density:                0.000     0.000   0.0% |
 Direct Minimisation step:           10.028     0.001   0.0% |
  Broadcast gradients:                0.000     0.000   0.0% |
  Calculate gradients:                0.268     0.003   0.0% |
   Construct Gradient Matrix:         0.003     0.003   0.0% |
   DenseAtomicCorrection:             0.001     0.001   0.0% |
   Distribute overlap matrix:         0.044     0.044   0.4% |
   Potential matrix:                  0.217     0.217   1.7% ||
  Density:                            0.593     0.000   0.0% |
   Atomic density matrices:           0.066     0.066   0.5% |
   Mix:                               0.324     0.324   2.6% ||
   Multipole moments:                 0.002     0.002   0.0% |
   Normalize:                         0.002     0.002   0.0% |
   Pseudo density:                    0.199     0.009   0.1% |
    Calculate density matrix:         0.001     0.001   0.0% |
    Construct density:                0.189     0.189   1.5% ||
    Symmetrize density:               0.000     0.000   0.0% |
  Get Search Direction:               0.002     0.002   0.0% |
  Hamiltonian:                        9.156     0.001   0.0% |
   Atomic:                            0.298     0.066   0.5% |
    XC Correction:                    0.233     0.233   1.9% ||
   Calculate atomic Hamiltonians:     0.030     0.030   0.2% |
   Communicate:                       0.001     0.001   0.0% |
   Hartree integrate/restrict:        0.232     0.232   1.8% ||
   New Kinetic Energy:                0.002     0.001   0.0% |
    Pseudo part:                      0.001     0.001   0.0% |
   Poisson:                           4.791     0.109   0.9% |
    Communicate from 1D:              0.977     0.977   7.8% |--|
    Communicate from 2D:              0.953     0.953   7.6% |--|
    Communicate to 1D:                0.994     0.994   7.9% |--|
    Communicate to 2D:                1.040     1.040   8.3% |--|
    FFT 1D:                           0.222     0.222   1.8% ||
    FFT 2D:                           0.494     0.494   3.9% |-|
   XC 3D grid:                        3.744     3.744  29.8% |-----------|
   vbar:                              0.057     0.057   0.5% |
  Preconditioning::                   0.001     0.001   0.0% |
  Unitary rotation:                   0.006     0.001   0.0% |
   Broadcast coefficients:            0.000     0.000   0.0% |
   Calculate projections:             0.000     0.000   0.0% |
   Eigendecomposition:                0.004     0.004   0.0% |
 Get canonical representation:        0.018     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Diagonalize and rotate:             0.001     0.001   0.0% |
  Distribute overlap matrix:          0.003     0.003   0.0% |
  Potential matrix:                   0.014     0.014   0.1% |
 Hamiltonian:                         0.610     0.000   0.0% |
  Atomic:                             0.020     0.004   0.0% |
   XC Correction:                     0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
  Communicate:                        0.000     0.000   0.0% |
  Hartree integrate/restrict:         0.015     0.015   0.1% |
  New Kinetic Energy:                 0.000     0.000   0.0% |
   Pseudo part:                       0.000     0.000   0.0% |
  Poisson:                            0.320     0.007   0.1% |
   Communicate from 1D:               0.065     0.065   0.5% |
   Communicate from 2D:               0.064     0.064   0.5% |
   Communicate to 1D:                 0.066     0.066   0.5% |
   Communicate to 2D:                 0.070     0.070   0.6% |
   FFT 1D:                            0.015     0.015   0.1% |
   FFT 2D:                            0.033     0.033   0.3% |
  XC 3D grid:                         0.249     0.249   2.0% ||
  vbar:                               0.004     0.004   0.0% |
 LCAO eigensolver:                    0.019     0.000   0.0% |
  Calculate projections:              0.000     0.000   0.0% |
  DenseAtomicCorrection:              0.000     0.000   0.0% |
  Distribute overlap matrix:          0.003     0.003   0.0% |
  Orbital Layouts:                    0.001     0.001   0.0% |
  Potential matrix:                   0.015     0.015   0.1% |
ST tci:                               0.002     0.002   0.0% |
Set symmetry:                         0.000     0.000   0.0% |
TCI: Evaluate splines:                0.204     0.204   1.6% ||
mktci:                                0.002     0.002   0.0% |
Other:                                0.986     0.986   7.9% |--|
------------------------------------------------------------
Total:                                         12.559 100.0%

Memory usage: 628.71 MiB
Date: Fri Aug  6 11:28:39 2021
