
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:23:35 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16.0
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/S.PBE.gz
  compensation charges: gauss, rc=0.24, lmax=2
  cutoffs: 1.77(filt), 1.66(core),
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -11922.718500

Spin-polarized calculation.
Magnetic moment: 1.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: <gpaw.occupations.FixMagneticMomentOccupationNumberCalculator object at 0x7f90812734f0> 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*108*104 grid
  Fine grid: 208*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 79.13 MiB
    Density: 49.35 MiB
      Arrays: 37.80 MiB
      Localized functions: 1.04 MiB
      Mixer: 10.52 MiB
    Hamiltonian: 28.17 MiB
      Arrays: 28.09 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 1.61 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.03 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.59 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 41
Number of bands in calculation: 11
Number of valence electrons: 13
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |            H                        |  
 |         |             C H                     |  
 |         |            S                        |  
 |         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.475361    8.798801    7.895197    ( 0.0000,  0.0000,  0.0000)
   1 S      7.475361    7.000000    7.895197    ( 0.0000,  0.0000,  1.0000)
   2 H      8.522486    9.119636    7.895197    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    9.201016    8.790394    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    9.201016    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.522486    0.000000    0.000000   104     0.1493
  2. axis:    no     0.000000   16.201016    0.000000   108     0.1500
  3. axis:    no     0.000000    0.000000   15.790394   104     0.1518

  Lengths:  15.522486  16.201016  15.790394
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1504

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:23:37                 -22.671440    1        +1.0002
iter:   2  11:23:38         -0.74   -21.972504    1        +1.0000
iter:   3  11:23:39         -0.93   -21.746849    1        +1.0000
iter:   4  11:23:39         -1.23   -21.692456    1        +1.0000
iter:   5  11:23:40         -2.05   -21.691441    1        +1.0000
iter:   6  11:23:41         -2.31   -21.690745    1        +1.0000
iter:   7  11:23:42         -2.91   -21.690734    1        +1.0000
iter:   8  11:23:43         -3.08   -21.690729    1        +1.0000
iter:   9  11:23:44         -3.59   -21.690728    1        +1.0000
iter:  10  11:23:45         -3.79   -21.690728    1        +1.0000
iter:  11  11:23:46         -4.06   -21.690728    1        +1.0000
iter:  12  11:23:47         -4.56   -21.690728    1        +1.0000
iter:  13  11:23:48         -4.73   -21.690728    1        +1.0000
iter:  14  11:23:49         -5.29   -21.690728    1        +1.0000
iter:  15  11:23:50         -5.46   -21.690728    1        +1.0000
iter:  16  11:23:51         -5.86   -21.690728    1        +1.0000
iter:  17  11:23:52         -6.22   -21.690728    1        +1.0000

Converged after 17 iterations.

Dipole moment: (0.018542, 0.305147, -0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999998)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000, -0.002078)
   1 S  ( 0.000000,  0.000000,  0.470721)
   2 H  ( 0.000000,  0.000000,  0.020218)
   3 H  ( 0.000000,  0.000000,  0.004314)
   4 H  ( 0.000000,  0.000000,  0.004314)

Energy contributions relative to reference atoms: (reference = -11922.718500)

Kinetic:        +22.404014
Potential:      -23.481575
External:        +0.000000
XC:             -20.641064
Entropy (-ST):   +0.000000
Local:           +0.027896
--------------------------
Free energy:    -21.690728
Extrapolated:   -21.690728

Spin contamination: 0.024611 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -19.28448    1.00000    -18.94392    1.00000
    1    -15.36490    1.00000    -14.78062    1.00000
    2    -10.70518    1.00000    -10.47915    1.00000
    3    -10.51519    1.00000    -10.46553    1.00000
    4     -8.87466    1.00000     -8.58794    1.00000
    5     -6.42724    1.00000     -4.94168    1.00000
    6     -5.29009    1.00000     -4.64876    0.00000
    7      0.80915    0.00000      1.11208    0.00000
    8      2.35115    0.00000      2.45500    0.00000
    9      3.28653    0.00000      3.40922    0.00000
   10      3.51049    0.00000      3.53657    0.00000

Fermi levels: -2.24047, -4.79522

Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.004     0.004   0.0% |
LCAO WFS Initialize:                 0.938     0.034   0.2% |
 Hamiltonian:                        0.904     0.003   0.0% |
  Atomic:                            0.042     0.042   0.2% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.023     0.023   0.1% |
  Initialize Hamiltonian:            0.000     0.000   0.0% |
  Poisson:                           0.281     0.008   0.0% |
   Communicate from 1D:              0.060     0.060   0.3% |
   Communicate from 2D:              0.054     0.054   0.3% |
   Communicate to 1D:                0.059     0.059   0.3% |
   Communicate to 2D:                0.058     0.058   0.3% |
   FFT 1D:                           0.013     0.013   0.1% |
   FFT 2D:                           0.030     0.030   0.2% |
  XC 3D grid:                        0.551     0.551   3.2% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          15.435     0.015   0.1% |
 Density:                            0.856     0.000   0.0% |
  Atomic density matrices:           0.098     0.098   0.6% |
  Mix:                               0.593     0.593   3.4% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.004     0.004   0.0% |
  Pseudo density:                    0.159     0.015   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.142     0.142   0.8% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       14.284     0.032   0.2% |
  Atomic:                            0.668     0.668   3.8% |-|
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.023     0.023   0.1% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.384     0.384   2.2% ||
  Poisson:                           4.469     0.101   0.6% |
   Communicate from 1D:              0.945     0.945   5.4% |-|
   Communicate from 2D:              0.872     0.872   5.0% |-|
   Communicate to 1D:                0.912     0.912   5.2% |-|
   Communicate to 2D:                0.948     0.948   5.4% |-|
   FFT 1D:                           0.205     0.205   1.2% |
   FFT 2D:                           0.485     0.485   2.8% ||
  XC 3D grid:                        8.655     8.655  49.6% |-------------------|
  vbar:                              0.053     0.053   0.3% |
 LCAO eigensolver:                   0.280     0.002   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.003     0.003   0.0% |
  Distribute overlap matrix:         0.087     0.087   0.5% |
  Orbital Layouts:                   0.015     0.015   0.1% |
  Potential matrix:                  0.172     0.172   1.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.204     0.204   1.2% |
mktci:                               0.001     0.001   0.0% |
Other:                               0.883     0.883   5.1% |-|
-----------------------------------------------------------
Total:                                        17.467 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:23:52 2021
