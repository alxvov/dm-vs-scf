
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:24:10 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16.0
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/S.PBE.gz
  compensation charges: gauss, rc=0.24, lmax=2
  cutoffs: 1.77(filt), 1.66(core),
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -15018.093008

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*124*116 grid
  Fine grid: 184*248*232 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*248*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 64.93 MiB
    Density: 40.64 MiB
      Arrays: 31.85 MiB
      Localized functions: 1.63 MiB
      Mixer: 7.16 MiB
    Hamiltonian: 20.97 MiB
      Arrays: 20.83 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.13 MiB
    Wavefunctions: 3.32 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.11 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.20 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 9
Number of atomic orbitals: 85
Number of bands in calculation: 20
Number of valence electrons: 26
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .---------------------------------.  
           /|                                 |  
          / |                                 |  
         /  |                                 |  
        /   |                                 |  
       /    |                                 |  
      /     |                                 |  
     /      |                                 |  
    /       |                                 |  
   /        |                                 |  
  /         |                                 |  
 *          |                                 |  
 |          |                                 |  
 |          |                                 |  
 |          |           SH                    |  
 |          |           C                     |  
 |          |         HC                      |  
 |          |          CC                     |  
 |          |                                 |  
 |          |          H                      |  
 |          |                                 |  
 |          .---------------------------------.  
 |         /                                 /   
 |        /                                 /    
 |       /                                 /     
 |      /                                 /      
 |     /                                 /       
 |    /                                 /        
 |   /                                 /         
 |  /                                 /          
 | /                                 /           
 |/                                 /            
 *---------------------------------*             

Positions:
   0 S      7.000000    9.275343   10.356984    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000   10.509219    9.165757    ( 0.0000,  0.0000,  0.0000)
   2 C      7.000000    8.041467    9.165757    ( 0.0000,  0.0000,  0.0000)
   3 C      7.000000    9.984516    7.894909    ( 0.0000,  0.0000,  0.0000)
   4 C      7.000000    8.566170    7.894909    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000   11.550686    9.459215    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.000000    9.459215    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000   10.597277    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000    7.953409    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   18.550686    0.000000   124     0.1496
  3. axis:    no     0.000000    0.000000   17.356984   116     0.1496

  Lengths:  14.000000  18.550686  17.356984
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1505

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:24:12                 -54.962458    1      
iter:   2  11:24:13         -0.81   -52.758788    1      
iter:   3  11:24:13         -1.00   -52.436581    1      
iter:   4  11:24:14         -1.20   -52.347038    1      
iter:   5  11:24:15         -1.89   -52.340990    1      
iter:   6  11:24:15         -2.26   -52.339390    1      
iter:   7  11:24:16         -2.65   -52.339155    1      
iter:   8  11:24:17         -3.10   -52.339137    1      
iter:   9  11:24:17         -3.30   -52.339134    1      
iter:  10  11:24:18         -3.70   -52.339133    1      
iter:  11  11:24:19         -4.00   -52.339133    1      
iter:  12  11:24:19         -4.25   -52.339133    1      
iter:  13  11:24:20         -4.60   -52.339133    1      
iter:  14  11:24:21         -4.82   -52.339133    1      
iter:  15  11:24:21         -5.09   -52.339133    1      
iter:  16  11:24:22         -5.39   -52.339133    1      
iter:  17  11:24:23         -5.59   -52.339133    1      
iter:  18  11:24:23         -5.90   -52.339133    1      
iter:  19  11:24:24         -6.16   -52.339133    1      

Converged after 19 iterations.

Dipole moment: (0.000000, -0.000000, -0.023158) |e|*Ang

Energy contributions relative to reference atoms: (reference = -15018.093008)

Kinetic:        +45.417997
Potential:      -48.436018
External:        +0.000000
XC:             -49.393712
Entropy (-ST):   +0.000000
Local:           +0.072600
--------------------------
Free energy:    -52.339133
Extrapolated:   -52.339133

 Band  Eigenvalues  Occupancy
    0    -21.83605    2.00000
    1    -18.10345    2.00000
    2    -17.71729    2.00000
    3    -13.73156    2.00000
    4    -13.48798    2.00000
    5    -12.51901    2.00000
    6     -9.81952    2.00000
    7     -9.38429    2.00000
    8     -9.31872    2.00000
    9     -9.11258    2.00000
   10     -8.18672    2.00000
   11     -5.97538    2.00000
   12     -5.57853    2.00000
   13     -0.99381    0.00000
   14      0.43675    0.00000
   15      0.85011    0.00000
   16      2.05690    0.00000
   17      2.40841    0.00000
   18      3.27263    0.00000
   19      3.89509    0.00000

Fermi level: -3.28617

Gap: 4.585 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.007     0.007   0.0% |
LCAO WFS Initialize:                 0.632     0.023   0.2% |
 Hamiltonian:                        0.609     0.000   0.0% |
  Atomic:                            0.020     0.000   0.0% |
   XC Correction:                    0.020     0.020   0.1% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.016     0.016   0.1% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.317     0.008   0.1% |
   Communicate from 1D:              0.065     0.065   0.5% |
   Communicate from 2D:              0.062     0.062   0.4% |
   Communicate to 1D:                0.062     0.062   0.5% |
   Communicate to 2D:                0.070     0.070   0.5% |
   FFT 1D:                           0.017     0.017   0.1% |
   FFT 2D:                           0.033     0.033   0.2% |
  XC 3D grid:                        0.248     0.248   1.8% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          11.988     0.008   0.1% |
 Density:                            0.793     0.000   0.0% |
  Atomic density matrices:           0.076     0.076   0.5% |
  Mix:                               0.443     0.443   3.2% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.269     0.011   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.257     0.257   1.9% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       10.803     0.002   0.0% |
  Atomic:                            0.364     0.007   0.1% |
   XC Correction:                    0.357     0.357   2.6% ||
  Calculate atomic Hamiltonians:     0.027     0.027   0.2% |
  Communicate:                       0.002     0.002   0.0% |
  Hartree integrate/restrict:        0.267     0.267   1.9% ||
  Poisson:                           5.691     0.124   0.9% |
   Communicate from 1D:              1.167     1.167   8.5% |--|
   Communicate from 2D:              1.113     1.113   8.1% |--|
   Communicate to 1D:                1.121     1.121   8.1% |--|
   Communicate to 2D:                1.277     1.277   9.3% |---|
   FFT 1D:                           0.306     0.306   2.2% ||
   FFT 2D:                           0.582     0.582   4.2% |-|
  XC 3D grid:                        4.382     4.382  31.8% |------------|
  vbar:                              0.068     0.068   0.5% |
 LCAO eigensolver:                   0.385     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.053     0.053   0.4% |
  Orbital Layouts:                   0.022     0.022   0.2% |
  Potential matrix:                  0.308     0.308   2.2% ||
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.203     0.203   1.5% ||
mktci:                               0.002     0.002   0.0% |
Other:                               0.960     0.960   7.0% |--|
-----------------------------------------------------------
Total:                                        13.793 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:24:24 2021
