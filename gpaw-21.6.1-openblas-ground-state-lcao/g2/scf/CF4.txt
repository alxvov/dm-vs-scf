
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:37:05 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -11883.951256

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*104 grid
  Fine grid: 208*208*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 53.05 MiB
    Density: 33.99 MiB
      Arrays: 27.03 MiB
      Localized functions: 0.89 MiB
      Mixer: 6.07 MiB
    Hamiltonian: 17.76 MiB
      Arrays: 17.69 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.30 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.23 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 65
Number of bands in calculation: 20
Number of valence electrons: 32
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |               F                     |  
 |         |           F                         |  
 |         |            FC                       |  
 |         |              F                      |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.767436    7.767436    7.767436    ( 0.0000,  0.0000,  0.0000)
   1 F      8.534872    8.534872    8.534872    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000    7.000000    8.534872    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    8.534872    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 F      8.534872    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.534872    0.000000    0.000000   104     0.1494
  2. axis:    no     0.000000   15.534872    0.000000   104     0.1494
  3. axis:    no     0.000000    0.000000   15.534872   104     0.1494

  Lengths:  15.534872  15.534872  15.534872
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1494

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:37:06                 -25.105651    1      
iter:   2  11:37:07         -1.21   -23.841431    1      
iter:   3  11:37:07         -1.42   -23.623240    1      
iter:   4  11:37:08         -1.73   -23.617393    1      
iter:   5  11:37:08         -2.43   -23.615350    1      
iter:   6  11:37:09         -2.76   -23.614677    1      
iter:   7  11:37:10         -3.08   -23.614431    1      
iter:   8  11:37:10         -3.58   -23.614426    1      
iter:   9  11:37:11         -3.77   -23.614423    1      
iter:  10  11:37:11         -4.28   -23.614423    1      
iter:  11  11:37:12         -4.44   -23.614423    1      
iter:  12  11:37:12         -4.67   -23.614423    1      
iter:  13  11:37:13         -5.02   -23.614423    1      
iter:  14  11:37:13         -5.22   -23.614423    1      
iter:  15  11:37:14         -5.59   -23.614423    1      
iter:  16  11:37:14         -5.86   -23.614423    1      
iter:  17  11:37:15         -6.19   -23.614423    1      

Converged after 17 iterations.

Dipole moment: (0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -11883.951256)

Kinetic:        +34.531085
Potential:      -30.514365
External:        +0.000000
XC:             -28.037456
Entropy (-ST):   +0.000000
Local:           +0.406314
--------------------------
Free energy:    -23.614423
Extrapolated:   -23.614423

 Band  Eigenvalues  Occupancy
    0    -34.27905    2.00000
    1    -31.24419    2.00000
    2    -31.24419    2.00000
    3    -31.24419    2.00000
    4    -17.78843    2.00000
    5    -15.37111    2.00000
    6    -15.37111    2.00000
    7    -15.37111    2.00000
    8    -11.56396    2.00000
    9    -11.56396    2.00000
   10    -11.01448    2.00000
   11    -11.01448    2.00000
   12    -11.01448    2.00000
   13     -9.76137    2.00000
   14     -9.76137    2.00000
   15     -9.76137    2.00000
   16      3.74797    0.00000
   17      3.75547    0.00000
   18      3.75547    0.00000
   19      3.75547    0.00000

Fermi level: -3.00670

Gap: 13.509 eV
Transition (v -> c):
  (s=0, k=0, n=15, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=16, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.003     0.003   0.0% |
LCAO WFS Initialize:                 0.536     0.017   0.2% |
 Hamiltonian:                        0.520     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.019     0.019   0.2% |
  Hartree integrate/restrict:        0.013     0.013   0.1% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.271     0.007   0.1% |
   Communicate from 1D:              0.057     0.057   0.6% |
   Communicate from 2D:              0.053     0.053   0.5% |
   Communicate to 1D:                0.055     0.055   0.6% |
   Communicate to 2D:                0.057     0.057   0.6% |
   FFT 1D:                           0.013     0.013   0.1% |
   FFT 2D:                           0.029     0.029   0.3% |
  XC 3D grid:                        0.212     0.212   2.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           8.615     0.007   0.1% |
 Density:                            0.386     0.000   0.0% |
  Atomic density matrices:           0.038     0.038   0.4% |
  Mix:                               0.278     0.278   2.8% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.066     0.008   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.057     0.057   0.6% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        8.116     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.007     0.007   0.1% |
  Communicate:                       0.303     0.303   3.0% ||
  Hartree integrate/restrict:        0.196     0.196   1.9% ||
  Poisson:                           4.247     0.098   1.0% |
   Communicate from 1D:              0.887     0.887   8.8% |---|
   Communicate from 2D:              0.849     0.849   8.4% |--|
   Communicate to 1D:                0.854     0.854   8.5% |--|
   Communicate to 2D:                0.887     0.887   8.8% |---|
   FFT 1D:                           0.211     0.211   2.1% ||
   FFT 2D:                           0.460     0.460   4.6% |-|
  XC 3D grid:                        3.307     3.307  32.8% |------------|
  vbar:                              0.051     0.051   0.5% |
 LCAO eigensolver:                   0.106     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.022     0.022   0.2% |
  Orbital Layouts:                   0.013     0.013   0.1% |
  Potential matrix:                  0.069     0.069   0.7% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.138     0.138   1.4% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.794     0.794   7.9% |--|
-----------------------------------------------------------
Total:                                        10.089 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:37:15 2021
