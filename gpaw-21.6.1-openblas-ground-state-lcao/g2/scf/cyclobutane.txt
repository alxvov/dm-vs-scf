
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:22:26 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4210.433760

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 120*120*108 grid
  Fine grid: 240*240*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*240*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 75.43 MiB
    Density: 47.22 MiB
      Arrays: 37.50 MiB
      Localized functions: 1.28 MiB
      Mixer: 8.45 MiB
    Hamiltonian: 24.63 MiB
      Arrays: 24.53 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.10 MiB
    Wavefunctions: 3.58 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.13 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.43 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 12
Number of atomic orbitals: 92
Number of bands in calculation: 19
Number of valence electrons: 24
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .-------------------------------------------.  
           /|                                           |  
          / |                                           |  
         /  |                                           |  
        /   |                                           |  
       /    |                                           |  
      /     |                                           |  
     /      |                                           |  
    /       |                                           |  
   /        |                                           |  
  /         |                                           |  
 *          |                                           |  
 |          |                                           |  
 |          |                H                          |  
 |          |                                           |  
 |          |           H   HCH  H                      |  
 |          |             C C  C                        |  
 |          |            H H    H                       |  
 |          |                                           |  
 |          |                                           |  
 |          .-------------------------------------------.  
 |         /                                           /   
 |        /                                           /    
 |       /                                           /     
 |      /                                           /      
 |     /                                           /       
 |    /                                           /        
 |   /                                           /         
 |  /                                           /          
 | /                                           /           
 |/                                           /            
 *-------------------------------------------*             

Positions:
   0 C      8.986858   10.058000    8.355146    ( 0.0000,  0.0000,  0.0000)
   1 C      8.986858    7.915716    8.355146    ( 0.0000,  0.0000,  0.0000)
   2 C      7.915716    8.986858    8.059894    ( 0.0000,  0.0000,  0.0000)
   3 C     10.058000    8.986858    8.059894    ( 0.0000,  0.0000,  0.0000)
   4 H      8.986858   10.973716    7.757443    ( 0.0000,  0.0000,  0.0000)
   5 H      8.986858   10.329779    9.415040    ( 0.0000,  0.0000,  0.0000)
   6 H      8.986858    7.000000    7.757443    ( 0.0000,  0.0000,  0.0000)
   7 H      8.986858    7.643937    9.415040    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000    8.986858    8.657597    ( 0.0000,  0.0000,  0.0000)
   9 H      7.643937    8.986858    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H     10.973716    8.986858    8.657597    ( 0.0000,  0.0000,  0.0000)
  11 H     10.329779    8.986858    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.973716    0.000000    0.000000   120     0.1498
  2. axis:    no     0.000000   17.973716    0.000000   120     0.1498
  3. axis:    no     0.000000    0.000000   16.415040   108     0.1520

  Lengths:  17.973716  17.973716  16.415040
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1505

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:22:28                 -66.506243    1      
iter:   2  11:22:28         -0.70   -64.479088    1      
iter:   3  11:22:29         -0.88   -63.635559    1      
iter:   4  11:22:30         -1.28   -63.543674    1      
iter:   5  11:22:31         -2.07   -63.540826    1      
iter:   6  11:22:32         -2.34   -63.539769    1      
iter:   7  11:22:32         -2.57   -63.539280    1      
iter:   8  11:22:33         -3.26   -63.539276    1      
iter:   9  11:22:34         -3.46   -63.539275    1      
iter:  10  11:22:35         -4.11   -63.539275    1      
iter:  11  11:22:36         -4.47   -63.539275    1      
iter:  12  11:22:36         -4.67   -63.539275    1      
iter:  13  11:22:37         -5.14   -63.539275    1      
iter:  14  11:22:38         -5.47   -63.539275    1      
iter:  15  11:22:39         -5.98   -63.539275    1      
iter:  16  11:22:40         -6.35   -63.539275    1      

Converged after 16 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4210.433760)

Kinetic:        +51.854011
Potential:      -57.315160
External:        +0.000000
XC:             -58.182983
Entropy (-ST):   +0.000000
Local:           +0.104857
--------------------------
Free energy:    -63.539275
Extrapolated:   -63.539275

 Band  Eigenvalues  Occupancy
    0    -20.94394    2.00000
    1    -16.01453    2.00000
    2    -16.01453    2.00000
    3    -13.34542    2.00000
    4    -11.61648    2.00000
    5    -11.57172    2.00000
    6     -9.13121    2.00000
    7     -9.13121    2.00000
    8     -8.45760    2.00000
    9     -7.39042    2.00000
   10     -7.21518    2.00000
   11     -7.21518    2.00000
   12      1.84261    0.00000
   13      1.96153    0.00000
   14      2.73863    0.00000
   15      2.73863    0.00000
   16      3.75774    0.00000
   17      4.43934    0.00000
   18      4.43934    0.00000

Fermi level: -2.68629

Gap: 9.058 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.008     0.008   0.1% |
LCAO WFS Initialize:                 0.740     0.026   0.2% |
 Hamiltonian:                        0.714     0.000   0.0% |
  Atomic:                            0.016     0.000   0.0% |
   XC Correction:                    0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:     0.009     0.009   0.1% |
  Communicate:                       0.006     0.006   0.0% |
  Hartree integrate/restrict:        0.019     0.019   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.364     0.009   0.1% |
   Communicate from 1D:              0.075     0.075   0.5% |
   Communicate from 2D:              0.072     0.072   0.5% |
   Communicate to 1D:                0.069     0.069   0.5% |
   Communicate to 2D:                0.079     0.079   0.6% |
   FFT 1D:                           0.023     0.023   0.2% |
   FFT 2D:                           0.037     0.037   0.3% |
  XC 3D grid:                        0.291     0.291   2.1% ||
  vbar:                              0.005     0.005   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          11.850     0.006   0.0% |
 Density:                            0.782     0.000   0.0% |
  Atomic density matrices:           0.076     0.076   0.6% |
  Mix:                               0.410     0.410   3.0% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.290     0.012   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.277     0.277   2.0% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       10.653     0.001   0.0% |
  Atomic:                            0.243     0.006   0.0% |
   XC Correction:                    0.237     0.237   1.7% ||
  Calculate atomic Hamiltonians:     0.134     0.134   1.0% |
  Communicate:                       0.099     0.099   0.7% |
  Hartree integrate/restrict:        0.278     0.278   2.0% ||
  Poisson:                           5.475     0.132   1.0% |
   Communicate from 1D:              1.125     1.125   8.2% |--|
   Communicate from 2D:              1.082     1.082   7.9% |--|
   Communicate to 1D:                1.045     1.045   7.6% |--|
   Communicate to 2D:                1.171     1.171   8.5% |--|
   FFT 1D:                           0.347     0.347   2.5% ||
   FFT 2D:                           0.573     0.573   4.2% |-|
  XC 3D grid:                        4.356     4.356  31.6% |------------|
  vbar:                              0.068     0.068   0.5% |
 LCAO eigensolver:                   0.408     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.048     0.048   0.4% |
  Orbital Layouts:                   0.021     0.021   0.2% |
  Potential matrix:                  0.337     0.337   2.5% ||
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.075     0.075   0.5% |
mktci:                               0.002     0.002   0.0% |
Other:                               1.087     1.087   7.9% |--|
-----------------------------------------------------------
Total:                                        13.765 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:22:40 2021
