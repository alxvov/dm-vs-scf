
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:22:52 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4185.453435

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 116*124*92 grid
  Fine grid: 232*248*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 232*248*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 64.28 MiB
    Density: 40.16 MiB
      Arrays: 31.85 MiB
      Localized functions: 1.15 MiB
      Mixer: 7.16 MiB
    Hamiltonian: 20.93 MiB
      Arrays: 20.83 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 3.20 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.10 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.08 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 82
Number of bands in calculation: 18
Number of valence electrons: 22
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .-----------------------------------------.  
           /|                                         |  
          / |                                         |  
         /  |                                         |  
        /   |                                         |  
       /    |                                         |  
      /     |                                         |  
     /      |                                         |  
    /       |                                         |  
   /        |                                         |  
  /         |                                         |  
 *          |                                         |  
 |          |                                         |  
 |          |               H C  H                    |  
 |          |           H C  C H                      |  
 |          |         H  C H                          |  
 |          |                                         |  
 |          .-----------------------------------------.  
 |         /                                         /   
 |        /                                         /    
 |       /                                         /     
 |      /                                         /      
 |     /                                         /       
 |    /                                         /        
 |   /                                         /         
 |  /                                         /          
 | /                                         /           
 |/                                         /            
 *-----------------------------------------*             

Positions:
   0 C      9.159214   11.063993    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C      9.159214    9.721526    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 C      7.947792    8.913360    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 C      7.947792    7.570893    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H     10.081120   11.634886    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      8.232371   11.630559    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H     10.107006    9.183803    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000    9.451083    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      8.874635    7.004327    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.025886    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.107006    0.000000    0.000000   116     0.1475
  2. axis:    no     0.000000   18.634886    0.000000   124     0.1503
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  17.107006  18.634886  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1500

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:22:54                 -57.815761    1      
iter:   2  11:22:54         -0.73   -56.100303    1      
iter:   3  11:22:55         -0.91   -55.370560    1      
iter:   4  11:22:56         -1.34   -55.310608    1      
iter:   5  11:22:56         -2.19   -55.311063    1      
iter:   6  11:22:57         -2.45   -55.310900    1      
iter:   7  11:22:58         -2.62   -55.310624    1      
iter:   8  11:22:58         -3.17   -55.310590    1      
iter:   9  11:22:59         -3.61   -55.310587    1      
iter:  10  11:23:00         -3.80   -55.310586    1      
iter:  11  11:23:00         -4.01   -55.310587    1      
iter:  12  11:23:01         -4.23   -55.310587    1      
iter:  13  11:23:02         -4.52   -55.310587    1      
iter:  14  11:23:02         -4.83   -55.310587    1      
iter:  15  11:23:03         -5.21   -55.310587    1      
iter:  16  11:23:04         -5.43   -55.310587    1      
iter:  17  11:23:04         -5.66   -55.310587    1      
iter:  18  11:23:05         -5.90   -55.310587    1      
iter:  19  11:23:06         -6.12   -55.310587    1      

Converged after 19 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4185.453435)

Kinetic:        +43.907549
Potential:      -49.915411
External:        +0.000000
XC:             -49.417100
Entropy (-ST):   +0.000000
Local:           +0.114376
--------------------------
Free energy:    -55.310587
Extrapolated:   -55.310587

 Band  Eigenvalues  Occupancy
    0    -19.95074    2.00000
    1    -18.10267    2.00000
    2    -14.86904    2.00000
    3    -13.53183    2.00000
    4    -11.33532    2.00000
    5    -11.24789    2.00000
    6     -9.42026    2.00000
    7     -9.26805    2.00000
    8     -8.26007    2.00000
    9     -7.88987    2.00000
   10     -5.66852    2.00000
   11     -1.64718    0.00000
   12      1.03472    0.00000
   13      2.18323    0.00000
   14      2.52307    0.00000
   15      2.64749    0.00000
   16      3.86551    0.00000
   17      4.34830    0.00000

Fermi level: -3.65785

Gap: 4.021 eV
Transition (v -> c):
  (s=0, k=0, n=10, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=11, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.007     0.007   0.1% |
LCAO WFS Initialize:                 0.632     0.022   0.2% |
 Hamiltonian:                        0.610     0.000   0.0% |
  Atomic:                            0.016     0.000   0.0% |
   XC Correction:                    0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.004     0.004   0.0% |
  Hartree integrate/restrict:        0.016     0.016   0.1% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.318     0.008   0.1% |
   Communicate from 1D:              0.063     0.063   0.5% |
   Communicate from 2D:              0.061     0.061   0.4% |
   Communicate to 1D:                0.062     0.062   0.5% |
   Communicate to 2D:                0.069     0.069   0.5% |
   FFT 1D:                           0.021     0.021   0.2% |
   FFT 2D:                           0.034     0.034   0.2% |
  XC 3D grid:                        0.248     0.248   1.8% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          12.050     0.007   0.1% |
 Density:                            0.779     0.000   0.0% |
  Atomic density matrices:           0.076     0.076   0.6% |
  Mix:                               0.418     0.418   3.1% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.279     0.011   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.266     0.266   1.9% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       10.881     0.001   0.0% |
  Atomic:                            0.290     0.007   0.1% |
   XC Correction:                    0.283     0.283   2.1% ||
  Calculate atomic Hamiltonians:     0.033     0.033   0.2% |
  Communicate:                       0.083     0.083   0.6% |
  Hartree integrate/restrict:        0.288     0.288   2.1% ||
  Poisson:                           5.716     0.122   0.9% |
   Communicate from 1D:              1.142     1.142   8.3% |--|
   Communicate from 2D:              1.100     1.100   8.0% |--|
   Communicate to 1D:                1.117     1.117   8.2% |--|
   Communicate to 2D:                1.252     1.252   9.1% |---|
   FFT 1D:                           0.379     0.379   2.8% ||
   FFT 2D:                           0.604     0.604   4.4% |-|
  XC 3D grid:                        4.402     4.402  32.2% |------------|
  vbar:                              0.068     0.068   0.5% |
 LCAO eigensolver:                   0.383     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.041     0.041   0.3% |
  Orbital Layouts:                   0.021     0.021   0.2% |
  Potential matrix:                  0.319     0.319   2.3% ||
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.076     0.076   0.6% |
mktci:                               0.002     0.002   0.0% |
Other:                               0.916     0.916   6.7% |--|
-----------------------------------------------------------
Total:                                        13.686 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:23:06 2021
