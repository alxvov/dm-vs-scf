
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:15:39 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Na-setup:
  name: Sodium
  id: d7ecbc49209718622bcbe287195dca2a
  Z: 11.0
  valence: 7
  core: 4
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Na.PBE.gz
  compensation charges: gauss, rc=0.37, lmax=2
  cutoffs: 2.17(filt), 2.59(core),
  valence states:
                energy  radius
    3s(1.00)    -2.744   1.201
    2p(6.00)   -28.672   1.217
    3p(0.00)    -0.743   1.217
    *s          24.468   1.201
    *d           0.000   1.238

  LCAO basis set for Na:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Na.dzp.basis.gz
    Number of radial functions: 7
    Number of spherical harmonics: 17
      l=0, rc=11.4062 Bohr: 3s-sz confined orbital
      l=1, rc=3.6719 Bohr: 2p-sz confined orbital
      l=1, rc=15.9375 Bohr: 3p-sz confined orbital
      l=0, rc=6.9531 Bohr: 3s-dz split-valence wave
      l=1, rc=2.4219 Bohr: 2p-dz split-valence wave
      l=1, rc=10.0469 Bohr: 3p-dz split-valence wave
      l=1, rc=11.4062 Bohr: p-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -16973.559893

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 46.93 MiB
    Density: 28.43 MiB
      Arrays: 21.92 MiB
      Localized functions: 1.59 MiB
      Mixer: 4.91 MiB
    Hamiltonian: 14.46 MiB
      Arrays: 14.34 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.04 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 4.03 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 30
Number of bands in calculation: 11
Number of valence electrons: 14
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Cl                   |  
 |        |                                 |  
 |        |                                 |  
 |        |            Na                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Na     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 Cl     7.000000    7.000000    9.390970    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.390970   108     0.1518

  Lengths:  14.000000  14.000000  16.390970
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1520

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:15:41                  -5.071216    1      
iter:   2  11:15:41         -1.00    -4.354038    1      
iter:   3  11:15:42         -1.16    -4.077079    1      
iter:   4  11:15:42         -1.33    -4.013368    1      
iter:   5  11:15:42         -1.82    -3.991776    1      
iter:   6  11:15:43         -2.58    -3.991478    1      
iter:   7  11:15:43         -2.76    -3.991495    1      
iter:   8  11:15:44         -3.26    -3.991487    1      
iter:   9  11:15:44         -3.41    -3.991483    1      
iter:  10  11:15:45         -3.66    -3.991482    1      
iter:  11  11:15:45         -4.59    -3.991482    1      
iter:  12  11:15:45         -4.76    -3.991482    1      
iter:  13  11:15:46         -5.20    -3.991482    1      
iter:  14  11:15:46         -5.42    -3.991482    1      
iter:  15  11:15:47         -5.80    -3.991482    1      
iter:  16  11:15:47         -6.51    -3.991482    1      

Converged after 16 iterations.

Dipole moment: (-0.000000, -0.000000, -1.631683) |e|*Ang

Energy contributions relative to reference atoms: (reference = -16973.559893)

Kinetic:        +10.450637
Potential:       -8.332253
External:        +0.000000
XC:              -6.143416
Entropy (-ST):   +0.000000
Local:           +0.033550
--------------------------
Free energy:     -3.991482
Extrapolated:    -3.991482

 Band  Eigenvalues  Occupancy
    0    -29.22060    2.00000
    1    -29.22060    2.00000
    2    -29.17207    2.00000
    3    -16.61555    2.00000
    4     -5.42611    2.00000
    5     -4.82923    2.00000
    6     -4.82923    2.00000
    7     -2.23062    0.00000
    8     -0.11716    0.00000
    9     -0.11716    0.00000
   10      0.74914    0.00000

Fermi level: -3.52993

Gap: 2.599 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.006     0.006   0.1% |
LCAO WFS Initialize:                 0.426     0.016   0.2% |
 Hamiltonian:                        0.410     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.021     0.021   0.3% |
  Hartree integrate/restrict:        0.010     0.010   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.209     0.005   0.1% |
   Communicate from 1D:              0.044     0.044   0.6% |
   Communicate from 2D:              0.043     0.043   0.6% |
   Communicate to 1D:                0.043     0.043   0.5% |
   Communicate to 2D:                0.043     0.043   0.6% |
   FFT 1D:                           0.010     0.010   0.1% |
   FFT 2D:                           0.022     0.022   0.3% |
  XC 3D grid:                        0.165     0.165   2.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           6.508     0.006   0.1% |
 Density:                            0.348     0.000   0.0% |
  Atomic density matrices:           0.012     0.012   0.1% |
  Mix:                               0.237     0.237   3.1% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.094     0.006   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.087     0.087   1.1% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        6.042     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.010     0.010   0.1% |
  Communicate:                       0.299     0.299   3.8% |-|
  Hartree integrate/restrict:        0.157     0.157   2.0% ||
  Poisson:                           3.161     0.070   0.9% |
   Communicate from 1D:              0.664     0.664   8.5% |--|
   Communicate from 2D:              0.649     0.649   8.3% |--|
   Communicate to 1D:                0.637     0.637   8.2% |--|
   Communicate to 2D:                0.656     0.656   8.4% |--|
   FFT 1D:                           0.150     0.150   1.9% ||
   FFT 2D:                           0.335     0.335   4.3% |-|
  XC 3D grid:                        2.371     2.371  30.5% |-----------|
  vbar:                              0.039     0.039   0.5% |
 LCAO eigensolver:                   0.112     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.010     0.010   0.1% |
  Orbital Layouts:                   0.005     0.005   0.1% |
  Potential matrix:                  0.095     0.095   1.2% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.168     0.168   2.2% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.669     0.669   8.6% |--|
-----------------------------------------------------------
Total:                                         7.779 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:15:47 2021
