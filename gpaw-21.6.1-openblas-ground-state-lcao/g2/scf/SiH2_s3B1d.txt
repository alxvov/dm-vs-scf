
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:12:07 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14.0
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Si.PBE.gz
  compensation charges: gauss, rc=0.33, lmax=2
  cutoffs: 1.86(filt), 2.06(core),
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -7911.324575

Spin-polarized calculation.
Magnetic moment: 2.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: <gpaw.occupations.FixMagneticMomentOccupationNumberCalculator object at 0x7f907b445d00> 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*112*100 grid
  Fine grid: 184*224*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*224*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 2, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 69.95 MiB
    Density: 43.53 MiB
      Arrays: 33.29 MiB
      Localized functions: 0.98 MiB
      Mixer: 9.25 MiB
    Hamiltonian: 24.81 MiB
      Arrays: 24.74 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.61 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.60 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 6
Number of valence electrons: 6
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           Si                    |  
 |         |          H                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 Si     7.000000    8.271862    7.758952    ( 0.0000,  0.0000,  2.0000)
   1 H      7.000000    9.543724    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.543724    0.000000   112     0.1477
  3. axis:    no     0.000000    0.000000   14.758952   100     0.1476

  Lengths:  14.000000  16.543724  14.758952
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1491

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:12:08                  -8.594424    1        +1.9987
iter:   2  11:12:09         -0.68    -8.562095    1        +1.9996
iter:   3  11:12:10         -0.85    -8.435896    1        +2.0001
iter:   4  11:12:11         -1.48    -8.439235    1        +2.0000
iter:   5  11:12:12         -1.91    -8.439689    1        +2.0000
iter:   6  11:12:12         -2.28    -8.439565    1        +2.0000
iter:   7  11:12:13         -2.46    -8.439649    1        +2.0000
iter:   8  11:12:14         -3.41    -8.439624    1        +2.0000
iter:   9  11:12:15         -3.64    -8.439652    1        +2.0000
iter:  10  11:12:16         -4.01    -8.439652    1        +2.0000
iter:  11  11:12:17         -4.13    -8.439652    1        +2.0000
iter:  12  11:12:17         -4.82    -8.439652    1        +2.0000
iter:  13  11:12:18         -5.38    -8.439652    1        +2.0000
iter:  14  11:12:19         -5.86    -8.439652    1        +2.0000
iter:  15  11:12:20         -6.17    -8.439652    1        +2.0000

Converged after 15 iterations.

Dipole moment: (0.000000, -0.000000, 0.012656) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.999999)
Local magnetic moments:
   0 Si ( 0.000000,  0.000000,  0.598758)
   1 H  ( 0.000000,  0.000000,  0.020619)
   2 H  ( 0.000000,  0.000000,  0.020619)

Energy contributions relative to reference atoms: (reference = -7911.324575)

Kinetic:         +9.377895
Potential:       -8.828338
External:        +0.000000
XC:              -8.973140
Entropy (-ST):   +0.000000
Local:           -0.016070
--------------------------
Free energy:     -8.439652
Extrapolated:    -8.439652

Spin contamination: 0.003905 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -13.05856    1.00000    -12.01611    1.00000
    1     -9.07711    1.00000     -8.63280    1.00000
    2     -6.18877    1.00000     -4.02348    0.00000
    3     -4.56660    1.00000     -2.39180    0.00000
    4      0.36214    0.00000      1.28257    0.00000
    5      0.92002    0.00000      2.13072    0.00000

Fermi levels: -2.10223, -6.32814

Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.003     0.003   0.0% |
LCAO WFS Initialize:                 0.829     0.032   0.2% |
 Hamiltonian:                        0.797     0.002   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.040     0.040   0.3% |
  Hartree integrate/restrict:        0.020     0.020   0.1% |
  Initialize Hamiltonian:            0.000     0.000   0.0% |
  Poisson:                           0.245     0.007   0.0% |
   Communicate from 1D:              0.052     0.052   0.4% |
   Communicate from 2D:              0.048     0.048   0.4% |
   Communicate to 1D:                0.053     0.053   0.4% |
   Communicate to 2D:                0.050     0.050   0.4% |
   FFT 1D:                           0.012     0.012   0.1% |
   FFT 2D:                           0.024     0.024   0.2% |
  XC 3D grid:                        0.487     0.487   3.6% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          11.712     0.013   0.1% |
 Density:                            0.598     0.000   0.0% |
  Atomic density matrices:           0.018     0.018   0.1% |
  Mix:                               0.480     0.480   3.6% ||
  Multipole moments:                 0.001     0.001   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.096     0.011   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.083     0.083   0.6% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       10.985     0.024   0.2% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.007     0.007   0.1% |
  Communicate:                       0.556     0.556   4.2% |-|
  Hartree integrate/restrict:        0.272     0.272   2.0% ||
  Poisson:                           3.417     0.078   0.6% |
   Communicate from 1D:              0.715     0.715   5.4% |-|
   Communicate from 2D:              0.669     0.669   5.0% |-|
   Communicate to 1D:                0.734     0.734   5.5% |-|
   Communicate to 2D:                0.714     0.714   5.3% |-|
   FFT 1D:                           0.168     0.168   1.3% ||
   FFT 2D:                           0.340     0.340   2.5% ||
  XC 3D grid:                        6.665     6.665  49.9% |-------------------|
  vbar:                              0.040     0.040   0.3% |
 LCAO eigensolver:                   0.116     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.017     0.017   0.1% |
  Orbital Layouts:                   0.008     0.008   0.1% |
  Potential matrix:                  0.089     0.089   0.7% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.075     0.075   0.6% |
mktci:                               0.001     0.001   0.0% |
Other:                               0.741     0.741   5.5% |-|
-----------------------------------------------------------
Total:                                        13.361 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:12:20 2021
