
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:20:11 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4235.414084

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 116*124*104 grid
  Fine grid: 232*248*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 232*248*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 73.32 MiB
    Density: 45.61 MiB
      Arrays: 36.05 MiB
      Localized functions: 1.44 MiB
      Mixer: 8.12 MiB
    Hamiltonian: 23.70 MiB
      Arrays: 23.58 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 4.02 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.16 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.84 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 14
Number of atomic orbitals: 102
Number of bands in calculation: 20
Number of valence electrons: 26
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .------------------------------------------.  
           /|                                          |  
          / |                                          |  
         /  |                                          |  
        /   |                                          |  
       /    |                                          |  
      /     |                                          |  
     /      |                                          |  
    /       |                                          |  
   /        |                                          |  
  /         |                                          |  
 *          |                                          |  
 |          |                                          |  
 |          |                 H                        |  
 |          |            H     H  H                    |  
 |          |             H   C                        |  
 |          |         H  C     H                       |  
 |          |             H                            |  
 |          |                                          |  
 |          .------------------------------------------.  
 |         /                                          /   
 |        /                                          /    
 |       /                                          /     
 |      /                                          /      
 |     /                                          /       
 |    /                                          /        
 |   /                                          /         
 |  /                                          /          
 | /                                          /           
 |/                                          /            
 *------------------------------------------*             

Positions:
   0 C      9.422390   11.043213    7.883614    ( 0.0000,  0.0000,  0.0000)
   1 C      9.422390    9.518665    7.883614    ( 0.0000,  0.0000,  0.0000)
   2 C      8.017228    8.926015    7.883614    ( 0.0000,  0.0000,  0.0000)
   3 C      8.017228    7.401467    7.883614    ( 0.0000,  0.0000,  0.0000)
   4 H     10.439618   11.444680    7.883614    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.000000    7.883614    ( 0.0000,  0.0000,  0.0000)
   6 H      8.907963   11.432702    8.767228    ( 0.0000,  0.0000,  0.0000)
   7 H      8.907963   11.432702    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      8.531655    7.011978    8.767228    ( 0.0000,  0.0000,  0.0000)
   9 H      8.531655    7.011978    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H      9.967516    9.149680    7.006045    ( 0.0000,  0.0000,  0.0000)
  11 H      9.967516    9.149680    8.761183    ( 0.0000,  0.0000,  0.0000)
  12 H      7.472102    9.295000    7.006045    ( 0.0000,  0.0000,  0.0000)
  13 H      7.472102    9.295000    8.761183    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.439618    0.000000    0.000000   116     0.1503
  2. axis:    no     0.000000   18.444680    0.000000   124     0.1487
  3. axis:    no     0.000000    0.000000   15.767228   104     0.1516

  Lengths:  17.439618  18.444680  15.767228
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1502

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:20:13                 -75.661594    1      
iter:   2  11:20:13         -0.67   -73.011419    1      
iter:   3  11:20:14         -0.86   -72.020933    1      
iter:   4  11:20:15         -1.21   -71.896896    1      
iter:   5  11:20:16         -2.12   -71.895472    1      
iter:   6  11:20:16         -2.40   -71.894933    1      
iter:   7  11:20:17         -2.59   -71.894543    1      
iter:   8  11:20:18         -3.24   -71.894531    1      
iter:   9  11:20:19         -3.48   -71.894529    1      
iter:  10  11:20:20         -3.88   -71.894529    1      
iter:  11  11:20:20         -4.16   -71.894529    1      
iter:  12  11:20:21         -4.42   -71.894529    1      
iter:  13  11:20:22         -4.85   -71.894529    1      
iter:  14  11:20:23         -5.20   -71.894529    1      
iter:  15  11:20:23         -5.55   -71.894529    1      
iter:  16  11:20:24         -5.76   -71.894529    1      
iter:  17  11:20:25         -6.15   -71.894529    1      

Converged after 17 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4235.414084)

Kinetic:        +58.037427
Potential:      -65.894323
External:        +0.000000
XC:             -64.139104
Entropy (-ST):   +0.000000
Local:           +0.101470
--------------------------
Free energy:    -71.894529
Extrapolated:   -71.894529

 Band  Eigenvalues  Occupancy
    0    -19.64326    2.00000
    1    -17.82750    2.00000
    2    -15.41539    2.00000
    3    -14.03381    2.00000
    4    -11.38042    2.00000
    5    -10.30328    2.00000
    6    -10.14112    2.00000
    7     -9.87254    2.00000
    8     -8.79186    2.00000
    9     -8.29028    2.00000
   10     -7.94163    2.00000
   11     -7.62802    2.00000
   12     -7.57413    2.00000
   13      1.84560    0.00000
   14      2.34701    0.00000
   15      2.63073    0.00000
   16      2.87301    0.00000
   17      3.89245    0.00000
   18      3.98569    0.00000
   19      4.16681    0.00000

Fermi level: -2.86427

Gap: 9.420 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.010     0.010   0.1% |
LCAO WFS Initialize:                 0.720     0.025   0.2% |
 Hamiltonian:                        0.696     0.000   0.0% |
  Atomic:                            0.031     0.015   0.1% |
   XC Correction:                    0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:     0.004     0.004   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.018     0.018   0.1% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.356     0.009   0.1% |
   Communicate from 1D:              0.072     0.072   0.5% |
   Communicate from 2D:              0.068     0.068   0.5% |
   Communicate to 1D:                0.066     0.066   0.5% |
   Communicate to 2D:                0.076     0.076   0.5% |
   FFT 1D:                           0.026     0.026   0.2% |
   FFT 2D:                           0.038     0.038   0.3% |
  XC 3D grid:                        0.280     0.280   1.9% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.012     0.012   0.1% |
SCF-cycle:                          12.500     0.007   0.0% |
 Density:                            0.879     0.000   0.0% |
  Atomic density matrices:           0.092     0.092   0.6% |
  Mix:                               0.397     0.397   2.8% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.386     0.012   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.373     0.373   2.6% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       11.079     0.001   0.0% |
  Atomic:                            0.501     0.248   1.7% ||
   XC Correction:                    0.254     0.254   1.8% ||
  Calculate atomic Hamiltonians:     0.059     0.059   0.4% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.293     0.293   2.0% ||
  Poisson:                           5.677     0.131   0.9% |
   Communicate from 1D:              1.171     1.171   8.1% |--|
   Communicate from 2D:              1.086     1.086   7.6% |--|
   Communicate to 1D:                1.060     1.060   7.4% |--|
   Communicate to 2D:                1.209     1.209   8.4% |--|
   FFT 1D:                           0.409     0.409   2.8% ||
   FFT 2D:                           0.611     0.611   4.2% |-|
  XC 3D grid:                        4.477     4.477  31.1% |-----------|
  vbar:                              0.069     0.069   0.5% |
 LCAO eigensolver:                   0.535     0.001   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.003     0.003   0.0% |
  Distribute overlap matrix:         0.051     0.051   0.4% |
  Orbital Layouts:                   0.025     0.025   0.2% |
  Potential matrix:                  0.454     0.454   3.2% ||
ST tci:                              0.003     0.003   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.075     0.075   0.5% |
mktci:                               0.002     0.002   0.0% |
Other:                               1.051     1.051   7.3% |--|
-----------------------------------------------------------
Total:                                        14.373 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:20:25 2021
