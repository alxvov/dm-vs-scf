
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:24:33 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2053.342463

Spin-polarized calculation.
Magnetic moment: 1.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: <gpaw.occupations.FixMagneticMomentOccupationNumberCalculator object at 0x7f908108e130> 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*100 grid
  Fine grid: 184*184*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 55.88 MiB
    Density: 35.15 MiB
      Arrays: 27.29 MiB
      Localized functions: 0.29 MiB
      Mixer: 7.57 MiB
    Hamiltonian: 20.30 MiB
      Arrays: 20.28 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.02 MiB
    Wavefunctions: 0.44 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.00 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.43 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 18
Number of bands in calculation: 5
Number of valence electrons: 7
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            O                    |  
 |        |            H                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 O      7.000000    7.000000    7.979070    ( 0.0000,  0.0000,  0.5000)
   1 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.5000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   14.979070   100     0.1498

  Lengths:  14.000000  14.000000  14.979070
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1514

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:24:34                  -6.721498    1        +0.9996
iter:   2  11:24:35         -0.86    -7.041786    1        +0.9999
iter:   3  11:24:36         -1.01    -7.004930    1        +1.0000
iter:   4  11:24:36         -1.47    -6.983841    1        +1.0000
iter:   5  11:24:37         -2.51    -6.984050    1        +1.0000
iter:   6  11:24:38         -2.71    -6.984057    1        +1.0000
iter:   7  11:24:38         -2.87    -6.984053    1        +1.0000
iter:   8  11:24:39         -3.16    -6.984068    1        +1.0000
iter:   9  11:24:40         -3.05    -6.984052    1        +1.0000
iter:  10  11:24:40         -3.21    -6.984052    1        +1.0000
iter:  11  11:24:41         -3.40    -6.984053    1        +1.0000
iter:  12  11:24:42         -3.47    -6.984052    1        +1.0000
iter:  13  11:24:42         -3.53    -6.984053    1        +1.0000
iter:  14  11:24:43         -3.43    -6.984052    1        +1.0000
iter:  15  11:24:44         -3.73    -6.984051    1        +1.0000
iter:  16  11:24:44         -4.02    -6.984051    1        +1.0000
iter:  17  11:24:45         -4.01    -6.984050    1        +1.0000
iter:  18  11:24:46         -3.82    -6.984053    1        +1.0000
iter:  19  11:24:46         -3.28    -6.984050    1        +1.0000
iter:  20  11:24:47         -3.80    -6.984049    1        +1.0000
iter:  21  11:24:48         -4.03    -6.984050    1        +1.0000
iter:  22  11:24:48         -3.88    -6.984049    1        +1.0000
iter:  23  11:24:49         -3.81    -6.984048    1        +1.0000
iter:  24  11:24:50         -3.91    -6.984051    1        +1.0000
iter:  25  11:24:50         -3.60    -6.984048    1        +1.0000
iter:  26  11:24:51         -3.63    -6.984048    1        +1.0000
iter:  27  11:24:52         -3.67    -6.984048    1        +1.0000
iter:  28  11:24:52         -3.74    -6.984047    1        +1.0000
iter:  29  11:24:53         -3.91    -6.984047    1        +1.0000
iter:  30  11:24:54         -4.76    -6.984047    1        +1.0000
iter:  31  11:24:54         -3.78    -6.984047    1        +1.0000
iter:  32  11:24:55         -3.60    -6.984047    1        +1.0000
iter:  33  11:24:56         -3.67    -6.984048    1        +1.0000
iter:  34  11:24:56         -3.53    -6.984046    1        +1.0000
iter:  35  11:24:57         -4.47    -6.984046    1        +1.0000
iter:  36  11:24:58         -5.82    -6.984046    1        +1.0000
iter:  37  11:24:58         -4.62    -6.984046    1        +1.0000
iter:  38  11:24:59         -3.62    -6.984046    1        +1.0000
iter:  39  11:25:00         -3.69    -6.984047    1        +1.0000
iter:  40  11:25:00         -3.43    -6.984045    1        +1.0000
iter:  41  11:25:01         -5.01    -6.984045    1        +1.0000
iter:  42  11:25:02         -5.85    -6.984045    1        +1.0000
iter:  43  11:25:02         -4.25    -6.984045    1        +1.0000
iter:  44  11:25:03         -3.62    -6.984045    1        +1.0000
iter:  45  11:25:04         -3.61    -6.984045    1        +1.0000
iter:  46  11:25:04         -3.59    -6.984044    1        +1.0000
iter:  47  11:25:05         -3.82    -6.984043    1        +1.0000
iter:  48  11:25:06         -4.30    -6.984043    1        +1.0000
iter:  49  11:25:06         -5.52    -6.984043    1        +1.0000
iter:  50  11:25:07         -5.10    -6.984044    1        +1.0000
iter:  51  11:25:08         -3.44    -6.984043    1        +1.0000
iter:  52  11:25:08         -3.61    -6.984043    1        +1.0000
iter:  53  11:25:09         -3.61    -6.984043    1        +1.0000
iter:  54  11:25:10         -3.53    -6.984042    1        +1.0000
iter:  55  11:25:10         -4.05    -6.984042    1        +1.0000
iter:  56  11:25:11         -4.29    -6.984046    1        +1.0000
iter:  57  11:25:12         -3.32    -6.984043    1        +1.0000
iter:  58  11:25:12         -3.60    -6.984042    1        +1.0000
iter:  59  11:25:13         -4.04    -6.984041    1        +1.0000
iter:  60  11:25:14         -4.32    -6.984042    1        +1.0000
iter:  61  11:25:14         -4.24    -6.984041    1        +1.0000
iter:  62  11:25:15         -3.99    -6.984041    1        +1.0000
iter:  63  11:25:16         -3.97    -6.984069    1        +1.0000
iter:  64  11:25:16         -2.82    -6.984040    1        +1.0000
iter:  65  11:25:17         -3.76    -6.984041    1        +1.0000
iter:  66  11:25:18         -3.38    -6.984039    1        +1.0000
iter:  67  11:25:18         -3.81    -6.984039    1        +1.0000
iter:  68  11:25:19         -3.95    -6.984039    1        +1.0000
iter:  69  11:25:20         -3.83    -6.984039    1        +1.0000
iter:  70  11:25:20         -5.77    -6.984039    1        +1.0000
iter:  71  11:25:21         -4.42    -6.984038    1        +1.0000
iter:  72  11:25:22         -4.04    -6.984040    1        +1.0000
iter:  73  11:25:22         -3.35    -6.984038    1        +1.0000
iter:  74  11:25:23         -3.77    -6.984037    1        +1.0000
iter:  75  11:25:24         -4.45    -6.984038    1        +1.0000
iter:  76  11:25:24         -4.79    -6.984040    1        +1.0000
iter:  77  11:25:25         -3.26    -6.984037    1        +1.0000
iter:  78  11:25:26         -3.66    -6.984037    1        +1.0000
iter:  79  11:25:26         -3.66    -6.984037    1        +1.0000
iter:  80  11:25:27         -3.89    -6.984037    1        +1.0000
iter:  81  11:25:28         -3.72    -6.984036    1        +1.0000
iter:  82  11:25:28         -4.51    -6.984037    1        +1.0000
iter:  83  11:25:29         -4.81    -6.984038    1        +1.0000
iter:  84  11:25:30         -3.69    -6.984036    1        +1.0000
iter:  85  11:25:30         -3.56    -6.984036    1        +1.0000
iter:  86  11:25:31         -3.70    -6.984036    1        +1.0000
iter:  87  11:25:32         -3.75    -6.984036    1        +1.0000
iter:  88  11:25:32         -4.03    -6.984036    1        +1.0000
iter:  89  11:25:33         -3.71    -6.984036    1        +1.0000
iter:  90  11:25:34         -3.80    -6.984036    1        +1.0000
iter:  91  11:25:35         -3.64    -6.984035    1        +1.0000
iter:  92  11:25:35         -4.02    -6.984035    1        +1.0000
iter:  93  11:25:36         -5.12    -6.984035    1        +1.0000
iter:  94  11:25:37         -5.56    -6.984037    1        +1.0000
iter:  95  11:25:37         -3.71    -6.984035    1        +1.0000
iter:  96  11:25:38         -3.64    -6.984035    1        +1.0000
iter:  97  11:25:39         -3.67    -6.984035    1        +1.0000
iter:  98  11:25:39         -3.71    -6.984035    1        +1.0000
iter:  99  11:25:40         -3.66    -6.984034    1        +1.0000
iter: 100  11:25:41         -4.47    -6.984034    1        +1.0000
iter: 101  11:25:41         -5.87    -6.984034    1        +1.0000
iter: 102  11:25:42         -5.88    -6.984108    1        +1.0000
iter: 103  11:25:43         -2.63    -6.984034    1        +1.0000
iter: 104  11:25:43         -3.66    -6.984034    1        +1.0000
iter: 105  11:25:44         -3.66    -6.984034    1        +1.0000
iter: 106  11:25:45         -3.66    -6.984034    1        +1.0000
iter: 107  11:25:45         -3.68    -6.984033    1        +1.0000
iter: 108  11:25:46         -4.48    -6.984033    1        +1.0000
iter: 109  11:25:47         -4.72    -6.984033    1        +1.0000
iter: 110  11:25:47         -4.23    -6.984033    1        +1.0000
iter: 111  11:25:48         -4.42    -6.984033    1        +1.0000
iter: 112  11:25:49         -4.48    -6.984033    1        +1.0000
iter: 113  11:25:49         -4.48    -6.984032    1        +1.0000
iter: 114  11:25:50         -4.05    -6.984032    1        +1.0000
iter: 115  11:25:51         -3.82    -6.984032    1        +1.0000
iter: 116  11:25:51         -3.66    -6.984032    1        +1.0000
iter: 117  11:25:52         -3.66    -6.984032    1        +1.0000
iter: 118  11:25:53         -3.65    -6.984032    1        +1.0000
iter: 119  11:25:53         -3.65    -6.984032    1        +1.0000
iter: 120  11:25:54         -3.64    -6.984032    1        +1.0000
iter: 121  11:25:55         -3.95    -6.984032    1        +1.0000
iter: 122  11:25:55         -4.09    -6.984032    1        +1.0000
iter: 123  11:25:56         -4.26    -6.984032    1        +1.0000
iter: 124  11:25:57         -3.51    -6.984031    1        +1.0000
iter: 125  11:25:57         -3.72    -6.984032    1        +1.0000
iter: 126  11:25:58         -3.55    -6.984031    1        +1.0000
iter: 127  11:25:59         -3.95    -6.984030    1        +1.0000
iter: 128  11:25:59         -4.15    -6.984030    1        +1.0000
iter: 129  11:26:00         -4.56    -6.984031    1        +1.0000
iter: 130  11:26:01         -4.21    -6.984032    1        +1.0000
iter: 131  11:26:01         -3.36    -6.984030    1        +1.0000
iter: 132  11:26:02         -3.69    -6.984030    1        +1.0000
iter: 133  11:26:03         -3.73    -6.984030    1        +1.0000
iter: 134  11:26:03         -3.80    -6.984030    1        +1.0000
iter: 135  11:26:04         -4.09    -6.984030    1        +1.0000
iter: 136  11:26:05         -4.72    -6.984029    1        +1.0000
iter: 137  11:26:05         -4.29    -6.984029    1        +1.0000
iter: 138  11:26:06         -4.14    -6.984034    1        +1.0000
iter: 139  11:26:07         -3.19    -6.984030    1        +1.0000
iter: 140  11:26:07         -3.51    -6.984029    1        +1.0000
iter: 141  11:26:08         -3.89    -6.984029    1        +1.0000
iter: 142  11:26:09         -3.77    -6.984029    1        +1.0000
iter: 143  11:26:09         -3.81    -6.984030    1        +1.0000
iter: 144  11:26:10         -3.80    -6.984267    1        +1.0000
iter: 145  11:26:11         -2.43    -6.984038    1        +1.0000
iter: 146  11:26:11         -3.13    -6.984034    1        +1.0000
iter: 147  11:26:12         -3.38    -6.984030    1        +1.0000
iter: 148  11:26:13         -4.31    -6.984030    1        +1.0000
iter: 149  11:26:13         -4.27    -6.984062    1        +1.0000
iter: 150  11:26:14         -2.80    -6.984030    1        +1.0000
iter: 151  11:26:15         -3.70    -6.984030    1        +1.0000
iter: 152  11:26:15         -3.69    -6.984029    1        +1.0000
iter: 153  11:26:16         -4.24    -6.984030    1        +1.0000
iter: 154  11:26:17         -3.66    -6.984029    1        +1.0000
iter: 155  11:26:17         -4.87    -6.984029    1        +1.0000
iter: 156  11:26:18         -6.43    -6.984029    1        +1.0000

Converged after 156 iterations.

Dipole moment: (0.000000, 0.000000, -0.319048) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000007)
Local magnetic moments:
   0 O  ( 0.000000,  0.000000,  0.642669)
   1 H  ( 0.000000,  0.000000, -0.006001)

Energy contributions relative to reference atoms: (reference = -2053.342463)

Kinetic:         +9.252811
Potential:       -8.137939
External:        +0.000000
XC:              -8.171412
Entropy (-ST):   +0.000000
Local:           +0.072512
--------------------------
Free energy:     -6.984029
Extrapolated:    -6.984029

Spin contamination: 0.020330 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -24.80708    1.00000    -23.24399    1.00000
    1    -11.00115    1.00000    -10.34953    1.00000
    2     -9.25571    1.00000     -6.55548    1.00000
    3     -7.20981    1.00000     -5.74165    0.00000
    4      1.41709    0.00000      1.70554    0.00000

Fermi levels: -2.89636, -6.14857

Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.001     0.001   0.0% |
LCAO WFS Initialize:                 0.667     0.027   0.0% |
 Hamiltonian:                        0.640     0.001   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.030     0.030   0.0% |
  Hartree integrate/restrict:        0.017     0.017   0.0% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.193     0.004   0.0% |
   Communicate from 1D:              0.041     0.041   0.0% |
   Communicate from 2D:              0.040     0.040   0.0% |
   Communicate to 1D:                0.039     0.039   0.0% |
   Communicate to 2D:                0.041     0.041   0.0% |
   FFT 1D:                           0.008     0.008   0.0% |
   FFT 2D:                           0.020     0.020   0.0% |
  XC 3D grid:                        0.395     0.395   0.4% |
  vbar:                              0.002     0.002   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                         103.718     0.118   0.1% |
 Density:                            4.832     0.003   0.0% |
  Atomic density matrices:           0.178     0.178   0.2% |
  Mix:                               4.304     4.304   4.1% |-|
  Multipole moments:                 0.013     0.013   0.0% |
  Normalize:                         0.029     0.029   0.0% |
  Pseudo density:                    0.304     0.101   0.1% |
   Calculate density matrix:         0.011     0.011   0.0% |
   Construct density:                0.190     0.190   0.2% |
   Symmetrize density:               0.002     0.002   0.0% |
 Hamiltonian:                       98.342     0.221   0.2% |
  Atomic:                            0.032     0.031   0.0% |
   XC Correction:                    0.001     0.001   0.0% |
  Calculate atomic Hamiltonians:     0.052     0.052   0.0% |
  Communicate:                       4.688     4.688   4.5% |-|
  Hartree integrate/restrict:        2.668     2.668   2.5% ||
  Poisson:                          30.838     0.667   0.6% |
   Communicate from 1D:              6.500     6.500   6.2% |-|
   Communicate from 2D:              6.180     6.180   5.9% |-|
   Communicate to 1D:                6.676     6.676   6.4% |--|
   Communicate to 2D:                6.712     6.712   6.4% |--|
   FFT 1D:                           1.247     1.247   1.2% |
   FFT 2D:                           2.856     2.856   2.7% ||
  XC 3D grid:                       59.484    59.484  56.6% |----------------------|
  vbar:                              0.359     0.359   0.3% |
 LCAO eigensolver:                   0.426     0.012   0.0% |
  Calculate projections:             0.002     0.002   0.0% |
  DenseAtomicCorrection:             0.007     0.007   0.0% |
  Distribute overlap matrix:         0.141     0.141   0.1% |
  Orbital Layouts:                   0.064     0.064   0.1% |
  Potential matrix:                  0.199     0.199   0.2% |
ST tci:                              0.000     0.000   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.074     0.074   0.1% |
mktci:                               0.001     0.001   0.0% |
Other:                               0.597     0.597   0.6% |
-----------------------------------------------------------
Total:                                       105.059 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:26:18 2021
