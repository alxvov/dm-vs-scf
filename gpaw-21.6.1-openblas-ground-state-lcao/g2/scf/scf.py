from ase.collections import g2
from gpaw import GPAW, LCAO, FermiDirac, ConvergenceError
from ase.parallel import parprint
from gpaw.utilities.memory import maxrss
import time
from gpaw.mpi import world

xc = 'PBE'
mode = LCAO()

file2write = open('scf-g2-results.txt', 'w')

for name in g2.names:

    atoms = g2[name]
    if len(atoms) == 1:
        continue
    atoms.center(vacuum=7.0)
    calc = GPAW(xc=xc, h=0.15,
                convergence={'density': 1.0e-6, 'eigenstates': 1.0e10}, # ignore eigenstates convergence as it is done in scf
                maxiter=333,
                basis='dzp',
                mode=mode,
                txt=name + '.txt',
                occupations=FermiDirac(width=0.0, fixmagmom=True),
                symmetry='off'
                )

    atoms.calc = calc

    try:
        t1 = time.time()
        e = atoms.get_potential_energy()
        t2 = time.time()
        steps = atoms.calc.get_number_of_iterations()
        memory = maxrss() / 1024.0 ** 2
        parprint(name +
                 "\t{}\t{}\t{}\t{:.3f}".format(steps, e,
                                               t2-t1, memory), file=file2write, flush=True)  # s,MB
    except ConvergenceError:
        parprint(name +
                 "\t{}\t{}\t{}\t{}".format(None, None, None, None), file=file2write, flush=True)
    calc = None
    atoms = None

file2write.close()
