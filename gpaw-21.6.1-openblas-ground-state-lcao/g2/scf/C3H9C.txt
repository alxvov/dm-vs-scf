
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:10:00 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4222.923922

Spin-polarized calculation.
Magnetic moment: 1.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: <gpaw.occupations.FixMagneticMomentOccupationNumberCalculator object at 0x7f907b5156a0> 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 120*116*104 grid
  Fine grid: 240*232*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*232*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 1, 0]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 463.51 MiB
  Calculator: 100.08 MiB
    Density: 61.36 MiB
      Arrays: 46.94 MiB
      Localized functions: 1.34 MiB
      Mixer: 13.09 MiB
    Hamiltonian: 34.98 MiB
      Arrays: 34.88 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 3.73 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.14 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.57 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 13
Number of atomic orbitals: 97
Number of bands in calculation: 20
Number of valence electrons: 25
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .--------------------------------------------.  
           /|                                            |  
          / |                                            |  
         /  |                                            |  
        /   |                                            |  
       /    |                                            |  
      /     |                                            |  
     /      |                                            |  
    /       |                                            |  
   /        |                                            |  
  /         |                                            |  
 *          |                                            |  
 |          |                                            |  
 |          |               H   H                        |  
 |          |           H    CC   H                      |  
 |          |            H    HHC                        |  
 |          |                                            |  
 |          |            H      H                        |  
 |          |                                            |  
 |          .--------------------------------------------.  
 |         /                                            /   
 |        /                                            /    
 |       /                                            /     
 |      /                                            /      
 |     /                                            /       
 |    /                                            /        
 |   /                                            /         
 |  /                                            /          
 | /                                            /           
 |/                                            /            
 *--------------------------------------------*             

Positions:
   0 C      9.128607    8.741086    8.285721    ( 0.0000,  0.0000,  1.0000)
   1 C      9.128607   10.219273    8.072926    ( 0.0000,  0.0000,  0.0000)
   2 C     10.408754    8.001993    8.072926    ( 0.0000,  0.0000,  0.0000)
   3 C      7.848460    8.001993    8.072926    ( 0.0000,  0.0000,  0.0000)
   4 H      9.128607   10.472582    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      8.241564   10.686855    8.511357    ( 0.0000,  0.0000,  0.0000)
   6 H     10.015650   10.686855    8.511357    ( 0.0000,  0.0000,  0.0000)
   7 H     10.628127    7.875338    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H     11.257214    8.536403    8.511357    ( 0.0000,  0.0000,  0.0000)
   9 H     10.370171    7.000000    8.511357    ( 0.0000,  0.0000,  0.0000)
  10 H      7.629087    7.875338    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.887043    7.000000    8.511357    ( 0.0000,  0.0000,  0.0000)
  12 H      7.000000    8.536403    8.511357    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.257214    0.000000    0.000000   120     0.1521
  2. axis:    no     0.000000   17.686855    0.000000   116     0.1525
  3. axis:    no     0.000000    0.000000   15.511357   104     0.1491

  Lengths:  18.257214  17.686855  15.511357
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1512

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:10:02                 -69.829420    1        +0.9982
iter:   2  11:10:03         -0.69   -67.557508    1        +0.9995
iter:   3  11:10:04         -0.88   -66.707500    1        +1.0000
iter:   4  11:10:06         -1.21   -66.592334    1        +1.0000
iter:   5  11:10:07         -2.17   -66.592530    1        +1.0000
iter:   6  11:10:08         -2.41   -66.592311    1        +1.0000
iter:   7  11:10:09         -2.65   -66.592084    1        +1.0000
iter:   8  11:10:10         -3.20   -66.592060    1        +1.0000
iter:   9  11:10:11         -3.55   -66.592057    1        +1.0000
iter:  10  11:10:13         -3.77   -66.592058    1        +1.0000
iter:  11  11:10:14         -3.98   -66.592058    1        +1.0000
iter:  12  11:10:15         -4.23   -66.592058    1        +1.0000
iter:  13  11:10:16         -4.62   -66.592058    1        +1.0000
iter:  14  11:10:17         -4.87   -66.592058    1        +1.0000
iter:  15  11:10:18         -5.14   -66.592058    1        +1.0000
iter:  16  11:10:20         -5.43   -66.592058    1        +1.0000
iter:  17  11:10:21         -5.63   -66.592058    1        +1.0000
iter:  18  11:10:22         -5.92   -66.592058    1        +1.0000
iter:  19  11:10:23         -6.23   -66.592058    1        +1.0000

Converged after 19 iterations.

Dipole moment: (-0.000000, -0.000092, -0.049154) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999994)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.305969)
   1 C  ( 0.000000,  0.000000, -0.012344)
   2 C  ( 0.000000,  0.000000, -0.012341)
   3 C  ( 0.000000,  0.000000, -0.012341)
   4 H  ( 0.000000,  0.000000,  0.022156)
   5 H  ( 0.000000,  0.000000,  0.004618)
   6 H  ( 0.000000,  0.000000,  0.004618)
   7 H  ( 0.000000,  0.000000,  0.022158)
   8 H  ( 0.000000,  0.000000,  0.004618)
   9 H  ( 0.000000,  0.000000,  0.004619)
  10 H  ( 0.000000,  0.000000,  0.022158)
  11 H  ( 0.000000,  0.000000,  0.004619)
  12 H  ( 0.000000,  0.000000,  0.004618)

Energy contributions relative to reference atoms: (reference = -4222.923922)

Kinetic:        +54.127278
Potential:      -60.517445
External:        +0.000000
XC:             -60.309971
Entropy (-ST):   +0.000000
Local:           +0.108079
--------------------------
Free energy:    -66.592058
Extrapolated:   -66.592058

Spin contamination: 0.061522 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -19.81889    1.00000    -19.43735    1.00000
    1    -16.96576    1.00000    -16.90782    1.00000
    2    -16.96547    1.00000    -16.90753    1.00000
    3    -12.97879    1.00000    -12.56969    1.00000
    4    -10.73614    1.00000    -10.52638    1.00000
    5    -10.62366    1.00000    -10.52618    1.00000
    6    -10.62353    1.00000    -10.34420    1.00000
    7     -9.24888    1.00000     -9.13653    1.00000
    8     -9.24881    1.00000     -9.13644    1.00000
    9     -8.71919    1.00000     -8.70065    1.00000
   10     -8.13581    1.00000     -8.00511    1.00000
   11     -8.13569    1.00000     -8.00497    1.00000
   12     -3.66411    1.00000     -1.67305    0.00000
   13      1.63884    0.00000      1.70248    0.00000
   14      3.02043    0.00000      3.06621    0.00000
   15      3.02057    0.00000      3.06625    0.00000
   16      3.27214    0.00000      3.35429    0.00000
   17      3.27243    0.00000      3.35480    0.00000
   18      3.63540    0.00000      3.82639    0.00000
   19      4.38788    0.00000      4.50137    0.00000

Fermi levels: -1.01263, -4.83901

Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.009     0.009   0.0% |
LCAO WFS Initialize:                 1.071     0.052   0.2% |
 Hamiltonian:                        1.019     0.003   0.0% |
  Atomic:                            0.035     0.005   0.0% |
   XC Correction:                    0.030     0.030   0.1% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.013     0.013   0.1% |
  Hartree integrate/restrict:        0.033     0.033   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.239     0.009   0.0% |
   Communicate from 1D:              0.019     0.019   0.1% |
   Communicate from 2D:              0.065     0.065   0.3% |
   Communicate to 1D:                0.064     0.064   0.3% |
   Communicate to 2D:                0.022     0.022   0.1% |
   FFT 1D:                           0.021     0.021   0.1% |
   FFT 2D:                           0.038     0.038   0.2% |
  XC 3D grid:                        0.687     0.687   2.9% ||
  vbar:                              0.006     0.006   0.0% |
P tci:                               0.003     0.003   0.0% |
SCF-cycle:                          21.193     0.020   0.1% |
 Density:                            1.938     0.000   0.0% |
  Atomic density matrices:           0.331     0.331   1.4% ||
  Mix:                               0.973     0.973   4.2% |-|
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.006     0.006   0.0% |
  Pseudo density:                    0.626     0.025   0.1% |
   Calculate density matrix:         0.002     0.002   0.0% |
   Construct density:                0.599     0.599   2.6% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       18.167     0.044   0.2% |
  Atomic:                            0.635     0.092   0.4% |
   XC Correction:                    0.543     0.543   2.3% ||
  Calculate atomic Hamiltonians:     0.042     0.042   0.2% |
  Communicate:                       0.234     0.234   1.0% |
  Hartree integrate/restrict:        0.562     0.562   2.4% ||
  Poisson:                           4.300     0.155   0.7% |
   Communicate from 1D:              0.347     0.347   1.5% ||
   Communicate from 2D:              1.195     1.195   5.1% |-|
   Communicate to 1D:                1.153     1.153   4.9% |-|
   Communicate to 2D:                0.405     0.405   1.7% ||
   FFT 1D:                           0.374     0.374   1.6% ||
   FFT 2D:                           0.671     0.671   2.9% ||
  XC 3D grid:                       12.277    12.277  52.4% |--------------------|
  vbar:                              0.073     0.073   0.3% |
 LCAO eigensolver:                   1.068     0.002   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.004     0.004   0.0% |
  Distribute overlap matrix:         0.283     0.283   1.2% |
  Orbital Layouts:                   0.054     0.054   0.2% |
  Potential matrix:                  0.724     0.724   3.1% ||
ST tci:                              0.003     0.003   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.076     0.076   0.3% |
mktci:                               0.002     0.002   0.0% |
Other:                               1.059     1.059   4.5% |-|
-----------------------------------------------------------
Total:                                        23.416 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:10:23 2021
