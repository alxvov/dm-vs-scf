
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:39:26 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3132.844995

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*120 grid
  Fine grid: 208*208*240 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 61.82 MiB
    Density: 39.08 MiB
      Arrays: 31.24 MiB
      Localized functions: 0.81 MiB
      Mixer: 7.02 MiB
    Hamiltonian: 20.50 MiB
      Arrays: 20.43 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 2.24 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.18 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 59
Number of bands in calculation: 14
Number of valence electrons: 16
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |             H                        |  
 |         |             C                        |  
 |         |             C                        |  
 |         |                                      |  
 |         |             CH                       |  
 |         |           H   H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.884337    7.510572    8.851114    ( 0.0000,  0.0000,  0.0000)
   1 C      7.884337    7.510572   10.069297    ( 0.0000,  0.0000,  0.0000)
   2 C      7.884337    7.510572    7.389691    ( 0.0000,  0.0000,  0.0000)
   3 H      7.884337    7.510572   11.135054    ( 0.0000,  0.0000,  0.0000)
   4 H      7.884337    8.531717    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      8.768674    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.768674    0.000000    0.000000   104     0.1516
  2. axis:    no     0.000000   15.531717    0.000000   104     0.1493
  3. axis:    no     0.000000    0.000000   18.135054   120     0.1511

  Lengths:  15.768674  15.531717  18.135054
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:39:28                 -40.548592    1      
iter:   2  11:39:28         -0.72   -39.079901    1      
iter:   3  11:39:29         -0.90   -38.452138    1      
iter:   4  11:39:29         -1.28   -38.418226    1      
iter:   5  11:39:30         -1.82   -38.417029    1      
iter:   6  11:39:31         -2.34   -38.416671    1      
iter:   7  11:39:31         -2.56   -38.416501    1      
iter:   8  11:39:32         -3.08   -38.416482    1      
iter:   9  11:39:33         -3.37   -38.416473    1      
iter:  10  11:39:33         -3.82   -38.416472    1      
iter:  11  11:39:34         -4.27   -38.416472    1      
iter:  12  11:39:34         -4.61   -38.416472    1      
iter:  13  11:39:35         -4.92   -38.416472    1      
iter:  14  11:39:36         -5.30   -38.416472    1      
iter:  15  11:39:36         -5.96   -38.416472    1      
iter:  16  11:39:37         -6.31   -38.416472    1      

Converged after 16 iterations.

Dipole moment: (-0.000000, 0.000041, -0.180369) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3132.844995)

Kinetic:        +29.016690
Potential:      -33.098975
External:        +0.000000
XC:             -34.427555
Entropy (-ST):   +0.000000
Local:           +0.093367
--------------------------
Free energy:    -38.416472
Extrapolated:   -38.416472

 Band  Eigenvalues  Occupancy
    0    -19.24740    2.00000
    1    -17.21460    2.00000
    2    -12.87679    2.00000
    3    -10.97955    2.00000
    4    -10.52677    2.00000
    5    -10.52654    2.00000
    6     -6.25589    2.00000
    7     -6.25583    2.00000
    8      0.55106    0.00000
    9      0.55111    0.00000
   10      2.00737    0.00000
   11      2.96835    0.00000
   12      3.97027    0.00000
   13      3.97079    0.00000

Fermi level: -2.85238

Gap: 6.807 eV
Transition (v -> c):
  (s=0, k=0, n=7, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.006     0.006   0.1% |
LCAO WFS Initialize:                 0.609     0.019   0.2% |
 Hamiltonian:                        0.590     0.000   0.0% |
  Atomic:                            0.010     0.010   0.1% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.007     0.007   0.1% |
  Hartree integrate/restrict:        0.014     0.014   0.1% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.309     0.007   0.1% |
   Communicate from 1D:              0.064     0.064   0.6% |
   Communicate from 2D:              0.061     0.061   0.6% |
   Communicate to 1D:                0.063     0.063   0.6% |
   Communicate to 2D:                0.066     0.066   0.6% |
   FFT 1D:                           0.014     0.014   0.1% |
   FFT 2D:                           0.033     0.033   0.3% |
  XC 3D grid:                        0.243     0.243   2.2% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                           9.498     0.006   0.1% |
 Density:                            0.503     0.000   0.0% |
  Atomic density matrices:           0.020     0.020   0.2% |
  Mix:                               0.297     0.297   2.7% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.181     0.009   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.172     0.172   1.5% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        8.752     0.001   0.0% |
  Atomic:                            0.158     0.158   1.4% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.017     0.017   0.2% |
  Communicate:                       0.102     0.102   0.9% |
  Hartree integrate/restrict:        0.197     0.197   1.8% ||
  Poisson:                           4.638     0.109   1.0% |
   Communicate from 1D:              0.953     0.953   8.6% |--|
   Communicate from 2D:              0.913     0.913   8.2% |--|
   Communicate to 1D:                0.982     0.982   8.9% |---|
   Communicate to 2D:                0.973     0.973   8.8% |---|
   FFT 1D:                           0.213     0.213   1.9% ||
   FFT 2D:                           0.494     0.494   4.5% |-|
  XC 3D grid:                        3.582     3.582  32.3% |------------|
  vbar:                              0.056     0.056   0.5% |
 LCAO eigensolver:                   0.237     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.016     0.016   0.1% |
  Orbital Layouts:                   0.011     0.011   0.1% |
  Potential matrix:                  0.207     0.207   1.9% ||
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.074     0.074   0.7% |
mktci:                               0.001     0.001   0.0% |
Other:                               0.898     0.898   8.1% |--|
-----------------------------------------------------------
Total:                                        11.090 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:39:37 2021
