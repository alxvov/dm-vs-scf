
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:30:46 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14.0
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Si.PBE.gz
  compensation charges: gauss, rc=0.33, lmax=2
  cutoffs: 1.86(filt), 2.06(core),
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -58106.141833

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 108*108*108 grid
  Fine grid: 216*216*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 62.03 MiB
    Density: 39.27 MiB
      Arrays: 30.31 MiB
      Localized functions: 2.14 MiB
      Mixer: 6.82 MiB
    Hamiltonian: 19.99 MiB
      Arrays: 19.83 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.16 MiB
    Wavefunctions: 2.78 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.70 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 65
Number of bands in calculation: 20
Number of valence electrons: 32
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |                  Cl                   |  
 |         |          Cl                           |  
 |         |              Si                       |  
 |         |            Cl                         |  
 |         |                Cl                     |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 Si     8.169349    8.169349    8.169349    ( 0.0000,  0.0000,  0.0000)
   1 Cl     9.338698    9.338698    9.338698    ( 0.0000,  0.0000,  0.0000)
   2 Cl     7.000000    7.000000    9.338698    ( 0.0000,  0.0000,  0.0000)
   3 Cl     9.338698    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 Cl     7.000000    9.338698    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.338698    0.000000    0.000000   108     0.1513
  2. axis:    no     0.000000   16.338698    0.000000   108     0.1513
  3. axis:    no     0.000000    0.000000   16.338698   108     0.1513

  Lengths:  16.338698  16.338698  16.338698
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1513

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:30:48                 -17.086386    1      
iter:   2  11:30:49         -1.16   -16.868806    1      
iter:   3  11:30:49         -1.31   -16.789124    1      
iter:   4  11:30:50         -1.89   -16.785723    1      
iter:   5  11:30:51         -2.35   -16.784680    1      
iter:   6  11:30:51         -2.60   -16.784082    1      
iter:   7  11:30:52         -3.30   -16.784066    1      
iter:   8  11:30:52         -3.53   -16.784061    1      
iter:   9  11:30:53         -3.97   -16.784060    1      
iter:  10  11:30:54         -4.36   -16.784060    1      
iter:  11  11:30:54         -4.61   -16.784060    1      
iter:  12  11:30:55         -4.98   -16.784060    1      
iter:  13  11:30:56         -5.18   -16.784060    1      
iter:  14  11:30:56         -5.69   -16.784060    1      
iter:  15  11:30:57         -5.88   -16.784060    1      
iter:  16  11:30:57         -6.09   -16.784060    1      

Converged after 16 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -58106.141833)

Kinetic:        +34.476685
Potential:      -30.251889
External:        +0.000000
XC:             -20.988557
Entropy (-ST):   +0.000000
Local:           -0.020298
--------------------------
Free energy:    -16.784060
Extrapolated:   -16.784060

 Band  Eigenvalues  Occupancy
    0    -22.05378    2.00000
    1    -20.80476    2.00000
    2    -20.80476    2.00000
    3    -20.80476    2.00000
    4    -13.45508    2.00000
    5    -10.73400    2.00000
    6    -10.73400    2.00000
    7    -10.73400    2.00000
    8     -8.68590    2.00000
    9     -8.68590    2.00000
   10     -8.45845    2.00000
   11     -8.45845    2.00000
   12     -8.45845    2.00000
   13     -7.63964    2.00000
   14     -7.63964    2.00000
   15     -7.63964    2.00000
   16     -1.20765    0.00000
   17      0.27977    0.00000
   18      0.27977    0.00000
   19      0.27977    0.00000

Fermi level: -4.42364

Gap: 6.432 eV
Transition (v -> c):
  (s=0, k=0, n=15, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=16, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.005     0.005   0.0% |
LCAO WFS Initialize:                 0.600     0.021   0.2% |
 Hamiltonian:                        0.579     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.022     0.022   0.2% |
  Hartree integrate/restrict:        0.015     0.015   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.300     0.007   0.1% |
   Communicate from 1D:              0.063     0.063   0.6% |
   Communicate from 2D:              0.059     0.059   0.5% |
   Communicate to 1D:                0.064     0.064   0.6% |
   Communicate to 2D:                0.064     0.064   0.6% |
   FFT 1D:                           0.014     0.014   0.1% |
   FFT 2D:                           0.029     0.029   0.3% |
  XC 3D grid:                        0.237     0.237   2.2% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           9.251     0.006   0.1% |
 Density:                            0.495     0.000   0.0% |
  Atomic density matrices:           0.038     0.038   0.3% |
  Mix:                               0.338     0.338   3.1% ||
  Multipole moments:                 0.001     0.001   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.115     0.008   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.106     0.106   1.0% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        8.577     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.009     0.009   0.1% |
  Communicate:                       0.337     0.337   3.1% ||
  Hartree integrate/restrict:        0.204     0.204   1.9% ||
  Poisson:                           4.491     0.102   0.9% |
   Communicate from 1D:              0.933     0.933   8.6% |--|
   Communicate from 2D:              0.892     0.892   8.2% |--|
   Communicate to 1D:                0.960     0.960   8.8% |---|
   Communicate to 2D:                0.963     0.963   8.8% |---|
   FFT 1D:                           0.206     0.206   1.9% ||
   FFT 2D:                           0.434     0.434   4.0% |-|
  XC 3D grid:                        3.479     3.479  31.9% |------------|
  vbar:                              0.053     0.053   0.5% |
 LCAO eigensolver:                   0.173     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.031     0.031   0.3% |
  Orbital Layouts:                   0.013     0.013   0.1% |
  Potential matrix:                  0.127     0.127   1.2% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.134     0.134   1.2% |
mktci:                               0.001     0.001   0.0% |
Other:                               0.906     0.906   8.3% |--|
-----------------------------------------------------------
Total:                                        10.898 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:30:57 2021
