
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:32:05 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7.0
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/N.PBE.gz
  compensation charges: gauss, rc=0.18, lmax=2
  cutoffs: 1.11(filt), 0.96(core),
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -5004.295310

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 42.66 MiB
    Density: 27.34 MiB
      Arrays: 21.92 MiB
      Localized functions: 0.50 MiB
      Mixer: 4.91 MiB
    Hamiltonian: 14.38 MiB
      Arrays: 14.34 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.95 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.92 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 39
Number of bands in calculation: 12
Number of valence electrons: 16
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            O                    |  
 |        |                                 |  
 |        |            N                    |  
 |        |            N                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 N      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 N      7.000000    7.000000    8.171118    ( 0.0000,  0.0000,  0.0000)
   2 O      7.000000    7.000000    9.363187    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.363187   108     0.1515

  Lengths:  14.000000  14.000000  16.363187
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1520

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:32:06                 -20.920313    1      
iter:   2  11:32:07         -0.99   -20.196611    1      
iter:   3  11:32:07         -1.26   -20.135788    1      
iter:   4  11:32:08         -1.46   -20.112258    1      
iter:   5  11:32:08         -1.75   -20.102329    1      
iter:   6  11:32:08         -2.43   -20.101930    1      
iter:   7  11:32:09         -2.60   -20.101498    1      
iter:   8  11:32:09         -3.12   -20.101467    1      
iter:   9  11:32:10         -3.53   -20.101457    1      
iter:  10  11:32:10         -3.62   -20.101455    1      
iter:  11  11:32:11         -3.87   -20.101454    1      
iter:  12  11:32:11         -4.07   -20.101454    1      
iter:  13  11:32:11         -4.44   -20.101454    1      
iter:  14  11:32:12         -4.77   -20.101454    1      
iter:  15  11:32:12         -5.10   -20.101454    1      
iter:  16  11:32:13         -5.42   -20.101454    1      
iter:  17  11:32:13         -5.65   -20.101454    1      
iter:  18  11:32:13         -5.95   -20.101454    1      
iter:  19  11:32:14         -6.28   -20.101454    1      

Converged after 19 iterations.

Dipole moment: (-0.000000, 0.000000, 0.065718) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5004.295310)

Kinetic:        +11.825226
Potential:      -13.623052
External:        +0.000000
XC:             -18.548441
Entropy (-ST):   +0.000000
Local:           +0.244813
--------------------------
Free energy:    -20.101454
Extrapolated:   -20.101454

 Band  Eigenvalues  Occupancy
    0    -31.09662    2.00000
    1    -26.52115    2.00000
    2    -14.75489    2.00000
    3    -13.40539    2.00000
    4    -13.40539    2.00000
    5    -11.14003    2.00000
    6     -7.98893    2.00000
    7     -7.98893    2.00000
    8     -1.55173    0.00000
    9     -1.55173    0.00000
   10      2.57739    0.00000
   11     10.43550    0.00000

Fermi level: -4.77033

Gap: 6.437 eV
Transition (v -> c):
  (s=0, k=0, n=7, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.002     0.002   0.0% |
LCAO WFS Initialize:                 0.424     0.015   0.2% |
 Hamiltonian:                        0.409     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.016     0.016   0.2% |
  Hartree integrate/restrict:        0.010     0.010   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.209     0.005   0.1% |
   Communicate from 1D:              0.044     0.044   0.5% |
   Communicate from 2D:              0.043     0.043   0.5% |
   Communicate to 1D:                0.042     0.042   0.5% |
   Communicate to 2D:                0.044     0.044   0.5% |
   FFT 1D:                           0.010     0.010   0.1% |
   FFT 2D:                           0.022     0.022   0.2% |
  XC 3D grid:                        0.169     0.169   1.9% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           7.556     0.007   0.1% |
 Density:                            0.351     0.000   0.0% |
  Atomic density matrices:           0.019     0.019   0.2% |
  Mix:                               0.281     0.281   3.2% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.047     0.006   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.039     0.039   0.4% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        7.127     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.007     0.007   0.1% |
  Communicate:                       0.285     0.285   3.3% ||
  Hartree integrate/restrict:        0.182     0.182   2.1% ||
  Poisson:                           3.764     0.084   1.0% |
   Communicate from 1D:              0.793     0.793   9.0% |---|
   Communicate from 2D:              0.777     0.777   8.9% |---|
   Communicate to 1D:                0.764     0.764   8.7% |--|
   Communicate to 2D:                0.782     0.782   8.9% |---|
   FFT 1D:                           0.174     0.174   2.0% ||
   FFT 2D:                           0.391     0.391   4.5% |-|
  XC 3D grid:                        2.836     2.836  32.3% |------------|
  vbar:                              0.047     0.047   0.5% |
 LCAO eigensolver:                   0.070     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.016     0.016   0.2% |
  Orbital Layouts:                   0.009     0.009   0.1% |
  Potential matrix:                  0.044     0.044   0.5% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.134     0.134   1.5% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.651     0.651   7.4% |--|
-----------------------------------------------------------
Total:                                         8.769 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:32:14 2021
