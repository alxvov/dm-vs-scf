
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:13:03 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14.0
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Si.PBE.gz
  compensation charges: gauss, rc=0.33, lmax=2
  cutoffs: 1.86(filt), 2.06(core),
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -7936.304899

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*104 grid
  Fine grid: 208*208*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 53.90 MiB
    Density: 34.20 MiB
      Arrays: 27.03 MiB
      Localized functions: 1.09 MiB
      Mixer: 6.07 MiB
    Hamiltonian: 17.77 MiB
      Arrays: 17.69 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 1.94 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.92 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 33
Number of bands in calculation: 8
Number of valence electrons: 8
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                H                     |  
 |         |           H                          |  
 |         |            HSi                       |  
 |         |               H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 Si     7.856135    7.856135    7.856135    ( 0.0000,  0.0000,  0.0000)
   1 H      8.712270    8.712270    8.712270    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    8.712270    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.712270    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      8.712270    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.712270    0.000000    0.000000   104     0.1511
  2. axis:    no     0.000000   15.712270    0.000000   104     0.1511
  3. axis:    no     0.000000    0.000000   15.712270   104     0.1511

  Lengths:  15.712270  15.712270  15.712270
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1511

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:13:04                 -19.204519    1      
iter:   2  11:13:05         -0.53   -18.783741    1      
iter:   3  11:13:05         -0.69   -18.406122    1      
iter:   4  11:13:06         -1.72   -18.406624    1      
iter:   5  11:13:06         -2.14   -18.407018    1      
iter:   6  11:13:07         -2.58   -18.406951    1      
iter:   7  11:13:07         -2.80   -18.406908    1      
iter:   8  11:13:08         -3.60   -18.406907    1      
iter:   9  11:13:09         -3.80   -18.406907    1      
iter:  10  11:13:09         -4.08   -18.406907    1      
iter:  11  11:13:10         -4.27   -18.406907    1      
iter:  12  11:13:10         -4.48   -18.406907    1      
iter:  13  11:13:11         -5.14   -18.406907    1      
iter:  14  11:13:11         -5.34   -18.406907    1      
iter:  15  11:13:12         -6.00   -18.406907    1      
iter:  16  11:13:12         -6.17   -18.406907    1      

Converged after 16 iterations.

Dipole moment: (-0.000000, -0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -7936.304899)

Kinetic:        +16.236313
Potential:      -18.694031
External:        +0.000000
XC:             -15.909698
Entropy (-ST):   +0.000000
Local:           -0.039491
--------------------------
Free energy:    -18.406907
Extrapolated:   -18.406907

 Band  Eigenvalues  Occupancy
    0    -13.46917    2.00000
    1     -8.43968    2.00000
    2     -8.43968    2.00000
    3     -8.43968    2.00000
    4      0.82214    0.00000
    5      0.82214    0.00000
    6      0.82214    0.00000
    7      1.68704    0.00000

Fermi level: -3.80877

Gap: 9.262 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.004     0.004   0.0% |
LCAO WFS Initialize:                 0.538     0.016   0.2% |
 Hamiltonian:                        0.522     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.021     0.021   0.2% |
  Hartree integrate/restrict:        0.012     0.012   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.271     0.007   0.1% |
   Communicate from 1D:              0.057     0.057   0.6% |
   Communicate from 2D:              0.053     0.053   0.6% |
   Communicate to 1D:                0.057     0.057   0.6% |
   Communicate to 2D:                0.056     0.056   0.6% |
   FFT 1D:                           0.013     0.013   0.1% |
   FFT 2D:                           0.028     0.028   0.3% |
  XC 3D grid:                        0.212     0.212   2.2% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           8.070     0.006   0.1% |
 Density:                            0.357     0.000   0.0% |
  Atomic density matrices:           0.020     0.020   0.2% |
  Mix:                               0.255     0.255   2.7% ||
  Multipole moments:                 0.001     0.001   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.078     0.007   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.069     0.069   0.7% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        7.607     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.009     0.009   0.1% |
  Communicate:                       0.315     0.315   3.3% ||
  Hartree integrate/restrict:        0.185     0.185   1.9% ||
  Poisson:                           3.958     0.092   1.0% |
   Communicate from 1D:              0.827     0.827   8.7% |--|
   Communicate from 2D:              0.793     0.793   8.4% |--|
   Communicate to 1D:                0.808     0.808   8.5% |--|
   Communicate to 2D:                0.818     0.818   8.6% |--|
   FFT 1D:                           0.198     0.198   2.1% ||
   FFT 2D:                           0.422     0.422   4.5% |-|
  XC 3D grid:                        3.088     3.088  32.6% |------------|
  vbar:                              0.049     0.049   0.5% |
 LCAO eigensolver:                   0.100     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.015     0.015   0.2% |
  Orbital Layouts:                   0.006     0.006   0.1% |
  Potential matrix:                  0.077     0.077   0.8% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.074     0.074   0.8% |
mktci:                               0.001     0.001   0.0% |
Other:                               0.781     0.781   8.2% |--|
-----------------------------------------------------------
Total:                                         9.470 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:13:12 2021
