
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:14:58 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14.0
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Si.PBE.gz
  compensation charges: gauss, rc=0.33, lmax=2
  cutoffs: 1.86(filt), 2.06(core),
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -18742.667391

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*104 grid
  Fine grid: 208*208*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 54.27 MiB
    Density: 34.56 MiB
      Arrays: 27.03 MiB
      Localized functions: 1.45 MiB
      Mixer: 6.07 MiB
    Hamiltonian: 17.80 MiB
      Arrays: 17.69 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 1.91 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.84 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 65
Number of bands in calculation: 20
Number of valence electrons: 32
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                F                     |  
 |         |           F Si                       |  
 |         |            F                         |  
 |         |               F                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 Si     7.912806    7.912806    7.912806    ( 0.0000,  0.0000,  0.0000)
   1 F      8.825612    8.825612    8.825612    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000    7.000000    8.825612    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    8.825612    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 F      8.825612    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.825612    0.000000    0.000000   104     0.1522
  2. axis:    no     0.000000   15.825612    0.000000   104     0.1522
  3. axis:    no     0.000000    0.000000   15.825612   104     0.1522

  Lengths:  15.825612  15.825612  15.825612
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1522

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:15:00                 -26.343003    1      
iter:   2  11:15:00         -1.14   -25.303984    1      
iter:   3  11:15:01         -1.35   -25.189503    1      
iter:   4  11:15:02         -1.62   -25.217623    1      
iter:   5  11:15:02         -2.49   -25.216112    1      
iter:   6  11:15:03         -2.82   -25.215567    1      
iter:   7  11:15:03         -3.06   -25.215315    1      
iter:   8  11:15:04         -3.84   -25.215313    1      
iter:   9  11:15:04         -4.11   -25.215311    1      
iter:  10  11:15:05         -4.55   -25.215311    1      
iter:  11  11:15:05         -4.76   -25.215311    1      
iter:  12  11:15:06         -4.95   -25.215311    1      
iter:  13  11:15:06         -5.36   -25.215311    1      
iter:  14  11:15:07         -5.60   -25.215311    1      
iter:  15  11:15:08         -6.04   -25.215311    1      

Converged after 15 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -18742.667391)

Kinetic:        +37.395193
Potential:      -33.704421
External:        +0.000000
XC:             -29.159183
Entropy (-ST):   +0.000000
Local:           +0.253100
--------------------------
Free energy:    -25.215311
Extrapolated:   -25.215311

 Band  Eigenvalues  Occupancy
    0    -31.37661    2.00000
    1    -30.43843    2.00000
    2    -30.43843    2.00000
    3    -30.43843    2.00000
    4    -14.96204    2.00000
    5    -12.92840    2.00000
    6    -12.92840    2.00000
    7    -12.92840    2.00000
    8    -11.36280    2.00000
    9    -11.36280    2.00000
   10    -11.21405    2.00000
   11    -11.21405    2.00000
   12    -11.21405    2.00000
   13    -10.21836    2.00000
   14    -10.21836    2.00000
   15    -10.21836    2.00000
   16      0.11852    0.00000
   17      1.91797    0.00000
   18      1.91797    0.00000
   19      1.91797    0.00000

Fermi level: -5.04992

Gap: 10.337 eV
Transition (v -> c):
  (s=0, k=0, n=15, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=16, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.004     0.004   0.0% |
LCAO WFS Initialize:                 0.537     0.016   0.2% |
 Hamiltonian:                        0.521     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.021     0.021   0.2% |
  Hartree integrate/restrict:        0.014     0.014   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.270     0.007   0.1% |
   Communicate from 1D:              0.056     0.056   0.6% |
   Communicate from 2D:              0.052     0.052   0.6% |
   Communicate to 1D:                0.057     0.057   0.6% |
   Communicate to 2D:                0.055     0.055   0.6% |
   FFT 1D:                           0.014     0.014   0.1% |
   FFT 2D:                           0.028     0.028   0.3% |
  XC 3D grid:                        0.211     0.211   2.3% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           7.569     0.006   0.1% |
 Density:                            0.333     0.000   0.0% |
  Atomic density matrices:           0.027     0.027   0.3% |
  Mix:                               0.239     0.239   2.6% ||
  Multipole moments:                 0.001     0.001   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.064     0.007   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.056     0.056   0.6% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        7.127     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.009     0.009   0.1% |
  Communicate:                       0.295     0.295   3.3% ||
  Hartree integrate/restrict:        0.178     0.178   2.0% ||
  Poisson:                           3.702     0.088   1.0% |
   Communicate from 1D:              0.771     0.771   8.5% |--|
   Communicate from 2D:              0.737     0.737   8.1% |--|
   Communicate to 1D:                0.755     0.755   8.3% |--|
   Communicate to 2D:                0.770     0.770   8.5% |--|
   FFT 1D:                           0.185     0.185   2.0% ||
   FFT 2D:                           0.396     0.396   4.4% |-|
  XC 3D grid:                        2.894     2.894  32.0% |------------|
  vbar:                              0.045     0.045   0.5% |
 LCAO eigensolver:                   0.103     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.023     0.023   0.3% |
  Orbital Layouts:                   0.012     0.012   0.1% |
  Potential matrix:                  0.066     0.066   0.7% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.135     0.135   1.5% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.805     0.805   8.9% |---|
-----------------------------------------------------------
Total:                                         9.052 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:15:08 2021
