
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:41:13 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Na-setup:
  name: Sodium
  id: d7ecbc49209718622bcbe287195dca2a
  Z: 11.0
  valence: 7
  core: 4
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Na.PBE.gz
  compensation charges: gauss, rc=0.37, lmax=2
  cutoffs: 2.17(filt), 2.59(core),
  valence states:
                energy  radius
    3s(1.00)    -2.744   1.201
    2p(6.00)   -28.672   1.217
    3p(0.00)    -0.743   1.217
    *s          24.468   1.201
    *d           0.000   1.238

  LCAO basis set for Na:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Na.dzp.basis.gz
    Number of radial functions: 7
    Number of spherical harmonics: 17
      l=0, rc=11.4062 Bohr: 3s-sz confined orbital
      l=1, rc=3.6719 Bohr: 2p-sz confined orbital
      l=1, rc=15.9375 Bohr: 3p-sz confined orbital
      l=0, rc=6.9531 Bohr: 3s-dz split-valence wave
      l=1, rc=2.4219 Bohr: 2p-dz split-valence wave
      l=1, rc=10.0469 Bohr: 3p-dz split-valence wave
      l=1, rc=11.4062 Bohr: p-type Gaussian polarization

Reference energy: -8837.220995

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*116 grid
  Fine grid: 184*184*232 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 54.54 MiB
    Density: 31.43 MiB
      Arrays: 23.56 MiB
      Localized functions: 2.59 MiB
      Mixer: 5.28 MiB
    Hamiltonian: 15.60 MiB
      Arrays: 15.41 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.19 MiB
    Wavefunctions: 7.51 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 7.49 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 34
Number of bands in calculation: 13
Number of valence electrons: 14
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Na                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Na                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Na     7.000000    7.000000   10.152524    ( 0.0000,  0.0000,  0.0000)
   1 Na     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   17.152524   116     0.1479

  Lengths:  14.000000  14.000000  17.152524
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:41:14                  -1.175467    1      
iter:   2  11:41:14         -1.25    -1.106891    1      
iter:   3  11:41:15         -1.44    -1.064134    1      
iter:   4  11:41:15         -2.58    -1.064537    1      
iter:   5  11:41:16         -2.53    -1.064325    1      
iter:   6  11:41:16         -3.69    -1.064323    1      
iter:   7  11:41:17         -3.82    -1.064321    1      
iter:   8  11:41:17         -4.34    -1.064321    1      
iter:   9  11:41:18         -4.90    -1.064321    1      
iter:  10  11:41:18         -5.08    -1.064321    1      
iter:  11  11:41:19         -6.05    -1.064321    1      

Converged after 11 iterations.

Dipole moment: (-0.000000, -0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -8837.220995)

Kinetic:         +3.676010
Potential:       -3.256147
External:        +0.000000
XC:              -1.488864
Entropy (-ST):   +0.000000
Local:           +0.004680
--------------------------
Free energy:     -1.064321
Extrapolated:    -1.064321

 Band  Eigenvalues  Occupancy
    0    -27.82684    2.00000
    1    -27.80683    2.00000
    2    -27.80683    2.00000
    3    -27.80174    2.00000
    4    -27.80174    2.00000
    5    -27.78158    2.00000
    6     -2.91092    2.00000
    7     -1.68477    0.00000
    8     -0.74634    0.00000
    9     -0.74634    0.00000
   10     -0.25182    0.00000
   11      0.36564    0.00000
   12      0.36564    0.00000

Fermi level: -2.29784

Gap: 1.226 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.010     0.010   0.2% |
LCAO WFS Initialize:                 0.462     0.018   0.3% |
 Hamiltonian:                        0.444     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.020     0.020   0.3% |
  Hartree integrate/restrict:        0.011     0.011   0.2% |
  Initialize Hamiltonian:            0.000     0.000   0.0% |
  Poisson:                           0.226     0.006   0.1% |
   Communicate from 1D:              0.047     0.047   0.8% |
   Communicate from 2D:              0.046     0.046   0.8% |
   Communicate to 1D:                0.046     0.046   0.8% |
   Communicate to 2D:                0.047     0.047   0.8% |
   FFT 1D:                           0.012     0.012   0.2% |
   FFT 2D:                           0.023     0.023   0.4% |
  XC 3D grid:                        0.183     0.183   3.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           4.778     0.004   0.1% |
 Density:                            0.319     0.000   0.0% |
  Atomic density matrices:           0.015     0.015   0.2% |
  Mix:                               0.172     0.172   2.9% ||
  Multipole moments:                 0.001     0.001   0.0% |
  Normalize:                         0.001     0.001   0.0% |
  Pseudo density:                    0.130     0.004   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.125     0.125   2.1% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        4.292     0.001   0.0% |
  Atomic:                            0.002     0.002   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.1% |
  Communicate:                       0.200     0.200   3.3% ||
  Hartree integrate/restrict:        0.106     0.106   1.8% ||
  Poisson:                           2.250     0.051   0.9% |
   Communicate from 1D:              0.471     0.471   7.9% |--|
   Communicate from 2D:              0.460     0.460   7.7% |--|
   Communicate to 1D:                0.455     0.455   7.6% |--|
   Communicate to 2D:                0.468     0.468   7.8% |--|
   FFT 1D:                           0.115     0.115   1.9% ||
   FFT 2D:                           0.229     0.229   3.8% |-|
  XC 3D grid:                        1.698     1.698  28.4% |----------|
  vbar:                              0.028     0.028   0.5% |
 LCAO eigensolver:                   0.163     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.012     0.012   0.2% |
  Orbital Layouts:                   0.004     0.004   0.1% |
  Potential matrix:                  0.145     0.145   2.4% ||
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.056     0.056   0.9% |
mktci:                               0.001     0.001   0.0% |
Other:                               0.673     0.673  11.3% |----|
-----------------------------------------------------------
Total:                                         5.981 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:41:19 2021
