
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:39:15 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17.0
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/Cl.PBE.gz
  compensation charges: gauss, rc=0.25, lmax=2
  cutoffs: 1.40(filt), 1.49(core),
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -52275.053814

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*112*116 grid
  Fine grid: 184*224*232 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*224*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 58.48 MiB
    Density: 36.92 MiB
      Arrays: 28.74 MiB
      Localized functions: 1.72 MiB
      Mixer: 6.46 MiB
    Hamiltonian: 18.93 MiB
      Arrays: 18.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.13 MiB
    Wavefunctions: 2.63 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.09 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.52 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 78
Number of bands in calculation: 24
Number of valence electrons: 36
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |            Cl                   |  
 |         |          Cl                     |  
 |         |           C                     |  
 |         |           C                     |  
 |         |            Cl                   |  
 |         |          Cl                     |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 C      7.000000    8.448939    9.265103    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000    8.448939    7.914299    ( 0.0000,  0.0000,  0.0000)
   2 Cl     7.000000    9.897878   10.179402    ( 0.0000,  0.0000,  0.0000)
   3 Cl     7.000000    7.000000   10.179402    ( 0.0000,  0.0000,  0.0000)
   4 Cl     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 Cl     7.000000    9.897878    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.897878    0.000000   112     0.1509
  3. axis:    no     0.000000    0.000000   17.179402   116     0.1481

  Lengths:  14.000000  16.897878  17.179402
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1504

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:39:17                 -24.182205    1      
iter:   2  11:39:17         -1.09   -23.801064    1      
iter:   3  11:39:18         -1.23   -23.601448    1      
iter:   4  11:39:18         -1.77   -23.598106    1      
iter:   5  11:39:19         -2.34   -23.595501    1      
iter:   6  11:39:19         -2.74   -23.595199    1      
iter:   7  11:39:20         -3.04   -23.595144    1      
iter:   8  11:39:21         -3.46   -23.595141    1      
iter:   9  11:39:21         -3.73   -23.595140    1      
iter:  10  11:39:22         -4.00   -23.595140    1      
iter:  11  11:39:22         -4.24   -23.595140    1      
iter:  12  11:39:23         -4.72   -23.595140    1      
iter:  13  11:39:24         -4.92   -23.595140    1      
iter:  14  11:39:24         -5.32   -23.595140    1      
iter:  15  11:39:25         -5.54   -23.595140    1      
iter:  16  11:39:25         -5.77   -23.595140    1      
iter:  17  11:39:26         -6.02   -23.595140    1      

Converged after 17 iterations.

Dipole moment: (-0.000000, 0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -52275.053814)

Kinetic:        +38.596084
Potential:      -33.396575
External:        +0.000000
XC:             -28.880495
Entropy (-ST):   +0.000000
Local:           +0.085847
--------------------------
Free energy:    -23.595140
Extrapolated:   -23.595140

 Band  Eigenvalues  Occupancy
    0    -23.76121    2.00000
    1    -22.44475    2.00000
    2    -21.35902    2.00000
    3    -20.76252    2.00000
    4    -17.76839    2.00000
    5    -13.78762    2.00000
    6    -12.37901    2.00000
    7    -12.32625    2.00000
    8    -10.66417    2.00000
    9    -10.62028    2.00000
   10     -8.96659    2.00000
   11     -8.37286    2.00000
   12     -8.09285    2.00000
   13     -8.01818    2.00000
   14     -7.87961    2.00000
   15     -7.74003    2.00000
   16     -6.97391    2.00000
   17     -5.61734    2.00000
   18     -1.35879    0.00000
   19     -0.79636    0.00000
   20     -0.61867    0.00000
   21     -0.30620    0.00000
   22      3.41010    0.00000
   23      6.48560    0.00000

Fermi level: -3.48806

Gap: 4.259 eV
Transition (v -> c):
  (s=0, k=0, n=17, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=18, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.005     0.005   0.0% |
LCAO WFS Initialize:                 0.572     0.021   0.2% |
 Hamiltonian:                        0.551     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.021     0.021   0.2% |
  Hartree integrate/restrict:        0.013     0.013   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.287     0.006   0.1% |
   Communicate from 1D:              0.060     0.060   0.6% |
   Communicate from 2D:              0.057     0.057   0.5% |
   Communicate to 1D:                0.059     0.059   0.5% |
   Communicate to 2D:                0.062     0.062   0.6% |
   FFT 1D:                           0.014     0.014   0.1% |
   FFT 2D:                           0.028     0.028   0.3% |
  XC 3D grid:                        0.224     0.224   2.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           9.357     0.007   0.1% |
 Density:                            0.518     0.000   0.0% |
  Atomic density matrices:           0.040     0.040   0.4% |
  Mix:                               0.328     0.328   3.0% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.146     0.008   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.136     0.136   1.2% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        8.613     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.010     0.010   0.1% |
  Communicate:                       0.341     0.341   3.1% ||
  Hartree integrate/restrict:        0.210     0.210   1.9% ||
  Poisson:                           4.580     0.093   0.9% |
   Communicate from 1D:              0.970     0.970   8.9% |---|
   Communicate from 2D:              0.915     0.915   8.4% |--|
   Communicate to 1D:                0.950     0.950   8.7% |--|
   Communicate to 2D:                0.985     0.985   9.0% |---|
   FFT 1D:                           0.220     0.220   2.0% ||
   FFT 2D:                           0.447     0.447   4.1% |-|
  XC 3D grid:                        3.414     3.414  31.2% |-----------|
  vbar:                              0.055     0.055   0.5% |
 LCAO eigensolver:                   0.218     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.032     0.032   0.3% |
  Orbital Layouts:                   0.017     0.017   0.2% |
  Potential matrix:                  0.168     0.168   1.5% ||
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.134     0.134   1.2% |
mktci:                               0.001     0.001   0.0% |
Other:                               0.854     0.854   7.8% |--|
-----------------------------------------------------------
Total:                                        10.926 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:39:26 2021
