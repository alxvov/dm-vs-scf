
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:38:29 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -5238.061875

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 112*112*116 grid
  Fine grid: 224*224*232 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 71.88 MiB
    Density: 44.50 MiB
      Arrays: 35.06 MiB
      Localized functions: 1.54 MiB
      Mixer: 7.89 MiB
    Hamiltonian: 23.06 MiB
      Arrays: 22.94 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.33 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.17 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 4.14 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 13
Number of atomic orbitals: 105
Number of bands in calculation: 21
Number of valence electrons: 28
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |             H C  H                     |  
 |         |           H  C H                       |  
 |         |               C                        |  
 |         |                                        |  
 |         |            HC  C H                     |  
 |         |           H     H                      |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      8.265075    8.265075    8.568090    ( 0.0000,  0.0000,  0.0000)
   1 C      8.265075    9.027089    9.833842    ( 0.0000,  0.0000,  0.0000)
   2 C      8.265075    7.503061    9.833842    ( 0.0000,  0.0000,  0.0000)
   3 C      9.027089    8.265075    7.302338    ( 0.0000,  0.0000,  0.0000)
   4 C      7.503061    8.265075    7.302338    ( 0.0000,  0.0000,  0.0000)
   5 H      7.351052    9.530150   10.136180    ( 0.0000,  0.0000,  0.0000)
   6 H      9.179098    9.530150   10.136180    ( 0.0000,  0.0000,  0.0000)
   7 H      7.351052    7.000000   10.136180    ( 0.0000,  0.0000,  0.0000)
   8 H      9.179098    7.000000   10.136180    ( 0.0000,  0.0000,  0.0000)
   9 H      9.530150    7.351052    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H      9.530150    9.179098    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.000000    7.351052    7.000000    ( 0.0000,  0.0000,  0.0000)
  12 H      7.000000    9.179098    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.530150    0.000000    0.000000   112     0.1476
  2. axis:    no     0.000000   16.530150    0.000000   112     0.1476
  3. axis:    no     0.000000    0.000000   17.136180   116     0.1477

  Lengths:  16.530150  16.530150  17.136180
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1476

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:38:31                 -74.019849    1      
iter:   2  11:38:32         -0.73   -71.867617    1      
iter:   3  11:38:33         -0.91   -71.001795    1      
iter:   4  11:38:33         -1.31   -70.918868    1      
iter:   5  11:38:34         -2.15   -70.917307    1      
iter:   6  11:38:35         -2.42   -70.917042    1      
iter:   7  11:38:36         -2.58   -70.916606    1      
iter:   8  11:38:36         -3.40   -70.916601    1      
iter:   9  11:38:37         -3.60   -70.916598    1      
iter:  10  11:38:38         -4.24   -70.916598    1      
iter:  11  11:38:39         -4.56   -70.916598    1      
iter:  12  11:38:39         -4.71   -70.916598    1      
iter:  13  11:38:40         -5.24   -70.916598    1      
iter:  14  11:38:41         -5.57   -70.916598    1      
iter:  15  11:38:42         -6.05   -70.916598    1      

Converged after 15 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5238.061875)

Kinetic:        +57.095272
Potential:      -61.373574
External:        +0.000000
XC:             -66.767078
Entropy (-ST):   +0.000000
Local:           +0.128782
--------------------------
Free energy:    -70.916598
Extrapolated:   -70.916598

 Band  Eigenvalues  Occupancy
    0    -21.99255    2.00000
    1    -19.88215    2.00000
    2    -14.71397    2.00000
    3    -14.71397    2.00000
    4    -13.18966    2.00000
    5    -11.88491    2.00000
    6    -11.42973    2.00000
    7    -11.42973    2.00000
    8     -8.83310    2.00000
    9     -8.49463    2.00000
   10     -8.39442    2.00000
   11     -6.76878    2.00000
   12     -6.76878    2.00000
   13     -6.44580    2.00000
   14      1.84003    0.00000
   15      1.84003    0.00000
   16      2.22660    0.00000
   17      2.61393    0.00000
   18      3.22316    0.00000
   19      3.22316    0.00000
   20      4.49361    0.00000

Fermi level: -2.30289

Gap: 8.286 eV
Transition (v -> c):
  (s=0, k=0, n=13, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=14, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.009     0.009   0.1% |
LCAO WFS Initialize:                 0.697     0.026   0.2% |
 Hamiltonian:                        0.671     0.000   0.0% |
  Atomic:                            0.020     0.004   0.0% |
   XC Correction:                    0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
  Communicate:                       0.011     0.011   0.1% |
  Hartree integrate/restrict:        0.019     0.019   0.2% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.340     0.009   0.1% |
   Communicate from 1D:              0.069     0.069   0.6% |
   Communicate from 2D:              0.068     0.068   0.5% |
   Communicate to 1D:                0.066     0.066   0.5% |
   Communicate to 2D:                0.074     0.074   0.6% |
   FFT 1D:                           0.017     0.017   0.1% |
   FFT 2D:                           0.037     0.037   0.3% |
  XC 3D grid:                        0.272     0.272   2.2% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.003     0.003   0.0% |
SCF-cycle:                          10.614     0.006   0.0% |
 Density:                            0.787     0.000   0.0% |
  Atomic density matrices:           0.085     0.085   0.7% |
  Mix:                               0.365     0.365   2.9% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.333     0.010   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.322     0.322   2.6% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        9.341     0.001   0.0% |
  Atomic:                            0.280     0.062   0.5% |
   XC Correction:                    0.219     0.219   1.8% ||
  Calculate atomic Hamiltonians:     0.020     0.020   0.2% |
  Communicate:                       0.161     0.161   1.3% ||
  Hartree integrate/restrict:        0.237     0.237   1.9% ||
  Poisson:                           4.764     0.120   1.0% |
   Communicate from 1D:              0.972     0.972   7.8% |--|
   Communicate from 2D:              0.958     0.958   7.7% |--|
   Communicate to 1D:                0.928     0.928   7.5% |--|
   Communicate to 2D:                1.044     1.044   8.4% |--|
   FFT 1D:                           0.238     0.238   1.9% ||
   FFT 2D:                           0.505     0.505   4.1% |-|
  XC 3D grid:                        3.819     3.819  30.7% |-----------|
  vbar:                              0.059     0.059   0.5% |
 LCAO eigensolver:                   0.481     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.064     0.064   0.5% |
  Orbital Layouts:                   0.022     0.022   0.2% |
  Potential matrix:                  0.391     0.391   3.1% ||
ST tci:                              0.003     0.003   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.074     0.074   0.6% |
mktci:                               0.002     0.002   0.0% |
Other:                               1.023     1.023   8.2% |--|
-----------------------------------------------------------
Total:                                        12.426 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:38:42 2021
