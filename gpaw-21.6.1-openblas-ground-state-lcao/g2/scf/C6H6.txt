
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:22:00 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6240.709666

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 120*128*92 grid
  Fine grid: 240*256*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*256*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 0, 1]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 69.70 MiB
    Density: 43.16 MiB
      Arrays: 34.03 MiB
      Localized functions: 1.47 MiB
      Mixer: 7.66 MiB
    Hamiltonian: 22.38 MiB
      Arrays: 22.26 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.16 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.18 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.97 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 12
Number of atomic orbitals: 108
Number of bands in calculation: 22
Number of valence electrons: 30
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
             .--------------------------------------------.  
            /|                                            |  
           / |                                            |  
          /  |                                            |  
         /   |                                            |  
        /    |                                            |  
       /     |                                            |  
      /      |                                            |  
     /       |                                            |  
    /        |                                            |  
   /         |                                            |  
  /          |                                            |  
 *           |                                            |  
 |           |                 H                          |  
 |           |           H C  C  C  H                     |  
 |           |         H  C  C  C H                       |  
 |           |              H                             |  
 |           .--------------------------------------------.  
 |          /                                            /   
 |         /                                            /    
 |        /                                            /     
 |       /                                            /      
 |      /                                            /       
 |     /                                            /        
 |    /                                            /         
 |   /                                            /          
 |  /                                            /           
 | /                                            /            
 |/                                            /             
 *--------------------------------------------*              

Positions:
   0 C      9.149787   10.877608    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C     10.358107   10.179984    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 C     10.358107    8.784736    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 C      9.149787    8.087112    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 C      7.941467    8.784736    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 C      7.941467   10.179984    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      9.149787   11.964720    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H     11.299574   10.723540    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H     11.299574    8.241180    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      9.149787    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H      7.000000    8.241180    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.000000   10.723540    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.299574    0.000000    0.000000   120     0.1525
  2. axis:    no     0.000000   18.964720    0.000000   128     0.1482
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  18.299574  18.964720  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:22:02                 -76.592382    1      
iter:   2  11:22:03         -0.78   -74.740492    1      
iter:   3  11:22:04         -0.95   -73.922894    1      
iter:   4  11:22:04         -1.43   -73.858887    1      
iter:   5  11:22:05         -2.12   -73.855441    1      
iter:   6  11:22:06         -2.35   -73.854285    1      
iter:   7  11:22:07         -2.55   -73.853414    1      
iter:   8  11:22:07         -3.35   -73.853406    1      
iter:   9  11:22:08         -3.53   -73.853400    1      
iter:  10  11:22:09         -4.41   -73.853400    1      
iter:  11  11:22:10         -4.56   -73.853400    1      
iter:  12  11:22:10         -4.92   -73.853400    1      
iter:  13  11:22:11         -5.38   -73.853400    1      
iter:  14  11:22:12         -5.54   -73.853400    1      
iter:  15  11:22:13         -6.48   -73.853400    1      

Converged after 15 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6240.709666)

Kinetic:        +57.899209
Potential:      -64.652900
External:        +0.000000
XC:             -67.281642
Entropy (-ST):   +0.000000
Local:           +0.181933
--------------------------
Free energy:    -73.853400
Extrapolated:   -73.853400

 Band  Eigenvalues  Occupancy
    0    -21.15131    2.00000
    1    -18.38553    2.00000
    2    -18.38552    2.00000
    3    -14.77111    2.00000
    4    -14.77046    2.00000
    5    -12.79153    2.00000
    6    -11.13784    2.00000
    7    -10.78101    2.00000
    8    -10.12225    2.00000
    9    -10.12223    2.00000
   10     -8.88188    2.00000
   11     -8.13637    2.00000
   12     -8.13614    2.00000
   13     -6.15826    2.00000
   14     -6.15817    2.00000
   15     -0.92802    0.00000
   16     -0.92794    0.00000
   17      1.63383    0.00000
   18      2.88778    0.00000
   19      2.88794    0.00000
   20      3.11604    0.00000
   21      3.85328    0.00000

Fermi level: -3.54309

Gap: 5.230 eV
Transition (v -> c):
  (s=0, k=0, n=14, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=15, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.008     0.008   0.1% |
LCAO WFS Initialize:                 0.681     0.024   0.2% |
 Hamiltonian:                        0.657     0.000   0.0% |
  Atomic:                            0.016     0.000   0.0% |
   XC Correction:                    0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
  Communicate:                       0.018     0.018   0.1% |
  Hartree integrate/restrict:        0.018     0.018   0.1% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.331     0.008   0.1% |
   Communicate from 1D:              0.069     0.069   0.6% |
   Communicate from 2D:              0.066     0.066   0.5% |
   Communicate to 1D:                0.063     0.063   0.5% |
   Communicate to 2D:                0.072     0.072   0.6% |
   FFT 1D:                           0.021     0.021   0.2% |
   FFT 2D:                           0.034     0.034   0.3% |
  XC 3D grid:                        0.264     0.264   2.2% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          10.359     0.006   0.0% |
 Density:                            0.721     0.000   0.0% |
  Atomic density matrices:           0.074     0.074   0.6% |
  Mix:                               0.347     0.347   2.9% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.296     0.009   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.286     0.286   2.4% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        9.212     0.001   0.0% |
  Atomic:                            0.230     0.007   0.1% |
   XC Correction:                    0.223     0.223   1.8% ||
  Calculate atomic Hamiltonians:     0.028     0.028   0.2% |
  Communicate:                       0.245     0.245   2.0% ||
  Hartree integrate/restrict:        0.250     0.250   2.1% ||
  Poisson:                           4.692     0.103   0.8% |
   Communicate from 1D:              0.966     0.966   8.0% |--|
   Communicate from 2D:              0.917     0.917   7.6% |--|
   Communicate to 1D:                0.929     0.929   7.7% |--|
   Communicate to 2D:                1.008     1.008   8.3% |--|
   FFT 1D:                           0.290     0.290   2.4% ||
   FFT 2D:                           0.480     0.480   4.0% |-|
  XC 3D grid:                        3.709     3.709  30.6% |-----------|
  vbar:                              0.057     0.057   0.5% |
 LCAO eigensolver:                   0.420     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.046     0.046   0.4% |
  Orbital Layouts:                   0.024     0.024   0.2% |
  Potential matrix:                  0.349     0.349   2.9% ||
ST tci:                              0.003     0.003   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.074     0.074   0.6% |
mktci:                               0.002     0.002   0.0% |
Other:                               0.980     0.980   8.1% |--|
-----------------------------------------------------------
Total:                                        12.108 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:22:13 2021
