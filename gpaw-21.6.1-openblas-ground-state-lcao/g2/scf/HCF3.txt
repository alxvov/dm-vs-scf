
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:23:26 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9.0
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/F.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 0.74(core),
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -9182.360633

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 108*104*104 grid
  Fine grid: 216*208*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [1, 2, 0]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 54.86 MiB
    Density: 35.16 MiB
      Arrays: 28.09 MiB
      Localized functions: 0.76 MiB
      Mixer: 6.31 MiB
    Hamiltonian: 18.43 MiB
      Arrays: 18.37 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.26 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.21 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 57
Number of bands in calculation: 17
Number of valence electrons: 26
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |              H                        |  
 |         |              CF                       |  
 |         |           F    F                      |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 C      8.089633    7.629100    7.469750    ( 0.0000,  0.0000,  0.0000)
   1 H      8.089633    7.629100    8.558212    ( 0.0000,  0.0000,  0.0000)
   2 F      8.089633    8.887300    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 F      9.179266    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.179266    0.000000    0.000000   108     0.1498
  2. axis:    no     0.000000   15.887300    0.000000   104     0.1528
  3. axis:    no     0.000000    0.000000   15.558212   104     0.1496

  Lengths:  16.179266  15.887300  15.558212
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:23:27                 -24.222903    1      
iter:   2  11:23:28         -1.13   -23.560976    1      
iter:   3  11:23:28         -1.30   -23.360181    1      
iter:   4  11:23:29         -1.70   -23.334336    1      
iter:   5  11:23:29         -2.51   -23.333456    1      
iter:   6  11:23:30         -2.72   -23.332940    1      
iter:   7  11:23:30         -3.07   -23.332798    1      
iter:   8  11:23:31         -3.65   -23.332793    1      
iter:   9  11:23:31         -3.92   -23.332791    1      
iter:  10  11:23:32         -4.41   -23.332791    1      
iter:  11  11:23:32         -4.61   -23.332791    1      
iter:  12  11:23:33         -4.80   -23.332791    1      
iter:  13  11:23:33         -5.24   -23.332791    1      
iter:  14  11:23:34         -5.53   -23.332791    1      
iter:  15  11:23:34         -5.95   -23.332791    1      
iter:  16  11:23:35         -6.18   -23.332791    1      

Converged after 16 iterations.

Dipole moment: (0.000000, 0.000058, 0.264110) |e|*Ang

Energy contributions relative to reference atoms: (reference = -9182.360633)

Kinetic:        +31.613825
Potential:      -29.260316
External:        +0.000000
XC:             -26.009324
Entropy (-ST):   +0.000000
Local:           +0.323024
--------------------------
Free energy:    -23.332791
Extrapolated:   -23.332791

 Band  Eigenvalues  Occupancy
    0    -32.75107    2.00000
    1    -30.42253    2.00000
    2    -30.42248    2.00000
    3    -17.54561    2.00000
    4    -14.54139    2.00000
    5    -14.20557    2.00000
    6    -14.20533    2.00000
    7    -10.70926    2.00000
    8    -10.70886    2.00000
    9     -9.77539    2.00000
   10     -9.77514    2.00000
   11     -9.18374    2.00000
   12     -9.00493    2.00000
   13      2.20799    0.00000
   14      2.97968    0.00000
   15      2.98032    0.00000
   16      4.32754    0.00000

Fermi level: -3.39847

Gap: 11.213 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.004     0.004   0.0% |
LCAO WFS Initialize:                 0.473     0.017   0.2% |
 Hamiltonian:                        0.456     0.000   0.0% |
  Atomic:                            0.016     0.016   0.2% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.001     0.001   0.0% |
  Hartree integrate/restrict:        0.013     0.013   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.200     0.008   0.1% |
   Communicate from 1D:              0.017     0.017   0.2% |
   Communicate from 2D:              0.053     0.053   0.6% |
   Communicate to 1D:                0.057     0.057   0.7% |
   Communicate to 2D:                0.018     0.018   0.2% |
   FFT 1D:                           0.017     0.017   0.2% |
   FFT 2D:                           0.030     0.030   0.3% |
  XC 3D grid:                        0.220     0.220   2.5% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.003     0.003   0.0% |
SCF-cycle:                           7.140     0.006   0.1% |
 Density:                            0.392     0.000   0.0% |
  Atomic density matrices:           0.013     0.013   0.2% |
  Mix:                               0.284     0.284   3.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.091     0.008   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.082     0.082   0.9% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        6.622     0.001   0.0% |
  Atomic:                            0.237     0.237   2.7% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.022     0.022   0.3% |
  Communicate:                       0.011     0.011   0.1% |
  Hartree integrate/restrict:        0.185     0.185   2.1% ||
  Poisson:                           2.898     0.105   1.2% |
   Communicate from 1D:              0.229     0.229   2.6% ||
   Communicate from 2D:              0.797     0.797   9.2% |---|
   Communicate to 1D:                0.802     0.802   9.3% |---|
   Communicate to 2D:                0.258     0.258   3.0% ||
   FFT 1D:                           0.255     0.255   2.9% ||
   FFT 2D:                           0.452     0.452   5.2% |-|
  XC 3D grid:                        3.217     3.217  37.1% |--------------|
  vbar:                              0.050     0.050   0.6% |
 LCAO eigensolver:                   0.121     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.008     0.008   0.1% |
  Orbital Layouts:                   0.010     0.010   0.1% |
  Potential matrix:                  0.099     0.099   1.1% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.205     0.205   2.4% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.834     0.834   9.6% |---|
-----------------------------------------------------------
Total:                                         8.662 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:23:35 2021
