
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:31:26 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4185.453435

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*128 grid
  Fine grid: 208*208*256 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*256 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 67.12 MiB
    Density: 42.00 MiB
      Arrays: 33.34 MiB
      Localized functions: 1.16 MiB
      Mixer: 7.50 MiB
    Hamiltonian: 21.90 MiB
      Arrays: 21.81 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 3.22 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.10 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.11 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 82
Number of bands in calculation: 18
Number of valence electrons: 22
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |              H                       |  
 |         |           H C H                      |  
 |         |                                      |  
 |         |             C                        |  
 |         |             C                        |  
 |         |                                      |  
 |         |             CH                       |  
 |         |           H   H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.883949    7.510348   11.536517    ( 0.0000,  0.0000,  0.0000)
   1 C      7.883949    7.510348   10.074532    ( 0.0000,  0.0000,  0.0000)
   2 C      7.883949    7.510348    8.854592    ( 0.0000,  0.0000,  0.0000)
   3 C      7.883949    7.510348    7.392607    ( 0.0000,  0.0000,  0.0000)
   4 H      7.883949    8.531044   11.929124    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.000000   11.929124    ( 0.0000,  0.0000,  0.0000)
   6 H      8.767898    7.000000   11.929124    ( 0.0000,  0.0000,  0.0000)
   7 H      7.883949    8.531044    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      8.767898    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.767898    0.000000    0.000000   104     0.1516
  2. axis:    no     0.000000   15.531044    0.000000   104     0.1493
  3. axis:    no     0.000000    0.000000   18.929124   128     0.1479

  Lengths:  15.767898  15.531044  18.929124
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1496

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:31:27                 -57.846972    1      
iter:   2  11:31:28         -0.71   -55.862430    1      
iter:   3  11:31:29         -0.89   -54.993994    1      
iter:   4  11:31:29         -1.30   -54.941225    1      
iter:   5  11:31:30         -2.03   -54.941126    1      
iter:   6  11:31:31         -2.34   -54.940536    1      
iter:   7  11:31:31         -2.63   -54.940190    1      
iter:   8  11:31:32         -3.05   -54.940142    1      
iter:   9  11:31:33         -3.49   -54.940138    1      
iter:  10  11:31:34         -3.70   -54.940138    1      
iter:  11  11:31:34         -3.88   -54.940139    1      
iter:  12  11:31:35         -4.23   -54.940139    1      
iter:  13  11:31:36         -4.51   -54.940138    1      
iter:  14  11:31:36         -4.83   -54.940138    1      
iter:  15  11:31:37         -5.09   -54.940138    1      
iter:  16  11:31:38         -5.34   -54.940138    1      
iter:  17  11:31:38         -5.60   -54.940138    1      
iter:  18  11:31:39         -5.80   -54.940138    1      
iter:  19  11:31:40         -6.17   -54.940138    1      

Converged after 19 iterations.

Dipole moment: (0.000000, 0.000099, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4185.453435)

Kinetic:        +41.899761
Potential:      -47.551375
External:        +0.000000
XC:             -49.405836
Entropy (-ST):   +0.000000
Local:           +0.117312
--------------------------
Free energy:    -54.940138
Extrapolated:   -54.940138

 Band  Eigenvalues  Occupancy
    0    -19.21904    2.00000
    1    -18.37578    2.00000
    2    -16.29475    2.00000
    3    -11.67920    2.00000
    4    -10.41208    2.00000
    5    -10.39249    2.00000
    6    -10.39225    2.00000
    7    -10.10771    2.00000
    8    -10.10744    2.00000
    9     -5.72068    2.00000
   10     -5.72067    2.00000
   11      0.89928    0.00000
   12      0.89952    0.00000
   13      2.19751    0.00000
   14      2.20652    0.00000
   15      3.78778    0.00000
   16      3.78851    0.00000
   17      4.51753    0.00000

Fermi level: -2.41070

Gap: 6.620 eV
Transition (v -> c):
  (s=0, k=0, n=10, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=11, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.007     0.007   0.0% |
LCAO WFS Initialize:                 0.655     0.023   0.2% |
 Hamiltonian:                        0.632     0.000   0.0% |
  Atomic:                            0.020     0.004   0.0% |
   XC Correction:                    0.016     0.016   0.1% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.016     0.016   0.1% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.329     0.009   0.1% |
   Communicate from 1D:              0.065     0.065   0.5% |
   Communicate from 2D:              0.065     0.065   0.5% |
   Communicate to 1D:                0.067     0.067   0.5% |
   Communicate to 2D:                0.071     0.071   0.5% |
   FFT 1D:                           0.015     0.015   0.1% |
   FFT 2D:                           0.036     0.036   0.3% |
  XC 3D grid:                        0.259     0.259   1.8% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          12.524     0.007   0.1% |
 Density:                            0.766     0.000   0.0% |
  Atomic density matrices:           0.047     0.047   0.3% |
  Mix:                               0.437     0.437   3.1% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.276     0.011   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.263     0.263   1.8% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       11.380     0.001   0.0% |
  Atomic:                            0.359     0.076   0.5% |
   XC Correction:                    0.283     0.283   2.0% ||
  Calculate atomic Hamiltonians:     0.041     0.041   0.3% |
  Communicate:                       0.001     0.001   0.0% |
  Hartree integrate/restrict:        0.308     0.308   2.2% ||
  Poisson:                           6.002     0.131   0.9% |
   Communicate from 1D:              1.213     1.213   8.5% |--|
   Communicate from 2D:              1.211     1.211   8.5% |--|
   Communicate to 1D:                1.218     1.218   8.6% |--|
   Communicate to 2D:                1.312     1.312   9.2% |---|
   FFT 1D:                           0.279     0.279   2.0% ||
   FFT 2D:                           0.638     0.638   4.5% |-|
  XC 3D grid:                        4.596     4.596  32.3% |------------|
  vbar:                              0.072     0.072   0.5% |
 LCAO eigensolver:                   0.370     0.001   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.029     0.029   0.2% |
  Orbital Layouts:                   0.021     0.021   0.1% |
  Potential matrix:                  0.317     0.317   2.2% ||
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.074     0.074   0.5% |
mktci:                               0.002     0.002   0.0% |
Other:                               0.961     0.961   6.8% |--|
-----------------------------------------------------------
Total:                                        14.227 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:31:40 2021
