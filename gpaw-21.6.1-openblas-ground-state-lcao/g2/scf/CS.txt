
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:09:43 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16.0
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/S.PBE.gz
  compensation charges: gauss, rc=0.24, lmax=2
  cutoffs: 1.77(filt), 1.66(core),
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

Reference energy: -11885.248013

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 463.51 MiB
  Calculator: 41.50 MiB
    Density: 26.63 MiB
      Arrays: 21.10 MiB
      Localized functions: 0.80 MiB
      Mixer: 4.73 MiB
    Hamiltonian: 13.87 MiB
      Arrays: 13.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.00 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.99 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 8
Number of valence electrons: 10
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            S                    |  
 |        |                                 |  
 |        |            C                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 C      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 S      7.000000    7.000000    8.544650    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.544650   104     0.1495

  Lengths:  14.000000  14.000000  15.544650
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1513

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:09:44                 -10.163340    1      
iter:   2  11:09:44         -0.84    -9.567740    1      
iter:   3  11:09:45         -1.08    -9.475046    1      
iter:   4  11:09:45         -1.27    -9.420903    1      
iter:   5  11:09:45         -1.96    -9.417955    1      
iter:   6  11:09:46         -2.33    -9.417517    1      
iter:   7  11:09:46         -2.64    -9.417405    1      
iter:   8  11:09:47         -3.20    -9.417396    1      
iter:   9  11:09:47         -3.41    -9.417393    1      
iter:  10  11:09:48         -3.91    -9.417393    1      
iter:  11  11:09:48         -4.25    -9.417393    1      
iter:  12  11:09:48         -4.81    -9.417393    1      
iter:  13  11:09:49         -5.40    -9.417393    1      
iter:  14  11:09:49         -5.64    -9.417393    1      
iter:  15  11:09:50         -6.02    -9.417393    1      

Converged after 15 iterations.

Dipole moment: (0.000000, 0.000000, 0.399701) |e|*Ang

Energy contributions relative to reference atoms: (reference = -11885.248013)

Kinetic:        +13.938530
Potential:      -14.564165
External:        +0.000000
XC:              -8.763086
Entropy (-ST):   +0.000000
Local:           -0.028672
--------------------------
Free energy:     -9.417393
Extrapolated:    -9.417393

 Band  Eigenvalues  Occupancy
    0    -20.55357    2.00000
    1    -12.61503    2.00000
    2     -8.46943    2.00000
    3     -8.46943    2.00000
    4     -6.81112    2.00000
    5     -2.86817    0.00000
    6     -2.86817    0.00000
    7      2.65457    0.00000

Fermi level: -4.83964

Gap: 3.943 eV
Transition (v -> c):
  (s=0, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.002     0.002   0.0% |
LCAO WFS Initialize:                 0.411     0.014   0.2% |
 Hamiltonian:                        0.397     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.020     0.020   0.3% |
  Hartree integrate/restrict:        0.009     0.009   0.1% |
  Initialize Hamiltonian:            0.000     0.000   0.0% |
  Poisson:                           0.205     0.006   0.1% |
   Communicate from 1D:              0.042     0.042   0.6% |
   Communicate from 2D:              0.042     0.042   0.6% |
   Communicate to 1D:                0.041     0.041   0.6% |
   Communicate to 2D:                0.042     0.042   0.6% |
   FFT 1D:                           0.010     0.010   0.1% |
   FFT 2D:                           0.021     0.021   0.3% |
  XC 3D grid:                        0.160     0.160   2.3% ||
  vbar:                              0.002     0.002   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           5.731     0.006   0.1% |
 Density:                            0.244     0.000   0.0% |
  Atomic density matrices:           0.011     0.011   0.2% |
  Mix:                               0.198     0.198   2.9% ||
  Multipole moments:                 0.001     0.001   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.032     0.005   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.026     0.026   0.4% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        5.439     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.005     0.005   0.1% |
  Communicate:                       0.280     0.280   4.0% |-|
  Hartree integrate/restrict:        0.147     0.147   2.1% ||
  Poisson:                           2.844     0.063   0.9% |
   Communicate from 1D:              0.591     0.591   8.5% |--|
   Communicate from 2D:              0.586     0.586   8.5% |--|
   Communicate to 1D:                0.571     0.571   8.3% |--|
   Communicate to 2D:                0.587     0.587   8.5% |--|
   FFT 1D:                           0.144     0.144   2.1% ||
   FFT 2D:                           0.301     0.301   4.4% |-|
  XC 3D grid:                        2.125     2.125  30.7% |-----------|
  vbar:                              0.035     0.035   0.5% |
 LCAO eigensolver:                   0.042     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.007     0.007   0.1% |
  Orbital Layouts:                   0.004     0.004   0.1% |
  Potential matrix:                  0.029     0.029   0.4% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.134     0.134   1.9% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.639     0.639   9.2% |---|
-----------------------------------------------------------
Total:                                         6.919 100.0%

Memory usage: 463.51 MiB
Date: Fri Aug  6 11:09:50 2021
