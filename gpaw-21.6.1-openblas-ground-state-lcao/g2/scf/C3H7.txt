
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:29:21 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3170.315482

Spin-polarized calculation.
Magnetic moment: 1.000000

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: <gpaw.occupations.FixMagneticMomentOccupationNumberCalculator object at 0x7f907b43b670> 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*112*120 grid
  Fine grid: 208*224*240 grid
  Total Charge: 0.000000 

Density mixing:
  Method: difference
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*224*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 592.07 MiB
  Calculator: 95.58 MiB
    Density: 58.97 MiB
      Arrays: 45.31 MiB
      Localized functions: 1.04 MiB
      Mixer: 12.63 MiB
    Hamiltonian: 33.75 MiB
      Arrays: 33.67 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.86 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.08 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.77 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 74
Number of bands in calculation: 16
Number of valence electrons: 19
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |              H                       |  
 |         |                                      |  
 |         |           H CHH                      |  
 |         |              C                       |  
 |         |                                      |  
 |         |           H CHH                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.969380    8.539158    9.138477    ( 0.0000,  0.0000,  1.0000)
   1 C      7.969380    7.795566   10.430049    ( 0.0000,  0.0000,  0.0000)
   2 C      7.969380    7.795566    7.846905    ( 0.0000,  0.0000,  0.0000)
   3 H      7.632267    9.570637    9.138477    ( 0.0000,  0.0000,  0.0000)
   4 H      8.176574    8.454482   11.276954    ( 0.0000,  0.0000,  0.0000)
   5 H      8.176574    8.454482    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.310679   10.623110    ( 0.0000,  0.0000,  0.0000)
   7 H      8.722338    7.000000   10.424716    ( 0.0000,  0.0000,  0.0000)
   8 H      8.722338    7.000000    7.852238    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.310679    7.653844    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.722338    0.000000    0.000000   104     0.1512
  2. axis:    no     0.000000   16.570637    0.000000   112     0.1480
  3. axis:    no     0.000000    0.000000   18.276954   120     0.1523

  Lengths:  15.722338  16.570637  18.276954
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1505

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson  magmom
iter:   1  11:29:23                 -52.618129    1        +0.9982
iter:   2  11:29:25         -0.69   -50.987789    1        +0.9995
iter:   3  11:29:26         -0.88   -50.317449    1        +1.0000
iter:   4  11:29:27         -1.22   -50.228812    1        +1.0000
iter:   5  11:29:28         -2.13   -50.228631    1        +1.0000
iter:   6  11:29:29         -2.41   -50.228348    1        +1.0000
iter:   7  11:29:31         -2.61   -50.228121    1        +1.0000
iter:   8  11:29:32         -3.16   -50.228104    1        +1.0000
iter:   9  11:29:33         -3.49   -50.228101    1        +1.0000
iter:  10  11:29:34         -3.72   -50.228102    1        +1.0000
iter:  11  11:29:35         -3.93   -50.228102    1        +1.0000
iter:  12  11:29:37         -4.23   -50.228102    1        +1.0000
iter:  13  11:29:38         -4.57   -50.228102    1        +1.0000
iter:  14  11:29:39         -4.83   -50.228102    1        +1.0000
iter:  15  11:29:40         -5.11   -50.228102    1        +1.0000
iter:  16  11:29:41         -5.36   -50.228102    1        +1.0000
iter:  17  11:29:43         -5.60   -50.228102    1        +1.0000
iter:  18  11:29:44         -5.86   -50.228102    1        +1.0000
iter:  19  11:29:45         -6.19   -50.228102    1        +1.0000

Converged after 19 iterations.

Dipole moment: (-0.038637, -0.038252, -0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000002)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.313288)
   1 C  ( 0.000000,  0.000000, -0.013294)
   2 C  ( 0.000000,  0.000000, -0.013294)
   3 H  ( 0.000000,  0.000000, -0.005978)
   4 H  ( 0.000000,  0.000000,  0.003172)
   5 H  ( 0.000000,  0.000000,  0.003172)
   6 H  ( 0.000000,  0.000000,  0.024041)
   7 H  ( 0.000000,  0.000000,  0.008134)
   8 H  ( 0.000000,  0.000000,  0.008134)
   9 H  ( 0.000000,  0.000000,  0.024041)

Energy contributions relative to reference atoms: (reference = -3170.315482)

Kinetic:        +40.913799
Potential:      -45.687403
External:        +0.000000
XC:             -45.534436
Entropy (-ST):   +0.000000
Local:           +0.079937
--------------------------
Free energy:    -50.228102
Extrapolated:   -50.228102

Spin contamination: 0.058034 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -19.28943    1.00000    -18.89888    1.00000
    1    -16.99268    1.00000    -16.93160    1.00000
    2    -13.87251    1.00000    -13.39176    1.00000
    3    -10.95932    1.00000    -10.82719    1.00000
    4    -10.36119    1.00000    -10.01606    1.00000
    5     -9.84253    1.00000     -9.69999    1.00000
    6     -9.26223    1.00000     -9.14703    1.00000
    7     -8.43679    1.00000     -8.32878    1.00000
    8     -8.24251    1.00000     -8.09921    1.00000
    9     -3.94832    1.00000     -1.82204    0.00000
   10      1.99044    0.00000      2.07315    0.00000
   11      2.70071    0.00000      2.81619    0.00000
   12      2.98168    0.00000      3.07183    0.00000
   13      3.68858    0.00000      3.86094    0.00000
   14      4.01813    0.00000      4.25707    0.00000
   15      4.18528    0.00000      4.26034    0.00000

Fermi levels: -0.97894, -4.96063

Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.007     0.007   0.0% |
LCAO WFS Initialize:                 1.111     0.040   0.2% |
 Hamiltonian:                        1.072     0.003   0.0% |
  Atomic:                            0.035     0.005   0.0% |
   XC Correction:                    0.030     0.030   0.1% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.032     0.032   0.1% |
  Initialize Hamiltonian:            0.000     0.000   0.0% |
  Poisson:                           0.332     0.009   0.0% |
   Communicate from 1D:              0.067     0.067   0.3% |
   Communicate from 2D:              0.066     0.066   0.3% |
   Communicate to 1D:                0.067     0.067   0.3% |
   Communicate to 2D:                0.073     0.073   0.3% |
   FFT 1D:                           0.015     0.015   0.1% |
   FFT 2D:                           0.035     0.035   0.1% |
  XC 3D grid:                        0.662     0.662   2.8% ||
  vbar:                              0.006     0.006   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          21.485     0.019   0.1% |
 Density:                            1.503     0.000   0.0% |
  Atomic density matrices:           0.157     0.157   0.7% |
  Mix:                               0.805     0.805   3.4% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.006     0.006   0.0% |
  Pseudo density:                    0.534     0.024   0.1% |
   Calculate density matrix:         0.002     0.002   0.0% |
   Construct density:                0.507     0.507   2.1% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       19.175     0.043   0.2% |
  Atomic:                            0.635     0.089   0.4% |
   XC Correction:                    0.545     0.545   2.3% ||
  Calculate atomic Hamiltonians:     0.035     0.035   0.1% |
  Communicate:                       0.001     0.001   0.0% |
  Hartree integrate/restrict:        0.543     0.543   2.3% ||
  Poisson:                           5.986     0.136   0.6% |
   Communicate from 1D:              1.217     1.217   5.1% |-|
   Communicate from 2D:              1.193     1.193   5.0% |-|
   Communicate to 1D:                1.247     1.247   5.3% |-|
   Communicate to 2D:                1.304     1.304   5.5% |-|
   FFT 1D:                           0.270     0.270   1.1% |
   FFT 2D:                           0.620     0.620   2.6% ||
  XC 3D grid:                       11.860    11.860  50.1% |-------------------|
  vbar:                              0.072     0.072   0.3% |
 LCAO eigensolver:                   0.788     0.002   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.003     0.003   0.0% |
  Distribute overlap matrix:         0.126     0.126   0.5% |
  Orbital Layouts:                   0.034     0.034   0.1% |
  Potential matrix:                  0.621     0.621   2.6% ||
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.075     0.075   0.3% |
mktci:                               0.002     0.002   0.0% |
Other:                               1.009     1.009   4.3% |-|
-----------------------------------------------------------
Total:                                        23.694 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:29:45 2021
