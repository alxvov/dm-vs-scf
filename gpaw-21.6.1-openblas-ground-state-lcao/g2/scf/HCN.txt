
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:32:55 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7.0
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/N.PBE.gz
  compensation charges: gauss, rc=0.18, lmax=2
  cutoffs: 1.11(filt), 0.96(core),
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2521.839782

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 42.60 MiB
    Density: 27.22 MiB
      Arrays: 21.92 MiB
      Localized functions: 0.39 MiB
      Mixer: 4.91 MiB
    Hamiltonian: 14.37 MiB
      Arrays: 14.34 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 1.01 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.00 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 31
Number of bands in calculation: 9
Number of valence electrons: 10
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |            C                    |  
 |        |            H                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 C      7.000000    7.000000    8.068999    ( 0.0000,  0.0000,  0.0000)
   1 N      7.000000    7.000000    9.245207    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.245207   108     0.1504

  Lengths:  14.000000  14.000000  16.245207
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1516

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:32:57                 -19.565275    1      
iter:   2  11:32:57         -0.82   -18.975723    1      
iter:   3  11:32:58         -1.01   -18.810841    1      
iter:   4  11:32:58         -1.19   -18.741289    1      
iter:   5  11:32:58         -1.74   -18.733920    1      
iter:   6  11:32:59         -1.99   -18.729301    1      
iter:   7  11:32:59         -2.77   -18.729263    1      
iter:   8  11:33:00         -3.03   -18.729255    1      
iter:   9  11:33:00         -3.17   -18.729248    1      
iter:  10  11:33:01         -3.84   -18.729248    1      
iter:  11  11:33:01         -4.16   -18.729248    1      
iter:  12  11:33:01         -4.67   -18.729248    1      
iter:  13  11:33:02         -4.97   -18.729248    1      
iter:  14  11:33:02         -5.15   -18.729248    1      
iter:  15  11:33:03         -5.53   -18.729248    1      
iter:  16  11:33:03         -5.66   -18.729248    1      
iter:  17  11:33:03         -6.18   -18.729248    1      

Converged after 17 iterations.

Dipole moment: (0.000000, 0.000000, -0.546262) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2521.839782)

Kinetic:        +12.998044
Potential:      -16.247834
External:        +0.000000
XC:             -15.602351
Entropy (-ST):   +0.000000
Local:           +0.122894
--------------------------
Free energy:    -18.729248
Extrapolated:   -18.729248

 Band  Eigenvalues  Occupancy
    0    -22.78659    2.00000
    1    -14.99999    2.00000
    2     -8.92575    2.00000
    3     -8.70658    2.00000
    4     -8.70658    2.00000
    5     -0.77171    0.00000
    6     -0.77171    0.00000
    7      1.74108    0.00000
    8      5.96533    0.00000

Fermi level: -4.73914

Gap: 7.935 eV
Transition (v -> c):
  (s=0, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.002     0.002   0.0% |
LCAO WFS Initialize:                 0.427     0.016   0.2% |
 Hamiltonian:                        0.411     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.016     0.016   0.2% |
  Hartree integrate/restrict:        0.011     0.011   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.210     0.005   0.1% |
   Communicate from 1D:              0.044     0.044   0.6% |
   Communicate from 2D:              0.043     0.043   0.5% |
   Communicate to 1D:                0.042     0.042   0.5% |
   Communicate to 2D:                0.044     0.044   0.5% |
   FFT 1D:                           0.010     0.010   0.1% |
   FFT 2D:                           0.022     0.022   0.3% |
  XC 3D grid:                        0.169     0.169   2.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           6.739     0.006   0.1% |
 Density:                            0.314     0.000   0.0% |
  Atomic density matrices:           0.026     0.026   0.3% |
  Mix:                               0.247     0.247   3.1% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.038     0.006   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.031     0.031   0.4% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        6.354     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.005     0.005   0.1% |
  Communicate:                       0.258     0.258   3.2% ||
  Hartree integrate/restrict:        0.161     0.161   2.0% ||
  Poisson:                           3.357     0.073   0.9% |
   Communicate from 1D:              0.708     0.708   8.8% |---|
   Communicate from 2D:              0.696     0.696   8.7% |--|
   Communicate to 1D:                0.679     0.679   8.4% |--|
   Communicate to 2D:                0.700     0.700   8.7% |--|
   FFT 1D:                           0.153     0.153   1.9% ||
   FFT 2D:                           0.348     0.348   4.3% |-|
  XC 3D grid:                        2.527     2.527  31.5% |------------|
  vbar:                              0.042     0.042   0.5% |
 LCAO eigensolver:                   0.064     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.022     0.022   0.3% |
  Orbital Layouts:                   0.006     0.006   0.1% |
  Potential matrix:                  0.034     0.034   0.4% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.202     0.202   2.5% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.660     0.660   8.2% |--|
-----------------------------------------------------------
Total:                                         8.033 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:33:03 2021
