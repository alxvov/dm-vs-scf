
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:17:33 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8.0
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7.0
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/N.PBE.gz
  compensation charges: gauss, rc=0.18, lmax=2
  cutoffs: 1.11(filt), 0.96(core),
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

Reference energy: -6628.524709

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 112*108*104 grid
  Fine grid: 224*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [2, 1, 0]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 59.84 MiB
    Density: 38.03 MiB
      Arrays: 30.27 MiB
      Localized functions: 0.96 MiB
      Mixer: 6.81 MiB
    Hamiltonian: 19.87 MiB
      Arrays: 19.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.93 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.85 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 67
Number of bands in calculation: 19
Number of valence electrons: 24
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-----------------------------------------.  
          /|                                         |  
         / |                                         |  
        /  |                                         |  
       /   |                                         |  
      /    |                                         |  
     /     |                                         |  
    /      |                                         |  
   /       |                                         |  
  /        |                                         |  
 *         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |            HC   O                       |  
 |         |                 O N                     |  
 |         |            H                            |  
 |         |                                         |  
 |         |                                         |  
 |         .-----------------------------------------.  
 |        /                                         /   
 |       /                                         /    
 |      /                                         /     
 |     /                                         /      
 |    /                                         /       
 |   /                                         /        
 |  /                                         /         
 | /                                         /          
 |/                                         /           
 *-----------------------------------------*            

Positions:
   0 C      7.669330    8.487663    7.890672    ( 0.0000,  0.0000,  0.0000)
   1 O      8.985538    9.075268    7.890672    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    9.344429    7.890672    ( 0.0000,  0.0000,  0.0000)
   3 H      7.521202    7.873779    8.781344    ( 0.0000,  0.0000,  0.0000)
   4 H      7.521202    7.873779    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 N     10.030872    8.155601    7.890672    ( 0.0000,  0.0000,  0.0000)
   6 O      9.672302    7.000000    7.890672    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.030872    0.000000    0.000000   112     0.1521
  2. axis:    no     0.000000   16.344429    0.000000   108     0.1513
  3. axis:    no     0.000000    0.000000   15.781344   104     0.1517

  Lengths:  17.030872  16.344429  15.781344
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1517

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:17:35                 -39.135310    1      
iter:   2  11:17:36         -0.87   -37.988412    1      
iter:   3  11:17:36         -1.06   -37.577676    1      
iter:   4  11:17:37         -1.35   -37.495284    1      
iter:   5  11:17:37         -1.91   -37.489065    1      
iter:   6  11:17:38         -2.19   -37.486383    1      
iter:   7  11:17:38         -2.47   -37.485387    1      
iter:   8  11:17:39         -2.77   -37.485130    1      
iter:   9  11:17:39         -2.98   -37.485014    1      
iter:  10  11:17:40         -3.33   -37.484998    1      
iter:  11  11:17:40         -3.57   -37.484994    1      
iter:  12  11:17:41         -3.75   -37.484993    1      
iter:  13  11:17:41         -4.05   -37.484992    1      
iter:  14  11:17:42         -4.27   -37.484992    1      
iter:  15  11:17:42         -4.53   -37.484992    1      
iter:  16  11:17:43         -4.81   -37.484992    1      
iter:  17  11:17:43         -5.00   -37.484992    1      
iter:  18  11:17:44         -5.21   -37.484992    1      
iter:  19  11:17:44         -5.47   -37.484992    1      
iter:  20  11:17:45         -5.71   -37.484992    1      
iter:  21  11:17:46         -5.95   -37.484992    1      
iter:  22  11:17:46         -6.18   -37.484992    1      

Converged after 22 iterations.

Dipole moment: (-0.438287, 0.067780, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6628.524709)

Kinetic:        +34.766771
Potential:      -37.005161
External:        +0.000000
XC:             -35.499488
Entropy (-ST):   +0.000000
Local:           +0.252886
--------------------------
Free energy:    -37.484992
Extrapolated:   -37.484992

 Band  Eigenvalues  Occupancy
    0    -30.17228    2.00000
    1    -26.25810    2.00000
    2    -18.94489    2.00000
    3    -15.34499    2.00000
    4    -13.27855    2.00000
    5    -12.42042    2.00000
    6    -12.10657    2.00000
    7    -10.59231    2.00000
    8     -9.97130    2.00000
    9     -8.73469    2.00000
   10     -7.67621    2.00000
   11     -6.10052    2.00000
   12     -2.93962    0.00000
   13      1.92604    0.00000
   14      2.02569    0.00000
   15      2.72592    0.00000
   16      3.50698    0.00000
   17      3.92356    0.00000
   18      7.46167    0.00000

Fermi level: -4.52007

Gap: 3.161 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.005     0.005   0.0% |
LCAO WFS Initialize:                 0.506     0.019   0.1% |
 Hamiltonian:                        0.487     0.000   0.0% |
  Atomic:                            0.011     0.011   0.1% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.006     0.006   0.0% |
  Hartree integrate/restrict:        0.015     0.015   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.212     0.007   0.1% |
   Communicate from 1D:              0.019     0.019   0.1% |
   Communicate from 2D:              0.056     0.056   0.4% |
   Communicate to 1D:                0.061     0.061   0.5% |
   Communicate to 2D:                0.018     0.018   0.1% |
   FFT 1D:                           0.019     0.019   0.1% |
   FFT 2D:                           0.031     0.031   0.2% |
  XC 3D grid:                        0.237     0.237   1.8% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          10.980     0.009   0.1% |
 Density:                            0.660     0.000   0.0% |
  Atomic density matrices:           0.077     0.077   0.6% |
  Mix:                               0.443     0.443   3.5% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.134     0.012   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.121     0.121   0.9% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       10.077     0.002   0.0% |
  Atomic:                            0.212     0.212   1.7% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.052     0.052   0.4% |
  Communicate:                       0.127     0.127   1.0% |
  Hartree integrate/restrict:        0.269     0.269   2.1% ||
  Poisson:                           4.439     0.144   1.1% |
   Communicate from 1D:              0.386     0.386   3.0% ||
   Communicate from 2D:              1.194     1.194   9.3% |---|
   Communicate to 1D:                1.289     1.289  10.0% |---|
   Communicate to 2D:                0.382     0.382   3.0% ||
   FFT 1D:                           0.398     0.398   3.1% ||
   FFT 2D:                           0.647     0.647   5.0% |-|
  XC 3D grid:                        4.899     4.899  38.2% |--------------|
  vbar:                              0.076     0.076   0.6% |
 LCAO eigensolver:                   0.235     0.001   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.068     0.068   0.5% |
  Orbital Layouts:                   0.018     0.018   0.1% |
  Potential matrix:                  0.144     0.144   1.1% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.391     0.391   3.0% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.950     0.950   7.4% |--|
-----------------------------------------------------------
Total:                                        12.837 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:17:46 2021
