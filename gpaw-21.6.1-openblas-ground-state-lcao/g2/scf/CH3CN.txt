
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:17:11 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7.0
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/N.PBE.gz
  compensation charges: gauss, rc=0.18, lmax=2
  cutoffs: 1.11(filt), 0.96(core),
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1.0
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3574.448222

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*112 grid
  Fine grid: 208*208*224 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 566.29 MiB
  Calculator: 57.42 MiB
    Density: 36.40 MiB
      Arrays: 29.14 MiB
      Localized functions: 0.71 MiB
      Mixer: 6.55 MiB
    Hamiltonian: 19.12 MiB
      Arrays: 19.06 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.91 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.86 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 54
Number of bands in calculation: 14
Number of valence electrons: 16
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |             N                        |  
 |         |             C                        |  
 |         |                                      |  
 |         |             CH                       |  
 |         |           H   H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.887664    7.512493    7.375440    ( 0.0000,  0.0000,  0.0000)
   1 C      7.887664    7.512493    8.836244    ( 0.0000,  0.0000,  0.0000)
   2 N      7.887664    7.512493   10.014576    ( 0.0000,  0.0000,  0.0000)
   3 H      7.887664    8.537479    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      8.775328    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.775328    0.000000    0.000000   104     0.1517
  2. axis:    no     0.000000   15.537479    0.000000   104     0.1494
  3. axis:    no     0.000000    0.000000   17.014576   112     0.1519

  Lengths:  15.775328  15.537479  17.014576
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1510

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:17:13                 -37.066600    1      
iter:   2  11:17:14         -0.75   -35.883640    1      
iter:   3  11:17:14         -0.96   -35.539473    1      
iter:   4  11:17:15         -1.15   -35.415468    1      
iter:   5  11:17:15         -1.66   -35.405035    1      
iter:   6  11:17:16         -2.01   -35.399945    1      
iter:   7  11:17:17         -2.56   -35.399642    1      
iter:   8  11:17:17         -2.88   -35.399577    1      
iter:   9  11:17:18         -3.13   -35.399555    1      
iter:  10  11:17:18         -3.54   -35.399553    1      
iter:  11  11:17:19         -3.73   -35.399552    1      
iter:  12  11:17:19         -4.16   -35.399552    1      
iter:  13  11:17:20         -4.75   -35.399552    1      
iter:  14  11:17:21         -4.96   -35.399552    1      
iter:  15  11:17:21         -5.36   -35.399552    1      
iter:  16  11:17:22         -5.52   -35.399552    1      
iter:  17  11:17:22         -6.19   -35.399552    1      

Converged after 17 iterations.

Dipole moment: (0.000000, -0.000015, -0.762998) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3574.448222)

Kinetic:        +25.937761
Potential:      -30.767954
External:        +0.000000
XC:             -30.718749
Entropy (-ST):   +0.000000
Local:           +0.149390
--------------------------
Free energy:    -35.399552
Extrapolated:   -35.399552

 Band  Eigenvalues  Occupancy
    0    -22.29453    2.00000
    1    -19.31527    2.00000
    2    -12.70707    2.00000
    3    -11.37006    2.00000
    4    -11.36998    2.00000
    5     -8.28808    2.00000
    6     -7.80149    2.00000
    7     -7.80099    2.00000
    8     -0.08256    0.00000
    9     -0.08208    0.00000
   10      1.43953    0.00000
   11      3.44988    0.00000
   12      3.44997    0.00000
   13      4.04460    0.00000

Fermi level: -3.94177

Gap: 7.718 eV
Transition (v -> c):
  (s=0, k=0, n=7, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.005     0.005   0.0% |
LCAO WFS Initialize:                 0.572     0.018   0.2% |
 Hamiltonian:                        0.555     0.000   0.0% |
  Atomic:                            0.010     0.010   0.1% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.007     0.007   0.1% |
  Hartree integrate/restrict:        0.013     0.013   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.291     0.007   0.1% |
   Communicate from 1D:              0.061     0.061   0.5% |
   Communicate from 2D:              0.057     0.057   0.5% |
   Communicate to 1D:                0.062     0.062   0.6% |
   Communicate to 2D:                0.060     0.060   0.5% |
   FFT 1D:                           0.014     0.014   0.1% |
   FFT 2D:                           0.030     0.030   0.3% |
  XC 3D grid:                        0.227     0.227   2.0% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                           9.435     0.007   0.1% |
 Density:                            0.460     0.000   0.0% |
  Atomic density matrices:           0.016     0.016   0.1% |
  Mix:                               0.296     0.296   2.7% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.143     0.009   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.133     0.133   1.2% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        8.786     0.001   0.0% |
  Atomic:                            0.164     0.164   1.5% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.017     0.017   0.2% |
  Communicate:                       0.124     0.124   1.1% |
  Hartree integrate/restrict:        0.209     0.209   1.9% ||
  Poisson:                           4.656     0.104   0.9% |
   Communicate from 1D:              0.979     0.979   8.8% |---|
   Communicate from 2D:              0.914     0.914   8.2% |--|
   Communicate to 1D:                0.996     0.996   9.0% |---|
   Communicate to 2D:                0.956     0.956   8.6% |--|
   FFT 1D:                           0.224     0.224   2.0% ||
   FFT 2D:                           0.483     0.483   4.4% |-|
  XC 3D grid:                        3.558     3.558  32.0% |------------|
  vbar:                              0.056     0.056   0.5% |
 LCAO eigensolver:                   0.183     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.012     0.012   0.1% |
  Orbital Layouts:                   0.010     0.010   0.1% |
  Potential matrix:                  0.158     0.158   1.4% ||
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.202     0.202   1.8% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.884     0.884   8.0% |--|
-----------------------------------------------------------
Total:                                        11.103 100.0%

Memory usage: 566.29 MiB
Date: Fri Aug  6 11:17:22 2021
