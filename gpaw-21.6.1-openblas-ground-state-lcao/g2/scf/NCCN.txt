
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  21.6.1b1
 |___|_|             

User:   ali5@compute-14
Date:   Fri Aug  6 11:30:14 2021
Arch:   x86_64
Pid:    1418133
Python: 3.8.9
gpaw:   /users/home/aleksei/gpawproject/gpaw/gpaw (5e46c27e61)
_gpaw:  /users/home/aleksei/gpawproject/gpaw/build/lib.linux-x86_64-3.8/
        _gpaw.cpython-38-x86_64-linux-gnu.so (5e46c27e61)
ase:    /users/home/aleksei/gpawproject/ase/ase (version 3.22.0b1-5c4361a7fd)
numpy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-numpy-1.20.2-dgf27bhjua75jm66wbtas3vnjet4dzqg/lib/python3.8/site-packages/numpy (version 1.20.2)
scipy:  /users/home/eojons/spack/opt/spack/linux-centos8-cascadelake/gcc-9.3.0/py-scipy-1.6.2-5mj6sw3rf63wmprfl3ofjpi64tayycfh/lib/python3.8/site-packages/scipy (version 1.6.2)
libxc:  5.1.3
units:  Angstrom and eV
cores: 8
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  basis: dzp
  convergence: {density: 1e-06,
                eigenstates: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  symmetry: off
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7.0
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/N.PBE.gz
  compensation charges: gauss, rc=0.18, lmax=2
  cutoffs: 1.11(filt), 0.96(core),
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6.0
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/eojons/gpaw-setups-0.9.20000/C.PBE.gz
  compensation charges: gauss, rc=0.20, lmax=2
  cutoffs: 1.14(filt), 1.14(core),
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/eojons/gpaw-setups-0.9.20000/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Reference energy: -5018.699239

Spin-paired calculation

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 1e-06 electrons
  Maximum integral of absolute eigenstate change: 1e+10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Occupation numbers: width=0.000 eV 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*120 grid
  Fine grid: 184*184*240 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [0, 1, 2]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 611.45 MiB
  Calculator: 48.18 MiB
    Density: 30.49 MiB
      Arrays: 24.38 MiB
      Localized functions: 0.64 MiB
      Mixer: 5.47 MiB
    Hamiltonian: 16.00 MiB
      Arrays: 15.95 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.69 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.64 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 15
Number of valence electrons: 18
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |            C                    |  
 |        |            C                    |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 N      7.000000    7.000000   10.751750    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000    7.000000    9.566448    ( 0.0000,  0.0000,  0.0000)
   2 C      7.000000    7.000000    8.185302    ( 0.0000,  0.0000,  0.0000)
   3 N      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   17.751750   120     0.1479

  Lengths:  14.000000  14.000000  17.751750
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       poisson
iter:   1  11:30:15                 -31.654825    1      
iter:   2  11:30:16         -0.90   -30.941980    1      
iter:   3  11:30:16         -1.08   -30.765847    1      
iter:   4  11:30:17         -1.24   -30.666482    1      
iter:   5  11:30:17         -1.96   -30.660594    1      
iter:   6  11:30:18         -2.16   -30.656867    1      
iter:   7  11:30:18         -3.04   -30.656844    1      
iter:   8  11:30:19         -3.21   -30.656831    1      
iter:   9  11:30:19         -3.54   -30.656830    1      
iter:  10  11:30:20         -3.73   -30.656830    1      
iter:  11  11:30:20         -4.08   -30.656830    1      
iter:  12  11:30:21         -4.20   -30.656830    1      
iter:  13  11:30:21         -4.58   -30.656830    1      
iter:  14  11:30:22         -4.85   -30.656830    1      
iter:  15  11:30:22         -5.01   -30.656830    1      
iter:  16  11:30:23         -5.92   -30.656830    1      
iter:  17  11:30:23         -6.29   -30.656830    1      

Converged after 17 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5018.699239)

Kinetic:        +18.834524
Potential:      -23.654332
External:        +0.000000
XC:             -26.079481
Entropy (-ST):   +0.000000
Local:           +0.242459
--------------------------
Free energy:    -30.656830
Extrapolated:   -30.656830

 Band  Eigenvalues  Occupancy
    0    -24.24634    2.00000
    1    -23.75842    2.00000
    2    -18.63855    2.00000
    3    -10.94679    2.00000
    4    -10.94679    2.00000
    5    -10.23824    2.00000
    6     -9.80487    2.00000
    7     -8.96193    2.00000
    8     -8.96193    2.00000
    9     -3.56720    0.00000
   10     -3.56720    0.00000
   11      1.81957    0.00000
   12      1.81957    0.00000
   13      2.19619    0.00000
   14      8.22375    0.00000

Fermi level: -6.26457

Gap: 5.395 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.003     0.003   0.0% |
LCAO WFS Initialize:                 0.473     0.016   0.2% |
 Hamiltonian:                        0.457     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.018     0.018   0.2% |
  Hartree integrate/restrict:        0.012     0.012   0.1% |
  Initialize Hamiltonian:            0.002     0.002   0.0% |
  Poisson:                           0.235     0.006   0.1% |
   Communicate from 1D:              0.049     0.049   0.6% |
   Communicate from 2D:              0.049     0.049   0.6% |
   Communicate to 1D:                0.047     0.047   0.5% |
   Communicate to 2D:                0.049     0.049   0.6% |
   FFT 1D:                           0.011     0.011   0.1% |
   FFT 2D:                           0.024     0.024   0.3% |
  XC 3D grid:                        0.187     0.187   2.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                           7.522     0.007   0.1% |
 Density:                            0.366     0.000   0.0% |
  Atomic density matrices:           0.025     0.025   0.3% |
  Mix:                               0.256     0.256   2.9% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.002     0.002   0.0% |
  Pseudo density:                    0.080     0.007   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.072     0.072   0.8% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        7.032     0.001   0.0% |
  Atomic:                            0.003     0.003   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.006     0.006   0.1% |
  Communicate:                       0.285     0.285   3.2% ||
  Hartree integrate/restrict:        0.174     0.174   2.0% ||
  Poisson:                           3.706     0.082   0.9% |
   Communicate from 1D:              0.780     0.780   8.8% |---|
   Communicate from 2D:              0.768     0.768   8.7% |--|
   Communicate to 1D:                0.758     0.758   8.6% |--|
   Communicate to 2D:                0.765     0.765   8.6% |--|
   FFT 1D:                           0.173     0.173   1.9% ||
   FFT 2D:                           0.381     0.381   4.3% |-|
  XC 3D grid:                        2.809     2.809  31.7% |------------|
  vbar:                              0.047     0.047   0.5% |
 LCAO eigensolver:                   0.117     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.019     0.019   0.2% |
  Orbital Layouts:                   0.010     0.010   0.1% |
  Potential matrix:                  0.087     0.087   1.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.000     0.000   0.0% |
TCI: Evaluate splines:               0.135     0.135   1.5% ||
mktci:                               0.001     0.001   0.0% |
Other:                               0.725     0.725   8.2% |--|
-----------------------------------------------------------
Total:                                         8.861 100.0%

Memory usage: 611.45 MiB
Date: Fri Aug  6 11:30:23 2021
