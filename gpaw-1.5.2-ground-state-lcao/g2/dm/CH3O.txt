
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:53:17 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3105.950903

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*104*104 grid
  Fine grid: 208*208*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 65.39 MiB
    Density: 36.99 MiB
      Arrays: 36.38 MiB
      Localized functions: 0.61 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 27.08 MiB
      Arrays: 27.03 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.32 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.03 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.28 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 41
Number of bands in calculation: 41
Bands to converge: occupied states only
Number of valence electrons: 13

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |           H O                       |  
 |         |            C H                      |  
 |         |           H                         |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.458740    7.417888    7.903279    ( 0.0000,  0.0000,  0.0000)
   1 O      7.458740    8.803904    7.903279    ( 0.0000,  0.0000,  1.0000)
   2 H      8.522721    7.135607    7.903279    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.000000    8.806558    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.522721    0.000000    0.000000   104     0.1493
  2. axis:    no     0.000000   15.803904    0.000000   104     0.1520
  3. axis:    no     0.000000    0.000000   15.806558   104     0.1520

  Lengths:  15.522721  15.803904  15.806558
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1511

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  22:53:28  +0.66          -23.417263    0      1        +1.0000
iter:   2  22:53:30  +0.48          -23.440378    0      1        +1.0000
iter:   3  22:53:32  -0.75          -23.452439    0      1        +1.0000
iter:   4  22:53:33  -1.14          -23.453116    0      1        +1.0000
iter:   5  22:53:35  -2.96          -23.453468    0      1        +1.0000
iter:   6  22:53:37  -3.54          -23.453481    0      1        +1.0000
iter:   7  22:53:39  -4.89          -23.453487    0      1        +1.0000
iter:   8  22:53:41  -5.24          -23.453487    0      1        +1.0000
iter:   9  22:53:43  -5.41          -23.453487    0      1        +1.0000
iter:  10  22:53:45  -6.90          -23.453487    0      1        +1.0000
iter:  11  22:53:47  -7.70          -23.453487    0      1        +1.0000
iter:  12  22:53:49  -8.38          -23.453487    0      1        +1.0000
iter:  13  22:53:51  -8.93          -23.453487    0      1        +1.0000
iter:  14  22:53:53  -9.34          -23.453487    0      1        +1.0000
iter:  15  22:53:55  -9.28          -23.453487    0      1        +1.0000
iter:  16  22:53:59 -10.74          -23.453487    0      1        +1.0000

Converged after 16 iterations.

Dipole moment: (0.044306, -0.384234, 0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000006)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000, -0.003320)
   1 O  ( 0.000000,  0.000000,  0.579575)
   2 H  ( 0.000000,  0.000000,  0.043275)
   3 H  ( 0.000000,  0.000000,  0.008644)
   4 H  ( 0.000000,  0.000000,  0.008644)

Energy contributions relative to reference atoms: (reference = -3105.950903)

Kinetic:        +21.293661
Potential:      -22.377204
External:        +0.000000
XC:             -22.469079
Entropy (-ST):   +0.000000
Local:           +0.099135
--------------------------
Free energy:    -23.453487
Extrapolated:   -23.453487

Spin contamination: 0.029320 electrons
Fermi levels: -2.05841, -5.22647

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -25.09560    1.00000    -23.98710    1.00000
    1    -16.57929    1.00000    -16.20216    1.00000
    2    -11.60969    1.00000    -10.95291    1.00000
    3    -11.05528    1.00000    -10.91945    1.00000
    4    -10.14832    1.00000     -9.64018    1.00000
    5     -7.43729    1.00000     -5.56216    1.00000
    6     -6.07632    1.00000     -4.89077    0.00000
    7      1.95950    0.00000      2.07885    0.00000
    8      3.17587    0.00000      3.39047    0.00000
    9      3.68824    0.00000      3.72487    0.00000
   10      3.69462    0.00000      3.98467    0.00000
   11      8.83487    0.00000      9.09720    0.00000
   12      9.46553    0.00000      9.59437    0.00000
   13     11.00329    0.00000     11.18323    0.00000
   14     13.16950    0.00000     13.46476    0.00000
   15     14.62355    0.00000     14.77549    0.00000
   16     14.92893    0.00000     14.96316    0.00000
   17     16.03676    0.00000     16.81648    0.00000
   18     16.62135    0.00000     17.03881    0.00000
   19     17.50764    0.00000     17.81799    0.00000
   20     21.50250    0.00000     21.92330    0.00000
   21     21.78950    0.00000     22.19388    0.00000
   22     23.93506    0.00000     24.06658    0.00000
   23     27.16122    0.00000     27.36638    0.00000
   24     31.86620    0.00000     32.16780    0.00000
   25     32.58560    0.00000     32.76274    0.00000
   26     34.13462    0.00000     34.40667    0.00000
   27     34.99669    0.00000     35.23916    0.00000
   28     35.09608    0.00000     35.35081    0.00000
   29     40.20936    0.00000     40.56767    0.00000
   30     41.06893    0.00000     41.50948    0.00000
   31     41.19127    0.00000     41.59716    0.00000
   32     42.99198    0.00000     43.60665    0.00000
   33     46.92828    0.00000     47.70017    0.00000
   34     47.09984    0.00000     47.92870    0.00000
   35     50.36697    0.00000     50.74857    0.00000
   36     52.01407    0.00000     52.60365    0.00000
   37     52.70221    0.00000     53.24992    0.00000
   38     59.46030    0.00000     60.62076    0.00000
   39     61.85518    0.00000     62.03532    0.00000
   40     63.99962    0.00000     64.63077    0.00000

Gap: 6.850 eV
Transition (v -> c):
  (s=1, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.007     0.007   0.0% |
LCAO WFS Initialize:                   1.917     0.064   0.2% |
 Hamiltonian:                          1.853     0.003   0.0% |
  Atomic:                              0.045     0.045   0.1% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.034     0.034   0.1% |
  Hartree integrate/restrict:          0.046     0.046   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.358     0.006   0.0% |
   Communicate bwd 0:                  0.071     0.071   0.2% |
   Communicate bwd 1:                  0.065     0.065   0.2% |
   Communicate fwd 0:                  0.068     0.068   0.2% |
   Communicate fwd 1:                  0.069     0.069   0.2% |
   fft:                                0.025     0.025   0.1% |
   fft2:                               0.054     0.054   0.1% |
  XC 3D grid:                          1.357     1.357   3.3% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            37.061     0.004   0.0% |
 Direct Minimisation step:            35.112     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.259     0.005   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.003     0.003   0.0% |
   Distribute overlap matrix:          0.019     0.019   0.0% |
   Potential matrix:                   0.227     0.227   0.5% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.015     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.013     0.013   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.025     0.003   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.022     0.022   0.1% |
  Update Kohn-Sham energy:            34.804     0.000   0.0% |
   Density:                            1.389     0.000   0.0% |
    Atomic density matrices:           0.036     0.036   0.1% |
    Mix:                               1.091     1.091   2.6% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.006     0.006   0.0% |
    Pseudo density:                    0.253     0.022   0.1% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.230     0.230   0.6% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       33.415     0.036   0.1% |
    Atomic:                            0.824     0.824   2.0% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.034     0.034   0.1% |
    Communicate:                       0.608     0.608   1.5% ||
    Hartree integrate/restrict:        0.796     0.796   1.9% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           6.588     0.112   0.3% |
     Communicate bwd 0:                1.332     1.332   3.2% ||
     Communicate bwd 1:                1.213     1.213   2.9% ||
     Communicate fwd 0:                1.221     1.221   2.9% ||
     Communicate fwd 1:                1.313     1.313   3.2% ||
     fft:                              0.448     0.448   1.1% |
     fft2:                             0.950     0.950   2.3% ||
    XC 3D grid:                       24.464    24.464  59.0% |-----------------------|
    vbar:                              0.063     0.063   0.2% |
 Get canonical representation:         1.945     0.000   0.0% |
  LCAO eigensolver:                    0.015     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.013     0.013   0.0% |
  Update Kohn-Sham energy:             1.930     0.000   0.0% |
   Density:                            0.076     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.060     0.060   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.014     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.013     0.013   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.854     0.002   0.0% |
    Atomic:                            0.046     0.046   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.034     0.034   0.1% |
    Hartree integrate/restrict:        0.045     0.045   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.362     0.007   0.0% |
     Communicate bwd 0:                0.072     0.072   0.2% |
     Communicate bwd 1:                0.065     0.065   0.2% |
     Communicate fwd 0:                0.067     0.067   0.2% |
     Communicate fwd 1:                0.073     0.073   0.2% |
     fft:                              0.025     0.025   0.1% |
     fft2:                             0.053     0.053   0.1% |
    XC 3D grid:                        1.359     1.359   3.3% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.502     0.502   1.2% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.007     2.007   4.8% |-|
-------------------------------------------------------------
Total:                                          41.499 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:53:59 2019
