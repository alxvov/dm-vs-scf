
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:22:26 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3624.408871

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 116*108*104 grid
  Fine grid: 232*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 232*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 55.70 MiB
    Density: 32.36 MiB
      Arrays: 31.36 MiB
      Localized functions: 1.00 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.59 MiB
      Arrays: 20.51 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.75 MiB
      C [qnM]: 0.04 MiB
      S, T [2 x qmm]: 0.08 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.62 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 74
Number of bands in calculation: 74
Bands to converge: occupied states only
Number of valence electrons: 20

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .------------------------------------------.  
          /|                                          |  
         / |                                          |  
        /  |                                          |  
       /   |                                          |  
      /    |                                          |  
     /     |                                          |  
    /      |                                          |  
   /       |                                          |  
  /        |                                          |  
 *         |                                          |  
 |         |                                          |  
 |         |                                          |  
 |         |               H                          |  
 |         |           H   C H  H                     |  
 |         |           N   H C                        |  
 |         |           H     H                        |  
 |         |                                          |  
 |         |                                          |  
 |         .------------------------------------------.  
 |        /                                          /   
 |       /                                          /    
 |      /                                          /     
 |     /                                          /      
 |    /                                          /       
 |   /                                          /        
 |  /                                          /         
 | /                                          /          
 |/                                          /           
 *------------------------------------------*            

Positions:
   0 C      9.582340    7.644162    7.884909    ( 0.0000,  0.0000,  0.0000)
   1 C      8.372326    8.573711    7.884909    ( 0.0000,  0.0000,  0.0000)
   2 N      7.066975    7.910282    7.884909    ( 0.0000,  0.0000,  0.0000)
   3 H     10.521636    8.206258    7.884909    ( 0.0000,  0.0000,  0.0000)
   4 H      9.574122    7.000000    8.769818    ( 0.0000,  0.0000,  0.0000)
   5 H      9.574122    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      8.406887    9.228723    7.008431    ( 0.0000,  0.0000,  0.0000)
   7 H      8.406887    9.228723    8.761387    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000    7.299420    8.698041    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.299420    7.071777    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.521636    0.000000    0.000000   116     0.1510
  2. axis:    no     0.000000   16.228723    0.000000   108     0.1503
  3. axis:    no     0.000000    0.000000   15.769818   104     0.1516

  Lengths:  17.521636  16.228723  15.769818
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1510

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:22:34  +0.65          -50.705430    0      1      
iter:   2  23:22:36  +0.82          -50.732019    0      1      
iter:   3  23:22:37  -0.68          -50.813134    0      1      
iter:   4  23:22:39  -1.06          -50.815034    0      1      
iter:   5  23:22:40  -3.05          -50.815991    0      1      
iter:   6  23:22:41  -3.53          -50.816026    0      1      
iter:   7  23:22:43  -4.21          -50.816045    0      1      
iter:   8  23:22:44  -4.51          -50.816047    0      1      
iter:   9  23:22:46  -5.30          -50.816047    0      1      
iter:  10  23:22:47  -6.34          -50.816047    0      1      
iter:  11  23:22:49  -7.39          -50.816047    0      1      
iter:  12  23:22:50  -7.94          -50.816047    0      1      
iter:  13  23:22:52  -8.80          -50.816047    0      1      
iter:  14  23:22:53  -9.11          -50.816047    0      1      
iter:  15  23:22:56 -10.26          -50.816047    0      1      

Converged after 15 iterations.

Dipole moment: (0.156307, -0.174687, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3624.408871)

Kinetic:        +42.051266
Potential:      -46.720443
External:        +0.000000
XC:             -46.300964
Entropy (-ST):   +0.000000
Local:           +0.154094
--------------------------
Free energy:    -50.816047
Extrapolated:   -50.816047

Fermi levels: -1.77788, -1.77788

 Band  Eigenvalues  Occupancy
    0    -21.73815    2.00000
    1    -17.90340    2.00000
    2    -14.89343    2.00000
    3    -12.05074    2.00000
    4    -10.57534    2.00000
    5     -9.86111    2.00000
    6     -9.77125    2.00000
    7     -8.57206    2.00000
    8     -7.99668    2.00000
    9     -5.14472    2.00000
   10      1.58896    0.00000
   11      2.69135    0.00000
   12      2.76062    0.00000
   13      3.18414    0.00000
   14      3.87577    0.00000
   15      4.44740    0.00000
   16      4.61716    0.00000
   17      5.42632    0.00000
   18      6.36279    0.00000
   19      8.76874    0.00000
   20      9.11658    0.00000
   21      9.70184    0.00000
   22      9.81833    0.00000
   23     10.12102    0.00000
   24     13.33438    0.00000
   25     13.51239    0.00000
   26     14.21497    0.00000
   27     14.85944    0.00000
   28     15.30977    0.00000
   29     15.65277    0.00000
   30     16.01847    0.00000
   31     16.15975    0.00000
   32     16.62948    0.00000
   33     17.87406    0.00000
   34     18.71559    0.00000
   35     19.10331    0.00000
   36     20.58893    0.00000
   37     23.06653    0.00000
   38     24.65711    0.00000
   39     25.58257    0.00000
   40     26.07168    0.00000
   41     26.67684    0.00000
   42     28.37008    0.00000
   43     30.75431    0.00000
   44     33.29863    0.00000
   45     33.66136    0.00000
   46     34.61032    0.00000
   47     35.18015    0.00000
   48     35.73505    0.00000
   49     35.90281    0.00000
   50     36.86286    0.00000
   51     36.90167    0.00000
   52     37.34995    0.00000
   53     38.22983    0.00000
   54     39.19510    0.00000
   55     40.05046    0.00000
   56     40.73981    0.00000
   57     42.52302    0.00000
   58     42.80997    0.00000
   59     44.05656    0.00000
   60     45.16480    0.00000
   61     47.24263    0.00000
   62     47.27275    0.00000
   63     48.86993    0.00000
   64     51.04692    0.00000
   65     52.02643    0.00000
   66     52.80174    0.00000
   67     53.95079    0.00000
   68     54.83873    0.00000
   69     54.90667    0.00000
   70     57.88764    0.00000
   71     58.45708    0.00000
   72     63.49141    0.00000
   73     65.13192    0.00000

Gap: 6.734 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.001     0.001   0.0% |
Basis functions set positions:         0.014     0.014   0.0% |
LCAO WFS Initialize:                   1.381     0.041   0.1% |
 Hamiltonian:                          1.340     0.000   0.0% |
  Atomic:                              0.054     0.008   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.000     0.000   0.0% |
  Hartree integrate/restrict:          0.025     0.025   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.306     0.016   0.1% |
   Communicate bwd 0:                  0.020     0.020   0.1% |
   Communicate bwd 1:                  0.078     0.078   0.3% |
   Communicate fwd 0:                  0.076     0.076   0.3% |
   Communicate fwd 1:                  0.023     0.023   0.1% |
   fft:                                0.042     0.042   0.1% |
   fft2:                               0.052     0.052   0.2% |
  XC 3D grid:                          0.943     0.943   3.2% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.003     0.003   0.0% |
SCF-cycle:                            25.789     0.003   0.0% |
 Direct Minimisation step:            24.380     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.352     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.013     0.013   0.0% |
   Potential matrix:                   0.329     0.329   1.1% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.021     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.000     0.000   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.020     0.020   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.018     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.016     0.016   0.1% |
  Update Kohn-Sham energy:            23.981     0.000   0.0% |
   Density:                            1.036     0.000   0.0% |
    Atomic density matrices:           0.045     0.045   0.2% |
    Mix:                               0.662     0.662   2.2% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.321     0.012   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.308     0.308   1.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       22.945     0.002   0.0% |
    Atomic:                            0.920     0.132   0.4% |
     XC Correction:                    0.788     0.788   2.6% ||
    Calculate atomic Hamiltonians:     0.039     0.039   0.1% |
    Communicate:                       0.001     0.001   0.0% |
    Hartree integrate/restrict:        0.596     0.596   2.0% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.399     0.277   0.9% |
     Communicate bwd 0:                0.359     0.359   1.2% |
     Communicate bwd 1:                1.382     1.382   4.6% |-|
     Communicate fwd 0:                1.259     1.259   4.2% |-|
     Communicate fwd 1:                0.530     0.530   1.8% ||
     fft:                              0.720     0.720   2.4% ||
     fft2:                             0.872     0.872   2.9% ||
    XC 3D grid:                       15.917    15.917  53.3% |--------------------|
    vbar:                              0.069     0.069   0.2% |
 Get canonical representation:         1.406     0.000   0.0% |
  LCAO eigensolver:                    0.021     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.019     0.019   0.1% |
  Update Kohn-Sham energy:             1.384     0.000   0.0% |
   Density:                            0.059     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.038     0.038   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.019     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.019     0.019   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.325     0.000   0.0% |
    Atomic:                            0.054     0.008   0.0% |
     XC Correction:                    0.046     0.046   0.2% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.000     0.000   0.0% |
    Hartree integrate/restrict:        0.025     0.025   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.306     0.016   0.1% |
     Communicate bwd 0:                0.020     0.020   0.1% |
     Communicate bwd 1:                0.080     0.080   0.3% |
     Communicate fwd 0:                0.074     0.074   0.2% |
     Communicate fwd 1:                0.023     0.023   0.1% |
     fft:                              0.042     0.042   0.1% |
     fft2:                             0.051     0.051   0.2% |
    XC 3D grid:                        0.934     0.934   3.1% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.003     0.003   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.501     0.501   1.7% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.157     2.157   7.2% |--|
-------------------------------------------------------------
Total:                                          29.851 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 23:22:56 2019
