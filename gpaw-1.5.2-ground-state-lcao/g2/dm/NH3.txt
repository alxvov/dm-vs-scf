
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:13:10 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -1519.191991

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*104*96 grid
  Fine grid: 208*208*192 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 42.62 MiB
    Density: 25.31 MiB
      Arrays: 24.93 MiB
      Localized functions: 0.38 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 16.34 MiB
      Arrays: 16.31 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.97 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.95 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 28
Number of bands in calculation: 28
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .-------------------------------------.  
         /|                                     |  
        / |                                     |  
       /  |                                     |  
      /   |                                     |  
     /    |                                     |  
    /     |                                     |  
   /      |                                     |  
  /       |                                     |  
 *        |                                     |  
 |        |                                     |  
 |        |                                     |  
 |        |                                     |  
 |        |             NH                      |  
 |        |           H   H                     |  
 |        |                                     |  
 |        |                                     |  
 |        |                                     |  
 |        .-------------------------------------.  
 |       /                                     /   
 |      /                                     /    
 |     /                                     /     
 |    /                                     /      
 |   /                                     /       
 |  /                                     /        
 | /                                     /         
 |/                                     /          
 *-------------------------------------*           

Positions:
   0 N      7.813831    7.469865    7.388297    ( 0.0000,  0.0000,  0.0000)
   1 H      7.813831    8.409596    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      8.627662    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.627662    0.000000    0.000000   104     0.1503
  2. axis:    no     0.000000   15.409596    0.000000   104     0.1482
  3. axis:    no     0.000000    0.000000   14.388297    96     0.1499

  Lengths:  15.627662  15.409596  14.388297
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1494

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:13:17  +0.44          -18.653930    0      1      
iter:   2  23:13:18  -0.41          -18.681884    0      1      
iter:   3  23:13:19  -2.17          -18.684469    0      1      
iter:   4  23:13:20  -2.75          -18.684545    0      1      
iter:   5  23:13:21  -3.92          -18.684568    0      1      
iter:   6  23:13:23  -3.97          -18.684569    0      1      
iter:   7  23:13:24  -4.17          -18.684569    0      1      
iter:   8  23:13:25  -5.92          -18.684569    0      1      
iter:   9  23:13:26  -7.09          -18.684569    0      1      
iter:  10  23:13:29 -10.19          -18.684569    0      1      

Converged after 10 iterations.

Dipole moment: (-0.000000, 0.000047, -0.304105) |e|*Ang

Energy contributions relative to reference atoms: (reference = -1519.191991)

Kinetic:        +16.546394
Potential:      -18.143337
External:        +0.000000
XC:             -17.184236
Entropy (-ST):   +0.000000
Local:           +0.096610
--------------------------
Free energy:    -18.684569
Extrapolated:   -18.684569

Fermi levels: -1.98852, -1.98852

 Band  Eigenvalues  Occupancy
    0    -20.89409    2.00000
    1    -10.98805    2.00000
    2    -10.98728    2.00000
    3     -5.64473    2.00000
    4      1.66770    0.00000
    5      3.92391    0.00000
    6      3.92500    0.00000
    7     10.13754    0.00000
    8     10.13938    0.00000
    9     13.76658    0.00000
   10     13.86795    0.00000
   11     18.27285    0.00000
   12     18.27336    0.00000
   13     19.54614    0.00000
   14     19.54671    0.00000
   15     24.93869    0.00000
   16     29.82817    0.00000
   17     34.13751    0.00000
   18     34.13765    0.00000
   19     35.18022    0.00000
   20     41.61176    0.00000
   21     41.61375    0.00000
   22     46.85038    0.00000
   23     52.45953    0.00000
   24     52.46010    0.00000
   25     57.22755    0.00000
   26     60.45670    0.00000
   27     60.45855    0.00000

Gap: 7.312 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.005     0.005   0.0% |
LCAO WFS Initialize:                   1.216     0.031   0.2% |
 Hamiltonian:                          1.185     0.000   0.0% |
  Atomic:                              0.027     0.027   0.1% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.021     0.021   0.1% |
  Hartree integrate/restrict:          0.022     0.022   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.343     0.010   0.1% |
   Communicate bwd 0:                  0.065     0.065   0.4% |
   Communicate bwd 1:                  0.066     0.066   0.4% |
   Communicate fwd 0:                  0.065     0.065   0.3% |
   Communicate fwd 1:                  0.069     0.069   0.4% |
   fft:                                0.023     0.023   0.1% |
   fft2:                               0.046     0.046   0.2% |
  XC 3D grid:                          0.767     0.767   4.1% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.001     0.001   0.0% |
SCF-cycle:                            15.448     0.002   0.0% |
 Direct Minimisation step:            14.265     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.050     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.006     0.006   0.0% |
   Potential matrix:                   0.040     0.040   0.2% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Preconditioning::                    0.000     0.000   0.0% |
  Unitary rotation:                    0.008     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.007     0.007   0.0% |
  Update Kohn-Sham energy:            14.198     0.000   0.0% |
   Density:                            0.431     0.000   0.0% |
    Atomic density matrices:           0.015     0.015   0.1% |
    Mix:                               0.366     0.366   2.0% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.046     0.005   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.040     0.040   0.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       13.767     0.001   0.0% |
    Atomic:                            0.328     0.328   1.8% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.010     0.010   0.1% |
    Communicate:                       0.248     0.248   1.3% ||
    Hartree integrate/restrict:        0.289     0.289   1.6% ||
    New Kinetic Energy:                0.001     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.939     0.102   0.5% |
     Communicate bwd 0:                0.765     0.765   4.1% |-|
     Communicate bwd 1:                0.760     0.760   4.1% |-|
     Communicate fwd 0:                0.710     0.710   3.8% |-|
     Communicate fwd 1:                0.782     0.782   4.2% |-|
     fft:                              0.277     0.277   1.5% ||
     fft2:                             0.543     0.543   2.9% ||
    XC 3D grid:                        8.912     8.912  47.9% |------------------|
    vbar:                              0.039     0.039   0.2% |
 Get canonical representation:         1.181     0.000   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.000     0.000   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Update Kohn-Sham energy:             1.177     0.000   0.0% |
   Density:                            0.035     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.030     0.030   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.004     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.003     0.003   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.142     0.000   0.0% |
    Atomic:                            0.027     0.027   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.021     0.021   0.1% |
    Hartree integrate/restrict:        0.021     0.021   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.328     0.009   0.0% |
     Communicate bwd 0:                0.064     0.064   0.3% |
     Communicate bwd 1:                0.062     0.062   0.3% |
     Communicate fwd 0:                0.059     0.059   0.3% |
     Communicate fwd 1:                0.065     0.065   0.3% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.045     0.045   0.2% |
    XC 3D grid:                        0.740     0.740   4.0% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.183     0.183   1.0% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.761     1.761   9.5% |---|
-------------------------------------------------------------
Total:                                          18.617 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 23:13:29 2019
