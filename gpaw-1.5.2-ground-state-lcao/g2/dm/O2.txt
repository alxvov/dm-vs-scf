
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:09:36 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -4081.704602

Spin-polarized calculation.
Magnetic moment: 2.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*100 grid
  Fine grid: 184*184*200 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 48.51 MiB
    Density: 27.72 MiB
      Arrays: 27.29 MiB
      Localized functions: 0.43 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.31 MiB
      Arrays: 20.28 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.49 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.48 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 26
Bands to converge: occupied states only
Number of valence electrons: 12

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            O                    |  
 |        |                                 |  
 |        |            O                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 O      7.000000    7.000000    8.245956    ( 0.0000,  0.0000,  1.0000)
   1 O      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  1.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.245956   100     0.1525

  Lengths:  14.000000  14.000000  15.245956
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1523

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  23:09:42  +1.75           -8.932128    0      1        +2.0000
iter:   2  23:09:44  -0.91           -9.100574    0      1        +2.0000
iter:   3  23:09:45  -1.71           -9.102087    0      1        +2.0000
iter:   4  23:09:47  -4.49           -9.102260    0      1        +2.0000
iter:   5  23:09:48  -5.94           -9.102261    0      1        +2.0000
iter:   6  23:09:50  -7.34           -9.102261    0      1        +2.0000
iter:   7  23:09:51  -8.30           -9.102261    0      1        +2.0000
iter:   8  23:09:53  -9.17           -9.102261    0      1        +2.0000
iter:   9  23:09:55 -11.28           -9.102261    0      1        +2.0000

Converged after 9 iterations.

Dipole moment: (0.000000, 0.000000, -0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 2.000017)
Local magnetic moments:
   0 O  ( 0.000000,  0.000000,  0.712826)
   1 O  ( 0.000000,  0.000000,  0.712826)

Energy contributions relative to reference atoms: (reference = -4081.704602)

Kinetic:        +10.853130
Potential:      -10.427068
External:        +0.000000
XC:              -9.677006
Entropy (-ST):   +0.000000
Local:           +0.148683
--------------------------
Free energy:     -9.102261
Extrapolated:    -9.102261

Spin contamination: 0.013043 electrons
Fermi levels: -1.66174, -7.51984

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -31.73461    1.00000    -30.49500    1.00000
    1    -20.25067    1.00000    -18.45923    1.00000
    2    -12.80217    1.00000    -11.86175    1.00000
    3    -12.67307    1.00000    -10.81522    1.00000
    4    -12.67307    1.00000    -10.81522    1.00000
    5     -6.50813    1.00000     -4.22446    0.00000
    6     -6.50813    1.00000     -4.22446    0.00000
    7      3.18464    0.00000      4.24401    0.00000
    8     15.12773    0.00000     15.61772    0.00000
    9     15.12773    0.00000     16.10206    0.00000
   10     15.14152    0.00000     16.10206    0.00000
   11     16.62253    0.00000     16.93855    0.00000
   12     17.98221    0.00000     18.97079    0.00000
   13     17.98221    0.00000     18.97079    0.00000
   14     24.87465    0.00000     25.77528    0.00000
   15     37.90625    0.00000     38.67629    0.00000
   16     37.90625    0.00000     38.67629    0.00000
   17     39.24135    0.00000     40.77539    0.00000
   18     39.24195    0.00000     40.77782    0.00000
   19     40.71002    0.00000     41.86229    0.00000
   20     48.74345    0.00000     50.57765    0.00000
   21     48.74487    0.00000     50.57962    0.00000
   22     59.08228    0.00000     60.02145    0.00000
   23     64.50585    0.00000     65.65538    0.00000
   24     64.50585    0.00000     65.65538    0.00000
   25     66.93226    0.00000     67.63752    0.00000

Gap: 2.284 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.003     0.003   0.0% |
LCAO WFS Initialize:                   1.460     0.047   0.2% |
 Hamiltonian:                          1.413     0.001   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.081     0.081   0.4% |
  Hartree integrate/restrict:          0.034     0.034   0.2% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.275     0.004   0.0% |
   Communicate bwd 0:                  0.054     0.054   0.3% |
   Communicate bwd 1:                  0.051     0.051   0.3% |
   Communicate fwd 0:                  0.049     0.049   0.3% |
   Communicate fwd 1:                  0.053     0.053   0.3% |
   fft:                                0.015     0.015   0.1% |
   fft2:                               0.048     0.048   0.2% |
  XC 3D grid:                          1.018     1.018   5.3% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            16.217     0.003   0.0% |
 Direct Minimisation step:            14.743     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.037     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.009     0.009   0.0% |
   Potential matrix:                   0.023     0.023   0.1% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.012     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.011     0.011   0.1% |
  Update Kohn-Sham energy:            14.686     0.000   0.0% |
   Density:                            0.501     0.000   0.0% |
    Atomic density matrices:           0.015     0.015   0.1% |
    Mix:                               0.450     0.450   2.3% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.032     0.008   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.023     0.023   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       14.185     0.014   0.1% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.004     0.004   0.0% |
    Communicate:                       0.811     0.811   4.2% |-|
    Hartree integrate/restrict:        0.336     0.336   1.7% ||
    New Kinetic Energy:                0.001     0.000   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           2.784     0.043   0.2% |
     Communicate bwd 0:                0.545     0.545   2.8% ||
     Communicate bwd 1:                0.513     0.513   2.6% ||
     Communicate fwd 0:                0.497     0.497   2.6% ||
     Communicate fwd 1:                0.548     0.548   2.8% ||
     fft:                              0.147     0.147   0.8% |
     fft2:                             0.492     0.492   2.5% ||
    XC 3D grid:                       10.202    10.202  52.6% |--------------------|
    vbar:                              0.027     0.027   0.1% |
 Get canonical representation:         1.472     0.000   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Update Kohn-Sham energy:             1.468     0.000   0.0% |
   Density:                            0.050     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.045     0.045   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.003     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.002     0.002   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.418     0.001   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.081     0.081   0.4% |
    Hartree integrate/restrict:        0.034     0.034   0.2% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.279     0.004   0.0% |
     Communicate bwd 0:                0.054     0.054   0.3% |
     Communicate bwd 1:                0.052     0.052   0.3% |
     Communicate fwd 0:                0.049     0.049   0.3% |
     Communicate fwd 1:                0.057     0.057   0.3% |
     fft:                              0.015     0.015   0.1% |
     fft2:                             0.048     0.048   0.2% |
    XC 3D grid:                        1.019     1.019   5.3% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.097     0.097   0.5% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.609     1.609   8.3% |--|
-------------------------------------------------------------
Total:                                          19.389 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 23:09:55 2019
