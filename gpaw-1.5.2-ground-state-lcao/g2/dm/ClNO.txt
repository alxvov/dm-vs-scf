
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:28:16 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -16077.523201

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*108*92 grid
  Fine grid: 208*216*184 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 42.82 MiB
    Density: 25.52 MiB
      Arrays: 24.81 MiB
      Localized functions: 0.71 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 16.28 MiB
      Arrays: 16.23 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.02 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.98 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 39
Number of bands in calculation: 39
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |             N  O                     |  
 |         |           Cl                         |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 Cl     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 N      7.537724    8.958328    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 O      8.680388    9.131626    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.680388    0.000000    0.000000   104     0.1508
  2. axis:    no     0.000000   16.131626    0.000000   108     0.1494
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  15.680388  16.131626  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1508

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:28:24  +0.81          -13.551228    0      1      
iter:   2  22:28:26  +0.23          -13.606262    0      1      
iter:   3  22:28:27  +0.07          -13.620419    0      1      
iter:   4  22:28:28  -1.23          -13.630686    0      1      
iter:   5  22:28:30  -1.53          -13.632442    0      1      
iter:   6  22:28:31  -1.67          -13.633956    0      1      
iter:   7  22:28:32  -1.84          -13.634137    0      1      
iter:   8  22:28:35  -2.62          -13.634218    0      1      
iter:   9  22:28:36  -3.01          -13.634240    0      1      
iter:  10  22:28:37  -4.71          -13.634251    0      1      
iter:  11  22:28:38  -5.61          -13.634252    0      1      
iter:  12  22:28:39  -6.58          -13.634252    0      1      
iter:  13  22:28:41  -6.72          -13.634252    0      1      
iter:  14  22:28:42  -6.76          -13.634252    0      1      
iter:  15  22:28:43  -7.88          -13.634252    0      1      
iter:  16  22:28:44  -8.40          -13.634252    0      1      
iter:  17  22:28:45  -9.16          -13.634252    0      1      
iter:  18  22:28:47  -9.52          -13.634252    0      1      
iter:  19  22:28:48  -9.90          -13.634252    0      1      
iter:  20  22:28:52 -10.09          -13.634252    0      1      

Converged after 20 iterations.

Dipole moment: (0.186528, 0.302053, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -16077.523201)

Kinetic:        +16.353500
Potential:      -17.068947
External:        +0.000000
XC:             -13.068563
Entropy (-ST):   +0.000000
Local:           +0.149758
--------------------------
Free energy:    -13.634252
Extrapolated:   -13.634252

Fermi levels: -5.75067, -5.75067

 Band  Eigenvalues  Occupancy
    0    -32.30802    2.00000
    1    -19.93094    2.00000
    2    -17.07601    2.00000
    3    -13.48762    2.00000
    4    -13.38953    2.00000
    5    -12.52577    2.00000
    6     -7.23697    2.00000
    7     -7.06555    2.00000
    8     -6.82512    2.00000
    9     -4.67621    0.00000
   10     -3.01919    0.00000
   11      6.91004    0.00000
   12     10.93147    0.00000
   13     11.72175    0.00000
   14     12.36916    0.00000
   15     13.70926    0.00000
   16     14.36609    0.00000
   17     14.37201    0.00000
   18     15.04092    0.00000
   19     15.71378    0.00000
   20     15.90618    0.00000
   21     16.17373    0.00000
   22     16.61216    0.00000
   23     17.46475    0.00000
   24     18.26044    0.00000
   25     19.45731    0.00000
   26     20.88644    0.00000
   27     26.99340    0.00000
   28     32.53204    0.00000
   29     34.15954    0.00000
   30     34.44129    0.00000
   31     39.58763    0.00000
   32     42.41926    0.00000
   33     46.90055    0.00000
   34     48.47618    0.00000
   35     52.78682    0.00000
   36     61.37134    0.00000
   37     62.08321    0.00000
   38     65.87901    0.00000

Gap: 2.149 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.005     0.005   0.0% |
LCAO WFS Initialize:                   1.214     0.033   0.1% |
 Hamiltonian:                          1.180     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.062     0.062   0.2% |
  Hartree integrate/restrict:          0.021     0.021   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.342     0.010   0.0% |
   Communicate bwd 0:                  0.064     0.064   0.2% |
   Communicate bwd 1:                  0.063     0.063   0.2% |
   Communicate fwd 0:                  0.062     0.062   0.2% |
   Communicate fwd 1:                  0.068     0.068   0.2% |
   fft:                                0.020     0.020   0.1% |
   fft2:                               0.056     0.056   0.2% |
  XC 3D grid:                          0.750     0.750   2.1% ||
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            31.540     0.005   0.0% |
 Direct Minimisation step:            30.324     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.117     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.038     0.038   0.1% |
   Potential matrix:                   0.070     0.070   0.2% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.005     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.018     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.016     0.016   0.0% |
  Update Kohn-Sham energy:            30.176     0.000   0.0% |
   Density:                            0.942     0.000   0.0% |
    Atomic density matrices:           0.053     0.053   0.1% |
    Mix:                               0.798     0.798   2.3% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.005     0.005   0.0% |
    Pseudo density:                    0.083     0.011   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.071     0.071   0.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       29.234     0.003   0.0% |
    Atomic:                            0.007     0.007   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.014     0.014   0.0% |
    Communicate:                       1.564     1.564   4.4% |-|
    Hartree integrate/restrict:        0.568     0.568   1.6% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           8.397     0.226   0.6% |
     Communicate bwd 0:                1.579     1.579   4.5% |-|
     Communicate bwd 1:                1.577     1.577   4.5% |-|
     Communicate fwd 0:                1.491     1.491   4.2% |-|
     Communicate fwd 1:                1.629     1.629   4.6% |-|
     fft:                              0.480     0.480   1.4% ||
     fft2:                             1.416     1.416   4.0% |-|
    XC 3D grid:                       18.597    18.597  52.6% |--------------------|
    vbar:                              0.080     0.080   0.2% |
 Get canonical representation:         1.211     0.000   0.0% |
  LCAO eigensolver:                    0.005     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Update Kohn-Sham energy:             1.206     0.000   0.0% |
   Density:                            0.039     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.033     0.033   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.003     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.003     0.003   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.167     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.062     0.062   0.2% |
    Hartree integrate/restrict:        0.023     0.023   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.333     0.009   0.0% |
     Communicate bwd 0:                0.063     0.063   0.2% |
     Communicate bwd 1:                0.062     0.062   0.2% |
     Communicate fwd 0:                0.059     0.059   0.2% |
     Communicate fwd 1:                0.064     0.064   0.2% |
     fft:                              0.019     0.019   0.1% |
     fft2:                             0.056     0.056   0.2% |
    XC 3D grid:                        0.745     0.745   2.1% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.751     0.751   2.1% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.828     1.828   5.2% |-|
-------------------------------------------------------------
Total:                                          35.341 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:28:52 2019
