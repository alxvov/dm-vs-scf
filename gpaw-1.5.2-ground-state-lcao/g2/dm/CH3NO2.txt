
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:51:47 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -6628.524709

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*108*108 grid
  Fine grid: 208*216*216 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 51.31 MiB
    Density: 30.16 MiB
      Arrays: 29.18 MiB
      Localized functions: 0.98 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.16 MiB
      Arrays: 19.09 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.99 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.89 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 67
Number of bands in calculation: 67
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |              O                      |  
 |         |                                     |  
 |         |          H CN                       |  
 |         |              H                      |  
 |         |          H                          |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.526639    7.400691    8.103775    ( 0.0000,  0.0000,  0.0000)
   1 N      7.640921    8.881736    8.103775    ( 0.0000,  0.0000,  0.0000)
   2 H      8.540486    7.000000    8.103775    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.108044    9.008731    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    7.108044    7.198819    ( 0.0000,  0.0000,  0.0000)
   5 O      7.707669    9.443488    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 O      7.707669    9.443488    9.207550    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.540486    0.000000    0.000000   104     0.1494
  2. axis:    no     0.000000   16.443488    0.000000   108     0.1523
  3. axis:    no     0.000000    0.000000   16.207550   108     0.1501

  Lengths:  15.540486  16.443488  16.207550
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1506

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:51:56  +0.71          -37.464089    0      1      
iter:   2  22:51:59  -0.15          -37.570547    0      1      
iter:   3  22:52:00  -1.44          -37.585717    0      1      
iter:   4  22:52:01  -2.43          -37.586783    0      1      
iter:   5  22:52:03  -2.56          -37.586859    0      1      
iter:   6  22:52:04  -3.88          -37.586893    0      1      
iter:   7  22:52:06  -4.82          -37.586897    0      1      
iter:   8  22:52:07  -5.68          -37.586897    0      1      
iter:   9  22:52:09  -6.13          -37.586897    0      1      
iter:  10  22:52:10  -6.22          -37.586897    0      1      
iter:  11  22:52:11  -7.03          -37.586897    0      1      
iter:  12  22:52:13  -7.76          -37.586897    0      1      
iter:  13  22:52:14  -8.75          -37.586897    0      1      
iter:  14  22:52:16  -9.12          -37.586897    0      1      
iter:  15  22:52:17  -9.64          -37.586897    0      1      
iter:  16  22:52:18  -9.39          -37.586897    0      1      
iter:  17  22:52:20  -9.58          -37.586897    0      1      
iter:  18  22:52:23 -11.30          -37.586897    0      1      

Converged after 18 iterations.

Dipole moment: (-0.048877, -0.648003, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6628.524709)

Kinetic:        +32.009214
Potential:      -33.117034
External:        +0.000000
XC:             -36.715611
Entropy (-ST):   +0.000000
Local:           +0.236533
--------------------------
Free energy:    -37.586897
Extrapolated:   -37.586897

Fermi levels: -4.64391, -4.64391

 Band  Eigenvalues  Occupancy
    0    -30.58950    2.00000
    1    -26.17293    2.00000
    2    -20.30576    2.00000
    3    -15.26060    2.00000
    4    -13.37128    2.00000
    5    -12.98631    2.00000
    6    -12.50050    2.00000
    7    -10.89061    2.00000
    8    -10.47175    2.00000
    9     -7.45228    2.00000
   10     -6.89976    2.00000
   11     -6.42931    2.00000
   12     -2.85851    0.00000
   13      1.65911    0.00000
   14      2.19510    0.00000
   15      2.96093    0.00000
   16      3.25424    0.00000
   17      5.57629    0.00000
   18      7.64487    0.00000
   19      8.28779    0.00000
   20      9.29025    0.00000
   21      9.84376    0.00000
   22     12.59391    0.00000
   23     12.96306    0.00000
   24     13.57051    0.00000
   25     14.50796    0.00000
   26     14.92078    0.00000
   27     15.64354    0.00000
   28     16.93757    0.00000
   29     17.06756    0.00000
   30     18.14289    0.00000
   31     18.58885    0.00000
   32     20.11483    0.00000
   33     20.65923    0.00000
   34     22.09328    0.00000
   35     23.40421    0.00000
   36     26.04331    0.00000
   37     26.67076    0.00000
   38     28.61287    0.00000
   39     30.57337    0.00000
   40     31.30463    0.00000
   41     32.99953    0.00000
   42     34.42361    0.00000
   43     35.10358    0.00000
   44     37.09109    0.00000
   45     37.98265    0.00000
   46     39.38792    0.00000
   47     42.34099    0.00000
   48     43.33751    0.00000
   49     44.52418    0.00000
   50     44.75633    0.00000
   51     46.21769    0.00000
   52     46.59232    0.00000
   53     48.29523    0.00000
   54     48.34928    0.00000
   55     49.52999    0.00000
   56     50.52681    0.00000
   57     51.34997    0.00000
   58     52.44868    0.00000
   59     54.07157    0.00000
   60     57.02973    0.00000
   61     61.18290    0.00000
   62     65.62704    0.00000
   63     66.42227    0.00000
   64     66.81244    0.00000
   65     70.66546    0.00000
   66     74.66522    0.00000

Gap: 3.571 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.009     0.009   0.0% |
LCAO WFS Initialize:                   1.385     0.040   0.1% |
 Hamiltonian:                          1.346     0.000   0.0% |
  Atomic:                              0.027     0.027   0.1% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.022     0.022   0.1% |
  Hartree integrate/restrict:          0.025     0.025   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.384     0.007   0.0% |
   Communicate bwd 0:                  0.077     0.077   0.2% |
   Communicate bwd 1:                  0.072     0.072   0.2% |
   Communicate fwd 0:                  0.074     0.074   0.2% |
   Communicate fwd 1:                  0.079     0.079   0.2% |
   fft:                                0.023     0.023   0.1% |
   fft2:                               0.053     0.053   0.1% |
  XC 3D grid:                          0.878     0.878   2.5% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            30.895     0.004   0.0% |
 Direct Minimisation step:            29.491     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.278     0.005   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.056     0.056   0.2% |
   Potential matrix:                   0.211     0.211   0.6% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.014     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.010     0.010   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.021     0.003   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.018     0.018   0.1% |
  Update Kohn-Sham energy:            29.169     0.000   0.0% |
   Density:                            1.089     0.000   0.0% |
    Atomic density matrices:           0.074     0.074   0.2% |
    Mix:                               0.793     0.793   2.2% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.214     0.013   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.200     0.200   0.6% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       28.080     0.002   0.0% |
    Atomic:                            0.570     0.570   1.6% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.028     0.028   0.1% |
    Communicate:                       0.444     0.444   1.3% ||
    Hartree integrate/restrict:        0.554     0.554   1.6% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           8.065     0.158   0.4% |
     Communicate bwd 0:                1.609     1.609   4.5% |-|
     Communicate bwd 1:                1.539     1.539   4.3% |-|
     Communicate fwd 0:                1.524     1.524   4.3% |-|
     Communicate fwd 1:                1.651     1.651   4.7% |-|
     fft:                              0.475     0.475   1.3% ||
     fft2:                             1.108     1.108   3.1% ||
    XC 3D grid:                       18.334    18.334  51.6% |--------------------|
    vbar:                              0.079     0.079   0.2% |
 Get canonical representation:         1.400     0.000   0.0% |
  LCAO eigensolver:                    0.014     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.010     0.010   0.0% |
  Update Kohn-Sham energy:             1.386     0.000   0.0% |
   Density:                            0.053     0.000   0.0% |
    Atomic density matrices:           0.004     0.004   0.0% |
    Mix:                               0.039     0.039   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.010     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.009     0.009   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.333     0.000   0.0% |
    Atomic:                            0.027     0.027   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.022     0.022   0.1% |
    Hartree integrate/restrict:        0.026     0.026   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.383     0.008   0.0% |
     Communicate bwd 0:                0.075     0.075   0.2% |
     Communicate bwd 1:                0.072     0.072   0.2% |
     Communicate fwd 0:                0.073     0.073   0.2% |
     Communicate fwd 1:                0.079     0.079   0.2% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.053     0.053   0.1% |
    XC 3D grid:                        0.871     0.871   2.5% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 1.033     1.033   2.9% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.169     2.169   6.1% |-|
-------------------------------------------------------------
Total:                                          35.499 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:52:23 2019
