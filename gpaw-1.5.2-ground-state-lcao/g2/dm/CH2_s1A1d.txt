
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:51:00 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -1052.608440

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*104*96 grid
  Fine grid: 184*208*192 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*208*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 37.61 MiB
    Density: 22.34 MiB
      Arrays: 22.03 MiB
      Localized functions: 0.31 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 14.43 MiB
      Arrays: 14.41 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.02 MiB
    Wavefunctions: 0.84 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.83 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 23
Bands to converge: occupied states only
Number of valence electrons: 6

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           CH                    |  
 |         |          H                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 C      7.000000    7.862232    7.697372    ( 0.0000,  0.0000,  0.0000)
   1 H      7.000000    8.724464    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   15.724464    0.000000   104     0.1512
  3. axis:    no     0.000000    0.000000   14.697372    96     0.1531

  Lengths:  14.000000  15.724464  14.697372
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1522

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:51:06  +0.79          -10.738748    0      1      
iter:   2  22:51:07  +0.24          -10.750442    0      1      
iter:   3  22:51:08  -2.41          -10.755244    0      1      
iter:   4  22:51:09  -3.02          -10.755268    0      1      
iter:   5  22:51:10  -5.02          -10.755277    0      1      
iter:   6  22:51:11  -5.77          -10.755277    0      1      
iter:   7  22:51:12  -6.30          -10.755277    0      1      
iter:   8  22:51:13  -8.07          -10.755277    0      1      
iter:   9  22:51:15  -8.63          -10.755277    0      1      
iter:  10  22:51:17 -11.89          -10.755277    0      1      

Converged after 10 iterations.

Dipole moment: (0.000000, -0.000000, -0.342989) |e|*Ang

Energy contributions relative to reference atoms: (reference = -1052.608440)

Kinetic:        +10.470427
Potential:      -10.545784
External:        +0.000000
XC:             -10.700317
Entropy (-ST):   +0.000000
Local:           +0.020396
--------------------------
Free energy:    -10.755277
Extrapolated:   -10.755277

Fermi levels: -5.07542, -5.07542

 Band  Eigenvalues  Occupancy
    0    -16.50437    2.00000
    1     -9.80049    2.00000
    2     -5.47808    2.00000
    3     -4.67276    0.00000
    4      2.23420    0.00000
    5      3.68628    0.00000
    6      8.97490    0.00000
    7     10.36145    0.00000
    8     10.83621    0.00000
    9     11.72286    0.00000
   10     15.06465    0.00000
   11     18.72163    0.00000
   12     19.12853    0.00000
   13     21.35256    0.00000
   14     22.12154    0.00000
   15     29.87992    0.00000
   16     32.87008    0.00000
   17     36.34615    0.00000
   18     36.49962    0.00000
   19     36.77234    0.00000
   20     42.49043    0.00000
   21     49.25074    0.00000
   22     51.20039    0.00000

Gap: 0.805 eV
Transition (v -> c):
  (s=0, k=0, n=2, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=3, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.004     0.004   0.0% |
LCAO WFS Initialize:                   1.054     0.027   0.2% |
 Hamiltonian:                          1.026     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.047     0.047   0.3% |
  Hartree integrate/restrict:          0.019     0.019   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.295     0.007   0.0% |
   Communicate bwd 0:                  0.058     0.058   0.4% |
   Communicate bwd 1:                  0.056     0.056   0.3% |
   Communicate fwd 0:                  0.052     0.052   0.3% |
   Communicate fwd 1:                  0.057     0.057   0.3% |
   fft:                                0.020     0.020   0.1% |
   fft2:                               0.045     0.045   0.3% |
  XC 3D grid:                          0.662     0.662   4.0% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            13.652     0.002   0.0% |
 Direct Minimisation step:            12.601     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.037     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.008     0.008   0.0% |
   Potential matrix:                   0.026     0.026   0.2% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.003     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Preconditioning::                    0.000     0.000   0.0% |
  Unitary rotation:                    0.008     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.007     0.007   0.0% |
  Update Kohn-Sham energy:            12.550     0.000   0.0% |
   Density:                            0.359     0.000   0.0% |
    Atomic density matrices:           0.014     0.014   0.1% |
    Mix:                               0.309     0.309   1.9% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.032     0.004   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.027     0.027   0.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       12.191     0.001   0.0% |
    Atomic:                            0.003     0.003   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.004     0.004   0.0% |
    Communicate:                       0.562     0.562   3.4% ||
    Hartree integrate/restrict:        0.239     0.239   1.4% ||
    New Kinetic Energy:                0.001     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.519     0.082   0.5% |
     Communicate bwd 0:                0.685     0.685   4.1% |-|
     Communicate bwd 1:                0.668     0.668   4.0% |-|
     Communicate fwd 0:                0.626     0.626   3.8% |-|
     Communicate fwd 1:                0.680     0.680   4.1% |-|
     fft:                              0.238     0.238   1.4% ||
     fft2:                             0.541     0.541   3.3% ||
    XC 3D grid:                        7.826     7.826  47.4% |------------------|
    vbar:                              0.035     0.035   0.2% |
 Get canonical representation:         1.048     0.000   0.0% |
  LCAO eigensolver:                    0.003     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Update Kohn-Sham energy:             1.045     0.000   0.0% |
   Density:                            0.030     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.026     0.026   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.003     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.002     0.002   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.015     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.047     0.047   0.3% |
    Hartree integrate/restrict:        0.020     0.020   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.293     0.007   0.0% |
     Communicate bwd 0:                0.057     0.057   0.3% |
     Communicate bwd 1:                0.057     0.057   0.3% |
     Communicate fwd 0:                0.052     0.052   0.3% |
     Communicate fwd 1:                0.057     0.057   0.3% |
     fft:                              0.020     0.020   0.1% |
     fft2:                             0.044     0.044   0.3% |
    XC 3D grid:                        0.651     0.651   3.9% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.179     0.179   1.1% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.615     1.615   9.8% |---|
-------------------------------------------------------------
Total:                                          16.507 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:51:17 2019
