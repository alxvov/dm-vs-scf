#!/bin/sh
# -------------------------------------------------------------------
#SBATCH -J dm
#SBATCH --workdir=/users/home/aleksei/NEW_PROJECTS/gpaw_lcao/dm_vs_scf/g2/dm
#SBATCH -o dm.py.%J.log
#SBATCH -e dm.py.%J.err
#SBATCH --job-name=dm.py
#SBATCH -N 1
#SBATCH --ntasks-per-node=8
#SBATCH -p normal 
#SBATCH --time=48:00:00
#SBATCH --exclusive
# -------------------------------------------------------------------

echo /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/dm_vs_scf/g2/dm
cd /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/dm_vs_scf/g2/dm
source ~/.bashrc

# module purge
# module load intel/compiler/2017.2
# module load intel/mkl/2017.2
# module load openmpi/intel/2.1.0
# module load python/2.7.13
# module load gnu/5.4.0

module list
which gpaw-python

export OMP_NUM_THREADS=1

# Run GPAW
start=`date +%s`
mpirun -np 8 gpaw-python dm.py
# srun gpaw-python dm.py
# gpaw-python dm.py
end=`date +%s`

# Write ending statements to stdout
runtime=$((end-start))
echo Stop time is `date`
echo 'Used walltime: ' `printf '%dh:%dm:%ds\n' $(($runtime/3600)) $(($runtime%3600/60)) $(($runtime%60))`
echo ========= Job finished ===================
