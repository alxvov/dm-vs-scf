
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:55:34 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -51247.425698

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*108*108 grid
  Fine grid: 216*216*216 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 54.09 MiB
    Density: 31.91 MiB
      Arrays: 30.31 MiB
      Localized functions: 1.60 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.95 MiB
      Arrays: 19.83 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 2.22 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.13 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 65
Number of bands in calculation: 65
Bands to converge: occupied states only
Number of valence electrons: 32

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                 Cl                   |  
 |         |          Cl                          |  
 |         |             C                        |  
 |         |            Cl                        |  
 |         |               Cl                     |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      8.021340    8.021340    8.021340    ( 0.0000,  0.0000,  0.0000)
   1 Cl     9.042680    9.042680    9.042680    ( 0.0000,  0.0000,  0.0000)
   2 Cl     7.000000    7.000000    9.042680    ( 0.0000,  0.0000,  0.0000)
   3 Cl     7.000000    9.042680    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 Cl     9.042680    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.042680    0.000000    0.000000   108     0.1485
  2. axis:    no     0.000000   16.042680    0.000000   108     0.1485
  3. axis:    no     0.000000    0.000000   16.042680   108     0.1485

  Lengths:  16.042680  16.042680  16.042680
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1485

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:55:43  -0.24          -15.244944    0      1      
iter:   2  22:55:44  -0.80          -15.264007    0      1      
iter:   3  22:55:46  -2.97          -15.273358    0      1      
iter:   4  22:55:47  -3.89          -15.273454    0      1      
iter:   5  22:55:49  -5.20          -15.273475    0      1      
iter:   6  22:55:50  -5.05          -15.273475    0      1      
iter:   7  22:55:52  -5.41          -15.273475    0      1      
iter:   8  22:55:53  -6.73          -15.273475    0      1      
iter:   9  22:55:55  -7.84          -15.273475    0      1      
iter:  10  22:55:58 -10.08          -15.273475    0      1      

Converged after 10 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -51247.425698)

Kinetic:        +35.297844
Potential:      -29.934648
External:        +0.000000
XC:             -20.695007
Entropy (-ST):   +0.000000
Local:           +0.058337
--------------------------
Free energy:    -15.273475
Extrapolated:   -15.273475

Fermi levels: -4.49981, -4.49981

 Band  Eigenvalues  Occupancy
    0    -24.27033    2.00000
    1    -20.97337    2.00000
    2    -20.97337    2.00000
    3    -20.97337    2.00000
    4    -15.23974    2.00000
    5    -12.13449    2.00000
    6    -12.13449    2.00000
    7    -12.13449    2.00000
    8     -8.65025    2.00000
    9     -8.65025    2.00000
   10     -8.01548    2.00000
   11     -8.01548    2.00000
   12     -8.01548    2.00000
   13     -7.07668    2.00000
   14     -7.07668    2.00000
   15     -7.07668    2.00000
   16     -1.92294    0.00000
   17     -0.49797    0.00000
   18     -0.49797    0.00000
   19     -0.49797    0.00000
   20      7.91910    0.00000
   21      7.91910    0.00000
   22      7.91910    0.00000
   23     10.45889    0.00000
   24     10.45889    0.00000
   25     11.59310    0.00000
   26     11.59310    0.00000
   27     11.59310    0.00000
   28     12.43171    0.00000
   29     12.70520    0.00000
   30     12.70520    0.00000
   31     12.70520    0.00000
   32     12.84352    0.00000
   33     12.84352    0.00000
   34     13.43466    0.00000
   35     13.43466    0.00000
   36     13.43466    0.00000
   37     14.67651    0.00000
   38     14.67651    0.00000
   39     14.67651    0.00000
   40     15.06904    0.00000
   41     15.06904    0.00000
   42     15.06904    0.00000
   43     15.49522    0.00000
   44     16.30759    0.00000
   45     16.30759    0.00000
   46     18.49755    0.00000
   47     18.49755    0.00000
   48     18.49755    0.00000
   49     19.83661    0.00000
   50     19.83661    0.00000
   51     19.83661    0.00000
   52     21.83287    0.00000
   53     26.05937    0.00000
   54     26.05937    0.00000
   55     26.05937    0.00000
   56     31.96451    0.00000
   57     31.96451    0.00000
   58     31.96451    0.00000
   59     34.43216    0.00000
   60     34.43216    0.00000
   61     34.67164    0.00000
   62     39.65931    0.00000
   63     39.65931    0.00000
   64     39.65931    0.00000

Gap: 5.154 eV
Transition (v -> c):
  (s=0, k=0, n=15, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=16, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.009     0.009   0.0% |
LCAO WFS Initialize:                   1.451     0.041   0.2% |
 Hamiltonian:                          1.410     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.064     0.064   0.3% |
  Hartree integrate/restrict:          0.029     0.029   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.395     0.007   0.0% |
   Communicate bwd 0:                  0.080     0.080   0.3% |
   Communicate bwd 1:                  0.074     0.074   0.3% |
   Communicate fwd 0:                  0.078     0.078   0.3% |
   Communicate fwd 1:                  0.081     0.081   0.4% |
   fft:                                0.024     0.024   0.1% |
   fft2:                               0.051     0.051   0.2% |
  XC 3D grid:                          0.912     0.912   4.0% |-|
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            19.080     0.002   0.0% |
 Direct Minimisation step:            17.611     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.135     0.003   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.022     0.022   0.1% |
   Potential matrix:                   0.106     0.106   0.5% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.012     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.009     0.009   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.011     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.010     0.010   0.0% |
  Update Kohn-Sham energy:            17.448     0.000   0.0% |
   Density:                            0.618     0.000   0.0% |
    Atomic density matrices:           0.034     0.034   0.1% |
    Mix:                               0.469     0.469   2.0% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.109     0.008   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.101     0.101   0.4% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       16.831     0.001   0.0% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.006     0.006   0.0% |
    Communicate:                       0.769     0.769   3.3% ||
    Hartree integrate/restrict:        0.348     0.348   1.5% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           4.745     0.092   0.4% |
     Communicate bwd 0:                0.966     0.966   4.2% |-|
     Communicate bwd 1:                0.923     0.923   4.0% |-|
     Communicate fwd 0:                0.901     0.901   3.9% |-|
     Communicate fwd 1:                0.977     0.977   4.2% |-|
     fft:                              0.281     0.281   1.2% |
     fft2:                             0.605     0.605   2.6% ||
    XC 3D grid:                       10.908    10.908  47.3% |------------------|
    vbar:                              0.046     0.046   0.2% |
 Get canonical representation:         1.466     0.000   0.0% |
  LCAO eigensolver:                    0.012     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.010     0.010   0.0% |
  Update Kohn-Sham energy:             1.454     0.000   0.0% |
   Density:                            0.051     0.000   0.0% |
    Atomic density matrices:           0.003     0.003   0.0% |
    Mix:                               0.039     0.039   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.009     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.008     0.008   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.403     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.065     0.065   0.3% |
    Hartree integrate/restrict:        0.029     0.029   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.395     0.008   0.0% |
     Communicate bwd 0:                0.080     0.080   0.3% |
     Communicate bwd 1:                0.078     0.078   0.3% |
     Communicate fwd 0:                0.076     0.076   0.3% |
     Communicate fwd 1:                0.081     0.081   0.4% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.051     0.051   0.2% |
    XC 3D grid:                        0.909     0.909   3.9% |-|
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.363     0.363   1.6% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.140     2.140   9.3% |---|
-------------------------------------------------------------
Total:                                          23.046 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:55:58 2019
