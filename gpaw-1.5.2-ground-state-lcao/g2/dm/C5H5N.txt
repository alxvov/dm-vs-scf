
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:49:49 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6682.312893

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*124*120 grid
  Fine grid: 184*248*240 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*248*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 60.03 MiB
    Density: 34.37 MiB
      Arrays: 32.96 MiB
      Localized functions: 1.41 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.67 MiB
      Arrays: 21.56 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 3.99 MiB
      C [qnM]: 0.08 MiB
      S, T [2 x qmm]: 0.16 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.75 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 11
Number of atomic orbitals: 103
Number of bands in calculation: 103
Bands to converge: occupied states only
Number of valence electrons: 30

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .---------------------------------.  
           /|                                 |  
          / |                                 |  
         /  |                                 |  
        /   |                                 |  
       /    |                                 |  
      /     |                                 |  
     /      |                                 |  
    /       |                                 |  
   /        |                                 |  
  /         |                                 |  
 *          |                                 |  
 |          |                                 |  
 |          |            H                    |  
 |          |           N                     |  
 |          |         H                       |  
 |          |          CCH                    |  
 |          |          C                      |  
 |          |           C                     |  
 |          |         H H                     |  
 |          |                                 |  
 |          |                                 |  
 |          .---------------------------------.  
 |         /                                 /   
 |        /                                 /    
 |       /                                 /     
 |      /                                 /      
 |     /                                 /       
 |    /                                 /        
 |   /                                 /         
 |  /                                 /          
 | /                                 /           
 |/                                 /            
 *---------------------------------*             

Positions:
   0 N      7.000000    9.155293   10.897724    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000    9.155293    8.086874    ( 0.0000,  0.0000,  0.0000)
   2 C      7.000000   10.299570   10.193358    ( 0.0000,  0.0000,  0.0000)
   3 C      7.000000    8.011016   10.193358    ( 0.0000,  0.0000,  0.0000)
   4 C      7.000000    7.958889    8.800135    ( 0.0000,  0.0000,  0.0000)
   5 C      7.000000   10.351697    8.800135    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    9.155293    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000   11.216016   10.780529    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000    7.094570   10.780529    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.000000    8.289949    ( 0.0000,  0.0000,  0.0000)
  10 H      7.000000   11.310586    8.289949    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   18.310586    0.000000   124     0.1477
  3. axis:    no     0.000000    0.000000   17.897724   120     0.1491

  Lengths:  14.000000  18.310586  17.897724
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1497

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:49:59  +0.52          -68.972707    0      1      
iter:   2  22:50:02  -0.35          -69.082181    0      1      
iter:   3  22:50:04  -0.69          -69.093513    0      1      
iter:   4  22:50:06  -2.38          -69.097778    0      1      
iter:   5  22:50:07  -2.81          -69.097931    0      1      
iter:   6  22:50:09  -3.91          -69.097987    0      1      
iter:   7  22:50:11  -4.62          -69.097997    0      1      
iter:   8  22:50:12  -4.85          -69.097998    0      1      
iter:   9  22:50:14  -5.20          -69.097998    0      1      
iter:  10  22:50:16  -6.44          -69.097999    0      1      
iter:  11  22:50:18  -7.31          -69.097999    0      1      
iter:  12  22:50:19  -8.31          -69.097999    0      1      
iter:  13  22:50:21  -8.81          -69.097999    0      1      
iter:  14  22:50:23  -9.22          -69.097999    0      1      
iter:  15  22:50:24  -9.83          -69.097999    0      1      
iter:  16  22:50:28 -10.75          -69.097999    0      1      

Converged after 16 iterations.

Dipole moment: (0.000000, 0.000000, -0.417992) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6682.312893)

Kinetic:        +54.544280
Potential:      -60.215593
External:        +0.000000
XC:             -63.664217
Entropy (-ST):   +0.000000
Local:           +0.237532
--------------------------
Free energy:    -69.097999
Extrapolated:   -69.097999

Fermi levels: -3.65357, -3.65357

 Band  Eigenvalues  Occupancy
    0    -23.39062    2.00000
    1    -19.96704    2.00000
    2    -18.85760    2.00000
    3    -15.53444    2.00000
    4    -15.48666    2.00000
    5    -12.96601    2.00000
    6    -11.72004    2.00000
    7    -11.44024    2.00000
    8    -10.37056    2.00000
    9     -9.71689    2.00000
   10     -9.70826    2.00000
   11     -8.69816    2.00000
   12     -7.10805    2.00000
   13     -6.52974    2.00000
   14     -5.70416    2.00000
   15     -1.60298    0.00000
   16     -1.25131    0.00000
   17      1.68102    0.00000
   18      2.65513    0.00000
   19      2.75142    0.00000
   20      3.18720    0.00000
   21      3.78730    0.00000
   22      4.01596    0.00000
   23      5.61192    0.00000
   24      6.94221    0.00000
   25      7.22225    0.00000
   26      7.35091    0.00000
   27      9.11627    0.00000
   28      9.27868    0.00000
   29      9.65482    0.00000
   30     10.61026    0.00000
   31     10.82828    0.00000
   32     10.99010    0.00000
   33     11.15933    0.00000
   34     11.21485    0.00000
   35     11.49048    0.00000
   36     12.30843    0.00000
   37     12.60693    0.00000
   38     13.31357    0.00000
   39     13.52887    0.00000
   40     14.40795    0.00000
   41     14.93900    0.00000
   42     15.80600    0.00000
   43     16.63305    0.00000
   44     16.90679    0.00000
   45     17.68313    0.00000
   46     17.72645    0.00000
   47     18.03685    0.00000
   48     20.43166    0.00000
   49     21.27918    0.00000
   50     21.61068    0.00000
   51     21.86212    0.00000
   52     22.24080    0.00000
   53     23.37236    0.00000
   54     23.95356    0.00000
   55     24.33972    0.00000
   56     24.51913    0.00000
   57     25.41397    0.00000
   58     25.67938    0.00000
   59     26.33351    0.00000
   60     27.01381    0.00000
   61     29.59315    0.00000
   62     31.11954    0.00000
   63     31.77177    0.00000
   64     32.34546    0.00000
   65     32.92735    0.00000
   66     33.87164    0.00000
   67     34.25811    0.00000
   68     34.50507    0.00000
   69     34.63740    0.00000
   70     35.36328    0.00000
   71     35.88007    0.00000
   72     36.03066    0.00000
   73     37.01173    0.00000
   74     37.46960    0.00000
   75     37.76123    0.00000
   76     40.30786    0.00000
   77     40.69165    0.00000
   78     41.67059    0.00000
   79     41.70828    0.00000
   80     42.45952    0.00000
   81     42.89220    0.00000
   82     42.92651    0.00000
   83     43.57527    0.00000
   84     44.51237    0.00000
   85     44.83289    0.00000
   86     44.95932    0.00000
   87     46.35934    0.00000
   88     48.30485    0.00000
   89     50.49713    0.00000
   90     52.58172    0.00000
   91     52.86690    0.00000
   92     53.99306    0.00000
   93     57.20315    0.00000
   94     59.95582    0.00000
   95     60.11943    0.00000
   96     62.91226    0.00000
   97     63.21855    0.00000
   98     66.34010    0.00000
   99     71.17825    0.00000
  100     71.88948    0.00000
  101     72.24623    0.00000
  102     85.42678    0.00000

Gap: 4.101 eV
Transition (v -> c):
  (s=0, k=0, n=14, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=15, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.001     0.001   0.0% |
Basis functions set positions:         0.018     0.018   0.0% |
LCAO WFS Initialize:                   1.606     0.045   0.1% |
 Hamiltonian:                          1.561     0.000   0.0% |
  Atomic:                              0.047     0.001   0.0% |
   XC Correction:                      0.047     0.047   0.1% |
  Calculate atomic Hamiltonians:       0.003     0.003   0.0% |
  Communicate:                         0.025     0.025   0.1% |
  Hartree integrate/restrict:          0.029     0.029   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.459     0.011   0.0% |
   Communicate bwd 0:                  0.086     0.086   0.2% |
   Communicate bwd 1:                  0.082     0.082   0.2% |
   Communicate fwd 0:                  0.080     0.080   0.2% |
   Communicate fwd 1:                  0.088     0.088   0.2% |
   fft:                                0.043     0.043   0.1% |
   fft2:                               0.069     0.069   0.2% |
  XC 3D grid:                          0.988     0.988   2.6% ||
  vbar:                                0.006     0.006   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            34.035     0.004   0.0% |
 Direct Minimisation step:            32.366     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.734     0.006   0.0% |
   Construct Gradient Matrix:          0.005     0.005   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.324     0.324   0.8% |
   Potential matrix:                   0.396     0.396   1.0% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.040     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.017     0.017   0.0% |
   Orbital Layouts:                    0.002     0.002   0.0% |
   Potential matrix:                   0.021     0.021   0.1% |
  Preconditioning::                    0.003     0.003   0.0% |
  Unitary rotation:                    0.030     0.004   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.026     0.026   0.1% |
  Update Kohn-Sham energy:            31.553     0.000   0.0% |
   Density:                            1.518     0.000   0.0% |
    Atomic density matrices:           0.340     0.340   0.9% |
    Mix:                               0.786     0.786   2.0% ||
    Multipole moments:                 0.004     0.004   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.383     0.014   0.0% |
     Calculate density matrix:         0.003     0.003   0.0% |
     Construct density:                0.366     0.366   1.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       30.035     0.002   0.0% |
    Atomic:                            0.901     0.010   0.0% |
     XC Correction:                    0.891     0.891   2.3% ||
    Calculate atomic Hamiltonians:     0.050     0.050   0.1% |
    Communicate:                       0.482     0.482   1.3% ||
    Hartree integrate/restrict:        0.772     0.772   2.0% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           8.924     0.224   0.6% |
     Communicate bwd 0:                1.632     1.632   4.2% |-|
     Communicate bwd 1:                1.707     1.707   4.4% |-|
     Communicate fwd 0:                1.515     1.515   3.9% |-|
     Communicate fwd 1:                1.706     1.706   4.4% |-|
     fft:                              0.808     0.808   2.1% ||
     fft2:                             1.331     1.331   3.5% ||
    XC 3D grid:                       18.820    18.820  48.9% |-------------------|
    vbar:                              0.080     0.080   0.2% |
 Get canonical representation:         1.665     0.000   0.0% |
  LCAO eigensolver:                    0.040     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.015     0.015   0.0% |
   Orbital Layouts:                    0.002     0.002   0.0% |
   Potential matrix:                   0.023     0.023   0.1% |
  Update Kohn-Sham energy:             1.625     0.000   0.0% |
   Density:                            0.078     0.000   0.0% |
    Atomic density matrices:           0.018     0.018   0.0% |
    Mix:                               0.040     0.040   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.020     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.019     0.019   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.546     0.000   0.0% |
    Atomic:                            0.048     0.001   0.0% |
     XC Correction:                    0.047     0.047   0.1% |
    Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
    Communicate:                       0.025     0.025   0.1% |
    Hartree integrate/restrict:        0.029     0.029   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.456     0.011   0.0% |
     Communicate bwd 0:                0.086     0.086   0.2% |
     Communicate bwd 1:                0.082     0.082   0.2% |
     Communicate fwd 0:                0.080     0.080   0.2% |
     Communicate fwd 1:                0.086     0.086   0.2% |
     fft:                              0.042     0.042   0.1% |
     fft2:                             0.070     0.070   0.2% |
    XC 3D grid:                        0.981     0.981   2.5% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.003     0.003   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.525     0.525   1.4% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.299     2.299   6.0% |-|
-------------------------------------------------------------
Total:                                          38.490 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:50:28 2019
