
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:10:50 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -12911.579372

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*108*112 grid
  Fine grid: 184*216*224 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 47.16 MiB
    Density: 27.78 MiB
      Arrays: 26.74 MiB
      Localized functions: 1.04 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 17.58 MiB
      Arrays: 17.49 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 1.81 MiB
      C [qnM]: 0.05 MiB
      S, T [2 x qmm]: 0.09 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.67 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 78
Number of bands in calculation: 78
Bands to converge: occupied states only
Number of valence electrons: 36

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |            F                    |  
 |         |          F                      |  
 |         |           C                     |  
 |         |           C                     |  
 |         |            F                    |  
 |         |          F                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 C      7.000000    8.112665    9.048882    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000    8.112665    7.722422    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000    9.225330    9.771304    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    7.000000    9.771304    ( 0.0000,  0.0000,  0.0000)
   4 F      7.000000    9.225330    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.225330    0.000000   108     0.1502
  3. axis:    no     0.000000    0.000000   16.771304   112     0.1497

  Lengths:  14.000000  16.225330  16.771304
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:10:57  +0.22          -30.177798    0      1      
iter:   2  23:11:00  -1.53          -30.232183    0      1      
iter:   3  23:11:01  -2.73          -30.233204    0      1      
iter:   4  23:11:02  -3.60          -30.233322    0      1      
iter:   5  23:11:04  -5.08          -30.233341    0      1      
iter:   6  23:11:05  -6.06          -30.233342    0      1      
iter:   7  23:11:06  -5.91          -30.233342    0      1      
iter:   8  23:11:08  -7.75          -30.233342    0      1      
iter:   9  23:11:09  -8.21          -30.233342    0      1      
iter:  10  23:11:10  -9.81          -30.233342    0      1      
iter:  11  23:11:13 -10.48          -30.233342    0      1      

Converged after 11 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -12911.579372)

Kinetic:        +37.466687
Potential:      -33.224633
External:        +0.000000
XC:             -34.943251
Entropy (-ST):   +0.000000
Local:           +0.467855
--------------------------
Free energy:    -30.233342
Extrapolated:   -30.233342

Fermi levels: -2.89231, -2.89231

 Band  Eigenvalues  Occupancy
    0    -32.58794    2.00000
    1    -32.21848    2.00000
    2    -31.13885    2.00000
    3    -30.84671    2.00000
    4    -19.80698    2.00000
    5    -16.13070    2.00000
    6    -14.89013    2.00000
    7    -14.84171    2.00000
    8    -13.68368    2.00000
    9    -13.17378    2.00000
   10    -11.98039    2.00000
   11    -11.20987    2.00000
   12    -10.61751    2.00000
   13    -10.47771    2.00000
   14    -10.44069    2.00000
   15    -10.34118    2.00000
   16     -9.90340    2.00000
   17     -5.76948    2.00000
   18     -0.01514    0.00000
   19      0.76007    0.00000
   20      2.23778    0.00000
   21      3.00191    0.00000
   22      9.22544    0.00000
   23      9.28825    0.00000
   24      9.40540    0.00000
   25     10.35819    0.00000
   26     11.91950    0.00000
   27     12.05572    0.00000
   28     12.21972    0.00000
   29     12.68769    0.00000
   30     15.70728    0.00000
   31     18.92364    0.00000
   32     18.96800    0.00000
   33     19.60335    0.00000
   34     19.77829    0.00000
   35     21.27874    0.00000
   36     21.31322    0.00000
   37     21.54898    0.00000
   38     21.59502    0.00000
   39     21.98511    0.00000
   40     23.21313    0.00000
   41     24.45999    0.00000
   42     24.99742    0.00000
   43     25.19774    0.00000
   44     26.79434    0.00000
   45     28.83420    0.00000
   46     31.22474    0.00000
   47     31.94332    0.00000
   48     35.63089    0.00000
   49     37.31570    0.00000
   50     37.46266    0.00000
   51     41.23785    0.00000
   52     46.38892    0.00000
   53     47.31645    0.00000
   54     51.77477    0.00000
   55     54.05572    0.00000
   56     55.97399    0.00000
   57     56.01777    0.00000
   58     57.06749    0.00000
   59     57.06799    0.00000
   60     57.46420    0.00000
   61     58.13186    0.00000
   62     58.36157    0.00000
   63     60.16667    0.00000
   64     60.50665    0.00000
   65     60.79403    0.00000
   66     63.06804    0.00000
   67     65.82036    0.00000
   68     66.12874    0.00000
   69     70.42415    0.00000
   70     72.30655    0.00000
   71     74.33116    0.00000
   72     74.35355    0.00000
   73     75.78795    0.00000
   74     77.82385    0.00000
   75     79.16217    0.00000
   76     85.06955    0.00000
   77     85.27343    0.00000

Gap: 5.754 eV
Transition (v -> c):
  (s=0, k=0, n=17, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=18, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.008     0.008   0.0% |
LCAO WFS Initialize:                   1.281     0.035   0.2% |
 Hamiltonian:                          1.245     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.048     0.048   0.2% |
  Hartree integrate/restrict:          0.025     0.025   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.361     0.008   0.0% |
   Communicate bwd 0:                  0.071     0.071   0.3% |
   Communicate bwd 1:                  0.066     0.066   0.3% |
   Communicate fwd 0:                  0.068     0.068   0.3% |
   Communicate fwd 1:                  0.072     0.072   0.3% |
   fft:                                0.022     0.022   0.1% |
   fft2:                               0.055     0.055   0.2% |
  XC 3D grid:                          0.803     0.803   3.5% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            19.312     0.003   0.0% |
 Direct Minimisation step:            18.019     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.142     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.027     0.027   0.1% |
   Potential matrix:                   0.106     0.106   0.5% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.011     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.008     0.008   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.016     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.014     0.014   0.1% |
  Update Kohn-Sham energy:            17.845     0.000   0.0% |
   Density:                            0.624     0.000   0.0% |
    Atomic density matrices:           0.041     0.041   0.2% |
    Mix:                               0.470     0.470   2.1% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.108     0.007   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.100     0.100   0.4% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       17.220     0.001   0.0% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.007     0.007   0.0% |
    Communicate:                       0.676     0.676   3.0% ||
    Hartree integrate/restrict:        0.340     0.340   1.5% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           4.955     0.082   0.4% |
     Communicate bwd 0:                0.973     0.973   4.3% |-|
     Communicate bwd 1:                0.940     0.940   4.1% |-|
     Communicate fwd 0:                0.913     0.913   4.0% |-|
     Communicate fwd 1:                0.963     0.963   4.2% |-|
     fft:                              0.310     0.310   1.4% ||
     fft2:                             0.774     0.774   3.4% ||
    XC 3D grid:                       11.186    11.186  48.9% |-------------------|
    vbar:                              0.049     0.049   0.2% |
 Get canonical representation:         1.290     0.000   0.0% |
  LCAO eigensolver:                    0.011     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.008     0.008   0.0% |
  Update Kohn-Sham energy:             1.279     0.000   0.0% |
   Density:                            0.044     0.000   0.0% |
    Atomic density matrices:           0.003     0.003   0.0% |
    Mix:                               0.033     0.033   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.008     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.007     0.007   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.235     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.048     0.048   0.2% |
    Hartree integrate/restrict:        0.024     0.024   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.360     0.007   0.0% |
     Communicate bwd 0:                0.069     0.069   0.3% |
     Communicate bwd 1:                0.067     0.067   0.3% |
     Communicate fwd 0:                0.068     0.068   0.3% |
     Communicate fwd 1:                0.072     0.072   0.3% |
     fft:                              0.022     0.022   0.1% |
     fft2:                             0.055     0.055   0.2% |
    XC 3D grid:                        0.799     0.799   3.5% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.350     0.350   1.5% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.927     1.927   8.4% |--|
-------------------------------------------------------------
Total:                                          22.882 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 23:11:13 2019
