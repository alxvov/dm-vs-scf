
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:35:37 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -7923.814737

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*108*96 grid
  Fine grid: 216*216*192 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 65.99 MiB
    Density: 37.24 MiB
      Arrays: 36.21 MiB
      Localized functions: 1.03 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 26.99 MiB
      Arrays: 26.91 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 1.76 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.74 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 28
Number of bands in calculation: 28
Bands to converge: occupied states only
Number of valence electrons: 7

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |              Si                       |  
 |         |          H     H                      |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 Si     8.223937    7.706640    7.449360    ( 0.0000,  0.0000,  1.0000)
   1 H      8.223937    9.119920    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      9.447874    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.447874    0.000000    0.000000   108     0.1523
  2. axis:    no     0.000000   16.119920    0.000000   108     0.1493
  3. axis:    no     0.000000    0.000000   14.449360    96     0.1505

  Lengths:  16.447874  16.119920  14.449360
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  22:35:45  +1.50          -13.308357    0      1        +1.0000
iter:   2  22:35:47  -0.19          -13.410305    0      1        +1.0000
iter:   3  22:35:49  -1.41          -13.412375    0      1        +1.0000
iter:   4  22:35:51  -3.06          -13.412496    0      1        +1.0000
iter:   5  22:35:53  -3.94          -13.412500    0      1        +1.0000
iter:   6  22:35:55  -5.27          -13.412501    0      1        +1.0000
iter:   7  22:35:57  -6.32          -13.412501    0      1        +1.0000
iter:   8  22:35:59  -6.68          -13.412501    0      1        +1.0000
iter:   9  22:36:01  -8.61          -13.412501    0      1        +1.0000
iter:  10  22:36:03  -9.28          -13.412501    0      1        +1.0000
iter:  11  22:36:07 -10.65          -13.412501    0      1        +1.0000

Converged after 11 iterations.

Dipole moment: (0.000000, -0.000018, 0.020627) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000000)
Local magnetic moments:
   0 Si ( 0.000000,  0.000000,  0.309243)
   1 H  ( 0.000000,  0.000000,  0.002955)
   2 H  ( 0.000000,  0.000000,  0.002961)
   3 H  ( 0.000000,  0.000000,  0.002961)

Energy contributions relative to reference atoms: (reference = -7923.814737)

Kinetic:        +12.733966
Potential:      -13.749941
External:        +0.000000
XC:             -12.368476
Entropy (-ST):   +0.000000
Local:           -0.028051
--------------------------
Free energy:    -13.412501
Extrapolated:   -13.412501

Spin contamination: 0.016932 electrons
Fermi levels: -2.22412, -5.90496

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -13.28642    1.00000    -12.78939    1.00000
    1     -8.63964    1.00000     -8.45842    1.00000
    2     -8.63956    1.00000     -8.45830    1.00000
    3     -5.19844    1.00000     -3.35163    0.00000
    4      0.75019    0.00000      1.27853    0.00000
    5      0.75067    0.00000      1.27913    0.00000
    6      1.06757    0.00000      1.51117    0.00000
    7      5.21938    0.00000      5.41485    0.00000
    8      5.21966    0.00000      5.41529    0.00000
    9      5.54390    0.00000      6.50443    0.00000
   10      6.52410    0.00000      7.34275    0.00000
   11      7.47148    0.00000      8.35242    0.00000
   12      7.47166    0.00000      8.35251    0.00000
   13     10.81486    0.00000     11.26396    0.00000
   14     10.81496    0.00000     11.26399    0.00000
   15     12.32646    0.00000     13.20093    0.00000
   16     16.41868    0.00000     17.18068    0.00000
   17     19.16565    0.00000     19.49921    0.00000
   18     19.16708    0.00000     19.50059    0.00000
   19     21.35465    0.00000     21.40572    0.00000
   20     24.23939    0.00000     24.67359    0.00000
   21     24.23961    0.00000     24.67378    0.00000
   22     28.21561    0.00000     28.55318    0.00000
   23     31.58091    0.00000     31.89679    0.00000
   24     31.58153    0.00000     31.89733    0.00000
   25     38.43111    0.00000     38.64542    0.00000
   26     42.90920    0.00000     43.21393    0.00000
   27     42.91200    0.00000     43.21662    0.00000

Gap: 1.847 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=3, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.008     0.008   0.0% |
LCAO WFS Initialize:                   1.936     0.066   0.2% |
 Hamiltonian:                          1.870     0.003   0.0% |
  Atomic:                              0.045     0.045   0.2% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.062     0.062   0.2% |
  Hartree integrate/restrict:          0.043     0.043   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.344     0.010   0.0% |
   Communicate bwd 0:                  0.068     0.068   0.2% |
   Communicate bwd 1:                  0.065     0.065   0.2% |
   Communicate fwd 0:                  0.064     0.064   0.2% |
   Communicate fwd 1:                  0.069     0.069   0.2% |
   fft:                                0.021     0.021   0.1% |
   fft2:                               0.046     0.046   0.2% |
  XC 3D grid:                          1.365     1.365   4.6% |-|
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.001     0.001   0.0% |
SCF-cycle:                            25.462     0.003   0.0% |
 Direct Minimisation step:            23.506     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.147     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.015     0.015   0.0% |
   Potential matrix:                   0.125     0.125   0.4% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.012     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.010     0.010   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.015     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.013     0.013   0.0% |
  Update Kohn-Sham energy:            23.328     0.000   0.0% |
   Density:                            0.923     0.000   0.0% |
    Atomic density matrices:           0.028     0.028   0.1% |
    Mix:                               0.741     0.741   2.5% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.148     0.013   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.134     0.134   0.5% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       22.404     0.024   0.1% |
    Atomic:                            0.545     0.545   1.8% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
    Communicate:                       0.741     0.741   2.5% ||
    Hartree integrate/restrict:        0.508     0.508   1.7% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           4.190     0.120   0.4% |
     Communicate bwd 0:                0.851     0.851   2.9% ||
     Communicate bwd 1:                0.818     0.818   2.8% ||
     Communicate fwd 0:                0.765     0.765   2.6% ||
     Communicate fwd 1:                0.841     0.841   2.8% ||
     fft:                              0.252     0.252   0.9% |
     fft2:                             0.542     0.542   1.8% ||
    XC 3D grid:                       16.345    16.345  55.3% |---------------------|
    vbar:                              0.041     0.041   0.1% |
 Get canonical representation:         1.953     0.000   0.0% |
  LCAO eigensolver:                    0.012     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.010     0.010   0.0% |
  Update Kohn-Sham energy:             1.940     0.000   0.0% |
   Density:                            0.077     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.062     0.062   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.012     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.011     0.011   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.863     0.002   0.0% |
    Atomic:                            0.045     0.045   0.2% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.062     0.062   0.2% |
    Hartree integrate/restrict:        0.043     0.043   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.347     0.010   0.0% |
     Communicate bwd 0:                0.071     0.071   0.2% |
     Communicate bwd 1:                0.067     0.067   0.2% |
     Communicate fwd 0:                0.064     0.064   0.2% |
     Communicate fwd 1:                0.068     0.068   0.2% |
     fft:                              0.021     0.021   0.1% |
     fft2:                             0.045     0.045   0.2% |
    XC 3D grid:                        1.360     1.360   4.6% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.201     0.201   0.7% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.924     1.924   6.5% |--|
-------------------------------------------------------------
Total:                                          29.536 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:36:07 2019
