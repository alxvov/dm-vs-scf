
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:03:46 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -7469.013872

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*108*100 grid
  Fine grid: 184*216*200 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 40.70 MiB
    Density: 24.41 MiB
      Arrays: 23.85 MiB
      Localized functions: 0.57 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 15.64 MiB
      Arrays: 15.60 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.65 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.61 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 39
Number of bands in calculation: 39
Bands to converge: occupied states only
Number of valence electrons: 20

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           O                     |  
 |         |            F                    |  
 |         |          F                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 F      7.000000    9.221152    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 O      7.000000    8.110576    7.889619    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.221152    0.000000   108     0.1502
  3. axis:    no     0.000000    0.000000   14.889619   100     0.1489

  Lengths:  14.000000  16.221152  14.889619
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1504

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:03:53  +1.18           -7.255747    0      1      
iter:   2  23:03:55  -0.33           -7.385237    0      1      
iter:   3  23:03:56  -1.31           -7.391427    0      1      
iter:   4  23:03:57  -1.81           -7.392376    0      1      
iter:   5  23:03:59  -2.83           -7.392910    0      1      
iter:   6  23:04:00  -3.22           -7.392954    0      1      
iter:   7  23:04:02  -4.41           -7.392961    0      1      
iter:   8  23:04:03  -5.31           -7.392962    0      1      
iter:   9  23:04:04  -6.48           -7.392962    0      1      
iter:  10  23:04:05  -7.18           -7.392962    0      1      
iter:  11  23:04:06  -7.89           -7.392962    0      1      
iter:  12  23:04:08  -8.16           -7.392962    0      1      
iter:  13  23:04:09  -8.56           -7.392962    0      1      
iter:  14  23:04:11 -10.26           -7.392962    0      1      

Converged after 14 iterations.

Dipole moment: (0.000000, 0.000000, 0.056380) |e|*Ang

Energy contributions relative to reference atoms: (reference = -7469.013872)

Kinetic:        +17.687109
Potential:      -14.517132
External:        +0.000000
XC:             -10.859292
Entropy (-ST):   +0.000000
Local:           +0.296354
--------------------------
Free energy:     -7.392962
Extrapolated:    -7.392962

Fermi levels: -5.42668, -5.42668

 Band  Eigenvalues  Occupancy
    0    -33.03674    2.00000
    1    -30.24317    2.00000
    2    -23.15380    2.00000
    3    -15.01926    2.00000
    4    -13.56172    2.00000
    5    -12.37456    2.00000
    6    -10.24219    2.00000
    7    -10.12464    2.00000
    8     -9.82911    2.00000
    9     -7.08655    2.00000
   10     -3.76680    0.00000
   11     -3.02711    0.00000
   12     15.47264    0.00000
   13     15.63433    0.00000
   14     16.12199    0.00000
   15     18.73362    0.00000
   16     20.65416    0.00000
   17     20.96513    0.00000
   18     21.24170    0.00000
   19     21.25025    0.00000
   20     23.95919    0.00000
   21     29.11470    0.00000
   22     39.05175    0.00000
   23     40.17510    0.00000
   24     41.18111    0.00000
   25     42.33012    0.00000
   26     45.33758    0.00000
   27     51.95092    0.00000
   28     52.41636    0.00000
   29     57.44698    0.00000
   30     57.62639    0.00000
   31     58.24047    0.00000
   32     58.24083    0.00000
   33     61.12233    0.00000
   34     67.28008    0.00000
   35     67.59093    0.00000
   36     71.90586    0.00000
   37     76.41938    0.00000
   38     77.74708    0.00000

Gap: 3.320 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.003     0.003   0.0% |
LCAO WFS Initialize:                   1.133     0.031   0.1% |
 Hamiltonian:                          1.102     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.048     0.048   0.2% |
  Hartree integrate/restrict:          0.022     0.022   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.314     0.007   0.0% |
   Communicate bwd 0:                  0.061     0.061   0.2% |
   Communicate bwd 1:                  0.060     0.060   0.2% |
   Communicate fwd 0:                  0.056     0.056   0.2% |
   Communicate fwd 1:                  0.061     0.061   0.2% |
   fft:                                0.018     0.018   0.1% |
   fft2:                               0.050     0.050   0.2% |
  XC 3D grid:                          0.713     0.713   2.9% ||
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            21.458     0.003   0.0% |
 Direct Minimisation step:            20.328     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.052     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.010     0.010   0.0% |
   Potential matrix:                   0.036     0.036   0.1% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.003     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.014     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.012     0.012   0.0% |
  Update Kohn-Sham energy:            20.253     0.000   0.0% |
   Density:                            0.594     0.000   0.0% |
    Atomic density matrices:           0.019     0.019   0.1% |
    Mix:                               0.526     0.526   2.1% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.044     0.008   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.035     0.035   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       19.659     0.002   0.0% |
    Atomic:                            0.007     0.007   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
    Communicate:                       0.871     0.871   3.5% ||
    Hartree integrate/restrict:        0.407     0.407   1.6% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.647     0.135   0.5% |
     Communicate bwd 0:                1.101     1.101   4.5% |-|
     Communicate bwd 1:                1.083     1.083   4.4% |-|
     Communicate fwd 0:                1.009     1.009   4.1% |-|
     Communicate fwd 1:                1.092     1.092   4.4% |-|
     fft:                              0.326     0.326   1.3% ||
     fft2:                             0.901     0.901   3.7% ||
    XC 3D grid:                       12.658    12.658  51.3% |--------------------|
    vbar:                              0.058     0.058   0.2% |
 Get canonical representation:         1.127     0.000   0.0% |
  LCAO eigensolver:                    0.003     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Update Kohn-Sham energy:             1.124     0.000   0.0% |
   Density:                            0.033     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.029     0.029   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.002     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.002     0.002   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.091     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.049     0.049   0.2% |
    Hartree integrate/restrict:        0.022     0.022   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.315     0.007   0.0% |
     Communicate bwd 0:                0.062     0.062   0.3% |
     Communicate bwd 1:                0.060     0.060   0.2% |
     Communicate fwd 0:                0.056     0.056   0.2% |
     Communicate fwd 1:                0.062     0.062   0.2% |
     fft:                              0.018     0.018   0.1% |
     fft2:                             0.049     0.049   0.2% |
    XC 3D grid:                        0.701     0.701   2.8% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.340     0.340   1.4% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.730     1.730   7.0% |--|
-------------------------------------------------------------
Total:                                          24.667 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 23:04:11 2019
