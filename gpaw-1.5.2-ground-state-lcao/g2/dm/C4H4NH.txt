
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:19:56 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Reference energy: -5654.684777

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*120*120 grid
  Fine grid: 184*240*240 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*240*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 57.38 MiB
    Density: 33.08 MiB
      Arrays: 31.89 MiB
      Localized functions: 1.19 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.95 MiB
      Arrays: 20.86 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.10 MiB
    Wavefunctions: 3.35 MiB
      C [qnM]: 0.06 MiB
      S, T [2 x qmm]: 0.12 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.16 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 90
Number of bands in calculation: 90
Bands to converge: occupied states only
Number of valence electrons: 26

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .---------------------------------.  
           /|                                 |  
          / |                                 |  
         /  |                                 |  
        /   |                                 |  
       /    |                                 |  
      /     |                                 |  
     /      |                                 |  
    /       |                                 |  
   /        |                                 |  
  /         |                                 |  
 *          |                                 |  
 |          |                                 |  
 |          |                                 |  
 |          |           HH                    |  
 |          |           N                     |  
 |          |           C                     |  
 |          |         HCC                     |  
 |          |          CH                     |  
 |          |                                 |  
 |          |          H                      |  
 |          |                                 |  
 |          .---------------------------------.  
 |         /                                 /   
 |        /                                 /    
 |       /                                 /     
 |      /                                 /      
 |     /                                 /       
 |    /                                 /        
 |   /                                 /         
 |  /                                 /          
 | /                                 /           
 |/                                 /            
 *---------------------------------*             

Positions:
   0 H      7.000000    9.112872   10.978381    ( 0.0000,  0.0000,  0.0000)
   1 N      7.000000    9.112872    9.967769    ( 0.0000,  0.0000,  0.0000)
   2 C      7.000000   10.237388    9.182650    ( 0.0000,  0.0000,  0.0000)
   3 C      7.000000    7.988356    9.182650    ( 0.0000,  0.0000,  0.0000)
   4 C      7.000000    9.821279    7.865278    ( 0.0000,  0.0000,  0.0000)
   5 C      7.000000    8.404465    7.865278    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000   11.225744    9.619581    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000    7.000000    9.619581    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000   10.470124    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.755620    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   18.225744    0.000000   120     0.1519
  3. axis:    no     0.000000    0.000000   17.978381   120     0.1498

  Lengths:  14.000000  18.225744  17.978381
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1513

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:20:05  +0.69          -60.209132    0      1      
iter:   2  22:20:08  -0.06          -60.337725    0      1      
iter:   3  22:20:09  -0.54          -60.355139    0      1      
iter:   4  22:20:11  -1.96          -60.359367    0      1      
iter:   5  22:20:13  -2.17          -60.359547    0      1      
iter:   6  22:20:14  -3.95          -60.359679    0      1      
iter:   7  22:20:16  -4.54          -60.359685    0      1      
iter:   8  22:20:17  -5.23          -60.359686    0      1      
iter:   9  22:20:19  -5.19          -60.359686    0      1      
iter:  10  22:20:20  -6.79          -60.359686    0      1      
iter:  11  22:20:22  -7.47          -60.359686    0      1      
iter:  12  22:20:24  -8.46          -60.359686    0      1      
iter:  13  22:20:25  -9.15          -60.359686    0      1      
iter:  14  22:20:28 -10.33          -60.359686    0      1      

Converged after 14 iterations.

Dipole moment: (0.000000, 0.000000, 0.410461) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5654.684777)

Kinetic:        +48.699412
Potential:      -52.653675
External:        +0.000000
XC:             -56.634477
Entropy (-ST):   +0.000000
Local:           +0.229054
--------------------------
Free energy:    -60.359686
Extrapolated:   -60.359686

Fermi levels: -2.27625, -2.27625

 Band  Eigenvalues  Occupancy
    0    -24.25886    2.00000
    1    -18.96671    2.00000
    2    -17.62523    2.00000
    3    -14.40490    2.00000
    4    -13.71729    2.00000
    5    -13.18818    2.00000
    6    -10.24227    2.00000
    7    -10.12580    2.00000
    8     -9.65975    2.00000
    9     -9.12022    2.00000
   10     -8.85899    2.00000
   11     -5.66066    2.00000
   12     -4.89328    2.00000
   13      0.34078    0.00000
   14      1.43425    0.00000
   15      1.47496    0.00000
   16      2.98668    0.00000
   17      3.39698    0.00000
   18      3.66817    0.00000
   19      4.01480    0.00000
   20      5.47951    0.00000
   21      5.65901    0.00000
   22      7.94684    0.00000
   23      8.22208    0.00000
   24      8.38316    0.00000
   25     10.33843    0.00000
   26     10.64666    0.00000
   27     11.02022    0.00000
   28     11.13907    0.00000
   29     11.54215    0.00000
   30     11.61381    0.00000
   31     11.70422    0.00000
   32     12.03615    0.00000
   33     13.25838    0.00000
   34     13.80134    0.00000
   35     15.22502    0.00000
   36     15.54482    0.00000
   37     16.30766    0.00000
   38     17.49173    0.00000
   39     17.54554    0.00000
   40     18.64555    0.00000
   41     20.81170    0.00000
   42     22.03429    0.00000
   43     22.36479    0.00000
   44     22.93201    0.00000
   45     23.55633    0.00000
   46     23.64536    0.00000
   47     23.84702    0.00000
   48     23.96526    0.00000
   49     24.43536    0.00000
   50     24.69853    0.00000
   51     25.41598    0.00000
   52     26.92903    0.00000
   53     28.40285    0.00000
   54     32.36904    0.00000
   55     32.56838    0.00000
   56     32.77687    0.00000
   57     32.90708    0.00000
   58     33.67775    0.00000
   59     34.30322    0.00000
   60     36.59548    0.00000
   61     36.60598    0.00000
   62     37.67734    0.00000
   63     38.57956    0.00000
   64     38.89993    0.00000
   65     39.06754    0.00000
   66     39.24146    0.00000
   67     40.34002    0.00000
   68     40.50865    0.00000
   69     40.83923    0.00000
   70     41.71803    0.00000
   71     42.26783    0.00000
   72     43.13825    0.00000
   73     43.46898    0.00000
   74     44.10138    0.00000
   75     46.60836    0.00000
   76     49.95610    0.00000
   77     50.49032    0.00000
   78     50.53377    0.00000
   79     53.20933    0.00000
   80     53.91991    0.00000
   81     58.91032    0.00000
   82     59.01691    0.00000
   83     62.22963    0.00000
   84     62.63373    0.00000
   85     64.61666    0.00000
   86     64.93593    0.00000
   87     69.70895    0.00000
   88     80.40343    0.00000
   89     81.11839    0.00000

Gap: 5.234 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.001     0.001   0.0% |
Basis functions set positions:         0.014     0.014   0.0% |
LCAO WFS Initialize:                   1.521     0.043   0.1% |
 Hamiltonian:                          1.478     0.000   0.0% |
  Atomic:                              0.027     0.001   0.0% |
   XC Correction:                      0.026     0.026   0.1% |
  Calculate atomic Hamiltonians:       0.003     0.003   0.0% |
  Communicate:                         0.027     0.027   0.1% |
  Hartree integrate/restrict:          0.026     0.026   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.426     0.008   0.0% |
   Communicate bwd 0:                  0.087     0.087   0.3% |
   Communicate bwd 1:                  0.077     0.077   0.2% |
   Communicate fwd 0:                  0.080     0.080   0.2% |
   Communicate fwd 1:                  0.087     0.087   0.3% |
   fft:                                0.024     0.024   0.1% |
   fft2:                               0.064     0.064   0.2% |
  XC 3D grid:                          0.964     0.964   3.0% ||
  vbar:                                0.004     0.004   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            28.198     0.003   0.0% |
 Direct Minimisation step:            26.636     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.473     0.005   0.0% |
   Construct Gradient Matrix:          0.004     0.004   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.077     0.077   0.2% |
   Potential matrix:                   0.385     0.385   1.2% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.029     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.002     0.002   0.0% |
   Potential matrix:                   0.023     0.023   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.023     0.003   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.020     0.020   0.1% |
  Update Kohn-Sham energy:            26.104     0.000   0.0% |
   Density:                            1.132     0.000   0.0% |
    Atomic density matrices:           0.078     0.078   0.2% |
    Mix:                               0.671     0.671   2.1% ||
    Multipole moments:                 0.004     0.004   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.375     0.012   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.361     0.361   1.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       24.971     0.002   0.0% |
    Atomic:                            0.457     0.009   0.0% |
     XC Correction:                    0.448     0.448   1.4% ||
    Calculate atomic Hamiltonians:     0.039     0.039   0.1% |
    Communicate:                       0.466     0.466   1.4% ||
    Hartree integrate/restrict:        0.481     0.481   1.5% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           7.221     0.131   0.4% |
     Communicate bwd 0:                1.440     1.440   4.4% |-|
     Communicate bwd 1:                1.361     1.361   4.2% |-|
     Communicate fwd 0:                1.360     1.360   4.2% |-|
     Communicate fwd 1:                1.427     1.427   4.4% |-|
     fft:                              0.397     0.397   1.2% |
     fft2:                             1.105     1.105   3.4% ||
    XC 3D grid:                       16.234    16.234  50.1% |-------------------|
    vbar:                              0.070     0.070   0.2% |
 Get canonical representation:         1.559     0.000   0.0% |
  LCAO eigensolver:                    0.029     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.023     0.023   0.1% |
  Update Kohn-Sham energy:             1.530     0.000   0.0% |
   Density:                            0.065     0.000   0.0% |
    Atomic density matrices:           0.005     0.005   0.0% |
    Mix:                               0.038     0.038   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.022     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.021     0.021   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.465     0.000   0.0% |
    Atomic:                            0.027     0.001   0.0% |
     XC Correction:                    0.027     0.027   0.1% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.027     0.027   0.1% |
    Hartree integrate/restrict:        0.030     0.030   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.424     0.008   0.0% |
     Communicate bwd 0:                0.084     0.084   0.3% |
     Communicate bwd 1:                0.080     0.080   0.2% |
     Communicate fwd 0:                0.080     0.080   0.2% |
     Communicate fwd 1:                0.084     0.084   0.3% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.065     0.065   0.2% |
    XC 3D grid:                        0.950     0.950   2.9% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.003     0.003   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.505     0.505   1.6% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.165     2.165   6.7% |--|
-------------------------------------------------------------
Total:                                          32.410 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:20:28 2019
