
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:25:37 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3080.970579

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 100*104*92 grid
  Fine grid: 200*208*184 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 200*208*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 55.27 MiB
    Density: 31.35 MiB
      Arrays: 30.89 MiB
      Localized functions: 0.46 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 22.99 MiB
      Arrays: 22.95 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.92 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.90 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 31
Number of bands in calculation: 31
Bands to converge: occupied states only
Number of valence electrons: 11

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .------------------------------------.  
          /|                                    |  
         / |                                    |  
        /  |                                    |  
       /   |                                    |  
      /    |                                    |  
     /     |                                    |  
    /      |                                    |  
   /       |                                    |  
  /        |                                    |  
 *         |                                    |  
 |         |                                    |  
 |         |                                    |  
 |         |            H C                     |  
 |         |             O                      |  
 |         |                                    |  
 |         |                                    |  
 |         .------------------------------------.  
 |        /                                    /   
 |       /                                    /    
 |      /                                    /     
 |     /                                    /      
 |    /                                    /       
 |   /                                    /        
 |  /                                    /         
 | /                                    /          
 |/                                    /           
 *------------------------------------*            

Positions:
   0 C      7.938395    8.190840    7.000000    ( 0.0000,  0.0000,  1.0000)
   1 O      7.938395    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    8.808669    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.938395    0.000000    0.000000   100     0.1494
  2. axis:    no     0.000000   15.808669    0.000000   104     0.1520
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  14.938395  15.808669  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1512

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  22:25:46  +0.86          -16.061847    0      1        +1.0000
iter:   2  22:25:47  +0.54          -16.093984    0      1        +1.0000
iter:   3  22:25:49  -0.81          -16.118199    0      1        +1.0000
iter:   4  22:25:51  -1.42          -16.119651    0      1        +1.0000
iter:   5  22:25:52  -2.06          -16.120269    0      1        +1.0000
iter:   6  22:25:54  -2.54          -16.120368    0      1        +1.0000
iter:   7  22:25:57  -2.85          -16.120382    0      1        +1.0000
iter:   8  22:25:59  -3.01          -16.120387    0      1        +1.0000
iter:   9  22:26:01  -5.18          -16.120389    0      1        +1.0000
iter:  10  22:26:02  -5.85          -16.120390    0      1        +1.0000
iter:  11  22:26:04  -6.84          -16.120390    0      1        +1.0000
iter:  12  22:26:06  -7.73          -16.120390    0      1        +1.0000
iter:  13  22:26:14  -7.07          -16.120390    0      1        +1.0000
iter:  14  22:26:16  -8.15          -16.120390    0      1        +1.0000
iter:  15  22:26:17  -9.49          -16.120390    0      1        +1.0000
iter:  16  22:26:21 -10.11          -16.120390    0      1        +1.0000

Converged after 16 iterations.

Dipole moment: (-0.173918, 0.215085, 0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999997)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.192518)
   1 O  ( 0.000000,  0.000000,  0.211040)
   2 H  ( 0.000000,  0.000000,  0.060912)

Energy contributions relative to reference atoms: (reference = -3080.970579)

Kinetic:        +14.355299
Potential:      -15.384483
External:        +0.000000
XC:             -15.170492
Entropy (-ST):   +0.000000
Local:           +0.079287
--------------------------
Free energy:    -16.120390
Extrapolated:   -16.120390

Spin contamination: 0.015061 electrons
Fermi levels: -3.43807, -5.96593

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -27.52201    1.00000    -27.16951    1.00000
    1    -14.98283    1.00000    -14.41479    1.00000
    2    -11.83993    1.00000    -11.34134    1.00000
    3    -10.45535    1.00000    -10.23144    1.00000
    4    -10.13703    1.00000     -9.12555    1.00000
    5     -4.56918    1.00000     -2.80631    0.00000
    6     -2.30697    0.00000     -1.83245    0.00000
    7      2.45244    0.00000      2.93546    0.00000
    8      6.17736    0.00000      6.51668    0.00000
    9      9.62618    0.00000      9.91275    0.00000
   10      9.93864    0.00000     10.94592    0.00000
   11     10.77016    0.00000     11.51498    0.00000
   12     14.63622    0.00000     14.97091    0.00000
   13     16.62668    0.00000     16.85371    0.00000
   14     17.85754    0.00000     18.30638    0.00000
   15     18.11961    0.00000     18.47397    0.00000
   16     20.27689    0.00000     20.89834    0.00000
   17     20.45959    0.00000     20.96270    0.00000
   18     23.41028    0.00000     24.29688    0.00000
   19     27.83569    0.00000     28.54503    0.00000
   20     31.01155    0.00000     31.66008    0.00000
   21     34.13035    0.00000     34.89044    0.00000
   22     34.35020    0.00000     34.91533    0.00000
   23     40.80989    0.00000     41.11799    0.00000
   24     44.80284    0.00000     45.45837    0.00000
   25     46.41590    0.00000     47.26788    0.00000
   26     48.69817    0.00000     49.26630    0.00000
   27     51.49229    0.00000     52.17551    0.00000
   28     59.73657    0.00000     59.95940    0.00000
   29     64.18204    0.00000     64.96928    0.00000
   30     67.44157    0.00000     67.75603    0.00000

Gap: 4.556 eV
Transition (v -> c):
  (s=1, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.005     0.005   0.0% |
LCAO WFS Initialize:                   1.647     0.054   0.1% |
 Hamiltonian:                          1.592     0.002   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.079     0.079   0.2% |
  Hartree integrate/restrict:          0.038     0.038   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.309     0.009   0.0% |
   Communicate bwd 0:                  0.059     0.059   0.1% |
   Communicate bwd 1:                  0.055     0.055   0.1% |
   Communicate fwd 0:                  0.058     0.058   0.1% |
   Communicate fwd 1:                  0.059     0.059   0.1% |
   fft:                                0.021     0.021   0.0% |
   fft2:                               0.048     0.048   0.1% |
  XC 3D grid:                          1.160     1.160   2.6% ||
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            39.887     0.005   0.0% |
 Direct Minimisation step:            38.215     0.003   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.187     0.006   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.068     0.068   0.2% |
   Potential matrix:                   0.106     0.106   0.2% |
   Residual:                           0.003     0.003   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.008     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.029     0.003   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.026     0.026   0.1% |
  Update Kohn-Sham energy:            37.982     0.000   0.0% |
   Density:                            1.441     0.000   0.0% |
    Atomic density matrices:           0.085     0.085   0.2% |
    Mix:                               1.217     1.217   2.8% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.007     0.007   0.0% |
    Pseudo density:                    0.129     0.022   0.1% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.104     0.104   0.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       36.541     0.039   0.1% |
    Atomic:                            0.007     0.007   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.009     0.009   0.0% |
    Communicate:                       1.811     1.811   4.1% |-|
    Hartree integrate/restrict:        0.856     0.856   2.0% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           7.161     0.191   0.4% |
     Communicate bwd 0:                1.360     1.360   3.1% ||
     Communicate bwd 1:                1.332     1.332   3.0% ||
     Communicate fwd 0:                1.280     1.280   2.9% ||
     Communicate fwd 1:                1.401     1.401   3.2% ||
     fft:                              0.488     0.488   1.1% |
     fft2:                             1.109     1.109   2.5% ||
    XC 3D grid:                       26.585    26.585  60.6% |-----------------------|
    vbar:                              0.070     0.070   0.2% |
 Get canonical representation:         1.667     0.000   0.0% |
  LCAO eigensolver:                    0.008     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Update Kohn-Sham energy:             1.659     0.000   0.0% |
   Density:                            0.062     0.000   0.0% |
    Atomic density matrices:           0.004     0.004   0.0% |
    Mix:                               0.052     0.052   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.006     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.005     0.005   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.597     0.002   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.079     0.079   0.2% |
    Hartree integrate/restrict:        0.036     0.036   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.314     0.009   0.0% |
     Communicate bwd 0:                0.060     0.060   0.1% |
     Communicate bwd 1:                0.059     0.059   0.1% |
     Communicate fwd 0:                0.056     0.056   0.1% |
     Communicate fwd 1:                0.062     0.062   0.1% |
     fft:                              0.021     0.021   0.0% |
     fft2:                             0.048     0.048   0.1% |
    XC 3D grid:                        1.163     1.163   2.7% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.503     0.503   1.1% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.799     1.799   4.1% |-|
-------------------------------------------------------------
Total:                                          43.843 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:26:21 2019
