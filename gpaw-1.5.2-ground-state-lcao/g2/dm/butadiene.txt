
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:44:02 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4185.453435

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 116*124*92 grid
  Fine grid: 232*248*184 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 232*248*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 57.16 MiB
    Density: 33.00 MiB
      Arrays: 31.85 MiB
      Localized functions: 1.15 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.93 MiB
      Arrays: 20.83 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 3.24 MiB
      C [qnM]: 0.05 MiB
      S, T [2 x qmm]: 0.10 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.08 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 82
Number of bands in calculation: 82
Bands to converge: occupied states only
Number of valence electrons: 22

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .-----------------------------------------.  
           /|                                         |  
          / |                                         |  
         /  |                                         |  
        /   |                                         |  
       /    |                                         |  
      /     |                                         |  
     /      |                                         |  
    /       |                                         |  
   /        |                                         |  
  /         |                                         |  
 *          |                                         |  
 |          |                                         |  
 |          |               H C  H                    |  
 |          |           H C  C H                      |  
 |          |         H  C H                          |  
 |          |                                         |  
 |          .-----------------------------------------.  
 |         /                                         /   
 |        /                                         /    
 |       /                                         /     
 |      /                                         /      
 |     /                                         /       
 |    /                                         /        
 |   /                                         /         
 |  /                                         /          
 | /                                         /           
 |/                                         /            
 *-----------------------------------------*             

Positions:
   0 C      9.159214   11.063993    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C      9.159214    9.721526    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 C      7.947792    8.913360    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 C      7.947792    7.570893    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H     10.081120   11.634886    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      8.232371   11.630559    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H     10.107006    9.183803    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000    9.451083    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      8.874635    7.004327    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.025886    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.107006    0.000000    0.000000   116     0.1475
  2. axis:    no     0.000000   18.634886    0.000000   124     0.1503
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  17.107006  18.634886  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1500

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:44:10  +0.17          -55.235813    0      1      
iter:   2  22:44:12  +0.31          -55.264220    0      1      
iter:   3  22:44:14  -0.52          -55.299010    0      1      
iter:   4  22:44:15  -1.29          -55.302076    0      1      
iter:   5  22:44:17  -3.50          -55.303022    0      1      
iter:   6  22:44:18  -4.38          -55.303030    0      1      
iter:   7  22:44:20  -5.75          -55.303032    0      1      
iter:   8  22:44:22  -6.22          -55.303032    0      1      
iter:   9  22:44:23  -6.49          -55.303032    0      1      
iter:  10  22:44:25  -7.31          -55.303032    0      1      
iter:  11  22:44:26  -7.61          -55.303032    0      1      
iter:  12  22:44:28  -9.74          -55.303032    0      1      
iter:  13  22:44:31 -10.74          -55.303032    0      1      

Converged after 13 iterations.

Dipole moment: (-0.000000, 0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4185.453435)

Kinetic:        +43.908009
Potential:      -49.908525
External:        +0.000000
XC:             -49.416872
Entropy (-ST):   +0.000000
Local:           +0.114357
--------------------------
Free energy:    -55.303032
Extrapolated:   -55.303032

Fermi levels: -3.65788, -3.65788

 Band  Eigenvalues  Occupancy
    0    -19.95074    2.00000
    1    -18.10268    2.00000
    2    -14.86906    2.00000
    3    -13.53185    2.00000
    4    -11.33535    2.00000
    5    -11.24792    2.00000
    6     -9.42029    2.00000
    7     -9.26810    2.00000
    8     -8.26011    2.00000
    9     -7.88989    2.00000
   10     -5.66854    2.00000
   11     -1.64721    0.00000
   12      1.03469    0.00000
   13      2.18322    0.00000
   14      2.52307    0.00000
   15      2.64749    0.00000
   16      3.86550    0.00000
   17      4.34829    0.00000
   18      4.52433    0.00000
   19      7.33238    0.00000
   20      8.50272    0.00000
   21      8.66340    0.00000
   22      8.94324    0.00000
   23      9.79145    0.00000
   24     10.05778    0.00000
   25     10.82770    0.00000
   26     10.84565    0.00000
   27     10.93172    0.00000
   28     11.80908    0.00000
   29     12.25960    0.00000
   30     12.37611    0.00000
   31     12.90025    0.00000
   32     15.31889    0.00000
   33     16.06703    0.00000
   34     16.46603    0.00000
   35     18.27579    0.00000
   36     18.29606    0.00000
   37     19.46967    0.00000
   38     19.61566    0.00000
   39     20.28815    0.00000
   40     20.73167    0.00000
   41     21.90390    0.00000
   42     22.45305    0.00000
   43     22.78692    0.00000
   44     23.46084    0.00000
   45     24.91939    0.00000
   46     25.69730    0.00000
   47     27.31022    0.00000
   48     30.53895    0.00000
   49     30.66444    0.00000
   50     31.10102    0.00000
   51     31.10919    0.00000
   52     31.99306    0.00000
   53     32.33358    0.00000
   54     32.62654    0.00000
   55     32.80558    0.00000
   56     33.53365    0.00000
   57     35.34625    0.00000
   58     35.54620    0.00000
   59     35.75283    0.00000
   60     36.04758    0.00000
   61     36.52992    0.00000
   62     40.32696    0.00000
   63     40.41505    0.00000
   64     41.66677    0.00000
   65     41.71198    0.00000
   66     42.83434    0.00000
   67     43.73754    0.00000
   68     47.36597    0.00000
   69     47.62026    0.00000
   70     48.70389    0.00000
   71     49.13563    0.00000
   72     49.41382    0.00000
   73     49.41409    0.00000
   74     50.00742    0.00000
   75     50.42374    0.00000
   76     55.77159    0.00000
   77     56.29274    0.00000
   78     65.41376    0.00000
   79     66.91370    0.00000
   80     71.44629    0.00000
   81     76.84468    0.00000

Gap: 4.021 eV
Transition (v -> c):
  (s=0, k=0, n=10, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=11, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.016     0.016   0.1% |
LCAO WFS Initialize:                   1.564     0.044   0.1% |
 Hamiltonian:                          1.520     0.000   0.0% |
  Atomic:                              0.046     0.001   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.003     0.003   0.0% |
  Communicate:                         0.008     0.008   0.0% |
  Hartree integrate/restrict:          0.028     0.028   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.463     0.011   0.0% |
   Communicate bwd 0:                  0.082     0.082   0.3% |
   Communicate bwd 1:                  0.080     0.080   0.3% |
   Communicate fwd 0:                  0.077     0.077   0.3% |
   Communicate fwd 1:                  0.088     0.088   0.3% |
   fft:                                0.041     0.041   0.1% |
   fft2:                               0.083     0.083   0.3% |
  XC 3D grid:                          0.963     0.963   3.3% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            25.660     0.003   0.0% |
 Direct Minimisation step:            24.064     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.355     0.004   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.045     0.045   0.2% |
   Potential matrix:                   0.302     0.302   1.0% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.025     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.020     0.020   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.018     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.016     0.016   0.1% |
  Update Kohn-Sham energy:            23.660     0.000   0.0% |
   Density:                            1.007     0.000   0.0% |
    Atomic density matrices:           0.073     0.073   0.2% |
    Mix:                               0.617     0.617   2.1% ||
    Multipole moments:                 0.010     0.010   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.303     0.011   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.291     0.291   1.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       22.653     0.002   0.0% |
    Atomic:                            0.700     0.008   0.0% |
     XC Correction:                    0.692     0.692   2.3% ||
    Calculate atomic Hamiltonians:     0.037     0.037   0.1% |
    Communicate:                       0.129     0.129   0.4% |
    Hartree integrate/restrict:        0.408     0.408   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.957     0.183   0.6% |
     Communicate bwd 0:                1.230     1.230   4.2% |-|
     Communicate bwd 1:                1.226     1.226   4.1% |-|
     Communicate fwd 0:                1.155     1.155   3.9% |-|
     Communicate fwd 1:                1.287     1.287   4.3% |-|
     fft:                              0.626     0.626   2.1% ||
     fft2:                             1.250     1.250   4.2% |-|
    XC 3D grid:                       14.358    14.358  48.5% |------------------|
    vbar:                              0.061     0.061   0.2% |
 Get canonical representation:         1.593     0.000   0.0% |
  LCAO eigensolver:                    0.025     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.020     0.020   0.1% |
  Update Kohn-Sham energy:             1.568     0.000   0.0% |
   Density:                            0.066     0.000   0.0% |
    Atomic density matrices:           0.006     0.006   0.0% |
    Mix:                               0.040     0.040   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.020     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.019     0.019   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.502     0.000   0.0% |
    Atomic:                            0.047     0.001   0.0% |
     XC Correction:                    0.046     0.046   0.2% |
    Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
    Communicate:                       0.007     0.007   0.0% |
    Hartree integrate/restrict:        0.028     0.028   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.460     0.011   0.0% |
     Communicate bwd 0:                0.081     0.081   0.3% |
     Communicate bwd 1:                0.080     0.080   0.3% |
     Communicate fwd 0:                0.077     0.077   0.3% |
     Communicate fwd 1:                0.087     0.087   0.3% |
     fft:                              0.041     0.041   0.1% |
     fft2:                             0.083     0.083   0.3% |
    XC 3D grid:                        0.953     0.953   3.2% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.003     0.003   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.201     0.201   0.7% |
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.159     2.159   7.3% |--|
-------------------------------------------------------------
Total:                                          29.606 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:44:31 2019
