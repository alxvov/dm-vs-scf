
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:23:45 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Na-setup:
  name: Sodium
  id: d7ecbc49209718622bcbe287195dca2a
  Z: 11
  valence: 7
  core: 4
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Na.PBE.gz
  cutoffs: 1.18(comp), 2.17(filt), 2.59(core), lmax=2
  valence states:
                energy  radius
    3s(1.00)    -2.744   1.201
    2p(6.00)   -28.672   1.217
    3p(0.00)    -0.743   1.217
    *s          24.468   1.201
    *d           0.000   1.238

  LCAO basis set for Na:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Na.dzp.basis.gz
    Number of radial functions: 7
    Number of spherical harmonics: 17
      l=0, rc=11.4062 Bohr: 3s-sz confined orbital
      l=1, rc=3.6719 Bohr: 2p-sz confined orbital
      l=1, rc=15.9375 Bohr: 3p-sz confined orbital
      l=0, rc=6.9531 Bohr: 3s-dz split-valence wave
      l=1, rc=2.4219 Bohr: 2p-dz split-valence wave
      l=1, rc=10.0469 Bohr: 3p-dz split-valence wave
      l=1, rc=11.4062 Bohr: p-type Gaussian polarization

Reference energy: -8837.220995

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*116 grid
  Fine grid: 184*184*232 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 49.27 MiB
    Density: 26.15 MiB
      Arrays: 23.56 MiB
      Localized functions: 2.59 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 15.60 MiB
      Arrays: 15.41 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.19 MiB
    Wavefunctions: 7.51 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 7.49 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 34
Number of bands in calculation: 34
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Na                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Na                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Na     7.000000    7.000000   10.152524    ( 0.0000,  0.0000,  0.0000)
   1 Na     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   17.152524   116     0.1479

  Lengths:  14.000000  14.000000  17.152524
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:23:51  -0.65           -1.057432    0      1      
iter:   2  23:23:52  -1.98           -1.063677    0      1      
iter:   3  23:23:53  -2.22           -1.063801    0      1      
iter:   4  23:23:55  -4.16           -1.063859    0      1      
iter:   5  23:23:56  -5.23           -1.063861    0      1      
iter:   6  23:23:57  -5.87           -1.063862    0      1      
iter:   7  23:23:58  -7.43           -1.063862    0      1      
iter:   8  23:24:00  -8.83           -1.063862    0      1      
iter:   9  23:24:01  -9.59           -1.063862    0      1      
iter:  10  23:24:03 -11.68           -1.063862    0      1      

Converged after 10 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -8837.220995)

Kinetic:         +3.676672
Potential:       -3.256638
External:        +0.000000
XC:              -1.488582
Entropy (-ST):   +0.000000
Local:           +0.004686
--------------------------
Free energy:     -1.063862
Extrapolated:    -1.063862

Fermi levels: -2.29784, -2.29784

 Band  Eigenvalues  Occupancy
    0    -27.82684    2.00000
    1    -27.80684    2.00000
    2    -27.80684    2.00000
    3    -27.80175    2.00000
    4    -27.80175    2.00000
    5    -27.78159    2.00000
    6     -2.91091    2.00000
    7     -1.68477    0.00000
    8     -0.74634    0.00000
    9     -0.74634    0.00000
   10     -0.25181    0.00000
   11      0.36564    0.00000
   12      0.36564    0.00000
   13      1.62878    0.00000
   14      2.36038    0.00000
   15      2.41954    0.00000
   16      2.41954    0.00000
   17      3.17118    0.00000
   18      3.17118    0.00000
   19      3.24645    0.00000
   20      4.96645    0.00000
   21      6.64605    0.00000
   22      9.83497    0.00000
   23     10.98716    0.00000
   24     10.98716    0.00000
   25     11.02496    0.00000
   26     11.02496    0.00000
   27     12.00733    0.00000
   28     46.05853    0.00000
   29     46.16236    0.00000
   30     46.16236    0.00000
   31     46.70806    0.00000
   32     46.70806    0.00000
   33     47.34302    0.00000

Gap: 1.226 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.022     0.022   0.1% |
LCAO WFS Initialize:                   1.192     0.032   0.2% |
 Hamiltonian:                          1.160     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.065     0.065   0.4% |
  Hartree integrate/restrict:          0.021     0.021   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.332     0.005   0.0% |
   Communicate bwd 0:                  0.063     0.063   0.3% |
   Communicate bwd 1:                  0.060     0.060   0.3% |
   Communicate fwd 0:                  0.057     0.057   0.3% |
   Communicate fwd 1:                  0.061     0.061   0.3% |
   fft:                                0.028     0.028   0.2% |
   fft2:                               0.057     0.057   0.3% |
  XC 3D grid:                          0.737     0.737   4.1% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            14.971     0.002   0.0% |
 Direct Minimisation step:            13.754     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.188     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.018     0.018   0.1% |
   Potential matrix:                   0.166     0.166   0.9% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.017     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.015     0.015   0.1% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.008     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.007     0.007   0.0% |
  Update Kohn-Sham energy:            13.537     0.000   0.0% |
   Density:                            0.534     0.000   0.0% |
    Atomic density matrices:           0.027     0.027   0.1% |
    Mix:                               0.322     0.322   1.8% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.182     0.006   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.175     0.175   1.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       13.003     0.001   0.0% |
    Atomic:                            0.003     0.003   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.009     0.009   0.1% |
    Communicate:                       0.724     0.724   4.0% |-|
    Hartree integrate/restrict:        0.375     0.375   2.1% ||
    New Kinetic Energy:                0.001     0.000   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.821     0.054   0.3% |
     Communicate bwd 0:                0.702     0.702   3.9% |-|
     Communicate bwd 1:                0.747     0.747   4.1% |-|
     Communicate fwd 0:                0.623     0.623   3.4% ||
     Communicate fwd 1:                0.743     0.743   4.1% |-|
     fft:                              0.316     0.316   1.7% ||
     fft2:                             0.637     0.637   3.5% ||
    XC 3D grid:                        8.034     8.034  44.4% |-----------------|
    vbar:                              0.034     0.034   0.2% |
 Get canonical representation:         1.215     0.000   0.0% |
  LCAO eigensolver:                    0.017     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.015     0.015   0.1% |
  Update Kohn-Sham energy:             1.198     0.000   0.0% |
   Density:                            0.048     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.029     0.029   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.017     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.016     0.016   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.149     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.066     0.066   0.4% |
    Hartree integrate/restrict:        0.021     0.021   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.331     0.005   0.0% |
     Communicate bwd 0:                0.063     0.063   0.3% |
     Communicate bwd 1:                0.060     0.060   0.3% |
     Communicate fwd 0:                0.057     0.057   0.3% |
     Communicate fwd 1:                0.062     0.062   0.3% |
     fft:                              0.028     0.028   0.2% |
     fft2:                             0.057     0.057   0.3% |
    XC 3D grid:                        0.727     0.727   4.0% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.153     0.153   0.8% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.749     1.749   9.7% |---|
-------------------------------------------------------------
Total:                                          18.090 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 23:24:03 2019
