
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:11:38 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -1052.608440

Spin-polarized calculation.
Magnetic moment: 2.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*108*96 grid
  Fine grid: 184*216*192 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 54.90 MiB
    Density: 31.12 MiB
      Arrays: 30.79 MiB
      Localized functions: 0.32 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 22.91 MiB
      Arrays: 22.88 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.88 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.87 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 23
Bands to converge: occupied states only
Number of valence electrons: 6

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           CH                    |  
 |         |          H                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 C      7.000000    7.982622    7.441523    ( 0.0000,  0.0000,  2.0000)
   1 H      7.000000    8.965244    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   15.965244    0.000000   108     0.1478
  3. axis:    no     0.000000    0.000000   14.441523    96     0.1504

  Lengths:  14.000000  15.965244  14.441523
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1501

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  23:11:46  +0.96          -11.441286    0      1        +2.0000
iter:   2  23:11:48  +0.24          -11.458877    0      1        +2.0000
iter:   3  23:11:49  -1.52          -11.461784    0      1        +2.0000
iter:   4  23:11:51  -2.41          -11.461870    0      1        +2.0000
iter:   5  23:11:53  -3.70          -11.461883    0      1        +2.0000
iter:   6  23:11:54  -4.88          -11.461884    0      1        +2.0000
iter:   7  23:11:56  -5.58          -11.461884    0      1        +2.0000
iter:   8  23:11:58  -6.16          -11.461884    0      1        +2.0000
iter:   9  23:11:59  -7.39          -11.461884    0      1        +2.0000
iter:  10  23:12:01  -8.72          -11.461884    0      1        +2.0000
iter:  11  23:12:04 -11.08          -11.461884    0      1        +2.0000

Converged after 11 iterations.

Dipole moment: (0.000000, 0.000000, -0.101713) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 2.000002)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.637734)
   1 H  ( 0.000000,  0.000000,  0.000791)
   2 H  ( 0.000000,  0.000000,  0.000791)

Energy contributions relative to reference atoms: (reference = -1052.608440)

Kinetic:        +11.164606
Potential:      -10.493848
External:        +0.000000
XC:             -12.158833
Entropy (-ST):   +0.000000
Local:           +0.026192
--------------------------
Free energy:    -11.461884
Extrapolated:   -11.461884

Spin contamination: 0.019767 electrons
Fermi levels: -1.60734, -6.61491

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -16.56714    1.00000    -14.77255    1.00000
    1    -10.93902    1.00000    -10.33002    1.00000
    2     -6.44370    1.00000     -2.89980    0.00000
    3     -5.43141    1.00000     -2.12686    0.00000
    4      2.21673    0.00000      2.91165    0.00000
    5      4.10163    0.00000      4.53845    0.00000
    6      8.68259    0.00000      9.49425    0.00000
    7      9.92935    0.00000     11.57434    0.00000
    8     10.17418    0.00000     12.05657    0.00000
    9     10.71175    0.00000     12.29513    0.00000
   10     16.11594    0.00000     16.60445    0.00000
   11     18.32617    0.00000     19.23408    0.00000
   12     20.42734    0.00000     21.88187    0.00000
   13     22.56995    0.00000     24.97091    0.00000
   14     22.64636    0.00000     25.25611    0.00000
   15     26.31914    0.00000     26.71907    0.00000
   16     31.36811    0.00000     33.42816    0.00000
   17     32.97567    0.00000     34.70077    0.00000
   18     38.36628    0.00000     39.98818    0.00000
   19     39.51514    0.00000     41.29620    0.00000
   20     41.11607    0.00000     42.17557    0.00000
   21     50.46931    0.00000     51.20284    0.00000
   22     56.67888    0.00000     57.96181    0.00000

Gap: 2.532 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=2, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.004     0.004   0.0% |
LCAO WFS Initialize:                   1.638     0.054   0.2% |
 Hamiltonian:                          1.583     0.001   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.078     0.078   0.3% |
  Hartree integrate/restrict:          0.038     0.038   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.303     0.008   0.0% |
   Communicate bwd 0:                  0.059     0.059   0.2% |
   Communicate bwd 1:                  0.055     0.055   0.2% |
   Communicate fwd 0:                  0.058     0.058   0.2% |
   Communicate fwd 1:                  0.058     0.058   0.2% |
   fft:                                0.017     0.017   0.1% |
   fft2:                               0.048     0.048   0.2% |
  XC 3D grid:                          1.159     1.159   4.4% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            23.126     0.003   0.0% |
 Direct Minimisation step:            21.470     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.081     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.017     0.017   0.1% |
   Potential matrix:                   0.058     0.058   0.2% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.006     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.004     0.004   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.015     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.014     0.014   0.1% |
  Update Kohn-Sham energy:            21.362     0.000   0.0% |
   Density:                            0.781     0.000   0.0% |
    Atomic density matrices:           0.028     0.028   0.1% |
    Mix:                               0.674     0.674   2.5% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.073     0.011   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.061     0.061   0.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       20.581     0.021   0.1% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.004     0.004   0.0% |
    Communicate:                       1.012     1.012   3.8% |-|
    Hartree integrate/restrict:        0.496     0.496   1.9% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.947     0.103   0.4% |
     Communicate bwd 0:                0.769     0.769   2.9% ||
     Communicate bwd 1:                0.745     0.745   2.8% ||
     Communicate fwd 0:                0.718     0.718   2.7% ||
     Communicate fwd 1:                0.768     0.768   2.9% ||
     fft:                              0.226     0.226   0.8% |
     fft2:                             0.619     0.619   2.3% ||
    XC 3D grid:                       15.055    15.055  56.5% |----------------------|
    vbar:                              0.040     0.040   0.1% |
 Get canonical representation:         1.654     0.000   0.0% |
  LCAO eigensolver:                    0.006     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.004     0.004   0.0% |
  Update Kohn-Sham energy:             1.647     0.000   0.0% |
   Density:                            0.061     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.053     0.053   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.006     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.005     0.005   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.586     0.002   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.078     0.078   0.3% |
    Hartree integrate/restrict:        0.040     0.040   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.306     0.009   0.0% |
     Communicate bwd 0:                0.059     0.059   0.2% |
     Communicate bwd 1:                0.056     0.056   0.2% |
     Communicate fwd 0:                0.056     0.056   0.2% |
     Communicate fwd 1:                0.062     0.062   0.2% |
     fft:                              0.017     0.017   0.1% |
     fft2:                             0.047     0.047   0.2% |
    XC 3D grid:                        1.157     1.157   4.3% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.180     0.180   0.7% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.678     1.678   6.3% |--|
-------------------------------------------------------------
Total:                                          26.630 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 23:12:04 2019
