
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:36:07 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3157.825320

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*108*104 grid
  Fine grid: 224*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 53.84 MiB
    Density: 31.24 MiB
      Arrays: 30.27 MiB
      Localized functions: 0.98 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.88 MiB
      Arrays: 19.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.72 MiB
      C [qnM]: 0.04 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.61 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 9
Number of atomic orbitals: 69
Number of bands in calculation: 69
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |               H                        |  
 |         |           H   C H                      |  
 |         |            C  HC                       |  
 |         |           H     H                      |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      8.260727    8.594879    7.910526    ( 0.0000,  0.0000,  0.0000)
   1 C      9.011569    7.294382    7.910526    ( 0.0000,  0.0000,  0.0000)
   2 C      7.509885    7.294382    7.910526    ( 0.0000,  0.0000,  0.0000)
   3 H      8.260727    9.183643    8.821052    ( 0.0000,  0.0000,  0.0000)
   4 H      8.260727    9.183643    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      9.521454    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      9.521454    7.000000    8.821052    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000    7.000000    8.821052    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.521454    0.000000    0.000000   112     0.1475
  2. axis:    no     0.000000   16.183643    0.000000   108     0.1498
  3. axis:    no     0.000000    0.000000   15.821052   104     0.1521

  Lengths:  16.521454  16.183643  15.821052
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1498

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:36:14  +0.73          -47.068275    0      1      
iter:   2  22:36:16  +0.57          -47.122774    0      1      
iter:   3  22:36:17  -2.34          -47.157274    0      1      
iter:   4  22:36:19  -3.72          -47.157401    0      1      
iter:   5  22:36:20  -4.79          -47.157408    0      1      
iter:   6  22:36:21  -5.58          -47.157409    0      1      
iter:   7  22:36:23  -5.95          -47.157409    0      1      
iter:   8  22:36:24  -6.92          -47.157409    0      1      
iter:   9  22:36:25  -9.08          -47.157409    0      1      
iter:  10  22:36:28 -10.17          -47.157409    0      1      

Converged after 10 iterations.

Dipole moment: (-0.000000, 0.000005, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3157.825320)

Kinetic:        +37.936200
Potential:      -41.762586
External:        +0.000000
XC:             -43.411063
Entropy (-ST):   +0.000000
Local:           +0.080040
--------------------------
Free energy:    -47.157409
Extrapolated:   -47.157409

Fermi levels: -2.47966, -2.47966

 Band  Eigenvalues  Occupancy
    0    -21.21472    2.00000
    1    -14.58050    2.00000
    2    -14.58018    2.00000
    3    -12.05429    2.00000
    4    -11.37567    2.00000
    5     -8.61709    2.00000
    6     -8.61670    2.00000
    7     -7.00424    2.00000
    8     -7.00415    2.00000
    9      2.04484    0.00000
   10      2.07095    0.00000
   11      3.09564    0.00000
   12      3.21654    0.00000
   13      3.21754    0.00000
   14      5.25813    0.00000
   15      5.25872    0.00000
   16      6.45542    0.00000
   17      6.45670    0.00000
   18      7.64166    0.00000
   19      8.45574    0.00000
   20     10.33835    0.00000
   21     10.33897    0.00000
   22     11.57079    0.00000
   23     12.99854    0.00000
   24     12.99863    0.00000
   25     13.04480    0.00000
   26     13.04568    0.00000
   27     14.53119    0.00000
   28     15.80356    0.00000
   29     16.77515    0.00000
   30     16.78047    0.00000
   31     16.94583    0.00000
   32     16.94627    0.00000
   33     19.87619    0.00000
   34     22.78938    0.00000
   35     25.21302    0.00000
   36     25.21321    0.00000
   37     26.72133    0.00000
   38     26.89815    0.00000
   39     26.89987    0.00000
   40     31.82020    0.00000
   41     31.82161    0.00000
   42     32.40386    0.00000
   43     33.81559    0.00000
   44     33.81713    0.00000
   45     33.86517    0.00000
   46     33.86592    0.00000
   47     34.07407    0.00000
   48     35.46652    0.00000
   49     37.36503    0.00000
   50     37.36512    0.00000
   51     37.96198    0.00000
   52     40.04813    0.00000
   53     40.04832    0.00000
   54     43.51118    0.00000
   55     43.51151    0.00000
   56     44.33191    0.00000
   57     44.33292    0.00000
   58     46.27250    0.00000
   59     46.95057    0.00000
   60     49.20118    0.00000
   61     49.26994    0.00000
   62     49.27181    0.00000
   63     51.71723    0.00000
   64     52.30160    0.00000
   65     59.98363    0.00000
   66     59.98474    0.00000
   67     62.97078    0.00000
   68     62.97271    0.00000

Gap: 9.049 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.014     0.014   0.1% |
LCAO WFS Initialize:                   1.322     0.040   0.2% |
 Hamiltonian:                          1.282     0.000   0.0% |
  Atomic:                              0.054     0.008   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.000     0.000   0.0% |
  Hartree integrate/restrict:          0.024     0.024   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.283     0.015   0.1% |
   Communicate bwd 0:                  0.018     0.018   0.1% |
   Communicate bwd 1:                  0.076     0.076   0.4% |
   Communicate fwd 0:                  0.072     0.072   0.3% |
   Communicate fwd 1:                  0.023     0.023   0.1% |
   fft:                                0.029     0.029   0.1% |
   fft2:                               0.050     0.050   0.2% |
  XC 3D grid:                          0.909     0.909   4.3% |-|
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            17.693     0.002   0.0% |
 Direct Minimisation step:            16.330     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.257     0.003   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.026     0.026   0.1% |
   Potential matrix:                   0.224     0.224   1.1% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.022     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.018     0.018   0.1% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.012     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.010     0.010   0.0% |
  Update Kohn-Sham energy:            16.035     0.000   0.0% |
   Density:                            0.730     0.000   0.0% |
    Atomic density matrices:           0.043     0.043   0.2% |
    Mix:                               0.460     0.460   2.2% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.222     0.008   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.213     0.213   1.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       15.305     0.001   0.0% |
    Atomic:                            0.650     0.095   0.4% |
     XC Correction:                    0.555     0.555   2.6% ||
    Calculate atomic Hamiltonians:     0.029     0.029   0.1% |
    Communicate:                       0.001     0.001   0.0% |
    Hartree integrate/restrict:        0.287     0.287   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.409     0.193   0.9% |
     Communicate bwd 0:                0.230     0.230   1.1% |
     Communicate bwd 1:                0.932     0.932   4.4% |-|
     Communicate fwd 0:                0.857     0.857   4.0% |-|
     Communicate fwd 1:                0.265     0.265   1.2% |
     fft:                              0.340     0.340   1.6% ||
     fft2:                             0.592     0.592   2.8% ||
    XC 3D grid:                       10.880    10.880  51.2% |-------------------|
    vbar:                              0.046     0.046   0.2% |
 Get canonical representation:         1.361     0.000   0.0% |
  LCAO eigensolver:                    0.022     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.018     0.018   0.1% |
  Update Kohn-Sham energy:             1.339     0.000   0.0% |
   Density:                            0.061     0.000   0.0% |
    Atomic density matrices:           0.004     0.004   0.0% |
    Mix:                               0.039     0.039   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.018     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.017     0.017   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.277     0.000   0.0% |
    Atomic:                            0.054     0.008   0.0% |
     XC Correction:                    0.047     0.047   0.2% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.000     0.000   0.0% |
    Hartree integrate/restrict:        0.025     0.025   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.285     0.016   0.1% |
     Communicate bwd 0:                0.018     0.018   0.1% |
     Communicate bwd 1:                0.077     0.077   0.4% |
     Communicate fwd 0:                0.072     0.072   0.3% |
     Communicate fwd 1:                0.023     0.023   0.1% |
     fft:                              0.028     0.028   0.1% |
     fft2:                             0.050     0.050   0.2% |
    XC 3D grid:                        0.907     0.907   4.3% |-|
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.180     0.180   0.8% |
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.013     2.013   9.5% |---|
-------------------------------------------------------------
Total:                                          21.229 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:36:28 2019
