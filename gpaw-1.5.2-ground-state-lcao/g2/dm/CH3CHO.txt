
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:13:37 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4146.069181

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*112*104 grid
  Fine grid: 224*224*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 260.56 MiB
  Calculator: 54.86 MiB
    Density: 32.25 MiB
      Arrays: 31.40 MiB
      Localized functions: 0.85 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.61 MiB
      Arrays: 20.54 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 2.01 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.93 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 59
Number of bands in calculation: 59
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .----------------------------------------.  
           /|                                        |  
          / |                                        |  
         /  |                                        |  
        /   |                                        |  
       /    |                                        |  
      /     |                                        |  
     /      |                                        |  
    /       |                                        |  
   /        |                                        |  
  /         |                                        |  
 *          |                                        |  
 |          |                                        |  
 |          |                                        |  
 |          |          H   H                         |  
 |          |            C  C  O                     |  
 |          |          H  H                          |  
 |          |                                        |  
 |          |                                        |  
 |          .----------------------------------------.  
 |         /                                        /   
 |        /                                        /    
 |       /                                        /     
 |      /                                        /      
 |     /                                        /       
 |    /                                        /        
 |   /                                        /         
 |  /                                        /          
 | /                                        /           
 |/                                        /            
 *----------------------------------------*             

Positions:
   0 O      9.814376    8.995476    7.880946    ( 0.0000,  0.0000,  0.0000)
   1 C      8.596321    9.098369    7.880946    ( 0.0000,  0.0000,  0.0000)
   2 H      8.119080   10.099531    7.880946    ( 0.0000,  0.0000,  0.0000)
   3 C      7.648219    7.934098    7.880946    ( 0.0000,  0.0000,  0.0000)
   4 H      8.210375    7.000000    7.880946    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.981761    8.761892    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.981761    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.814376    0.000000    0.000000   112     0.1501
  2. axis:    no     0.000000   17.099531    0.000000   112     0.1527
  3. axis:    no     0.000000    0.000000   15.761892   104     0.1516

  Lengths:  16.814376  17.099531  15.761892
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1514

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:13:46  +1.07          -37.589647    0      1      
iter:   2  22:13:49  -0.07          -37.723765    0      1      
iter:   3  22:13:51  -1.26          -37.742739    0      1      
iter:   4  22:13:52  -1.96          -37.744399    0      1      
iter:   5  22:13:54  -2.39          -37.744629    0      1      
iter:   6  22:13:55  -2.96          -37.744673    0      1      
iter:   7  22:13:57  -3.33          -37.744684    0      1      
iter:   8  22:13:58  -4.63          -37.744689    0      1      
iter:   9  22:14:00  -5.33          -37.744689    0      1      
iter:  10  22:14:01  -6.35          -37.744689    0      1      
iter:  11  22:14:03  -6.86          -37.744689    0      1      
iter:  12  22:14:04  -6.84          -37.744689    0      1      
iter:  13  22:14:06  -8.14          -37.744689    0      1      
iter:  14  22:14:07  -9.09          -37.744689    0      1      
iter:  15  22:14:10 -10.12          -37.744689    0      1      

Converged after 15 iterations.

Dipole moment: (-0.504266, -0.060489, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4146.069181)

Kinetic:        +30.871970
Potential:      -34.452981
External:        +0.000000
XC:             -34.277395
Entropy (-ST):   +0.000000
Local:           +0.113716
--------------------------
Free energy:    -37.744689
Extrapolated:   -37.744689

Fermi levels: -3.67704, -3.67704

 Band  Eigenvalues  Occupancy
    0    -25.93066    2.00000
    1    -18.68688    2.00000
    2    -14.21463    2.00000
    3    -11.53437    2.00000
    4    -10.88451    2.00000
    5    -10.33667    2.00000
    6     -9.49900    2.00000
    7     -8.77925    2.00000
    8     -5.59634    2.00000
    9     -1.75773    0.00000
   10      2.00738    0.00000
   11      2.73160    0.00000
   12      3.48235    0.00000
   13      3.61910    0.00000
   14      5.82833    0.00000
   15      6.49698    0.00000
   16      9.11608    0.00000
   17      9.52714    0.00000
   18     10.47799    0.00000
   19     10.48695    0.00000
   20     12.17359    0.00000
   21     14.03286    0.00000
   22     14.33355    0.00000
   23     14.91407    0.00000
   24     15.29643    0.00000
   25     16.02330    0.00000
   26     17.39956    0.00000
   27     17.85891    0.00000
   28     19.02436    0.00000
   29     20.38247    0.00000
   30     22.35992    0.00000
   31     24.47686    0.00000
   32     25.57522    0.00000
   33     27.05004    0.00000
   34     27.43445    0.00000
   35     29.93617    0.00000
   36     30.42146    0.00000
   37     33.34395    0.00000
   38     33.92345    0.00000
   39     35.30166    0.00000
   40     35.80046    0.00000
   41     35.94016    0.00000
   42     37.16096    0.00000
   43     38.98404    0.00000
   44     39.73444    0.00000
   45     42.44319    0.00000
   46     43.81733    0.00000
   47     45.39971    0.00000
   48     47.39337    0.00000
   49     48.12537    0.00000
   50     49.59115    0.00000
   51     49.62702    0.00000
   52     50.79211    0.00000
   53     54.89403    0.00000
   54     55.17567    0.00000
   55     60.85170    0.00000
   56     61.49722    0.00000
   57     68.24306    0.00000
   58     70.30888    0.00000

Gap: 3.839 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.010     0.010   0.0% |
LCAO WFS Initialize:                   1.489     0.041   0.1% |
 Hamiltonian:                          1.447     0.000   0.0% |
  Atomic:                              0.029     0.029   0.1% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.020     0.020   0.1% |
  Hartree integrate/restrict:          0.029     0.029   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.419     0.012   0.0% |
   Communicate bwd 0:                  0.081     0.081   0.2% |
   Communicate bwd 1:                  0.080     0.080   0.2% |
   Communicate fwd 0:                  0.074     0.074   0.2% |
   Communicate fwd 1:                  0.083     0.083   0.3% |
   fft:                                0.026     0.026   0.1% |
   fft2:                               0.062     0.062   0.2% |
  XC 3D grid:                          0.945     0.945   2.9% ||
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            28.675     0.004   0.0% |
 Direct Minimisation step:            27.164     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.221     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.009     0.009   0.0% |
   Potential matrix:                   0.203     0.203   0.6% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.013     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.000     0.000   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.011     0.011   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.017     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.015     0.015   0.0% |
  Update Kohn-Sham energy:            26.906     0.000   0.0% |
   Density:                            0.934     0.000   0.0% |
    Atomic density matrices:           0.028     0.028   0.1% |
    Mix:                               0.691     0.691   2.1% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.209     0.012   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.195     0.195   0.6% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       25.972     0.002   0.0% |
    Atomic:                            0.498     0.498   1.5% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.041     0.041   0.1% |
    Communicate:                       0.367     0.367   1.1% |
    Hartree integrate/restrict:        0.508     0.508   1.5% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           7.594     0.210   0.6% |
     Communicate bwd 0:                1.460     1.460   4.4% |-|
     Communicate bwd 1:                1.417     1.417   4.3% |-|
     Communicate fwd 0:                1.407     1.407   4.3% |-|
     Communicate fwd 1:                1.498     1.498   4.6% |-|
     fft:                              0.485     0.485   1.5% ||
     fft2:                             1.118     1.118   3.4% ||
    XC 3D grid:                       16.886    16.886  51.3% |--------------------|
    vbar:                              0.074     0.074   0.2% |
 Get canonical representation:         1.507     0.000   0.0% |
  LCAO eigensolver:                    0.013     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.011     0.011   0.0% |
  Update Kohn-Sham energy:             1.494     0.000   0.0% |
   Density:                            0.052     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.038     0.038   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.011     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.011     0.011   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.442     0.000   0.0% |
    Atomic:                            0.028     0.028   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.020     0.020   0.1% |
    Hartree integrate/restrict:        0.029     0.029   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.419     0.011   0.0% |
     Communicate bwd 0:                0.080     0.080   0.2% |
     Communicate bwd 1:                0.078     0.078   0.2% |
     Communicate fwd 0:                0.078     0.078   0.2% |
     Communicate fwd 1:                0.083     0.083   0.3% |
     fft:                              0.027     0.027   0.1% |
     fft2:                             0.062     0.062   0.2% |
    XC 3D grid:                        0.939     0.939   2.9% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.517     0.517   1.6% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.205     2.205   6.7% |--|
-------------------------------------------------------------
Total:                                          32.902 100.0%

Memory usage: 263.32 MiB
Date: Sat Feb 16 22:14:10 2019
