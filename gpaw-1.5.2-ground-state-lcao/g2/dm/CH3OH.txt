
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:53:59 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3118.441066

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*108*104 grid
  Fine grid: 216*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 50.58 MiB
    Density: 29.88 MiB
      Arrays: 29.18 MiB
      Localized functions: 0.70 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.14 MiB
      Arrays: 19.09 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.56 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.03 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.51 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 46
Number of bands in calculation: 46
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |               H                      |  
 |         |            H C                       |  
 |         |             O H                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      8.045864    8.712847    7.891772    ( 0.0000,  0.0000,  0.0000)
   1 O      8.045864    7.289907    7.891772    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    9.018243    7.891772    ( 0.0000,  0.0000,  0.0000)
   3 H      8.971529    7.000000    7.891772    ( 0.0000,  0.0000,  0.0000)
   4 H      8.530140    9.128834    8.783544    ( 0.0000,  0.0000,  0.0000)
   5 H      8.530140    9.128834    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.971529    0.000000    0.000000   108     0.1479
  2. axis:    no     0.000000   16.128834    0.000000   108     0.1493
  3. axis:    no     0.000000    0.000000   15.783544   104     0.1518

  Lengths:  15.971529  16.128834  15.783544
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1497

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:54:07  +0.91          -28.986482    0      1      
iter:   2  22:54:10  -0.10          -29.045741    0      1      
iter:   3  22:54:11  -0.60          -29.052780    0      1      
iter:   4  22:54:13  -2.27          -29.054243    0      1      
iter:   5  22:54:14  -3.34          -29.054311    0      1      
iter:   6  22:54:16  -4.04          -29.054323    0      1      
iter:   7  22:54:17  -4.21          -29.054324    0      1      
iter:   8  22:54:18  -5.19          -29.054324    0      1      
iter:   9  22:54:20  -6.34          -29.054325    0      1      
iter:  10  22:54:21  -7.54          -29.054325    0      1      
iter:  11  22:54:23  -8.30          -29.054325    0      1      
iter:  12  22:54:24  -9.18          -29.054325    0      1      
iter:  13  22:54:25  -9.19          -29.054325    0      1      
iter:  14  22:54:28 -10.71          -29.054325    0      1      

Converged after 14 iterations.

Dipole moment: (0.258300, 0.149766, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3118.441066)

Kinetic:        +26.150106
Potential:      -27.933170
External:        +0.000000
XC:             -27.359371
Entropy (-ST):   +0.000000
Local:           +0.088110
--------------------------
Free energy:    -29.054325
Extrapolated:   -29.054325

Fermi levels: -2.21031, -2.21031

 Band  Eigenvalues  Occupancy
    0    -25.09761    2.00000
    1    -16.70265    2.00000
    2    -12.23253    2.00000
    3    -10.43647    2.00000
    4    -10.09955    2.00000
    5     -7.67597    2.00000
    6     -5.81366    2.00000
    7      1.39304    0.00000
    8      2.61216    0.00000
    9      3.75527    0.00000
   10      3.88410    0.00000
   11      4.69170    0.00000
   12      9.40437    0.00000
   13      9.76541    0.00000
   14     10.70912    0.00000
   15     12.34592    0.00000
   16     14.26880    0.00000
   17     15.24783    0.00000
   18     15.59634    0.00000
   19     16.66580    0.00000
   20     16.72364    0.00000
   21     20.75153    0.00000
   22     21.22510    0.00000
   23     22.07871    0.00000
   24     25.24753    0.00000
   25     25.95422    0.00000
   26     27.89476    0.00000
   27     28.77647    0.00000
   28     33.15984    0.00000
   29     34.57317    0.00000
   30     35.72654    0.00000
   31     36.49256    0.00000
   32     38.34047    0.00000
   33     41.16083    0.00000
   34     41.92224    0.00000
   35     42.90272    0.00000
   36     43.40457    0.00000
   37     45.78980    0.00000
   38     50.18612    0.00000
   39     51.47392    0.00000
   40     51.90372    0.00000
   41     56.95117    0.00000
   42     61.21126    0.00000
   43     61.32293    0.00000
   44     64.14871    0.00000
   45     73.07159    0.00000

Gap: 7.207 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.009     0.009   0.0% |
LCAO WFS Initialize:                   1.392     0.037   0.1% |
 Hamiltonian:                          1.354     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.049     0.049   0.2% |
  Hartree integrate/restrict:          0.025     0.025   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.387     0.011   0.0% |
   Communicate bwd 0:                  0.074     0.074   0.3% |
   Communicate bwd 1:                  0.071     0.071   0.2% |
   Communicate fwd 0:                  0.072     0.072   0.2% |
   Communicate fwd 1:                  0.079     0.079   0.3% |
   fft:                                0.023     0.023   0.1% |
   fft2:                               0.056     0.056   0.2% |
  XC 3D grid:                          0.885     0.885   3.0% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            25.201     0.003   0.0% |
 Direct Minimisation step:            23.809     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.171     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.074     0.074   0.3% |
   Potential matrix:                   0.091     0.091   0.3% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.010     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.013     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.012     0.012   0.0% |
  Update Kohn-Sham energy:            23.609     0.000   0.0% |
   Density:                            0.811     0.000   0.0% |
    Atomic density matrices:           0.091     0.091   0.3% |
    Mix:                               0.617     0.617   2.1% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.096     0.009   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.086     0.086   0.3% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       22.798     0.002   0.0% |
    Atomic:                            0.005     0.005   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.007     0.007   0.0% |
    Communicate:                       0.836     0.836   2.9% ||
    Hartree integrate/restrict:        0.427     0.427   1.5% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.571     0.200   0.7% |
     Communicate bwd 0:                1.275     1.275   4.4% |-|
     Communicate bwd 1:                1.248     1.248   4.3% |-|
     Communicate fwd 0:                1.195     1.195   4.1% |-|
     Communicate fwd 1:                1.324     1.324   4.5% |-|
     fft:                              0.389     0.389   1.3% ||
     fft2:                             0.940     0.940   3.2% ||
    XC 3D grid:                       14.884    14.884  50.9% |-------------------|
    vbar:                              0.065     0.065   0.2% |
 Get canonical representation:         1.388     0.000   0.0% |
  LCAO eigensolver:                    0.010     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Update Kohn-Sham energy:             1.377     0.000   0.0% |
   Density:                            0.046     0.000   0.0% |
    Atomic density matrices:           0.005     0.005   0.0% |
    Mix:                               0.035     0.035   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.006     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.005     0.005   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.331     0.000   0.0% |
    Atomic:                            0.001     0.001   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.049     0.049   0.2% |
    Hartree integrate/restrict:        0.025     0.025   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.385     0.011   0.0% |
     Communicate bwd 0:                0.075     0.075   0.3% |
     Communicate bwd 1:                0.074     0.074   0.3% |
     Communicate fwd 0:                0.071     0.071   0.2% |
     Communicate fwd 1:                0.077     0.077   0.3% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.055     0.055   0.2% |
    XC 3D grid:                        0.868     0.868   3.0% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.529     0.529   1.8% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.113     2.113   7.2% |--|
-------------------------------------------------------------
Total:                                          29.248 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:54:28 2019
