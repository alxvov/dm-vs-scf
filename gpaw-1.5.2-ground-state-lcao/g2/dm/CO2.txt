
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:36:28 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -5109.332718

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 37.87 MiB
    Density: 22.52 MiB
      Arrays: 21.92 MiB
      Localized functions: 0.60 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 14.38 MiB
      Arrays: 14.34 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.98 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.94 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 39
Number of bands in calculation: 39
Bands to converge: occupied states only
Number of valence electrons: 16

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            O                    |  
 |        |                                 |  
 |        |            C                    |  
 |        |            O                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 C      7.000000    7.000000    8.178658    ( 0.0000,  0.0000,  0.0000)
   1 O      7.000000    7.000000    9.357316    ( 0.0000,  0.0000,  0.0000)
   2 O      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.357316   108     0.1515

  Lengths:  14.000000  14.000000  16.357316
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1519

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:36:34  -0.29          -21.672087    0      1      
iter:   2  22:36:35  -1.17          -21.678983    0      1      
iter:   3  22:36:36  -3.28          -21.680023    0      1      
iter:   4  22:36:37  -4.54          -21.680028    0      1      
iter:   5  22:36:39  -5.28          -21.680028    0      1      
iter:   6  22:36:40  -5.66          -21.680028    0      1      
iter:   7  22:36:41  -7.67          -21.680028    0      1      
iter:   8  22:36:42  -8.72          -21.680028    0      1      
iter:   9  22:36:44 -10.20          -21.680028    0      1      

Converged after 9 iterations.

Dipole moment: (0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5109.332718)

Kinetic:        +16.107075
Potential:      -18.035130
External:        +0.000000
XC:             -19.899699
Entropy (-ST):   +0.000000
Local:           +0.147726
--------------------------
Free energy:    -21.680028
Extrapolated:   -21.680028

Fermi levels: -4.44233, -4.44233

 Band  Eigenvalues  Occupancy
    0    -28.83200    2.00000
    1    -27.76990    2.00000
    2    -13.56587    2.00000
    3    -12.29632    2.00000
    4    -12.27748    2.00000
    5    -12.27748    2.00000
    6     -8.66769    2.00000
    7     -8.66769    2.00000
    8     -0.21697    0.00000
    9     -0.21697    0.00000
   10      2.04882    0.00000
   11      9.21782    0.00000
   12      9.21782    0.00000
   13     10.86943    0.00000
   14     14.81246    0.00000
   15     15.54070    0.00000
   16     15.90385    0.00000
   17     17.02693    0.00000
   18     17.02693    0.00000
   19     21.81592    0.00000
   20     21.81592    0.00000
   21     22.54881    0.00000
   22     22.55047    0.00000
   23     23.54974    0.00000
   24     30.07045    0.00000
   25     30.07045    0.00000
   26     40.89711    0.00000
   27     44.43235    0.00000
   28     44.43243    0.00000
   29     45.31269    0.00000
   30     49.92865    0.00000
   31     49.92913    0.00000
   32     54.54584    0.00000
   33     54.54584    0.00000
   34     63.58554    0.00000
   35     66.28156    0.00000
   36     69.34099    0.00000
   37     71.29841    0.00000
   38     71.29841    0.00000

Gap: 8.451 eV
Transition (v -> c):
  (s=0, k=0, n=7, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.004     0.004   0.0% |
LCAO WFS Initialize:                   1.061     0.028   0.2% |
 Hamiltonian:                          1.033     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.048     0.048   0.3% |
  Hartree integrate/restrict:          0.021     0.021   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.297     0.004   0.0% |
   Communicate bwd 0:                  0.058     0.058   0.4% |
   Communicate bwd 1:                  0.055     0.055   0.4% |
   Communicate fwd 0:                  0.053     0.053   0.3% |
   Communicate fwd 1:                  0.057     0.057   0.4% |
   fft:                                0.017     0.017   0.1% |
   fft2:                               0.053     0.053   0.3% |
  XC 3D grid:                          0.663     0.663   4.2% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            12.691     0.002   0.0% |
 Direct Minimisation step:            11.634     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.045     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.010     0.010   0.1% |
   Potential matrix:                   0.031     0.031   0.2% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.008     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.007     0.007   0.0% |
  Update Kohn-Sham energy:            11.572     0.000   0.0% |
   Density:                            0.347     0.000   0.0% |
    Atomic density matrices:           0.016     0.016   0.1% |
    Mix:                               0.291     0.291   1.8% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.036     0.004   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.032     0.032   0.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       11.225     0.001   0.0% |
    Atomic:                            0.003     0.003   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.005     0.005   0.0% |
    Communicate:                       0.535     0.535   3.4% ||
    Hartree integrate/restrict:        0.224     0.224   1.4% ||
    New Kinetic Energy:                0.001     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.276     0.050   0.3% |
     Communicate bwd 0:                0.635     0.635   4.0% |-|
     Communicate bwd 1:                0.605     0.605   3.8% |-|
     Communicate fwd 0:                0.580     0.580   3.7% ||
     Communicate fwd 1:                0.635     0.635   4.0% |-|
     fft:                              0.183     0.183   1.2% |
     fft2:                             0.588     0.588   3.7% ||
    XC 3D grid:                        7.148     7.148  45.4% |-----------------|
    vbar:                              0.032     0.032   0.2% |
 Get canonical representation:         1.056     0.000   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Update Kohn-Sham energy:             1.051     0.000   0.0% |
   Density:                            0.031     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.026     0.026   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.003     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.003     0.003   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.020     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.049     0.049   0.3% |
    Hartree integrate/restrict:        0.019     0.019   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.299     0.005   0.0% |
     Communicate bwd 0:                0.059     0.059   0.4% |
     Communicate bwd 1:                0.054     0.054   0.3% |
     Communicate fwd 0:                0.053     0.053   0.3% |
     Communicate fwd 1:                0.058     0.058   0.4% |
     fft:                              0.017     0.017   0.1% |
     fft2:                             0.053     0.053   0.3% |
    XC 3D grid:                        0.649     0.649   4.1% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.349     0.349   2.2% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.644     1.644  10.4% |---|
-------------------------------------------------------------
Total:                                          15.754 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:36:44 2019
