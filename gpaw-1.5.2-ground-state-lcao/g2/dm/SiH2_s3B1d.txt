
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:20:58 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -7911.324575

Spin-polarized calculation.
Magnetic moment: 2.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*112*100 grid
  Fine grid: 184*224*200 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*224*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 60.70 MiB
    Density: 34.28 MiB
      Arrays: 33.29 MiB
      Localized functions: 0.98 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 24.81 MiB
      Arrays: 24.74 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.61 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.60 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 23
Bands to converge: occupied states only
Number of valence electrons: 6

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           Si                    |  
 |         |          H                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 Si     7.000000    8.271862    7.758952    ( 0.0000,  0.0000,  2.0000)
   1 H      7.000000    9.543724    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.543724    0.000000   112     0.1477
  3. axis:    no     0.000000    0.000000   14.758952   100     0.1476

  Lengths:  14.000000  16.543724  14.758952
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1491

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  22:21:06  +1.78           -8.300191    0      1        +2.0000
iter:   2  22:21:07  -0.43           -8.436889    0      1        +2.0000
iter:   3  22:21:09  -1.59           -8.437992    0      1        +2.0000
iter:   4  22:21:11  -2.99           -8.438055    0      1        +2.0000
iter:   5  22:21:13  -3.90           -8.438059    0      1        +2.0000
iter:   6  22:21:15  -5.04           -8.438060    0      1        +2.0000
iter:   7  22:21:17  -6.26           -8.438060    0      1        +2.0000
iter:   8  22:21:18  -6.78           -8.438060    0      1        +2.0000
iter:   9  22:21:20  -7.87           -8.438060    0      1        +2.0000
iter:  10  22:21:22  -9.14           -8.438060    0      1        +2.0000
iter:  11  22:21:26 -10.56           -8.438060    0      1        +2.0000

Converged after 11 iterations.

Dipole moment: (-0.000000, -0.000000, 0.012659) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.999999)
Local magnetic moments:
   0 Si ( 0.000000,  0.000000,  0.598759)
   1 H  ( 0.000000,  0.000000,  0.020620)
   2 H  ( 0.000000,  0.000000,  0.020620)

Energy contributions relative to reference atoms: (reference = -7911.324575)

Kinetic:         +9.377830
Potential:       -8.826898
External:        +0.000000
XC:              -8.972920
Entropy (-ST):   +0.000000
Local:           -0.016071
--------------------------
Free energy:     -8.438060
Extrapolated:    -8.438060

Spin contamination: 0.003905 electrons
Fermi levels: -2.10224, -6.32815

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -13.05857    1.00000    -12.01611    1.00000
    1     -9.07713    1.00000     -8.63282    1.00000
    2     -6.18878    1.00000     -4.02348    0.00000
    3     -4.56660    1.00000     -2.39180    0.00000
    4      0.36213    0.00000      1.28257    0.00000
    5      0.92002    0.00000      2.13072    0.00000
    6      5.03743    0.00000      5.42334    0.00000
    7      5.25770    0.00000      6.08466    0.00000
    8      5.76428    0.00000      7.24402    0.00000
    9      6.55833    0.00000      7.64564    0.00000
   10      7.41932    0.00000      8.87353    0.00000
   11      7.56654    0.00000      9.91331    0.00000
   12      7.71881    0.00000     10.26328    0.00000
   13     10.94601    0.00000     11.90044    0.00000
   14     13.62359    0.00000     14.65279    0.00000
   15     16.08042    0.00000     17.39897    0.00000
   16     18.09892    0.00000     18.76895    0.00000
   17     23.32678    0.00000     24.03330    0.00000
   18     25.56002    0.00000     26.27963    0.00000
   19     26.40317    0.00000     27.31047    0.00000
   20     30.30776    0.00000     30.96432    0.00000
   21     38.63609    0.00000     39.27834    0.00000
   22     42.70163    0.00000     43.40968    0.00000

Gap: 0.543 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=2, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.006     0.006   0.0% |
LCAO WFS Initialize:                   1.802     0.060   0.2% |
 Hamiltonian:                          1.742     0.002   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.106     0.106   0.4% |
  Hartree integrate/restrict:          0.042     0.042   0.2% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.329     0.008   0.0% |
   Communicate bwd 0:                  0.063     0.063   0.2% |
   Communicate bwd 1:                  0.060     0.060   0.2% |
   Communicate fwd 0:                  0.062     0.062   0.2% |
   Communicate fwd 1:                  0.062     0.062   0.2% |
   fft:                                0.021     0.021   0.1% |
   fft2:                               0.053     0.053   0.2% |
  XC 3D grid:                          1.260     1.260   4.6% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            23.657     0.003   0.0% |
 Direct Minimisation step:            21.835     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.109     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.019     0.019   0.1% |
   Potential matrix:                   0.084     0.084   0.3% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.009     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.007     0.007   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.014     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.013     0.013   0.0% |
  Update Kohn-Sham energy:            21.697     0.000   0.0% |
   Density:                            0.827     0.000   0.0% |
    Atomic density matrices:           0.027     0.027   0.1% |
    Mix:                               0.691     0.691   2.5% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.104     0.013   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.090     0.090   0.3% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       20.869     0.021   0.1% |
    Atomic:                            0.003     0.003   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.006     0.006   0.0% |
    Communicate:                       1.280     1.280   4.6% |-|
    Hartree integrate/restrict:        0.501     0.501   1.8% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.965     0.098   0.4% |
     Communicate bwd 0:                0.759     0.759   2.8% ||
     Communicate bwd 1:                0.723     0.723   2.6% ||
     Communicate fwd 0:                0.731     0.731   2.7% ||
     Communicate fwd 1:                0.775     0.775   2.8% ||
     fft:                              0.249     0.249   0.9% |
     fft2:                             0.630     0.630   2.3% ||
    XC 3D grid:                       15.052    15.052  54.7% |---------------------|
    vbar:                              0.039     0.039   0.1% |
 Get canonical representation:         1.819     0.000   0.0% |
  LCAO eigensolver:                    0.009     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.007     0.007   0.0% |
  Update Kohn-Sham energy:             1.809     0.000   0.0% |
   Density:                            0.069     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.057     0.057   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.009     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.007     0.007   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.741     0.002   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.107     0.107   0.4% |
    Hartree integrate/restrict:        0.041     0.041   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.329     0.008   0.0% |
     Communicate bwd 0:                0.063     0.063   0.2% |
     Communicate bwd 1:                0.060     0.060   0.2% |
     Communicate fwd 0:                0.061     0.061   0.2% |
     Communicate fwd 1:                0.064     0.064   0.2% |
     fft:                              0.021     0.021   0.1% |
     fft2:                             0.052     0.052   0.2% |
    XC 3D grid:                        1.258     1.258   4.6% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.202     0.202   0.7% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.861     1.861   6.8% |--|
-------------------------------------------------------------
Total:                                          27.532 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:21:26 2019
