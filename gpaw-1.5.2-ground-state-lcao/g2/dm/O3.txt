
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:58:24 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -6122.556903

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*108*96 grid
  Fine grid: 184*216*192 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 39.30 MiB
    Density: 23.53 MiB
      Arrays: 22.88 MiB
      Localized functions: 0.65 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 15.01 MiB
      Arrays: 14.97 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.76 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.72 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 39
Number of bands in calculation: 39
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           OO                    |  
 |         |          O                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 O      7.000000    9.207620    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 O      7.000000    8.103810    7.685626    ( 0.0000,  0.0000,  0.0000)
   2 O      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.207620    0.000000   108     0.1501
  3. axis:    no     0.000000    0.000000   14.685626    96     0.1530

  Lengths:  14.000000  16.207620  14.685626
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1517

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:58:30  +1.42          -12.240243    0      1      
iter:   2  22:58:32  +0.14          -12.411277    0      1      
iter:   3  22:58:33  -1.52          -12.423769    0      1      
iter:   4  22:58:34  -2.16          -12.424361    0      1      
iter:   5  22:58:35  -2.70          -12.424556    0      1      
iter:   6  22:58:36  -3.58          -12.424584    0      1      
iter:   7  22:58:38  -3.88          -12.424589    0      1      
iter:   8  22:58:39  -4.88          -12.424590    0      1      
iter:   9  22:58:40  -5.95          -12.424590    0      1      
iter:  10  22:58:41  -6.44          -12.424590    0      1      
iter:  11  22:58:42  -7.19          -12.424590    0      1      
iter:  12  22:58:43  -7.64          -12.424590    0      1      
iter:  13  22:58:44  -8.77          -12.424590    0      1      
iter:  14  22:58:45  -9.46          -12.424590    0      1      
iter:  15  22:58:46  -9.76          -12.424590    0      1      
iter:  16  22:58:47  -9.95          -12.424590    0      1      
iter:  17  22:58:49 -10.74          -12.424590    0      1      

Converged after 17 iterations.

Dipole moment: (0.000000, 0.000000, 0.126112) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6122.556903)

Kinetic:        +15.025117
Potential:      -13.894851
External:        +0.000000
XC:             -13.792200
Entropy (-ST):   +0.000000
Local:           +0.237344
--------------------------
Free energy:    -12.424590
Extrapolated:   -12.424590

Fermi levels: -6.55825, -6.55825

 Band  Eigenvalues  Occupancy
    0    -33.34206    2.00000
    1    -26.60291    2.00000
    2    -19.05579    2.00000
    3    -14.07080    2.00000
    4    -14.06628    2.00000
    5    -13.65822    2.00000
    6     -8.51844    2.00000
    7     -7.41021    2.00000
    8     -7.36727    2.00000
    9     -5.74923    0.00000
   10      0.29096    0.00000
   11      2.38500    0.00000
   12     14.58047    0.00000
   13     14.61329    0.00000
   14     15.30590    0.00000
   15     16.08371    0.00000
   16     16.69052    0.00000
   17     17.48936    0.00000
   18     18.05337    0.00000
   19     18.66906    0.00000
   20     20.15040    0.00000
   21     24.69054    0.00000
   22     34.90966    0.00000
   23     35.63311    0.00000
   24     36.60487    0.00000
   25     38.08980    0.00000
   26     38.58341    0.00000
   27     45.54770    0.00000
   28     45.55097    0.00000
   29     46.09658    0.00000
   30     46.86528    0.00000
   31     51.48119    0.00000
   32     55.41242    0.00000
   33     58.92540    0.00000
   34     60.93218    0.00000
   35     63.24140    0.00000
   36     64.80329    0.00000
   37     67.61661    0.00000
   38     72.62486    0.00000

Gap: 1.618 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.004     0.004   0.0% |
LCAO WFS Initialize:                   1.091     0.029   0.1% |
 Hamiltonian:                          1.063     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.048     0.048   0.2% |
  Hartree integrate/restrict:          0.022     0.022   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.302     0.008   0.0% |
   Communicate bwd 0:                  0.059     0.059   0.2% |
   Communicate bwd 1:                  0.058     0.058   0.2% |
   Communicate fwd 0:                  0.054     0.054   0.2% |
   Communicate fwd 1:                  0.059     0.059   0.2% |
   fft:                                0.017     0.017   0.1% |
   fft2:                               0.047     0.047   0.2% |
  XC 3D grid:                          0.687     0.687   2.7% ||
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            22.824     0.004   0.0% |
 Direct Minimisation step:            21.734     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.067     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.012     0.012   0.0% |
   Potential matrix:                   0.048     0.048   0.2% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.015     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.013     0.013   0.1% |
  Update Kohn-Sham energy:            21.641     0.000   0.0% |
   Density:                            0.624     0.000   0.0% |
    Atomic density matrices:           0.022     0.022   0.1% |
    Mix:                               0.539     0.539   2.1% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.056     0.007   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.048     0.048   0.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       21.017     0.002   0.0% |
    Atomic:                            0.006     0.006   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.009     0.009   0.0% |
    Communicate:                       0.957     0.957   3.7% ||
    Hartree integrate/restrict:        0.424     0.424   1.7% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           6.018     0.143   0.6% |
     Communicate bwd 0:                1.179     1.179   4.6% |-|
     Communicate bwd 1:                1.156     1.156   4.5% |-|
     Communicate fwd 0:                1.077     1.077   4.2% |-|
     Communicate fwd 1:                1.173     1.173   4.6% |-|
     fft:                              0.345     0.345   1.3% ||
     fft2:                             0.945     0.945   3.7% ||
    XC 3D grid:                       13.538    13.538  52.7% |--------------------|
    vbar:                              0.061     0.061   0.2% |
 Get canonical representation:         1.086     0.000   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Update Kohn-Sham energy:             1.082     0.000   0.0% |
   Density:                            0.031     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.027     0.027   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.003     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.002     0.002   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.051     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.048     0.048   0.2% |
    Hartree integrate/restrict:        0.022     0.022   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.300     0.007   0.0% |
     Communicate bwd 0:                0.059     0.059   0.2% |
     Communicate bwd 1:                0.058     0.058   0.2% |
     Communicate fwd 0:                0.054     0.054   0.2% |
     Communicate fwd 1:                0.058     0.058   0.2% |
     fft:                              0.017     0.017   0.1% |
     fft2:                             0.047     0.047   0.2% |
    XC 3D grid:                        0.677     0.677   2.6% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.098     0.098   0.4% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.687     1.687   6.6% |--|
-------------------------------------------------------------
Total:                                          25.706 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 22:58:49 2019
