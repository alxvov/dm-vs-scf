
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:19:06 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -52275.053814

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*112*116 grid
  Fine grid: 184*224*232 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*224*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.64 MiB
  Calculator: 52.06 MiB
    Density: 30.46 MiB
      Arrays: 28.74 MiB
      Localized functions: 1.72 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 18.93 MiB
      Arrays: 18.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.13 MiB
    Wavefunctions: 2.66 MiB
      C [qnM]: 0.05 MiB
      S, T [2 x qmm]: 0.09 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.52 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 78
Number of bands in calculation: 78
Bands to converge: occupied states only
Number of valence electrons: 36

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |            Cl                   |  
 |         |          Cl                     |  
 |         |           C                     |  
 |         |           C                     |  
 |         |            Cl                   |  
 |         |          Cl                     |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 C      7.000000    8.448939    9.265103    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000    8.448939    7.914299    ( 0.0000,  0.0000,  0.0000)
   2 Cl     7.000000    9.897878   10.179402    ( 0.0000,  0.0000,  0.0000)
   3 Cl     7.000000    7.000000   10.179402    ( 0.0000,  0.0000,  0.0000)
   4 Cl     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 Cl     7.000000    9.897878    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.897878    0.000000   112     0.1509
  3. axis:    no     0.000000    0.000000   17.179402   116     0.1481

  Lengths:  14.000000  16.897878  17.179402
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1504

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:19:15  +0.09          -23.539711    0      1      
iter:   2  23:19:17  -1.27          -23.588090    0      1      
iter:   3  23:19:19  -2.42          -23.590896    0      1      
iter:   4  23:19:20  -3.63          -23.591142    0      1      
iter:   5  23:19:22  -4.50          -23.591169    0      1      
iter:   6  23:19:23  -5.22          -23.591172    0      1      
iter:   7  23:19:25  -6.03          -23.591172    0      1      
iter:   8  23:19:26  -6.36          -23.591172    0      1      
iter:   9  23:19:27  -7.16          -23.591172    0      1      
iter:  10  23:19:29  -8.82          -23.591172    0      1      
iter:  11  23:19:30  -9.80          -23.591172    0      1      
iter:  12  23:19:33 -10.44          -23.591172    0      1      

Converged after 12 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -52275.053814)

Kinetic:        +38.596322
Potential:      -33.394176
External:        +0.000000
XC:             -28.879025
Entropy (-ST):   +0.000000
Local:           +0.085707
--------------------------
Free energy:    -23.591172
Extrapolated:   -23.591172

Fermi levels: -3.48808, -3.48808

 Band  Eigenvalues  Occupancy
    0    -23.76119    2.00000
    1    -22.44474    2.00000
    2    -21.35901    2.00000
    3    -20.76251    2.00000
    4    -17.76838    2.00000
    5    -13.78761    2.00000
    6    -12.37901    2.00000
    7    -12.32625    2.00000
    8    -10.66417    2.00000
    9    -10.62028    2.00000
   10     -8.96659    2.00000
   11     -8.37286    2.00000
   12     -8.09284    2.00000
   13     -8.01818    2.00000
   14     -7.87961    2.00000
   15     -7.74003    2.00000
   16     -6.97391    2.00000
   17     -5.61735    2.00000
   18     -1.35880    0.00000
   19     -0.79637    0.00000
   20     -0.61866    0.00000
   21     -0.30620    0.00000
   22      3.41008    0.00000
   23      6.48560    0.00000
   24      8.15945    0.00000
   25      8.20056    0.00000
   26      9.61427    0.00000
   27     10.33115    0.00000
   28     11.23645    0.00000
   29     11.54663    0.00000
   30     11.58949    0.00000
   31     11.72158    0.00000
   32     12.02411    0.00000
   33     12.05953    0.00000
   34     12.13820    0.00000
   35     12.48855    0.00000
   36     12.50670    0.00000
   37     12.68571    0.00000
   38     12.82917    0.00000
   39     13.10609    0.00000
   40     13.12655    0.00000
   41     13.41004    0.00000
   42     13.71209    0.00000
   43     14.00036    0.00000
   44     14.00562    0.00000
   45     14.75559    0.00000
   46     15.07610    0.00000
   47     15.43517    0.00000
   48     15.43689    0.00000
   49     15.67835    0.00000
   50     16.18960    0.00000
   51     16.89716    0.00000
   52     17.36831    0.00000
   53     18.57134    0.00000
   54     18.87130    0.00000
   55     20.17302    0.00000
   56     20.79519    0.00000
   57     21.05847    0.00000
   58     22.41434    0.00000
   59     22.56386    0.00000
   60     22.59154    0.00000
   61     22.97504    0.00000
   62     27.01573    0.00000
   63     28.27136    0.00000
   64     28.45947    0.00000
   65     28.46821    0.00000
   66     28.84081    0.00000
   67     33.53434    0.00000
   68     36.42906    0.00000
   69     37.31952    0.00000
   70     37.65429    0.00000
   71     39.38013    0.00000
   72     40.98344    0.00000
   73     41.77621    0.00000
   74     42.09234    0.00000
   75     46.36121    0.00000
   76     51.39823    0.00000
   77     55.00154    0.00000

Gap: 4.259 eV
Transition (v -> c):
  (s=0, k=0, n=17, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=18, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.010     0.010   0.0% |
LCAO WFS Initialize:                   1.444     0.039   0.1% |
 Hamiltonian:                          1.404     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.064     0.064   0.2% |
  Hartree integrate/restrict:          0.020     0.020   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.421     0.006   0.0% |
   Communicate bwd 0:                  0.080     0.080   0.3% |
   Communicate bwd 1:                  0.076     0.076   0.3% |
   Communicate fwd 0:                  0.081     0.081   0.3% |
   Communicate fwd 1:                  0.081     0.081   0.3% |
   fft:                                0.035     0.035   0.1% |
   fft2:                               0.062     0.062   0.2% |
  XC 3D grid:                          0.891     0.891   3.3% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            22.895     0.003   0.0% |
 Direct Minimisation step:            21.472     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.225     0.004   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.040     0.040   0.1% |
   Potential matrix:                   0.176     0.176   0.7% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.016     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.017     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.015     0.015   0.1% |
  Update Kohn-Sham energy:            21.207     0.000   0.0% |
   Density:                            0.796     0.000   0.0% |
    Atomic density matrices:           0.055     0.055   0.2% |
    Mix:                               0.559     0.559   2.1% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.176     0.009   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.166     0.166   0.6% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       20.411     0.002   0.0% |
    Atomic:                            0.005     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.010     0.010   0.0% |
    Communicate:                       0.956     0.956   3.6% ||
    Hartree integrate/restrict:        0.372     0.372   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.062     0.105   0.4% |
     Communicate bwd 0:                1.165     1.165   4.4% |-|
     Communicate bwd 1:                1.116     1.116   4.2% |-|
     Communicate fwd 0:                1.082     1.082   4.0% |-|
     Communicate fwd 1:                1.145     1.145   4.3% |-|
     fft:                              0.533     0.533   2.0% ||
     fft2:                             0.915     0.915   3.4% ||
    XC 3D grid:                       12.948    12.948  48.4% |------------------|
    vbar:                              0.056     0.056   0.2% |
 Get canonical representation:         1.420     0.000   0.0% |
  LCAO eigensolver:                    0.016     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Update Kohn-Sham energy:             1.404     0.000   0.0% |
   Density:                            0.052     0.000   0.0% |
    Atomic density matrices:           0.004     0.004   0.0% |
    Mix:                               0.036     0.036   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.012     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.011     0.011   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.353     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.064     0.064   0.2% |
    Hartree integrate/restrict:        0.026     0.026   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.402     0.006   0.0% |
     Communicate bwd 0:                0.077     0.077   0.3% |
     Communicate bwd 1:                0.074     0.074   0.3% |
     Communicate fwd 0:                0.072     0.072   0.3% |
     Communicate fwd 1:                0.076     0.076   0.3% |
     fft:                              0.035     0.035   0.1% |
     fft2:                             0.061     0.061   0.2% |
    XC 3D grid:                        0.856     0.856   3.2% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.349     0.349   1.3% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.046     2.046   7.6% |--|
-------------------------------------------------------------
Total:                                          26.748 100.0%

Memory usage: 436.64 MiB
Date: Sat Feb 16 23:19:33 2019
