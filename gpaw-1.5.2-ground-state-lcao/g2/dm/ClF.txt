
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:32:57 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -15269.030181

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 36.02 MiB
    Density: 21.60 MiB
      Arrays: 21.10 MiB
      Localized functions: 0.50 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 13.84 MiB
      Arrays: 13.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.58 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.56 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 26
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Cl                   |  
 |        |                                 |  
 |        |            F                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 Cl     7.000000    7.000000    8.659096    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.659096   104     0.1506

  Lengths:  14.000000  14.000000  15.659096
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1516

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:33:03  +0.97           -3.028972    0      1      
iter:   2  22:33:05  -0.33           -3.078716    0      1      
iter:   3  22:33:06  -1.95           -3.081699    0      1      
iter:   4  22:33:07  -3.32           -3.081782    0      1      
iter:   5  22:33:08  -4.19           -3.081788    0      1      
iter:   6  22:33:09  -4.55           -3.081788    0      1      
iter:   7  22:33:10  -5.51           -3.081788    0      1      
iter:   8  22:33:11  -6.52           -3.081788    0      1      
iter:   9  22:33:12  -8.20           -3.081788    0      1      
iter:  10  22:33:13  -9.33           -3.081788    0      1      
iter:  11  22:33:15 -10.48           -3.081788    0      1      

Converged after 11 iterations.

Dipole moment: (-0.000000, 0.000000, 0.214011) |e|*Ang

Energy contributions relative to reference atoms: (reference = -15269.030181)

Kinetic:        +11.964807
Potential:       -8.661662
External:        +0.000000
XC:              -6.491848
Entropy (-ST):   +0.000000
Local:           +0.106916
--------------------------
Free energy:     -3.081788
Extrapolated:    -3.081788

Fermi levels: -5.64698, -5.64698

 Band  Eigenvalues  Occupancy
    0    -30.38573    2.00000
    1    -20.44675    2.00000
    2    -12.85182    2.00000
    3    -10.83752    2.00000
    4    -10.83752    2.00000
    5     -7.27582    2.00000
    6     -7.27582    2.00000
    7     -4.01815    0.00000
    8     11.86421    0.00000
    9     12.88607    0.00000
   10     12.88607    0.00000
   11     14.03395    0.00000
   12     14.03395    0.00000
   13     14.60711    0.00000
   14     14.60809    0.00000
   15     16.38185    0.00000
   16     20.67452    0.00000
   17     22.49535    0.00000
   18     22.49535    0.00000
   19     23.01297    0.00000
   20     45.74013    0.00000
   21     57.81835    0.00000
   22     57.81873    0.00000
   23     59.45610    0.00000
   24     59.45610    0.00000
   25     69.73151    0.00000

Gap: 3.258 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.003     0.003   0.0% |
LCAO WFS Initialize:                   1.040     0.026   0.1% |
 Hamiltonian:                          1.015     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.063     0.063   0.3% |
  Hartree integrate/restrict:          0.020     0.020   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.290     0.004   0.0% |
   Communicate bwd 0:                  0.056     0.056   0.3% |
   Communicate bwd 1:                  0.053     0.053   0.3% |
   Communicate fwd 0:                  0.051     0.051   0.3% |
   Communicate fwd 1:                  0.056     0.056   0.3% |
   fft:                                0.019     0.019   0.1% |
   fft2:                               0.051     0.051   0.3% |
  XC 3D grid:                          0.637     0.637   3.4% ||
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            15.486     0.003   0.0% |
 Direct Minimisation step:            14.453     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.029     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.007     0.007   0.0% |
   Potential matrix:                   0.018     0.018   0.1% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.002     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.000     0.000   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.001     0.001   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.009     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.008     0.008   0.0% |
  Update Kohn-Sham energy:            14.408     0.000   0.0% |
   Density:                            0.383     0.000   0.0% |
    Atomic density matrices:           0.013     0.013   0.1% |
    Mix:                               0.342     0.342   1.8% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.024     0.005   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.018     0.018   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       14.025     0.001   0.0% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.005     0.005   0.0% |
    Communicate:                       0.890     0.890   4.8% |-|
    Hartree integrate/restrict:        0.280     0.280   1.5% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           4.057     0.061   0.3% |
     Communicate bwd 0:                0.786     0.786   4.2% |-|
     Communicate bwd 1:                0.739     0.739   4.0% |-|
     Communicate fwd 0:                0.708     0.708   3.8% |-|
     Communicate fwd 1:                0.781     0.781   4.2% |-|
     fft:                              0.267     0.267   1.4% ||
     fft2:                             0.715     0.715   3.9% |-|
    XC 3D grid:                        8.745     8.745  47.2% |------------------|
    vbar:                              0.040     0.040   0.2% |
 Get canonical representation:         1.031     0.000   0.0% |
  LCAO eigensolver:                    0.002     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.000     0.000   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.001     0.001   0.0% |
  Update Kohn-Sham energy:             1.028     0.000   0.0% |
   Density:                            0.027     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.024     0.024   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.002     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.001     0.001   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.001     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.064     0.064   0.3% |
    Hartree integrate/restrict:        0.020     0.020   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.290     0.004   0.0% |
     Communicate bwd 0:                0.057     0.057   0.3% |
     Communicate bwd 1:                0.053     0.053   0.3% |
     Communicate fwd 0:                0.051     0.051   0.3% |
     Communicate fwd 1:                0.056     0.056   0.3% |
     fft:                              0.019     0.019   0.1% |
     fft2:                             0.050     0.050   0.3% |
    XC 3D grid:                        0.623     0.623   3.4% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.351     0.351   1.9% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.659     1.659   8.9% |---|
-------------------------------------------------------------
Total:                                          18.542 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:33:16 2019
