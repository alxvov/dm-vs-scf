
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:21:26 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/S.PBE.gz
  cutoffs: 0.76(comp), 1.77(filt), 1.66(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -11935.208662

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*108*104 grid
  Fine grid: 216*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 51.21 MiB
    Density: 30.26 MiB
      Arrays: 29.18 MiB
      Localized functions: 1.08 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.17 MiB
      Arrays: 19.09 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 1.78 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.03 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.73 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 46
Number of bands in calculation: 46
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |                H                      |  
 |         |            H C                        |  
 |         |             S  H                      |  
 |         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 C      8.044648    8.972768    7.892259    ( 0.0000,  0.0000,  0.0000)
   1 S      8.044648    7.158393    7.892259    ( 0.0000,  0.0000,  0.0000)
   2 H      9.375677    7.000000    7.892259    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    9.284677    7.892259    ( 0.0000,  0.0000,  0.0000)
   4 H      8.524850    9.374456    8.784518    ( 0.0000,  0.0000,  0.0000)
   5 H      8.524850    9.374456    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.375677    0.000000    0.000000   108     0.1516
  2. axis:    no     0.000000   16.374456    0.000000   108     0.1516
  3. axis:    no     0.000000    0.000000   15.784518   104     0.1518

  Lengths:  16.375677  16.374456  15.784518
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1517

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  22:21:34  +0.64          -26.516231    0      1      
iter:   2  22:21:35  +0.69          -26.535397    0      1      
iter:   3  22:21:37  -2.01          -26.572167    0      1      
iter:   4  22:21:38  -3.01          -26.572301    0      1      
iter:   5  22:21:40  -4.07          -26.572313    0      1      
iter:   6  22:21:41  -4.65          -26.572314    0      1      
iter:   7  22:21:43  -5.76          -26.572314    0      1      
iter:   8  22:21:44  -6.28          -26.572314    0      1      
iter:   9  22:21:45  -7.72          -26.572314    0      1      
iter:  10  22:21:47  -8.58          -26.572314    0      1      
iter:  11  22:21:48  -9.85          -26.572314    0      1      
iter:  12  22:21:51 -10.14          -26.572314    0      1      

Converged after 12 iterations.

Dipole moment: (0.131624, 0.232667, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -11935.208662)

Kinetic:        +26.029426
Potential:      -28.212394
External:        +0.000000
XC:             -24.389131
Entropy (-ST):   +0.000000
Local:           -0.000215
--------------------------
Free energy:    -26.572314
Extrapolated:   -26.572314

Fermi levels: -2.42800, -2.42800

 Band  Eigenvalues  Occupancy
    0    -19.23641    2.00000
    1    -15.78134    2.00000
    2    -10.87507    2.00000
    3    -10.31231    2.00000
    4     -9.43696    2.00000
    5     -7.83241    2.00000
    6     -5.19378    2.00000
    7      0.33778    0.00000
    8      1.30660    0.00000
    9      2.64088    0.00000
   10      3.51752    0.00000
   11      3.59211    0.00000
   12      7.04377    0.00000
   13      7.99192    0.00000
   14      8.42717    0.00000
   15      8.76346    0.00000
   16     11.13024    0.00000
   17     11.52911    0.00000
   18     11.67079    0.00000
   19     12.72806    0.00000
   20     13.67530    0.00000
   21     13.94488    0.00000
   22     14.97226    0.00000
   23     15.45040    0.00000
   24     15.54533    0.00000
   25     17.31920    0.00000
   26     21.27346    0.00000
   27     22.26851    0.00000
   28     23.33361    0.00000
   29     23.70751    0.00000
   30     27.53506    0.00000
   31     28.15737    0.00000
   32     28.54352    0.00000
   33     30.12854    0.00000
   34     34.32450    0.00000
   35     34.53415    0.00000
   36     36.31740    0.00000
   37     36.73542    0.00000
   38     37.13008    0.00000
   39     41.57438    0.00000
   40     44.05510    0.00000
   41     44.42693    0.00000
   42     45.50504    0.00000
   43     50.56845    0.00000
   44     51.45482    0.00000
   45     51.81884    0.00000

Gap: 5.532 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.009     0.009   0.0% |
LCAO WFS Initialize:                   1.406     0.039   0.2% |
 Hamiltonian:                          1.367     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.065     0.065   0.3% |
  Hartree integrate/restrict:          0.026     0.026   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.387     0.011   0.0% |
   Communicate bwd 0:                  0.074     0.074   0.3% |
   Communicate bwd 1:                  0.072     0.072   0.3% |
   Communicate fwd 0:                  0.071     0.071   0.3% |
   Communicate fwd 1:                  0.080     0.080   0.3% |
   fft:                                0.023     0.023   0.1% |
   fft2:                               0.056     0.056   0.2% |
  XC 3D grid:                          0.884     0.884   3.5% ||
  vbar:                                0.004     0.004   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            21.308     0.003   0.0% |
 Direct Minimisation step:            19.883     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.141     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.046     0.046   0.2% |
   Potential matrix:                   0.091     0.091   0.4% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.010     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.007     0.007   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.011     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.010     0.010   0.0% |
  Update Kohn-Sham energy:            19.714     0.000   0.0% |
   Density:                            0.676     0.000   0.0% |
    Atomic density matrices:           0.059     0.059   0.2% |
    Mix:                               0.515     0.515   2.0% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.096     0.008   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.087     0.087   0.3% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       19.038     0.002   0.0% |
    Atomic:                            0.005     0.005   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.009     0.009   0.0% |
    Communicate:                       0.926     0.926   3.7% ||
    Hartree integrate/restrict:        0.361     0.361   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.431     0.166   0.7% |
     Communicate bwd 0:                1.055     1.055   4.2% |-|
     Communicate bwd 1:                1.038     1.038   4.1% |-|
     Communicate fwd 0:                0.986     0.986   3.9% |-|
     Communicate fwd 1:                1.091     1.091   4.3% |-|
     fft:                              0.320     0.320   1.3% ||
     fft2:                             0.774     0.774   3.1% ||
    XC 3D grid:                       12.250    12.250  48.4% |------------------|
    vbar:                              0.054     0.054   0.2% |
 Get canonical representation:         1.423     0.000   0.0% |
  LCAO eigensolver:                    0.010     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.006     0.006   0.0% |
  Update Kohn-Sham energy:             1.412     0.000   0.0% |
   Density:                            0.049     0.000   0.0% |
    Atomic density matrices:           0.004     0.004   0.0% |
    Mix:                               0.038     0.038   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.007     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.006     0.006   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.363     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.065     0.065   0.3% |
    Hartree integrate/restrict:        0.022     0.022   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.394     0.012   0.0% |
     Communicate bwd 0:                0.078     0.078   0.3% |
     Communicate bwd 1:                0.076     0.076   0.3% |
     Communicate fwd 0:                0.071     0.071   0.3% |
     Communicate fwd 1:                0.078     0.078   0.3% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.056     0.056   0.2% |
    XC 3D grid:                        0.876     0.876   3.5% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.530     0.530   2.1% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.066     2.066   8.2% |--|
-------------------------------------------------------------
Total:                                          25.323 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:21:51 2019
