
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 22:21:51 2019
Arch:   x86_64
Pid:    21950
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: direct_min_lcao
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -4133.579019

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*108*104 grid
  Fine grid: 224*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 414.69 MiB
  Calculator: 73.70 MiB
    Density: 41.53 MiB
      Arrays: 40.74 MiB
      Localized functions: 0.79 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 30.33 MiB
      Arrays: 30.27 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.85 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.78 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 54
Number of bands in calculation: 54
Bands to converge: occupied states only
Number of valence electrons: 17

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |           H                            |  
 |         |                C  O                    |  
 |         |             H                          |  
 |         |           H                            |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      7.639335    7.960023    7.881061    ( 0.0000,  0.0000,  0.1000)
   1 C      8.617626    9.114120    7.881061    ( 0.0000,  0.0000,  0.6000)
   2 H      8.162075    7.000000    7.881061    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.044566    8.762122    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    8.044566    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 O      9.812695    9.055782    7.881061    ( 0.0000,  0.0000,  0.3000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.812695    0.000000    0.000000   112     0.1501
  2. axis:    no     0.000000   16.114120    0.000000   108     0.1492
  3. axis:    no     0.000000    0.000000   15.762122   104     0.1516

  Lengths:  16.812695  16.114120  15.762122
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1503

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  22:22:02  +1.03          -32.596370    0      1        +1.0000
iter:   2  22:22:06  -0.05          -32.701130    0      1        +1.0000
iter:   3  22:22:08  -1.19          -32.718713    0      1        +1.0000
iter:   4  22:22:10  -1.93          -32.720415    0      1        +1.0000
iter:   5  22:22:12  -2.69          -32.720627    0      1        +1.0000
iter:   6  22:22:14  -3.37          -32.720656    0      1        +1.0000
iter:   7  22:22:16  -3.24          -32.720659    0      1        +1.0000
iter:   8  22:22:18  -4.43          -32.720664    0      1        +1.0000
iter:   9  22:22:20  -5.06          -32.720664    0      1        +1.0000
iter:  10  22:22:22  -5.93          -32.720665    0      1        +1.0000
iter:  11  22:22:25  -6.41          -32.720665    0      1        +1.0000
iter:  12  22:22:27  -6.92          -32.720665    0      1        +1.0000
iter:  13  22:22:31  -6.98          -32.720665    0      1        +1.0000
iter:  14  22:22:33  -7.19          -32.720665    0      1        +1.0000
iter:  15  22:22:35  -8.69          -32.720665    0      1        +1.0000
iter:  16  22:22:37  -9.72          -32.720665    0      1        +1.0000
iter:  17  22:22:41 -10.39          -32.720665    0      1        +1.0000

Converged after 17 iterations.

Dipole moment: (-0.373386, -0.254052, -0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999998)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.058751)
   1 C  ( 0.000000,  0.000000,  0.202795)
   2 H  ( 0.000000,  0.000000,  0.009243)
   3 H  ( 0.000000,  0.000000, -0.001407)
   4 H  ( 0.000000,  0.000000, -0.001407)
   5 O  ( 0.000000,  0.000000,  0.197080)

Energy contributions relative to reference atoms: (reference = -4133.579019)

Kinetic:        +27.344782
Potential:      -30.070069
External:        +0.000000
XC:             -30.100070
Entropy (-ST):   +0.000000
Local:           +0.104694
--------------------------
Free energy:    -32.720665
Extrapolated:   -32.720665

Spin contamination: 0.018635 electrons
Fermi levels: -2.83270, -5.31640

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -26.94852    1.00000    -26.60711    1.00000
    1    -18.85507    1.00000    -18.61571    1.00000
    2    -13.21030    1.00000    -12.80812    1.00000
    3    -11.20205    1.00000    -11.02091    1.00000
    4    -11.14949    1.00000    -10.85371    1.00000
    5    -10.97090    1.00000    -10.58067    1.00000
    6     -9.36041    1.00000     -9.21015    1.00000
    7     -9.01213    1.00000     -8.25713    1.00000
    8     -4.03597    1.00000     -2.37568    0.00000
    9     -1.62943    0.00000     -1.17014    0.00000
   10      2.01333    0.00000      2.09584    0.00000
   11      3.30806    0.00000      3.49006    0.00000
   12      3.43902    0.00000      3.52860    0.00000
   13      5.08027    0.00000      5.32347    0.00000
   14      6.90872    0.00000      7.14492    0.00000
   15      8.97955    0.00000      9.07225    0.00000
   16      9.88014    0.00000     10.48582    0.00000
   17     10.41918    0.00000     10.79671    0.00000
   18     10.68107    0.00000     10.92124    0.00000
   19     11.06107    0.00000     11.50440    0.00000
   20     13.84995    0.00000     13.98025    0.00000
   21     14.44804    0.00000     14.69353    0.00000
   22     14.78010    0.00000     14.86279    0.00000
   23     15.25207    0.00000     15.53191    0.00000
   24     16.73125    0.00000     16.94410    0.00000
   25     17.96369    0.00000     18.03896    0.00000
   26     18.53879    0.00000     19.03377    0.00000
   27     19.97613    0.00000     20.21222    0.00000
   28     20.10169    0.00000     20.51670    0.00000
   29     22.45380    0.00000     22.75413    0.00000
   30     26.51388    0.00000     27.10490    0.00000
   31     27.19528    0.00000     27.46464    0.00000
   32     27.28006    0.00000     27.53754    0.00000
   33     30.65522    0.00000     30.88970    0.00000
   34     30.71783    0.00000     31.24298    0.00000
   35     33.87176    0.00000     34.11786    0.00000
   36     35.02487    0.00000     35.24358    0.00000
   37     35.51882    0.00000     35.58315    0.00000
   38     36.13209    0.00000     36.32580    0.00000
   39     37.76279    0.00000     38.15661    0.00000
   40     38.57430    0.00000     38.81874    0.00000
   41     42.24136    0.00000     42.51822    0.00000
   42     43.17943    0.00000     43.46634    0.00000
   43     44.94303    0.00000     45.17050    0.00000
   44     46.00213    0.00000     46.23427    0.00000
   45     48.17390    0.00000     48.55232    0.00000
   46     48.78965    0.00000     49.04614    0.00000
   47     49.83793    0.00000     50.29624    0.00000
   48     50.54186    0.00000     50.66149    0.00000
   49     53.43537    0.00000     53.71340    0.00000
   50     55.22318    0.00000     55.80454    0.00000
   51     61.47991    0.00000     61.72862    0.00000
   52     65.24458    0.00000     65.87991    0.00000
   53     68.06443    0.00000     68.39879    0.00000

Gap: 4.221 eV
Transition (v -> c):
  (s=1, k=0, n=7, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.009     0.009   0.0% |
LCAO WFS Initialize:                   2.013     0.074   0.1% |
 Hamiltonian:                          1.939     0.003   0.0% |
  Atomic:                              0.044     0.044   0.1% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.036     0.036   0.1% |
  Hartree integrate/restrict:          0.048     0.048   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.277     0.015   0.0% |
   Communicate bwd 0:                  0.018     0.018   0.0% |
   Communicate bwd 1:                  0.073     0.073   0.1% |
   Communicate fwd 0:                  0.072     0.072   0.1% |
   Communicate fwd 1:                  0.020     0.020   0.0% |
   fft:                                0.029     0.029   0.1% |
   fft2:                               0.050     0.050   0.1% |
  XC 3D grid:                          1.522     1.522   3.0% ||
  vbar:                                0.004     0.004   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            45.395     0.005   0.0% |
 Direct Minimisation step:            43.289     0.003   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.470     0.008   0.0% |
   Construct Gradient Matrix:          0.004     0.004   0.0% |
   DenseAtomicCorrection:              0.004     0.004   0.0% |
   Distribute overlap matrix:          0.124     0.124   0.2% |
   Potential matrix:                   0.326     0.326   0.7% |
   Residual:                           0.003     0.003   0.0% |
  Get Search Direction:                0.005     0.005   0.0% |
  LCAO eigensolver:                    0.023     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.006     0.006   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.016     0.016   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.034     0.004   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Pade Approximants:                  0.029     0.029   0.1% |
  Update Kohn-Sham energy:            42.752     0.000   0.0% |
   Density:                            1.976     0.000   0.0% |
    Atomic density matrices:           0.165     0.165   0.3% |
    Mix:                               1.444     1.444   2.9% ||
    Multipole moments:                 0.004     0.004   0.0% |
    Normalize:                         0.009     0.009   0.0% |
    Pseudo density:                    0.355     0.027   0.1% |
     Calculate density matrix:         0.003     0.003   0.0% |
     Construct density:                0.325     0.325   0.6% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       40.776     0.047   0.1% |
    Atomic:                            0.923     0.923   1.8% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.040     0.040   0.1% |
    Communicate:                       0.763     0.763   1.5% ||
    Hartree integrate/restrict:        0.980     0.980   2.0% ||
    New Kinetic Energy:                0.004     0.002   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           5.954     0.326   0.6% |
     Communicate bwd 0:                0.443     0.443   0.9% |
     Communicate bwd 1:                1.616     1.616   3.2% ||
     Communicate fwd 0:                1.508     1.508   3.0% ||
     Communicate fwd 1:                0.421     0.421   0.8% |
     fft:                              0.597     0.597   1.2% |
     fft2:                             1.043     1.043   2.1% ||
    XC 3D grid:                       31.981    31.981  63.8% |-------------------------|
    vbar:                              0.083     0.083   0.2% |
 Get canonical representation:         2.101     0.000   0.0% |
  LCAO eigensolver:                    0.024     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.006     0.006   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.016     0.016   0.0% |
  Update Kohn-Sham energy:             2.077     0.000   0.0% |
   Density:                            0.094     0.000   0.0% |
    Atomic density matrices:           0.007     0.007   0.0% |
    Mix:                               0.069     0.069   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.017     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.016     0.016   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.983     0.002   0.0% |
    Atomic:                            0.044     0.044   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.036     0.036   0.1% |
    Hartree integrate/restrict:        0.047     0.047   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.287     0.015   0.0% |
     Communicate bwd 0:                0.021     0.021   0.0% |
     Communicate bwd 1:                0.078     0.078   0.2% |
     Communicate fwd 0:                0.074     0.074   0.1% |
     Communicate fwd 1:                0.020     0.020   0.0% |
     fft:                              0.029     0.029   0.1% |
     fft2:                             0.050     0.050   0.1% |
    XC 3D grid:                        1.562     1.562   3.1% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.527     0.527   1.1% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.161     2.161   4.3% |-|
-------------------------------------------------------------
Total:                                          50.111 100.0%

Memory usage: 414.69 MiB
Date: Sat Feb 16 22:22:41 2019
