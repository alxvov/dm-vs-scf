
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:49:16 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

B-setup:
  name: Boron
  id: 6e91439235a05fd7549e31636e8f777c
  Z: 5
  valence: 3
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/B.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.38(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)    -9.443   0.635
    2p(1.00)    -3.606   0.635
    *s          17.768   0.635
    *p          23.605   0.635
    *d           0.000   0.635

  LCAO basis set for B:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/B.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.6719 Bohr: 2s-sz confined orbital
      l=1, rc=8.2188 Bohr: 2p-sz confined orbital
      l=0, rc=3.7500 Bohr: 2s-dz split-valence wave
      l=1, rc=4.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=8.2188 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -8811.731923

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 108*108*92 grid
  Fine grid: 216*216*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 50.66 MiB
    Density: 32.26 MiB
      Arrays: 25.78 MiB
      Localized functions: 0.70 MiB
      Mixer: 5.79 MiB
    Hamiltonian: 16.92 MiB
      Arrays: 16.86 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.48 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.43 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 16
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |               F                       |  
 |         |           F  B F                      |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 B      8.144678    7.660880    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 F      8.144678    8.982640    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 F      9.289356    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.289356    0.000000    0.000000   108     0.1508
  2. axis:    no     0.000000   15.982640    0.000000   108     0.1480
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  16.289356  15.982640  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1503

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:49:19   +inf   +inf   -21.983451    0      1      
iter:   2  01:49:20  +0.45  -1.24   -21.592339    0      1      
iter:   3  01:49:22  +0.07  -1.43   -21.558812    0      1      
iter:   4  01:49:23  -1.11  -1.71   -21.567589    0      1      
iter:   5  01:49:24  -2.59  -2.91   -21.567346    0      1      
iter:   6  01:49:25  -3.96  -3.26   -21.567322    0      1      
iter:   7  01:49:26  -3.43  -3.43   -21.567304    0      1      
iter:   8  01:49:28  -5.95  -4.02   -21.567304    0      1      
iter:   9  01:49:29  -5.34  -4.27   -21.567304    0      1      
iter:  10  01:49:30  -7.27  -5.10   -21.567304    0      1      
iter:  11  01:49:31  -8.19  -5.24   -21.567303    0      1      
iter:  12  01:49:33  -8.39  -5.42   -21.567303    0      1      
iter:  13  01:49:34 -10.00  -5.84   -21.567303    0      1      
iter:  14  01:49:35  -9.29  -6.02   -21.567303    0      1      
iter:  15  01:49:36 -11.13  -6.58   -21.567303    0      1      

Converged after 15 iterations.

Dipole moment: (0.000000, -0.000022, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -8811.731923)

Kinetic:        +29.294089
Potential:      -28.447552
External:        +0.000000
XC:             -22.728126
Entropy (-ST):   +0.000000
Local:           +0.314286
--------------------------
Free energy:    -21.567303
Extrapolated:   -21.567303

Fermi levels: -4.89512, -4.89512

 Band  Eigenvalues  Occupancy
    0    -31.57965    2.00000
    1    -30.58353    2.00000
    2    -30.58342    2.00000
    3    -14.54869    2.00000
    4    -13.48482    2.00000
    5    -13.48431    2.00000
    6    -12.53515    2.00000
    7    -10.71270    2.00000
    8    -10.71264    2.00000
    9    -10.50807    2.00000
   10    -10.50793    2.00000
   11     -9.58891    2.00000
   12     -0.20132    0.00000
   13      1.90476    0.00000
   14      5.12676    0.00000
   15      5.12696    0.00000

Gap: 9.388 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.006     0.006   0.0% |
LCAO WFS Initialize:                 1.240     0.035   0.2% |
 Hamiltonian:                        1.205     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.048     0.048   0.2% |
  Hartree integrate/restrict:        0.022     0.022   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.350     0.011   0.1% |
   Communicate bwd 0:                0.067     0.067   0.3% |
   Communicate bwd 1:                0.064     0.064   0.3% |
   Communicate fwd 0:                0.063     0.063   0.3% |
   Communicate fwd 1:                0.071     0.071   0.3% |
   fft:                              0.020     0.020   0.1% |
   fft2:                             0.053     0.053   0.3% |
  XC 3D grid:                        0.775     0.775   3.7% ||
  vbar:                              0.005     0.005   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          17.419     0.004   0.0% |
 Density:                            0.603     0.000   0.0% |
  Atomic density matrices:           0.021     0.021   0.1% |
  Mix:                               0.498     0.498   2.4% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.079     0.007   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.071     0.071   0.3% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       16.708     0.002   0.0% |
  Atomic:                            0.004     0.004   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
  Communicate:                       0.682     0.682   3.3% ||
  Hartree integrate/restrict:        0.313     0.313   1.5% ||
  Poisson:                           4.831     0.137   0.7% |
   Communicate bwd 0:                0.931     0.931   4.5% |-|
   Communicate bwd 1:                0.902     0.902   4.3% |-|
   Communicate fwd 0:                0.863     0.863   4.1% |-|
   Communicate fwd 1:                0.963     0.963   4.6% |-|
   fft:                              0.282     0.282   1.4% ||
   fft2:                             0.752     0.752   3.6% ||
  XC 3D grid:                       10.821    10.821  51.8% |--------------------|
  vbar:                              0.047     0.047   0.2% |
 LCAO eigensolver:                   0.104     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.015     0.015   0.1% |
  Orbital Layouts:                   0.009     0.009   0.0% |
  Potential matrix:                  0.075     0.075   0.4% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.351     0.351   1.7% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.881     1.881   9.0% |---|
-----------------------------------------------------------
Total:                                        20.901 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 01:49:36 2019
