
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:40:55 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4222.923922

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 120*116*104 grid
  Fine grid: 240*232*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*232*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 336.22 MiB
  Calculator: 94.85 MiB
    Density: 56.13 MiB
      Arrays: 46.94 MiB
      Localized functions: 1.34 MiB
      Mixer: 7.85 MiB
    Hamiltonian: 34.98 MiB
      Arrays: 34.88 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 3.73 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.14 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.57 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 13
Number of atomic orbitals: 97
Number of bands in calculation: 20
Bands to converge: occupied states only
Number of valence electrons: 25

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .--------------------------------------------.  
           /|                                            |  
          / |                                            |  
         /  |                                            |  
        /   |                                            |  
       /    |                                            |  
      /     |                                            |  
     /      |                                            |  
    /       |                                            |  
   /        |                                            |  
  /         |                                            |  
 *          |                                            |  
 |          |                                            |  
 |          |               H   H                        |  
 |          |           H    CC   H                      |  
 |          |            H    HHC                        |  
 |          |                                            |  
 |          |            H      H                        |  
 |          |                                            |  
 |          .--------------------------------------------.  
 |         /                                            /   
 |        /                                            /    
 |       /                                            /     
 |      /                                            /      
 |     /                                            /       
 |    /                                            /        
 |   /                                            /         
 |  /                                            /          
 | /                                            /           
 |/                                            /            
 *--------------------------------------------*             

Positions:
   0 C      9.128607    8.741086    8.285721    ( 0.0000,  0.0000,  1.0000)
   1 C      9.128607   10.219273    8.072926    ( 0.0000,  0.0000,  0.0000)
   2 C     10.408754    8.001993    8.072926    ( 0.0000,  0.0000,  0.0000)
   3 C      7.848460    8.001993    8.072926    ( 0.0000,  0.0000,  0.0000)
   4 H      9.128607   10.472582    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      8.241564   10.686855    8.511357    ( 0.0000,  0.0000,  0.0000)
   6 H     10.015650   10.686855    8.511357    ( 0.0000,  0.0000,  0.0000)
   7 H     10.628127    7.875338    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H     11.257214    8.536403    8.511357    ( 0.0000,  0.0000,  0.0000)
   9 H     10.370171    7.000000    8.511357    ( 0.0000,  0.0000,  0.0000)
  10 H      7.629087    7.875338    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.887043    7.000000    8.511357    ( 0.0000,  0.0000,  0.0000)
  12 H      7.000000    8.536403    8.511357    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.257214    0.000000    0.000000   120     0.1521
  2. axis:    no     0.000000   17.686855    0.000000   116     0.1525
  3. axis:    no     0.000000    0.000000   15.511357   104     0.1491

  Lengths:  18.257214  17.686855  15.511357
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1512

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  00:41:00   +inf   +inf   -69.820056    0      1        +1.0000
iter:   2  00:41:03  +0.96  -0.69   -67.592443    0      1        +1.0000
iter:   3  00:41:05  +0.75  -0.88   -66.699022    0      1        +1.0000
iter:   4  00:41:08  -0.18  -1.21   -66.583366    0      1        +1.0000
iter:   5  00:41:10  -1.83  -2.13   -66.583154    0      1        +1.0000
iter:   6  00:41:13  -2.18  -2.42   -66.582970    0      1        +1.0000
iter:   7  00:41:15  -2.48  -2.60   -66.582704    0      1        +1.0000
iter:   8  00:41:18  -3.81  -3.23   -66.582685    0      1        +1.0000
iter:   9  00:41:20  -4.12  -3.53   -66.582682    0      1        +1.0000
iter:  10  00:41:23  -5.05  -3.78   -66.582682    0      1        +1.0000
iter:  11  00:41:25  -5.78  -4.01   -66.582682    0      1        +1.0000
iter:  12  00:41:28  -5.77  -4.26   -66.582682    0      1        +1.0000
iter:  13  00:41:30  -6.81  -4.69   -66.582682    0      1        +1.0000
iter:  14  00:41:33  -7.24  -4.95   -66.582682    0      1        +1.0000
iter:  15  00:41:35  -8.16  -5.22   -66.582682    0      1        +1.0000
iter:  16  00:41:38  -8.60  -5.48   -66.582682    0      1        +1.0000
iter:  17  00:41:41  -9.24  -5.76   -66.582682    0      1        +1.0000
iter:  18  00:41:43  -9.98  -5.98   -66.582682    0      1        +1.0000
iter:  19  00:41:46 -10.11  -6.39   -66.582682    0      1        +1.0000

Converged after 19 iterations.

Dipole moment: (0.000000, -0.000092, -0.049153) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999994)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.305970)
   1 C  ( 0.000000,  0.000000, -0.012344)
   2 C  ( 0.000000,  0.000000, -0.012341)
   3 C  ( 0.000000,  0.000000, -0.012341)
   4 H  ( 0.000000,  0.000000,  0.022156)
   5 H  ( 0.000000,  0.000000,  0.004618)
   6 H  ( 0.000000,  0.000000,  0.004618)
   7 H  ( 0.000000,  0.000000,  0.022158)
   8 H  ( 0.000000,  0.000000,  0.004618)
   9 H  ( 0.000000,  0.000000,  0.004619)
  10 H  ( 0.000000,  0.000000,  0.022158)
  11 H  ( 0.000000,  0.000000,  0.004619)
  12 H  ( 0.000000,  0.000000,  0.004618)

Energy contributions relative to reference atoms: (reference = -4222.923922)

Kinetic:        +54.127659
Potential:      -60.508623
External:        +0.000000
XC:             -60.309796
Entropy (-ST):   +0.000000
Local:           +0.108077
--------------------------
Free energy:    -66.582682
Extrapolated:   -66.582682

Spin contamination: 0.061523 electrons
Fermi levels: -1.01265, -4.83904

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -19.81889    1.00000    -19.43736    1.00000
    1    -16.96577    1.00000    -16.90783    1.00000
    2    -16.96549    1.00000    -16.90755    1.00000
    3    -12.97881    1.00000    -12.56971    1.00000
    4    -10.73617    1.00000    -10.52641    1.00000
    5    -10.62369    1.00000    -10.52621    1.00000
    6    -10.62355    1.00000    -10.34423    1.00000
    7     -9.24892    1.00000     -9.13656    1.00000
    8     -9.24885    1.00000     -9.13648    1.00000
    9     -8.71923    1.00000     -8.70070    1.00000
   10     -8.13584    1.00000     -8.00514    1.00000
   11     -8.13572    1.00000     -8.00500    1.00000
   12     -3.66415    1.00000     -1.67308    0.00000
   13      1.63885    0.00000      1.70249    0.00000
   14      3.02043    0.00000      3.06621    0.00000
   15      3.02057    0.00000      3.06625    0.00000
   16      3.27214    0.00000      3.35429    0.00000
   17      3.27243    0.00000      3.35479    0.00000
   18      3.63540    0.00000      3.82639    0.00000
   19      4.38787    0.00000      4.50136    0.00000

Gap: 1.991 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.001     0.001   0.0% |
Basis functions set positions:       0.019     0.019   0.0% |
LCAO WFS Initialize:                 2.396     0.093   0.2% |
 Hamiltonian:                        2.303     0.002   0.0% |
  Atomic:                            0.085     0.007   0.0% |
   XC Correction:                    0.078     0.078   0.2% |
  Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
  Communicate:                       0.041     0.041   0.1% |
  Hartree integrate/restrict:        0.059     0.059   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.334     0.018   0.0% |
   Communicate bwd 0:                0.023     0.023   0.0% |
   Communicate bwd 1:                0.085     0.085   0.2% |
   Communicate fwd 0:                0.081     0.081   0.2% |
   Communicate fwd 1:                0.026     0.026   0.1% |
   fft:                              0.029     0.029   0.1% |
   fft2:                             0.070     0.070   0.1% |
  XC 3D grid:                        1.774     1.774   3.5% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.004     0.004   0.0% |
SCF-cycle:                          45.390     0.006   0.0% |
 Density:                            2.817     0.000   0.0% |
  Atomic density matrices:           0.448     0.448   0.9% |
  Mix:                               1.575     1.575   3.1% ||
  Multipole moments:                 0.004     0.004   0.0% |
  Normalize:                         0.009     0.009   0.0% |
  Pseudo density:                    0.782     0.028   0.1% |
   Calculate density matrix:         0.002     0.002   0.0% |
   Construct density:                0.751     0.751   1.5% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       41.248     0.046   0.1% |
  Atomic:                            1.538     0.127   0.3% |
   XC Correction:                    1.410     1.410   2.8% ||
  Calculate atomic Hamiltonians:     0.046     0.046   0.1% |
  Communicate:                       0.737     0.737   1.5% ||
  Hartree integrate/restrict:        1.029     1.029   2.0% ||
  Poisson:                           6.090     0.321   0.6% |
   Communicate bwd 0:                0.458     0.458   0.9% |
   Communicate bwd 1:                1.563     1.563   3.1% ||
   Communicate fwd 0:                1.461     1.461   2.9% ||
   Communicate fwd 1:                0.481     0.481   1.0% |
   fft:                              0.528     0.528   1.0% |
   fft2:                             1.278     1.278   2.5% ||
  XC 3D grid:                       31.683    31.683  62.9% |------------------------|
  vbar:                              0.080     0.080   0.2% |
 LCAO eigensolver:                   1.319     0.004   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.004     0.004   0.0% |
  Distribute overlap matrix:         0.379     0.379   0.8% |
  Orbital Layouts:                   0.061     0.061   0.1% |
  Potential matrix:                  0.859     0.859   1.7% ||
  Residual:                          0.012     0.012   0.0% |
ST tci:                              0.004     0.004   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.181     0.181   0.4% |
mktci:                               0.003     0.003   0.0% |
Other:                               2.379     2.379   4.7% |-|
-----------------------------------------------------------
Total:                                        50.376 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:41:46 2019
