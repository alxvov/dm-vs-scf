
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:50:26 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3080.970579

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 100*104*92 grid
  Fine grid: 200*208*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 200*208*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 60.41 MiB
    Density: 36.50 MiB
      Arrays: 30.89 MiB
      Localized functions: 0.46 MiB
      Mixer: 5.15 MiB
    Hamiltonian: 22.99 MiB
      Arrays: 22.95 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.92 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.90 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 31
Number of bands in calculation: 9
Bands to converge: occupied states only
Number of valence electrons: 11

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .------------------------------------.  
          /|                                    |  
         / |                                    |  
        /  |                                    |  
       /   |                                    |  
      /    |                                    |  
     /     |                                    |  
    /      |                                    |  
   /       |                                    |  
  /        |                                    |  
 *         |                                    |  
 |         |                                    |  
 |         |                                    |  
 |         |            H C                     |  
 |         |             O                      |  
 |         |                                    |  
 |         |                                    |  
 |         .------------------------------------.  
 |        /                                    /   
 |       /                                    /    
 |      /                                    /     
 |     /                                    /      
 |    /                                    /       
 |   /                                    /        
 |  /                                    /         
 | /                                    /          
 |/                                    /           
 *------------------------------------*            

Positions:
   0 C      7.938395    8.190840    7.000000    ( 0.0000,  0.0000,  1.0000)
   1 O      7.938395    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    8.808669    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.938395    0.000000    0.000000   100     0.1494
  2. axis:    no     0.000000   15.808669    0.000000   104     0.1520
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  14.938395  15.808669  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1512

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  00:50:30   +inf   +inf   -16.185046    0      1        +1.0000
iter:   2  00:50:32  +1.20  -0.95   -16.192560    0      1        +1.0000
iter:   3  00:50:33  -0.08  -1.13   -16.135213    0      1        +1.0000
iter:   4  00:50:35  +0.00  -1.36   -16.124232    0      1        +1.0000
iter:   5  00:50:37  -0.85  -2.07   -16.122158    0      1        +1.0000
iter:   6  00:50:38  -1.96  -2.61   -16.122186    0      1        +1.0000
iter:   7  00:50:40  -2.90  -2.79   -16.122174    0      1        +1.0000
iter:   8  00:50:42  -2.81  -3.01   -16.122175    0      1        +1.0000
iter:   9  00:50:43  -4.04  -3.55   -16.122176    0      1        +1.0000
iter:  10  00:50:45  -4.77  -4.01   -16.122177    0      1        +1.0000
iter:  11  00:50:47  -5.40  -4.44   -16.122178    0      1        +1.0000
iter:  12  00:50:48  -5.91  -4.79   -16.122178    0      1        +1.0000
iter:  13  00:50:50  -6.50  -5.08   -16.122178    0      1        +1.0000
iter:  14  00:50:52  -7.09  -5.38   -16.122178    0      1        +1.0000
iter:  15  00:50:53  -7.70  -5.58   -16.122178    0      1        +1.0000
iter:  16  00:50:55  -8.01  -5.70   -16.122178    0      1        +1.0000
iter:  17  00:50:57  -8.44  -5.93   -16.122178    0      1        +1.0000
iter:  18  00:50:58  -8.81  -6.12   -16.122178    0      1        +1.0000
iter:  19  00:51:00  -8.94  -6.33   -16.122178    0      1        +1.0000
iter:  20  00:51:02  -9.53  -6.77   -16.122178    0      1        +1.0000
iter:  21  00:51:03 -10.08  -7.16   -16.122178    0      1        +1.0000

Converged after 21 iterations.

Dipole moment: (-0.173917, 0.215085, -0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999997)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.192519)
   1 O  ( 0.000000,  0.000000,  0.211038)
   2 H  ( 0.000000,  0.000000,  0.060912)

Energy contributions relative to reference atoms: (reference = -3080.970579)

Kinetic:        +14.353508
Potential:      -15.384479
External:        +0.000000
XC:             -15.170492
Entropy (-ST):   +0.000000
Local:           +0.079286
--------------------------
Free energy:    -16.122178
Extrapolated:   -16.122178

Spin contamination: 0.015061 electrons
Fermi levels: -3.43807, -5.96593

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -27.52201    1.00000    -27.16951    1.00000
    1    -14.98283    1.00000    -14.41479    1.00000
    2    -11.83993    1.00000    -11.34134    1.00000
    3    -10.45535    1.00000    -10.23144    1.00000
    4    -10.13703    1.00000     -9.12555    1.00000
    5     -4.56918    1.00000     -2.80631    0.00000
    6     -2.30697    0.00000     -1.83245    0.00000
    7      2.45244    0.00000      2.93546    0.00000
    8      6.17736    0.00000      6.51668    0.00000

Gap: 4.556 eV
Transition (v -> c):
  (s=1, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.005     0.005   0.0% |
LCAO WFS Initialize:                 1.662     0.055   0.1% |
 Hamiltonian:                        1.607     0.002   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.079     0.079   0.2% |
  Hartree integrate/restrict:        0.037     0.037   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.313     0.009   0.0% |
   Communicate bwd 0:                0.060     0.060   0.2% |
   Communicate bwd 1:                0.057     0.057   0.2% |
   Communicate fwd 0:                0.058     0.058   0.2% |
   Communicate fwd 1:                0.062     0.062   0.2% |
   fft:                              0.021     0.021   0.1% |
   fft2:                             0.046     0.046   0.1% |
  XC 3D grid:                        1.171     1.171   3.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          33.399     0.006   0.0% |
 Density:                            1.311     0.000   0.0% |
  Atomic density matrices:           0.073     0.073   0.2% |
  Mix:                               1.118     1.118   3.0% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.006     0.006   0.0% |
  Pseudo density:                    0.112     0.019   0.1% |
   Calculate density matrix:         0.002     0.002   0.0% |
   Construct density:                0.091     0.091   0.2% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       31.901     0.034   0.1% |
  Atomic:                            0.006     0.006   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
  Communicate:                       1.576     1.576   4.2% |-|
  Hartree integrate/restrict:        0.763     0.763   2.0% ||
  Poisson:                           6.258     0.167   0.4% |
   Communicate bwd 0:                1.202     1.202   3.2% ||
   Communicate bwd 1:                1.175     1.175   3.1% ||
   Communicate fwd 0:                1.111     1.111   3.0% ||
   Communicate fwd 1:                1.254     1.254   3.4% ||
   fft:                              0.414     0.414   1.1% |
   fft2:                             0.934     0.934   2.5% ||
  XC 3D grid:                       23.197    23.197  62.1% |------------------------|
  vbar:                              0.061     0.061   0.2% |
 LCAO eigensolver:                   0.180     0.003   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.064     0.064   0.2% |
  Orbital Layouts:                   0.011     0.011   0.0% |
  Potential matrix:                  0.095     0.095   0.3% |
  Residual:                          0.006     0.006   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.502     0.502   1.3% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.786     1.786   4.8% |-|
-----------------------------------------------------------
Total:                                        37.357 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:51:03 2019
