
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:42:53 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -5134.313042

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 108*112*92 grid
  Fine grid: 216*224*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*224*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 52.44 MiB
    Density: 33.50 MiB
      Arrays: 26.74 MiB
      Localized functions: 0.76 MiB
      Mixer: 6.00 MiB
    Hamiltonian: 17.55 MiB
      Arrays: 17.49 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.39 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.35 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 49
Number of bands in calculation: 14
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |              C  O                     |  
 |         |           H                           |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 O      7.000000    7.898702    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C      8.040945    8.759083    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 O      9.210317    8.438875    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.391375    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      7.663098    9.788101    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.210317    0.000000    0.000000   108     0.1501
  2. axis:    no     0.000000   16.788101    0.000000   112     0.1499
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  16.210317  16.788101  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:42:57   +inf   +inf   -29.616524    0      1      
iter:   2  00:42:58  +1.09  -0.88   -28.618373    0      1      
iter:   3  00:43:00  +0.19  -1.10   -28.445571    0      1      
iter:   4  00:43:01  -0.14  -1.32   -28.404596    0      1      
iter:   5  00:43:02  -1.37  -1.97   -28.401815    0      1      
iter:   6  00:43:03  -1.77  -2.42   -28.400525    0      1      
iter:   7  00:43:05  -2.63  -2.86   -28.400415    0      1      
iter:   8  00:43:06  -3.34  -3.19   -28.400395    0      1      
iter:   9  00:43:07  -4.06  -3.51   -28.400393    0      1      
iter:  10  00:43:09  -4.51  -3.75   -28.400393    0      1      
iter:  11  00:43:10  -5.38  -4.10   -28.400393    0      1      
iter:  12  00:43:11  -5.66  -4.43   -28.400393    0      1      
iter:  13  00:43:12  -6.25  -4.77   -28.400393    0      1      
iter:  14  00:43:14  -6.80  -4.99   -28.400393    0      1      
iter:  15  00:43:15  -7.42  -5.22   -28.400393    0      1      
iter:  16  00:43:16  -7.98  -5.47   -28.400393    0      1      
iter:  17  00:43:18  -8.40  -5.74   -28.400393    0      1      
iter:  18  00:43:19  -9.12  -6.08   -28.400393    0      1      
iter:  19  00:43:20  -9.16  -6.33   -28.400393    0      1      
iter:  20  00:43:21 -10.10  -6.64   -28.400393    0      1      

Converged after 20 iterations.

Dipole moment: (-0.289192, -0.097758, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5134.313042)

Kinetic:        +25.398444
Potential:      -26.654441
External:        +0.000000
XC:             -27.289215
Entropy (-ST):   +0.000000
Local:           +0.144820
--------------------------
Free energy:    -28.400393
Extrapolated:   -28.400393

Fermi levels: -3.77365, -3.77365

 Band  Eigenvalues  Occupancy
    0    -27.78020    2.00000
    1    -25.46974    2.00000
    2    -16.42422    2.00000
    3    -12.55119    2.00000
    4    -11.64187    2.00000
    5    -10.76587    2.00000
    6     -9.54713    2.00000
    7     -7.72030    2.00000
    8     -6.44996    2.00000
    9     -1.09734    0.00000
   10      1.16941    0.00000
   11      2.75997    0.00000
   12      5.98265    0.00000
   13      6.89826    0.00000

Gap: 5.353 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.007     0.007   0.0% |
LCAO WFS Initialize:                 1.297     0.038   0.1% |
 Hamiltonian:                        1.259     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.049     0.049   0.2% |
  Hartree integrate/restrict:        0.026     0.026   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.364     0.010   0.0% |
   Communicate bwd 0:                0.069     0.069   0.2% |
   Communicate bwd 1:                0.067     0.067   0.2% |
   Communicate fwd 0:                0.065     0.065   0.2% |
   Communicate fwd 1:                0.074     0.074   0.3% |
   fft:                              0.022     0.022   0.1% |
   fft2:                             0.055     0.055   0.2% |
  XC 3D grid:                        0.814     0.814   2.9% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          24.470     0.005   0.0% |
 Density:                            0.859     0.000   0.0% |
  Atomic density matrices:           0.024     0.024   0.1% |
  Mix:                               0.704     0.704   2.5% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.004     0.004   0.0% |
  Pseudo density:                    0.124     0.010   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.112     0.112   0.4% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       23.454     0.002   0.0% |
  Atomic:                            0.006     0.006   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.011     0.011   0.0% |
  Communicate:                       0.943     0.943   3.3% ||
  Hartree integrate/restrict:        0.457     0.457   1.6% ||
  Poisson:                           6.783     0.180   0.6% |
   Communicate bwd 0:                1.306     1.306   4.6% |-|
   Communicate bwd 1:                1.275     1.275   4.5% |-|
   Communicate fwd 0:                1.186     1.186   4.2% |-|
   Communicate fwd 1:                1.348     1.348   4.8% |-|
   fft:                              0.425     0.425   1.5% ||
   fft2:                             1.064     1.064   3.8% |-|
  XC 3D grid:                       15.186    15.186  53.8% |---------------------|
  vbar:                              0.066     0.066   0.2% |
 LCAO eigensolver:                   0.151     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.009     0.009   0.0% |
  Orbital Layouts:                   0.011     0.011   0.0% |
  Potential matrix:                  0.125     0.125   0.4% |
  Residual:                          0.004     0.004   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.503     0.503   1.8% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.931     1.931   6.8% |--|
-----------------------------------------------------------
Total:                                        28.212 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:43:21 2019
