
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 02:25:07 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4106.684927

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*96 grid
  Fine grid: 208*208*192 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 48.35 MiB
    Density: 31.11 MiB
      Arrays: 24.93 MiB
      Localized functions: 0.58 MiB
      Mixer: 5.60 MiB
    Hamiltonian: 16.35 MiB
      Arrays: 16.31 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.89 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.87 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 36
Number of bands in calculation: 10
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                H                     |  
 |         |           H  O                       |  
 |         |             O                        |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 O      7.839547    8.614810    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 O      7.839547    7.146694    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      8.679094    8.761504    7.474751    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.000000    7.474751    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.679094    0.000000    0.000000   104     0.1508
  2. axis:    no     0.000000   15.761504    0.000000   104     0.1516
  3. axis:    no     0.000000    0.000000   14.474751    96     0.1508

  Lengths:  15.679094  15.761504  14.474751
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1510

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  02:25:10   +inf   +inf   -18.009638    0      1      
iter:   2  02:25:11  +0.75  -0.86   -17.354381    0      1      
iter:   3  02:25:12  +1.09  -1.01   -16.990629    0      1      
iter:   4  02:25:13  +0.06  -1.54   -16.953858    0      1      
iter:   5  02:25:14  -1.54  -2.22   -16.952155    0      1      
iter:   6  02:25:16  -1.50  -2.48   -16.951505    0      1      
iter:   7  02:25:17  -2.54  -3.04   -16.951435    0      1      
iter:   8  02:25:18  -4.14  -3.47   -16.951430    0      1      
iter:   9  02:25:19  -4.32  -3.73   -16.951428    0      1      
iter:  10  02:25:20  -6.37  -4.22   -16.951428    0      1      
iter:  11  02:25:22  -5.97  -4.34   -16.951428    0      1      
iter:  12  02:25:23  -7.04  -4.93   -16.951428    0      1      
iter:  13  02:25:24  -7.67  -5.26   -16.951428    0      1      
iter:  14  02:25:25  -7.53  -5.43   -16.951428    0      1      
iter:  15  02:25:26  -9.24  -5.81   -16.951428    0      1      
iter:  16  02:25:28  -8.66  -5.99   -16.951428    0      1      
iter:  17  02:25:29 -10.37  -6.45   -16.951428    0      1      

Converged after 17 iterations.

Dipole moment: (0.000000, -0.000000, 0.306527) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4106.684927)

Kinetic:        +19.354287
Potential:      -18.446482
External:        +0.000000
XC:             -18.008646
Entropy (-ST):   +0.000000
Local:           +0.149412
--------------------------
Free energy:    -16.951428
Extrapolated:   -16.951428

Fermi levels: -3.20625, -3.20625

 Band  Eigenvalues  Occupancy
    0    -27.54493    2.00000
    1    -22.02325    2.00000
    2    -12.68428    2.00000
    3    -11.59887    2.00000
    4     -9.61589    2.00000
    5     -7.11062    2.00000
    6     -5.68041    2.00000
    7     -0.73209    0.00000
    8      1.29918    0.00000
    9      2.42092    0.00000

Gap: 4.948 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.005     0.005   0.0% |
LCAO WFS Initialize:                 1.188     0.031   0.1% |
 Hamiltonian:                        1.157     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.049     0.049   0.2% |
  Hartree integrate/restrict:        0.022     0.022   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.334     0.009   0.0% |
   Communicate bwd 0:                0.064     0.064   0.3% |
   Communicate bwd 1:                0.066     0.066   0.3% |
   Communicate fwd 0:                0.059     0.059   0.3% |
   Communicate fwd 1:                0.069     0.069   0.3% |
   fft:                              0.022     0.022   0.1% |
   fft2:                             0.045     0.045   0.2% |
  XC 3D grid:                        0.747     0.747   3.4% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          19.034     0.004   0.0% |
 Density:                            0.616     0.000   0.0% |
  Atomic density matrices:           0.029     0.029   0.1% |
  Mix:                               0.525     0.525   2.4% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.057     0.007   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.049     0.049   0.2% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       18.338     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.007     0.007   0.0% |
  Communicate:                       0.781     0.781   3.5% ||
  Hartree integrate/restrict:        0.357     0.357   1.6% ||
  Poisson:                           5.218     0.133   0.6% |
   Communicate bwd 0:                1.026     1.026   4.6% |-|
   Communicate bwd 1:                1.011     1.011   4.5% |-|
   Communicate fwd 0:                0.936     0.936   4.2% |-|
   Communicate fwd 1:                1.035     1.035   4.7% |-|
   fft:                              0.360     0.360   1.6% ||
   fft2:                             0.717     0.717   3.2% ||
  XC 3D grid:                       11.916    11.916  53.6% |--------------------|
  vbar:                              0.052     0.052   0.2% |
 LCAO eigensolver:                   0.075     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.012     0.012   0.1% |
  Orbital Layouts:                   0.007     0.007   0.0% |
  Potential matrix:                  0.052     0.052   0.2% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.201     0.201   0.9% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.785     1.785   8.0% |--|
-----------------------------------------------------------
Total:                                        22.217 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 02:25:29 2019
