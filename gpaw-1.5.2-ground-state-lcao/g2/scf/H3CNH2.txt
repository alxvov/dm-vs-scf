
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:53:50 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2571.800431

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*108*104 grid
  Fine grid: 208*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 55.34 MiB
    Density: 35.08 MiB
      Arrays: 28.09 MiB
      Localized functions: 0.69 MiB
      Mixer: 6.31 MiB
    Hamiltonian: 18.43 MiB
      Arrays: 18.37 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.83 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.78 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 51
Number of bands in calculation: 13
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |                H                    |  
 |         |            H C                      |  
 |         |             N  H                    |  
 |         |            H                        |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.993471    8.803855    7.880670    ( 0.0000,  0.0000,  0.0000)
   1 N      7.993471    7.339817    7.880670    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    9.275625    7.880670    ( 0.0000,  0.0000,  0.0000)
   3 H      7.483554    7.000000    8.693040    ( 0.0000,  0.0000,  0.0000)
   4 H      7.483554    7.000000    7.068300    ( 0.0000,  0.0000,  0.0000)
   5 H      8.534498    9.156160    8.761340    ( 0.0000,  0.0000,  0.0000)
   6 H      8.534498    9.156160    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.534498    0.000000    0.000000   104     0.1494
  2. axis:    no     0.000000   16.275625    0.000000   108     0.1507
  3. axis:    no     0.000000    0.000000   15.761340   104     0.1516

  Lengths:  15.534498  16.275625  15.761340
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1505

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:53:54   +inf   +inf   -36.665574    0      1      
iter:   2  00:53:55  +1.27  -0.68   -35.117860    0      1      
iter:   3  00:53:56  +0.94  -0.87   -34.627634    0      1      
iter:   4  00:53:58  +0.29  -1.16   -34.541298    0      1      
iter:   5  00:53:59  -1.39  -2.04   -34.539447    0      1      
iter:   6  00:54:01  -1.54  -2.32   -34.538727    0      1      
iter:   7  00:54:02  -1.94  -2.64   -34.538497    0      1      
iter:   8  00:54:03  -4.55  -3.21   -34.538499    0      1      
iter:   9  00:54:05  -3.77  -3.37   -34.538499    0      1      
iter:  10  00:54:06  -5.50  -4.10   -34.538499    0      1      
iter:  11  00:54:07  -6.23  -4.41   -34.538499    0      1      
iter:  12  00:54:09  -6.48  -4.72   -34.538499    0      1      
iter:  13  00:54:10  -7.71  -5.25   -34.538499    0      1      
iter:  14  00:54:11  -8.22  -5.62   -34.538499    0      1      
iter:  15  00:54:13  -9.33  -6.11   -34.538499    0      1      
iter:  16  00:54:14  -9.64  -6.41   -34.538499    0      1      
iter:  17  00:54:16 -11.08  -6.80   -34.538499    0      1      

Converged after 17 iterations.

Dipole moment: (-0.240009, 0.048506, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2571.800431)

Kinetic:        +28.985194
Potential:      -32.001406
External:        +0.000000
XC:             -31.647269
Entropy (-ST):   +0.000000
Local:           +0.124983
--------------------------
Free energy:    -34.538499
Extrapolated:   -34.538499

Fermi levels: -1.70417, -1.70417

 Band  Eigenvalues  Occupancy
    0    -21.58600    2.00000
    1    -16.11594    2.00000
    2    -11.81902    2.00000
    3    -10.57344    2.00000
    4     -9.56512    2.00000
    5     -8.69491    2.00000
    6     -5.09708    2.00000
    7      1.68875    0.00000
    8      2.84353    0.00000
    9      3.30329    0.00000
   10      3.95027    0.00000
   11      4.50077    0.00000
   12      5.54182    0.00000

Gap: 6.786 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.010     0.010   0.0% |
LCAO WFS Initialize:                 1.348     0.038   0.1% |
 Hamiltonian:                        1.310     0.000   0.0% |
  Atomic:                            0.027     0.027   0.1% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.021     0.021   0.1% |
  Hartree integrate/restrict:        0.030     0.030   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.374     0.010   0.0% |
   Communicate bwd 0:                0.073     0.073   0.3% |
   Communicate bwd 1:                0.068     0.068   0.3% |
   Communicate fwd 0:                0.069     0.069   0.3% |
   Communicate fwd 1:                0.076     0.076   0.3% |
   fft:                              0.022     0.022   0.1% |
   fft2:                             0.056     0.056   0.2% |
  XC 3D grid:                        0.849     0.849   3.3% ||
  vbar:                              0.005     0.005   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          21.796     0.004   0.0% |
 Density:                            0.812     0.000   0.0% |
  Atomic density matrices:           0.079     0.079   0.3% |
  Mix:                               0.598     0.598   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.129     0.008   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.120     0.120   0.5% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       20.765     0.002   0.0% |
  Atomic:                            0.432     0.431   1.7% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.032     0.032   0.1% |
  Communicate:                       0.333     0.333   1.3% ||
  Hartree integrate/restrict:        0.388     0.388   1.5% ||
  Poisson:                           6.013     0.168   0.7% |
   Communicate bwd 0:                1.174     1.174   4.6% |-|
   Communicate bwd 1:                1.123     1.123   4.4% |-|
   Communicate fwd 0:                1.091     1.091   4.3% |-|
   Communicate fwd 1:                1.213     1.213   4.7% |-|
   fft:                              0.349     0.349   1.4% ||
   fft2:                             0.894     0.894   3.5% ||
  XC 3D grid:                       13.507    13.507  52.6% |--------------------|
  vbar:                              0.058     0.058   0.2% |
 LCAO eigensolver:                   0.215     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.064     0.064   0.2% |
  Orbital Layouts:                   0.010     0.010   0.0% |
  Potential matrix:                  0.134     0.134   0.5% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.521     0.521   2.0% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.984     1.984   7.7% |--|
-----------------------------------------------------------
Total:                                        25.665 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:54:16 2019
