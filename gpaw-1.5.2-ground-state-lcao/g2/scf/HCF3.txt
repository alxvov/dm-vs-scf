
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:24:14 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -9182.360633

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 108*104*104 grid
  Fine grid: 216*208*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [1, 2, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 54.86 MiB
    Density: 35.16 MiB
      Arrays: 28.09 MiB
      Localized functions: 0.76 MiB
      Mixer: 6.31 MiB
    Hamiltonian: 18.43 MiB
      Arrays: 18.37 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.26 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.21 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 57
Number of bands in calculation: 17
Bands to converge: occupied states only
Number of valence electrons: 26

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |              H                        |  
 |         |              CF                       |  
 |         |           F    F                      |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 C      8.089633    7.629100    7.469750    ( 0.0000,  0.0000,  0.0000)
   1 H      8.089633    7.629100    8.558212    ( 0.0000,  0.0000,  0.0000)
   2 F      8.089633    8.887300    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 F      9.179266    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.179266    0.000000    0.000000   108     0.1498
  2. axis:    no     0.000000   15.887300    0.000000   104     0.1528
  3. axis:    no     0.000000    0.000000   15.558212   104     0.1496

  Lengths:  16.179266  15.887300  15.558212
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:24:17   +inf   +inf   -24.217031    0      1      
iter:   2  01:24:19  +0.65  -1.13   -23.555049    0      1      
iter:   3  01:24:20  +0.55  -1.30   -23.354249    0      1      
iter:   4  01:24:21  -0.40  -1.70   -23.328402    0      1      
iter:   5  01:24:22  -2.37  -2.51   -23.327523    0      1      
iter:   6  01:24:24  -2.29  -2.72   -23.327006    0      1      
iter:   7  01:24:25  -2.62  -3.06   -23.326865    0      1      
iter:   8  01:24:26  -4.91  -3.65   -23.326859    0      1      
iter:   9  01:24:27  -4.62  -3.92   -23.326857    0      1      
iter:  10  01:24:28  -6.00  -4.41   -23.326857    0      1      
iter:  11  01:24:30  -6.60  -4.61   -23.326857    0      1      
iter:  12  01:24:31  -6.47  -4.80   -23.326857    0      1      
iter:  13  01:24:32  -7.85  -5.24   -23.326857    0      1      
iter:  14  01:24:33  -8.00  -5.53   -23.326857    0      1      
iter:  15  01:24:35  -8.96  -5.95   -23.326857    0      1      
iter:  16  01:24:36  -9.23  -6.18   -23.326857    0      1      
iter:  17  01:24:37 -10.28  -6.47   -23.326857    0      1      

Converged after 17 iterations.

Dipole moment: (-0.000000, 0.000058, 0.264109) |e|*Ang

Energy contributions relative to reference atoms: (reference = -9182.360633)

Kinetic:        +31.617238
Potential:      -29.257761
External:        +0.000000
XC:             -26.009338
Entropy (-ST):   +0.000000
Local:           +0.323004
--------------------------
Free energy:    -23.326857
Extrapolated:   -23.326857

Fermi levels: -3.39849, -3.39849

 Band  Eigenvalues  Occupancy
    0    -32.75103    2.00000
    1    -30.42249    2.00000
    2    -30.42243    2.00000
    3    -17.54560    2.00000
    4    -14.54141    2.00000
    5    -14.20558    2.00000
    6    -14.20534    2.00000
    7    -10.70929    2.00000
    8    -10.70889    2.00000
    9     -9.77542    2.00000
   10     -9.77517    2.00000
   11     -9.18378    2.00000
   12     -9.00497    2.00000
   13      2.20799    0.00000
   14      2.97964    0.00000
   15      2.98029    0.00000
   16      4.32751    0.00000

Gap: 11.213 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.007     0.007   0.0% |
LCAO WFS Initialize:                 1.227     0.037   0.2% |
 Hamiltonian:                        1.190     0.000   0.0% |
  Atomic:                            0.047     0.047   0.2% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.001     0.001   0.0% |
  Hartree integrate/restrict:        0.023     0.023   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.268     0.015   0.1% |
   Communicate bwd 0:                0.020     0.020   0.1% |
   Communicate bwd 1:                0.069     0.069   0.3% |
   Communicate fwd 0:                0.066     0.066   0.3% |
   Communicate fwd 1:                0.024     0.024   0.1% |
   fft:                              0.025     0.025   0.1% |
   fft2:                             0.048     0.048   0.2% |
  XC 3D grid:                        0.841     0.841   3.6% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          19.726     0.004   0.0% |
 Density:                            0.748     0.000   0.0% |
  Atomic density matrices:           0.021     0.021   0.1% |
  Mix:                               0.594     0.594   2.5% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.127     0.009   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.117     0.117   0.5% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       18.816     0.002   0.0% |
  Atomic:                            0.761     0.761   3.2% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.031     0.031   0.1% |
  Communicate:                       0.015     0.015   0.1% |
  Hartree integrate/restrict:        0.370     0.370   1.6% ||
  Poisson:                           4.189     0.232   1.0% |
   Communicate bwd 0:                0.297     0.297   1.3% ||
   Communicate bwd 1:                1.117     1.117   4.8% |-|
   Communicate fwd 0:                1.042     1.042   4.4% |-|
   Communicate fwd 1:                0.335     0.335   1.4% ||
   fft:                              0.394     0.394   1.7% ||
   fft2:                             0.772     0.772   3.3% ||
  XC 3D grid:                       13.390    13.390  57.1% |----------------------|
  vbar:                              0.057     0.057   0.2% |
 LCAO eigensolver:                   0.157     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.011     0.011   0.0% |
  Orbital Layouts:                   0.011     0.011   0.0% |
  Potential matrix:                  0.127     0.127   0.5% |
  Residual:                          0.004     0.004   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.515     0.515   2.2% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.991     1.991   8.5% |--|
-----------------------------------------------------------
Total:                                        23.471 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:24:37 2019
