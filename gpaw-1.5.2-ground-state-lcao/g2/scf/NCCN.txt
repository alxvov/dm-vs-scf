
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:45:07 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Reference energy: -5018.699239

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*120 grid
  Fine grid: 184*184*240 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 48.18 MiB
    Density: 30.49 MiB
      Arrays: 24.38 MiB
      Localized functions: 0.64 MiB
      Mixer: 5.47 MiB
    Hamiltonian: 16.00 MiB
      Arrays: 15.95 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.69 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.64 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 15
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |            C                    |  
 |        |            C                    |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 N      7.000000    7.000000   10.751750    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000    7.000000    9.566448    ( 0.0000,  0.0000,  0.0000)
   2 C      7.000000    7.000000    8.185302    ( 0.0000,  0.0000,  0.0000)
   3 N      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   17.751750   120     0.1479

  Lengths:  14.000000  14.000000  17.751750
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:45:10   +inf   +inf   -31.652569    0      1      
iter:   2  01:45:11  +1.19  -0.90   -30.939732    0      1      
iter:   3  01:45:12  +0.06  -1.08   -30.763596    0      1      
iter:   4  01:45:13  +0.25  -1.24   -30.664230    0      1      
iter:   5  01:45:15  -1.90  -1.96   -30.658343    0      1      
iter:   6  01:45:16  -1.45  -2.16   -30.654616    0      1      
iter:   7  01:45:17  -3.56  -3.04   -30.654593    0      1      
iter:   8  01:45:18  -3.95  -3.21   -30.654579    0      1      
iter:   9  01:45:19  -4.69  -3.54   -30.654578    0      1      
iter:  10  01:45:21  -4.65  -3.73   -30.654579    0      1      
iter:  11  01:45:22  -5.99  -4.08   -30.654579    0      1      
iter:  12  01:45:23  -5.88  -4.20   -30.654579    0      1      
iter:  13  01:45:24  -6.70  -4.58   -30.654578    0      1      
iter:  14  01:45:25  -7.81  -4.85   -30.654578    0      1      
iter:  15  01:45:26  -7.19  -5.01   -30.654578    0      1      
iter:  16  01:45:28  -9.20  -5.92   -30.654578    0      1      
iter:  17  01:45:29 -10.33  -6.29   -30.654578    0      1      

Converged after 17 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5018.699239)

Kinetic:        +18.834696
Potential:      -23.652551
External:        +0.000000
XC:             -26.079183
Entropy (-ST):   +0.000000
Local:           +0.242459
--------------------------
Free energy:    -30.654578
Extrapolated:   -30.654578

Fermi levels: -6.26457, -6.26457

 Band  Eigenvalues  Occupancy
    0    -24.24633    2.00000
    1    -23.75841    2.00000
    2    -18.63854    2.00000
    3    -10.94680    2.00000
    4    -10.94680    2.00000
    5    -10.23823    2.00000
    6     -9.80488    2.00000
    7     -8.96193    2.00000
    8     -8.96193    2.00000
    9     -3.56720    0.00000
   10     -3.56720    0.00000
   11      1.81954    0.00000
   12      1.81954    0.00000
   13      2.19620    0.00000
   14      8.22375    0.00000

Gap: 5.395 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.007     0.007   0.0% |
LCAO WFS Initialize:                 1.172     0.031   0.1% |
 Hamiltonian:                        1.141     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.049     0.049   0.2% |
  Hartree integrate/restrict:        0.021     0.021   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.331     0.006   0.0% |
   Communicate bwd 0:                0.067     0.067   0.3% |
   Communicate bwd 1:                0.061     0.061   0.3% |
   Communicate fwd 0:                0.059     0.059   0.3% |
   Communicate fwd 1:                0.065     0.065   0.3% |
   fft:                              0.018     0.018   0.1% |
   fft2:                             0.056     0.056   0.3% |
  XC 3D grid:                        0.735     0.735   3.3% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          18.860     0.004   0.0% |
 Density:                            0.652     0.000   0.0% |
  Atomic density matrices:           0.035     0.035   0.2% |
  Mix:                               0.509     0.509   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.103     0.008   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.094     0.094   0.4% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       18.057     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.007     0.007   0.0% |
  Communicate:                       0.786     0.786   3.5% ||
  Hartree integrate/restrict:        0.365     0.365   1.6% ||
  Poisson:                           5.269     0.083   0.4% |
   Communicate bwd 0:                1.045     1.045   4.7% |-|
   Communicate bwd 1:                0.971     0.971   4.4% |-|
   Communicate fwd 0:                0.940     0.940   4.2% |-|
   Communicate fwd 1:                1.036     1.036   4.7% |-|
   fft:                              0.280     0.280   1.3% ||
   fft2:                             0.914     0.914   4.1% |-|
  XC 3D grid:                       11.571    11.571  52.2% |--------------------|
  vbar:                              0.051     0.051   0.2% |
 LCAO eigensolver:                   0.146     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.026     0.026   0.1% |
  Orbital Layouts:                   0.011     0.011   0.0% |
  Potential matrix:                  0.104     0.104   0.5% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.338     0.338   1.5% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.780     1.780   8.0% |--|
-----------------------------------------------------------
Total:                                        22.159 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 01:45:29 2019
