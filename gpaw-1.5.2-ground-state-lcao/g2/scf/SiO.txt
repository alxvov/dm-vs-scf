
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:46:46 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -9927.196551

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 42.13 MiB
    Density: 26.84 MiB
      Arrays: 21.10 MiB
      Localized functions: 1.02 MiB
      Mixer: 4.73 MiB
    Hamiltonian: 13.88 MiB
      Arrays: 13.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.41 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.40 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 8
Bands to converge: occupied states only
Number of valence electrons: 10

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Si                   |  
 |        |                                 |  
 |        |            O                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Si     7.000000    7.000000    8.542326    ( 0.0000,  0.0000,  0.0000)
   1 O      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.542326   104     0.1494

  Lengths:  14.000000  14.000000  15.542326
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1513

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:46:49   +inf   +inf   -12.405663    0      1      
iter:   2  01:46:50  +1.94  -0.72   -10.358021    0      1      
iter:   3  01:46:51  -0.66  -1.09   -10.316911    0      1      
iter:   4  01:46:52  +0.05  -1.20   -10.264462    0      1      
iter:   5  01:46:53  -0.91  -2.04   -10.261644    0      1      
iter:   6  01:46:54  -2.38  -2.39   -10.261321    0      1      
iter:   7  01:46:55  -1.97  -2.56   -10.261104    0      1      
iter:   8  01:46:56  -4.76  -3.11   -10.261108    0      1      
iter:   9  01:46:57  -3.37  -3.27   -10.261108    0      1      
iter:  10  01:46:59  -4.46  -3.78   -10.261107    0      1      
iter:  11  01:47:00  -6.07  -4.02   -10.261107    0      1      
iter:  12  01:47:01  -5.60  -4.19   -10.261107    0      1      
iter:  13  01:47:02  -7.12  -4.57   -10.261107    0      1      
iter:  14  01:47:03  -7.38  -4.74   -10.261107    0      1      
iter:  15  01:47:04  -7.53  -5.22   -10.261107    0      1      
iter:  16  01:47:05  -8.87  -5.74   -10.261107    0      1      
iter:  17  01:47:06  -9.32  -6.00   -10.261107    0      1      
iter:  18  01:47:07 -10.31  -6.47   -10.261107    0      1      

Converged after 18 iterations.

Dipole moment: (0.000000, -0.000000, 0.543034) |e|*Ang

Energy contributions relative to reference atoms: (reference = -9927.196551)

Kinetic:        +14.109527
Potential:      -14.441612
External:        +0.000000
XC:              -9.962244
Entropy (-ST):   +0.000000
Local:           +0.033221
--------------------------
Free energy:    -10.261107
Extrapolated:   -10.261107

Fermi levels: -4.73742, -4.73742

 Band  Eigenvalues  Occupancy
    0    -23.08248    2.00000
    1    -10.46211    2.00000
    2     -7.67914    2.00000
    3     -7.67914    2.00000
    4     -7.00129    2.00000
    5     -2.47356    0.00000
    6     -2.47356    0.00000
    7      0.89706    0.00000

Gap: 4.528 eV
Transition (v -> c):
  (s=0, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.005     0.005   0.0% |
LCAO WFS Initialize:                 1.045     0.026   0.1% |
 Hamiltonian:                        1.019     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.063     0.063   0.3% |
  Hartree integrate/restrict:        0.020     0.020   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.290     0.004   0.0% |
   Communicate bwd 0:                0.058     0.058   0.3% |
   Communicate bwd 1:                0.053     0.053   0.3% |
   Communicate fwd 0:                0.051     0.051   0.2% |
   Communicate fwd 1:                0.057     0.057   0.3% |
   fft:                              0.019     0.019   0.1% |
   fft2:                             0.048     0.048   0.2% |
  XC 3D grid:                        0.641     0.641   3.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          17.810     0.004   0.0% |
 Density:                            0.530     0.000   0.0% |
  Atomic density matrices:           0.022     0.022   0.1% |
  Mix:                               0.449     0.449   2.2% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.054     0.007   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.047     0.047   0.2% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       17.204     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
  Communicate:                       1.083     1.083   5.2% |-|
  Hartree integrate/restrict:        0.349     0.349   1.7% ||
  Poisson:                           4.939     0.075   0.4% |
   Communicate bwd 0:                0.973     0.973   4.7% |-|
   Communicate bwd 1:                0.911     0.911   4.4% |-|
   Communicate fwd 0:                0.867     0.867   4.2% |-|
   Communicate fwd 1:                0.965     0.965   4.6% |-|
   fft:                              0.318     0.318   1.5% ||
   fft2:                             0.829     0.829   4.0% |-|
  XC 3D grid:                       10.769    10.769  51.6% |--------------------|
  vbar:                              0.048     0.048   0.2% |
 LCAO eigensolver:                   0.071     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.016     0.016   0.1% |
  Orbital Layouts:                   0.004     0.004   0.0% |
  Potential matrix:                  0.047     0.047   0.2% |
  Residual:                          0.002     0.002   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.352     0.352   1.7% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.640     1.640   7.9% |--|
-----------------------------------------------------------
Total:                                        20.856 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 01:47:07 2019
