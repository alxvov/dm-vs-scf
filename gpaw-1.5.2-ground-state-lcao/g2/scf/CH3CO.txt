
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:47:06 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -4133.579019

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 112*108*104 grid
  Fine grid: 224*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 80.49 MiB
    Density: 48.33 MiB
      Arrays: 40.74 MiB
      Localized functions: 0.79 MiB
      Mixer: 6.81 MiB
    Hamiltonian: 30.33 MiB
      Arrays: 30.27 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.83 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.78 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 54
Number of bands in calculation: 15
Bands to converge: occupied states only
Number of valence electrons: 17

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |           H                            |  
 |         |                C  O                    |  
 |         |             H                          |  
 |         |           H                            |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      7.639335    7.960023    7.881061    ( 0.0000,  0.0000,  0.1000)
   1 C      8.617626    9.114120    7.881061    ( 0.0000,  0.0000,  0.6000)
   2 H      8.162075    7.000000    7.881061    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.044566    8.762122    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    8.044566    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 O      9.812695    9.055782    7.881061    ( 0.0000,  0.0000,  0.3000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.812695    0.000000    0.000000   112     0.1501
  2. axis:    no     0.000000   16.114120    0.000000   108     0.1492
  3. axis:    no     0.000000    0.000000   15.762122   104     0.1516

  Lengths:  16.812695  16.114120  15.762122
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1503

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  00:47:11   +inf   +inf   -33.713116    0      1        +1.0000
iter:   2  00:47:13  +1.04  -0.83   -33.035653    0      1        +1.0000
iter:   3  00:47:15  +0.46  -1.03   -32.764577    0      1        +1.0000
iter:   4  00:47:17  -0.31  -1.29   -32.725660    0      1        +1.0000
iter:   5  00:47:19  -0.90  -1.89   -32.721374    0      1        +1.0000
iter:   6  00:47:21  -2.40  -2.44   -32.721071    0      1        +1.0000
iter:   7  00:47:23  -2.76  -2.67   -32.720901    0      1        +1.0000
iter:   8  00:47:25  -3.68  -3.14   -32.720902    0      1        +1.0000
iter:   9  00:47:27  -4.12  -3.43   -32.720900    0      1        +1.0000
iter:  10  00:47:29  -4.95  -3.74   -32.720901    0      1        +1.0000
iter:  11  00:47:31  -5.08  -4.02   -32.720901    0      1        +1.0000
iter:  12  00:47:33  -6.73  -4.53   -32.720901    0      1        +1.0000
iter:  13  00:47:36  -6.31  -4.74   -32.720901    0      1        +1.0000
iter:  14  00:47:38  -7.73  -5.33   -32.720901    0      1        +1.0000
iter:  15  00:47:40  -8.03  -5.54   -32.720901    0      1        +1.0000
iter:  16  00:47:42  -8.93  -5.79   -32.720901    0      1        +1.0000
iter:  17  00:47:44  -9.12  -6.09   -32.720901    0      1        +1.0000
iter:  18  00:47:46 -10.11  -6.53   -32.720901    0      1        +1.0000

Converged after 18 iterations.

Dipole moment: (-0.373386, -0.254052, 0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999998)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.058751)
   1 C  ( 0.000000,  0.000000,  0.202796)
   2 H  ( 0.000000,  0.000000,  0.009243)
   3 H  ( 0.000000,  0.000000, -0.001407)
   4 H  ( 0.000000,  0.000000, -0.001407)
   5 O  ( 0.000000,  0.000000,  0.197078)

Energy contributions relative to reference atoms: (reference = -4133.579019)

Kinetic:        +27.344565
Potential:      -30.070102
External:        +0.000000
XC:             -30.100069
Entropy (-ST):   +0.000000
Local:           +0.104705
--------------------------
Free energy:    -32.720901
Extrapolated:   -32.720901

Spin contamination: 0.018635 electrons
Fermi levels: -2.83270, -5.31640

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -26.94852    1.00000    -26.60711    1.00000
    1    -18.85507    1.00000    -18.61571    1.00000
    2    -13.21030    1.00000    -12.80812    1.00000
    3    -11.20205    1.00000    -11.02091    1.00000
    4    -11.14949    1.00000    -10.85371    1.00000
    5    -10.97090    1.00000    -10.58067    1.00000
    6     -9.36041    1.00000     -9.21015    1.00000
    7     -9.01212    1.00000     -8.25713    1.00000
    8     -4.03597    1.00000     -2.37568    0.00000
    9     -1.62943    0.00000     -1.17014    0.00000
   10      2.01333    0.00000      2.09584    0.00000
   11      3.30806    0.00000      3.49006    0.00000
   12      3.43902    0.00000      3.52860    0.00000
   13      5.08027    0.00000      5.32347    0.00000
   14      6.90872    0.00000      7.14492    0.00000

Gap: 4.221 eV
Transition (v -> c):
  (s=1, k=0, n=7, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.009     0.009   0.0% |
LCAO WFS Initialize:                 2.028     0.076   0.2% |
 Hamiltonian:                        1.952     0.003   0.0% |
  Atomic:                            0.043     0.043   0.1% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.037     0.037   0.1% |
  Hartree integrate/restrict:        0.047     0.047   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.281     0.015   0.0% |
   Communicate bwd 0:                0.017     0.017   0.0% |
   Communicate bwd 1:                0.074     0.074   0.2% |
   Communicate fwd 0:                0.074     0.074   0.2% |
   Communicate fwd 1:                0.021     0.021   0.1% |
   fft:                              0.028     0.028   0.1% |
   fft2:                             0.051     0.051   0.1% |
  XC 3D grid:                        1.532     1.532   3.8% |-|
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          35.235     0.005   0.0% |
 Density:                            1.663     0.000   0.0% |
  Atomic density matrices:           0.119     0.119   0.3% |
  Mix:                               1.247     1.247   3.1% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.007     0.007   0.0% |
  Pseudo density:                    0.286     0.022   0.1% |
   Calculate density matrix:         0.002     0.002   0.0% |
   Construct density:                0.262     0.262   0.7% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       33.140     0.039   0.1% |
  Atomic:                            0.735     0.735   1.8% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.036     0.036   0.1% |
  Communicate:                       0.629     0.629   1.6% ||
  Hartree integrate/restrict:        0.812     0.812   2.0% ||
  Poisson:                           4.825     0.259   0.6% |
   Communicate bwd 0:                0.359     0.359   0.9% |
   Communicate bwd 1:                1.286     1.286   3.2% ||
   Communicate fwd 0:                1.220     1.220   3.1% ||
   Communicate fwd 1:                0.387     0.387   1.0% |
   fft:                              0.485     0.485   1.2% |
   fft2:                             0.829     0.829   2.1% ||
  XC 3D grid:                       25.997    25.997  65.1% |-------------------------|
  vbar:                              0.067     0.067   0.2% |
 LCAO eigensolver:                   0.426     0.003   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.004     0.004   0.0% |
  Distribute overlap matrix:         0.110     0.110   0.3% |
  Orbital Layouts:                   0.022     0.022   0.1% |
  Potential matrix:                  0.280     0.280   0.7% |
  Residual:                          0.007     0.007   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.519     0.519   1.3% ||
mktci:                               0.001     0.001   0.0% |
Other:                               2.140     2.140   5.4% |-|
-----------------------------------------------------------
Total:                                        39.937 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:47:46 2019
