
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:25:36 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -7911.324575

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*108*100 grid
  Fine grid: 184*216*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 47.37 MiB
    Density: 30.15 MiB
      Arrays: 23.85 MiB
      Localized functions: 0.95 MiB
      Mixer: 5.35 MiB
    Hamiltonian: 15.67 MiB
      Arrays: 15.60 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.56 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.55 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 6
Bands to converge: occupied states only
Number of valence electrons: 6

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           Si                    |  
 |         |            H                    |  
 |         |          H                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 Si     7.000000    8.096938    8.050177    ( 0.0000,  0.0000,  0.0000)
   1 H      7.000000    9.193876    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.193876    0.000000   108     0.1499
  3. axis:    no     0.000000    0.000000   15.050177   100     0.1505

  Lengths:  14.000000  16.193876  15.050177
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:25:39   +inf   +inf    -9.970819    0      1      
iter:   2  01:25:40  +0.80  -0.53    -9.487513    0      1      
iter:   3  01:25:41  +1.24  -0.69    -9.150032    0      1      
iter:   4  01:25:42  +0.33  -1.34    -9.140661    0      1      
iter:   5  01:25:43  -1.18  -1.92    -9.139757    0      1      
iter:   6  01:25:44  -2.66  -2.21    -9.139741    0      1      
iter:   7  01:25:46  -2.63  -2.40    -9.139700    0      1      
iter:   8  01:25:47  -3.92  -3.02    -9.139694    0      1      
iter:   9  01:25:48  -3.76  -3.27    -9.139691    0      1      
iter:  10  01:25:49  -5.35  -3.83    -9.139691    0      1      
iter:  11  01:25:50  -6.02  -4.03    -9.139691    0      1      
iter:  12  01:25:51  -7.02  -4.20    -9.139691    0      1      
iter:  13  01:25:53  -7.71  -4.58    -9.139691    0      1      
iter:  14  01:25:54  -7.10  -4.73    -9.139691    0      1      
iter:  15  01:25:55  -8.90  -5.22    -9.139691    0      1      
iter:  16  01:25:56  -8.95  -5.39    -9.139691    0      1      
iter:  17  01:25:57  -9.63  -5.74    -9.139691    0      1      
iter:  18  01:25:58 -10.24  -5.90    -9.139691    0      1      

Converged after 18 iterations.

Dipole moment: (0.000000, -0.000000, -0.058492) |e|*Ang

Energy contributions relative to reference atoms: (reference = -7911.324575)

Kinetic:         +9.340125
Potential:       -9.943649
External:        +0.000000
XC:              -8.514933
Entropy (-ST):   +0.000000
Local:           -0.021234
--------------------------
Free energy:     -9.139691
Extrapolated:    -9.139691

Fermi levels: -4.71830, -4.71830

 Band  Eigenvalues  Occupancy
    0    -12.44171    2.00000
    1     -7.83245    2.00000
    2     -5.58231    2.00000
    3     -3.85430    0.00000
    4      0.65445    0.00000
    5      1.94005    0.00000

Gap: 1.728 eV
Transition (v -> c):
  (s=0, k=0, n=2, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=3, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.006     0.006   0.0% |
LCAO WFS Initialize:                 1.160     0.031   0.1% |
 Hamiltonian:                        1.129     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.064     0.064   0.3% |
  Hartree integrate/restrict:        0.023     0.023   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.315     0.007   0.0% |
   Communicate bwd 0:                0.062     0.062   0.3% |
   Communicate bwd 1:                0.060     0.060   0.3% |
   Communicate fwd 0:                0.056     0.056   0.2% |
   Communicate fwd 1:                0.063     0.063   0.3% |
   fft:                              0.018     0.018   0.1% |
   fft2:                             0.048     0.048   0.2% |
  XC 3D grid:                        0.722     0.722   3.2% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          19.711     0.005   0.0% |
 Density:                            0.637     0.000   0.0% |
  Atomic density matrices:           0.022     0.022   0.1% |
  Mix:                               0.539     0.539   2.4% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.070     0.008   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.061     0.061   0.3% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       18.986     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.009     0.009   0.0% |
  Communicate:                       1.092     1.092   4.8% |-|
  Hartree integrate/restrict:        0.363     0.363   1.6% ||
  Poisson:                           5.356     0.129   0.6% |
   Communicate bwd 0:                1.064     1.064   4.7% |-|
   Communicate bwd 1:                1.014     1.014   4.4% |-|
   Communicate fwd 0:                0.954     0.954   4.2% |-|
   Communicate fwd 1:                1.057     1.057   4.6% |-|
   fft:                              0.310     0.310   1.4% ||
   fft2:                             0.828     0.828   3.6% ||
  XC 3D grid:                       12.105    12.105  53.1% |--------------------|
  vbar:                              0.054     0.054   0.2% |
 LCAO eigensolver:                   0.084     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.016     0.016   0.1% |
  Orbital Layouts:                   0.004     0.004   0.0% |
  Potential matrix:                  0.060     0.060   0.3% |
  Residual:                          0.002     0.002   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.180     0.180   0.8% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.726     1.726   7.6% |--|
-----------------------------------------------------------
Total:                                        22.787 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:25:58 2019
