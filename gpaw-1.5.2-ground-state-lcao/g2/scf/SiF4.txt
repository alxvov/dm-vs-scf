
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:53:25 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -18742.667391

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*104 grid
  Fine grid: 208*208*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 54.27 MiB
    Density: 34.56 MiB
      Arrays: 27.03 MiB
      Localized functions: 1.45 MiB
      Mixer: 6.07 MiB
    Hamiltonian: 17.80 MiB
      Arrays: 17.69 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 1.91 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.84 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 65
Number of bands in calculation: 20
Bands to converge: occupied states only
Number of valence electrons: 32

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                F                     |  
 |         |           F Si                       |  
 |         |            F                         |  
 |         |               F                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 Si     7.912806    7.912806    7.912806    ( 0.0000,  0.0000,  0.0000)
   1 F      8.825612    8.825612    8.825612    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000    7.000000    8.825612    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    8.825612    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 F      8.825612    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.825612    0.000000    0.000000   104     0.1522
  2. axis:    no     0.000000   15.825612    0.000000   104     0.1522
  3. axis:    no     0.000000    0.000000   15.825612   104     0.1522

  Lengths:  15.825612  15.825612  15.825612
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1522

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:53:29   +inf   +inf   -26.335478    0      1      
iter:   2  00:53:30  +0.57  -1.14   -25.296284    0      1      
iter:   3  00:53:31  +0.20  -1.35   -25.181780    0      1      
iter:   4  00:53:33  -1.08  -1.62   -25.209902    0      1      
iter:   5  00:53:34  -2.28  -2.49   -25.208391    0      1      
iter:   6  00:53:35  -2.96  -2.82   -25.207846    0      1      
iter:   7  00:53:37  -2.69  -3.06   -25.207594    0      1      
iter:   8  00:53:38  -5.78  -3.84   -25.207592    0      1      
iter:   9  00:53:39  -5.29  -4.11   -25.207590    0      1      
iter:  10  00:53:41  -6.16  -4.55   -25.207590    0      1      
iter:  11  00:53:42  -6.95  -4.76   -25.207590    0      1      
iter:  12  00:53:43  -6.80  -4.95   -25.207590    0      1      
iter:  13  00:53:45  -8.10  -5.36   -25.207590    0      1      
iter:  14  00:53:46  -8.52  -5.60   -25.207590    0      1      
iter:  15  00:53:47  -9.22  -6.04   -25.207590    0      1      
iter:  16  00:53:49  -9.77  -6.36   -25.207590    0      1      
iter:  17  00:53:50 -10.73  -6.59   -25.207590    0      1      

Converged after 17 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -18742.667391)

Kinetic:        +37.400186
Potential:      -33.701772
External:        +0.000000
XC:             -29.159173
Entropy (-ST):   +0.000000
Local:           +0.253170
--------------------------
Free energy:    -25.207590
Extrapolated:   -25.207590

Fermi levels: -5.04996, -5.04996

 Band  Eigenvalues  Occupancy
    0    -31.37656    2.00000
    1    -30.43838    2.00000
    2    -30.43838    2.00000
    3    -30.43838    2.00000
    4    -14.96206    2.00000
    5    -12.92843    2.00000
    6    -12.92843    2.00000
    7    -12.92843    2.00000
    8    -11.36285    2.00000
    9    -11.36285    2.00000
   10    -11.21409    2.00000
   11    -11.21409    2.00000
   12    -11.21409    2.00000
   13    -10.21841    2.00000
   14    -10.21841    2.00000
   15    -10.21841    2.00000
   16      0.11849    0.00000
   17      1.91794    0.00000
   18      1.91794    0.00000
   19      1.91794    0.00000

Gap: 10.337 eV
Transition (v -> c):
  (s=0, k=0, n=15, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=16, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.008     0.008   0.0% |
LCAO WFS Initialize:                 1.314     0.031   0.1% |
 Hamiltonian:                        1.283     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.066     0.066   0.3% |
  Hartree integrate/restrict:        0.024     0.024   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.367     0.009   0.0% |
   Communicate bwd 0:                0.073     0.073   0.3% |
   Communicate bwd 1:                0.067     0.067   0.3% |
   Communicate fwd 0:                0.070     0.070   0.3% |
   Communicate fwd 1:                0.073     0.073   0.3% |
   fft:                              0.024     0.024   0.1% |
   fft2:                             0.052     0.052   0.2% |
  XC 3D grid:                        0.817     0.817   3.3% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          21.127     0.004   0.0% |
 Density:                            0.714     0.000   0.0% |
  Atomic density matrices:           0.044     0.044   0.2% |
  Mix:                               0.571     0.571   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.095     0.009   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.084     0.084   0.3% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       20.263     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.009     0.009   0.0% |
  Communicate:                       1.053     1.053   4.3% |-|
  Hartree integrate/restrict:        0.412     0.412   1.7% ||
  Poisson:                           5.756     0.096   0.4% |
   Communicate bwd 0:                1.154     1.154   4.7% |-|
   Communicate bwd 1:                1.099     1.099   4.4% |-|
   Communicate fwd 0:                1.053     1.053   4.3% |-|
   Communicate fwd 1:                1.149     1.149   4.7% |-|
   fft:                              0.384     0.384   1.6% ||
   fft2:                             0.820     0.820   3.3% ||
  XC 3D grid:                       12.970    12.970  52.5% |--------------------|
  vbar:                              0.056     0.056   0.2% |
 LCAO eigensolver:                   0.145     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.034     0.034   0.1% |
  Orbital Layouts:                   0.015     0.015   0.1% |
  Potential matrix:                  0.090     0.090   0.4% |
  Residual:                          0.004     0.004   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.337     0.337   1.4% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.913     1.913   7.7% |--|
-----------------------------------------------------------
Total:                                        24.703 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:53:50 2019
