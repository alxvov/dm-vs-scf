
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:45:57 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Al-setup:
  name: Aluminium
  id: 0292cae29f5d6237e50f6abdd43a7bdd
  Z: 13
  valence: 3
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Al.PBE.gz
  cutoffs: 1.08(comp), 1.91(filt), 2.36(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)    -7.753   1.085
    3p(1.00)    -2.712   1.085
    *s          19.459   1.085
    *p          24.499   1.085
    *d           0.000   1.085

  LCAO basis set for Al:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Al.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=7.8750 Bohr: 3s-sz confined orbital
      l=1, rc=10.4219 Bohr: 3p-sz confined orbital
      l=0, rc=4.6094 Bohr: 3s-dz split-valence wave
      l=1, rc=6.2812 Bohr: 3p-dz split-valence wave
      l=2, rc=10.4219 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -44268.271452

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 116*116*92 grid
  Fine grid: 232*232*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 232*232*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 61.03 MiB
    Density: 38.37 MiB
      Arrays: 29.78 MiB
      Localized functions: 1.91 MiB
      Mixer: 6.69 MiB
    Hamiltonian: 19.62 MiB
      Arrays: 19.48 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.14 MiB
    Wavefunctions: 3.04 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.99 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 16
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .------------------------------------------.  
           /|                                          |  
          / |                                          |  
         /  |                                          |  
        /   |                                          |  
       /    |                                          |  
      /     |                                          |  
     /      |                                          |  
    /       |                                          |  
   /        |                                          |  
  /         |                                          |  
 *          |                                          |  
 |          |                                          |  
 |          |                Cl                        |  
 |          |               Al                         |  
 |          |          Cl      Cl                      |  
 |          |                                          |  
 |          .------------------------------------------.  
 |         /                                          /   
 |        /                                          /    
 |       /                                          /     
 |      /                                          /      
 |     /                                          /       
 |    /                                          /        
 |   /                                          /         
 |  /                                          /          
 | /                                          /           
 |/                                          /            
 *------------------------------------------*             

Positions:
   0 Al     8.791842    8.034520    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 Cl     8.791842   10.103561    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 Cl    10.583684    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 Cl     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.583684    0.000000    0.000000   116     0.1516
  2. axis:    no     0.000000   17.103561    0.000000   116     0.1474
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  17.583684  17.103561  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1504

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:46:01   +inf   +inf   -13.342216    0      1      
iter:   2  01:46:02  -0.29  -1.10   -13.085511    0      1      
iter:   3  01:46:04  -0.24  -1.26   -12.988682    0      1      
iter:   4  01:46:05  -0.94  -1.66   -12.985083    0      1      
iter:   5  01:46:07  -2.48  -2.30   -12.984507    0      1      
iter:   6  01:46:08  -2.26  -2.59   -12.984200    0      1      
iter:   7  01:46:10  -4.32  -3.15   -12.984159    0      1      
iter:   8  01:46:11  -4.69  -3.55   -12.984152    0      1      
iter:   9  01:46:13  -5.20  -3.97   -12.984152    0      1      
iter:  10  01:46:14  -7.18  -4.59   -12.984152    0      1      
iter:  11  01:46:16  -7.19  -4.89   -12.984152    0      1      
iter:  12  01:46:17  -8.31  -5.33   -12.984152    0      1      
iter:  13  01:46:19  -9.28  -5.61   -12.984152    0      1      
iter:  14  01:46:20  -9.28  -5.80   -12.984152    0      1      
iter:  15  01:46:22 -10.51  -6.41   -12.984152    0      1      

Converged after 15 iterations.

Dipole moment: (0.000000, 0.000129, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -44268.271452)

Kinetic:        +27.594211
Potential:      -24.804570
External:        +0.000000
XC:             -15.817100
Entropy (-ST):   +0.000000
Local:           +0.043307
--------------------------
Free energy:    -12.984152
Extrapolated:   -12.984152

Fermi levels: -4.62667, -4.62667

 Band  Eigenvalues  Occupancy
    0    -20.80941    2.00000
    1    -20.38276    2.00000
    2    -20.38263    2.00000
    3    -11.32289    2.00000
    4     -9.60586    2.00000
    5     -9.60567    2.00000
    6     -8.68734    2.00000
    7     -8.20197    2.00000
    8     -8.20187    2.00000
    9     -8.03664    2.00000
   10     -8.03646    2.00000
   11     -7.57144    2.00000
   12     -1.68190    0.00000
   13     -1.62992    0.00000
   14      1.52476    0.00000
   15      1.52479    0.00000

Gap: 5.890 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.011     0.011   0.0% |
LCAO WFS Initialize:                 1.480     0.041   0.2% |
 Hamiltonian:                        1.439     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.066     0.066   0.3% |
  Hartree integrate/restrict:        0.030     0.030   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.431     0.011   0.0% |
   Communicate bwd 0:                0.078     0.078   0.3% |
   Communicate bwd 1:                0.077     0.077   0.3% |
   Communicate fwd 0:                0.073     0.073   0.3% |
   Communicate fwd 1:                0.082     0.082   0.3% |
   fft:                              0.036     0.036   0.1% |
   fft2:                             0.074     0.074   0.3% |
  XC 3D grid:                        0.902     0.902   3.6% ||
  vbar:                              0.006     0.006   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          20.874     0.004   0.0% |
 Density:                            0.765     0.000   0.0% |
  Atomic density matrices:           0.030     0.030   0.1% |
  Mix:                               0.573     0.573   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.157     0.009   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.147     0.147   0.6% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       19.919     0.001   0.0% |
  Atomic:                            0.004     0.004   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.013     0.013   0.1% |
  Communicate:                       0.913     0.913   3.7% ||
  Hartree integrate/restrict:        0.366     0.366   1.5% ||
  Poisson:                           6.025     0.158   0.6% |
   Communicate bwd 0:                1.099     1.099   4.4% |-|
   Communicate bwd 1:                1.096     1.096   4.4% |-|
   Communicate fwd 0:                0.994     0.994   4.0% |-|
   Communicate fwd 1:                1.132     1.132   4.6% |-|
   fft:                              0.501     0.501   2.0% ||
   fft2:                             1.045     1.045   4.2% |-|
  XC 3D grid:                       12.542    12.542  50.6% |-------------------|
  vbar:                              0.053     0.053   0.2% |
 LCAO eigensolver:                   0.186     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.021     0.021   0.1% |
  Orbital Layouts:                   0.009     0.009   0.0% |
  Potential matrix:                  0.151     0.151   0.6% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.356     0.356   1.4% ||
mktci:                               0.001     0.001   0.0% |
Other:                               2.061     2.061   8.3% |--|
-----------------------------------------------------------
Total:                                        24.787 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 01:46:22 2019
