
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:48:26 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -7936.304899

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*104 grid
  Fine grid: 208*208*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 53.90 MiB
    Density: 34.20 MiB
      Arrays: 27.03 MiB
      Localized functions: 1.09 MiB
      Mixer: 6.07 MiB
    Hamiltonian: 17.77 MiB
      Arrays: 17.69 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 1.94 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.92 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 33
Number of bands in calculation: 8
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                H                     |  
 |         |           H                          |  
 |         |            HSi                       |  
 |         |               H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 Si     7.856135    7.856135    7.856135    ( 0.0000,  0.0000,  0.0000)
   1 H      8.712270    8.712270    8.712270    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    8.712270    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.712270    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      8.712270    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.712270    0.000000    0.000000   104     0.1511
  2. axis:    no     0.000000   15.712270    0.000000   104     0.1511
  3. axis:    no     0.000000    0.000000   15.712270   104     0.1511

  Lengths:  15.712270  15.712270  15.712270
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1511

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:48:29   +inf   +inf   -19.201677    0      1      
iter:   2  00:48:31  +0.06  -0.53   -18.780882    0      1      
iter:   3  00:48:32  +0.75  -0.69   -18.403250    0      1      
iter:   4  00:48:33  -1.06  -1.72   -18.403752    0      1      
iter:   5  00:48:34  -2.93  -2.14   -18.404145    0      1      
iter:   6  00:48:36  -2.86  -2.58   -18.404079    0      1      
iter:   7  00:48:37  -2.58  -2.80   -18.404036    0      1      
iter:   8  00:48:38  -5.46  -3.60   -18.404035    0      1      
iter:   9  00:48:40  -5.13  -3.80   -18.404035    0      1      
iter:  10  00:48:41  -5.72  -4.08   -18.404035    0      1      
iter:  11  00:48:42  -7.75  -4.27   -18.404035    0      1      
iter:  12  00:48:44  -6.14  -4.48   -18.404035    0      1      
iter:  13  00:48:45  -8.35  -5.14   -18.404035    0      1      
iter:  14  00:48:46  -8.24  -5.34   -18.404035    0      1      
iter:  15  00:48:48  -9.51  -6.00   -18.404035    0      1      
iter:  16  00:48:49  -9.58  -6.17   -18.404035    0      1      
iter:  17  00:48:50 -11.43  -6.41   -18.404035    0      1      

Converged after 17 iterations.

Dipole moment: (-0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -7936.304899)

Kinetic:        +16.236173
Potential:      -18.691222
External:        +0.000000
XC:             -15.909491
Entropy (-ST):   +0.000000
Local:           -0.039495
--------------------------
Free energy:    -18.404035
Extrapolated:   -18.404035

Fermi levels: -3.80878, -3.80878

 Band  Eigenvalues  Occupancy
    0    -13.46917    2.00000
    1     -8.43970    2.00000
    2     -8.43970    2.00000
    3     -8.43970    2.00000
    4      0.82213    0.00000
    5      0.82213    0.00000
    6      0.82213    0.00000
    7      1.68704    0.00000

Gap: 9.262 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.009     0.009   0.0% |
LCAO WFS Initialize:                 1.319     0.036   0.1% |
 Hamiltonian:                        1.284     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.066     0.066   0.3% |
  Hartree integrate/restrict:        0.025     0.025   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.365     0.008   0.0% |
   Communicate bwd 0:                0.073     0.073   0.3% |
   Communicate bwd 1:                0.066     0.066   0.3% |
   Communicate fwd 0:                0.067     0.067   0.3% |
   Communicate fwd 1:                0.075     0.075   0.3% |
   fft:                              0.024     0.024   0.1% |
   fft2:                             0.051     0.051   0.2% |
  XC 3D grid:                        0.823     0.823   3.4% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          21.146     0.004   0.0% |
 Density:                            0.711     0.000   0.0% |
  Atomic density matrices:           0.033     0.033   0.1% |
  Mix:                               0.570     0.570   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.103     0.009   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.093     0.093   0.4% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       20.303     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
  Communicate:                       1.058     1.058   4.3% |-|
  Hartree integrate/restrict:        0.402     0.402   1.6% ||
  Poisson:                           5.766     0.098   0.4% |
   Communicate bwd 0:                1.153     1.153   4.7% |-|
   Communicate bwd 1:                1.094     1.094   4.5% |-|
   Communicate fwd 0:                1.067     1.067   4.4% |-|
   Communicate fwd 1:                1.148     1.148   4.7% |-|
   fft:                              0.385     0.385   1.6% ||
   fft2:                             0.821     0.821   3.4% ||
  XC 3D grid:                       13.006    13.006  53.1% |--------------------|
  vbar:                              0.056     0.056   0.2% |
 LCAO eigensolver:                   0.129     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.022     0.022   0.1% |
  Orbital Layouts:                   0.006     0.006   0.0% |
  Potential matrix:                  0.096     0.096   0.4% |
  Residual:                          0.002     0.002   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.181     0.181   0.7% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.835     1.835   7.5% |--|
-----------------------------------------------------------
Total:                                        24.494 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:48:50 2019
