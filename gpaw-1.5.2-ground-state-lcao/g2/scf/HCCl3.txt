
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:43:21 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -38704.966465

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 112*112*104 grid
  Fine grid: 224*224*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 62.33 MiB
    Density: 39.76 MiB
      Arrays: 31.40 MiB
      Localized functions: 1.30 MiB
      Mixer: 7.06 MiB
    Hamiltonian: 20.64 MiB
      Arrays: 20.54 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.10 MiB
    Wavefunctions: 1.94 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.88 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 57
Number of bands in calculation: 17
Bands to converge: occupied states only
Number of valence electrons: 26

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-----------------------------------------.  
          /|                                         |  
         / |                                         |  
        /  |                                         |  
       /   |                                         |  
      /    |                                         |  
     /     |                                         |  
    /      |                                         |  
   /       |                                         |  
  /        |                                         |  
 *         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |               H                         |  
 |         |               CCl                       |  
 |         |           Cl     Cl                     |  
 |         |                                         |  
 |         |                                         |  
 |         .-----------------------------------------.  
 |        /                                         /   
 |       /                                         /    
 |      /                                         /     
 |     /                                         /      
 |    /                                         /       
 |   /                                         /        
 |  /                                         /         
 | /                                         /          
 |/                                         /           
 *-----------------------------------------*            

Positions:
   0 C      8.456415    7.840862    7.534966    ( 0.0000,  0.0000,  0.0000)
   1 H      8.456415    7.840862    8.620873    ( 0.0000,  0.0000,  0.0000)
   2 Cl     8.456415    9.522585    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 Cl     9.912830    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 Cl     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.912830    0.000000    0.000000   112     0.1510
  2. axis:    no     0.000000   16.522585    0.000000   112     0.1475
  3. axis:    no     0.000000    0.000000   15.620873   104     0.1502

  Lengths:  16.912830  16.522585  15.620873
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1496

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:43:26   +inf   +inf   -17.818207    0      1      
iter:   2  00:43:27  -0.41  -1.03   -17.592598    0      1      
iter:   3  00:43:29  +0.21  -1.17   -17.407211    0      1      
iter:   4  00:43:30  -1.45  -2.11   -17.401792    0      1      
iter:   5  00:43:32  -2.07  -2.44   -17.400761    0      1      
iter:   6  00:43:33  -2.98  -2.77   -17.400660    0      1      
iter:   7  00:43:35  -3.81  -3.17   -17.400641    0      1      
iter:   8  00:43:36  -5.12  -3.64   -17.400640    0      1      
iter:   9  00:43:38  -5.00  -3.86   -17.400640    0      1      
iter:  10  00:43:39  -6.78  -4.38   -17.400640    0      1      
iter:  11  00:43:41  -6.86  -4.62   -17.400640    0      1      
iter:  12  00:43:42  -7.78  -5.04   -17.400640    0      1      
iter:  13  00:43:44  -8.29  -5.33   -17.400640    0      1      
iter:  14  00:43:46  -9.03  -5.68   -17.400640    0      1      
iter:  15  00:43:47  -9.74  -5.97   -17.400640    0      1      
iter:  16  00:43:49 -10.14  -6.19   -17.400640    0      1      

Converged after 16 iterations.

Dipole moment: (-0.000000, 0.000020, 0.182032) |e|*Ang

Energy contributions relative to reference atoms: (reference = -38704.966465)

Kinetic:        +31.876002
Potential:      -28.593720
External:        +0.000000
XC:             -20.735899
Entropy (-ST):   +0.000000
Local:           +0.052978
--------------------------
Free energy:    -17.400640
Extrapolated:   -17.400640

Fermi levels: -4.08913, -4.08913

 Band  Eigenvalues  Occupancy
    0    -23.42829    2.00000
    1    -20.70962    2.00000
    2    -20.70940    2.00000
    3    -15.82372    2.00000
    4    -12.29236    2.00000
    5    -11.54846    2.00000
    6    -11.54837    2.00000
    7     -8.15794    2.00000
    8     -8.15789    2.00000
    9     -7.40937    2.00000
   10     -7.40927    2.00000
   11     -7.30999    2.00000
   12     -6.91308    2.00000
   13     -1.26519    0.00000
   14     -0.00702    0.00000
   15     -0.00701    0.00000
   16      1.95101    0.00000

Gap: 5.648 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.009     0.009   0.0% |
LCAO WFS Initialize:                 1.506     0.043   0.2% |
 Hamiltonian:                        1.463     0.000   0.0% |
  Atomic:                            0.063     0.063   0.2% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.027     0.027   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.419     0.011   0.0% |
   Communicate bwd 0:                0.080     0.080   0.3% |
   Communicate bwd 1:                0.077     0.077   0.3% |
   Communicate fwd 0:                0.078     0.078   0.3% |
   Communicate fwd 1:                0.085     0.085   0.3% |
   fft:                              0.027     0.027   0.1% |
   fft2:                             0.061     0.061   0.2% |
  XC 3D grid:                        0.947     0.947   3.5% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          22.895     0.004   0.0% |
 Density:                            0.841     0.000   0.0% |
  Atomic density matrices:           0.025     0.025   0.1% |
  Mix:                               0.629     0.629   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.181     0.010   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.169     0.169   0.6% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       21.831     0.002   0.0% |
  Atomic:                            0.949     0.949   3.5% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.032     0.032   0.1% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.408     0.408   1.5% ||
  Poisson:                           6.315     0.186   0.7% |
   Communicate bwd 0:                1.222     1.222   4.5% |-|
   Communicate bwd 1:                1.181     1.181   4.3% |-|
   Communicate fwd 0:                1.140     1.140   4.2% |-|
   Communicate fwd 1:                1.271     1.271   4.7% |-|
   fft:                              0.401     0.401   1.5% ||
   fft2:                             0.915     0.915   3.4% ||
  XC 3D grid:                       14.066    14.066  51.8% |--------------------|
  vbar:                              0.059     0.059   0.2% |
 LCAO eigensolver:                   0.219     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.003     0.003   0.0% |
  Distribute overlap matrix:         0.017     0.017   0.1% |
  Orbital Layouts:                   0.011     0.011   0.0% |
  Potential matrix:                  0.183     0.183   0.7% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.529     0.529   1.9% ||
mktci:                               0.001     0.001   0.0% |
Other:                               2.232     2.232   8.2% |--|
-----------------------------------------------------------
Total:                                        27.178 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:43:49 2019
