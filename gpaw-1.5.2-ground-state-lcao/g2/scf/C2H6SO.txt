
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 02:10:13 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/S.PBE.gz
  cutoffs: 0.76(comp), 1.77(filt), 1.66(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -15028.669403

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 124*116*104 grid
  Fine grid: 248*232*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 248*232*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 72.58 MiB
    Density: 45.84 MiB
      Arrays: 36.05 MiB
      Localized functions: 1.67 MiB
      Mixer: 8.12 MiB
    Hamiltonian: 23.71 MiB
      Arrays: 23.58 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.13 MiB
    Wavefunctions: 3.03 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.10 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.91 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 82
Number of bands in calculation: 20
Bands to converge: occupied states only
Number of valence electrons: 26

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .---------------------------------------------.  
           /|                                             |  
          / |                                             |  
         /  |                                             |  
        /   |                                             |  
       /    |                                             |  
      /     |                                             |  
     /      |                                             |  
    /       |                                             |  
   /        |                                             |  
  /         |                                             |  
 *          |                                             |  
 |          |                                             |  
 |          |                                             |  
 |          |             H    OH                         |  
 |          |                                             |  
 |          |           H C   S C  H                      |  
 |          |            H      H                         |  
 |          |                                             |  
 |          .---------------------------------------------.  
 |         /                                             /   
 |        /                                             /    
 |       /                                             /     
 |      /                                             /      
 |     /                                             /       
 |    /                                             /        
 |   /                                             /         
 |  /                                             /          
 | /                                             /           
 |/                                             /            
 *---------------------------------------------*             

Positions:
   0 S      9.279406    9.025165    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 O      9.279424   10.294069    7.818462    ( 0.0000,  0.0000,  0.0000)
   2 C     10.618932    7.984305    7.619360    ( 0.0000,  0.0000,  0.0000)
   3 C      7.939856    7.984335    7.619361    ( 0.0000,  0.0000,  0.0000)
   4 H     10.535239    7.896942    8.705468    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    8.479403    7.369969    ( 0.0000,  0.0000,  0.0000)
   6 H     10.583811    7.000000    7.146054    ( 0.0000,  0.0000,  0.0000)
   7 H     11.558799    8.479353    7.369969    ( 0.0000,  0.0000,  0.0000)
   8 H      7.974957    7.000029    7.146056    ( 0.0000,  0.0000,  0.0000)
   9 H      8.023547    7.896972    8.705469    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.558799    0.000000    0.000000   124     0.1497
  2. axis:    no     0.000000   17.294069    0.000000   116     0.1491
  3. axis:    no     0.000000    0.000000   15.705469   104     0.1510

  Lengths:  18.558799  17.294069  15.705469
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1499

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  02:10:19   +inf   +inf   -52.066224    0      1      
iter:   2  02:10:20  +1.22  -0.70   -48.730941    0      1      
iter:   3  02:10:22  +0.50  -0.93   -48.155790    0      1      
iter:   4  02:10:24  -0.28  -1.17   -48.029260    0      1      
iter:   5  02:10:25  -1.17  -1.82   -48.019465    0      1      
iter:   6  02:10:27  -2.34  -2.36   -48.018804    0      1      
iter:   7  02:10:29  -2.63  -2.61   -48.018465    0      1      
iter:   8  02:10:30  -3.43  -2.98   -48.018415    0      1      
iter:   9  02:10:32  -4.71  -3.49   -48.018411    0      1      
iter:  10  02:10:34  -4.40  -3.69   -48.018410    0      1      
iter:  11  02:10:35  -6.13  -4.07   -48.018410    0      1      
iter:  12  02:10:37  -6.15  -4.31   -48.018410    0      1      
iter:  13  02:10:38  -6.30  -4.58   -48.018410    0      1      
iter:  14  02:10:40  -7.59  -4.84   -48.018410    0      1      
iter:  15  02:10:42  -7.30  -5.04   -48.018410    0      1      
iter:  16  02:10:43  -8.75  -5.51   -48.018410    0      1      
iter:  17  02:10:45  -8.54  -5.68   -48.018410    0      1      
iter:  18  02:10:47  -9.59  -6.05   -48.018410    0      1      
iter:  19  02:10:48 -10.03  -6.28   -48.018410    0      1      

Converged after 19 iterations.

Dipole moment: (-0.000010, -0.746492, -0.125539) |e|*Ang

Energy contributions relative to reference atoms: (reference = -15028.669403)

Kinetic:        +43.041177
Potential:      -43.999067
External:        +0.000000
XC:             -46.987652
Entropy (-ST):   +0.000000
Local:           -0.072867
--------------------------
Free energy:    -48.018410
Extrapolated:   -48.018410

Fermi levels: -2.10747, -2.10747

 Band  Eigenvalues  Occupancy
    0    -24.82580    2.00000
    1    -19.60487    2.00000
    2    -17.80571    2.00000
    3    -14.74102    2.00000
    4    -11.36795    2.00000
    5    -11.15274    2.00000
    6    -11.13328    2.00000
    7    -10.25367    2.00000
    8     -9.47384    2.00000
    9     -9.39894    2.00000
   10     -8.15352    2.00000
   11     -5.92038    2.00000
   12     -4.84159    2.00000
   13      0.62666    0.00000
   14      0.62823    0.00000
   15      1.39740    0.00000
   16      1.90425    0.00000
   17      2.82967    0.00000
   18      3.05150    0.00000
   19      3.08968    0.00000

Gap: 5.468 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.016     0.016   0.0% |
LCAO WFS Initialize:                 1.602     0.049   0.1% |
 Hamiltonian:                        1.553     0.000   0.0% |
  Atomic:                            0.063     0.001   0.0% |
   XC Correction:                    0.063     0.063   0.2% |
  Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.034     0.034   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.364     0.018   0.1% |
   Communicate bwd 0:                0.024     0.024   0.1% |
   Communicate bwd 1:                0.091     0.091   0.3% |
   Communicate fwd 0:                0.082     0.082   0.2% |
   Communicate fwd 1:                0.025     0.025   0.1% |
   fft:                              0.050     0.050   0.1% |
   fft2:                             0.073     0.073   0.2% |
  XC 3D grid:                        1.078     1.078   3.1% ||
  vbar:                              0.006     0.006   0.0% |
P tci:                               0.008     0.008   0.0% |
SCF-cycle:                          29.873     0.005   0.0% |
 Density:                            1.323     0.000   0.0% |
  Atomic density matrices:           0.038     0.038   0.1% |
  Mix:                               0.876     0.876   2.5% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.005     0.005   0.0% |
  Pseudo density:                    0.401     0.015   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.384     0.384   1.1% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       28.065     0.002   0.0% |
  Atomic:                            1.148     0.012   0.0% |
   XC Correction:                    1.135     1.135   3.2% ||
  Calculate atomic Hamiltonians:     0.055     0.055   0.2% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.545     0.545   1.6% ||
  Poisson:                           6.626     0.325   0.9% |
   Communicate bwd 0:                0.450     0.450   1.3% ||
   Communicate bwd 1:                1.663     1.663   4.7% |-|
   Communicate fwd 0:                1.517     1.517   4.3% |-|
   Communicate fwd 1:                0.452     0.452   1.3% ||
   fft:                              0.890     0.890   2.5% ||
   fft2:                             1.328     1.328   3.8% |-|
  XC 3D grid:                       19.607    19.607  56.0% |---------------------|
  vbar:                              0.081     0.081   0.2% |
 LCAO eigensolver:                   0.481     0.002   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.004     0.004   0.0% |
  Distribute overlap matrix:         0.029     0.029   0.1% |
  Orbital Layouts:                   0.024     0.024   0.1% |
  Potential matrix:                  0.416     0.416   1.2% |
  Residual:                          0.005     0.005   0.0% |
ST tci:                              0.003     0.003   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               1.014     1.014   2.9% ||
mktci:                               0.002     0.002   0.0% |
Other:                               2.513     2.513   7.2% |--|
-----------------------------------------------------------
Total:                                        35.031 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 02:10:48 2019
