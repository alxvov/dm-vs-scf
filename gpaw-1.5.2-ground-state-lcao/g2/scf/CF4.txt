
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 02:07:17 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -11883.951256

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*104 grid
  Fine grid: 208*208*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 53.05 MiB
    Density: 33.99 MiB
      Arrays: 27.03 MiB
      Localized functions: 0.89 MiB
      Mixer: 6.07 MiB
    Hamiltonian: 17.76 MiB
      Arrays: 17.69 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.30 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.23 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 65
Number of bands in calculation: 20
Bands to converge: occupied states only
Number of valence electrons: 32

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |               F                     |  
 |         |           F                         |  
 |         |            FC                       |  
 |         |              F                      |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.767436    7.767436    7.767436    ( 0.0000,  0.0000,  0.0000)
   1 F      8.534872    8.534872    8.534872    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000    7.000000    8.534872    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    8.534872    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 F      8.534872    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.534872    0.000000    0.000000   104     0.1494
  2. axis:    no     0.000000   15.534872    0.000000   104     0.1494
  3. axis:    no     0.000000    0.000000   15.534872   104     0.1494

  Lengths:  15.534872  15.534872  15.534872
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1494

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  02:07:20   +inf   +inf   -25.098905    0      1      
iter:   2  02:07:21  +0.83  -1.21   -23.834601    0      1      
iter:   3  02:07:23  +0.52  -1.42   -23.616406    0      1      
iter:   4  02:07:24  -0.70  -1.73   -23.610559    0      1      
iter:   5  02:07:25  -1.88  -2.43   -23.608516    0      1      
iter:   6  02:07:27  -2.91  -2.76   -23.607843    0      1      
iter:   7  02:07:28  -2.66  -3.08   -23.607597    0      1      
iter:   8  02:07:29  -4.75  -3.58   -23.607592    0      1      
iter:   9  02:07:31  -5.08  -3.77   -23.607589    0      1      
iter:  10  02:07:32  -5.52  -4.28   -23.607589    0      1      
iter:  11  02:07:33  -6.12  -4.44   -23.607589    0      1      
iter:  12  02:07:34  -6.56  -4.67   -23.607589    0      1      
iter:  13  02:07:36  -7.22  -5.02   -23.607589    0      1      
iter:  14  02:07:37  -7.18  -5.22   -23.607589    0      1      
iter:  15  02:07:38  -8.64  -5.59   -23.607589    0      1      
iter:  16  02:07:40  -8.61  -5.86   -23.607589    0      1      
iter:  17  02:07:41  -9.46  -6.19   -23.607589    0      1      
iter:  18  02:07:42 -10.94  -6.31   -23.607589    0      1      

Converged after 18 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -11883.951256)

Kinetic:        +34.535622
Potential:      -30.511944
External:        +0.000000
XC:             -28.037496
Entropy (-ST):   +0.000000
Local:           +0.406229
--------------------------
Free energy:    -23.607589
Extrapolated:   -23.607589

Fermi levels: -3.00673, -3.00673

 Band  Eigenvalues  Occupancy
    0    -34.27901    2.00000
    1    -31.24415    2.00000
    2    -31.24415    2.00000
    3    -31.24415    2.00000
    4    -17.78841    2.00000
    5    -15.37112    2.00000
    6    -15.37112    2.00000
    7    -15.37112    2.00000
    8    -11.56398    2.00000
    9    -11.56398    2.00000
   10    -11.01451    2.00000
   11    -11.01451    2.00000
   12    -11.01451    2.00000
   13     -9.76141    2.00000
   14     -9.76141    2.00000
   15     -9.76141    2.00000
   16      3.74795    0.00000
   17      3.75542    0.00000
   18      3.75542    0.00000
   19      3.75542    0.00000

Gap: 13.509 eV
Transition (v -> c):
  (s=0, k=0, n=15, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=16, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.006     0.006   0.0% |
LCAO WFS Initialize:                 1.297     0.035   0.1% |
 Hamiltonian:                        1.263     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.049     0.049   0.2% |
  Hartree integrate/restrict:        0.027     0.027   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.366     0.008   0.0% |
   Communicate bwd 0:                0.072     0.072   0.3% |
   Communicate bwd 1:                0.067     0.067   0.3% |
   Communicate fwd 0:                0.067     0.067   0.3% |
   Communicate fwd 1:                0.075     0.075   0.3% |
   fft:                              0.024     0.024   0.1% |
   fft2:                             0.052     0.052   0.2% |
  XC 3D grid:                        0.812     0.812   3.2% ||
  vbar:                              0.005     0.005   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          21.988     0.005   0.0% |
 Density:                            0.729     0.000   0.0% |
  Atomic density matrices:           0.041     0.041   0.2% |
  Mix:                               0.592     0.592   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.090     0.010   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.079     0.079   0.3% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       21.113     0.002   0.0% |
  Atomic:                            0.006     0.006   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
  Communicate:                       0.840     0.840   3.3% ||
  Hartree integrate/restrict:        0.439     0.439   1.7% ||
  Poisson:                           6.068     0.096   0.4% |
   Communicate bwd 0:                1.223     1.223   4.8% |-|
   Communicate bwd 1:                1.182     1.182   4.6% |-|
   Communicate fwd 0:                1.101     1.101   4.3% |-|
   Communicate fwd 1:                1.193     1.193   4.7% |-|
   fft:                              0.403     0.403   1.6% ||
   fft2:                             0.871     0.871   3.4% ||
  XC 3D grid:                       13.691    13.691  53.5% |--------------------|
  vbar:                              0.060     0.060   0.2% |
 LCAO eigensolver:                   0.140     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.030     0.030   0.1% |
  Orbital Layouts:                   0.016     0.016   0.1% |
  Potential matrix:                  0.087     0.087   0.3% |
  Residual:                          0.005     0.005   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.356     0.356   1.4% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.936     1.936   7.6% |--|
-----------------------------------------------------------
Total:                                        25.588 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 02:07:42 2019
