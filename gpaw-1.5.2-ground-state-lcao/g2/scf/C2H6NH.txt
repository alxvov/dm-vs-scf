
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:52:05 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3624.408871

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*108*120 grid
  Fine grid: 208*216*240 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 64.78 MiB
    Density: 40.75 MiB
      Arrays: 32.45 MiB
      Localized functions: 1.00 MiB
      Mixer: 7.30 MiB
    Hamiltonian: 21.31 MiB
      Arrays: 21.23 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.72 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.08 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.63 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 74
Number of bands in calculation: 16
Bands to converge: occupied states only
Number of valence electrons: 20

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |              H                       |  
 |         |                                      |  
 |         |           H C HH                     |  
 |         |              N                       |  
 |         |                                      |  
 |         |           H CHH                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.944690    7.738040   10.288285    ( 0.0000,  0.0000,  0.0000)
   1 N      7.944690    8.555212    9.083405    ( 0.0000,  0.0000,  0.0000)
   2 C      7.944690    7.738040    7.878525    ( 0.0000,  0.0000,  0.0000)
   3 H      8.763721    7.000000   10.331911    ( 0.0000,  0.0000,  0.0000)
   4 H      8.011818    8.383924   11.166810    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.189755   10.345155    ( 0.0000,  0.0000,  0.0000)
   6 H      8.777523    9.140962    9.083405    ( 0.0000,  0.0000,  0.0000)
   7 H      8.763721    7.000000    7.834899    ( 0.0000,  0.0000,  0.0000)
   8 H      8.011818    8.383924    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.189755    7.821655    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.777523    0.000000    0.000000   104     0.1517
  2. axis:    no     0.000000   16.140962    0.000000   108     0.1495
  3. axis:    no     0.000000    0.000000   18.166810   120     0.1514

  Lengths:  15.777523  16.140962  18.166810
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1508

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:52:10   +inf   +inf   -53.095383    0      1      
iter:   2  01:52:11  +0.96  -0.69   -51.303777    0      1      
iter:   3  01:52:13  +0.83  -0.87   -50.607324    0      1      
iter:   4  01:52:14  +0.15  -1.24   -50.523377    0      1      
iter:   5  01:52:16  -1.50  -2.07   -50.521141    0      1      
iter:   6  01:52:18  -1.75  -2.36   -50.520424    0      1      
iter:   7  01:52:19  -2.04  -2.61   -50.520091    0      1      
iter:   8  01:52:21  -4.32  -3.25   -50.520088    0      1      
iter:   9  01:52:22  -3.79  -3.42   -50.520086    0      1      
iter:  10  01:52:24  -5.63  -4.12   -50.520086    0      1      
iter:  11  01:52:25  -6.20  -4.42   -50.520086    0      1      
iter:  12  01:52:27  -6.23  -4.67   -50.520086    0      1      
iter:  13  01:52:29  -8.08  -5.18   -50.520086    0      1      
iter:  14  01:52:30  -7.70  -5.47   -50.520086    0      1      
iter:  15  01:52:32  -9.17  -6.02   -50.520086    0      1      
iter:  16  01:52:33  -9.90  -6.27   -50.520086    0      1      
iter:  17  01:52:35 -10.07  -6.58   -50.520086    0      1      

Converged after 17 iterations.

Dipole moment: (0.169181, -0.055416, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3624.408871)

Kinetic:        +41.852308
Potential:      -46.383218
External:        +0.000000
XC:             -46.142250
Entropy (-ST):   +0.000000
Local:           +0.153074
--------------------------
Free energy:    -50.520086
Extrapolated:   -50.520086

Fermi levels: -1.56903, -1.56903

 Band  Eigenvalues  Occupancy
    0    -22.26204    2.00000
    1    -17.19329    2.00000
    2    -15.31662    2.00000
    3    -11.86937    2.00000
    4    -10.60596    2.00000
    5    -10.52993    2.00000
    6     -9.20690    2.00000
    7     -8.65631    2.00000
    8     -8.57426    2.00000
    9     -4.80089    2.00000
   10      1.66283    0.00000
   11      2.69502    0.00000
   12      3.04046    0.00000
   13      3.45701    0.00000
   14      3.56459    0.00000
   15      4.23017    0.00000

Gap: 6.464 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.014     0.014   0.0% |
LCAO WFS Initialize:                 1.540     0.044   0.2% |
 Hamiltonian:                        1.496     0.000   0.0% |
  Atomic:                            0.054     0.008   0.0% |
   XC Correction:                    0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.031     0.031   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.426     0.008   0.0% |
   Communicate bwd 0:                0.086     0.086   0.3% |
   Communicate bwd 1:                0.079     0.079   0.3% |
   Communicate fwd 0:                0.081     0.081   0.3% |
   Communicate fwd 1:                0.088     0.088   0.3% |
   fft:                              0.024     0.024   0.1% |
   fft2:                             0.059     0.059   0.2% |
  XC 3D grid:                        0.974     0.974   3.3% ||
  vbar:                              0.006     0.006   0.0% |
P tci:                               0.003     0.003   0.0% |
SCF-cycle:                          25.184     0.004   0.0% |
 Density:                            1.063     0.000   0.0% |
  Atomic density matrices:           0.090     0.090   0.3% |
  Mix:                               0.701     0.701   2.4% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.004     0.004   0.0% |
  Pseudo density:                    0.266     0.011   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.253     0.253   0.9% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       23.728     0.002   0.0% |
  Atomic:                            0.870     0.125   0.4% |
   XC Correction:                    0.745     0.745   2.5% ||
  Calculate atomic Hamiltonians:     0.034     0.034   0.1% |
  Communicate:                       0.001     0.001   0.0% |
  Hartree integrate/restrict:        0.461     0.461   1.6% ||
  Poisson:                           6.770     0.132   0.4% |
   Communicate bwd 0:                1.385     1.385   4.7% |-|
   Communicate bwd 1:                1.294     1.294   4.4% |-|
   Communicate fwd 0:                1.277     1.277   4.3% |-|
   Communicate fwd 1:                1.362     1.362   4.6% |-|
   fft:                              0.379     0.379   1.3% ||
   fft2:                             0.941     0.941   3.2% ||
  XC 3D grid:                       15.524    15.524  52.6% |--------------------|
  vbar:                              0.066     0.066   0.2% |
 LCAO eigensolver:                   0.388     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.072     0.072   0.2% |
  Orbital Layouts:                   0.018     0.018   0.1% |
  Potential matrix:                  0.290     0.290   1.0% |
  Residual:                          0.004     0.004   0.0% |
ST tci:                              0.003     0.003   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.545     0.545   1.8% ||
mktci:                               0.002     0.002   0.0% |
Other:                               2.246     2.246   7.6% |--|
-----------------------------------------------------------
Total:                                        29.538 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 01:52:35 2019
