
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:52:23 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

Reference energy: -2509.349620

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*100 grid
  Fine grid: 184*184*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 53.27 MiB
    Density: 32.15 MiB
      Arrays: 27.29 MiB
      Localized functions: 0.31 MiB
      Mixer: 4.54 MiB
    Hamiltonian: 20.30 MiB
      Arrays: 20.28 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.81 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.80 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 8
Bands to converge: occupied states only
Number of valence electrons: 9

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |            C                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 C      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  1.0000)
   1 N      7.000000    7.000000    8.134799    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.134799   100     0.1513

  Lengths:  14.000000  14.000000  15.134799
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1519

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  00:52:27   +inf   +inf   -12.472984    0      1        +1.0000
iter:   2  00:52:28  +1.80  -0.83   -12.175473    0      1        +1.0000
iter:   3  00:52:30  +0.58  -1.16   -12.074360    0      1        +1.0000
iter:   4  00:52:31  +0.85  -1.32   -12.009532    0      1        +1.0000
iter:   5  00:52:33  -0.95  -1.98   -12.012579    0      1        +1.0000
iter:   6  00:52:34  -0.62  -2.09   -12.012622    0      1        +1.0000
iter:   7  00:52:36  -1.63  -2.27   -12.012350    0      1        +1.0000
iter:   8  00:52:37  -2.22  -2.59   -12.012473    0      1        +1.0000
iter:   9  00:52:39  -2.83  -3.03   -12.012496    0      1        +1.0000
iter:  10  00:52:40  -2.83  -3.24   -12.012494    0      1        +1.0000
iter:  11  00:52:42  -3.43  -3.53   -12.012492    0      1        +1.0000
iter:  12  00:52:43  -4.49  -3.95   -12.012492    0      1        +1.0000
iter:  13  00:52:45  -4.52  -4.12   -12.012492    0      1        +1.0000
iter:  14  00:52:46  -5.47  -4.25   -12.012492    0      1        +1.0000
iter:  15  00:52:48  -5.83  -4.42   -12.012492    0      1        +1.0000
iter:  16  00:52:49  -6.48  -4.86   -12.012492    0      1        +1.0000
iter:  17  00:52:50  -7.33  -5.26   -12.012492    0      1        +1.0000
iter:  18  00:52:52  -7.62  -5.49   -12.012492    0      1        +1.0000
iter:  19  00:52:53  -8.36  -5.73   -12.012492    0      1        +1.0000
iter:  20  00:52:55  -8.67  -5.84   -12.012492    0      1        +1.0000
iter:  21  00:52:56  -8.60  -5.96   -12.012492    0      1        +1.0000
iter:  22  00:52:58  -9.20  -6.34   -12.012492    0      1        +1.0000
iter:  23  00:52:59 -10.23  -7.02   -12.012492    0      1        +1.0000

Converged after 23 iterations.

Dipole moment: (0.000000, 0.000000, -0.222784) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000008)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.302816)
   1 N  ( 0.000000,  0.000000,  0.115137)

Energy contributions relative to reference atoms: (reference = -2509.349620)

Kinetic:        +15.151928
Potential:      -14.953237
External:        +0.000000
XC:             -12.329717
Entropy (-ST):   +0.000000
Local:           +0.118534
--------------------------
Free energy:    -12.012492
Extrapolated:   -12.012492

Spin contamination: 0.035628 electrons
Fermi levels: -5.05573, -8.25844

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -24.11911    1.00000    -23.71161    1.00000
    1    -11.74213    1.00000    -10.38326    1.00000
    2     -9.57879    1.00000     -9.31186    1.00000
    3     -9.57879    1.00000     -9.31186    1.00000
    4     -9.32930    1.00000     -7.20503    0.00000
    5     -0.78215    0.00000     -0.07181    0.00000
    6     -0.78215    0.00000     -0.07181    0.00000
    7      7.61676    0.00000      8.30935    0.00000

Gap: 6.423 eV
Transition (v -> c):
  (s=1, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.003     0.003   0.0% |
LCAO WFS Initialize:                 1.475     0.049   0.1% |
 Hamiltonian:                        1.426     0.001   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.080     0.080   0.2% |
  Hartree integrate/restrict:        0.034     0.034   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.276     0.004   0.0% |
   Communicate bwd 0:                0.057     0.057   0.2% |
   Communicate bwd 1:                0.050     0.050   0.1% |
   Communicate fwd 0:                0.049     0.049   0.1% |
   Communicate fwd 1:                0.056     0.056   0.2% |
   fft:                              0.015     0.015   0.0% |
   fft2:                             0.046     0.046   0.1% |
  XC 3D grid:                        1.031     1.031   2.9% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          32.665     0.007   0.0% |
 Density:                            1.212     0.000   0.0% |
  Atomic density matrices:           0.036     0.036   0.1% |
  Mix:                               1.057     1.057   2.9% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.006     0.006   0.0% |
  Pseudo density:                    0.111     0.018   0.1% |
   Calculate density matrix:         0.002     0.002   0.0% |
   Construct density:                0.090     0.090   0.2% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       31.308     0.032   0.1% |
  Atomic:                            0.007     0.007   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
  Communicate:                       1.772     1.772   4.9% |-|
  Hartree integrate/restrict:        0.755     0.755   2.1% ||
  Poisson:                           6.161     0.097   0.3% |
   Communicate bwd 0:                1.232     1.232   3.4% ||
   Communicate bwd 1:                1.133     1.133   3.1% ||
   Communicate fwd 0:                1.103     1.103   3.0% ||
   Communicate fwd 1:                1.251     1.251   3.5% ||
   fft:                              0.323     0.323   0.9% |
   fft2:                             1.024     1.024   2.8% ||
  XC 3D grid:                       22.513    22.513  62.2% |------------------------|
  vbar:                              0.059     0.059   0.2% |
 LCAO eigensolver:                   0.138     0.003   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.032     0.032   0.1% |
  Orbital Layouts:                   0.010     0.010   0.0% |
  Potential matrix:                  0.085     0.085   0.2% |
  Residual:                          0.005     0.005   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.359     0.359   1.0% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.663     1.663   4.6% |-|
-----------------------------------------------------------
Total:                                        36.169 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:52:59 2019
