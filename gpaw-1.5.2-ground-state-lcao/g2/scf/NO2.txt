
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 02:26:55 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -5563.426107

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*108*96 grid
  Fine grid: 184*216*192 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 60.29 MiB
    Density: 36.51 MiB
      Arrays: 30.79 MiB
      Localized functions: 0.58 MiB
      Mixer: 5.13 MiB
    Hamiltonian: 22.92 MiB
      Arrays: 22.88 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.86 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.83 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 39
Number of bands in calculation: 12
Bands to converge: occupied states only
Number of valence electrons: 17

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           NO                    |  
 |         |          O                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 N      7.000000    8.118122    7.477643    ( 0.0000,  0.0000,  1.0000)
   1 O      7.000000    9.236244    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 O      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.236244    0.000000   108     0.1503
  3. axis:    no     0.000000    0.000000   14.477643    96     0.1508

  Lengths:  14.000000  16.236244  14.477643
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1511

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  02:26:59   +inf   +inf   -17.698223    0      1        +1.0000
iter:   2  02:27:00  +1.38  -1.06   -17.189517    0      1        +1.0000
iter:   3  02:27:02  -0.00  -1.36   -17.163512    0      1        +1.0000
iter:   4  02:27:04  +0.21  -1.55   -17.163777    0      1        +1.0000
iter:   5  02:27:05  -1.59  -2.24   -17.162191    0      1        +1.0000
iter:   6  02:27:07  -1.68  -2.45   -17.161530    0      1        +1.0000
iter:   7  02:27:09  -2.11  -2.87   -17.161494    0      1        +1.0000
iter:   8  02:27:10  -3.86  -3.33   -17.161496    0      1        +1.0000
iter:   9  02:27:12  -4.19  -3.61   -17.161496    0      1        +1.0000
iter:  10  02:27:14  -5.58  -4.14   -17.161496    0      1        +1.0000
iter:  11  02:27:15  -5.65  -4.36   -17.161497    0      1        +1.0000
iter:  12  02:27:17  -6.59  -4.93   -17.161497    0      1        +1.0000
iter:  13  02:27:19  -6.92  -5.19   -17.161497    0      1        +1.0000
iter:  14  02:27:20  -7.91  -5.55   -17.161497    0      1        +1.0000
iter:  15  02:27:22  -8.05  -5.72   -17.161497    0      1        +1.0000
iter:  16  02:27:23  -9.04  -6.11   -17.161497    0      1        +1.0000
iter:  17  02:27:25  -9.65  -6.38   -17.161497    0      1        +1.0000
iter:  18  02:27:27 -10.23  -6.85   -17.161497    0      1        +1.0000

Converged after 18 iterations.

Dipole moment: (-0.000000, -0.000000, 0.044321) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999997)
Local magnetic moments:
   0 N  ( 0.000000,  0.000000,  0.197356)
   1 O  ( 0.000000,  0.000000,  0.219133)
   2 O  ( 0.000000,  0.000000,  0.219133)

Energy contributions relative to reference atoms: (reference = -5563.426107)

Kinetic:        +15.502228
Potential:      -15.152507
External:        +0.000000
XC:             -17.724121
Entropy (-ST):   +0.000000
Local:           +0.212903
--------------------------
Free energy:    -17.161497
Extrapolated:   -17.161497

Spin contamination: 0.014441 electrons
Fermi levels: -4.63212, -6.20054

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -31.70457    1.00000    -31.31726    1.00000
    1    -27.97621    1.00000    -27.53149    1.00000
    2    -16.26532    1.00000    -15.63371    1.00000
    3    -13.55946    1.00000    -13.22401    1.00000
    4    -13.49021    1.00000    -13.09403    1.00000
    5    -13.39570    1.00000    -12.53740    1.00000
    6     -8.86086    1.00000     -8.65959    1.00000
    7     -8.50256    1.00000     -7.85595    1.00000
    8     -6.01922    1.00000     -4.54513    0.00000
    9     -3.24503    0.00000     -2.85039    0.00000
   10      4.41322    0.00000      4.75964    0.00000
   11      8.86214    0.00000      9.41007    0.00000

Gap: 1.474 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.004     0.004   0.0% |
LCAO WFS Initialize:                 1.657     0.055   0.2% |
 Hamiltonian:                        1.602     0.002   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.080     0.080   0.2% |
  Hartree integrate/restrict:        0.037     0.037   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.304     0.009   0.0% |
   Communicate bwd 0:                0.060     0.060   0.2% |
   Communicate bwd 1:                0.055     0.055   0.2% |
   Communicate fwd 0:                0.058     0.058   0.2% |
   Communicate fwd 1:                0.058     0.058   0.2% |
   fft:                              0.018     0.018   0.1% |
   fft2:                             0.046     0.046   0.1% |
  XC 3D grid:                        1.175     1.175   3.7% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          28.152     0.005   0.0% |
 Density:                            1.091     0.000   0.0% |
  Atomic density matrices:           0.030     0.030   0.1% |
  Mix:                               0.943     0.943   3.0% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.005     0.005   0.0% |
  Pseudo density:                    0.110     0.016   0.0% |
   Calculate density matrix:         0.002     0.002   0.0% |
   Construct density:                0.093     0.093   0.3% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       26.908     0.028   0.1% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
  Communicate:                       1.352     1.352   4.2% |-|
  Hartree integrate/restrict:        0.628     0.628   2.0% ||
  Poisson:                           5.203     0.136   0.4% |
   Communicate bwd 0:                1.040     1.040   3.3% ||
   Communicate bwd 1:                0.978     0.978   3.1% ||
   Communicate fwd 0:                0.947     0.947   3.0% ||
   Communicate fwd 1:                1.020     1.020   3.2% ||
   fft:                              0.296     0.296   0.9% |
   fft2:                             0.785     0.785   2.5% ||
  XC 3D grid:                       19.631    19.631  61.5% |------------------------|
  vbar:                              0.052     0.052   0.2% |
 LCAO eigensolver:                   0.148     0.003   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.025     0.025   0.1% |
  Orbital Layouts:                   0.014     0.014   0.0% |
  Potential matrix:                  0.099     0.099   0.3% |
  Residual:                          0.006     0.006   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.356     0.356   1.1% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.773     1.773   5.5% |-|
-----------------------------------------------------------
Total:                                        31.945 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 02:27:27 2019
