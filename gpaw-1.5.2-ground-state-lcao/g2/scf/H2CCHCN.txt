
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:22:13 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

Reference energy: -4602.076337

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 112*124*92 grid
  Fine grid: 224*248*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*248*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 61.14 MiB
    Density: 38.54 MiB
      Arrays: 30.74 MiB
      Localized functions: 0.89 MiB
      Mixer: 6.91 MiB
    Hamiltonian: 20.18 MiB
      Arrays: 20.11 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 2.42 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.34 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 67
Number of bands in calculation: 16
Bands to converge: occupied states only
Number of valence electrons: 20

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .-----------------------------------------.  
           /|                                         |  
          / |                                         |  
         /  |                                         |  
        /   |                                         |  
       /    |                                         |  
      /     |                                         |  
     /      |                                         |  
    /       |                                         |  
   /        |                                         |  
  /         |                                         |  
 *          |                                         |  
 |          |                                         |  
 |          |              NC                         |  
 |          |               C  H                      |  
 |          |          H  H                           |  
 |          |                                         |  
 |          .-----------------------------------------.  
 |         /                                         /   
 |        /                                         /    
 |       /                                         /     
 |      /                                         /      
 |     /                                         /       
 |    /                                         /        
 |   /                                         /         
 |  /                                         /          
 | /                                         /           
 |/                                         /            
 *-----------------------------------------*             

Positions:
   0 C      8.083609    7.977780    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C      8.830160    9.091444    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 C      8.245203   10.398658    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.018236    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      8.551176    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      9.915066    9.044298    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 N      7.777944   11.484216    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.915066    0.000000    0.000000   112     0.1510
  2. axis:    no     0.000000   18.484216    0.000000   124     0.1491
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  16.915066  18.484216  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1508

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:22:17   +inf   +inf   -44.864480    0      1      
iter:   2  01:22:19  +0.93  -0.79   -43.692266    0      1      
iter:   3  01:22:20  +0.30  -0.97   -43.283884    0      1      
iter:   4  01:22:22  +0.16  -1.19   -43.177789    0      1      
iter:   5  01:22:23  -1.13  -1.72   -43.164916    0      1      
iter:   6  01:22:25  -1.09  -1.98   -43.157415    0      1      
iter:   7  01:22:26  -2.36  -2.52   -43.156966    0      1      
iter:   8  01:22:28  -3.41  -2.85   -43.156880    0      1      
iter:   9  01:22:29  -3.34  -3.05   -43.156830    0      1      
iter:  10  01:22:31  -4.55  -3.50   -43.156825    0      1      
iter:  11  01:22:32  -4.91  -3.78   -43.156823    0      1      
iter:  12  01:22:34  -5.70  -4.21   -43.156823    0      1      
iter:  13  01:22:35  -6.81  -4.69   -43.156823    0      1      
iter:  14  01:22:37  -6.74  -4.89   -43.156823    0      1      
iter:  15  01:22:38  -8.40  -5.34   -43.156823    0      1      
iter:  16  01:22:40  -8.46  -5.51   -43.156823    0      1      
iter:  17  01:22:41  -9.11  -5.94   -43.156823    0      1      
iter:  18  01:22:43 -10.00  -6.42   -43.156823    0      1      
iter:  19  01:22:44 -10.62  -6.70   -43.156823    0      1      

Converged after 19 iterations.

Dipole moment: (0.232890, -0.718451, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4602.076337)

Kinetic:        +31.792469
Potential:      -37.150755
External:        +0.000000
XC:             -37.978944
Entropy (-ST):   +0.000000
Local:           +0.180407
--------------------------
Free energy:    -43.156823
Extrapolated:   -43.156823

Fermi levels: -4.81852, -4.81852

 Band  Eigenvalues  Occupancy
    0    -22.49265    2.00000
    1    -20.32100    2.00000
    2    -16.29830    2.00000
    3    -13.28995    2.00000
    4    -11.76114    2.00000
    5    -10.10614    2.00000
    6     -9.24778    2.00000
    7     -8.45378    2.00000
    8     -8.06931    2.00000
    9     -7.09646    2.00000
   10     -2.54059    0.00000
   11     -0.41843    0.00000
   12      1.70125    0.00000
   13      2.17964    0.00000
   14      2.36864    0.00000
   15      3.09246    0.00000

Gap: 4.556 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.011     0.011   0.0% |
LCAO WFS Initialize:                 1.487     0.043   0.1% |
 Hamiltonian:                        1.444     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.049     0.049   0.2% |
  Hartree integrate/restrict:        0.026     0.026   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.434     0.011   0.0% |
   Communicate bwd 0:                0.080     0.080   0.3% |
   Communicate bwd 1:                0.076     0.076   0.2% |
   Communicate fwd 0:                0.076     0.076   0.2% |
   Communicate fwd 1:                0.085     0.085   0.3% |
   fft:                              0.040     0.040   0.1% |
   fft2:                             0.066     0.066   0.2% |
  XC 3D grid:                        0.926     0.926   3.0% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          27.194     0.005   0.0% |
 Density:                            1.050     0.000   0.0% |
  Atomic density matrices:           0.049     0.049   0.2% |
  Mix:                               0.770     0.770   2.5% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.004     0.004   0.0% |
  Pseudo density:                    0.224     0.012   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.210     0.210   0.7% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       25.849     0.002   0.0% |
  Atomic:                            0.006     0.006   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.011     0.011   0.0% |
  Communicate:                       0.891     0.891   2.8% ||
  Hartree integrate/restrict:        0.463     0.463   1.5% ||
  Poisson:                           7.826     0.223   0.7% |
   Communicate bwd 0:                1.441     1.441   4.6% |-|
   Communicate bwd 1:                1.393     1.393   4.4% |-|
   Communicate fwd 0:                1.349     1.349   4.3% |-|
   Communicate fwd 1:                1.503     1.503   4.8% |-|
   fft:                              0.717     0.717   2.3% ||
   fft2:                             1.200     1.200   3.8% |-|
  XC 3D grid:                       16.578    16.578  52.9% |--------------------|
  vbar:                              0.071     0.071   0.2% |
 LCAO eigensolver:                   0.290     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.033     0.033   0.1% |
  Orbital Layouts:                   0.018     0.018   0.1% |
  Potential matrix:                  0.232     0.232   0.7% |
  Residual:                          0.005     0.005   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.521     0.521   1.7% ||
mktci:                               0.001     0.001   0.0% |
Other:                               2.139     2.139   6.8% |--|
-----------------------------------------------------------
Total:                                        31.356 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:22:44 2019
