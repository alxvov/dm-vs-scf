
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:26:33 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3013.403657

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*108*100 grid
  Fine grid: 208*216*200 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 52.82 MiB
    Density: 33.64 MiB
      Arrays: 26.99 MiB
      Localized functions: 0.58 MiB
      Mixer: 6.06 MiB
    Hamiltonian: 17.71 MiB
      Arrays: 17.66 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.48 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.03 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.44 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 46
Number of bands in calculation: 12
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |               H                      |  
 |         |            H N                       |  
 |         |            HN                        |  
 |         |               H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 N      7.948214    8.811711    7.226391    ( 0.0000,  0.0000,  0.0000)
   1 N      7.948214    7.373793    7.226391    ( 0.0000,  0.0000,  0.0000)
   2 H      8.159296    9.185504    8.151965    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    9.097778    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      7.737132    7.000000    8.151965    ( 0.0000,  0.0000,  0.0000)
   5 H      8.896428    7.087726    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.896428    0.000000    0.000000   104     0.1529
  2. axis:    no     0.000000   16.185504    0.000000   108     0.1499
  3. axis:    no     0.000000    0.000000   15.151965   100     0.1515

  Lengths:  15.896428  16.185504  15.151965
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1514

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:26:37   +inf   +inf   -30.926349    0      1      
iter:   2  01:26:38  +1.21  -0.71   -29.543609    0      1      
iter:   3  01:26:39  +0.96  -0.90   -29.113154    0      1      
iter:   4  01:26:41  +0.39  -1.21   -29.047362    0      1      
iter:   5  01:26:42  -1.25  -2.00   -29.044681    0      1      
iter:   6  01:26:43  -1.47  -2.31   -29.043850    0      1      
iter:   7  01:26:45  -1.87  -2.66   -29.043574    0      1      
iter:   8  01:26:46  -4.91  -3.31   -29.043572    0      1      
iter:   9  01:26:47  -4.31  -3.48   -29.043570    0      1      
iter:  10  01:26:48  -5.14  -4.03   -29.043571    0      1      
iter:  11  01:26:50  -6.62  -4.29   -29.043571    0      1      
iter:  12  01:26:51  -5.93  -4.53   -29.043571    0      1      
iter:  13  01:26:52  -8.28  -5.15   -29.043571    0      1      
iter:  14  01:26:54  -7.97  -5.29   -29.043571    0      1      
iter:  15  01:26:55  -9.13  -6.09   -29.043571    0      1      
iter:  16  01:26:56  -9.98  -6.45   -29.043571    0      1      
iter:  17  01:26:58 -10.57  -6.64   -29.043571    0      1      

Converged after 17 iterations.

Dipole moment: (0.000000, -0.000000, 0.372859) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3013.403657)

Kinetic:        +25.925884
Potential:      -26.979509
External:        +0.000000
XC:             -28.195553
Entropy (-ST):   +0.000000
Local:           +0.205608
--------------------------
Free energy:    -29.043571
Extrapolated:   -29.043571

Fermi levels: -1.92356, -1.92356

 Band  Eigenvalues  Occupancy
    0    -23.38498    2.00000
    1    -18.29984    2.00000
    2    -12.09110    2.00000
    3    -11.67312    2.00000
    4    -10.76472    2.00000
    5     -5.48085    2.00000
    6     -5.33513    2.00000
    7      1.48801    0.00000
    8      2.10896    0.00000
    9      3.54320    0.00000
   10      3.87325    0.00000
   11      5.10818    0.00000

Gap: 6.823 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.008     0.008   0.0% |
LCAO WFS Initialize:                 1.292     0.037   0.2% |
 Hamiltonian:                        1.255     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.050     0.050   0.2% |
  Hartree integrate/restrict:        0.025     0.025   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.358     0.012   0.0% |
   Communicate bwd 0:                0.069     0.069   0.3% |
   Communicate bwd 1:                0.067     0.067   0.3% |
   Communicate fwd 0:                0.066     0.066   0.3% |
   Communicate fwd 1:                0.073     0.073   0.3% |
   fft:                              0.021     0.021   0.1% |
   fft2:                             0.050     0.050   0.2% |
  XC 3D grid:                        0.813     0.813   3.4% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          20.825     0.004   0.0% |
 Density:                            0.749     0.000   0.0% |
  Atomic density matrices:           0.031     0.031   0.1% |
  Mix:                               0.596     0.596   2.5% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.116     0.009   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.106     0.106   0.4% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       19.917     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.007     0.007   0.0% |
  Communicate:                       0.804     0.804   3.3% ||
  Hartree integrate/restrict:        0.393     0.393   1.6% ||
  Poisson:                           5.656     0.163   0.7% |
   Communicate bwd 0:                1.113     1.113   4.6% |-|
   Communicate bwd 1:                1.082     1.082   4.5% |-|
   Communicate fwd 0:                1.033     1.033   4.3% |-|
   Communicate fwd 1:                1.131     1.131   4.7% |-|
   fft:                              0.339     0.339   1.4% ||
   fft2:                             0.795     0.795   3.3% ||
  XC 3D grid:                       12.995    12.995  53.8% |---------------------|
  vbar:                              0.056     0.056   0.2% |
 LCAO eigensolver:                   0.154     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.023     0.023   0.1% |
  Orbital Layouts:                   0.009     0.009   0.0% |
  Potential matrix:                  0.118     0.118   0.5% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.181     0.181   0.7% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.866     1.866   7.7% |--|
-----------------------------------------------------------
Total:                                        24.176 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:26:58 2019
