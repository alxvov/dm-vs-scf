
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:36:23 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2065.832626

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*104*96 grid
  Fine grid: 184*208*192 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*208*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 42.39 MiB
    Density: 27.33 MiB
      Arrays: 22.03 MiB
      Localized functions: 0.36 MiB
      Mixer: 4.94 MiB
    Hamiltonian: 14.44 MiB
      Arrays: 14.41 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.63 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.62 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 6
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           O                     |  
 |         |           H                     |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 O      7.000000    7.763239    7.596309    ( 0.0000,  0.0000,  0.0000)
   1 H      7.000000    8.526478    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   15.526478    0.000000   104     0.1493
  3. axis:    no     0.000000    0.000000   14.596309    96     0.1520

  Lengths:  14.000000  15.526478  14.596309
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1512

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:36:26   +inf   +inf   -14.465029    0      1      
iter:   2  01:36:27  +1.69  -0.78   -13.478747    0      1      
iter:   3  01:36:28  +1.08  -0.99   -13.295568    0      1      
iter:   4  01:36:29  -0.15  -1.20   -13.247706    0      1      
iter:   5  01:36:30  -0.98  -2.29   -13.246709    0      1      
iter:   6  01:36:31  -3.01  -2.86   -13.246692    0      1      
iter:   7  01:36:32  -2.42  -3.02   -13.246684    0      1      
iter:   8  01:36:33  -4.93  -3.77   -13.246684    0      1      
iter:   9  01:36:34  -4.98  -4.05   -13.246684    0      1      
iter:  10  01:36:35  -5.47  -4.29   -13.246684    0      1      
iter:  11  01:36:37  -6.75  -4.64   -13.246684    0      1      
iter:  12  01:36:38  -6.89  -5.13   -13.246684    0      1      
iter:  13  01:36:39  -8.27  -5.35   -13.246684    0      1      
iter:  14  01:36:40  -8.36  -5.48   -13.246684    0      1      
iter:  15  01:36:41  -8.96  -5.89   -13.246684    0      1      
iter:  16  01:36:42  -8.93  -6.12   -13.246684    0      1      
iter:  17  01:36:43 -10.26  -6.52   -13.246684    0      1      

Converged after 17 iterations.

Dipole moment: (-0.000000, -0.000000, -0.364910) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2065.832626)

Kinetic:        +14.007954
Potential:      -14.104463
External:        +0.000000
XC:             -13.217974
Entropy (-ST):   +0.000000
Local:           +0.067799
--------------------------
Free energy:    -13.246684
Extrapolated:   -13.246684

Fermi levels: -2.58718, -2.58718

 Band  Eigenvalues  Occupancy
    0    -24.69102    2.00000
    1    -12.57496    2.00000
    2     -8.57095    2.00000
    3     -6.40670    2.00000
    4      1.23234    0.00000
    5      3.42578    0.00000

Gap: 7.639 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.004     0.004   0.0% |
LCAO WFS Initialize:                 1.052     0.027   0.1% |
 Hamiltonian:                        1.025     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.047     0.047   0.2% |
  Hartree integrate/restrict:        0.020     0.020   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.293     0.007   0.0% |
   Communicate bwd 0:                0.058     0.058   0.3% |
   Communicate bwd 1:                0.055     0.055   0.3% |
   Communicate fwd 0:                0.052     0.052   0.3% |
   Communicate fwd 1:                0.057     0.057   0.3% |
   fft:                              0.019     0.019   0.1% |
   fft2:                             0.044     0.044   0.2% |
  XC 3D grid:                        0.661     0.661   3.4% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          16.793     0.004   0.0% |
 Density:                            0.494     0.000   0.0% |
  Atomic density matrices:           0.020     0.020   0.1% |
  Mix:                               0.438     0.438   2.2% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.032     0.006   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.025     0.025   0.1% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       16.249     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.006     0.006   0.0% |
  Communicate:                       0.754     0.754   3.8% |-|
  Hartree integrate/restrict:        0.316     0.316   1.6% ||
  Poisson:                           4.695     0.113   0.6% |
   Communicate bwd 0:                0.931     0.931   4.7% |-|
   Communicate bwd 1:                0.883     0.883   4.5% |-|
   Communicate fwd 0:                0.828     0.828   4.2% |-|
   Communicate fwd 1:                0.926     0.926   4.7% |-|
   fft:                              0.312     0.312   1.6% ||
   fft2:                             0.701     0.701   3.6% ||
  XC 3D grid:                       10.425    10.425  52.9% |--------------------|
  vbar:                              0.047     0.047   0.2% |
 LCAO eigensolver:                   0.046     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.012     0.012   0.1% |
  Orbital Layouts:                   0.004     0.004   0.0% |
  Potential matrix:                  0.025     0.025   0.1% |
  Residual:                          0.002     0.002   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.200     0.200   1.0% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.663     1.663   8.4% |--|
-----------------------------------------------------------
Total:                                        19.715 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:36:43 2019
