from ase.collections import g2
from gpaw import GPAW, LCAO, PoissonSolver, FermiDirac, ConvergenceError
from ase.parallel import parprint
from gpaw.utilities import h2gpts
from gpaw.utilities.memory import maxrss
import time
from gpaw.mpi import world

xc = 'PBE'
mode = LCAO()

for name in g2.names:

    sys = g2[name]
    if len(sys) == 1:
        continue
    sys.center(vacuum=7.0)
    calc = GPAW(xc=xc, h=0.15,
                convergence={'density': 1.0e10,
                             'eigenstates': 1.0e-10,
                             'energy': 1.0e10},
                maxiter=333,
                basis='dzp',
                parallel={'domain': world.size},
                mode=mode,
                txt=name + '.txt',
                occupations=FermiDirac(width=0.0, fixmagmom=True)
                )

    sys.set_calculator(calc)

    try:
        t1 = time.time()
        e = sys.get_potential_energy()
        t2 = time.time()
        steps = sys.calc.get_number_of_iterations()
        memory = maxrss() / 1024.0 ** 2
        parprint(name +
                 "\t{}\t{}\t{}\t{:.3f}".format(steps, e,
                                               t2-t1, memory))  # s,MB
    except ConvergenceError:
        parprint(name +
                 "\t{}\t{}\t{}\t{}".format(None, None, None, None))
    calc = None
    sys = None

