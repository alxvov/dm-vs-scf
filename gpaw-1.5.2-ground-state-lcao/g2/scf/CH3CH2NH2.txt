
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 02:24:03 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3624.408871

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 116*108*104 grid
  Fine grid: 232*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 232*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 62.72 MiB
    Density: 39.41 MiB
      Arrays: 31.36 MiB
      Localized functions: 1.00 MiB
      Mixer: 7.05 MiB
    Hamiltonian: 20.59 MiB
      Arrays: 20.51 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.71 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.08 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.62 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 74
Number of bands in calculation: 16
Bands to converge: occupied states only
Number of valence electrons: 20

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .------------------------------------------.  
          /|                                          |  
         / |                                          |  
        /  |                                          |  
       /   |                                          |  
      /    |                                          |  
     /     |                                          |  
    /      |                                          |  
   /       |                                          |  
  /        |                                          |  
 *         |                                          |  
 |         |                                          |  
 |         |                                          |  
 |         |               H                          |  
 |         |           H   C H  H                     |  
 |         |           N   H C                        |  
 |         |           H     H                        |  
 |         |                                          |  
 |         |                                          |  
 |         .------------------------------------------.  
 |        /                                          /   
 |       /                                          /    
 |      /                                          /     
 |     /                                          /      
 |    /                                          /       
 |   /                                          /        
 |  /                                          /         
 | /                                          /          
 |/                                          /           
 *------------------------------------------*            

Positions:
   0 C      9.582340    7.644162    7.884909    ( 0.0000,  0.0000,  0.0000)
   1 C      8.372326    8.573711    7.884909    ( 0.0000,  0.0000,  0.0000)
   2 N      7.066975    7.910282    7.884909    ( 0.0000,  0.0000,  0.0000)
   3 H     10.521636    8.206258    7.884909    ( 0.0000,  0.0000,  0.0000)
   4 H      9.574122    7.000000    8.769818    ( 0.0000,  0.0000,  0.0000)
   5 H      9.574122    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      8.406887    9.228723    7.008431    ( 0.0000,  0.0000,  0.0000)
   7 H      8.406887    9.228723    8.761387    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000    7.299420    8.698041    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.299420    7.071777    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.521636    0.000000    0.000000   116     0.1510
  2. axis:    no     0.000000   16.228723    0.000000   108     0.1503
  3. axis:    no     0.000000    0.000000   15.769818   104     0.1516

  Lengths:  17.521636  16.228723  15.769818
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1510

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  02:24:07   +inf   +inf   -53.706003    0      1      
iter:   2  02:24:08  +1.07  -0.68   -51.604779    0      1      
iter:   3  02:24:09  +0.80  -0.88   -50.922498    0      1      
iter:   4  02:24:11  +0.05  -1.18   -50.817957    0      1      
iter:   5  02:24:12  -1.68  -2.08   -50.816243    0      1      
iter:   6  02:24:14  -1.77  -2.34   -50.815410    0      1      
iter:   7  02:24:15  -2.22  -2.64   -50.815131    0      1      
iter:   8  02:24:17  -4.40  -3.24   -50.815132    0      1      
iter:   9  02:24:18  -3.86  -3.42   -50.815131    0      1      
iter:  10  02:24:19  -5.53  -4.02   -50.815132    0      1      
iter:  11  02:24:21  -5.97  -4.36   -50.815132    0      1      
iter:  12  02:24:22  -6.21  -4.73   -50.815132    0      1      
iter:  13  02:24:24  -7.59  -5.20   -50.815132    0      1      
iter:  14  02:24:25  -8.01  -5.53   -50.815132    0      1      
iter:  15  02:24:27  -9.01  -5.93   -50.815132    0      1      
iter:  16  02:24:28  -9.87  -6.20   -50.815132    0      1      
iter:  17  02:24:29 -10.13  -6.53   -50.815132    0      1      

Converged after 17 iterations.

Dipole moment: (0.156307, -0.174688, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3624.408871)

Kinetic:        +42.052230
Potential:      -46.720501
External:        +0.000000
XC:             -46.300967
Entropy (-ST):   +0.000000
Local:           +0.154106
--------------------------
Free energy:    -50.815132
Extrapolated:   -50.815132

Fermi levels: -1.77788, -1.77788

 Band  Eigenvalues  Occupancy
    0    -21.73815    2.00000
    1    -17.90340    2.00000
    2    -14.89343    2.00000
    3    -12.05074    2.00000
    4    -10.57534    2.00000
    5     -9.86111    2.00000
    6     -9.77125    2.00000
    7     -8.57206    2.00000
    8     -7.99668    2.00000
    9     -5.14472    2.00000
   10      1.58896    0.00000
   11      2.69135    0.00000
   12      2.76062    0.00000
   13      3.18414    0.00000
   14      3.87577    0.00000
   15      4.44740    0.00000

Gap: 6.734 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.001     0.001   0.0% |
Basis functions set positions:       0.014     0.014   0.1% |
LCAO WFS Initialize:                 1.387     0.043   0.2% |
 Hamiltonian:                        1.344     0.000   0.0% |
  Atomic:                            0.054     0.008   0.0% |
   XC Correction:                    0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.027     0.027   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.306     0.015   0.1% |
   Communicate bwd 0:                0.019     0.019   0.1% |
   Communicate bwd 1:                0.081     0.081   0.3% |
   Communicate fwd 0:                0.075     0.075   0.3% |
   Communicate fwd 1:                0.024     0.024   0.1% |
   fft:                              0.041     0.041   0.2% |
   fft2:                             0.052     0.052   0.2% |
  XC 3D grid:                        0.945     0.945   3.5% ||
  vbar:                              0.005     0.005   0.0% |
P tci:                               0.003     0.003   0.0% |
SCF-cycle:                          22.725     0.004   0.0% |
 Density:                            1.016     0.000   0.0% |
  Atomic density matrices:           0.041     0.041   0.2% |
  Mix:                               0.667     0.667   2.5% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.004     0.004   0.0% |
  Pseudo density:                    0.301     0.011   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.289     0.289   1.1% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       21.337     0.002   0.0% |
  Atomic:                            0.876     0.130   0.5% |
   XC Correction:                    0.746     0.746   2.8% ||
  Calculate atomic Hamiltonians:     0.040     0.040   0.2% |
  Communicate:                       0.001     0.001   0.0% |
  Hartree integrate/restrict:        0.417     0.417   1.6% ||
  Poisson:                           4.904     0.261   1.0% |
   Communicate bwd 0:                0.310     0.310   1.2% |
   Communicate bwd 1:                1.310     1.310   4.9% |-|
   Communicate fwd 0:                1.185     1.185   4.4% |-|
   Communicate fwd 1:                0.373     0.373   1.4% ||
   fft:                              0.650     0.650   2.4% ||
   fft2:                             0.816     0.816   3.0% ||
  XC 3D grid:                       15.033    15.033  56.1% |---------------------|
  vbar:                              0.064     0.064   0.2% |
 LCAO eigensolver:                   0.368     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.016     0.016   0.1% |
  Orbital Layouts:                   0.018     0.018   0.1% |
  Potential matrix:                  0.327     0.327   1.2% |
  Residual:                          0.004     0.004   0.0% |
ST tci:                              0.003     0.003   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.503     0.503   1.9% ||
mktci:                               0.002     0.002   0.0% |
Other:                               2.141     2.141   8.0% |--|
-----------------------------------------------------------
Total:                                        26.779 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 02:24:29 2019
