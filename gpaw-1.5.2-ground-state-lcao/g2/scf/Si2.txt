
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 02:09:45 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

Reference energy: -15772.688500

Spin-polarized calculation.
Magnetic moment: 2.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 60.34 MiB
    Density: 35.99 MiB
      Arrays: 29.50 MiB
      Localized functions: 1.58 MiB
      Mixer: 4.91 MiB
    Hamiltonian: 22.04 MiB
      Arrays: 21.92 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 2.31 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.30 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 8
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Si                   |  
 |        |                                 |  
 |        |                                 |  
 |        |            Si                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Si     7.000000    7.000000    9.260108    ( 0.0000,  0.0000,  1.0000)
   1 Si     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  1.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.260108   108     0.1506

  Lengths:  14.000000  14.000000  16.260108
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1516

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  02:09:49   +inf   +inf    -4.829682    0      1        +2.0000
iter:   2  02:09:50  +0.62  -0.78    -4.913977    0      1        +2.0000
iter:   3  02:09:52  +0.61  -0.92    -4.795664    0      1        +2.0000
iter:   4  02:09:54  -1.19  -1.85    -4.792814    0      1        +2.0000
iter:   5  02:09:55  -1.69  -2.15    -4.792436    0      1        +2.0000
iter:   6  02:09:57  -3.62  -2.76    -4.792420    0      1        +2.0000
iter:   7  02:09:58  -2.97  -2.93    -4.792406    0      1        +2.0000
iter:   8  02:10:00  -5.61  -3.65    -4.792406    0      1        +2.0000
iter:   9  02:10:02  -5.26  -3.82    -4.792406    0      1        +2.0000
iter:  10  02:10:03  -7.02  -4.50    -4.792406    0      1        +2.0000
iter:  11  02:10:05  -7.27  -4.78    -4.792406    0      1        +2.0000
iter:  12  02:10:07  -7.19  -4.98    -4.792406    0      1        +2.0000
iter:  13  02:10:08  -8.51  -5.31    -4.792406    0      1        +2.0000
iter:  14  02:10:10  -9.78  -5.68    -4.792406    0      1        +2.0000
iter:  15  02:10:12  -9.39  -5.85    -4.792406    0      1        +2.0000
iter:  16  02:10:13 -11.55  -6.88    -4.792406    0      1        +2.0000

Converged after 16 iterations.

Dipole moment: (-0.000000, -0.000000, 0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 2.000000)
Local magnetic moments:
   0 Si ( 0.000000,  0.000000,  0.274220)
   1 Si ( 0.000000,  0.000000,  0.274220)

Energy contributions relative to reference atoms: (reference = -15772.688500)

Kinetic:        +11.009784
Potential:       -9.720766
External:        +0.000000
XC:              -6.072658
Entropy (-ST):   +0.000000
Local:           -0.008766
--------------------------
Free energy:     -4.792406
Extrapolated:    -4.792406

Spin contamination: 0.025200 electrons
Fermi levels: -3.75987, -4.38154

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -12.97882    1.00000    -11.94659    1.00000
    1     -9.00454    1.00000     -8.24055    1.00000
    2     -5.56924    1.00000     -4.50022    1.00000
    3     -5.56924    1.00000     -4.26286    0.00000
    4     -4.81834    1.00000     -4.26286    0.00000
    5     -2.70141    0.00000     -1.69564    0.00000
    6     -2.70141    0.00000     -1.69564    0.00000
    7      0.95187    0.00000      1.46703    0.00000

Gap: 1.561 eV
Transition (v -> c):
  (s=1, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.007     0.007   0.0% |
LCAO WFS Initialize:                 1.631     0.054   0.2% |
 Hamiltonian:                        1.577     0.001   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.107     0.107   0.4% |
  Hartree integrate/restrict:        0.038     0.038   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.298     0.005   0.0% |
   Communicate bwd 0:                0.059     0.059   0.2% |
   Communicate bwd 1:                0.055     0.055   0.2% |
   Communicate fwd 0:                0.053     0.053   0.2% |
   Communicate fwd 1:                0.060     0.060   0.2% |
   fft:                              0.017     0.017   0.1% |
   fft2:                             0.050     0.050   0.2% |
  XC 3D grid:                        1.128     1.128   4.0% |-|
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          24.787     0.005   0.0% |
 Density:                            1.015     0.000   0.0% |
  Atomic density matrices:           0.036     0.036   0.1% |
  Mix:                               0.795     0.795   2.8% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.004     0.004   0.0% |
  Pseudo density:                    0.178     0.014   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.162     0.162   0.6% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       23.559     0.025   0.1% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.010     0.010   0.0% |
  Communicate:                       1.613     1.613   5.7% |-|
  Hartree integrate/restrict:        0.552     0.552   2.0% ||
  Poisson:                           4.529     0.071   0.3% |
   Communicate bwd 0:                0.906     0.906   3.2% ||
   Communicate bwd 1:                0.828     0.828   2.9% ||
   Communicate fwd 0:                0.813     0.813   2.9% ||
   Communicate fwd 1:                0.898     0.898   3.2% ||
   fft:                              0.252     0.252   0.9% |
   fft2:                             0.762     0.762   2.7% ||
  XC 3D grid:                       16.783    16.783  59.4% |-----------------------|
  vbar:                              0.044     0.044   0.2% |
 LCAO eigensolver:                   0.208     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.001     0.001   0.0% |
  Distribute overlap matrix:         0.034     0.034   0.1% |
  Orbital Layouts:                   0.007     0.007   0.0% |
  Potential matrix:                  0.160     0.160   0.6% |
  Residual:                          0.004     0.004   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.098     0.098   0.3% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.707     1.707   6.0% |-|
-----------------------------------------------------------
Total:                                        28.233 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 02:10:13 2019
