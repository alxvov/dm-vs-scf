
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:37:53 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -6628.524709

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*108*108 grid
  Fine grid: 208*216*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 57.85 MiB
    Density: 36.72 MiB
      Arrays: 29.18 MiB
      Localized functions: 0.98 MiB
      Mixer: 6.56 MiB
    Hamiltonian: 19.16 MiB
      Arrays: 19.09 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.97 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.89 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 67
Number of bands in calculation: 19
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |              O                      |  
 |         |                                     |  
 |         |          H CN                       |  
 |         |              H                      |  
 |         |          H                          |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.526639    7.400691    8.103775    ( 0.0000,  0.0000,  0.0000)
   1 N      7.640921    8.881736    8.103775    ( 0.0000,  0.0000,  0.0000)
   2 H      8.540486    7.000000    8.103775    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.108044    9.008731    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    7.108044    7.198819    ( 0.0000,  0.0000,  0.0000)
   5 O      7.707669    9.443488    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 O      7.707669    9.443488    9.207550    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.540486    0.000000    0.000000   104     0.1494
  2. axis:    no     0.000000   16.443488    0.000000   108     0.1523
  3. axis:    no     0.000000    0.000000   16.207550   108     0.1501

  Lengths:  15.540486  16.443488  16.207550
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1506

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:37:57   +inf   +inf   -39.863155    0      1      
iter:   2  01:37:59  +1.30  -0.84   -37.963033    0      1      
iter:   3  01:38:00  +0.10  -1.08   -37.695643    0      1      
iter:   4  01:38:02  -0.06  -1.28   -37.596677    0      1      
iter:   5  01:38:03  -1.24  -2.01   -37.588437    0      1      
iter:   6  01:38:04  -1.93  -2.41   -37.587243    0      1      
iter:   7  01:38:06  -2.82  -2.79   -37.587087    0      1      
iter:   8  01:38:07  -3.93  -3.10   -37.587087    0      1      
iter:   9  01:38:09  -3.89  -3.29   -37.587086    0      1      
iter:  10  01:38:10  -5.45  -3.94   -37.587085    0      1      
iter:  11  01:38:11  -5.42  -4.24   -37.587085    0      1      
iter:  12  01:38:13  -7.06  -4.72   -37.587085    0      1      
iter:  13  01:38:14  -7.59  -4.96   -37.587085    0      1      
iter:  14  01:38:16  -7.63  -5.20   -37.587085    0      1      
iter:  15  01:38:17  -9.40  -5.57   -37.587085    0      1      
iter:  16  01:38:18  -8.91  -5.78   -37.587085    0      1      
iter:  17  01:38:20 -10.06  -6.52   -37.587085    0      1      

Converged after 17 iterations.

Dipole moment: (-0.048877, -0.648003, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6628.524709)

Kinetic:        +32.009139
Potential:      -33.117133
External:        +0.000000
XC:             -36.715626
Entropy (-ST):   +0.000000
Local:           +0.236535
--------------------------
Free energy:    -37.587085
Extrapolated:   -37.587085

Fermi levels: -4.64391, -4.64391

 Band  Eigenvalues  Occupancy
    0    -30.58950    2.00000
    1    -26.17293    2.00000
    2    -20.30576    2.00000
    3    -15.26060    2.00000
    4    -13.37128    2.00000
    5    -12.98631    2.00000
    6    -12.50050    2.00000
    7    -10.89061    2.00000
    8    -10.47175    2.00000
    9     -7.45227    2.00000
   10     -6.89976    2.00000
   11     -6.42931    2.00000
   12     -2.85851    0.00000
   13      1.65911    0.00000
   14      2.19510    0.00000
   15      2.96093    0.00000
   16      3.25424    0.00000
   17      5.57629    0.00000
   18      7.64488    0.00000

Gap: 3.571 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.010     0.010   0.0% |
LCAO WFS Initialize:                 1.386     0.040   0.1% |
 Hamiltonian:                        1.346     0.000   0.0% |
  Atomic:                            0.027     0.027   0.1% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.021     0.021   0.1% |
  Hartree integrate/restrict:        0.026     0.026   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.384     0.007   0.0% |
   Communicate bwd 0:                0.077     0.077   0.3% |
   Communicate bwd 1:                0.072     0.072   0.3% |
   Communicate fwd 0:                0.073     0.073   0.3% |
   Communicate fwd 1:                0.080     0.080   0.3% |
   fft:                              0.023     0.023   0.1% |
   fft2:                             0.052     0.052   0.2% |
  XC 3D grid:                        0.877     0.877   3.2% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          22.471     0.004   0.0% |
 Density:                            0.869     0.000   0.0% |
  Atomic density matrices:           0.065     0.065   0.2% |
  Mix:                               0.635     0.635   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.164     0.010   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.152     0.152   0.6% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       21.358     0.002   0.0% |
  Atomic:                            0.434     0.434   1.6% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.024     0.024   0.1% |
  Communicate:                       0.343     0.343   1.3% ||
  Hartree integrate/restrict:        0.401     0.401   1.5% ||
  Poisson:                           6.165     0.118   0.4% |
   Communicate bwd 0:                1.246     1.246   4.6% |-|
   Communicate bwd 1:                1.175     1.175   4.3% |-|
   Communicate fwd 0:                1.160     1.160   4.3% |-|
   Communicate fwd 1:                1.271     1.271   4.7% |-|
   fft:                              0.363     0.363   1.3% ||
   fft2:                             0.832     0.832   3.1% ||
  XC 3D grid:                       13.930    13.930  51.5% |--------------------|
  vbar:                              0.060     0.060   0.2% |
 LCAO eigensolver:                   0.240     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.045     0.045   0.2% |
  Orbital Layouts:                   0.016     0.016   0.1% |
  Potential matrix:                  0.170     0.170   0.6% |
  Residual:                          0.004     0.004   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               1.017     1.017   3.8% |-|
mktci:                               0.001     0.001   0.0% |
Other:                               2.168     2.168   8.0% |--|
-----------------------------------------------------------
Total:                                        27.060 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:38:20 2019
