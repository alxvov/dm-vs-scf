
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:58:55 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3574.448222

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*104*112 grid
  Fine grid: 208*208*224 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 57.42 MiB
    Density: 36.40 MiB
      Arrays: 29.14 MiB
      Localized functions: 0.71 MiB
      Mixer: 6.55 MiB
    Hamiltonian: 19.12 MiB
      Arrays: 19.06 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.91 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.86 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 54
Number of bands in calculation: 14
Bands to converge: occupied states only
Number of valence electrons: 16

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |             N                        |  
 |         |             C                        |  
 |         |                                      |  
 |         |             CH                       |  
 |         |           H   H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.887664    7.512493    7.375440    ( 0.0000,  0.0000,  0.0000)
   1 C      7.887664    7.512493    8.836244    ( 0.0000,  0.0000,  0.0000)
   2 N      7.887664    7.512493   10.014576    ( 0.0000,  0.0000,  0.0000)
   3 H      7.887664    8.537479    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      8.775328    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.775328    0.000000    0.000000   104     0.1517
  2. axis:    no     0.000000   15.537479    0.000000   104     0.1494
  3. axis:    no     0.000000    0.000000   17.014576   112     0.1519

  Lengths:  15.775328  15.537479  17.014576
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1510

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:58:59   +inf   +inf   -37.062704    0      1      
iter:   2  00:59:01  +1.15  -0.75   -35.879752    0      1      
iter:   3  00:59:02  +0.39  -0.96   -35.535580    0      1      
iter:   4  00:59:03  +0.33  -1.15   -35.411574    0      1      
iter:   5  00:59:05  -0.99  -1.66   -35.401141    0      1      
iter:   6  00:59:06  -1.05  -2.01   -35.396051    0      1      
iter:   7  00:59:08  -2.33  -2.56   -35.395748    0      1      
iter:   8  00:59:09  -3.18  -2.88   -35.395683    0      1      
iter:   9  00:59:10  -3.21  -3.13   -35.395661    0      1      
iter:  10  00:59:12  -4.80  -3.54   -35.395659    0      1      
iter:  11  00:59:13  -4.85  -3.73   -35.395657    0      1      
iter:  12  00:59:15  -5.64  -4.16   -35.395657    0      1      
iter:  13  00:59:16  -6.57  -4.75   -35.395657    0      1      
iter:  14  00:59:17  -6.69  -4.96   -35.395657    0      1      
iter:  15  00:59:19  -8.59  -5.36   -35.395657    0      1      
iter:  16  00:59:20  -8.73  -5.52   -35.395657    0      1      
iter:  17  00:59:22  -9.34  -6.19   -35.395657    0      1      
iter:  18  00:59:23 -10.23  -6.58   -35.395657    0      1      

Converged after 18 iterations.

Dipole moment: (0.000000, -0.000015, -0.762993) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3574.448222)

Kinetic:        +25.937968
Potential:      -30.764457
External:        +0.000000
XC:             -30.718562
Entropy (-ST):   +0.000000
Local:           +0.149394
--------------------------
Free energy:    -35.395657
Extrapolated:   -35.395657

Fermi levels: -3.94179, -3.94179

 Band  Eigenvalues  Occupancy
    0    -22.29453    2.00000
    1    -19.31527    2.00000
    2    -12.70708    2.00000
    3    -11.37009    2.00000
    4    -11.37001    2.00000
    5     -8.28809    2.00000
    6     -7.80151    2.00000
    7     -7.80101    2.00000
    8     -0.08258    0.00000
    9     -0.08210    0.00000
   10      1.43954    0.00000
   11      3.44988    0.00000
   12      3.44997    0.00000
   13      4.04459    0.00000

Gap: 7.718 eV
Transition (v -> c):
  (s=0, k=0, n=7, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.010     0.010   0.0% |
LCAO WFS Initialize:                 1.390     0.038   0.1% |
 Hamiltonian:                        1.352     0.000   0.0% |
  Atomic:                            0.027     0.027   0.1% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.021     0.021   0.1% |
  Hartree integrate/restrict:        0.030     0.030   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.389     0.007   0.0% |
   Communicate bwd 0:                0.078     0.078   0.3% |
   Communicate bwd 1:                0.070     0.070   0.3% |
   Communicate fwd 0:                0.073     0.073   0.3% |
   Communicate fwd 1:                0.081     0.081   0.3% |
   fft:                              0.025     0.025   0.1% |
   fft2:                             0.056     0.056   0.2% |
  XC 3D grid:                        0.875     0.875   3.1% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.002     0.002   0.0% |
SCF-cycle:                          23.898     0.004   0.0% |
 Density:                            0.888     0.000   0.0% |
  Atomic density matrices:           0.025     0.025   0.1% |
  Mix:                               0.661     0.661   2.4% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.004     0.004   0.0% |
  Pseudo density:                    0.195     0.010   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.183     0.183   0.7% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       22.770     0.002   0.0% |
  Atomic:                            0.469     0.469   1.7% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.018     0.018   0.1% |
  Communicate:                       0.362     0.362   1.3% ||
  Hartree integrate/restrict:        0.461     0.461   1.7% ||
  Poisson:                           6.641     0.127   0.5% |
   Communicate bwd 0:                1.337     1.337   4.8% |-|
   Communicate bwd 1:                1.244     1.244   4.5% |-|
   Communicate fwd 0:                1.227     1.227   4.4% |-|
   Communicate fwd 1:                1.351     1.351   4.8% |-|
   fft:                              0.412     0.412   1.5% ||
   fft2:                             0.944     0.944   3.4% ||
  XC 3D grid:                       14.755    14.755  53.0% |--------------------|
  vbar:                              0.063     0.063   0.2% |
 LCAO eigensolver:                   0.236     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.002     0.002   0.0% |
  Distribute overlap matrix:         0.020     0.020   0.1% |
  Orbital Layouts:                   0.011     0.011   0.0% |
  Potential matrix:                  0.198     0.198   0.7% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.002     0.002   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.501     0.501   1.8% ||
mktci:                               0.001     0.001   0.0% |
Other:                               2.054     2.054   7.4% |--|
-----------------------------------------------------------
Total:                                        27.859 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:59:23 2019
