
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:51:44 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2521.839782

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 42.60 MiB
    Density: 27.22 MiB
      Arrays: 21.92 MiB
      Localized functions: 0.39 MiB
      Mixer: 4.91 MiB
    Hamiltonian: 14.37 MiB
      Arrays: 14.34 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 1.01 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.00 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 31
Number of bands in calculation: 9
Bands to converge: occupied states only
Number of valence electrons: 10

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            N                    |  
 |        |                                 |  
 |        |            C                    |  
 |        |            H                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 C      7.000000    7.000000    8.068999    ( 0.0000,  0.0000,  0.0000)
   1 N      7.000000    7.000000    9.245207    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.245207   108     0.1504

  Lengths:  14.000000  14.000000  16.245207
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1516

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:51:47   +inf   +inf   -19.563428    0      1      
iter:   2  01:51:48  +1.36  -0.82   -18.973878    0      1      
iter:   3  01:51:49  +0.44  -1.01   -18.808995    0      1      
iter:   4  01:51:50  +0.61  -1.19   -18.739443    0      1      
iter:   5  01:51:52  -1.02  -1.74   -18.732074    0      1      
iter:   6  01:51:53  -0.71  -1.99   -18.727455    0      1      
iter:   7  01:51:54  -2.54  -2.77   -18.727417    0      1      
iter:   8  01:51:55  -4.22  -3.03   -18.727409    0      1      
iter:   9  01:51:56  -3.77  -3.17   -18.727402    0      1      
iter:  10  01:51:57  -5.01  -3.84   -18.727401    0      1      
iter:  11  01:51:58  -5.40  -4.16   -18.727401    0      1      
iter:  12  01:51:59  -6.57  -4.67   -18.727401    0      1      
iter:  13  01:52:00  -7.35  -4.97   -18.727401    0      1      
iter:  14  01:52:01  -7.33  -5.15   -18.727401    0      1      
iter:  15  01:52:02  -9.13  -5.53   -18.727401    0      1      
iter:  16  01:52:03  -8.92  -5.66   -18.727401    0      1      
iter:  17  01:52:04  -9.65  -6.18   -18.727401    0      1      
iter:  18  01:52:05 -11.28  -6.74   -18.727401    0      1      

Converged after 18 iterations.

Dipole moment: (-0.000000, -0.000000, -0.546259) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2521.839782)

Kinetic:        +12.998138
Potential:      -16.246231
External:        +0.000000
XC:             -15.602206
Entropy (-ST):   +0.000000
Local:           +0.122897
--------------------------
Free energy:    -18.727401
Extrapolated:   -18.727401

Fermi levels: -4.73916, -4.73916

 Band  Eigenvalues  Occupancy
    0    -22.78658    2.00000
    1    -15.00000    2.00000
    2     -8.92576    2.00000
    3     -8.70659    2.00000
    4     -8.70659    2.00000
    5     -0.77172    0.00000
    6     -0.77172    0.00000
    7      1.74109    0.00000
    8      5.96532    0.00000

Gap: 7.935 eV
Transition (v -> c):
  (s=0, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.005     0.005   0.0% |
LCAO WFS Initialize:                 1.059     0.028   0.1% |
 Hamiltonian:                        1.031     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.048     0.048   0.2% |
  Hartree integrate/restrict:        0.020     0.020   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.299     0.005   0.0% |
   Communicate bwd 0:                0.060     0.060   0.3% |
   Communicate bwd 1:                0.055     0.055   0.3% |
   Communicate fwd 0:                0.053     0.053   0.2% |
   Communicate fwd 1:                0.059     0.059   0.3% |
   fft:                              0.017     0.017   0.1% |
   fft2:                             0.050     0.050   0.2% |
  XC 3D grid:                        0.660     0.660   3.1% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          18.018     0.004   0.0% |
 Density:                            0.574     0.000   0.0% |
  Atomic density matrices:           0.037     0.037   0.2% |
  Mix:                               0.481     0.481   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.050     0.007   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.042     0.042   0.2% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       17.355     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.007     0.007   0.0% |
  Communicate:                       0.813     0.813   3.8% |-|
  Hartree integrate/restrict:        0.333     0.333   1.6% ||
  Poisson:                           5.105     0.077   0.4% |
   Communicate bwd 0:                1.021     1.021   4.8% |-|
   Communicate bwd 1:                0.942     0.942   4.4% |-|
   Communicate fwd 0:                0.903     0.903   4.2% |-|
   Communicate fwd 1:                1.016     1.016   4.8% |-|
   fft:                              0.282     0.282   1.3% ||
   fft2:                             0.863     0.863   4.0% |-|
  XC 3D grid:                       11.040    11.040  51.7% |--------------------|
  vbar:                              0.050     0.050   0.2% |
 LCAO eigensolver:                   0.085     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.033     0.033   0.2% |
  Orbital Layouts:                   0.005     0.005   0.0% |
  Potential matrix:                  0.042     0.042   0.2% |
  Residual:                          0.002     0.002   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.537     0.537   2.5% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.743     1.743   8.2% |--|
-----------------------------------------------------------
Total:                                        21.366 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 01:52:05 2019
