
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 02:11:22 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6480.770010

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*108*104 grid
  Fine grid: 208*216*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 448.26 MiB
  Calculator: 54.77 MiB
    Density: 35.07 MiB
      Arrays: 28.09 MiB
      Localized functions: 0.67 MiB
      Mixer: 6.31 MiB
    Hamiltonian: 18.43 MiB
      Arrays: 18.37 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.27 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.23 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 49
Number of bands in calculation: 14
Bands to converge: occupied states only
Number of valence electrons: 20

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |           H  C H                     |  
 |         |              F                       |  
 |         |             F                        |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.908369    8.109716    7.793504    ( 0.0000,  0.0000,  0.0000)
   1 F      7.908369    9.219432    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 F      7.908369    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.109716    8.397300    ( 0.0000,  0.0000,  0.0000)
   4 H      8.816738    8.109716    8.397300    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.816738    0.000000    0.000000   104     0.1521
  2. axis:    no     0.000000   16.219432    0.000000   108     0.1502
  3. axis:    no     0.000000    0.000000   15.397300   104     0.1481

  Lengths:  15.816738  16.219432  15.397300
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1501

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  02:11:25   +inf   +inf   -23.646215    0      1      
iter:   2  02:11:27  +0.32  -1.02   -23.250564    0      1      
iter:   3  02:11:28  +0.68  -1.17   -22.980338    0      1      
iter:   4  02:11:29  -0.52  -1.82   -22.967496    0      1      
iter:   5  02:11:31  -2.14  -2.38   -22.966474    0      1      
iter:   6  02:11:32  -1.77  -2.62   -22.965876    0      1      
iter:   7  02:11:33  -4.22  -3.33   -22.965865    0      1      
iter:   8  02:11:35  -4.17  -3.53   -22.965862    0      1      
iter:   9  02:11:36  -4.53  -3.94   -22.965861    0      1      
iter:  10  02:11:37  -6.16  -4.29   -22.965861    0      1      
iter:  11  02:11:39  -6.06  -4.48   -22.965861    0      1      
iter:  12  02:11:40  -6.67  -5.04   -22.965861    0      1      
iter:  13  02:11:41  -7.80  -5.46   -22.965861    0      1      
iter:  14  02:11:43  -8.27  -5.65   -22.965861    0      1      
iter:  15  02:11:44  -9.08  -5.97   -22.965861    0      1      
iter:  16  02:11:45  -9.63  -6.21   -22.965861    0      1      
iter:  17  02:11:47 -10.35  -6.59   -22.965861    0      1      

Converged after 17 iterations.

Dipole moment: (-0.000000, -0.000000, 0.324461) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6480.770010)

Kinetic:        +27.729677
Potential:      -26.940398
External:        +0.000000
XC:             -23.977750
Entropy (-ST):   +0.000000
Local:           +0.222609
--------------------------
Free energy:    -22.965861
Extrapolated:   -22.965861

Fermi levels: -2.80701, -2.80701

 Band  Eigenvalues  Occupancy
    0    -31.03283    2.00000
    1    -29.54494    2.00000
    2    -17.29484    2.00000
    3    -13.07907    2.00000
    4    -12.99630    2.00000
    5    -12.94386    2.00000
    6     -9.39256    2.00000
    7     -9.37691    2.00000
    8     -8.96371    2.00000
    9     -7.70214    2.00000
   10      2.08812    0.00000
   11      2.38645    0.00000
   12      3.18983    0.00000
   13      3.26026    0.00000

Gap: 9.790 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.006     0.006   0.0% |
LCAO WFS Initialize:                 1.340     0.037   0.1% |
 Hamiltonian:                        1.303     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.049     0.049   0.2% |
  Hartree integrate/restrict:        0.025     0.025   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.377     0.012   0.0% |
   Communicate bwd 0:                0.074     0.074   0.3% |
   Communicate bwd 1:                0.068     0.068   0.3% |
   Communicate fwd 0:                0.069     0.069   0.3% |
   Communicate fwd 1:                0.076     0.076   0.3% |
   fft:                              0.022     0.022   0.1% |
   fft2:                             0.056     0.056   0.2% |
  XC 3D grid:                        0.843     0.843   3.3% ||
  vbar:                              0.004     0.004   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          21.369     0.004   0.0% |
 Density:                            0.715     0.000   0.0% |
  Atomic density matrices:           0.028     0.028   0.1% |
  Mix:                               0.590     0.590   2.3% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.091     0.009   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.081     0.081   0.3% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       20.526     0.002   0.0% |
  Atomic:                            0.005     0.005   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.008     0.008   0.0% |
  Communicate:                       0.790     0.790   3.1% ||
  Hartree integrate/restrict:        0.373     0.373   1.5% ||
  Poisson:                           5.903     0.152   0.6% |
   Communicate bwd 0:                1.179     1.179   4.7% |-|
   Communicate bwd 1:                1.114     1.114   4.4% |-|
   Communicate fwd 0:                1.051     1.051   4.2% |-|
   Communicate fwd 1:                1.165     1.165   4.6% |-|
   fft:                              0.346     0.346   1.4% ||
   fft2:                             0.896     0.896   3.6% ||
  XC 3D grid:                       13.387    13.387  53.1% |--------------------|
  vbar:                              0.059     0.059   0.2% |
 LCAO eigensolver:                   0.123     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.018     0.018   0.1% |
  Orbital Layouts:                   0.009     0.009   0.0% |
  Potential matrix:                  0.090     0.090   0.4% |
  Residual:                          0.003     0.003   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.503     0.503   2.0% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.971     1.971   7.8% |--|
-----------------------------------------------------------
Total:                                        25.194 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 02:11:47 2019
