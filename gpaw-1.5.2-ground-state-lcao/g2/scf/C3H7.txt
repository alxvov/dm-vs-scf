
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:43:04 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3170.315482

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 104*112*120 grid
  Fine grid: 208*224*240 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*224*240 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 419.46 MiB
  Calculator: 90.53 MiB
    Density: 53.92 MiB
      Arrays: 45.31 MiB
      Localized functions: 1.04 MiB
      Mixer: 7.58 MiB
    Hamiltonian: 33.75 MiB
      Arrays: 33.67 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.86 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.08 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.77 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 74
Number of bands in calculation: 16
Bands to converge: occupied states only
Number of valence electrons: 19

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |              H                       |  
 |         |                                      |  
 |         |           H CHH                      |  
 |         |              C                       |  
 |         |                                      |  
 |         |           H CHH                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.969380    8.539158    9.138477    ( 0.0000,  0.0000,  1.0000)
   1 C      7.969380    7.795566   10.430049    ( 0.0000,  0.0000,  0.0000)
   2 C      7.969380    7.795566    7.846905    ( 0.0000,  0.0000,  0.0000)
   3 H      7.632267    9.570637    9.138477    ( 0.0000,  0.0000,  0.0000)
   4 H      8.176574    8.454482   11.276954    ( 0.0000,  0.0000,  0.0000)
   5 H      8.176574    8.454482    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.310679   10.623110    ( 0.0000,  0.0000,  0.0000)
   7 H      8.722338    7.000000   10.424716    ( 0.0000,  0.0000,  0.0000)
   8 H      8.722338    7.000000    7.852238    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.310679    7.653844    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.722338    0.000000    0.000000   104     0.1512
  2. axis:    no     0.000000   16.570637    0.000000   112     0.1480
  3. axis:    no     0.000000    0.000000   18.276954   120     0.1523

  Lengths:  15.722338  16.570637  18.276954
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1505

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  01:43:08   +inf   +inf   -52.610967    0      1        +1.0000
iter:   2  01:43:11  +1.07  -0.69   -51.025787    0      1        +1.0000
iter:   3  01:43:13  +0.88  -0.88   -50.311751    0      1        +1.0000
iter:   4  01:43:16  +0.03  -1.22   -50.222055    0      1        +1.0000
iter:   5  01:43:18  -1.68  -2.09   -50.221492    0      1        +1.0000
iter:   6  01:43:21  -1.97  -2.40   -50.221221    0      1        +1.0000
iter:   7  01:43:23  -2.20  -2.58   -50.220963    0      1        +1.0000
iter:   8  01:43:26  -3.70  -3.18   -50.220948    0      1        +1.0000
iter:   9  01:43:28  -3.84  -3.47   -50.220945    0      1        +1.0000
iter:  10  01:43:31  -4.82  -3.72   -50.220945    0      1        +1.0000
iter:  11  01:43:33  -5.52  -3.95   -50.220945    0      1        +1.0000
iter:  12  01:43:36  -5.44  -4.24   -50.220945    0      1        +1.0000
iter:  13  01:43:38  -6.58  -4.63   -50.220945    0      1        +1.0000
iter:  14  01:43:41  -6.93  -4.90   -50.220945    0      1        +1.0000
iter:  15  01:43:43  -7.79  -5.19   -50.220945    0      1        +1.0000
iter:  16  01:43:46  -8.30  -5.43   -50.220945    0      1        +1.0000
iter:  17  01:43:48  -8.84  -5.70   -50.220945    0      1        +1.0000
iter:  18  01:43:51  -9.28  -5.95   -50.220945    0      1        +1.0000
iter:  19  01:43:53  -9.84  -6.31   -50.220945    0      1        +1.0000
iter:  20  01:43:56 -10.34  -6.60   -50.220945    0      1        +1.0000

Converged after 20 iterations.

Dipole moment: (-0.038637, -0.038252, 0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000002)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000,  0.313289)
   1 C  ( 0.000000,  0.000000, -0.013295)
   2 C  ( 0.000000,  0.000000, -0.013295)
   3 H  ( 0.000000,  0.000000, -0.005978)
   4 H  ( 0.000000,  0.000000,  0.003172)
   5 H  ( 0.000000,  0.000000,  0.003172)
   6 H  ( 0.000000,  0.000000,  0.024041)
   7 H  ( 0.000000,  0.000000,  0.008134)
   8 H  ( 0.000000,  0.000000,  0.008134)
   9 H  ( 0.000000,  0.000000,  0.024041)

Energy contributions relative to reference atoms: (reference = -3170.315482)

Kinetic:        +40.913950
Potential:      -45.680536
External:        +0.000000
XC:             -45.534289
Entropy (-ST):   +0.000000
Local:           +0.079931
--------------------------
Free energy:    -50.220945
Extrapolated:   -50.220945

Spin contamination: 0.058035 electrons
Fermi levels: -0.97896, -4.96066

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -19.28944    1.00000    -18.89889    1.00000
    1    -16.99270    1.00000    -16.93162    1.00000
    2    -13.87253    1.00000    -13.39179    1.00000
    3    -10.95935    1.00000    -10.82723    1.00000
    4    -10.36123    1.00000    -10.01609    1.00000
    5     -9.84257    1.00000     -9.70003    1.00000
    6     -9.26227    1.00000     -9.14707    1.00000
    7     -8.43683    1.00000     -8.32882    1.00000
    8     -8.24256    1.00000     -8.09926    1.00000
    9     -3.94836    1.00000     -1.82207    0.00000
   10      1.99044    0.00000      2.07316    0.00000
   11      2.70070    0.00000      2.81618    0.00000
   12      2.98169    0.00000      3.07183    0.00000
   13      3.68858    0.00000      3.86093    0.00000
   14      4.01813    0.00000      4.25706    0.00000
   15      4.18527    0.00000      4.26034    0.00000

Gap: 2.126 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.001     0.001   0.0% |
Basis functions set positions:       0.015     0.015   0.0% |
LCAO WFS Initialize:                 2.375     0.084   0.2% |
 Hamiltonian:                        2.291     0.003   0.0% |
  Atomic:                            0.088     0.010   0.0% |
   XC Correction:                    0.078     0.078   0.1% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.000     0.000   0.0% |
  Hartree integrate/restrict:        0.056     0.056   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.437     0.008   0.0% |
   Communicate bwd 0:                0.087     0.087   0.2% |
   Communicate bwd 1:                0.081     0.081   0.2% |
   Communicate fwd 0:                0.086     0.086   0.2% |
   Communicate fwd 1:                0.085     0.085   0.2% |
   fft:                              0.025     0.025   0.0% |
   fft2:                             0.064     0.064   0.1% |
  XC 3D grid:                        1.696     1.696   3.3% ||
  vbar:                              0.006     0.006   0.0% |
P tci:                               0.003     0.003   0.0% |
SCF-cycle:                          47.243     0.006   0.0% |
 Density:                            2.480     0.000   0.0% |
  Atomic density matrices:           0.218     0.218   0.4% |
  Mix:                               1.537     1.537   3.0% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.009     0.009   0.0% |
  Pseudo density:                    0.713     0.028   0.1% |
   Calculate density matrix:         0.002     0.002   0.0% |
   Construct density:                0.683     0.683   1.3% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       43.729     0.048   0.1% |
  Atomic:                            1.689     0.202   0.4% |
   XC Correction:                    1.487     1.487   2.9% ||
  Calculate atomic Hamiltonians:     0.043     0.043   0.1% |
  Communicate:                       0.001     0.001   0.0% |
  Hartree integrate/restrict:        1.075     1.075   2.1% ||
  Poisson:                           8.471     0.150   0.3% |
   Communicate bwd 0:                1.736     1.736   3.3% ||
   Communicate bwd 1:                1.594     1.594   3.1% ||
   Communicate fwd 0:                1.612     1.612   3.1% ||
   Communicate fwd 1:                1.717     1.717   3.3% ||
   fft:                              0.467     0.467   0.9% |
   fft2:                             1.194     1.194   2.3% ||
  XC 3D grid:                       32.321    32.321  62.0% |------------------------|
  vbar:                              0.081     0.081   0.2% |
 LCAO eigensolver:                   1.028     0.004   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.004     0.004   0.0% |
  Distribute overlap matrix:         0.196     0.196   0.4% |
  Orbital Layouts:                   0.040     0.040   0.1% |
  Potential matrix:                  0.774     0.774   1.5% ||
  Residual:                          0.010     0.010   0.0% |
ST tci:                              0.003     0.003   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.182     0.182   0.3% |
mktci:                               0.002     0.002   0.0% |
Other:                               2.269     2.269   4.4% |-|
-----------------------------------------------------------
Total:                                        52.093 100.0%

Memory usage: 448.26 MiB
Date: Sun Feb 17 01:43:56 2019
