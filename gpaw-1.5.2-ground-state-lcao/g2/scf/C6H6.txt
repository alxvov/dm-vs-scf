
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:20:35 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6240.709666

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 120*128*92 grid
  Fine grid: 240*256*184 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*256*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 69.70 MiB
    Density: 43.16 MiB
      Arrays: 34.03 MiB
      Localized functions: 1.47 MiB
      Mixer: 7.66 MiB
    Hamiltonian: 22.38 MiB
      Arrays: 22.26 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.16 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.18 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.97 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 12
Number of atomic orbitals: 108
Number of bands in calculation: 22
Bands to converge: occupied states only
Number of valence electrons: 30

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
             .--------------------------------------------.  
            /|                                            |  
           / |                                            |  
          /  |                                            |  
         /   |                                            |  
        /    |                                            |  
       /     |                                            |  
      /      |                                            |  
     /       |                                            |  
    /        |                                            |  
   /         |                                            |  
  /          |                                            |  
 *           |                                            |  
 |           |                 H                          |  
 |           |           H C  C  C  H                     |  
 |           |         H  C  C  C H                       |  
 |           |              H                             |  
 |           .--------------------------------------------.  
 |          /                                            /   
 |         /                                            /    
 |        /                                            /     
 |       /                                            /      
 |      /                                            /       
 |     /                                            /        
 |    /                                            /         
 |   /                                            /          
 |  /                                            /           
 | /                                            /            
 |/                                            /             
 *--------------------------------------------*              

Positions:
   0 C      9.149787   10.877608    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C     10.358107   10.179984    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 C     10.358107    8.784736    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 C      9.149787    8.087112    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 C      7.941467    8.784736    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 C      7.941467   10.179984    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      9.149787   11.964720    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H     11.299574   10.723540    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H     11.299574    8.241180    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      9.149787    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H      7.000000    8.241180    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.000000   10.723540    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.299574    0.000000    0.000000   120     0.1525
  2. axis:    no     0.000000   18.964720    0.000000   128     0.1482
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  18.299574  18.964720  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:20:39   +inf   +inf   -76.583172    0      1      
iter:   2  01:20:41  +0.54  -0.78   -74.731282    0      1      
iter:   3  01:20:42  +0.66  -0.95   -73.913669    0      1      
iter:   4  01:20:44  +0.13  -1.43   -73.849660    0      1      
iter:   5  01:20:46  -1.50  -2.12   -73.846215    0      1      
iter:   6  01:20:48  -1.89  -2.35   -73.845058    0      1      
iter:   7  01:20:49  -2.28  -2.55   -73.844188    0      1      
iter:   8  01:20:51  -4.98  -3.35   -73.844179    0      1      
iter:   9  01:20:53  -4.24  -3.53   -73.844173    0      1      
iter:  10  01:20:55  -7.15  -4.41   -73.844173    0      1      
iter:  11  01:20:56  -6.92  -4.56   -73.844173    0      1      
iter:  12  01:20:58  -7.05  -4.92   -73.844173    0      1      
iter:  13  01:21:00  -9.15  -5.38   -73.844173    0      1      
iter:  14  01:21:02  -8.60  -5.54   -73.844173    0      1      
iter:  15  01:21:03 -10.52  -6.48   -73.844173    0      1      

Converged after 15 iterations.

Dipole moment: (0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6240.709666)

Kinetic:        +57.899581
Potential:      -64.644373
External:        +0.000000
XC:             -67.281314
Entropy (-ST):   +0.000000
Local:           +0.181933
--------------------------
Free energy:    -73.844173
Extrapolated:   -73.844173

Fermi levels: -3.54311, -3.54311

 Band  Eigenvalues  Occupancy
    0    -21.15130    2.00000
    1    -18.38553    2.00000
    2    -18.38552    2.00000
    3    -14.77112    2.00000
    4    -14.77047    2.00000
    5    -12.79156    2.00000
    6    -11.13787    2.00000
    7    -10.78103    2.00000
    8    -10.12228    2.00000
    9    -10.12226    2.00000
   10     -8.88189    2.00000
   11     -8.13640    2.00000
   12     -8.13618    2.00000
   13     -6.15827    2.00000
   14     -6.15819    2.00000
   15     -0.92804    0.00000
   16     -0.92796    0.00000
   17      1.63383    0.00000
   18      2.88778    0.00000
   19      2.88794    0.00000
   20      3.11601    0.00000
   21      3.85329    0.00000

Gap: 5.230 eV
Transition (v -> c):
  (s=0, k=0, n=14, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=15, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.001     0.001   0.0% |
Basis functions set positions:       0.018     0.018   0.1% |
LCAO WFS Initialize:                 1.676     0.049   0.2% |
 Hamiltonian:                        1.628     0.000   0.0% |
  Atomic:                            0.048     0.001   0.0% |
   XC Correction:                    0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
  Communicate:                       0.047     0.047   0.2% |
  Hartree integrate/restrict:        0.033     0.033   0.1% |
  Initialize Hamiltonian:            0.004     0.004   0.0% |
  Poisson:                           0.455     0.013   0.0% |
   Communicate bwd 0:                0.088     0.088   0.3% |
   Communicate bwd 1:                0.085     0.085   0.3% |
   Communicate fwd 0:                0.083     0.083   0.3% |
   Communicate fwd 1:                0.092     0.092   0.3% |
   fft:                              0.024     0.024   0.1% |
   fft2:                             0.070     0.070   0.2% |
  XC 3D grid:                        1.033     1.033   3.6% ||
  vbar:                              0.006     0.006   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          24.352     0.004   0.0% |
 Density:                            1.150     0.000   0.0% |
  Atomic density matrices:           0.098     0.098   0.3% |
  Mix:                               0.663     0.663   2.3% ||
  Multipole moments:                 0.004     0.004   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.382     0.011   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.370     0.370   1.3% ||
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       22.667     0.001   0.0% |
  Atomic:                            0.662     0.008   0.0% |
   XC Correction:                    0.654     0.654   2.3% ||
  Calculate atomic Hamiltonians:     0.035     0.035   0.1% |
  Communicate:                       0.664     0.664   2.3% ||
  Hartree integrate/restrict:        0.429     0.429   1.5% ||
  Poisson:                           6.401     0.183   0.6% |
   Communicate bwd 0:                1.251     1.251   4.4% |-|
   Communicate bwd 1:                1.183     1.183   4.2% |-|
   Communicate fwd 0:                1.160     1.160   4.1% |-|
   Communicate fwd 1:                1.291     1.291   4.5% |-|
   fft:                              0.344     0.344   1.2% |
   fft2:                             0.988     0.988   3.5% ||
  XC 3D grid:                       14.414    14.414  50.6% |-------------------|
  vbar:                              0.061     0.061   0.2% |
 LCAO eigensolver:                   0.530     0.002   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.067     0.067   0.2% |
  Orbital Layouts:                   0.030     0.030   0.1% |
  Potential matrix:                  0.425     0.425   1.5% ||
  Residual:                          0.005     0.005   0.0% |
ST tci:                              0.004     0.004   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.180     0.180   0.6% |
mktci:                               0.002     0.002   0.0% |
Other:                               2.273     2.273   8.0% |--|
-----------------------------------------------------------
Total:                                        28.507 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:21:03 2019
