
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:11:02 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Li-setup:
  name: Lithium
  id: 830b4218f175192f93f959cfc0aad614
  Z: 3
  valence: 1
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Li.PBE.gz
  cutoffs: 1.06(comp), 1.91(filt), 2.55(core), lmax=2
  valence states:
                energy  radius
    2s(1.00)    -2.874   1.058
    2p(0.00)    -1.090   1.058
    *s          24.337   1.058

  LCAO basis set for Li:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Li.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=10.8906 Bohr: 2s-sz confined orbital
      l=0, rc=6.6719 Bohr: 2s-dz split-valence wave
      l=1, rc=10.8906 Bohr: p-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -215.277982

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 41.69 MiB
    Density: 26.70 MiB
      Arrays: 21.10 MiB
      Localized functions: 0.87 MiB
      Mixer: 4.73 MiB
    Hamiltonian: 13.87 MiB
      Arrays: 13.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.12 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.00 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.12 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 10
Number of bands in calculation: 5
Bands to converge: occupied states only
Number of valence electrons: 2

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Li                   |  
 |        |                                 |  
 |        |            H                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Li     7.000000    7.000000    8.640000    ( 0.0000,  0.0000,  0.0000)
   1 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.640000   104     0.1504

  Lengths:  14.000000  14.000000  15.640000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1516

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  01:11:05   +inf   +inf    -5.160670    0      1      
iter:   2  01:11:06  +1.87  -0.15    -4.063611    0      1      
iter:   3  01:11:07  +2.20  -0.33    -3.531244    0      1      
iter:   4  01:11:08  +1.84  -0.82    -3.651165    0      1      
iter:   5  01:11:09  +1.62  -0.63    -3.531207    0      1      
iter:   6  01:11:10  -0.69  -1.84    -3.530663    0      1      
iter:   7  01:11:11  -1.38  -2.10    -3.530712    0      1      
iter:   8  01:11:12  -2.51  -2.72    -3.530703    0      1      
iter:   9  01:11:13  -4.61  -2.96    -3.530704    0      1      
iter:  10  01:11:14  -3.61  -3.07    -3.530705    0      1      
iter:  11  01:11:15  -6.16  -4.01    -3.530705    0      1      
iter:  12  01:11:16  -5.62  -4.13    -3.530705    0      1      
iter:  13  01:11:18  -7.21  -4.82    -3.530705    0      1      
iter:  14  01:11:19  -9.16  -5.43    -3.530705    0      1      
iter:  15  01:11:20  -9.01  -5.58    -3.530705    0      1      
iter:  16  01:11:21 -10.66  -5.94    -3.530705    0      1      

Converged after 16 iterations.

Dipole moment: (0.000000, -0.000000, 1.024949) |e|*Ang

Energy contributions relative to reference atoms: (reference = -215.277982)

Kinetic:         +4.578827
Potential:       -3.914798
External:        +0.000000
XC:              -4.184742
Entropy (-ST):   +0.000000
Local:           -0.009993
--------------------------
Free energy:     -3.530705
Extrapolated:    -3.530705

Fermi levels: -2.63664, -2.63664

 Band  Eigenvalues  Occupancy
    0     -4.18142    2.00000
    1     -1.09186    0.00000
    2      3.09413    0.00000
    3      3.09413    0.00000
    4      4.75393    0.00000

Gap: 3.090 eV
Transition (v -> c):
  (s=0, k=0, n=0, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=1, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.006     0.006   0.0% |
LCAO WFS Initialize:                 1.043     0.028   0.2% |
 Hamiltonian:                        1.015     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
  Communicate:                       0.042     0.042   0.2% |
  Hartree integrate/restrict:        0.019     0.019   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.289     0.004   0.0% |
   Communicate bwd 0:                0.057     0.057   0.3% |
   Communicate bwd 1:                0.053     0.053   0.3% |
   Communicate fwd 0:                0.051     0.051   0.3% |
   Communicate fwd 1:                0.057     0.057   0.3% |
   fft:                              0.019     0.019   0.1% |
   fft2:                             0.048     0.048   0.3% |
  XC 3D grid:                        0.659     0.659   3.6% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          15.448     0.004   0.0% |
 Density:                            0.444     0.000   0.0% |
  Atomic density matrices:           0.013     0.013   0.1% |
  Mix:                               0.397     0.397   2.2% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.030     0.006   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.024     0.024   0.1% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       14.964     0.002   0.0% |
  Atomic:                            0.005     0.004   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.007     0.007   0.0% |
  Communicate:                       0.642     0.642   3.5% ||
  Hartree integrate/restrict:        0.307     0.307   1.7% ||
  Poisson:                           4.352     0.066   0.4% |
   Communicate bwd 0:                0.858     0.858   4.7% |-|
   Communicate bwd 1:                0.802     0.802   4.4% |-|
   Communicate fwd 0:                0.761     0.761   4.2% |-|
   Communicate fwd 1:                0.856     0.856   4.7% |-|
   fft:                              0.282     0.282   1.5% ||
   fft2:                             0.728     0.728   4.0% |-|
  XC 3D grid:                        9.606     9.606  52.8% |--------------------|
  vbar:                              0.043     0.043   0.2% |
 LCAO eigensolver:                   0.036     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.008     0.008   0.0% |
  Orbital Layouts:                   0.002     0.002   0.0% |
  Potential matrix:                  0.022     0.022   0.1% |
  Residual:                          0.001     0.001   0.0% |
ST tci:                              0.000     0.000   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.078     0.078   0.4% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.600     1.600   8.8% |---|
-----------------------------------------------------------
Total:                                        18.177 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:11:21 2019
