
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 01:00:23 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -7923.814737

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 108*108*96 grid
  Fine grid: 216*216*192 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 72.03 MiB
    Density: 43.28 MiB
      Arrays: 36.21 MiB
      Localized functions: 1.03 MiB
      Mixer: 6.04 MiB
    Hamiltonian: 26.99 MiB
      Arrays: 26.91 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 1.76 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.74 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 28
Number of bands in calculation: 7
Bands to converge: occupied states only
Number of valence electrons: 7

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |              Si                       |  
 |         |          H     H                      |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 Si     8.223937    7.706640    7.449360    ( 0.0000,  0.0000,  1.0000)
   1 H      8.223937    9.119920    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      9.447874    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.447874    0.000000    0.000000   108     0.1523
  2. axis:    no     0.000000   16.119920    0.000000   108     0.1493
  3. axis:    no     0.000000    0.000000   14.449360    96     0.1505

  Lengths:  16.447874  16.119920  14.449360
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  01:00:27   +inf   +inf   -13.817648    0      1        +1.0000
iter:   2  01:00:29  +1.06  -0.59   -13.716974    0      1        +1.0000
iter:   3  01:00:31  +1.13  -0.74   -13.420102    0      1        +1.0000
iter:   4  01:00:32  -0.16  -1.57   -13.410066    0      1        +1.0000
iter:   5  01:00:34  -1.05  -1.83   -13.412756    0      1        +1.0000
iter:   6  01:00:36  -2.56  -2.31   -13.412683    0      1        +1.0000
iter:   7  01:00:38  -2.17  -2.49   -13.412611    0      1        +1.0000
iter:   8  01:00:40  -4.55  -3.62   -13.412612    0      1        +1.0000
iter:   9  01:00:42  -4.53  -3.83   -13.412611    0      1        +1.0000
iter:  10  01:00:44  -6.29  -4.41   -13.412611    0      1        +1.0000
iter:  11  01:00:46  -6.28  -4.58   -13.412611    0      1        +1.0000
iter:  12  01:00:48  -7.41  -5.30   -13.412611    0      1        +1.0000
iter:  13  01:00:50  -8.46  -5.50   -13.412611    0      1        +1.0000
iter:  14  01:00:52  -9.38  -5.98   -13.412611    0      1        +1.0000
iter:  15  01:00:54  -9.68  -6.20   -13.412611    0      1        +1.0000
iter:  16  01:00:56 -10.84  -6.61   -13.412611    0      1        +1.0000

Converged after 16 iterations.

Dipole moment: (-0.000000, -0.000018, 0.020627) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000000)
Local magnetic moments:
   0 Si ( 0.000000,  0.000000,  0.309243)
   1 H  ( 0.000000,  0.000000,  0.002955)
   2 H  ( 0.000000,  0.000000,  0.002961)
   3 H  ( 0.000000,  0.000000,  0.002961)

Energy contributions relative to reference atoms: (reference = -7923.814737)

Kinetic:        +12.733844
Potential:      -13.749934
External:        +0.000000
XC:             -12.368473
Entropy (-ST):   +0.000000
Local:           -0.028048
--------------------------
Free energy:    -13.412611
Extrapolated:   -13.412611

Spin contamination: 0.016932 electrons
Fermi levels: -2.22413, -5.90496

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -13.28643    1.00000    -12.78939    1.00000
    1     -8.63964    1.00000     -8.45842    1.00000
    2     -8.63956    1.00000     -8.45830    1.00000
    3     -5.19844    1.00000     -3.35163    0.00000
    4      0.75019    0.00000      1.27853    0.00000
    5      0.75067    0.00000      1.27913    0.00000
    6      1.06757    0.00000      1.51117    0.00000

Gap: 1.847 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=3, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.008     0.008   0.0% |
LCAO WFS Initialize:                 1.945     0.067   0.2% |
 Hamiltonian:                        1.878     0.002   0.0% |
  Atomic:                            0.044     0.044   0.1% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.064     0.064   0.2% |
  Hartree integrate/restrict:        0.044     0.044   0.1% |
  Initialize Hamiltonian:            0.003     0.003   0.0% |
  Poisson:                           0.350     0.010   0.0% |
   Communicate bwd 0:                0.069     0.069   0.2% |
   Communicate bwd 1:                0.064     0.064   0.2% |
   Communicate fwd 0:                0.065     0.065   0.2% |
   Communicate fwd 1:                0.073     0.073   0.2% |
   fft:                              0.022     0.022   0.1% |
   fft2:                             0.046     0.046   0.1% |
  XC 3D grid:                        1.365     1.365   4.1% |-|
  vbar:                              0.005     0.005   0.0% |
P tci:                               0.001     0.001   0.0% |
SCF-cycle:                          29.389     0.005   0.0% |
 Density:                            1.206     0.000   0.0% |
  Atomic density matrices:           0.029     0.029   0.1% |
  Mix:                               0.984     0.984   2.9% ||
  Multipole moments:                 0.002     0.002   0.0% |
  Normalize:                         0.005     0.005   0.0% |
  Pseudo density:                    0.186     0.017   0.1% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.168     0.168   0.5% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       27.977     0.030   0.1% |
  Atomic:                            0.669     0.669   2.0% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.010     0.010   0.0% |
  Communicate:                       0.951     0.951   2.8% ||
  Hartree integrate/restrict:        0.612     0.612   1.8% ||
  Poisson:                           5.267     0.153   0.5% |
   Communicate bwd 0:                1.053     1.053   3.1% ||
   Communicate bwd 1:                1.016     1.016   3.0% ||
   Communicate fwd 0:                0.975     0.975   2.9% ||
   Communicate fwd 1:                1.067     1.067   3.2% ||
   fft:                              0.318     0.318   1.0% |
   fft2:                             0.684     0.684   2.0% ||
  XC 3D grid:                       20.387    20.387  60.9% |-----------------------|
  vbar:                              0.052     0.052   0.2% |
 LCAO eigensolver:                   0.202     0.002   0.0% |
  Calculate projections:             0.001     0.001   0.0% |
  DenseAtomicCorrection:             0.003     0.003   0.0% |
  Distribute overlap matrix:         0.019     0.019   0.1% |
  Orbital Layouts:                   0.007     0.007   0.0% |
  Potential matrix:                  0.166     0.166   0.5% |
  Residual:                          0.004     0.004   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.201     0.201   0.6% |
mktci:                               0.001     0.001   0.0% |
Other:                               1.914     1.914   5.7% |-|
-----------------------------------------------------------
Total:                                        33.462 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 01:00:56 2019
