
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:55:13 2019
Arch:   x86_64
Pid:    23260
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  h: 0.15
  mode: {interpolation: 3,
         name: lcao}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Na-setup:
  name: Sodium
  id: d7ecbc49209718622bcbe287195dca2a
  Z: 11
  valence: 7
  core: 4
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Na.PBE.gz
  cutoffs: 1.18(comp), 2.17(filt), 2.59(core), lmax=2
  valence states:
                energy  radius
    3s(1.00)    -2.744   1.201
    2p(6.00)   -28.672   1.217
    3p(0.00)    -0.743   1.217
    *s          24.468   1.201
    *d           0.000   1.238

  LCAO basis set for Na:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Na.dzp.basis.gz
    Number of radial functions: 7
    Number of spherical harmonics: 17
      l=0, rc=11.4062 Bohr: 3s-sz confined orbital
      l=1, rc=3.6719 Bohr: 2p-sz confined orbital
      l=1, rc=15.9375 Bohr: 3p-sz confined orbital
      l=0, rc=6.9531 Bohr: 3s-dz split-valence wave
      l=1, rc=2.4219 Bohr: 2p-dz split-valence wave
      l=1, rc=10.0469 Bohr: 3p-dz split-valence wave
      l=1, rc=11.4062 Bohr: p-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -16973.559893

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   LCAO using direct dense diagonalizer 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 417.51 MiB
  Calculator: 46.93 MiB
    Density: 28.43 MiB
      Arrays: 21.92 MiB
      Localized functions: 1.59 MiB
      Mixer: 4.91 MiB
    Hamiltonian: 14.46 MiB
      Arrays: 14.34 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.04 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 4.03 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 30
Number of bands in calculation: 11
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Cl                   |  
 |        |                                 |  
 |        |                                 |  
 |        |            Na                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Na     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 Cl     7.000000    7.000000    9.390970    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.390970   108     0.1518

  Lengths:  14.000000  14.000000  16.390970
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1520

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:55:16   +inf   +inf    -5.070652    0      1      
iter:   2  00:55:17  +0.32  -1.00    -4.353478    0      1      
iter:   3  00:55:18  +0.08  -1.16    -4.076522    0      1      
iter:   4  00:55:20  -0.75  -1.33    -4.012808    0      1      
iter:   5  00:55:21  -0.49  -1.82    -3.991218    0      1      
iter:   6  00:55:22  -2.38  -2.58    -3.990921    0      1      
iter:   7  00:55:23  -3.10  -2.76    -3.990938    0      1      
iter:   8  00:55:24  -4.38  -3.26    -3.990929    0      1      
iter:   9  00:55:25  -4.88  -3.41    -3.990925    0      1      
iter:  10  00:55:26  -5.42  -3.66    -3.990924    0      1      
iter:  11  00:55:27  -6.75  -4.59    -3.990924    0      1      
iter:  12  00:55:29  -6.65  -4.76    -3.990924    0      1      
iter:  13  00:55:30  -9.25  -5.20    -3.990924    0      1      
iter:  14  00:55:31  -9.23  -5.42    -3.990924    0      1      
iter:  15  00:55:32  -9.47  -5.80    -3.990924    0      1      
iter:  16  00:55:33 -11.00  -6.51    -3.990924    0      1      

Converged after 16 iterations.

Dipole moment: (-0.000000, -0.000000, -1.631683) |e|*Ang

Energy contributions relative to reference atoms: (reference = -16973.559893)

Kinetic:        +10.450696
Potential:       -8.332258
External:        +0.000000
XC:              -6.142913
Entropy (-ST):   +0.000000
Local:           +0.033551
--------------------------
Free energy:     -3.990924
Extrapolated:    -3.990924

Fermi levels: -3.52993, -3.52993

 Band  Eigenvalues  Occupancy
    0    -29.22063    2.00000
    1    -29.22063    2.00000
    2    -29.17209    2.00000
    3    -16.61554    2.00000
    4     -5.42611    2.00000
    5     -4.82923    2.00000
    6     -4.82923    2.00000
    7     -2.23062    0.00000
    8     -0.11716    0.00000
    9     -0.11716    0.00000
   10      0.74915    0.00000

Gap: 2.599 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                              incl.     excl.
-----------------------------------------------------------
Basic WFS set positions:             0.000     0.000   0.0% |
Basis functions set positions:       0.013     0.013   0.1% |
LCAO WFS Initialize:                 1.092     0.029   0.1% |
 Hamiltonian:                        1.063     0.000   0.0% |
  Atomic:                            0.000     0.000   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
  Communicate:                       0.064     0.064   0.3% |
  Hartree integrate/restrict:        0.020     0.020   0.1% |
  Initialize Hamiltonian:            0.001     0.001   0.0% |
  Poisson:                           0.298     0.005   0.0% |
   Communicate bwd 0:                0.060     0.060   0.3% |
   Communicate bwd 1:                0.055     0.055   0.3% |
   Communicate fwd 0:                0.053     0.053   0.3% |
   Communicate fwd 1:                0.059     0.059   0.3% |
   fft:                              0.016     0.016   0.1% |
   fft2:                             0.050     0.050   0.2% |
  XC 3D grid:                        0.676     0.676   3.4% ||
  vbar:                              0.003     0.003   0.0% |
P tci:                               0.000     0.000   0.0% |
SCF-cycle:                          16.839     0.004   0.0% |
 Density:                            0.577     0.000   0.0% |
  Atomic density matrices:           0.018     0.018   0.1% |
  Mix:                               0.435     0.435   2.2% ||
  Multipole moments:                 0.003     0.003   0.0% |
  Normalize:                         0.003     0.003   0.0% |
  Pseudo density:                    0.118     0.007   0.0% |
   Calculate density matrix:         0.001     0.001   0.0% |
   Construct density:                0.110     0.110   0.5% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                       16.130     0.002   0.0% |
  Atomic:                            0.005     0.004   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.011     0.011   0.1% |
  Communicate:                       0.967     0.967   4.8% |-|
  Hartree integrate/restrict:        0.303     0.303   1.5% ||
  Poisson:                           4.481     0.069   0.3% |
   Communicate bwd 0:                0.895     0.895   4.5% |-|
   Communicate bwd 1:                0.832     0.832   4.1% |-|
   Communicate fwd 0:                0.794     0.794   3.9% |-|
   Communicate fwd 1:                0.887     0.887   4.4% |-|
   fft:                              0.245     0.245   1.2% |
   fft2:                             0.759     0.759   3.8% |-|
  XC 3D grid:                       10.320    10.320  51.3% |--------------------|
  vbar:                              0.042     0.042   0.2% |
 LCAO eigensolver:                   0.128     0.001   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.012     0.012   0.1% |
  Orbital Layouts:                   0.004     0.004   0.0% |
  Potential matrix:                  0.108     0.108   0.5% |
  Residual:                          0.002     0.002   0.0% |
ST tci:                              0.001     0.001   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
TCI: Evaluate splines:               0.444     0.444   2.2% ||
mktci:                               0.001     0.001   0.0% |
Other:                               1.719     1.719   8.5% |--|
-----------------------------------------------------------
Total:                                        20.111 100.0%

Memory usage: 417.51 MiB
Date: Sun Feb 17 00:55:33 2019
