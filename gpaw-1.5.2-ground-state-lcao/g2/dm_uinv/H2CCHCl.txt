
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:51:19 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -14647.676113

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*112*92 grid
  Fine grid: 224*224*184 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 48.81 MiB
    Density: 28.64 MiB
      Arrays: 27.74 MiB
      Localized functions: 0.90 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 18.22 MiB
      Arrays: 18.15 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.96 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.89 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 54
Number of bands in calculation: 54
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |            HC  C H                     |  
 |         |           Cl     H                     |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      7.771098    8.610996    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C      9.074321    8.883487    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 Cl     7.139543    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    9.371943    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      9.827193    8.104407    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      9.403194    9.916105    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.827193    0.000000    0.000000   112     0.1502
  2. axis:    no     0.000000   16.916105    0.000000   112     0.1510
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  16.827193  16.916105  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1511

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:51:27  +0.61          -29.150774    0      1      
iter:   2  23:51:30  -0.17          -29.224801    0      1      
iter:   3  23:51:31  -0.48          -29.231224    0      1      
iter:   4  23:51:32  -2.06          -29.234524    0      1      
iter:   5  23:51:34  -3.07          -29.234663    0      1      
iter:   6  23:51:35  -4.12          -29.234679    0      1      
iter:   7  23:51:37  -4.84          -29.234682    0      1      
iter:   8  23:51:38  -5.17          -29.234682    0      1      
iter:   9  23:51:39  -6.01          -29.234682    0      1      
iter:  10  23:51:41  -6.40          -29.234682    0      1      
iter:  11  23:51:42  -8.05          -29.234682    0      1      
iter:  12  23:51:43  -8.77          -29.234682    0      1      
iter:  13  23:51:45  -9.64          -29.234682    0      1      
iter:  14  23:51:47 -10.37          -29.234682    0      1      

Converged after 14 iterations.

Dipole moment: (0.069440, 0.186325, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -14647.676113)

Kinetic:        +28.587449
Potential:      -30.259767
External:        +0.000000
XC:             -27.631091
Entropy (-ST):   +0.000000
Local:           +0.068727
--------------------------
Free energy:    -29.234682
Extrapolated:   -29.234682

Fermi levels: -3.50639, -3.50639

 Band  Eigenvalues  Occupancy
    0    -21.62898    2.00000
    1    -18.41867    2.00000
    2    -14.25650    2.00000
    3    -11.82100    2.00000
    4    -10.83375    2.00000
    5     -9.21189    2.00000
    6     -8.66101    2.00000
    7     -7.11250    2.00000
    8     -6.08262    2.00000
    9     -0.93017    0.00000
   10      0.56189    0.00000
   11      2.26712    0.00000
   12      2.89563    0.00000
   13      4.90443    0.00000
   14      7.71404    0.00000
   15      8.34246    0.00000
   16      8.94304    0.00000
   17      9.92277    0.00000
   18     10.38562    0.00000
   19     11.51103    0.00000
   20     11.98679    0.00000
   21     13.08000    0.00000
   22     13.31292    0.00000
   23     13.93883    0.00000
   24     15.02674    0.00000
   25     15.32344    0.00000
   26     15.66568    0.00000
   27     15.84957    0.00000
   28     17.05923    0.00000
   29     19.19714    0.00000
   30     20.17465    0.00000
   31     20.50201    0.00000
   32     21.32124    0.00000
   33     21.87819    0.00000
   34     25.36211    0.00000
   35     26.44409    0.00000
   36     28.10970    0.00000
   37     28.76665    0.00000
   38     30.67394    0.00000
   39     32.22043    0.00000
   40     33.02106    0.00000
   41     33.75337    0.00000
   42     34.67267    0.00000
   43     36.15180    0.00000
   44     38.53470    0.00000
   45     42.46760    0.00000
   46     42.76337    0.00000
   47     44.32319    0.00000
   48     45.94416    0.00000
   49     47.49796    0.00000
   50     50.00388    0.00000
   51     51.70134    0.00000
   52     63.77454    0.00000
   53     65.85956    0.00000

Gap: 5.152 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.009     0.009   0.0% |
LCAO WFS Initialize:                   1.349     0.038   0.1% |
 Hamiltonian:                          1.311     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.063     0.063   0.2% |
  Hartree integrate/restrict:          0.025     0.025   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.376     0.010   0.0% |
   Communicate bwd 0:                  0.070     0.070   0.2% |
   Communicate bwd 1:                  0.069     0.069   0.2% |
   Communicate fwd 0:                  0.067     0.067   0.2% |
   Communicate fwd 1:                  0.075     0.075   0.3% |
   fft:                                0.024     0.024   0.1% |
   fft2:                               0.061     0.061   0.2% |
  XC 3D grid:                          0.838     0.838   3.0% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            24.548     0.003   0.0% |
 Direct Minimisation step:            23.183     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.178     0.003   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.009     0.009   0.0% |
   Potential matrix:                   0.162     0.162   0.6% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.011     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.010     0.010   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.007     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.006     0.006   0.0% |
  Update Kohn-Sham energy:            22.981     0.000   0.0% |
   Density:                            0.811     0.000   0.0% |
    Atomic density matrices:           0.032     0.032   0.1% |
    Mix:                               0.608     0.608   2.1% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.165     0.009   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.156     0.156   0.5% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       22.170     0.002   0.0% |
    Atomic:                            0.005     0.005   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.012     0.012   0.0% |
    Communicate:                       1.072     1.072   3.8% |-|
    Hartree integrate/restrict:        0.403     0.403   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.423     0.178   0.6% |
     Communicate bwd 0:                1.226     1.226   4.3% |-|
     Communicate bwd 1:                1.192     1.192   4.2% |-|
     Communicate fwd 0:                1.137     1.137   4.0% |-|
     Communicate fwd 1:                1.260     1.260   4.4% |-|
     fft:                              0.396     0.396   1.4% ||
     fft2:                             1.035     1.035   3.6% ||
    XC 3D grid:                       14.189    14.189  50.0% |-------------------|
    vbar:                              0.061     0.061   0.2% |
 Get canonical representation:         1.361     0.000   0.0% |
  LCAO eigensolver:                    0.011     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.009     0.009   0.0% |
  Update Kohn-Sham energy:             1.350     0.000   0.0% |
   Density:                            0.047     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.035     0.035   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.010     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.009     0.009   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.302     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.063     0.063   0.2% |
    Hartree integrate/restrict:        0.025     0.025   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.376     0.009   0.0% |
     Communicate bwd 0:                0.070     0.070   0.2% |
     Communicate bwd 1:                0.072     0.072   0.3% |
     Communicate fwd 0:                0.067     0.067   0.2% |
     Communicate fwd 1:                0.073     0.073   0.3% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.062     0.062   0.2% |
    XC 3D grid:                        0.834     0.834   2.9% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.503     0.503   1.8% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.939     1.939   6.8% |--|
-------------------------------------------------------------
Total:                                          28.352 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:51:47 2019
