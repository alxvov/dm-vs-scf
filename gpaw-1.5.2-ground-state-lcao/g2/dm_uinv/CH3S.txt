
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:58:35 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/S.PBE.gz
  cutoffs: 0.76(comp), 1.77(filt), 1.66(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -11922.718500

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*108*104 grid
  Fine grid: 208*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 68.63 MiB
    Density: 38.83 MiB
      Arrays: 37.80 MiB
      Localized functions: 1.04 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 28.17 MiB
      Arrays: 28.09 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 1.62 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.03 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.59 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 41
Number of bands in calculation: 41
Bands to converge: occupied states only
Number of valence electrons: 13

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |            H                        |  
 |         |             C H                     |  
 |         |            S                        |  
 |         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.475361    8.798801    7.895197    ( 0.0000,  0.0000,  0.0000)
   1 S      7.475361    7.000000    7.895197    ( 0.0000,  0.0000,  1.0000)
   2 H      8.522486    9.119636    7.895197    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    9.201016    8.790394    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    9.201016    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.522486    0.000000    0.000000   104     0.1493
  2. axis:    no     0.000000   16.201016    0.000000   108     0.1500
  3. axis:    no     0.000000    0.000000   15.790394   104     0.1518

  Lengths:  15.522486  16.201016  15.790394
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1504

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  23:58:45  +0.73          -21.626074    0      1        +1.0000
iter:   2  23:58:47  +0.45          -21.671911    0      1        +1.0000
iter:   3  23:58:49  -1.52          -21.686905    0      1        +1.0000
iter:   4  23:58:51  -2.26          -21.687336    0      1        +1.0000
iter:   5  23:58:53  -3.09          -21.687439    0      1        +1.0000
iter:   6  23:58:56  -4.11          -21.687455    0      1        +1.0000
iter:   7  23:58:58  -4.28          -21.687456    0      1        +1.0000
iter:   8  23:59:00  -4.68          -21.687457    0      1        +1.0000
iter:   9  23:59:02  -5.62          -21.687457    0      1        +1.0000
iter:  10  23:59:04  -7.16          -21.687457    0      1        +1.0000
iter:  11  23:59:06  -7.93          -21.687457    0      1        +1.0000
iter:  12  23:59:08  -8.50          -21.687457    0      1        +1.0000
iter:  13  23:59:10  -9.18          -21.687457    0      1        +1.0000
iter:  14  23:59:12  -9.35          -21.687457    0      1        +1.0000
iter:  15  23:59:14  -9.75          -21.687457    0      1        +1.0000
iter:  16  23:59:18 -10.38          -21.687457    0      1        +1.0000

Converged after 16 iterations.

Dipole moment: (0.018542, 0.305142, 0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999998)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000, -0.002078)
   1 S  ( 0.000000,  0.000000,  0.470721)
   2 H  ( 0.000000,  0.000000,  0.020218)
   3 H  ( 0.000000,  0.000000,  0.004314)
   4 H  ( 0.000000,  0.000000,  0.004314)

Energy contributions relative to reference atoms: (reference = -11922.718500)

Kinetic:        +22.404315
Potential:      -23.478926
External:        +0.000000
XC:             -20.640716
Entropy (-ST):   +0.000000
Local:           +0.027870
--------------------------
Free energy:    -21.687457
Extrapolated:   -21.687457

Spin contamination: 0.024611 electrons
Fermi levels: -2.24048, -4.79523

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -19.28449    1.00000    -18.94393    1.00000
    1    -15.36492    1.00000    -14.78064    1.00000
    2    -10.70521    1.00000    -10.47918    1.00000
    3    -10.51522    1.00000    -10.46557    1.00000
    4     -8.87467    1.00000     -8.58795    1.00000
    5     -6.42725    1.00000     -4.94169    1.00000
    6     -5.29010    1.00000     -4.64877    0.00000
    7      0.80914    0.00000      1.11208    0.00000
    8      2.35115    0.00000      2.45500    0.00000
    9      3.28653    0.00000      3.40922    0.00000
   10      3.51048    0.00000      3.53657    0.00000
   11      7.93122    0.00000      8.19401    0.00000
   12      8.04659    0.00000      8.35051    0.00000
   13      8.34886    0.00000      8.43287    0.00000
   14     10.81190    0.00000     11.42111    0.00000
   15     11.19073    0.00000     11.76238    0.00000
   16     11.53383    0.00000     11.86644    0.00000
   17     12.08373    0.00000     13.05991    0.00000
   18     12.27789    0.00000     13.13297    0.00000
   19     13.27020    0.00000     14.11882    0.00000
   20     13.94515    0.00000     14.20474    0.00000
   21     13.98323    0.00000     14.34783    0.00000
   22     15.26534    0.00000     15.57049    0.00000
   23     15.48938    0.00000     15.64321    0.00000
   24     18.07836    0.00000     18.48393    0.00000
   25     22.37349    0.00000     22.44610    0.00000
   26     22.43699    0.00000     22.88155    0.00000
   27     23.70671    0.00000     23.91060    0.00000
   28     27.46499    0.00000     27.55618    0.00000
   29     28.91499    0.00000     29.32200    0.00000
   30     33.96404    0.00000     34.12454    0.00000
   31     34.11320    0.00000     34.20510    0.00000
   32     35.96231    0.00000     36.06997    0.00000
   33     36.54502    0.00000     36.70356    0.00000
   34     37.01366    0.00000     37.04372    0.00000
   35     41.16244    0.00000     41.24529    0.00000
   36     44.22185    0.00000     44.26872    0.00000
   37     44.26605    0.00000     44.35956    0.00000
   38     50.01020    0.00000     50.13879    0.00000
   39     51.23262    0.00000     51.25255    0.00000
   40     51.25462    0.00000     51.33577    0.00000

Gap: 5.458 eV
Transition (v -> c):
  (s=1, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.009     0.009   0.0% |
LCAO WFS Initialize:                   2.022     0.068   0.2% |
 Hamiltonian:                          1.955     0.003   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.108     0.108   0.2% |
  Hartree integrate/restrict:          0.047     0.047   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.367     0.010   0.0% |
   Communicate bwd 0:                  0.073     0.073   0.2% |
   Communicate bwd 1:                  0.067     0.067   0.2% |
   Communicate fwd 0:                  0.067     0.067   0.2% |
   Communicate fwd 1:                  0.072     0.072   0.2% |
   fft:                                0.022     0.022   0.1% |
   fft2:                               0.057     0.057   0.1% |
  XC 3D grid:                          1.421     1.421   3.2% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            39.183     0.005   0.0% |
 Direct Minimisation step:            37.122     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.343     0.006   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.117     0.117   0.3% |
   Potential matrix:                   0.214     0.214   0.5% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.020     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.007     0.007   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.013     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.011     0.011   0.0% |
  Update Kohn-Sham energy:            36.738     0.000   0.0% |
   Density:                            1.527     0.000   0.0% |
    Atomic density matrices:           0.142     0.142   0.3% |
    Mix:                               1.143     1.143   2.6% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.007     0.007   0.0% |
    Pseudo density:                    0.232     0.021   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.208     0.208   0.5% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       35.211     0.038   0.1% |
    Atomic:                            0.005     0.005   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.012     0.012   0.0% |
    Communicate:                       1.941     1.941   4.4% |-|
    Hartree integrate/restrict:        0.835     0.835   1.9% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           6.716     0.170   0.4% |
     Communicate bwd 0:                1.344     1.344   3.1% ||
     Communicate bwd 1:                1.270     1.270   2.9% ||
     Communicate fwd 0:                1.214     1.214   2.8% ||
     Communicate fwd 1:                1.302     1.302   3.0% ||
     fft:                              0.386     0.386   0.9% |
     fft2:                             1.029     1.029   2.4% ||
    XC 3D grid:                       25.595    25.595  58.5% |----------------------|
    vbar:                              0.066     0.066   0.2% |
 Get canonical representation:         2.056     0.000   0.0% |
  LCAO eigensolver:                    0.019     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.006     0.006   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Update Kohn-Sham energy:             2.037     0.000   0.0% |
   Density:                            0.085     0.000   0.0% |
    Atomic density matrices:           0.008     0.008   0.0% |
    Mix:                               0.063     0.063   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.013     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.012     0.012   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.952     0.002   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.108     0.108   0.2% |
    Hartree integrate/restrict:        0.046     0.046   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.372     0.010   0.0% |
     Communicate bwd 0:                0.074     0.074   0.2% |
     Communicate bwd 1:                0.070     0.070   0.2% |
     Communicate fwd 0:                0.068     0.068   0.2% |
     Communicate fwd 1:                0.073     0.073   0.2% |
     fft:                              0.022     0.022   0.0% |
     fft2:                             0.057     0.057   0.1% |
    XC 3D grid:                        1.418     1.418   3.2% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.503     0.503   1.2% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.039     2.039   4.7% |-|
-------------------------------------------------------------
Total:                                          43.760 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:59:18 2019
