
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:28:01 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6186.921482

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*112*104 grid
  Fine grid: 240*224*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*224*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 59.24 MiB
    Density: 34.77 MiB
      Arrays: 33.67 MiB
      Localized functions: 1.10 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 22.10 MiB
      Arrays: 22.02 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.37 MiB
      C [qnM]: 0.04 MiB
      S, T [2 x qmm]: 0.08 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.25 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 8
Number of atomic orbitals: 72
Number of bands in calculation: 72
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------------.  
          /|                                           |  
         / |                                           |  
        /  |                                           |  
       /   |                                           |  
      /    |                                           |  
     /     |                                           |  
    /      |                                           |  
   /       |                                           |  
  /        |                                           |  
 *         |                                           |  
 |         |                                           |  
 |         |                                           |  
 |         |                                           |  
 |         |           H    O   H H                    |  
 |         |             OC                            |  
 |         |                    H                      |  
 |         |                                           |  
 |         |                                           |  
 |         .-------------------------------------------.  
 |        /                                           /   
 |       /                                           /    
 |      /                                           /     
 |     /                                           /      
 |    /                                           /       
 |   /                                           /        
 |  /                                           /         
 | /                                           /          
 |/                                           /           
 *-------------------------------------------*            

Positions:
   0 C      7.997151    8.194343    7.889397    ( 0.0000,  0.0000,  0.0000)
   1 O      8.217341    7.000000    7.889397    ( 0.0000,  0.0000,  0.0000)
   2 O      8.928360    9.165050    7.889397    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.652807    7.889397    ( 0.0000,  0.0000,  0.0000)
   4 C     10.285259    8.675496    7.889397    ( 0.0000,  0.0000,  0.0000)
   5 H     10.908494    9.566373    7.889397    ( 0.0000,  0.0000,  0.0000)
   6 H     10.469481    8.072037    8.778794    ( 0.0000,  0.0000,  0.0000)
   7 H     10.469481    8.072037    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.908494    0.000000    0.000000   120     0.1492
  2. axis:    no     0.000000   16.566373    0.000000   112     0.1479
  3. axis:    no     0.000000    0.000000   15.778794   104     0.1517

  Lengths:  17.908494  16.566373  15.778794
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1496

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:28:09  +0.72          -44.112491    0      1      
iter:   2  00:28:12  -0.08          -44.236398    0      1      
iter:   3  00:28:14  -0.56          -44.251836    0      1      
iter:   4  00:28:15  -1.73          -44.255719    0      1      
iter:   5  00:28:17  -2.09          -44.256041    0      1      
iter:   6  00:28:18  -3.50          -44.256165    0      1      
iter:   7  00:28:20  -3.77          -44.256174    0      1      
iter:   8  00:28:21  -4.97          -44.256177    0      1      
iter:   9  00:28:23  -5.78          -44.256178    0      1      
iter:  10  00:28:24  -6.44          -44.256178    0      1      
iter:  11  00:28:26  -6.81          -44.256178    0      1      
iter:  12  00:28:27  -7.96          -44.256178    0      1      
iter:  13  00:28:29  -8.63          -44.256178    0      1      
iter:  14  00:28:30  -9.45          -44.256178    0      1      
iter:  15  00:28:32  -9.86          -44.256178    0      1      
iter:  16  00:28:35 -10.80          -44.256178    0      1      

Converged after 16 iterations.

Dipole moment: (0.198378, 0.316294, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6186.921482)

Kinetic:        +38.234039
Potential:      -41.028181
External:        +0.000000
XC:             -41.626345
Entropy (-ST):   +0.000000
Local:           +0.164309
--------------------------
Free energy:    -44.256178
Extrapolated:   -44.256178

Fermi levels: -3.60608, -3.60608

 Band  Eigenvalues  Occupancy
    0    -27.67172    2.00000
    1    -25.35149    2.00000
    2    -18.35931    2.00000
    3    -14.68395    2.00000
    4    -12.90359    2.00000
    5    -11.76586    2.00000
    6    -11.32366    2.00000
    7    -10.18227    2.00000
    8     -9.55827    2.00000
    9     -8.60056    2.00000
   10     -7.23047    2.00000
   11     -6.28315    2.00000
   12     -0.92901    0.00000
   13      2.14996    0.00000
   14      2.18157    0.00000
   15      2.88846    0.00000
   16      3.56763    0.00000
   17      3.73604    0.00000
   18      6.42586    0.00000
   19      7.09386    0.00000
   20      9.07843    0.00000
   21      9.44292    0.00000
   22      9.90364    0.00000
   23     10.73100    0.00000
   24     11.27879    0.00000
   25     13.41794    0.00000
   26     14.15439    0.00000
   27     14.88149    0.00000
   28     15.08325    0.00000
   29     15.66333    0.00000
   30     16.70852    0.00000
   31     17.45405    0.00000
   32     18.58677    0.00000
   33     18.80926    0.00000
   34     20.09663    0.00000
   35     21.58735    0.00000
   36     22.18555    0.00000
   37     23.04903    0.00000
   38     23.62721    0.00000
   39     25.18397    0.00000
   40     27.30303    0.00000
   41     27.93298    0.00000
   42     27.94385    0.00000
   43     30.22196    0.00000
   44     32.16159    0.00000
   45     32.27059    0.00000
   46     35.03682    0.00000
   47     35.30671    0.00000
   48     36.43396    0.00000
   49     36.51740    0.00000
   50     37.99343    0.00000
   51     38.69395    0.00000
   52     39.72987    0.00000
   53     42.34009    0.00000
   54     42.83774    0.00000
   55     43.96765    0.00000
   56     46.50432    0.00000
   57     47.74558    0.00000
   58     48.23604    0.00000
   59     50.18139    0.00000
   60     50.71914    0.00000
   61     51.23999    0.00000
   62     52.11220    0.00000
   63     55.99987    0.00000
   64     57.19488    0.00000
   65     58.19632    0.00000
   66     59.88923    0.00000
   67     64.80564    0.00000
   68     67.44493    0.00000
   69     68.41683    0.00000
   70     68.93099    0.00000
   71     74.98008    0.00000

Gap: 5.354 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.012     0.012   0.0% |
LCAO WFS Initialize:                   1.453     0.044   0.1% |
 Hamiltonian:                          1.409     0.000   0.0% |
  Atomic:                              0.046     0.000   0.0% |
   XC Correction:                      0.046     0.046   0.1% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.002     0.002   0.0% |
  Hartree integrate/restrict:          0.027     0.027   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.311     0.017   0.0% |
   Communicate bwd 0:                  0.018     0.018   0.1% |
   Communicate bwd 1:                  0.084     0.084   0.2% |
   Communicate fwd 0:                  0.082     0.082   0.2% |
   Communicate fwd 1:                  0.026     0.026   0.1% |
   fft:                                0.028     0.028   0.1% |
   fft2:                               0.058     0.058   0.2% |
  XC 3D grid:                          1.010     1.010   3.0% ||
  vbar:                                0.006     0.006   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            29.737     0.004   0.0% |
 Direct Minimisation step:            28.251     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.341     0.005   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.132     0.132   0.4% |
   Potential matrix:                   0.199     0.199   0.6% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.019     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.007     0.007   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.010     0.010   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.010     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.008     0.008   0.0% |
  Update Kohn-Sham energy:            27.874     0.000   0.0% |
   Density:                            1.148     0.000   0.0% |
    Atomic density matrices:           0.159     0.159   0.5% |
    Mix:                               0.779     0.779   2.3% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.202     0.013   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.187     0.187   0.6% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       26.727     0.002   0.0% |
    Atomic:                            0.890     0.010   0.0% |
     XC Correction:                    0.880     0.880   2.6% ||
    Calculate atomic Hamiltonians:     0.050     0.050   0.1% |
    Communicate:                       0.027     0.027   0.1% |
    Hartree integrate/restrict:        0.474     0.474   1.4% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.017     0.323   1.0% |
     Communicate bwd 0:                0.435     0.435   1.3% ||
     Communicate bwd 1:                1.636     1.636   4.8% |-|
     Communicate fwd 0:                1.555     1.555   4.6% |-|
     Communicate fwd 1:                0.422     0.422   1.2% |
     fft:                              0.543     0.543   1.6% ||
     fft2:                             1.102     1.102   3.2% ||
    XC 3D grid:                       19.181    19.181  56.5% |----------------------|
    vbar:                              0.082     0.082   0.2% |
 Get canonical representation:         1.483     0.001   0.0% |
  LCAO eigensolver:                    0.019     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.007     0.007   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.010     0.010   0.0% |
  Update Kohn-Sham energy:             1.463     0.000   0.0% |
   Density:                            0.059     0.000   0.0% |
    Atomic density matrices:           0.008     0.008   0.0% |
    Mix:                               0.040     0.040   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.011     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.010     0.010   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.404     0.000   0.0% |
    Atomic:                            0.047     0.000   0.0% |
     XC Correction:                    0.046     0.046   0.1% |
    Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
    Communicate:                       0.002     0.002   0.0% |
    Hartree integrate/restrict:        0.025     0.025   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.318     0.017   0.0% |
     Communicate bwd 0:                0.024     0.024   0.1% |
     Communicate bwd 1:                0.086     0.086   0.3% |
     Communicate fwd 0:                0.082     0.082   0.2% |
     Communicate fwd 1:                0.022     0.022   0.1% |
     fft:                              0.029     0.029   0.1% |
     fft2:                             0.058     0.058   0.2% |
    XC 3D grid:                        1.006     1.006   3.0% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.504     0.504   1.5% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.259     2.259   6.7% |--|
-------------------------------------------------------------
Total:                                          33.971 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:28:35 2019
