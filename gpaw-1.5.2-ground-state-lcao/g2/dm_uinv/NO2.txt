
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:37:34 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -5563.426107

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*108*96 grid
  Fine grid: 184*216*192 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 55.17 MiB
    Density: 31.38 MiB
      Arrays: 30.79 MiB
      Localized functions: 0.58 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 22.92 MiB
      Arrays: 22.88 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.87 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.02 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.83 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 39
Number of bands in calculation: 39
Bands to converge: occupied states only
Number of valence electrons: 17

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           NO                    |  
 |         |          O                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 N      7.000000    8.118122    7.477643    ( 0.0000,  0.0000,  1.0000)
   1 O      7.000000    9.236244    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 O      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.236244    0.000000   108     0.1503
  3. axis:    no     0.000000    0.000000   14.477643    96     0.1508

  Lengths:  14.000000  16.236244  14.477643
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1511

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  00:37:43  +0.82          -17.091240    0      1        +1.0000
iter:   2  00:37:45  +0.56          -17.136238    0      1        +1.0000
iter:   3  00:37:47  -1.08          -17.160912    0      1        +1.0000
iter:   4  00:37:48  -1.78          -17.162400    0      1        +1.0000
iter:   5  00:37:50  -2.55          -17.162981    0      1        +1.0000
iter:   6  00:37:51  -3.13          -17.163038    0      1        +1.0000
iter:   7  00:37:55  -3.93          -17.163044    0      1        +1.0000
iter:   8  00:37:56  -4.58          -17.163045    0      1        +1.0000
iter:   9  00:37:58  -6.23          -17.163045    0      1        +1.0000
iter:  10  00:38:00  -6.91          -17.163045    0      1        +1.0000
iter:  11  00:38:01  -8.26          -17.163045    0      1        +1.0000
iter:  12  00:38:03  -8.46          -17.163045    0      1        +1.0000
iter:  13  00:38:11  -8.50          -17.163045    0      1        +1.0000
iter:  14  00:38:13  -9.99          -17.163045    0      1        +1.0000
iter:  15  00:38:16 -10.80          -17.163045    0      1        +1.0000

Converged after 15 iterations.

Dipole moment: (-0.000000, 0.000000, 0.044321) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999998)
Local magnetic moments:
   0 N  ( 0.000000,  0.000000,  0.197352)
   1 O  ( 0.000000,  0.000000,  0.219136)
   2 O  ( 0.000000,  0.000000,  0.219136)

Energy contributions relative to reference atoms: (reference = -5563.426107)

Kinetic:        +15.500669
Potential:      -15.152493
External:        +0.000000
XC:             -17.724120
Entropy (-ST):   +0.000000
Local:           +0.212899
--------------------------
Free energy:    -17.163045
Extrapolated:   -17.163045

Spin contamination: 0.014441 electrons
Fermi levels: -4.63212, -6.20054

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -31.70457    1.00000    -31.31726    1.00000
    1    -27.97621    1.00000    -27.53149    1.00000
    2    -16.26532    1.00000    -15.63371    1.00000
    3    -13.55946    1.00000    -13.22401    1.00000
    4    -13.49021    1.00000    -13.09403    1.00000
    5    -13.39570    1.00000    -12.53740    1.00000
    6     -8.86086    1.00000     -8.65959    1.00000
    7     -8.50256    1.00000     -7.85595    1.00000
    8     -6.01922    1.00000     -4.54513    0.00000
    9     -3.24503    0.00000     -2.85039    0.00000
   10      4.41322    0.00000      4.75964    0.00000
   11      8.86214    0.00000      9.41007    0.00000
   12     11.96648    0.00000     12.16856    0.00000
   13     11.99914    0.00000     12.54740    0.00000
   14     13.16860    0.00000     13.32260    0.00000
   15     15.62017    0.00000     15.74472    0.00000
   16     16.65206    0.00000     17.05513    0.00000
   17     17.32236    0.00000     17.41507    0.00000
   18     17.40392    0.00000     18.02902    0.00000
   19     18.91733    0.00000     19.08319    0.00000
   20     18.99355    0.00000     19.27919    0.00000
   21     19.84924    0.00000     20.10811    0.00000
   22     31.02847    0.00000     31.66531    0.00000
   23     31.17760    0.00000     31.78308    0.00000
   24     32.32923    0.00000     32.46117    0.00000
   25     40.18734    0.00000     40.68300    0.00000
   26     40.21591    0.00000     40.70303    0.00000
   27     44.69271    0.00000     44.98996    0.00000
   28     44.72414    0.00000     45.03188    0.00000
   29     46.15409    0.00000     46.40629    0.00000
   30     46.52773    0.00000     46.94646    0.00000
   31     46.95103    0.00000     47.50470    0.00000
   32     53.23814    0.00000     53.70876    0.00000
   33     57.61796    0.00000     57.81007    0.00000
   34     63.76844    0.00000     64.50213    0.00000
   35     64.15222    0.00000     64.51118    0.00000
   36     65.92088    0.00000     66.26256    0.00000
   37     66.57555    0.00000     66.90862    0.00000
   38     72.89060    0.00000     73.63562    0.00000

Gap: 1.474 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=8, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.004     0.004   0.0% |
LCAO WFS Initialize:                   1.633     0.054   0.1% |
 Hamiltonian:                          1.579     0.002   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.080     0.080   0.2% |
  Hartree integrate/restrict:          0.037     0.037   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.303     0.009   0.0% |
   Communicate bwd 0:                  0.059     0.059   0.1% |
   Communicate bwd 1:                  0.055     0.055   0.1% |
   Communicate fwd 0:                  0.057     0.057   0.1% |
   Communicate fwd 1:                  0.058     0.058   0.1% |
   fft:                                0.018     0.018   0.0% |
   fft2:                               0.048     0.048   0.1% |
  XC 3D grid:                          1.152     1.152   2.8% ||
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            38.096     0.004   0.0% |
 Direct Minimisation step:            36.441     0.003   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.164     0.007   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.027     0.027   0.1% |
   Potential matrix:                   0.123     0.123   0.3% |
   Residual:                           0.003     0.003   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.008     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.006     0.006   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.016     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.014     0.014   0.0% |
  Update Kohn-Sham energy:            36.244     0.000   0.0% |
   Density:                            1.341     0.000   0.0% |
    Atomic density matrices:           0.043     0.043   0.1% |
    Mix:                               1.150     1.150   2.7% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.006     0.006   0.0% |
    Pseudo density:                    0.139     0.018   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.118     0.118   0.3% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       34.903     0.037   0.1% |
    Atomic:                            0.006     0.006   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.009     0.009   0.0% |
    Communicate:                       1.773     1.773   4.2% |-|
    Hartree integrate/restrict:        0.803     0.803   1.9% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           6.708     0.176   0.4% |
     Communicate bwd 0:                1.312     1.312   3.1% ||
     Communicate bwd 1:                1.268     1.268   3.0% ||
     Communicate fwd 0:                1.225     1.225   2.9% ||
     Communicate fwd 1:                1.298     1.298   3.1% ||
     fft:                              0.381     0.381   0.9% |
     fft2:                             1.047     1.047   2.5% ||
    XC 3D grid:                       25.497    25.497  60.9% |-----------------------|
    vbar:                              0.067     0.067   0.2% |
 Get canonical representation:         1.651     0.000   0.0% |
  LCAO eigensolver:                    0.008     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.006     0.006   0.0% |
  Update Kohn-Sham energy:             1.643     0.000   0.0% |
   Density:                            0.062     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.053     0.053   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.006     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.005     0.005   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.581     0.002   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.080     0.080   0.2% |
    Hartree integrate/restrict:        0.036     0.036   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.304     0.008   0.0% |
     Communicate bwd 0:                0.061     0.061   0.1% |
     Communicate bwd 1:                0.058     0.058   0.1% |
     Communicate fwd 0:                0.055     0.055   0.1% |
     Communicate fwd 1:                0.057     0.057   0.1% |
     fft:                              0.017     0.017   0.0% |
     fft2:                             0.048     0.048   0.1% |
    XC 3D grid:                        1.156     1.156   2.8% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.360     0.360   0.9% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.773     1.773   4.2% |-|
-------------------------------------------------------------
Total:                                          41.870 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:38:16 2019
