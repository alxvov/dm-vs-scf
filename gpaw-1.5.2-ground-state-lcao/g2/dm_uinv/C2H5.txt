
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:15:08 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2117.707042

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*108*104 grid
  Fine grid: 208*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 68.58 MiB
    Density: 38.50 MiB
      Arrays: 37.80 MiB
      Localized functions: 0.70 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 28.14 MiB
      Arrays: 28.09 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.94 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.88 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 51
Number of bands in calculation: 51
Bands to converge: occupied states only
Number of valence electrons: 13

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------.  
          /|                                     |  
         / |                                     |  
        /  |                                     |  
       /   |                                     |  
      /    |                                     |  
     /     |                                     |  
    /      |                                     |  
   /       |                                     |  
  /        |                                     |  
 *         |                                     |  
 |         |                                     |  
 |         |                                     |  
 |         |             H                       |  
 |         |          H  C                       |  
 |         |            CHH                      |  
 |         |          H                          |  
 |         |                                     |  
 |         |                                     |  
 |         .-------------------------------------.  
 |        /                                     /   
 |       /                                     /    
 |      /                                     /     
 |     /                                     /      
 |    /                                     /       
 |   /                                     /        
 |  /                                     /         
 | /                                     /          
 |/                                     /           
 *-------------------------------------*            

Positions:
   0 C      7.502678    7.409425    7.923705    ( 0.0000,  0.0000,  0.0000)
   1 C      7.502678    8.898515    7.923705    ( 0.0000,  0.0000,  1.0000)
   2 H      8.523138    7.000000    7.923705    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.010429    8.808544    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    7.010429    7.038866    ( 0.0000,  0.0000,  0.0000)
   5 H      7.617174    9.450107    8.847410    ( 0.0000,  0.0000,  0.0000)
   6 H      7.617174    9.450107    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.523138    0.000000    0.000000   104     0.1493
  2. axis:    no     0.000000   16.450107    0.000000   108     0.1523
  3. axis:    no     0.000000    0.000000   15.847410   104     0.1524

  Lengths:  15.523138  16.450107  15.847410
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1513

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  00:15:19  +0.76          -33.769234    0      1        +1.0000
iter:   2  00:15:21  +0.43          -33.827580    0      1        +1.0000
iter:   3  00:15:23  -1.03          -33.844548    0      1        +1.0000
iter:   4  00:15:25  -1.39          -33.845042    0      1        +1.0000
iter:   5  00:15:27  -3.23          -33.845275    0      1        +1.0000
iter:   6  00:15:29  -3.90          -33.845283    0      1        +1.0000
iter:   7  00:15:31  -5.15          -33.845285    0      1        +1.0000
iter:   8  00:15:33  -6.02          -33.845286    0      1        +1.0000
iter:   9  00:15:35  -5.84          -33.845286    0      1        +1.0000
iter:  10  00:15:37  -7.14          -33.845286    0      1        +1.0000
iter:  11  00:15:39  -7.94          -33.845286    0      1        +1.0000
iter:  12  00:15:41  -9.17          -33.845286    0      1        +1.0000
iter:  13  00:15:43  -9.86          -33.845286    0      1        +1.0000
iter:  14  00:15:47 -10.98          -33.845286    0      1        +1.0000

Converged after 14 iterations.

Dipole moment: (0.028384, -0.062677, 0.000000) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999999)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000, -0.012990)
   1 C  ( 0.000000,  0.000000,  0.321199)
   2 H  ( 0.000000,  0.000000,  0.026972)
   3 H  ( 0.000000,  0.000000,  0.006314)
   4 H  ( 0.000000,  0.000000,  0.006314)
   5 H  ( 0.000000,  0.000000, -0.006835)
   6 H  ( 0.000000,  0.000000, -0.006835)

Energy contributions relative to reference atoms: (reference = -2117.707042)

Kinetic:        +27.875808
Potential:      -31.007496
External:        +0.000000
XC:             -30.767781
Entropy (-ST):   +0.000000
Local:           +0.054184
--------------------------
Free energy:    -33.845286
Extrapolated:   -33.845286

Spin contamination: 0.051221 electrons
Fermi levels: -1.10204, -5.23344

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -18.66093    1.00000    -18.27636    1.00000
    1    -14.87249    1.00000    -14.30766    1.00000
    2    -10.91143    1.00000    -10.73297    1.00000
    3    -10.00781    1.00000     -9.71055    1.00000
    4     -9.41391    1.00000     -9.18379    1.00000
    5     -8.50141    1.00000     -8.36942    1.00000
    6     -4.42096    1.00000     -2.09747    0.00000
    7      2.21689    0.00000      2.34608    0.00000
    8      2.94902    0.00000      3.27628    0.00000
    9      3.20572    0.00000      3.27890    0.00000
   10      3.90391    0.00000      4.04808    0.00000
   11      5.08717    0.00000      5.20595    0.00000
   12      6.27267    0.00000      6.50030    0.00000
   13      8.57923    0.00000      8.64549    0.00000
   14      8.84114    0.00000      8.95550    0.00000
   15      9.69332    0.00000      9.97800    0.00000
   16     10.66593    0.00000     11.42223    0.00000
   17     10.73879    0.00000     12.11262    0.00000
   18     12.34764    0.00000     12.48921    0.00000
   19     14.87178    0.00000     15.11893    0.00000
   20     15.41167    0.00000     15.53659    0.00000
   21     15.49482    0.00000     15.54287    0.00000
   22     16.47010    0.00000     16.65682    0.00000
   23     16.58444    0.00000     16.73100    0.00000
   24     18.07828    0.00000     18.41441    0.00000
   25     19.51868    0.00000     20.19867    0.00000
   26     22.03619    0.00000     22.75383    0.00000
   27     24.30907    0.00000     24.33187    0.00000
   28     27.09946    0.00000     27.56784    0.00000
   29     28.05693    0.00000     28.19117    0.00000
   30     28.31484    0.00000     28.78649    0.00000
   31     31.24909    0.00000     31.36023    0.00000
   32     31.74524    0.00000     32.14963    0.00000
   33     33.69490    0.00000     34.11635    0.00000
   34     33.93210    0.00000     34.25437    0.00000
   35     35.11832    0.00000     35.32974    0.00000
   36     35.73574    0.00000     36.17090    0.00000
   37     36.18591    0.00000     36.41164    0.00000
   38     37.18346    0.00000     37.89716    0.00000
   39     37.85232    0.00000     38.26167    0.00000
   40     41.31392    0.00000     41.99106    0.00000
   41     43.15043    0.00000     43.67535    0.00000
   42     44.29929    0.00000     44.47563    0.00000
   43     46.70362    0.00000     47.01148    0.00000
   44     47.60630    0.00000     47.76645    0.00000
   45     48.65920    0.00000     48.93538    0.00000
   46     50.32380    0.00000     50.47253    0.00000
   47     52.39074    0.00000     52.70755    0.00000
   48     53.21918    0.00000     53.37039    0.00000
   49     58.08539    0.00000     58.44682    0.00000
   50     60.06164    0.00000     60.29333    0.00000

Gap: 2.323 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=6, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.010     0.010   0.0% |
LCAO WFS Initialize:                   1.992     0.068   0.2% |
 Hamiltonian:                          1.924     0.003   0.0% |
  Atomic:                              0.044     0.044   0.1% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.035     0.035   0.1% |
  Hartree integrate/restrict:          0.046     0.046   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.373     0.010   0.0% |
   Communicate bwd 0:                  0.072     0.072   0.2% |
   Communicate bwd 1:                  0.067     0.067   0.2% |
   Communicate fwd 0:                  0.068     0.068   0.2% |
   Communicate fwd 1:                  0.077     0.077   0.2% |
   fft:                                0.022     0.022   0.1% |
   fft2:                               0.057     0.057   0.1% |
  XC 3D grid:                          1.412     1.412   3.6% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            34.868     0.004   0.0% |
 Direct Minimisation step:            32.813     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.385     0.005   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.003     0.003   0.0% |
   Distribute overlap matrix:          0.006     0.006   0.0% |
   Potential matrix:                   0.366     0.366   0.9% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.025     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.023     0.023   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.012     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.010     0.010   0.0% |
  Update Kohn-Sham energy:            32.383     0.000   0.0% |
   Density:                            1.443     0.000   0.0% |
    Atomic density matrices:           0.043     0.043   0.1% |
    Mix:                               1.023     1.023   2.6% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.006     0.006   0.0% |
    Pseudo density:                    0.369     0.020   0.1% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.347     0.347   0.9% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       30.939     0.033   0.1% |
    Atomic:                            0.717     0.717   1.8% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.028     0.028   0.1% |
    Communicate:                       0.569     0.569   1.5% ||
    Hartree integrate/restrict:        0.735     0.735   1.9% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.018     0.154   0.4% |
     Communicate bwd 0:                1.192     1.192   3.1% ||
     Communicate bwd 1:                1.120     1.120   2.9% ||
     Communicate fwd 0:                1.095     1.095   2.8% ||
     Communicate fwd 1:                1.199     1.199   3.1% ||
     fft:                              0.345     0.345   0.9% |
     fft2:                             0.914     0.914   2.3% ||
    XC 3D grid:                       22.779    22.779  58.4% |----------------------|
    vbar:                              0.059     0.059   0.2% |
 Get canonical representation:         2.051     0.001   0.0% |
  LCAO eigensolver:                    0.025     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.000     0.000   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.023     0.023   0.1% |
  Update Kohn-Sham energy:             2.026     0.000   0.0% |
   Density:                            0.091     0.000   0.0% |
    Atomic density matrices:           0.003     0.003   0.0% |
    Mix:                               0.064     0.064   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.023     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.022     0.022   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.935     0.002   0.0% |
    Atomic:                            0.045     0.045   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.036     0.036   0.1% |
    Hartree integrate/restrict:        0.047     0.047   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.374     0.010   0.0% |
     Communicate bwd 0:                0.074     0.074   0.2% |
     Communicate bwd 1:                0.069     0.069   0.2% |
     Communicate fwd 0:                0.068     0.068   0.2% |
     Communicate fwd 1:                0.075     0.075   0.2% |
     fft:                              0.022     0.022   0.1% |
     fft2:                             0.057     0.057   0.1% |
    XC 3D grid:                        1.426     1.426   3.7% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.199     0.199   0.5% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.953     1.953   5.0% |-|
-------------------------------------------------------------
Total:                                          39.027 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:15:47 2019
