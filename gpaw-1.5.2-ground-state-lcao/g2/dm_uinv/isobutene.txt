
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:37:37 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4210.433760

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*124*116 grid
  Fine grid: 208*248*232 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*248*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 64.74 MiB
    Density: 37.36 MiB
      Arrays: 36.05 MiB
      Localized functions: 1.31 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 23.69 MiB
      Arrays: 23.58 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.10 MiB
    Wavefunctions: 3.69 MiB
      C [qnM]: 0.06 MiB
      S, T [2 x qmm]: 0.13 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.50 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 12
Number of atomic orbitals: 92
Number of bands in calculation: 92
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .--------------------------------------.  
           /|                                      |  
          / |                                      |  
         /  |                                      |  
        /   |                                      |  
       /    |                                      |  
      /     |                                      |  
     /      |                                      |  
    /       |                                      |  
   /        |                                      |  
  /         |                                      |  
 *          |                                      |  
 |          |                                      |  
 |          |              H                       |  
 |          |                                      |  
 |          |            HCH                       |  
 |          |             CC                       |  
 |          |            H   H                     |  
 |          |            H                         |  
 |          |          H   H                       |  
 |          |                                      |  
 |          .--------------------------------------.  
 |         /                                      /   
 |        /                                      /    
 |       /                                      /     
 |      /                                      /      
 |     /                                      /       
 |    /                                      /        
 |   /                                      /         
 |  /                                      /          
 | /                                      /           
 |/                                      /            
 *--------------------------------------*             

Positions:
   0 C      7.880211    9.153042    9.788399    ( 0.0000,  0.0000,  0.0000)
   1 C      7.880211    9.153042    8.449180    ( 0.0000,  0.0000,  0.0000)
   2 H      7.880211   10.077344   10.358001    ( 0.0000,  0.0000,  0.0000)
   3 H      7.880211    8.228740   10.358001    ( 0.0000,  0.0000,  0.0000)
   4 C      7.880211   10.425725    7.650789    ( 0.0000,  0.0000,  0.0000)
   5 H      7.880211   11.306084    8.298004    ( 0.0000,  0.0000,  0.0000)
   6 H      8.760422   10.476584    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000   10.476584    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 C      7.880211    7.880359    7.650789    ( 0.0000,  0.0000,  0.0000)
   9 H      7.880211    7.000000    8.298004    ( 0.0000,  0.0000,  0.0000)
  10 H      7.000000    7.829500    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      8.760422    7.829500    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.760422    0.000000    0.000000   104     0.1515
  2. axis:    no     0.000000   18.306084    0.000000   124     0.1476
  3. axis:    no     0.000000    0.000000   17.358001   116     0.1496

  Lengths:  15.760422  18.306084  17.358001
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1496

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:37:47  +0.37          -63.539024    0      1      
iter:   2  23:37:51  -0.02          -63.639393    0      1      
iter:   3  23:37:53  -0.48          -63.657255    0      1      
iter:   4  23:37:55  -2.27          -63.662785    0      1      
iter:   5  23:37:56  -2.69          -63.662935    0      1      
iter:   6  23:37:58  -3.93          -63.662987    0      1      
iter:   7  23:38:00  -4.93          -63.662992    0      1      
iter:   8  23:38:02  -5.76          -63.662993    0      1      
iter:   9  23:38:04  -6.00          -63.662993    0      1      
iter:  10  23:38:05  -6.73          -63.662993    0      1      
iter:  11  23:38:07  -7.51          -63.662993    0      1      
iter:  12  23:38:09  -8.74          -63.662993    0      1      
iter:  13  23:38:11  -9.80          -63.662993    0      1      
iter:  14  23:38:15 -10.82          -63.662993    0      1      

Converged after 14 iterations.

Dipole moment: (-0.000000, 0.000000, -0.117723) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4210.433760)

Kinetic:        +50.793655
Potential:      -57.767245
External:        +0.000000
XC:             -56.798002
Entropy (-ST):   +0.000000
Local:           +0.108600
--------------------------
Free energy:    -63.662993
Extrapolated:   -63.662993

Fermi levels: -2.99809, -2.99809

 Band  Eigenvalues  Occupancy
    0    -19.92175    2.00000
    1    -17.06998    2.00000
    2    -16.66241    2.00000
    3    -12.74543    2.00000
    4    -10.95451    2.00000
    5    -10.72807    2.00000
    6    -10.48321    2.00000
    7     -9.28752    2.00000
    8     -8.82390    2.00000
    9     -8.66607    2.00000
   10     -8.06614    2.00000
   11     -5.77741    2.00000
   12     -0.21877    0.00000
   13      1.77594    0.00000
   14      2.97183    0.00000
   15      3.09373    0.00000
   16      3.22180    0.00000
   17      3.71170    0.00000
   18      3.85376    0.00000
   19      4.54400    0.00000
   20      5.11781    0.00000
   21      5.13570    0.00000
   22      7.60864    0.00000
   23      8.25943    0.00000
   24      8.85500    0.00000
   25      9.46930    0.00000
   26      9.48130    0.00000
   27      9.72620    0.00000
   28     10.60673    0.00000
   29     11.59897    0.00000
   30     11.89507    0.00000
   31     12.52836    0.00000
   32     13.31062    0.00000
   33     13.44802    0.00000
   34     15.02757    0.00000
   35     15.12798    0.00000
   36     15.19442    0.00000
   37     15.84599    0.00000
   38     15.97636    0.00000
   39     16.11834    0.00000
   40     16.67271    0.00000
   41     17.91733    0.00000
   42     18.68119    0.00000
   43     19.51762    0.00000
   44     19.99022    0.00000
   45     20.80600    0.00000
   46     20.83290    0.00000
   47     24.36064    0.00000
   48     25.42311    0.00000
   49     25.59100    0.00000
   50     27.39045    0.00000
   51     27.54468    0.00000
   52     28.28114    0.00000
   53     29.16057    0.00000
   54     30.16179    0.00000
   55     30.53020    0.00000
   56     31.65987    0.00000
   57     33.42786    0.00000
   58     33.45332    0.00000
   59     34.14456    0.00000
   60     35.43628    0.00000
   61     35.84328    0.00000
   62     35.87369    0.00000
   63     36.03315    0.00000
   64     36.07603    0.00000
   65     36.72002    0.00000
   66     37.77512    0.00000
   67     38.58200    0.00000
   68     38.82210    0.00000
   69     39.40667    0.00000
   70     39.76657    0.00000
   71     40.79408    0.00000
   72     42.01597    0.00000
   73     42.47525    0.00000
   74     42.64849    0.00000
   75     43.34030    0.00000
   76     44.96223    0.00000
   77     45.38518    0.00000
   78     47.43743    0.00000
   79     49.08929    0.00000
   80     50.10976    0.00000
   81     51.04125    0.00000
   82     51.52967    0.00000
   83     51.74405    0.00000
   84     52.81408    0.00000
   85     53.37226    0.00000
   86     54.22926    0.00000
   87     55.02693    0.00000
   88     57.17358    0.00000
   89     60.14219    0.00000
   90     68.19483    0.00000
   91     68.37293    0.00000

Gap: 5.559 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.001     0.001   0.0% |
Basis functions set positions:         0.018     0.018   0.0% |
LCAO WFS Initialize:                   1.758     0.050   0.1% |
 Hamiltonian:                          1.708     0.000   0.0% |
  Atomic:                              0.053     0.007   0.0% |
   XC Correction:                      0.046     0.046   0.1% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.020     0.020   0.1% |
  Hartree integrate/restrict:          0.035     0.035   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.507     0.013   0.0% |
   Communicate bwd 0:                  0.093     0.093   0.2% |
   Communicate bwd 1:                  0.091     0.091   0.2% |
   Communicate fwd 0:                  0.084     0.084   0.2% |
   Communicate fwd 1:                  0.092     0.092   0.2% |
   fft:                                0.046     0.046   0.1% |
   fft2:                               0.087     0.087   0.2% |
  XC 3D grid:                          1.081     1.081   2.9% ||
  vbar:                                0.006     0.006   0.0% |
P tci:                                 0.003     0.003   0.0% |
SCF-cycle:                            32.835     0.003   0.0% |
 Direct Minimisation step:            31.017     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.597     0.005   0.0% |
   Construct Gradient Matrix:          0.004     0.004   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.091     0.091   0.2% |
   Potential matrix:                   0.494     0.494   1.3% ||
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.036     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.005     0.005   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.029     0.029   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.010     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.007     0.007   0.0% |
  Update Kohn-Sham energy:            30.367     0.000   0.0% |
   Density:                            1.371     0.000   0.0% |
    Atomic density matrices:           0.086     0.086   0.2% |
    Mix:                               0.801     0.801   2.2% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.475     0.014   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.460     0.460   1.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       28.996     0.002   0.0% |
    Atomic:                            0.914     0.125   0.3% |
     XC Correction:                    0.789     0.789   2.1% ||
    Calculate atomic Hamiltonians:     0.029     0.029   0.1% |
    Communicate:                       0.348     0.348   0.9% |
    Hartree integrate/restrict:        0.612     0.612   1.6% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           8.638     0.204   0.5% |
     Communicate bwd 0:                1.585     1.585   4.3% |-|
     Communicate bwd 1:                1.573     1.573   4.2% |-|
     Communicate fwd 0:                1.446     1.446   3.9% |-|
     Communicate fwd 1:                1.552     1.552   4.2% |-|
     fft:                              0.785     0.785   2.1% ||
     fft2:                             1.493     1.493   4.0% |-|
    XC 3D grid:                       18.373    18.373  49.5% |-------------------|
    vbar:                              0.078     0.078   0.2% |
 Get canonical representation:         1.815     0.001   0.0% |
  LCAO eigensolver:                    0.036     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.005     0.005   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.029     0.029   0.1% |
  Update Kohn-Sham energy:             1.778     0.000   0.0% |
   Density:                            0.079     0.000   0.0% |
    Atomic density matrices:           0.005     0.005   0.0% |
    Mix:                               0.046     0.046   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.028     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.027     0.027   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.698     0.000   0.0% |
    Atomic:                            0.054     0.007   0.0% |
     XC Correction:                    0.047     0.047   0.1% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.020     0.020   0.1% |
    Hartree integrate/restrict:        0.034     0.034   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.509     0.012   0.0% |
     Communicate bwd 0:                0.096     0.096   0.3% |
     Communicate bwd 1:                0.093     0.093   0.2% |
     Communicate fwd 0:                0.085     0.085   0.2% |
     Communicate fwd 1:                0.091     0.091   0.2% |
     fft:                              0.045     0.045   0.1% |
     fft2:                             0.088     0.088   0.2% |
    XC 3D grid:                        1.074     1.074   2.9% ||
    vbar:                              0.005     0.005   0.0% |
ST tci:                                0.004     0.004   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.181     0.181   0.5% |
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.335     2.335   6.3% |--|
-------------------------------------------------------------
Total:                                          37.137 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:38:15 2019
