
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:14:40 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4185.453435

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*104*128 grid
  Fine grid: 208*208*256 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*256 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 59.66 MiB
    Density: 34.50 MiB
      Arrays: 33.34 MiB
      Localized functions: 1.16 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.90 MiB
      Arrays: 21.81 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 3.26 MiB
      C [qnM]: 0.05 MiB
      S, T [2 x qmm]: 0.10 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.11 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 82
Number of bands in calculation: 82
Bands to converge: occupied states only
Number of valence electrons: 22

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |              H                       |  
 |         |           H C H                      |  
 |         |                                      |  
 |         |             C                        |  
 |         |             C                        |  
 |         |                                      |  
 |         |             CH                       |  
 |         |           H   H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.883949    7.510348   11.536517    ( 0.0000,  0.0000,  0.0000)
   1 C      7.883949    7.510348   10.074532    ( 0.0000,  0.0000,  0.0000)
   2 C      7.883949    7.510348    8.854592    ( 0.0000,  0.0000,  0.0000)
   3 C      7.883949    7.510348    7.392607    ( 0.0000,  0.0000,  0.0000)
   4 H      7.883949    8.531044   11.929124    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.000000   11.929124    ( 0.0000,  0.0000,  0.0000)
   6 H      8.767898    7.000000   11.929124    ( 0.0000,  0.0000,  0.0000)
   7 H      7.883949    8.531044    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      8.767898    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.767898    0.000000    0.000000   104     0.1516
  2. axis:    no     0.000000   15.531044    0.000000   104     0.1493
  3. axis:    no     0.000000    0.000000   18.929124   128     0.1479

  Lengths:  15.767898  15.531044  18.929124
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1496

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:14:49  +0.54          -54.834889    0      1      
iter:   2  00:14:52  -0.58          -54.924909    0      1      
iter:   3  00:14:54  -1.26          -54.931349    0      1      
iter:   4  00:14:55  -2.92          -54.932171    0      1      
iter:   5  00:14:57  -4.13          -54.932207    0      1      
iter:   6  00:14:59  -5.20          -54.932210    0      1      
iter:   7  00:15:00  -6.11          -54.932210    0      1      
iter:   8  00:15:02  -6.19          -54.932210    0      1      
iter:   9  00:15:03  -7.34          -54.932210    0      1      
iter:  10  00:15:05  -8.60          -54.932210    0      1      
iter:  11  00:15:08 -10.19          -54.932210    0      1      

Converged after 11 iterations.

Dipole moment: (-0.000000, 0.000097, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4185.453435)

Kinetic:        +41.900659
Potential:      -47.544564
External:        +0.000000
XC:             -49.405610
Entropy (-ST):   +0.000000
Local:           +0.117305
--------------------------
Free energy:    -54.932210
Extrapolated:   -54.932210

Fermi levels: -2.41073, -2.41073

 Band  Eigenvalues  Occupancy
    0    -19.21905    2.00000
    1    -18.37580    2.00000
    2    -16.29476    2.00000
    3    -11.67923    2.00000
    4    -10.41211    2.00000
    5    -10.39253    2.00000
    6    -10.39229    2.00000
    7    -10.10775    2.00000
    8    -10.10748    2.00000
    9     -5.72071    2.00000
   10     -5.72070    2.00000
   11      0.89924    0.00000
   12      0.89949    0.00000
   13      2.19751    0.00000
   14      2.20653    0.00000
   15      3.78777    0.00000
   16      3.78850    0.00000
   17      4.51751    0.00000
   18      4.51785    0.00000
   19      4.72829    0.00000
   20      6.34984    0.00000
   21      8.84635    0.00000
   22      9.12586    0.00000
   23      9.36672    0.00000
   24      9.36745    0.00000
   25     10.35944    0.00000
   26     10.36030    0.00000
   27     11.86858    0.00000
   28     11.86909    0.00000
   29     14.57700    0.00000
   30     14.70008    0.00000
   31     14.98303    0.00000
   32     14.98334    0.00000
   33     15.03237    0.00000
   34     15.03333    0.00000
   35     15.36373    0.00000
   36     15.36490    0.00000
   37     18.72343    0.00000
   38     19.05227    0.00000
   39     19.05261    0.00000
   40     19.84046    0.00000
   41     23.15141    0.00000
   42     23.15251    0.00000
   43     23.31161    0.00000
   44     25.80358    0.00000
   45     26.30655    0.00000
   46     26.30730    0.00000
   47     27.91590    0.00000
   48     27.91590    0.00000
   49     30.54817    0.00000
   50     30.54842    0.00000
   51     31.13375    0.00000
   52     31.13428    0.00000
   53     31.49303    0.00000
   54     34.47707    0.00000
   55     34.76293    0.00000
   56     34.76296    0.00000
   57     36.26091    0.00000
   58     36.26166    0.00000
   59     36.50988    0.00000
   60     39.39198    0.00000
   61     39.39217    0.00000
   62     40.32240    0.00000
   63     40.32308    0.00000
   64     40.74679    0.00000
   65     43.21003    0.00000
   66     43.29386    0.00000
   67     44.96962    0.00000
   68     44.97042    0.00000
   69     47.90096    0.00000
   70     47.90104    0.00000
   71     50.34944    0.00000
   72     51.79298    0.00000
   73     51.79409    0.00000
   74     52.49328    0.00000
   75     53.63190    0.00000
   76     53.63473    0.00000
   77     58.64975    0.00000
   78     63.16143    0.00000
   79     63.80402    0.00000
   80     63.80580    0.00000
   81     69.39885    0.00000

Gap: 6.620 eV
Transition (v -> c):
  (s=0, k=0, n=10, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=11, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.015     0.015   0.1% |
LCAO WFS Initialize:                   1.586     0.044   0.2% |
 Hamiltonian:                          1.542     0.000   0.0% |
  Atomic:                              0.054     0.008   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.000     0.000   0.0% |
  Hartree integrate/restrict:          0.035     0.035   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.443     0.008   0.0% |
   Communicate bwd 0:                  0.090     0.090   0.3% |
   Communicate bwd 1:                  0.079     0.079   0.3% |
   Communicate fwd 0:                  0.086     0.086   0.3% |
   Communicate fwd 1:                  0.091     0.091   0.3% |
   fft:                                0.024     0.024   0.1% |
   fft2:                               0.065     0.065   0.2% |
  XC 3D grid:                          1.000     1.000   3.5% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            24.376     0.003   0.0% |
 Direct Minimisation step:            22.746     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.323     0.004   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.035     0.035   0.1% |
   Potential matrix:                   0.279     0.279   1.0% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.025     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.020     0.020   0.1% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.008     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.006     0.006   0.0% |
  Update Kohn-Sham energy:            22.385     0.000   0.0% |
   Density:                            0.914     0.000   0.0% |
    Atomic density matrices:           0.054     0.054   0.2% |
    Mix:                               0.581     0.581   2.1% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.273     0.010   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.262     0.262   0.9% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       21.470     0.001   0.0% |
    Atomic:                            0.756     0.106   0.4% |
     XC Correction:                    0.649     0.649   2.3% ||
    Calculate atomic Hamiltonians:     0.018     0.018   0.1% |
    Communicate:                       0.001     0.001   0.0% |
    Hartree integrate/restrict:        0.428     0.428   1.5% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.219     0.110   0.4% |
     Communicate bwd 0:                1.283     1.283   4.5% |-|
     Communicate bwd 1:                1.144     1.144   4.0% |-|
     Communicate fwd 0:                1.161     1.161   4.1% |-|
     Communicate fwd 1:                1.275     1.275   4.5% |-|
     fft:                              0.332     0.332   1.2% |
     fft2:                             0.913     0.913   3.2% ||
    XC 3D grid:                       13.985    13.985  49.4% |-------------------|
    vbar:                              0.059     0.059   0.2% |
 Get canonical representation:         1.627     0.001   0.0% |
  LCAO eigensolver:                    0.025     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.020     0.020   0.1% |
  Update Kohn-Sham energy:             1.602     0.000   0.0% |
   Density:                            0.066     0.000   0.0% |
    Atomic density matrices:           0.004     0.004   0.0% |
    Mix:                               0.043     0.043   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.019     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.019     0.019   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.535     0.000   0.0% |
    Atomic:                            0.054     0.007   0.0% |
     XC Correction:                    0.046     0.046   0.2% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.000     0.000   0.0% |
    Hartree integrate/restrict:        0.026     0.026   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.448     0.008   0.0% |
     Communicate bwd 0:                0.094     0.094   0.3% |
     Communicate bwd 1:                0.083     0.083   0.3% |
     Communicate fwd 0:                0.083     0.083   0.3% |
     Communicate fwd 1:                0.092     0.092   0.3% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.065     0.065   0.2% |
    XC 3D grid:                        1.001     1.001   3.5% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.003     0.003   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.182     0.182   0.6% |
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.139     2.139   7.6% |--|
-------------------------------------------------------------
Total:                                          28.307 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:15:08 2019
