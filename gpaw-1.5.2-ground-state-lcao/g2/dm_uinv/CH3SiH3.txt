
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:44:08 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -8988.913339

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*108*112 grid
  Fine grid: 216*216*224 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 56.35 MiB
    Density: 32.85 MiB
      Arrays: 31.44 MiB
      Localized functions: 1.40 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.68 MiB
      Arrays: 20.57 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 2.83 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.76 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 8
Number of atomic orbitals: 56
Number of bands in calculation: 56
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |               H                       |  
 |         |           H    H                      |  
 |         |              Si                       |  
 |         |                                       |  
 |         |            H C H                      |  
 |         |             H                         |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 C      8.204844    8.019762    7.391897    ( 0.0000,  0.0000,  0.0000)
   1 Si     8.204844    8.019762    9.272066    ( 0.0000,  0.0000,  0.0000)
   2 H      8.204844    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 H      7.321704    8.529643    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      9.087984    8.529643    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      8.204844    9.410996    9.795045    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.324145    9.795045    ( 0.0000,  0.0000,  0.0000)
   7 H      9.409688    7.324145    9.795045    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.409688    0.000000    0.000000   108     0.1519
  2. axis:    no     0.000000   16.410996    0.000000   108     0.1520
  3. axis:    no     0.000000    0.000000   16.795045   112     0.1500

  Lengths:  16.409688  16.410996  16.795045
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1513

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:44:17  +0.51          -34.858716    0      1      
iter:   2  23:44:19  +0.35          -34.920606    0      1      
iter:   3  23:44:20  -1.79          -34.951927    0      1      
iter:   4  23:44:22  -2.47          -34.952087    0      1      
iter:   5  23:44:23  -3.97          -34.952111    0      1      
iter:   6  23:44:25  -4.30          -34.952112    0      1      
iter:   7  23:44:26  -6.34          -34.952113    0      1      
iter:   8  23:44:28  -7.28          -34.952113    0      1      
iter:   9  23:44:29  -8.22          -34.952113    0      1      
iter:  10  23:44:31  -9.60          -34.952113    0      1      
iter:  11  23:44:34 -10.07          -34.952113    0      1      

Converged after 11 iterations.

Dipole moment: (-0.000000, 0.000005, -0.159862) |e|*Ang

Energy contributions relative to reference atoms: (reference = -8988.913339)

Kinetic:        +30.034144
Potential:      -33.913484
External:        +0.000000
XC:             -31.054664
Entropy (-ST):   +0.000000
Local:           -0.018109
--------------------------
Free energy:    -34.952113
Extrapolated:   -34.952113

Fermi levels: -3.58633, -3.58633

 Band  Eigenvalues  Occupancy
    0    -17.33678    2.00000
    1    -12.73925    2.00000
    2     -9.98828    2.00000
    3     -9.98799    2.00000
    4     -7.94885    2.00000
    5     -7.94884    2.00000
    6     -7.86024    2.00000
    7      0.68757    0.00000
    8      1.48209    0.00000
    9      1.48265    0.00000
   10      2.19229    0.00000
   11      3.01701    0.00000
   12      3.40334    0.00000
   13      3.40369    0.00000
   14      5.75586    0.00000
   15      5.75607    0.00000
   16      6.69586    0.00000
   17      7.18096    0.00000
   18      7.18112    0.00000
   19      9.34824    0.00000
   20     11.42172    0.00000
   21     11.42213    0.00000
   22     11.69675    0.00000
   23     11.77524    0.00000
   24     11.77642    0.00000
   25     15.22892    0.00000
   26     15.22992    0.00000
   27     15.52831    0.00000
   28     18.11035    0.00000
   29     19.40339    0.00000
   30     19.40347    0.00000
   31     20.07660    0.00000
   32     20.99260    0.00000
   33     20.99293    0.00000
   34     21.91721    0.00000
   35     26.52061    0.00000
   36     27.07383    0.00000
   37     27.07453    0.00000
   38     27.95017    0.00000
   39     29.92532    0.00000
   40     30.56264    0.00000
   41     30.56293    0.00000
   42     34.70160    0.00000
   43     34.70196    0.00000
   44     36.08255    0.00000
   45     38.25602    0.00000
   46     38.25611    0.00000
   47     40.29314    0.00000
   48     42.34056    0.00000
   49     43.77864    0.00000
   50     43.77983    0.00000
   51     45.10855    0.00000
   52     45.10893    0.00000
   53     50.53477    0.00000
   54     52.63055    0.00000
   55     52.63484    0.00000

Gap: 8.548 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.013     0.013   0.0% |
LCAO WFS Initialize:                   1.497     0.042   0.2% |
 Hamiltonian:                          1.455     0.000   0.0% |
  Atomic:                              0.047     0.001   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.018     0.018   0.1% |
  Hartree integrate/restrict:          0.028     0.028   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.410     0.007   0.0% |
   Communicate bwd 0:                  0.081     0.081   0.3% |
   Communicate bwd 1:                  0.077     0.077   0.3% |
   Communicate fwd 0:                  0.079     0.079   0.3% |
   Communicate fwd 1:                  0.086     0.086   0.3% |
   fft:                                0.027     0.027   0.1% |
   fft2:                               0.053     0.053   0.2% |
  XC 3D grid:                          0.944     0.944   3.7% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            21.406     0.002   0.0% |
 Direct Minimisation step:            19.871     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.209     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.013     0.013   0.1% |
   Potential matrix:                   0.191     0.191   0.7% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.017     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.014     0.014   0.1% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.005     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.004     0.004   0.0% |
  Update Kohn-Sham energy:            19.636     0.000   0.0% |
   Density:                            0.729     0.000   0.0% |
    Atomic density matrices:           0.024     0.024   0.1% |
    Mix:                               0.509     0.509   2.0% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.191     0.009   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.182     0.182   0.7% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       18.906     0.001   0.0% |
    Atomic:                            0.608     0.007   0.0% |
     XC Correction:                    0.601     0.601   2.3% ||
    Calculate atomic Hamiltonians:     0.015     0.015   0.1% |
    Communicate:                       0.231     0.231   0.9% |
    Hartree integrate/restrict:        0.344     0.344   1.3% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.356     0.104   0.4% |
     Communicate bwd 0:                1.077     1.077   4.2% |-|
     Communicate bwd 1:                1.031     1.031   4.0% |-|
     Communicate fwd 0:                1.025     1.025   4.0% |-|
     Communicate fwd 1:                1.086     1.086   4.2% |-|
     fft:                              0.345     0.345   1.3% ||
     fft2:                             0.687     0.687   2.7% ||
    XC 3D grid:                       12.297    12.297  48.0% |------------------|
    vbar:                              0.052     0.052   0.2% |
 Get canonical representation:         1.532     0.000   0.0% |
  LCAO eigensolver:                    0.016     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.014     0.014   0.1% |
  Update Kohn-Sham energy:             1.515     0.000   0.0% |
   Density:                            0.056     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.039     0.039   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.014     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.014     0.014   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.459     0.000   0.0% |
    Atomic:                            0.047     0.001   0.0% |
     XC Correction:                    0.047     0.047   0.2% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.017     0.017   0.1% |
    Hartree integrate/restrict:        0.027     0.027   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.412     0.009   0.0% |
     Communicate bwd 0:                0.084     0.084   0.3% |
     Communicate bwd 1:                0.079     0.079   0.3% |
     Communicate fwd 0:                0.077     0.077   0.3% |
     Communicate fwd 1:                0.084     0.084   0.3% |
     fft:                              0.027     0.027   0.1% |
     fft2:                             0.052     0.052   0.2% |
    XC 3D grid:                        0.950     0.950   3.7% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.529     0.529   2.1% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.162     2.162   8.4% |--|
-------------------------------------------------------------
Total:                                          25.612 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:44:34 2019
