
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:37:06 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -3599.428546

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*108*112 grid
  Fine grid: 208*216*224 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*216*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 53.43 MiB
    Density: 31.15 MiB
      Arrays: 30.27 MiB
      Localized functions: 0.88 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.87 MiB
      Arrays: 19.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 2.40 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.31 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 8
Number of atomic orbitals: 64
Number of bands in calculation: 64
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |           H   HH                     |  
 |         |             CN                       |  
 |         |             C                        |  
 |         |           H   H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.917211    7.311073    9.019468    ( 0.0000,  0.0000,  0.0000)
   1 N      7.917211    8.583588    8.280047    ( 0.0000,  0.0000,  0.0000)
   2 C      7.917211    7.311073    7.540626    ( 0.0000,  0.0000,  0.0000)
   3 H      8.858713    8.976638    8.280047    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    7.103473    9.560094    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.103473    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      8.825070    7.000000    9.529080    ( 0.0000,  0.0000,  0.0000)
   7 H      8.825070    7.000000    7.031014    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.858713    0.000000    0.000000   104     0.1525
  2. axis:    no     0.000000   15.976638    0.000000   108     0.1479
  3. axis:    no     0.000000    0.000000   16.560094   112     0.1479

  Lengths:  15.858713  15.976638  16.560094
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1494

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:37:14  +0.83          -42.075468    0      1      
iter:   2  23:37:17  -0.07          -42.153451    0      1      
iter:   3  23:37:18  -0.41          -42.162356    0      1      
iter:   4  23:37:20  -2.48          -42.165639    0      1      
iter:   5  23:37:21  -3.44          -42.165722    0      1      
iter:   6  23:37:23  -4.19          -42.165738    0      1      
iter:   7  23:37:24  -4.40          -42.165742    0      1      
iter:   8  23:37:26  -4.62          -42.165742    0      1      
iter:   9  23:37:27  -5.74          -42.165742    0      1      
iter:  10  23:37:29  -6.36          -42.165742    0      1      
iter:  11  23:37:30  -7.70          -42.165742    0      1      
iter:  12  23:37:32  -8.30          -42.165742    0      1      
iter:  13  23:37:33  -8.92          -42.165742    0      1      
iter:  14  23:37:35  -9.07          -42.165742    0      1      
iter:  15  23:37:37 -10.21          -42.165742    0      1      

Converged after 15 iterations.

Dipole moment: (0.245342, -0.186901, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -3599.428546)

Kinetic:        +34.984990
Potential:      -37.554889
External:        +0.000000
XC:             -39.740865
Entropy (-ST):   +0.000000
Local:           +0.145022
--------------------------
Free energy:    -42.165742
Extrapolated:   -42.165742

Fermi levels: -2.05544, -2.05544

 Band  Eigenvalues  Occupancy
    0    -23.82129    2.00000
    1    -16.10641    2.00000
    2    -15.04236    2.00000
    3    -12.69999    2.00000
    4    -11.35079    2.00000
    5     -8.92193    2.00000
    6     -8.19354    2.00000
    7     -7.96410    2.00000
    8     -5.59094    2.00000
    9      1.48006    0.00000
   10      2.49358    0.00000
   11      2.52269    0.00000
   12      3.11382    0.00000
   13      3.13197    0.00000
   14      5.08912    0.00000
   15      5.39805    0.00000
   16      6.89627    0.00000
   17      8.06334    0.00000
   18      9.22940    0.00000
   19     10.68671    0.00000
   20     11.03922    0.00000
   21     11.97098    0.00000
   22     12.74224    0.00000
   23     12.88760    0.00000
   24     13.80166    0.00000
   25     14.11560    0.00000
   26     15.97999    0.00000
   27     16.18632    0.00000
   28     17.31871    0.00000
   29     17.56812    0.00000
   30     18.56108    0.00000
   31     20.33587    0.00000
   32     21.15504    0.00000
   33     23.56967    0.00000
   34     26.68867    0.00000
   35     27.81276    0.00000
   36     27.94909    0.00000
   37     28.35757    0.00000
   38     29.96167    0.00000
   39     32.93354    0.00000
   40     33.32173    0.00000
   41     34.06860    0.00000
   42     34.19586    0.00000
   43     34.83063    0.00000
   44     35.92956    0.00000
   45     37.63151    0.00000
   46     39.13388    0.00000
   47     39.16002    0.00000
   48     40.95956    0.00000
   49     41.34990    0.00000
   50     43.64852    0.00000
   51     43.85320    0.00000
   52     44.88723    0.00000
   53     47.65732    0.00000
   54     49.01779    0.00000
   55     49.23713    0.00000
   56     50.31879    0.00000
   57     53.73278    0.00000
   58     53.78562    0.00000
   59     54.40595    0.00000
   60     57.24061    0.00000
   61     62.55261    0.00000
   62     63.70458    0.00000
   63     64.80953    0.00000

Gap: 7.071 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.012     0.012   0.0% |
LCAO WFS Initialize:                   1.433     0.040   0.1% |
 Hamiltonian:                          1.393     0.000   0.0% |
  Atomic:                              0.047     0.001   0.0% |
   XC Correction:                      0.046     0.046   0.1% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.002     0.002   0.0% |
  Hartree integrate/restrict:          0.027     0.027   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.398     0.007   0.0% |
   Communicate bwd 0:                  0.079     0.079   0.2% |
   Communicate bwd 1:                  0.074     0.074   0.2% |
   Communicate fwd 0:                  0.076     0.076   0.2% |
   Communicate fwd 1:                  0.081     0.081   0.3% |
   fft:                                0.026     0.026   0.1% |
   fft2:                               0.055     0.055   0.2% |
  XC 3D grid:                          0.914     0.914   2.9% ||
  vbar:                                0.004     0.004   0.0% |
P tci:                                 0.005     0.005   0.0% |
SCF-cycle:                            27.878     0.003   0.0% |
 Direct Minimisation step:            26.415     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.329     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.003     0.003   0.0% |
   Distribute overlap matrix:          0.046     0.046   0.1% |
   Potential matrix:                   0.272     0.272   0.9% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.019     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.015     0.015   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.009     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.007     0.007   0.0% |
  Update Kohn-Sham energy:            26.053     0.000   0.0% |
   Density:                            1.042     0.000   0.0% |
    Atomic density matrices:           0.077     0.077   0.2% |
    Mix:                               0.690     0.690   2.2% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.269     0.010   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.257     0.257   0.8% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       25.010     0.002   0.0% |
    Atomic:                            0.844     0.011   0.0% |
     XC Correction:                    0.833     0.833   2.6% ||
    Calculate atomic Hamiltonians:     0.020     0.020   0.1% |
    Communicate:                       0.031     0.031   0.1% |
    Hartree integrate/restrict:        0.504     0.504   1.6% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           7.162     0.132   0.4% |
     Communicate bwd 0:                1.456     1.456   4.6% |-|
     Communicate bwd 1:                1.379     1.379   4.3% |-|
     Communicate fwd 0:                1.329     1.329   4.2% |-|
     Communicate fwd 1:                1.421     1.421   4.5% |-|
     fft:                              0.458     0.458   1.4% ||
     fft2:                             0.987     0.987   3.1% ||
    XC 3D grid:                       16.376    16.376  51.3% |--------------------|
    vbar:                              0.069     0.069   0.2% |
 Get canonical representation:         1.460     0.000   0.0% |
  LCAO eigensolver:                    0.019     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.015     0.015   0.0% |
  Update Kohn-Sham energy:             1.441     0.000   0.0% |
   Density:                            0.060     0.000   0.0% |
    Atomic density matrices:           0.004     0.004   0.0% |
    Mix:                               0.040     0.040   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.015     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.014     0.014   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.381     0.000   0.0% |
    Atomic:                            0.047     0.001   0.0% |
     XC Correction:                    0.046     0.046   0.1% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.002     0.002   0.0% |
    Hartree integrate/restrict:        0.028     0.028   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.390     0.006   0.0% |
     Communicate bwd 0:                0.079     0.079   0.2% |
     Communicate bwd 1:                0.075     0.075   0.2% |
     Communicate fwd 0:                0.072     0.072   0.2% |
     Communicate fwd 1:                0.078     0.078   0.2% |
     fft:                              0.026     0.026   0.1% |
     fft2:                             0.055     0.055   0.2% |
    XC 3D grid:                        0.909     0.909   2.8% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.505     0.505   1.6% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.084     2.084   6.5% |--|
-------------------------------------------------------------
Total:                                          31.922 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:37:37 2019
