
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:03:09 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2065.832626

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*104*96 grid
  Fine grid: 184*208*192 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*208*192 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 37.46 MiB
    Density: 22.39 MiB
      Arrays: 22.03 MiB
      Localized functions: 0.36 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 14.44 MiB
      Arrays: 14.41 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.03 MiB
    Wavefunctions: 0.63 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.62 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 23
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           O                     |  
 |         |           H                     |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 O      7.000000    7.763239    7.596309    ( 0.0000,  0.0000,  0.0000)
   1 H      7.000000    8.526478    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   15.526478    0.000000   104     0.1493
  3. axis:    no     0.000000    0.000000   14.596309    96     0.1520

  Lengths:  14.000000  15.526478  14.596309
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1512

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:03:15  +0.46          -13.231690    0      1      
iter:   2  00:03:16  -0.20          -13.243418    0      1      
iter:   3  00:03:17  -2.03          -13.244521    0      1      
iter:   4  00:03:18  -2.98          -13.244567    0      1      
iter:   5  00:03:19  -4.40          -13.244575    0      1      
iter:   6  00:03:20  -5.09          -13.244576    0      1      
iter:   7  00:03:22  -4.93          -13.244576    0      1      
iter:   8  00:03:23  -7.66          -13.244576    0      1      
iter:   9  00:03:24  -9.07          -13.244576    0      1      
iter:  10  00:03:26 -10.69          -13.244576    0      1      

Converged after 10 iterations.

Dipole moment: (-0.000000, -0.000000, -0.364910) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2065.832626)

Kinetic:        +14.010071
Potential:      -14.104464
External:        +0.000000
XC:             -13.217975
Entropy (-ST):   +0.000000
Local:           +0.067792
--------------------------
Free energy:    -13.244576
Extrapolated:   -13.244576

Fermi levels: -2.58718, -2.58718

 Band  Eigenvalues  Occupancy
    0    -24.69102    2.00000
    1    -12.57496    2.00000
    2     -8.57095    2.00000
    3     -6.40670    2.00000
    4      1.23234    0.00000
    5      3.42578    0.00000
    6     11.14830    0.00000
    7     12.81971    0.00000
    8     17.07329    0.00000
    9     17.12364    0.00000
   10     21.71258    0.00000
   11     22.20604    0.00000
   12     22.63564    0.00000
   13     26.67052    0.00000
   14     33.65738    0.00000
   15     37.05265    0.00000
   16     43.53948    0.00000
   17     43.75001    0.00000
   18     55.53463    0.00000
   19     56.02584    0.00000
   20     60.37722    0.00000
   21     67.66721    0.00000
   22     72.05346    0.00000

Gap: 7.639 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.004     0.004   0.0% |
LCAO WFS Initialize:                   1.053     0.027   0.2% |
 Hamiltonian:                          1.026     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.048     0.048   0.3% |
  Hartree integrate/restrict:          0.020     0.020   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.291     0.007   0.0% |
   Communicate bwd 0:                  0.057     0.057   0.3% |
   Communicate bwd 1:                  0.055     0.055   0.3% |
   Communicate fwd 0:                  0.052     0.052   0.3% |
   Communicate fwd 1:                  0.056     0.056   0.3% |
   fft:                                0.020     0.020   0.1% |
   fft2:                               0.045     0.045   0.3% |
  XC 3D grid:                          0.663     0.663   3.7% ||
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            14.790     0.002   0.0% |
 Direct Minimisation step:            13.738     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.032     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.008     0.008   0.0% |
   Potential matrix:                   0.020     0.020   0.1% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.003     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Preconditioning::                    0.000     0.000   0.0% |
  Unitary rotation:                    0.005     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.004     0.004   0.0% |
  Update Kohn-Sham energy:            13.694     0.000   0.0% |
   Density:                            0.382     0.000   0.0% |
    Atomic density matrices:           0.015     0.015   0.1% |
    Mix:                               0.336     0.336   1.9% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.026     0.004   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.021     0.021   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       13.312     0.001   0.0% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.005     0.005   0.0% |
    Communicate:                       0.623     0.623   3.5% ||
    Hartree integrate/restrict:        0.260     0.260   1.5% ||
    New Kinetic Energy:                0.001     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.849     0.088   0.5% |
     Communicate bwd 0:                0.745     0.745   4.2% |-|
     Communicate bwd 1:                0.737     0.737   4.2% |-|
     Communicate fwd 0:                0.693     0.693   3.9% |-|
     Communicate fwd 1:                0.745     0.745   4.2% |-|
     fft:                              0.256     0.256   1.4% ||
     fft2:                             0.586     0.586   3.3% ||
    XC 3D grid:                        8.532     8.532  48.1% |------------------|
    vbar:                              0.038     0.038   0.2% |
 Get canonical representation:         1.050     0.000   0.0% |
  LCAO eigensolver:                    0.003     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Update Kohn-Sham energy:             1.047     0.000   0.0% |
   Density:                            0.029     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.026     0.026   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.002     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.002     0.002   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.017     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.048     0.048   0.3% |
    Hartree integrate/restrict:        0.020     0.020   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.292     0.007   0.0% |
     Communicate bwd 0:                0.057     0.057   0.3% |
     Communicate bwd 1:                0.055     0.055   0.3% |
     Communicate fwd 0:                0.052     0.052   0.3% |
     Communicate fwd 1:                0.056     0.056   0.3% |
     fft:                              0.020     0.020   0.1% |
     fft2:                             0.045     0.045   0.3% |
    XC 3D grid:                        0.654     0.654   3.7% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.202     0.202   1.1% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.672     1.672   9.4% |---|
-------------------------------------------------------------
Total:                                          17.722 100.0%

Memory usage: 416.48 MiB
Date: Sun Feb 17 00:03:26 2019
