
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:53:59 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4146.069181

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*112*104 grid
  Fine grid: 208*224*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*224*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 51.23 MiB
    Density: 30.02 MiB
      Arrays: 29.14 MiB
      Localized functions: 0.88 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.13 MiB
      Arrays: 19.06 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 2.08 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.00 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 59
Number of bands in calculation: 59
Bands to converge: occupied states only
Number of valence electrons: 18

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |             O                        |  
 |         |            H C  H                    |  
 |         |          H  C H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.919568    9.000401    7.219204    ( 0.0000,  0.0000,  0.0000)
   1 O      7.919568    8.268821    8.455828    ( 0.0000,  0.0000,  0.0000)
   2 C      7.919568    7.537241    7.219204    ( 0.0000,  0.0000,  0.0000)
   3 H      8.839136    9.537642    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    9.537642    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      8.839136    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.839136    0.000000    0.000000   104     0.1523
  2. axis:    no     0.000000   16.537642    0.000000   112     0.1477
  3. axis:    no     0.000000    0.000000   15.455828   104     0.1486

  Lengths:  15.839136  16.537642  15.455828
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1495

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:54:07  +0.74          -36.689666    0      1      
iter:   2  23:54:10  -0.13          -36.743811    0      1      
iter:   3  23:54:11  -0.38          -36.750857    0      1      
iter:   4  23:54:13  -2.57          -36.753899    0      1      
iter:   5  23:54:14  -3.48          -36.753980    0      1      
iter:   6  23:54:16  -4.09          -36.754000    0      1      
iter:   7  23:54:17  -4.12          -36.754003    0      1      
iter:   8  23:54:18  -4.47          -36.754004    0      1      
iter:   9  23:54:20  -5.37          -36.754004    0      1      
iter:  10  23:54:21  -6.40          -36.754004    0      1      
iter:  11  23:54:23  -8.35          -36.754004    0      1      
iter:  12  23:54:24  -9.13          -36.754004    0      1      
iter:  13  23:54:25  -9.74          -36.754004    0      1      
iter:  14  23:54:27  -9.78          -36.754004    0      1      
iter:  15  23:54:30 -12.01          -36.754004    0      1      

Converged after 15 iterations.

Dipole moment: (0.000000, 0.000000, -0.342053) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4146.069181)

Kinetic:        +31.783014
Potential:      -33.547170
External:        +0.000000
XC:             -35.107739
Entropy (-ST):   +0.000000
Local:           +0.117890
--------------------------
Free energy:    -36.754004
Extrapolated:   -36.754004

Fermi levels: -2.03658, -2.03658

 Band  Eigenvalues  Occupancy
    0    -26.74259    2.00000
    1    -16.58155    2.00000
    2    -15.54427    2.00000
    3    -12.42182    2.00000
    4    -11.68159    2.00000
    5     -9.47627    2.00000
    6     -9.21311    2.00000
    7     -7.51826    2.00000
    8     -5.88429    2.00000
    9      1.81112    0.00000
   10      2.07223    0.00000
   11      2.41842    0.00000
   12      2.88338    0.00000
   13      2.98124    0.00000
   14      4.92119    0.00000
   15      6.97816    0.00000
   16      8.14225    0.00000
   17      9.36795    0.00000
   18     10.74537    0.00000
   19     10.94576    0.00000
   20     11.77090    0.00000
   21     12.39042    0.00000
   22     14.39676    0.00000
   23     15.50491    0.00000
   24     16.32241    0.00000
   25     16.39371    0.00000
   26     17.31384    0.00000
   27     18.32509    0.00000
   28     20.42893    0.00000
   29     21.39321    0.00000
   30     22.88287    0.00000
   31     23.54833    0.00000
   32     26.57792    0.00000
   33     28.60093    0.00000
   34     29.98269    0.00000
   35     30.45458    0.00000
   36     32.31542    0.00000
   37     33.34743    0.00000
   38     33.36510    0.00000
   39     33.92373    0.00000
   40     34.46141    0.00000
   41     36.08887    0.00000
   42     38.99494    0.00000
   43     39.73031    0.00000
   44     40.36754    0.00000
   45     40.57202    0.00000
   46     43.70886    0.00000
   47     44.16393    0.00000
   48     47.60277    0.00000
   49     48.96634    0.00000
   50     49.64745    0.00000
   51     53.10356    0.00000
   52     54.83208    0.00000
   53     56.78311    0.00000
   54     62.63710    0.00000
   55     63.93668    0.00000
   56     64.22171    0.00000
   57     64.23161    0.00000
   58     64.69451    0.00000

Gap: 7.695 eV
Transition (v -> c):
  (s=0, k=0, n=8, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=9, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.011     0.011   0.0% |
LCAO WFS Initialize:                   1.387     0.038   0.1% |
 Hamiltonian:                          1.349     0.000   0.0% |
  Atomic:                              0.027     0.027   0.1% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.021     0.021   0.1% |
  Hartree integrate/restrict:          0.026     0.026   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.390     0.010   0.0% |
   Communicate bwd 0:                  0.076     0.076   0.2% |
   Communicate bwd 1:                  0.070     0.070   0.2% |
   Communicate fwd 0:                  0.071     0.071   0.2% |
   Communicate fwd 1:                  0.079     0.079   0.3% |
   fft:                                0.025     0.025   0.1% |
   fft2:                               0.059     0.059   0.2% |
  XC 3D grid:                          0.875     0.875   2.8% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            26.811     0.003   0.0% |
 Direct Minimisation step:            25.400     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.258     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.039     0.039   0.1% |
   Potential matrix:                   0.210     0.210   0.7% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.015     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.008     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.007     0.007   0.0% |
  Update Kohn-Sham energy:            25.113     0.000   0.0% |
   Density:                            0.923     0.000   0.0% |
    Atomic density matrices:           0.051     0.051   0.2% |
    Mix:                               0.648     0.648   2.1% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.218     0.010   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.206     0.206   0.7% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       24.189     0.002   0.0% |
    Atomic:                            0.487     0.486   1.6% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.032     0.032   0.1% |
    Communicate:                       0.381     0.381   1.2% |
    Hartree integrate/restrict:        0.446     0.446   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           7.026     0.187   0.6% |
     Communicate bwd 0:                1.367     1.367   4.4% |-|
     Communicate bwd 1:                1.324     1.324   4.3% |-|
     Communicate fwd 0:                1.259     1.259   4.1% |-|
     Communicate fwd 1:                1.384     1.384   4.5% |-|
     fft:                              0.436     0.436   1.4% ||
     fft2:                             1.070     1.070   3.5% ||
    XC 3D grid:                       15.745    15.745  51.1% |-------------------|
    vbar:                              0.068     0.068   0.2% |
 Get canonical representation:         1.407     0.000   0.0% |
  LCAO eigensolver:                    0.015     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Update Kohn-Sham energy:             1.392     0.000   0.0% |
   Density:                            0.052     0.000   0.0% |
    Atomic density matrices:           0.003     0.003   0.0% |
    Mix:                               0.037     0.037   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.012     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.011     0.011   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.340     0.000   0.0% |
    Atomic:                            0.027     0.027   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.021     0.021   0.1% |
    Hartree integrate/restrict:        0.025     0.025   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.390     0.010   0.0% |
     Communicate bwd 0:                0.076     0.076   0.2% |
     Communicate bwd 1:                0.071     0.071   0.2% |
     Communicate fwd 0:                0.072     0.072   0.2% |
     Communicate fwd 1:                0.076     0.076   0.2% |
     fft:                              0.024     0.024   0.1% |
     fft2:                             0.059     0.059   0.2% |
    XC 3D grid:                        0.871     0.871   2.8% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.521     0.521   1.7% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.047     2.047   6.7% |--|
-------------------------------------------------------------
Total:                                          30.783 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:54:30 2019
