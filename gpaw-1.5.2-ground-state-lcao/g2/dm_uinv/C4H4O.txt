
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:05:26 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6201.325412

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*120*112 grid
  Fine grid: 184*240*224 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*240*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 53.49 MiB
    Density: 30.93 MiB
      Arrays: 29.74 MiB
      Localized functions: 1.19 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.55 MiB
      Arrays: 19.46 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 3.02 MiB
      C [qnM]: 0.06 MiB
      S, T [2 x qmm]: 0.11 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.85 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 9
Number of atomic orbitals: 85
Number of bands in calculation: 85
Bands to converge: occupied states only
Number of valence electrons: 26

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .---------------------------------.  
           /|                                 |  
          / |                                 |  
         /  |                                 |  
        /   |                                 |  
       /    |                                 |  
      /     |                                 |  
     /      |                                 |  
    /       |                                 |  
   /        |                                 |  
  /         |                                 |  
 *          |                                 |  
 |          |                                 |  
 |          |            H                    |  
 |          |           O                     |  
 |          |                                 |  
 |          |         HCC                     |  
 |          |          CH                     |  
 |          |                                 |  
 |          |          H                      |  
 |          |                                 |  
 |          .---------------------------------.  
 |         /                                 /   
 |        /                                 /    
 |       /                                 /     
 |      /                                 /      
 |     /                                 /       
 |    /                                 /        
 |   /                                 /         
 |  /                                 /          
 | /                                 /           
 |/                                 /            
 *---------------------------------*             

Positions:
   0 O      7.000000    9.049359    9.983077    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000   10.144059    9.167777    ( 0.0000,  0.0000,  0.0000)
   2 C      7.000000    7.954659    9.167777    ( 0.0000,  0.0000,  0.0000)
   3 C      7.000000    9.762559    7.857577    ( 0.0000,  0.0000,  0.0000)
   4 C      7.000000    8.336159    7.857577    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000   11.098718    9.670851    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.000000    9.670851    ( 0.0000,  0.0000,  0.0000)
   7 H      7.000000   10.420187    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000    7.678531    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   18.098718    0.000000   120     0.1508
  3. axis:    no     0.000000    0.000000   16.983077   112     0.1516

  Lengths:  14.000000  18.098718  16.983077
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1515

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:05:35  +0.79          -54.428522    0      1      
iter:   2  00:05:38  -0.14          -54.605134    0      1      
iter:   3  00:05:39  -0.55          -54.619170    0      1      
iter:   4  00:05:41  -1.69          -54.623171    0      1      
iter:   5  00:05:42  -2.07          -54.623487    0      1      
iter:   6  00:05:44  -4.02          -54.623603    0      1      
iter:   7  00:05:45  -4.51          -54.623606    0      1      
iter:   8  00:05:47  -5.04          -54.623608    0      1      
iter:   9  00:05:48  -5.12          -54.623608    0      1      
iter:  10  00:05:50  -6.38          -54.623608    0      1      
iter:  11  00:05:51  -7.03          -54.623608    0      1      
iter:  12  00:05:53  -8.31          -54.623608    0      1      
iter:  13  00:05:54  -9.14          -54.623608    0      1      
iter:  14  00:05:56  -9.69          -54.623608    0      1      
iter:  15  00:05:59 -10.32          -54.623608    0      1      

Converged after 15 iterations.

Dipole moment: (-0.000000, -0.000000, -0.080099) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6201.325412)

Kinetic:        +44.961930
Potential:      -48.272864
External:        +0.000000
XC:             -51.504469
Entropy (-ST):   +0.000000
Local:           +0.191795
--------------------------
Free energy:    -54.623608
Extrapolated:   -54.623608

Fermi levels: -2.95536, -2.95536

 Band  Eigenvalues  Occupancy
    0    -27.71517    2.00000
    1    -19.71925    2.00000
    2    -18.35550    2.00000
    3    -14.67387    2.00000
    4    -13.90209    2.00000
    5    -13.16439    2.00000
    6    -10.76104    2.00000
    7    -10.75043    2.00000
    8     -9.76766    2.00000
    9     -9.53889    2.00000
   10     -8.73570    2.00000
   11     -6.64367    2.00000
   12     -5.42720    2.00000
   13     -0.48352    0.00000
   14      1.02199    0.00000
   15      2.30394    0.00000
   16      2.99156    0.00000
   17      3.29135    0.00000
   18      3.61777    0.00000
   19      4.20885    0.00000
   20      4.47554    0.00000
   21      7.12318    0.00000
   22      8.44115    0.00000
   23      9.22636    0.00000
   24      9.82107    0.00000
   25      9.91899    0.00000
   26     10.76807    0.00000
   27     10.78256    0.00000
   28     11.09108    0.00000
   29     11.53649    0.00000
   30     11.81215    0.00000
   31     12.50369    0.00000
   32     13.29705    0.00000
   33     13.49246    0.00000
   34     15.85608    0.00000
   35     16.73478    0.00000
   36     17.24205    0.00000
   37     18.54107    0.00000
   38     18.75813    0.00000
   39     19.63485    0.00000
   40     21.34785    0.00000
   41     21.97670    0.00000
   42     22.02539    0.00000
   43     22.52437    0.00000
   44     23.56035    0.00000
   45     23.61890    0.00000
   46     24.01724    0.00000
   47     24.70645    0.00000
   48     25.28690    0.00000
   49     27.89063    0.00000
   50     28.45880    0.00000
   51     30.07847    0.00000
   52     31.68756    0.00000
   53     32.22160    0.00000
   54     32.37786    0.00000
   55     32.56403    0.00000
   56     32.83476    0.00000
   57     35.76437    0.00000
   58     37.00248    0.00000
   59     37.28218    0.00000
   60     38.55415    0.00000
   61     38.55500    0.00000
   62     39.18648    0.00000
   63     40.16150    0.00000
   64     41.33506    0.00000
   65     41.50101    0.00000
   66     41.66126    0.00000
   67     42.42311    0.00000
   68     42.77172    0.00000
   69     43.62948    0.00000
   70     45.56797    0.00000
   71     50.50590    0.00000
   72     50.70251    0.00000
   73     52.03589    0.00000
   74     57.59315    0.00000
   75     58.26850    0.00000
   76     59.13069    0.00000
   77     59.77760    0.00000
   78     60.31089    0.00000
   79     61.56958    0.00000
   80     65.06625    0.00000
   81     65.94045    0.00000
   82     68.58338    0.00000
   83     76.48933    0.00000
   84     81.46546    0.00000

Gap: 4.944 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.001     0.001   0.0% |
Basis functions set positions:         0.015     0.015   0.0% |
LCAO WFS Initialize:                   1.486     0.040   0.1% |
 Hamiltonian:                          1.446     0.000   0.0% |
  Atomic:                              0.047     0.001   0.0% |
   XC Correction:                      0.046     0.046   0.1% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.006     0.006   0.0% |
  Hartree integrate/restrict:          0.024     0.024   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.402     0.011   0.0% |
   Communicate bwd 0:                  0.079     0.079   0.2% |
   Communicate bwd 1:                  0.072     0.072   0.2% |
   Communicate fwd 0:                  0.073     0.073   0.2% |
   Communicate fwd 1:                  0.080     0.080   0.2% |
   fft:                                0.022     0.022   0.1% |
   fft2:                               0.066     0.066   0.2% |
  XC 3D grid:                          0.957     0.957   3.0% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.001     0.001   0.0% |
SCF-cycle:                            27.937     0.003   0.0% |
 Direct Minimisation step:            26.464     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.428     0.005   0.0% |
   Construct Gradient Matrix:          0.004     0.004   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.128     0.128   0.4% |
   Potential matrix:                   0.288     0.288   0.9% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.025     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.007     0.007   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.016     0.016   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.010     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.008     0.008   0.0% |
  Update Kohn-Sham energy:            25.993     0.000   0.0% |
   Density:                            1.104     0.000   0.0% |
    Atomic density matrices:           0.151     0.151   0.5% |
    Mix:                               0.662     0.662   2.1% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.284     0.011   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.272     0.272   0.8% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       24.889     0.002   0.0% |
    Atomic:                            0.850     0.010   0.0% |
     XC Correction:                    0.841     0.841   2.6% ||
    Calculate atomic Hamiltonians:     0.026     0.026   0.1% |
    Communicate:                       0.114     0.114   0.4% |
    Hartree integrate/restrict:        0.483     0.483   1.5% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           7.217     0.193   0.6% |
     Communicate bwd 0:                1.396     1.396   4.4% |-|
     Communicate bwd 1:                1.359     1.359   4.2% |-|
     Communicate fwd 0:                1.304     1.304   4.1% |-|
     Communicate fwd 1:                1.399     1.399   4.4% |-|
     fft:                              0.381     0.381   1.2% |
     fft2:                             1.184     1.184   3.7% ||
    XC 3D grid:                       16.126    16.126  50.3% |-------------------|
    vbar:                              0.069     0.069   0.2% |
 Get canonical representation:         1.470     0.001   0.0% |
  LCAO eigensolver:                    0.025     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.007     0.007   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.016     0.016   0.0% |
  Update Kohn-Sham energy:             1.444     0.000   0.0% |
   Density:                            0.063     0.000   0.0% |
    Atomic density matrices:           0.007     0.007   0.0% |
    Mix:                               0.039     0.039   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.017     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.017     0.017   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.381     0.000   0.0% |
    Atomic:                            0.047     0.001   0.0% |
     XC Correction:                    0.047     0.047   0.1% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.007     0.007   0.0% |
    Hartree integrate/restrict:        0.027     0.027   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.399     0.011   0.0% |
     Communicate bwd 0:                0.077     0.077   0.2% |
     Communicate bwd 1:                0.074     0.074   0.2% |
     Communicate fwd 0:                0.073     0.073   0.2% |
     Communicate fwd 1:                0.077     0.077   0.2% |
     fft:                              0.021     0.021   0.1% |
     fft2:                             0.066     0.066   0.2% |
    XC 3D grid:                        0.896     0.896   2.8% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.003     0.003   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.533     0.533   1.7% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.088     2.088   6.5% |--|
-------------------------------------------------------------
Total:                                          32.066 100.0%

Memory usage: 416.48 MiB
Date: Sun Feb 17 00:05:59 2019
