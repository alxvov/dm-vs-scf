
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:03:58 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

S-setup:
  name: Sulfur
  id: ca434db9faa07220b7a1d8cb6886b7a9
  Z: 16
  valence: 6
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/S.PBE.gz
  cutoffs: 0.76(comp), 1.77(filt), 1.66(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -17.254   0.974
    3p(4.00)    -7.008   0.979
    *s           9.957   0.974
    *p          20.203   0.979
    *d           0.000   0.900

  LCAO basis set for S:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/S.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5156 Bohr: 3s-sz confined orbital
      l=1, rc=6.9375 Bohr: 3p-sz confined orbital
      l=0, rc=3.0469 Bohr: 3s-dz split-valence wave
      l=1, rc=3.9375 Bohr: 3p-dz split-valence wave
      l=2, rc=6.9375 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -12987.817102

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*108*104 grid
  Fine grid: 240*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 57.88 MiB
    Density: 33.85 MiB
      Arrays: 32.45 MiB
      Localized functions: 1.40 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.34 MiB
      Arrays: 21.23 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 2.69 MiB
      C [qnM]: 0.04 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.58 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 9
Number of atomic orbitals: 69
Number of bands in calculation: 69
Bands to converge: occupied states only
Number of valence electrons: 20

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------------.  
          /|                                           |  
         / |                                           |  
        /  |                                           |  
       /   |                                           |  
      /    |                                           |  
     /     |                                           |  
    /      |                                           |  
   /       |                                           |  
  /        |                                           |  
 *         |                                           |  
 |         |                                           |  
 |         |                                           |  
 |         |                H                          |  
 |         |                C   H H                    |  
 |         |           H S  H                          |  
 |         |                    H                      |  
 |         |                                           |  
 |         |                                           |  
 |         .-------------------------------------------.  
 |        /                                           /   
 |       /                                           /    
 |      /                                           /     
 |     /                                           /      
 |    /                                           /       
 |   /                                           /        
 |  /                                           /         
 | /                                           /          
 |/                                           /           
 *-------------------------------------------*            

Positions:
   0 C     10.549689    8.510696    7.885793    ( 0.0000,  0.0000,  0.0000)
   1 C      9.035346    8.657696    7.885793    ( 0.0000,  0.0000,  0.0000)
   2 S      8.279278    7.000000    7.885793    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.403546    7.885793    ( 0.0000,  0.0000,  0.0000)
   4 H      8.710376    9.207766    8.771586    ( 0.0000,  0.0000,  0.0000)
   5 H      8.710376    9.207766    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H     11.021849    9.496366    7.885793    ( 0.0000,  0.0000,  0.0000)
   7 H     10.890250    7.968929    8.771287    ( 0.0000,  0.0000,  0.0000)
   8 H     10.890250    7.968929    7.000299    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.021849    0.000000    0.000000   120     0.1502
  2. axis:    no     0.000000   16.496366    0.000000   108     0.1527
  3. axis:    no     0.000000    0.000000   15.771586   104     0.1516

  Lengths:  18.021849  16.496366  15.771586
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1515

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:04:06  +0.63          -42.673536    0      1      
iter:   2  00:04:09  -0.45          -42.757474    0      1      
iter:   3  00:04:11  -1.05          -42.764422    0      1      
iter:   4  00:04:12  -2.10          -42.765908    0      1      
iter:   5  00:04:14  -3.23          -42.766030    0      1      
iter:   6  00:04:15  -4.58          -42.766040    0      1      
iter:   7  00:04:17  -5.23          -42.766041    0      1      
iter:   8  00:04:18  -5.65          -42.766042    0      1      
iter:   9  00:04:20  -6.45          -42.766042    0      1      
iter:  10  00:04:21  -7.03          -42.766042    0      1      
iter:  11  00:04:23  -8.27          -42.766042    0      1      
iter:  12  00:04:24  -9.27          -42.766042    0      1      
iter:  13  00:04:25  -9.98          -42.766042    0      1      
iter:  14  00:04:28 -10.52          -42.766042    0      1      

Converged after 14 iterations.

Dipole moment: (0.003420, 0.283324, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -12987.817102)

Kinetic:        +39.319378
Potential:      -43.007469
External:        +0.000000
XC:             -39.106216
Entropy (-ST):   +0.000000
Local:           +0.028265
--------------------------
Free energy:    -42.766042
Extrapolated:   -42.766042

Fermi levels: -2.44823, -2.44823

 Band  Eigenvalues  Occupancy
    0    -19.82060    2.00000
    1    -17.31109    2.00000
    2    -14.68338    2.00000
    3    -11.19225    2.00000
    4    -11.18751    2.00000
    5     -9.43123    2.00000
    6     -8.76028    2.00000
    7     -8.68196    2.00000
    8     -7.65704    2.00000
    9     -5.14958    2.00000
   10      0.25312    0.00000
   11      1.40173    0.00000
   12      2.40393    0.00000
   13      3.04762    0.00000
   14      3.14898    0.00000
   15      3.86428    0.00000
   16      4.30847    0.00000
   17      4.84177    0.00000
   18      7.32799    0.00000
   19      8.13345    0.00000
   20      8.33755    0.00000
   21      9.06760    0.00000
   22      9.70431    0.00000
   23      9.74826    0.00000
   24     11.32945    0.00000
   25     11.57005    0.00000
   26     11.85923    0.00000
   27     12.91243    0.00000
   28     14.42242    0.00000
   29     14.50374    0.00000
   30     15.14231    0.00000
   31     15.32790    0.00000
   32     15.84071    0.00000
   33     16.18345    0.00000
   34     16.31606    0.00000
   35     16.60217    0.00000
   36     17.47462    0.00000
   37     20.93503    0.00000
   38     22.28619    0.00000
   39     22.51196    0.00000
   40     23.04446    0.00000
   41     24.70877    0.00000
   42     27.91329    0.00000
   43     27.91584    0.00000
   44     28.18515    0.00000
   45     28.99974    0.00000
   46     30.38271    0.00000
   47     31.97243    0.00000
   48     34.18746    0.00000
   49     34.25538    0.00000
   50     34.62075    0.00000
   51     36.14093    0.00000
   52     36.27004    0.00000
   53     36.94545    0.00000
   54     37.10969    0.00000
   55     37.90680    0.00000
   56     39.46300    0.00000
   57     42.43343    0.00000
   58     42.88276    0.00000
   59     43.55450    0.00000
   60     44.68699    0.00000
   61     46.06321    0.00000
   62     47.56914    0.00000
   63     49.50646    0.00000
   64     50.96043    0.00000
   65     52.82409    0.00000
   66     54.32969    0.00000
   67     54.69451    0.00000
   68     55.16455    0.00000

Gap: 5.403 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.014     0.014   0.0% |
LCAO WFS Initialize:                   1.437     0.043   0.1% |
 Hamiltonian:                          1.393     0.000   0.0% |
  Atomic:                              0.047     0.001   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.003     0.003   0.0% |
  Communicate:                         0.016     0.016   0.1% |
  Hartree integrate/restrict:          0.026     0.026   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.299     0.016   0.1% |
   Communicate bwd 0:                  0.022     0.022   0.1% |
   Communicate bwd 1:                  0.078     0.078   0.3% |
   Communicate fwd 0:                  0.077     0.077   0.3% |
   Communicate fwd 1:                  0.025     0.025   0.1% |
   fft:                                0.027     0.027   0.1% |
   fft2:                               0.053     0.053   0.2% |
  XC 3D grid:                          0.993     0.993   3.3% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            26.315     0.003   0.0% |
 Direct Minimisation step:            24.860     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.384     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.181     0.181   0.6% |
   Potential matrix:                   0.195     0.195   0.6% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.023     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.011     0.011   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.011     0.011   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.008     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.007     0.007   0.0% |
  Update Kohn-Sham energy:            24.439     0.000   0.0% |
   Density:                            1.103     0.000   0.0% |
    Atomic density matrices:           0.208     0.208   0.7% |
    Mix:                               0.691     0.691   2.3% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.197     0.012   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.184     0.184   0.6% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       23.336     0.002   0.0% |
    Atomic:                            0.795     0.009   0.0% |
     XC Correction:                    0.786     0.786   2.6% ||
    Calculate atomic Hamiltonians:     0.045     0.045   0.1% |
    Communicate:                       0.279     0.279   0.9% |
    Hartree integrate/restrict:        0.438     0.438   1.4% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.098     0.291   1.0% |
     Communicate bwd 0:                0.368     0.368   1.2% |
     Communicate bwd 1:                1.375     1.375   4.5% |-|
     Communicate fwd 0:                1.305     1.305   4.3% |-|
     Communicate fwd 1:                0.399     0.399   1.3% ||
     fft:                              0.459     0.459   1.5% ||
     fft2:                             0.902     0.902   3.0% ||
    XC 3D grid:                       16.607    16.607  54.5% |---------------------|
    vbar:                              0.070     0.070   0.2% |
 Get canonical representation:         1.452     0.001   0.0% |
  LCAO eigensolver:                    0.023     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.010     0.010   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Update Kohn-Sham energy:             1.428     0.000   0.0% |
   Density:                            0.064     0.000   0.0% |
    Atomic density matrices:           0.013     0.013   0.0% |
    Mix:                               0.039     0.039   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.012     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.011     0.011   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.364     0.000   0.0% |
    Atomic:                            0.047     0.001   0.0% |
     XC Correction:                    0.046     0.046   0.2% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.017     0.017   0.1% |
    Hartree integrate/restrict:        0.026     0.026   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.296     0.016   0.1% |
     Communicate bwd 0:                0.019     0.019   0.1% |
     Communicate bwd 1:                0.081     0.081   0.3% |
     Communicate fwd 0:                0.077     0.077   0.3% |
     Communicate fwd 1:                0.024     0.024   0.1% |
     fft:                              0.027     0.027   0.1% |
     fft2:                             0.053     0.053   0.2% |
    XC 3D grid:                        0.971     0.971   3.2% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.517     0.517   1.7% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.174     2.174   7.1% |--|
-------------------------------------------------------------
Total:                                          30.463 100.0%

Memory usage: 416.48 MiB
Date: Sun Feb 17 00:04:28 2019
