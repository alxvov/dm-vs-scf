
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:19:35 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -14595.801697

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*104 grid
  Fine grid: 184*184*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 50.73 MiB
    Density: 28.94 MiB
      Arrays: 28.39 MiB
      Localized functions: 0.55 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.14 MiB
      Arrays: 21.10 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.04 MiB
    Wavefunctions: 0.64 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.63 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 26
Bands to converge: occupied states only
Number of valence electrons: 13

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Cl                   |  
 |        |                                 |  
 |        |            O                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Cl     7.000000    7.000000    8.606787    ( 0.0000,  0.0000,  1.0000)
   1 O      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.606787   104     0.1501

  Lengths:  14.000000  14.000000  15.606787
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1515

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  00:19:43  +1.43           -4.360998    0      1        +1.0000
iter:   2  00:19:44  +1.22           -4.575842    0      1        +1.0000
iter:   3  00:19:46  -0.02           -4.675392    0      1        +1.0000
iter:   4  00:19:47  -1.02           -4.687304    0      1        +1.0000
iter:   5  00:19:49  -1.87           -4.689695    0      1        +1.0000
iter:   6  00:19:51  -2.47           -4.689801    0      1        +1.0000
iter:   7  00:19:54  -3.20           -4.689818    0      1        +1.0000
iter:   8  00:19:55  -4.25           -4.689822    0      1        +1.0000
iter:   9  00:19:57  -5.06           -4.689822    0      1        +1.0000
iter:  10  00:19:58  -5.64           -4.689822    0      1        +1.0000
iter:  11  00:20:00  -6.18           -4.689822    0      1        +1.0000
iter:  12  00:20:02  -6.07           -4.689822    0      1        +1.0000
iter:  13  00:20:03  -5.26           -4.689823    0      1        +1.0000
iter:  14  00:20:05  -4.46           -4.689823    0      1        +1.0000
iter:  15  00:20:06  -4.34           -4.689824    0      1        +1.0000
iter:  16  00:20:08  -3.61           -4.689831    0      1        +1.0000
iter:  17  00:20:11  -2.64           -4.689850    0      1        +1.0000
iter:  18  00:20:14  -2.46           -4.689856    0      1        +1.0000
iter:  19  00:20:17  -2.26           -4.689873    0      1        +1.0000
iter:  20  00:20:19  -2.64           -4.689933    0      1        +1.0000
iter:  21  00:20:22  -3.12           -4.689950    0      1        +1.0000
iter:  22  00:20:23  -3.75           -4.689955    0      1        +1.0000
iter:  23  00:20:25  -5.00           -4.689957    0      1        +1.0000
iter:  24  00:20:27  -5.53           -4.689957    0      1        +1.0000
iter:  25  00:20:28  -5.08           -4.689957    0      1        +1.0000
iter:  26  00:20:30  -4.54           -4.689957    0      1        +1.0000
iter:  27  00:20:31  -4.05           -4.689958    0      1        +1.0000
iter:  28  00:20:33  -3.62           -4.689961    0      1        +1.0000
iter:  29  00:20:34  -3.06           -4.689967    0      1        +1.0000
iter:  30  00:20:36  -2.83           -4.689983    0      1        +1.0000
iter:  31  00:20:38  -2.53           -4.690027    0      1        +1.0000
iter:  32  00:20:41  -2.03           -4.690081    0      1        +1.0000
iter:  33  00:20:42  -1.98           -4.690171    0      1        +1.0000
iter:  34  00:20:45  -2.09           -4.690277    0      1        +1.0000
iter:  35  00:20:47  -2.76           -4.690313    0      1        +1.0000
iter:  36  00:20:49  -3.14           -4.690340    0      1        +1.0000
iter:  37  00:20:50  -3.19           -4.690350    0      1        +1.0000
iter:  38  00:20:52  -3.19           -4.690360    0      1        +1.0000
iter:  39  00:20:53  -3.28           -4.690364    0      1        +1.0000
iter:  40  00:20:56  -3.75           -4.690367    0      1        +1.0000
iter:  41  00:20:58  -4.56           -4.690370    0      1        +1.0000
iter:  42  00:21:00  -5.62           -4.690370    0      1        +1.0000
iter:  43  00:21:01  -6.25           -4.690370    0      1        +1.0000
iter:  44  00:21:03  -6.70           -4.690370    0      1        +1.0000
iter:  45  00:21:04  -6.68           -4.690370    0      1        +1.0000
iter:  46  00:21:06  -5.87           -4.690370    0      1        +1.0000
iter:  47  00:21:07  -5.02           -4.690370    0      1        +1.0000
iter:  48  00:21:09  -4.88           -4.690370    0      1        +1.0000
iter:  49  00:21:11  -4.59           -4.690371    0      1        +1.0000
iter:  50  00:21:12  -3.96           -4.690374    0      1        +1.0000
iter:  51  00:21:14  -3.72           -4.690377    0      1        +1.0000
iter:  52  00:21:15  -3.56           -4.690380    0      1        +1.0000
iter:  53  00:21:17  -4.05           -4.690383    0      1        +1.0000
iter:  54  00:21:18  -4.25           -4.690383    0      1        +1.0000
iter:  55  00:21:20  -5.75           -4.690383    0      1        +1.0000
iter:  56  00:21:21  -6.38           -4.690383    0      1        +1.0000
iter:  57  00:21:23  -7.06           -4.690383    0      1        +1.0000
iter:  58  00:21:25  -7.23           -4.690383    0      1        +1.0000
iter:  59  00:21:26  -8.22           -4.690383    0      1        +1.0000
iter:  60  00:21:28  -8.71           -4.690383    0      1        +1.0000
iter:  61  00:21:29  -9.36           -4.690383    0      1        +1.0000
iter:  62  00:21:32 -10.61           -4.690383    0      1        +1.0000

Converged after 62 iterations.

Dipole moment: (-0.000000, 0.000000, 0.318890) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000008)
Local magnetic moments:
   0 Cl ( 0.000000,  0.000000,  0.129622)
   1 O  ( 0.000000,  0.000000,  0.476509)

Energy contributions relative to reference atoms: (reference = -14595.801697)

Kinetic:        +10.980172
Potential:       -8.096373
External:        +0.000000
XC:              -7.641136
Entropy (-ST):   +0.000000
Local:           +0.066954
--------------------------
Free energy:     -4.690383
Extrapolated:    -4.690383

Spin contamination: 0.021160 electrons
Fermi levels: -3.98707, -5.52447

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -26.65954    1.00000    -25.95094    1.00000
    1    -19.34111    1.00000    -18.68126    1.00000
    2    -11.55100    1.00000    -11.11450    1.00000
    3    -10.92706    1.00000     -9.78753    1.00000
    4     -9.99946    1.00000     -9.77738    1.00000
    5     -7.14743    1.00000     -5.66437    1.00000
    6     -6.02363    1.00000     -5.38457    0.00000
    7     -1.95050    0.00000     -1.57563    0.00000
    8     12.07045    0.00000     12.22850    0.00000
    9     12.28151    0.00000     12.67442    0.00000
   10     12.70998    0.00000     12.75987    0.00000
   11     13.44587    0.00000     14.15861    0.00000
   12     13.87581    0.00000     14.17350    0.00000
   13     13.87748    0.00000     14.30450    0.00000
   14     14.04757    0.00000     14.32618    0.00000
   15     15.47413    0.00000     15.65217    0.00000
   16     18.06204    0.00000     18.36085    0.00000
   17     18.66425    0.00000     19.43181    0.00000
   18     19.26795    0.00000     19.56926    0.00000
   19     23.92831    0.00000     24.28418    0.00000
   20     36.82867    0.00000     37.45955    0.00000
   21     45.17489    0.00000     46.30917    0.00000
   22     45.18800    0.00000     46.45556    0.00000
   23     48.60718    0.00000     49.75969    0.00000
   24     49.56733    0.00000     49.79237    0.00000
   25     61.69038    0.00000     62.20950    0.00000

Gap: 3.434 eV
Transition (v -> c):
  (s=1, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.003     0.003   0.0% |
LCAO WFS Initialize:                   1.554     0.048   0.0% |
 Hamiltonian:                          1.507     0.002   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.105     0.105   0.1% |
  Hartree integrate/restrict:          0.035     0.035   0.0% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.289     0.004   0.0% |
   Communicate bwd 0:                  0.056     0.056   0.0% |
   Communicate bwd 1:                  0.053     0.053   0.0% |
   Communicate fwd 0:                  0.051     0.051   0.0% |
   Communicate fwd 1:                  0.055     0.055   0.0% |
   fft:                                0.019     0.019   0.0% |
   fft2:                               0.051     0.051   0.0% |
  XC 3D grid:                          1.070     1.070   0.9% |
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                           114.364     0.018   0.0% |
 Direct Minimisation step:           112.788     0.008   0.0% |
  Broadcast gradients:                 0.001     0.001   0.0% |
  Calculate gradients:                 0.312     0.018   0.0% |
   Construct Gradient Matrix:          0.008     0.008   0.0% |
   DenseAtomicCorrection:              0.004     0.004   0.0% |
   Distribute overlap matrix:          0.065     0.065   0.1% |
   Potential matrix:                   0.209     0.209   0.2% |
   Residual:                           0.009     0.009   0.0% |
  Get Search Direction:                0.016     0.016   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Preconditioning::                    0.003     0.003   0.0% |
  Unitary rotation:                    0.050     0.005   0.0% |
   Broadcast coefficients:             0.001     0.001   0.0% |
   Eigendecomposition:                 0.044     0.044   0.0% |
  Update Kohn-Sham energy:           112.392     0.001   0.0% |
   Density:                            3.784     0.001   0.0% |
    Atomic density matrices:           0.101     0.101   0.1% |
    Mix:                               3.363     3.363   2.9% ||
    Multipole moments:                 0.008     0.008   0.0% |
    Normalize:                         0.020     0.020   0.0% |
    Pseudo density:                    0.290     0.063   0.1% |
     Calculate density matrix:         0.006     0.006   0.0% |
     Construct density:                0.220     0.220   0.2% |
     Symmetrize density:               0.001     0.001   0.0% |
   Hamiltonian:                      108.607     0.106   0.1% |
    Atomic:                            0.023     0.022   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.030     0.030   0.0% |
    Communicate:                       7.527     7.527   6.4% |--|
    Hartree integrate/restrict:        2.484     2.484   2.1% ||
    New Kinetic Energy:                0.010     0.003   0.0% |
     Pseudo part:                      0.007     0.007   0.0% |
    Poisson:                          21.091     0.325   0.3% |
     Communicate bwd 0:                4.112     4.112   3.5% ||
     Communicate bwd 1:                3.867     3.867   3.3% ||
     Communicate fwd 0:                3.778     3.778   3.2% ||
     Communicate fwd 1:                4.026     4.026   3.4% ||
     fft:                              1.339     1.339   1.1% |
     fft2:                             3.643     3.643   3.1% ||
    XC 3D grid:                       77.134    77.134  65.4% |-------------------------|
    vbar:                              0.204     0.204   0.2% |
 Get canonical representation:         1.559     0.000   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Update Kohn-Sham energy:             1.554     0.000   0.0% |
   Density:                            0.052     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.047     0.047   0.0% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.004     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.003     0.003   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.502     0.001   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.105     0.105   0.1% |
    Hartree integrate/restrict:        0.035     0.035   0.0% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.289     0.004   0.0% |
     Communicate bwd 0:                0.056     0.056   0.0% |
     Communicate bwd 1:                0.053     0.053   0.0% |
     Communicate fwd 0:                0.051     0.051   0.0% |
     Communicate fwd 1:                0.055     0.055   0.0% |
     fft:                              0.019     0.019   0.0% |
     fft2:                             0.051     0.051   0.0% |
    XC 3D grid:                        1.067     1.067   0.9% |
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.361     0.361   0.3% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.634     1.634   1.4% ||
-------------------------------------------------------------
Total:                                         117.920 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:21:33 2019
