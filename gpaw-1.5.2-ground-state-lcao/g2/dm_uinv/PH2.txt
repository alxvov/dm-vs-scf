
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:46:22 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

P-setup:
  name: Phosphorus
  id: 03b4a34d18bb161274a4ee27145ba70a
  Z: 15
  valence: 5
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/P.PBE.gz
  cutoffs: 0.95(comp), 1.69(filt), 1.81(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -13.968   0.953
    3p(3.00)    -5.506   0.953
    *s          13.244   0.953
    *p          21.705   0.953
    *d           0.000   0.953

  LCAO basis set for P:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/P.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.0938 Bohr: 3s-sz confined orbital
      l=1, rc=7.7031 Bohr: 3p-sz confined orbital
      l=0, rc=3.4375 Bohr: 3s-dz split-valence wave
      l=1, rc=4.4688 Bohr: 3p-dz split-valence wave
      l=2, rc=7.7031 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -9327.893983

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*108*100 grid
  Fine grid: 184*216*200 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*216*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 57.87 MiB
    Density: 32.83 MiB
      Arrays: 32.09 MiB
      Localized functions: 0.74 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 23.90 MiB
      Arrays: 23.85 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.13 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.12 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 23
Number of bands in calculation: 23
Bands to converge: occupied states only
Number of valence electrons: 7

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |           P                     |  
 |         |            H                    |  
 |         |          H                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 P      7.000000    8.025642    7.980864    ( 0.0000,  0.0000,  1.0000)
   1 H      7.000000    9.051284    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   16.051284    0.000000   108     0.1486
  3. axis:    no     0.000000    0.000000   14.980864   100     0.1498

  Lengths:  14.000000  16.051284  14.980864
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1502

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  23:46:29  +1.36          -10.270352    0      1        +1.0000
iter:   2  23:46:31  -0.26          -10.329968    0      1        +1.0000
iter:   3  23:46:32  -1.51          -10.332197    0      1        +1.0000
iter:   4  23:46:34  -2.67          -10.332357    0      1        +1.0000
iter:   5  23:46:36  -3.46          -10.332368    0      1        +1.0000
iter:   6  23:46:38  -4.67          -10.332370    0      1        +1.0000
iter:   7  23:46:39  -5.48          -10.332370    0      1        +1.0000
iter:   8  23:46:41  -6.02          -10.332370    0      1        +1.0000
iter:   9  23:46:43  -8.25          -10.332370    0      1        +1.0000
iter:  10  23:46:45  -9.25          -10.332370    0      1        +1.0000
iter:  11  23:46:48 -10.50          -10.332370    0      1        +1.0000

Converged after 11 iterations.

Dipole moment: (-0.000000, -0.000000, -0.126333) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 1.000000)
Local magnetic moments:
   0 P  ( 0.000000,  0.000000,  0.344205)
   1 H  ( 0.000000,  0.000000, -0.007406)
   2 H  ( 0.000000,  0.000000, -0.007406)

Energy contributions relative to reference atoms: (reference = -9327.893983)

Kinetic:        +12.333901
Potential:      -13.137806
External:        +0.000000
XC:              -9.502675
Entropy (-ST):   +0.000000
Local:           -0.025791
--------------------------
Free energy:    -10.332370
Extrapolated:   -10.332370

Spin contamination: 0.035272 electrons
Fermi levels: -2.63272, -5.34949

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -15.60721    1.00000    -14.90674    1.00000
    1     -9.35643    1.00000     -9.16563    1.00000
    2     -7.22313    1.00000     -6.85221    1.00000
    3     -5.72457    1.00000     -3.84676    0.00000
    4      0.45913    0.00000      0.61668    0.00000
    5      1.20790    0.00000      1.62574    0.00000
    6      6.59304    0.00000      6.72258    0.00000
    7      6.70761    0.00000      7.51395    0.00000
    8      8.66858    0.00000      9.19850    0.00000
    9      8.95915    0.00000      9.80474    0.00000
   10      9.24725    0.00000      9.99006    0.00000
   11      9.61195    0.00000     10.48512    0.00000
   12      9.62697    0.00000     10.80298    0.00000
   13     11.92433    0.00000     12.08686    0.00000
   14     15.17730    0.00000     15.89655    0.00000
   15     19.87125    0.00000     20.00776    0.00000
   16     19.91600    0.00000     20.73018    0.00000
   17     25.49261    0.00000     25.87285    0.00000
   18     26.03915    0.00000     26.04563    0.00000
   19     27.73899    0.00000     28.25259    0.00000
   20     29.37140    0.00000     29.47704    0.00000
   21     41.61716    0.00000     41.78111    0.00000
   22     43.07896    0.00000     43.27630    0.00000

Gap: 1.878 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=1, k=0, n=3, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.005     0.005   0.0% |
LCAO WFS Initialize:                   1.736     0.057   0.2% |
 Hamiltonian:                          1.679     0.002   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.106     0.106   0.4% |
  Hartree integrate/restrict:          0.041     0.041   0.2% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.317     0.009   0.0% |
   Communicate bwd 0:                  0.062     0.062   0.2% |
   Communicate bwd 1:                  0.057     0.057   0.2% |
   Communicate fwd 0:                  0.060     0.060   0.2% |
   Communicate fwd 1:                  0.060     0.060   0.2% |
   fft:                                0.019     0.019   0.1% |
   fft2:                               0.051     0.051   0.2% |
  XC 3D grid:                          1.209     1.209   4.6% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            22.699     0.003   0.0% |
 Direct Minimisation step:            20.958     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.087     0.003   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.001     0.001   0.0% |
   Distribute overlap matrix:          0.017     0.017   0.1% |
   Potential matrix:                   0.064     0.064   0.2% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.007     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.008     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.007     0.007   0.0% |
  Update Kohn-Sham energy:            20.851     0.000   0.0% |
   Density:                            0.775     0.000   0.0% |
    Atomic density matrices:           0.026     0.026   0.1% |
    Mix:                               0.662     0.662   2.5% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.081     0.012   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.068     0.068   0.3% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       20.075     0.021   0.1% |
    Atomic:                            0.003     0.003   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.006     0.006   0.0% |
    Communicate:                       1.287     1.287   4.9% |-|
    Hartree integrate/restrict:        0.469     0.469   1.8% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.810     0.100   0.4% |
     Communicate bwd 0:                0.750     0.750   2.8% ||
     Communicate bwd 1:                0.711     0.711   2.7% ||
     Communicate fwd 0:                0.690     0.690   2.6% ||
     Communicate fwd 1:                0.743     0.743   2.8% ||
     fft:                              0.219     0.219   0.8% |
     fft2:                             0.598     0.598   2.3% ||
    XC 3D grid:                       14.439    14.439  54.7% |---------------------|
    vbar:                              0.038     0.038   0.1% |
 Get canonical representation:         1.738     0.000   0.0% |
  LCAO eigensolver:                    0.007     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Update Kohn-Sham energy:             1.730     0.000   0.0% |
   Density:                            0.064     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.055     0.055   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.007     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.006     0.006   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.666     0.002   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.107     0.107   0.4% |
    Hartree integrate/restrict:        0.041     0.041   0.2% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.315     0.007   0.0% |
     Communicate bwd 0:                0.062     0.062   0.2% |
     Communicate bwd 1:                0.060     0.060   0.2% |
     Communicate fwd 0:                0.056     0.056   0.2% |
     Communicate fwd 1:                0.062     0.062   0.2% |
     fft:                              0.018     0.018   0.1% |
     fft2:                             0.050     0.050   0.2% |
    XC 3D grid:                        1.197     1.197   4.5% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.198     0.198   0.8% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.750     1.750   6.6% |--|
-------------------------------------------------------------
Total:                                          26.392 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:46:48 2019
