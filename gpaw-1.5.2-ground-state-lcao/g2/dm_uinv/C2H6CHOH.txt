
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:36:26 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -5223.657946

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*116*112 grid
  Fine grid: 240*232*224 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*232*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 66.98 MiB
    Density: 38.91 MiB
      Arrays: 37.59 MiB
      Localized functions: 1.32 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 24.69 MiB
      Arrays: 24.59 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.10 MiB
    Wavefunctions: 3.38 MiB
      C [qnM]: 0.06 MiB
      S, T [2 x qmm]: 0.13 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.18 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 12
Number of atomic orbitals: 92
Number of bands in calculation: 92
Bands to converge: occupied states only
Number of valence electrons: 26

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .--------------------------------------------.  
           /|                                            |  
          / |                                            |  
         /  |                                            |  
        /   |                                            |  
       /    |                                            |  
      /     |                                            |  
     /      |                                            |  
    /       |                                            |  
   /        |                                            |  
  /         |                                            |  
 *          |                                            |  
 |          |                                            |  
 |          |                                            |  
 |          |                H  H                        |  
 |          |           H    CO                          |  
 |          |           HC     H  H                      |  
 |          |            H                               |  
 |          |                  H                         |  
 |          |                                            |  
 |          .--------------------------------------------.  
 |         /                                            /   
 |        /                                            /    
 |       /                                            /     
 |      /                                            /      
 |     /                                            /       
 |    /                                            /        
 |   /                                            /         
 |  /                                            /          
 | /                                            /           
 |/                                            /            
 *--------------------------------------------*             

Positions:
   0 O      9.175003   10.148127    8.029673    ( 0.0000,  0.0000,  0.0000)
   1 C      9.146886    8.820895    8.567317    ( 0.0000,  0.0000,  0.0000)
   2 H     10.007277   10.560083    8.318496    ( 0.0000,  0.0000,  0.0000)
   3 H      9.155183    8.866581    9.667695    ( 0.0000,  0.0000,  0.0000)
   4 C      7.834537    8.220922    8.108210    ( 0.0000,  0.0000,  0.0000)
   5 C     10.348533    8.019956    8.092269    ( 0.0000,  0.0000,  0.0000)
   6 H      7.813807    8.177183    7.016180    ( 0.0000,  0.0000,  0.0000)
   7 H     10.350655    7.976619    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      7.000000    8.839429    8.444865    ( 0.0000,  0.0000,  0.0000)
   9 H     11.284274    8.485112    8.420353    ( 0.0000,  0.0000,  0.0000)
  10 H      7.709103    7.210161    8.505529    ( 0.0000,  0.0000,  0.0000)
  11 H     10.325548    7.000000    8.487156    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.284274    0.000000    0.000000   120     0.1524
  2. axis:    no     0.000000   17.560083    0.000000   116     0.1514
  3. axis:    no     0.000000    0.000000   16.667695   112     0.1488

  Lengths:  18.284274  17.560083  16.667695
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1508

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:36:36  +0.47          -61.589602    0      1      
iter:   2  23:36:40  -0.26          -61.697110    0      1      
iter:   3  23:36:41  -0.71          -61.710714    0      1      
iter:   4  23:36:43  -2.43          -61.715059    0      1      
iter:   5  23:36:45  -3.12          -61.715198    0      1      
iter:   6  23:36:46  -3.90          -61.715230    0      1      
iter:   7  23:36:48  -4.57          -61.715239    0      1      
iter:   8  23:36:50  -5.24          -61.715240    0      1      
iter:   9  23:36:52  -5.53          -61.715240    0      1      
iter:  10  23:36:53  -6.20          -61.715240    0      1      
iter:  11  23:36:55  -7.50          -61.715240    0      1      
iter:  12  23:36:57  -8.31          -61.715240    0      1      
iter:  13  23:36:59  -9.00          -61.715240    0      1      
iter:  14  23:37:00  -9.50          -61.715240    0      1      
iter:  15  23:37:02  -9.47          -61.715240    0      1      
iter:  16  23:37:06 -10.59          -61.715240    0      1      

Converged after 16 iterations.

Dipole moment: (0.208933, -0.109805, 0.155743) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5223.657946)

Kinetic:        +52.668544
Potential:      -57.766453
External:        +0.000000
XC:             -56.761653
Entropy (-ST):   +0.000000
Local:           +0.144322
--------------------------
Free energy:    -61.715240
Extrapolated:   -61.715240

Fermi levels: -2.27058, -2.27058

 Band  Eigenvalues  Occupancy
    0    -25.09001    2.00000
    1    -19.04393    2.00000
    2    -16.99249    2.00000
    3    -14.24808    2.00000
    4    -12.42359    2.00000
    5    -10.95570    2.00000
    6    -10.42426    2.00000
    7     -9.34830    2.00000
    8     -9.11192    2.00000
    9     -8.62547    2.00000
   10     -8.39799    2.00000
   11     -7.46873    2.00000
   12     -5.78987    2.00000
   13      1.24870    0.00000
   14      2.39174    0.00000
   15      2.94597    0.00000
   16      3.08761    0.00000
   17      3.29964    0.00000
   18      3.88862    0.00000
   19      4.31459    0.00000
   20      4.71008    0.00000
   21      5.29982    0.00000
   22      5.57770    0.00000
   23      6.35048    0.00000
   24      8.37657    0.00000
   25      8.41096    0.00000
   26      9.95373    0.00000
   27     10.30294    0.00000
   28     10.71501    0.00000
   29     12.00456    0.00000
   30     12.14471    0.00000
   31     13.34106    0.00000
   32     13.92104    0.00000
   33     14.89324    0.00000
   34     15.05512    0.00000
   35     15.54825    0.00000
   36     15.58008    0.00000
   37     16.05486    0.00000
   38     16.31700    0.00000
   39     16.32999    0.00000
   40     17.57637    0.00000
   41     18.28094    0.00000
   42     18.54342    0.00000
   43     20.86628    0.00000
   44     21.28278    0.00000
   45     22.10455    0.00000
   46     23.92787    0.00000
   47     24.45599    0.00000
   48     25.18481    0.00000
   49     26.15394    0.00000
   50     27.73053    0.00000
   51     28.61859    0.00000
   52     28.75508    0.00000
   53     29.48530    0.00000
   54     31.34403    0.00000
   55     32.58379    0.00000
   56     33.35762    0.00000
   57     33.75679    0.00000
   58     33.94764    0.00000
   59     34.60294    0.00000
   60     35.76302    0.00000
   61     36.43071    0.00000
   62     36.71995    0.00000
   63     36.93104    0.00000
   64     38.14119    0.00000
   65     38.42342    0.00000
   66     39.17545    0.00000
   67     40.31336    0.00000
   68     40.65405    0.00000
   69     41.00189    0.00000
   70     42.25106    0.00000
   71     43.42175    0.00000
   72     43.88456    0.00000
   73     44.62016    0.00000
   74     45.40810    0.00000
   75     47.09686    0.00000
   76     48.38495    0.00000
   77     49.48719    0.00000
   78     50.03947    0.00000
   79     51.11752    0.00000
   80     51.48568    0.00000
   81     51.94328    0.00000
   82     53.30758    0.00000
   83     54.43995    0.00000
   84     54.92167    0.00000
   85     55.36703    0.00000
   86     56.60797    0.00000
   87     59.52495    0.00000
   88     62.74641    0.00000
   89     64.05512    0.00000
   90     65.80920    0.00000
   91     74.66799    0.00000

Gap: 7.039 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.001     0.001   0.0% |
Basis functions set positions:         0.017     0.017   0.0% |
LCAO WFS Initialize:                   1.669     0.050   0.1% |
 Hamiltonian:                          1.620     0.000   0.0% |
  Atomic:                              0.093     0.047   0.1% |
   XC Correction:                      0.047     0.047   0.1% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.000     0.000   0.0% |
  Hartree integrate/restrict:          0.036     0.036   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.353     0.019   0.0% |
   Communicate bwd 0:                  0.022     0.022   0.1% |
   Communicate bwd 1:                  0.094     0.094   0.2% |
   Communicate fwd 0:                  0.086     0.086   0.2% |
   Communicate fwd 1:                  0.022     0.022   0.1% |
   fft:                                0.031     0.031   0.1% |
   fft2:                               0.078     0.078   0.2% |
  XC 3D grid:                          1.126     1.126   2.9% ||
  vbar:                                0.006     0.006   0.0% |
P tci:                                 0.007     0.007   0.0% |
SCF-cycle:                            34.573     0.004   0.0% |
 Direct Minimisation step:            32.843     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.547     0.006   0.0% |
   Construct Gradient Matrix:          0.004     0.004   0.0% |
   DenseAtomicCorrection:              0.003     0.003   0.0% |
   Distribute overlap matrix:          0.050     0.050   0.1% |
   Potential matrix:                   0.483     0.483   1.2% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.030     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.025     0.025   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.011     0.003   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.008     0.008   0.0% |
  Update Kohn-Sham energy:            32.246     0.000   0.0% |
   Density:                            1.445     0.000   0.0% |
    Atomic density matrices:           0.058     0.058   0.1% |
    Mix:                               0.907     0.907   2.3% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.005     0.005   0.0% |
    Pseudo density:                    0.472     0.016   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.454     0.454   1.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       30.800     0.002   0.0% |
    Atomic:                            1.790     0.900   2.3% ||
     XC Correction:                    0.889     0.889   2.3% ||
    Calculate atomic Hamiltonians:     0.032     0.032   0.1% |
    Communicate:                       0.000     0.000   0.0% |
    Hartree integrate/restrict:        0.590     0.590   1.5% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           6.842     0.362   0.9% |
     Communicate bwd 0:                0.485     0.485   1.2% |
     Communicate bwd 1:                1.806     1.806   4.6% |-|
     Communicate fwd 0:                1.655     1.655   4.2% |-|
     Communicate fwd 1:                0.449     0.449   1.1% |
     fft:                              0.599     0.599   1.5% ||
     fft2:                             1.487     1.487   3.8% |-|
    XC 3D grid:                       21.451    21.451  54.6% |---------------------|
    vbar:                              0.090     0.090   0.2% |
 Get canonical representation:         1.726     0.001   0.0% |
  LCAO eigensolver:                    0.030     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.025     0.025   0.1% |
  Update Kohn-Sham energy:             1.696     0.000   0.0% |
   Density:                            0.077     0.000   0.0% |
    Atomic density matrices:           0.003     0.003   0.0% |
    Mix:                               0.048     0.048   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.025     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.024     0.024   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.619     0.000   0.0% |
    Atomic:                            0.094     0.047   0.1% |
     XC Correction:                    0.047     0.047   0.1% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.000     0.000   0.0% |
    Hartree integrate/restrict:        0.033     0.033   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.364     0.019   0.0% |
     Communicate bwd 0:                0.024     0.024   0.1% |
     Communicate bwd 1:                0.097     0.097   0.2% |
     Communicate fwd 0:                0.089     0.089   0.2% |
     Communicate fwd 1:                0.024     0.024   0.1% |
     fft:                              0.032     0.032   0.1% |
     fft2:                             0.079     0.079   0.2% |
    XC 3D grid:                        1.120     1.120   2.8% ||
    vbar:                              0.005     0.005   0.0% |
ST tci:                                0.004     0.004   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.533     0.533   1.4% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.511     2.511   6.4% |--|
-------------------------------------------------------------
Total:                                          39.317 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:37:06 2019
