
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:54:30 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -6240.709666

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*128*92 grid
  Fine grid: 240*256*184 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*256*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 62.12 MiB
    Density: 35.50 MiB
      Arrays: 34.03 MiB
      Localized functions: 1.47 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 22.38 MiB
      Arrays: 22.26 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.23 MiB
      C [qnM]: 0.09 MiB
      S, T [2 x qmm]: 0.18 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.97 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 12
Number of atomic orbitals: 108
Number of bands in calculation: 108
Bands to converge: occupied states only
Number of valence electrons: 30

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
             .--------------------------------------------.  
            /|                                            |  
           / |                                            |  
          /  |                                            |  
         /   |                                            |  
        /    |                                            |  
       /     |                                            |  
      /      |                                            |  
     /       |                                            |  
    /        |                                            |  
   /         |                                            |  
  /          |                                            |  
 *           |                                            |  
 |           |                 H                          |  
 |           |           H C  C  C  H                     |  
 |           |         H  C  C  C H                       |  
 |           |              H                             |  
 |           .--------------------------------------------.  
 |          /                                            /   
 |         /                                            /    
 |        /                                            /     
 |       /                                            /      
 |      /                                            /       
 |     /                                            /        
 |    /                                            /         
 |   /                                            /          
 |  /                                            /           
 | /                                            /            
 |/                                            /             
 *--------------------------------------------*              

Positions:
   0 C      9.149787   10.877608    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 C     10.358107   10.179984    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 C     10.358107    8.784736    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 C      9.149787    8.087112    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 C      7.941467    8.784736    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 C      7.941467   10.179984    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      9.149787   11.964720    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H     11.299574   10.723540    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H     11.299574    8.241180    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      9.149787    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H      7.000000    8.241180    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.000000   10.723540    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.299574    0.000000    0.000000   120     0.1525
  2. axis:    no     0.000000   18.964720    0.000000   128     0.1482
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  18.299574  18.964720  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:54:39  +0.45          -73.741398    0      1      
iter:   2  23:54:41  +0.42          -73.782739    0      1      
iter:   3  23:54:43  -2.28          -73.842676    0      1      
iter:   4  23:54:44  -3.63          -73.842924    0      1      
iter:   5  23:54:46  -4.97          -73.842946    0      1      
iter:   6  23:54:48  -6.00          -73.842948    0      1      
iter:   7  23:54:49  -6.69          -73.842949    0      1      
iter:   8  23:54:51  -6.95          -73.842949    0      1      
iter:   9  23:54:53  -8.34          -73.842949    0      1      
iter:  10  23:54:55  -8.53          -73.842949    0      1      
iter:  11  23:54:58 -10.83          -73.842949    0      1      

Converged after 11 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6240.709666)

Kinetic:        +57.900723
Potential:      -64.644241
External:        +0.000000
XC:             -67.281323
Entropy (-ST):   +0.000000
Local:           +0.181893
--------------------------
Free energy:    -73.842949
Extrapolated:   -73.842949

Fermi levels: -3.54311, -3.54311

 Band  Eigenvalues  Occupancy
    0    -21.15130    2.00000
    1    -18.38553    2.00000
    2    -18.38551    2.00000
    3    -14.77111    2.00000
    4    -14.77046    2.00000
    5    -12.79156    2.00000
    6    -11.13786    2.00000
    7    -10.78102    2.00000
    8    -10.12228    2.00000
    9    -10.12226    2.00000
   10     -8.88189    2.00000
   11     -8.13640    2.00000
   12     -8.13618    2.00000
   13     -6.15827    2.00000
   14     -6.15818    2.00000
   15     -0.92804    0.00000
   16     -0.92796    0.00000
   17      1.63383    0.00000
   18      2.88778    0.00000
   19      2.88794    0.00000
   20      3.11601    0.00000
   21      3.85329    0.00000
   22      3.85436    0.00000
   23      4.52180    0.00000
   24      7.15027    0.00000
   25      7.15208    0.00000
   26      7.37459    0.00000
   27      7.37682    0.00000
   28      8.60733    0.00000
   29      8.62418    0.00000
   30      9.92671    0.00000
   31     10.92070    0.00000
   32     10.92092    0.00000
   33     11.46738    0.00000
   34     11.46908    0.00000
   35     11.56100    0.00000
   36     11.56111    0.00000
   37     11.92044    0.00000
   38     12.33508    0.00000
   39     12.77726    0.00000
   40     12.77742    0.00000
   41     14.59814    0.00000
   42     15.43937    0.00000
   43     15.91879    0.00000
   44     15.91966    0.00000
   45     16.08126    0.00000
   46     16.08186    0.00000
   47     16.54491    0.00000
   48     18.71191    0.00000
   49     20.43492    0.00000
   50     21.23252    0.00000
   51     21.57740    0.00000
   52     21.57740    0.00000
   53     22.11202    0.00000
   54     22.11289    0.00000
   55     23.57715    0.00000
   56     23.57727    0.00000
   57     24.23377    0.00000
   58     24.23467    0.00000
   59     25.64220    0.00000
   60     25.64976    0.00000
   61     25.69424    0.00000
   62     25.71781    0.00000
   63     26.63379    0.00000
   64     31.72132    0.00000
   65     32.33453    0.00000
   66     33.28879    0.00000
   67     33.37236    0.00000
   68     33.37254    0.00000
   69     33.46952    0.00000
   70     33.47168    0.00000
   71     34.80408    0.00000
   72     34.80903    0.00000
   73     35.19303    0.00000
   74     35.19306    0.00000
   75     35.71802    0.00000
   76     35.75227    0.00000
   77     35.75362    0.00000
   78     38.52452    0.00000
   79     38.53021    0.00000
   80     40.89252    0.00000
   81     40.90768    0.00000
   82     42.25078    0.00000
   83     42.25123    0.00000
   84     42.50294    0.00000
   85     42.50422    0.00000
   86     42.53818    0.00000
   87     42.53942    0.00000
   88     43.66258    0.00000
   89     43.66451    0.00000
   90     44.42182    0.00000
   91     44.42184    0.00000
   92     47.14335    0.00000
   93     47.43040    0.00000
   94     48.63711    0.00000
   95     52.73201    0.00000
   96     53.16075    0.00000
   97     53.54191    0.00000
   98     53.54569    0.00000
   99     61.15828    0.00000
  100     61.15856    0.00000
  101     64.20638    0.00000
  102     64.21268    0.00000
  103     70.24246    0.00000
  104     70.24560    0.00000
  105     73.93004    0.00000
  106     80.92832    0.00000
  107     90.49030    0.00000

Gap: 5.230 eV
Transition (v -> c):
  (s=0, k=0, n=14, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=15, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.001     0.001   0.0% |
Basis functions set positions:         0.018     0.018   0.1% |
LCAO WFS Initialize:                   1.668     0.047   0.2% |
 Hamiltonian:                          1.621     0.000   0.0% |
  Atomic:                              0.047     0.001   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.047     0.047   0.2% |
  Hartree integrate/restrict:          0.034     0.034   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.452     0.012   0.0% |
   Communicate bwd 0:                  0.086     0.086   0.3% |
   Communicate bwd 1:                  0.086     0.086   0.3% |
   Communicate fwd 0:                  0.080     0.080   0.3% |
   Communicate fwd 1:                  0.092     0.092   0.3% |
   fft:                                0.024     0.024   0.1% |
   fft2:                               0.072     0.072   0.3% |
  XC 3D grid:                          1.030     1.030   3.6% ||
  vbar:                                0.006     0.006   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            24.269     0.003   0.0% |
 Direct Minimisation step:            22.539     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.432     0.004   0.0% |
   Construct Gradient Matrix:          0.004     0.004   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.057     0.057   0.2% |
   Potential matrix:                   0.365     0.365   1.3% ||
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.035     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.005     0.005   0.0% |
   Orbital Layouts:                    0.002     0.002   0.0% |
   Potential matrix:                   0.028     0.028   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.009     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.007     0.007   0.0% |
  Update Kohn-Sham energy:            22.056     0.000   0.0% |
   Density:                            1.026     0.000   0.0% |
    Atomic density matrices:           0.089     0.089   0.3% |
    Mix:                               0.571     0.571   2.0% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.360     0.010   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.348     0.348   1.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       21.030     0.001   0.0% |
    Atomic:                            0.610     0.007   0.0% |
     XC Correction:                    0.602     0.602   2.1% ||
    Calculate atomic Hamiltonians:     0.031     0.031   0.1% |
    Communicate:                       0.618     0.618   2.2% ||
    Hartree integrate/restrict:        0.389     0.389   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.931     0.166   0.6% |
     Communicate bwd 0:                1.153     1.153   4.1% |-|
     Communicate bwd 1:                1.118     1.118   3.9% |-|
     Communicate fwd 0:                1.071     1.071   3.8% |-|
     Communicate fwd 1:                1.173     1.173   4.1% |-|
     fft:                              0.315     0.315   1.1% |
     fft2:                             0.934     0.934   3.3% ||
    XC 3D grid:                       13.392    13.392  47.2% |------------------|
    vbar:                              0.057     0.057   0.2% |
 Get canonical representation:         1.727     0.001   0.0% |
  LCAO eigensolver:                    0.034     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.005     0.005   0.0% |
   Orbital Layouts:                    0.002     0.002   0.0% |
   Potential matrix:                   0.028     0.028   0.1% |
  Update Kohn-Sham energy:             1.692     0.000   0.0% |
   Density:                            0.079     0.000   0.0% |
    Atomic density matrices:           0.006     0.006   0.0% |
    Mix:                               0.045     0.045   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.028     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.027     0.027   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.613     0.000   0.0% |
    Atomic:                            0.047     0.001   0.0% |
     XC Correction:                    0.046     0.046   0.2% |
    Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
    Communicate:                       0.047     0.047   0.2% |
    Hartree integrate/restrict:        0.030     0.030   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.457     0.013   0.0% |
     Communicate bwd 0:                0.088     0.088   0.3% |
     Communicate bwd 1:                0.086     0.086   0.3% |
     Communicate fwd 0:                0.082     0.082   0.3% |
     Communicate fwd 1:                0.091     0.091   0.3% |
     fft:                              0.024     0.024   0.1% |
     fft2:                             0.073     0.073   0.3% |
    XC 3D grid:                        1.024     1.024   3.6% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.004     0.004   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.183     0.183   0.6% |
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.239     2.239   7.9% |--|
-------------------------------------------------------------
Total:                                          28.384 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:54:58 2019
