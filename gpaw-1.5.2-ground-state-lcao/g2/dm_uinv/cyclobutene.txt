
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:47:17 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4185.453435

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*112*112 grid
  Fine grid: 208*224*224 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*224*224 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 56.37 MiB
    Density: 32.54 MiB
      Arrays: 31.40 MiB
      Localized functions: 1.14 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.63 MiB
      Arrays: 20.54 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 3.20 MiB
      C [qnM]: 0.05 MiB
      S, T [2 x qmm]: 0.10 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.05 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 10
Number of atomic orbitals: 82
Number of bands in calculation: 82
Bands to converge: occupied states only
Number of valence electrons: 22

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |              H                       |  
 |         |              C                       |  
 |         |             H                        |  
 |         |            H C H                     |  
 |         |             C                        |  
 |         |           H   H                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 C      7.889310    7.749631    8.953808    ( 0.0000,  0.0000,  0.0000)
   1 C      7.889310    9.095155    8.953808    ( 0.0000,  0.0000,  0.0000)
   2 C      7.889310    7.640413    7.445943    ( 0.0000,  0.0000,  0.0000)
   3 C      7.889310    9.204373    7.445943    ( 0.0000,  0.0000,  0.0000)
   4 H      7.889310    7.000000    9.740354    ( 0.0000,  0.0000,  0.0000)
   5 H      7.889310    9.844786    9.740354    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    7.183151    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H      8.778620    7.183151    7.000000    ( 0.0000,  0.0000,  0.0000)
   8 H      8.778620    9.661635    7.000000    ( 0.0000,  0.0000,  0.0000)
   9 H      7.000000    9.661635    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.778620    0.000000    0.000000   104     0.1517
  2. axis:    no     0.000000   16.844786    0.000000   112     0.1504
  3. axis:    no     0.000000    0.000000   16.740354   112     0.1495

  Lengths:  15.778620  16.844786  16.740354
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1505

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:47:26  +0.64          -54.979356    0      1      
iter:   2  23:47:27  +0.57          -55.031364    0      1      
iter:   3  23:47:29  -1.36          -55.085548    0      1      
iter:   4  23:47:30  -1.58          -55.085941    0      1      
iter:   5  23:47:32  -3.71          -55.086312    0      1      
iter:   6  23:47:34  -4.54          -55.086319    0      1      
iter:   7  23:47:35  -5.61          -55.086321    0      1      
iter:   8  23:47:37  -5.84          -55.086321    0      1      
iter:   9  23:47:38  -7.10          -55.086321    0      1      
iter:  10  23:47:40  -7.69          -55.086321    0      1      
iter:  11  23:47:41  -9.18          -55.086321    0      1      
iter:  12  23:47:44 -10.11          -55.086321    0      1      

Converged after 12 iterations.

Dipole moment: (0.000000, 0.000000, -0.034093) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4185.453435)

Kinetic:        +44.336700
Potential:      -48.816439
External:        +0.000000
XC:             -50.716592
Entropy (-ST):   +0.000000
Local:           +0.110011
--------------------------
Free energy:    -55.086321
Extrapolated:   -55.086321

Fermi levels: -3.13182, -3.13182

 Band  Eigenvalues  Occupancy
    0    -21.27479    2.00000
    1    -16.05424    2.00000
    2    -15.77851    2.00000
    3    -12.50520    2.00000
    4    -12.15471    2.00000
    5    -11.12959    2.00000
    6     -9.13858    2.00000
    7     -8.59406    2.00000
    8     -7.80203    2.00000
    9     -7.54348    2.00000
   10     -5.89525    2.00000
   11     -0.36839    0.00000
   12      2.21030    0.00000
   13      2.85115    0.00000
   14      2.95228    0.00000
   15      3.00130    0.00000
   16      3.64516    0.00000
   17      4.20605    0.00000
   18      4.70712    0.00000
   19      5.18879    0.00000
   20      7.36628    0.00000
   21      8.58752    0.00000
   22      8.85516    0.00000
   23      9.14345    0.00000
   24      9.41301    0.00000
   25     10.19493    0.00000
   26     10.52532    0.00000
   27     11.60557    0.00000
   28     12.27038    0.00000
   29     12.65676    0.00000
   30     13.63094    0.00000
   31     14.08279    0.00000
   32     14.90091    0.00000
   33     15.97377    0.00000
   34     15.99902    0.00000
   35     17.07985    0.00000
   36     17.38751    0.00000
   37     17.85906    0.00000
   38     18.71257    0.00000
   39     19.11058    0.00000
   40     20.83623    0.00000
   41     21.67123    0.00000
   42     23.49628    0.00000
   43     23.54265    0.00000
   44     24.89553    0.00000
   45     28.20778    0.00000
   46     28.33708    0.00000
   47     28.35122    0.00000
   48     29.65130    0.00000
   49     29.83540    0.00000
   50     30.99437    0.00000
   51     31.43919    0.00000
   52     32.63057    0.00000
   53     34.94641    0.00000
   54     35.01580    0.00000
   55     35.06534    0.00000
   56     36.04207    0.00000
   57     37.49028    0.00000
   58     38.17996    0.00000
   59     38.29807    0.00000
   60     38.65048    0.00000
   61     38.79215    0.00000
   62     39.35845    0.00000
   63     41.24464    0.00000
   64     41.30094    0.00000
   65     41.90374    0.00000
   66     42.58179    0.00000
   67     42.81940    0.00000
   68     45.77924    0.00000
   69     46.36082    0.00000
   70     47.41054    0.00000
   71     48.04123    0.00000
   72     48.92928    0.00000
   73     50.83026    0.00000
   74     51.65514    0.00000
   75     52.71047    0.00000
   76     54.84486    0.00000
   77     58.55205    0.00000
   78     59.32626    0.00000
   79     61.46416    0.00000
   80     62.36572    0.00000
   81     76.58272    0.00000

Gap: 5.527 eV
Transition (v -> c):
  (s=0, k=0, n=10, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=11, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.015     0.015   0.1% |
LCAO WFS Initialize:                   1.494     0.042   0.2% |
 Hamiltonian:                          1.452     0.000   0.0% |
  Atomic:                              0.053     0.007   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.001     0.001   0.0% |
  Hartree integrate/restrict:          0.028     0.028   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.417     0.007   0.0% |
   Communicate bwd 0:                  0.083     0.083   0.3% |
   Communicate bwd 1:                  0.076     0.076   0.3% |
   Communicate fwd 0:                  0.078     0.078   0.3% |
   Communicate fwd 1:                  0.087     0.087   0.3% |
   fft:                                0.027     0.027   0.1% |
   fft2:                               0.058     0.058   0.2% |
  XC 3D grid:                          0.943     0.943   3.5% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.003     0.003   0.0% |
SCF-cycle:                            23.180     0.003   0.0% |
 Direct Minimisation step:            21.635     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.383     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.055     0.055   0.2% |
   Potential matrix:                   0.319     0.319   1.2% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.028     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.023     0.023   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.008     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.006     0.006   0.0% |
  Update Kohn-Sham energy:            21.210     0.000   0.0% |
   Density:                            0.918     0.000   0.0% |
    Atomic density matrices:           0.064     0.064   0.2% |
    Mix:                               0.541     0.541   2.0% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.308     0.009   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.297     0.297   1.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       20.292     0.001   0.0% |
    Atomic:                            0.743     0.094   0.3% |
     XC Correction:                    0.649     0.649   2.4% ||
    Calculate atomic Hamiltonians:     0.014     0.014   0.1% |
    Communicate:                       0.013     0.013   0.0% |
    Hartree integrate/restrict:        0.382     0.382   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.843     0.110   0.4% |
     Communicate bwd 0:                1.174     1.174   4.4% |-|
     Communicate bwd 1:                1.111     1.111   4.1% |-|
     Communicate fwd 0:                1.087     1.087   4.0% |-|
     Communicate fwd 1:                1.170     1.170   4.3% |-|
     fft:                              0.368     0.368   1.4% ||
     fft2:                             0.823     0.823   3.1% ||
    XC 3D grid:                       13.237    13.237  49.1% |-------------------|
    vbar:                              0.056     0.056   0.2% |
 Get canonical representation:         1.542     0.001   0.0% |
  LCAO eigensolver:                    0.028     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.023     0.023   0.1% |
  Update Kohn-Sham energy:             1.513     0.000   0.0% |
   Density:                            0.067     0.000   0.0% |
    Atomic density matrices:           0.005     0.005   0.0% |
    Mix:                               0.040     0.040   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.022     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.021     0.021   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.446     0.000   0.0% |
    Atomic:                            0.053     0.007   0.0% |
     XC Correction:                    0.047     0.047   0.2% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.000     0.000   0.0% |
    Hartree integrate/restrict:        0.027     0.027   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.419     0.009   0.0% |
     Communicate bwd 0:                0.085     0.085   0.3% |
     Communicate bwd 1:                0.079     0.079   0.3% |
     Communicate fwd 0:                0.078     0.078   0.3% |
     Communicate fwd 1:                0.084     0.084   0.3% |
     fft:                              0.026     0.026   0.1% |
     fft2:                             0.058     0.058   0.2% |
    XC 3D grid:                        0.941     0.941   3.5% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.003     0.003   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.198     0.198   0.7% |
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.082     2.082   7.7% |--|
-------------------------------------------------------------
Total:                                          26.977 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:47:44 2019
