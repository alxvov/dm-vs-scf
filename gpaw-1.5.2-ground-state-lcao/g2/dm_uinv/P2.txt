
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:26:02 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

P-setup:
  name: Phosphorus
  id: 03b4a34d18bb161274a4ee27145ba70a
  Z: 15
  valence: 5
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/P.PBE.gz
  cutoffs: 0.95(comp), 1.69(filt), 1.81(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -13.968   0.953
    3p(3.00)    -5.506   0.953
    *s          13.244   0.953
    *p          21.705   0.953
    *d           0.000   0.953

  LCAO basis set for P:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/P.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.0938 Bohr: 3s-sz confined orbital
      l=1, rc=7.7031 Bohr: 3p-sz confined orbital
      l=0, rc=3.4375 Bohr: 3s-dz split-valence wave
      l=1, rc=4.4688 Bohr: 3p-dz split-valence wave
      l=2, rc=7.7031 Bohr: d-type Gaussian polarization

Reference energy: -18605.827317

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 239.30 MiB
  Calculator: 38.99 MiB
    Density: 23.10 MiB
      Arrays: 21.92 MiB
      Localized functions: 1.18 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 14.43 MiB
      Arrays: 14.34 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 1.47 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.45 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 26
Bands to converge: occupied states only
Number of valence electrons: 10

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            P                    |  
 |        |                                 |  
 |        |                                 |  
 |        |            P                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 P      7.000000    7.000000    8.932288    ( 0.0000,  0.0000,  0.0000)
   1 P      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   15.932288   108     0.1475

  Lengths:  14.000000  14.000000  15.932288
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1506

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:26:08  -0.38           -8.378378    0      1      
iter:   2  23:26:09  -1.20           -8.380536    0      1      
iter:   3  23:26:10  -5.12           -8.380874    0      1      
iter:   4  23:26:11  -6.73           -8.380874    0      1      
iter:   5  23:26:12  -8.03           -8.380874    0      1      
iter:   6  23:26:14 -10.28           -8.380874    0      1      

Converged after 6 iterations.

Dipole moment: (0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -18605.827317)

Kinetic:        +12.608808
Potential:      -13.575886
External:        +0.000000
XC:              -7.383589
Entropy (-ST):   +0.000000
Local:           -0.030207
--------------------------
Free energy:     -8.380874
Extrapolated:    -8.380874

Fermi levels: -4.89414, -4.89414

 Band  Eigenvalues  Occupancy
    0    -16.51801    2.00000
    1    -11.30567    2.00000
    2     -6.78952    2.00000
    3     -6.69097    2.00000
    4     -6.69097    2.00000
    5     -3.09732    0.00000
    6     -3.09732    0.00000
    7      1.50279    0.00000
    8      5.52039    0.00000
    9      7.24013    0.00000
   10      7.24013    0.00000
   11      9.01734    0.00000
   12      9.37107    0.00000
   13      9.37107    0.00000
   14      9.37277    0.00000
   15      9.37298    0.00000
   16      9.96361    0.00000
   17      9.96361    0.00000
   18     11.39456    0.00000
   19     12.50824    0.00000
   20     12.50848    0.00000
   21     17.81220    0.00000
   22     18.55988    0.00000
   23     18.59753    0.00000
   24     18.59753    0.00000
   25     29.49891    0.00000

Gap: 3.594 eV
Transition (v -> c):
  (s=0, k=0, n=4, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=5, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.005     0.005   0.0% |
LCAO WFS Initialize:                   1.087     0.028   0.2% |
 Hamiltonian:                          1.059     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.063     0.063   0.5% |
  Hartree integrate/restrict:          0.020     0.020   0.2% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.306     0.006   0.0% |
   Communicate bwd 0:                  0.058     0.058   0.5% |
   Communicate bwd 1:                  0.056     0.056   0.4% |
   Communicate fwd 0:                  0.057     0.057   0.5% |
   Communicate fwd 1:                  0.059     0.059   0.5% |
   fft:                                0.017     0.017   0.1% |
   fft2:                               0.054     0.054   0.4% |
  XC 3D grid:                          0.665     0.665   5.2% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                             9.834     0.001   0.0% |
 Direct Minimisation step:             8.739     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.035     0.001   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.006     0.006   0.0% |
   Potential matrix:                   0.026     0.026   0.2% |
   Residual:                           0.000     0.000   0.0% |
  Get Search Direction:                0.001     0.001   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Preconditioning::                    0.000     0.000   0.0% |
  Unitary rotation:                    0.003     0.000   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.003     0.003   0.0% |
  Update Kohn-Sham energy:             8.694     0.000   0.0% |
   Density:                            0.262     0.000   0.0% |
    Atomic density matrices:           0.010     0.010   0.1% |
    Mix:                               0.219     0.219   1.7% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.001     0.001   0.0% |
    Pseudo density:                    0.031     0.003   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.028     0.028   0.2% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        8.431     0.001   0.0% |
    Atomic:                            0.002     0.002   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.004     0.004   0.0% |
    Communicate:                       0.511     0.511   4.0% |-|
    Hartree integrate/restrict:        0.161     0.161   1.3% ||
    New Kinetic Energy:                0.001     0.000   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           2.415     0.040   0.3% |
     Communicate bwd 0:                0.469     0.469   3.7% ||
     Communicate bwd 1:                0.444     0.444   3.5% ||
     Communicate fwd 0:                0.428     0.428   3.4% ||
     Communicate fwd 1:                0.479     0.479   3.8% |-|
     fft:                              0.133     0.133   1.0% |
     fft2:                             0.422     0.422   3.3% ||
    XC 3D grid:                        5.313     5.313  41.9% |----------------|
    vbar:                              0.024     0.024   0.2% |
 Get canonical representation:         1.093     0.000   0.0% |
  LCAO eigensolver:                    0.004     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.003     0.003   0.0% |
  Update Kohn-Sham energy:             1.089     0.000   0.0% |
   Density:                            0.032     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.027     0.027   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.004     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.003     0.003   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.056     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.064     0.064   0.5% |
    Hartree integrate/restrict:        0.020     0.020   0.2% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.305     0.005   0.0% |
     Communicate bwd 0:                0.058     0.058   0.5% |
     Communicate bwd 1:                0.056     0.056   0.4% |
     Communicate fwd 0:                0.056     0.056   0.4% |
     Communicate fwd 1:                0.060     0.060   0.5% |
     fft:                              0.017     0.017   0.1% |
     fft2:                             0.054     0.054   0.4% |
    XC 3D grid:                        0.663     0.663   5.2% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.098     0.098   0.8% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.649     1.649  13.0% |----|
-------------------------------------------------------------
Total:                                          12.675 100.0%

Memory usage: 239.30 MiB
Date: Sat Feb 16 23:26:14 2019
