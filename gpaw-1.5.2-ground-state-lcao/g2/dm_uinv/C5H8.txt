
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:30:13 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -5238.061875

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*112*116 grid
  Fine grid: 224*224*232 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 64.06 MiB
    Density: 36.61 MiB
      Arrays: 35.06 MiB
      Localized functions: 1.54 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 23.06 MiB
      Arrays: 22.94 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.12 MiB
    Wavefunctions: 4.40 MiB
      C [qnM]: 0.08 MiB
      S, T [2 x qmm]: 0.17 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 4.14 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 13
Number of atomic orbitals: 105
Number of bands in calculation: 105
Bands to converge: occupied states only
Number of valence electrons: 28

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .----------------------------------------.  
          /|                                        |  
         / |                                        |  
        /  |                                        |  
       /   |                                        |  
      /    |                                        |  
     /     |                                        |  
    /      |                                        |  
   /       |                                        |  
  /        |                                        |  
 *         |                                        |  
 |         |                                        |  
 |         |                                        |  
 |         |             H C  H                     |  
 |         |           H  C H                       |  
 |         |               C                        |  
 |         |                                        |  
 |         |            HC  C H                     |  
 |         |           H     H                      |  
 |         |                                        |  
 |         |                                        |  
 |         .----------------------------------------.  
 |        /                                        /   
 |       /                                        /    
 |      /                                        /     
 |     /                                        /      
 |    /                                        /       
 |   /                                        /        
 |  /                                        /         
 | /                                        /          
 |/                                        /           
 *----------------------------------------*            

Positions:
   0 C      8.265075    8.265075    8.568090    ( 0.0000,  0.0000,  0.0000)
   1 C      8.265075    9.027089    9.833842    ( 0.0000,  0.0000,  0.0000)
   2 C      8.265075    7.503061    9.833842    ( 0.0000,  0.0000,  0.0000)
   3 C      9.027089    8.265075    7.302338    ( 0.0000,  0.0000,  0.0000)
   4 C      7.503061    8.265075    7.302338    ( 0.0000,  0.0000,  0.0000)
   5 H      7.351052    9.530150   10.136180    ( 0.0000,  0.0000,  0.0000)
   6 H      9.179098    9.530150   10.136180    ( 0.0000,  0.0000,  0.0000)
   7 H      7.351052    7.000000   10.136180    ( 0.0000,  0.0000,  0.0000)
   8 H      9.179098    7.000000   10.136180    ( 0.0000,  0.0000,  0.0000)
   9 H      9.530150    7.351052    7.000000    ( 0.0000,  0.0000,  0.0000)
  10 H      9.530150    9.179098    7.000000    ( 0.0000,  0.0000,  0.0000)
  11 H      7.000000    7.351052    7.000000    ( 0.0000,  0.0000,  0.0000)
  12 H      7.000000    9.179098    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.530150    0.000000    0.000000   112     0.1476
  2. axis:    no     0.000000   16.530150    0.000000   112     0.1476
  3. axis:    no     0.000000    0.000000   17.136180   116     0.1477

  Lengths:  16.530150  16.530150  17.136180
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1476

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:30:23  +0.34          -70.798442    0      1      
iter:   2  00:30:25  +0.20          -70.871100    0      1      
iter:   3  00:30:27  -1.25          -70.906614    0      1      
iter:   4  00:30:29  -1.46          -70.906969    0      1      
iter:   5  00:30:30  -4.01          -70.907979    0      1      
iter:   6  00:30:32  -4.82          -70.907986    0      1      
iter:   7  00:30:34  -5.82          -70.907989    0      1      
iter:   8  00:30:36  -6.12          -70.907989    0      1      
iter:   9  00:30:37  -7.17          -70.907989    0      1      
iter:  10  00:30:39  -7.86          -70.907989    0      1      
iter:  11  00:30:41  -8.97          -70.907989    0      1      
iter:  12  00:30:45 -10.10          -70.907989    0      1      

Converged after 12 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5238.061875)

Kinetic:        +57.094330
Potential:      -61.364193
External:        +0.000000
XC:             -66.766818
Entropy (-ST):   +0.000000
Local:           +0.128692
--------------------------
Free energy:    -70.907989
Extrapolated:   -70.907989

Fermi levels: -2.30290, -2.30290

 Band  Eigenvalues  Occupancy
    0    -21.99254    2.00000
    1    -19.88214    2.00000
    2    -14.71398    2.00000
    3    -14.71398    2.00000
    4    -13.18967    2.00000
    5    -11.88493    2.00000
    6    -11.42976    2.00000
    7    -11.42976    2.00000
    8     -8.83314    2.00000
    9     -8.49468    2.00000
   10     -8.39443    2.00000
   11     -6.76881    2.00000
   12     -6.76881    2.00000
   13     -6.44582    2.00000
   14      1.84001    0.00000
   15      1.84001    0.00000
   16      2.22661    0.00000
   17      2.61393    0.00000
   18      3.22316    0.00000
   19      3.22316    0.00000
   20      4.49360    0.00000
   21      4.49360    0.00000
   22      4.63969    0.00000
   23      4.93367    0.00000
   24      5.51251    0.00000
   25      6.78250    0.00000
   26      6.78250    0.00000
   27      7.14846    0.00000
   28      8.80303    0.00000
   29      9.00015    0.00000
   30     10.01567    0.00000
   31     10.01567    0.00000
   32     10.06654    0.00000
   33     11.57326    0.00000
   34     11.72382    0.00000
   35     11.97570    0.00000
   36     11.97570    0.00000
   37     12.18733    0.00000
   38     12.99576    0.00000
   39     12.99576    0.00000
   40     15.54792    0.00000
   41     15.60378    0.00000
   42     15.60378    0.00000
   43     15.94272    0.00000
   44     15.94272    0.00000
   45     16.43677    0.00000
   46     16.80903    0.00000
   47     17.53860    0.00000
   48     17.53860    0.00000
   49     19.95789    0.00000
   50     20.91596    0.00000
   51     21.10002    0.00000
   52     23.20879    0.00000
   53     24.30087    0.00000
   54     24.30087    0.00000
   55     24.36487    0.00000
   56     24.79970    0.00000
   57     27.48650    0.00000
   58     29.65868    0.00000
   59     29.83681    0.00000
   60     30.49368    0.00000
   61     31.90912    0.00000
   62     31.90912    0.00000
   63     32.91483    0.00000
   64     33.13661    0.00000
   65     33.13661    0.00000
   66     34.06846    0.00000
   67     34.22158    0.00000
   68     34.22158    0.00000
   69     34.37694    0.00000
   70     34.37694    0.00000
   71     35.37029    0.00000
   72     36.31412    0.00000
   73     36.60078    0.00000
   74     36.67583    0.00000
   75     36.67583    0.00000
   76     37.41025    0.00000
   77     37.41025    0.00000
   78     38.46081    0.00000
   79     41.07028    0.00000
   80     41.23117    0.00000
   81     41.23117    0.00000
   82     42.03683    0.00000
   83     43.16778    0.00000
   84     43.34692    0.00000
   85     44.10619    0.00000
   86     44.93247    0.00000
   87     44.93247    0.00000
   88     46.13719    0.00000
   89     47.31218    0.00000
   90     47.90364    0.00000
   91     49.06728    0.00000
   92     49.06728    0.00000
   93     49.81863    0.00000
   94     49.81863    0.00000
   95     51.89295    0.00000
   96     53.90653    0.00000
   97     55.74514    0.00000
   98     59.11454    0.00000
   99     60.58330    0.00000
  100     60.59237    0.00000
  101     60.59237    0.00000
  102     61.74173    0.00000
  103     61.74173    0.00000
  104     65.51060    0.00000

Gap: 8.286 eV
Transition (v -> c):
  (s=0, k=0, n=13, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=14, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.001     0.001   0.0% |
Basis functions set positions:         0.019     0.019   0.1% |
LCAO WFS Initialize:                   1.717     0.050   0.2% |
 Hamiltonian:                          1.668     0.000   0.0% |
  Atomic:                              0.054     0.007   0.0% |
   XC Correction:                      0.046     0.046   0.1% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.040     0.040   0.1% |
  Hartree integrate/restrict:          0.037     0.037   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.472     0.007   0.0% |
   Communicate bwd 0:                  0.091     0.091   0.3% |
   Communicate bwd 1:                  0.089     0.089   0.3% |
   Communicate fwd 0:                  0.087     0.087   0.3% |
   Communicate fwd 1:                  0.091     0.091   0.3% |
   fft:                                0.044     0.044   0.1% |
   fft2:                               0.064     0.064   0.2% |
  XC 3D grid:                          1.054     1.054   3.4% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.003     0.003   0.0% |
SCF-cycle:                            26.920     0.003   0.0% |
 Direct Minimisation step:            25.130     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.543     0.005   0.0% |
   Construct Gradient Matrix:          0.004     0.004   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.089     0.089   0.3% |
   Potential matrix:                   0.442     0.442   1.4% ||
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.040     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.007     0.007   0.0% |
   Orbital Layouts:                    0.002     0.002   0.0% |
   Potential matrix:                   0.031     0.031   0.1% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.009     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.006     0.006   0.0% |
  Update Kohn-Sham energy:            24.531     0.000   0.0% |
   Density:                            1.177     0.000   0.0% |
    Atomic density matrices:           0.113     0.113   0.4% |
    Mix:                               0.636     0.636   2.0% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.421     0.011   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.409     0.409   1.3% ||
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       23.354     0.001   0.0% |
    Atomic:                            0.756     0.105   0.3% |
     XC Correction:                    0.651     0.651   2.1% ||
    Calculate atomic Hamiltonians:     0.026     0.026   0.1% |
    Communicate:                       0.558     0.558   1.8% ||
    Hartree integrate/restrict:        0.458     0.458   1.5% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.712     0.108   0.3% |
     Communicate bwd 0:                1.331     1.331   4.3% |-|
     Communicate bwd 1:                1.258     1.258   4.0% |-|
     Communicate fwd 0:                1.199     1.199   3.8% |-|
     Communicate fwd 1:                1.293     1.293   4.1% |-|
     fft:                              0.631     0.631   2.0% ||
     fft2:                             0.891     0.891   2.9% ||
    XC 3D grid:                       14.777    14.777  47.3% |------------------|
    vbar:                              0.063     0.063   0.2% |
 Get canonical representation:         1.788     0.001   0.0% |
  LCAO eigensolver:                    0.040     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.002     0.002   0.0% |
   Potential matrix:                   0.033     0.033   0.1% |
  Update Kohn-Sham energy:             1.747     0.000   0.0% |
   Density:                            0.085     0.000   0.0% |
    Atomic density matrices:           0.009     0.009   0.0% |
    Mix:                               0.046     0.046   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.029     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.028     0.028   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.663     0.000   0.0% |
    Atomic:                            0.054     0.008   0.0% |
     XC Correction:                    0.047     0.047   0.1% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.040     0.040   0.1% |
    Hartree integrate/restrict:        0.035     0.035   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.474     0.007   0.0% |
     Communicate bwd 0:                0.093     0.093   0.3% |
     Communicate bwd 1:                0.088     0.088   0.3% |
     Communicate fwd 0:                0.085     0.085   0.3% |
     Communicate fwd 1:                0.091     0.091   0.3% |
     fft:                              0.046     0.046   0.1% |
     fft2:                             0.063     0.063   0.2% |
    XC 3D grid:                        1.053     1.053   3.4% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.004     0.004   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.201     0.201   0.6% |
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.375     2.375   7.6% |--|
-------------------------------------------------------------
Total:                                          31.244 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:30:45 2019
