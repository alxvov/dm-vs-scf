
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:30:50 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -38704.966465

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*112*104 grid
  Fine grid: 224*224*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 55.29 MiB
    Density: 32.70 MiB
      Arrays: 31.40 MiB
      Localized functions: 1.30 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 20.64 MiB
      Arrays: 20.54 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.10 MiB
    Wavefunctions: 1.95 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.88 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 57
Number of bands in calculation: 57
Bands to converge: occupied states only
Number of valence electrons: 26

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-----------------------------------------.  
          /|                                         |  
         / |                                         |  
        /  |                                         |  
       /   |                                         |  
      /    |                                         |  
     /     |                                         |  
    /      |                                         |  
   /       |                                         |  
  /        |                                         |  
 *         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |               H                         |  
 |         |               CCl                       |  
 |         |           Cl     Cl                     |  
 |         |                                         |  
 |         |                                         |  
 |         .-----------------------------------------.  
 |        /                                         /   
 |       /                                         /    
 |      /                                         /     
 |     /                                         /      
 |    /                                         /       
 |   /                                         /        
 |  /                                         /         
 | /                                         /          
 |/                                         /           
 *-----------------------------------------*            

Positions:
   0 C      8.456415    7.840862    7.534966    ( 0.0000,  0.0000,  0.0000)
   1 H      8.456415    7.840862    8.620873    ( 0.0000,  0.0000,  0.0000)
   2 Cl     8.456415    9.522585    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 Cl     9.912830    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 Cl     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.912830    0.000000    0.000000   112     0.1510
  2. axis:    no     0.000000   16.522585    0.000000   112     0.1475
  3. axis:    no     0.000000    0.000000   15.620873   104     0.1502

  Lengths:  16.912830  16.522585  15.620873
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1496

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:30:58  -0.17          -17.383543    0      1      
iter:   2  23:31:01  -1.30          -17.399137    0      1      
iter:   3  23:31:03  -2.04          -17.400413    0      1      
iter:   4  23:31:05  -3.46          -17.400686    0      1      
iter:   5  23:31:06  -4.31          -17.400705    0      1      
iter:   6  23:31:08  -5.10          -17.400710    0      1      
iter:   7  23:31:09  -5.35          -17.400710    0      1      
iter:   8  23:31:11  -5.48          -17.400710    0      1      
iter:   9  23:31:12  -6.64          -17.400710    0      1      
iter:  10  23:31:14  -7.89          -17.400710    0      1      
iter:  11  23:31:15  -9.14          -17.400710    0      1      
iter:  12  23:31:17  -9.75          -17.400710    0      1      
iter:  13  23:31:20 -10.13          -17.400710    0      1      

Converged after 13 iterations.

Dipole moment: (-0.000000, 0.000020, 0.182031) |e|*Ang

Energy contributions relative to reference atoms: (reference = -38704.966465)

Kinetic:        +31.876055
Potential:      -28.593758
External:        +0.000000
XC:             -20.735923
Entropy (-ST):   +0.000000
Local:           +0.052916
--------------------------
Free energy:    -17.400710
Extrapolated:   -17.400710

Fermi levels: -4.08913, -4.08913

 Band  Eigenvalues  Occupancy
    0    -23.42829    2.00000
    1    -20.70962    2.00000
    2    -20.70940    2.00000
    3    -15.82371    2.00000
    4    -12.29236    2.00000
    5    -11.54846    2.00000
    6    -11.54837    2.00000
    7     -8.15794    2.00000
    8     -8.15789    2.00000
    9     -7.40937    2.00000
   10     -7.40926    2.00000
   11     -7.30999    2.00000
   12     -6.91307    2.00000
   13     -1.26518    0.00000
   14     -0.00702    0.00000
   15     -0.00701    0.00000
   16      1.95101    0.00000
   17      7.79701    0.00000
   18      7.79712    0.00000
   19      8.43595    0.00000
   20     11.12586    0.00000
   21     11.12631    0.00000
   22     12.06927    0.00000
   23     12.06934    0.00000
   24     12.07738    0.00000
   25     12.86482    0.00000
   26     12.87745    0.00000
   27     13.18477    0.00000
   28     13.18480    0.00000
   29     13.27075    0.00000
   30     13.90335    0.00000
   31     13.90346    0.00000
   32     15.04184    0.00000
   33     15.09893    0.00000
   34     15.09915    0.00000
   35     15.28204    0.00000
   36     16.02127    0.00000
   37     16.02151    0.00000
   38     18.28887    0.00000
   39     18.84641    0.00000
   40     18.84714    0.00000
   41     19.86214    0.00000
   42     20.80687    0.00000
   43     20.80692    0.00000
   44     22.36771    0.00000
   45     26.71698    0.00000
   46     28.73540    0.00000
   47     28.73573    0.00000
   48     32.94663    0.00000
   49     32.94721    0.00000
   50     33.61912    0.00000
   51     38.26368    0.00000
   52     39.15893    0.00000
   53     39.15923    0.00000
   54     40.16550    0.00000
   55     40.16583    0.00000
   56     49.52248    0.00000

Gap: 5.648 eV
Transition (v -> c):
  (s=0, k=0, n=12, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=13, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.009     0.009   0.0% |
LCAO WFS Initialize:                   1.505     0.041   0.1% |
 Hamiltonian:                          1.463     0.000   0.0% |
  Atomic:                              0.062     0.062   0.2% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.001     0.001   0.0% |
  Hartree integrate/restrict:          0.029     0.029   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.420     0.011   0.0% |
   Communicate bwd 0:                  0.080     0.080   0.3% |
   Communicate bwd 1:                  0.077     0.077   0.3% |
   Communicate fwd 0:                  0.077     0.077   0.3% |
   Communicate fwd 1:                  0.086     0.086   0.3% |
   fft:                                0.027     0.027   0.1% |
   fft2:                               0.062     0.062   0.2% |
  XC 3D grid:                          0.944     0.944   3.1% ||
  vbar:                                0.004     0.004   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            25.920     0.003   0.0% |
 Direct Minimisation step:            24.387     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.210     0.003   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.020     0.020   0.1% |
   Potential matrix:                   0.181     0.181   0.6% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.013     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.011     0.011   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.008     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.007     0.007   0.0% |
  Update Kohn-Sham energy:            24.150     0.000   0.0% |
   Density:                            0.860     0.000   0.0% |
    Atomic density matrices:           0.025     0.025   0.1% |
    Mix:                               0.635     0.635   2.1% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.194     0.010   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.183     0.183   0.6% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       23.289     0.002   0.0% |
    Atomic:                            0.999     0.999   3.3% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.034     0.034   0.1% |
    Communicate:                       0.012     0.012   0.0% |
    Hartree integrate/restrict:        0.420     0.420   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           6.734     0.199   0.7% |
     Communicate bwd 0:                1.299     1.299   4.3% |-|
     Communicate bwd 1:                1.265     1.265   4.2% |-|
     Communicate fwd 0:                1.223     1.223   4.1% |-|
     Communicate fwd 1:                1.328     1.328   4.4% |-|
     fft:                              0.429     0.429   1.4% ||
     fft2:                             0.991     0.991   3.3% ||
    XC 3D grid:                       15.023    15.023  49.8% |-------------------|
    vbar:                              0.064     0.064   0.2% |
 Get canonical representation:         1.530     0.000   0.0% |
  LCAO eigensolver:                    0.013     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.011     0.011   0.0% |
  Update Kohn-Sham energy:             1.516     0.000   0.0% |
   Density:                            0.054     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.040     0.040   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.012     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.011     0.011   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.462     0.000   0.0% |
    Atomic:                            0.063     0.063   0.2% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.000     0.000   0.0% |
    Hartree integrate/restrict:        0.026     0.026   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.424     0.013   0.0% |
     Communicate bwd 0:                0.082     0.082   0.3% |
     Communicate bwd 1:                0.080     0.080   0.3% |
     Communicate fwd 0:                0.076     0.076   0.3% |
     Communicate fwd 1:                0.084     0.084   0.3% |
     fft:                              0.026     0.026   0.1% |
     fft2:                             0.061     0.061   0.2% |
    XC 3D grid:                        0.942     0.942   3.1% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.538     0.538   1.8% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.159     2.159   7.2% |--|
-------------------------------------------------------------
Total:                                          30.137 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:31:20 2019
