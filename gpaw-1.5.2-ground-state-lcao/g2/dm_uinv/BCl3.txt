
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:05:04 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

B-setup:
  name: Boron
  id: 6e91439235a05fd7549e31636e8f777c
  Z: 5
  valence: 3
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/B.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.38(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)    -9.443   0.635
    2p(1.00)    -3.606   0.635
    *s          17.768   0.635
    *p          23.605   0.635
    *d           0.000   0.635

  LCAO basis set for B:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/B.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.6719 Bohr: 2s-sz confined orbital
      l=1, rc=8.2188 Bohr: 2p-sz confined orbital
      l=0, rc=3.7500 Bohr: 2s-dz split-valence wave
      l=1, rc=4.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=8.2188 Bohr: d-type Gaussian polarization

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -38334.337754

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*112*92 grid
  Fine grid: 224*224*184 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*224*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 49.28 MiB
    Density: 28.93 MiB
      Arrays: 27.74 MiB
      Localized functions: 1.19 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 18.24 MiB
      Arrays: 18.15 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 2.11 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.05 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 52
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-----------------------------------------.  
          /|                                         |  
         / |                                         |  
        /  |                                         |  
       /   |                                         |  
      /    |                                         |  
     /     |                                         |  
    /      |                                         |  
   /       |                                         |  
  /        |                                         |  
 *         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |                Cl                       |  
 |         |           Cl  B  Cl                     |  
 |         |                                         |  
 |         |                                         |  
 |         .-----------------------------------------.  
 |        /                                         /   
 |       /                                         /    
 |      /                                         /     
 |     /                                         /      
 |    /                                         /       
 |   /                                         /        
 |  /                                         /         
 | /                                         /          
 |/                                         /           
 *-----------------------------------------*            

Positions:
   0 B      8.502859    7.867676    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 Cl     8.502859    9.603028    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 Cl    10.005718    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 Cl     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.005718    0.000000    0.000000   112     0.1518
  2. axis:    no     0.000000   16.603028    0.000000   112     0.1482
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  17.005718  16.603028  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1507

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:05:12  -0.78          -14.561958    0      1      
iter:   2  00:05:14  -2.24          -14.566234    0      1      
iter:   3  00:05:16  -3.48          -14.566432    0      1      
iter:   4  00:05:17  -4.76          -14.566448    0      1      
iter:   5  00:05:18  -5.93          -14.566449    0      1      
iter:   6  00:05:20  -6.69          -14.566449    0      1      
iter:   7  00:05:21  -6.86          -14.566449    0      1      
iter:   8  00:05:22  -8.51          -14.566449    0      1      
iter:   9  00:05:24  -9.53          -14.566449    0      1      
iter:  10  00:05:26 -11.42          -14.566449    0      1      

Converged after 10 iterations.

Dipole moment: (0.000000, 0.000049, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -38334.337754)

Kinetic:        +26.185747
Potential:      -24.060954
External:        +0.000000
XC:             -16.742452
Entropy (-ST):   +0.000000
Local:           +0.051210
--------------------------
Free energy:    -14.566449
Extrapolated:   -14.566449

Fermi levels: -4.62071, -4.62071

 Band  Eigenvalues  Occupancy
    0    -22.05864    2.00000
    1    -20.79780    2.00000
    2    -20.79773    2.00000
    3    -12.77068    2.00000
    4    -10.88867    2.00000
    5    -10.88865    2.00000
    6     -9.70067    2.00000
    7     -8.06183    2.00000
    8     -8.06176    2.00000
    9     -7.97142    2.00000
   10     -7.97137    2.00000
   11     -7.15720    2.00000
   12     -2.08423    0.00000
   13      1.12188    0.00000
   14      2.87135    0.00000
   15      2.87191    0.00000
   16      5.65388    0.00000
   17      5.65404    0.00000
   18      6.56341    0.00000
   19      7.06082    0.00000
   20     10.77403    0.00000
   21     10.77417    0.00000
   22     10.82110    0.00000
   23     10.82186    0.00000
   24     12.78903    0.00000
   25     13.11945    0.00000
   26     13.74964    0.00000
   27     13.74968    0.00000
   28     13.97318    0.00000
   29     13.97364    0.00000
   30     14.02301    0.00000
   31     14.88955    0.00000
   32     15.00457    0.00000
   33     15.00462    0.00000
   34     15.09056    0.00000
   35     15.64085    0.00000
   36     15.64089    0.00000
   37     19.18258    0.00000
   38     19.56675    0.00000
   39     19.56783    0.00000
   40     20.30605    0.00000
   41     22.30721    0.00000
   42     22.47945    0.00000
   43     22.47978    0.00000
   44     25.06950    0.00000
   45     28.57982    0.00000
   46     28.57998    0.00000
   47     34.38179    0.00000
   48     34.38339    0.00000
   49     38.14813    0.00000
   50     38.14876    0.00000
   51     38.83335    0.00000

Gap: 5.073 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.008     0.008   0.0% |
LCAO WFS Initialize:                   1.347     0.038   0.2% |
 Hamiltonian:                          1.309     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.063     0.063   0.3% |
  Hartree integrate/restrict:          0.025     0.025   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.377     0.011   0.1% |
   Communicate bwd 0:                  0.070     0.070   0.3% |
   Communicate bwd 1:                  0.068     0.068   0.3% |
   Communicate fwd 0:                  0.067     0.067   0.3% |
   Communicate fwd 1:                  0.075     0.075   0.3% |
   fft:                                0.024     0.024   0.1% |
   fft2:                               0.061     0.061   0.3% |
  XC 3D grid:                          0.835     0.835   3.7% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            18.965     0.002   0.0% |
 Direct Minimisation step:            17.609     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.130     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.015     0.015   0.1% |
   Potential matrix:                   0.109     0.109   0.5% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.010     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.008     0.008   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.006     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.005     0.005   0.0% |
  Update Kohn-Sham energy:            17.458     0.000   0.0% |
   Density:                            0.615     0.000   0.0% |
    Atomic density matrices:           0.027     0.027   0.1% |
    Mix:                               0.465     0.465   2.1% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.118     0.007   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.110     0.110   0.5% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       16.843     0.001   0.0% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.009     0.009   0.0% |
    Communicate:                       0.828     0.828   3.7% ||
    Hartree integrate/restrict:        0.323     0.323   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           4.816     0.127   0.6% |
     Communicate bwd 0:                0.911     0.911   4.0% |-|
     Communicate bwd 1:                0.894     0.894   4.0% |-|
     Communicate fwd 0:                0.858     0.858   3.8% |-|
     Communicate fwd 1:                0.927     0.927   4.1% |-|
     fft:                              0.304     0.304   1.3% ||
     fft2:                             0.794     0.794   3.5% ||
    XC 3D grid:                       10.814    10.814  47.9% |------------------|
    vbar:                              0.046     0.046   0.2% |
 Get canonical representation:         1.354     0.000   0.0% |
  LCAO eigensolver:                    0.010     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.008     0.008   0.0% |
  Update Kohn-Sham energy:             1.344     0.000   0.0% |
   Density:                            0.048     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.037     0.037   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.009     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.009     0.009   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.296     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.063     0.063   0.3% |
    Hartree integrate/restrict:        0.025     0.025   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.368     0.009   0.0% |
     Communicate bwd 0:                0.070     0.070   0.3% |
     Communicate bwd 1:                0.069     0.069   0.3% |
     Communicate fwd 0:                0.065     0.065   0.3% |
     Communicate fwd 1:                0.071     0.071   0.3% |
     fft:                              0.023     0.023   0.1% |
     fft2:                             0.061     0.061   0.3% |
    XC 3D grid:                        0.834     0.834   3.7% ||
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.349     0.349   1.5% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.924     1.924   8.5% |--|
-------------------------------------------------------------
Total:                                          22.596 100.0%

Memory usage: 416.48 MiB
Date: Sun Feb 17 00:05:26 2019
