
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:42:52 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -4171.049506

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*108*104 grid
  Fine grid: 240*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 57.20 MiB
    Density: 33.45 MiB
      Arrays: 32.45 MiB
      Localized functions: 1.00 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 21.30 MiB
      Arrays: 21.23 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.08 MiB
    Wavefunctions: 2.44 MiB
      C [qnM]: 0.04 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.33 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 9
Number of atomic orbitals: 69
Number of bands in calculation: 69
Bands to converge: occupied states only
Number of valence electrons: 20

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-------------------------------------------.  
          /|                                           |  
         / |                                           |  
        /  |                                           |  
       /   |                                           |  
      /    |                                           |  
     /     |                                           |  
    /      |                                           |  
   /       |                                           |  
  /        |                                           |  
 *         |                                           |  
 |         |                                           |  
 |         |                                           |  
 |         |                 H                         |  
 |         |           H    C H  H                     |  
 |         |             O   HC                        |  
 |         |                  H                        |  
 |         |                                           |  
 |         |                                           |  
 |         .-------------------------------------------.  
 |        /                                           /   
 |       /                                           /    
 |      /                                           /     
 |     /                                           /      
 |    /                                           /       
 |   /                                           /        
 |  /                                           /         
 | /                                           /          
 |/                                           /           
 *-------------------------------------------*            

Positions:
   0 C     10.114804    7.636852    7.886933    ( 0.0000,  0.0000,  0.0000)
   1 C      8.946623    8.596696    7.886933    ( 0.0000,  0.0000,  0.0000)
   2 O      7.756540    7.809565    7.886933    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    8.418759    7.886933    ( 0.0000,  0.0000,  0.0000)
   4 H      8.989180    9.244742    8.773866    ( 0.0000,  0.0000,  0.0000)
   5 H      8.989180    9.244742    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H     11.062514    8.182034    7.886933    ( 0.0000,  0.0000,  0.0000)
   7 H     10.075222    7.000000    8.772814    ( 0.0000,  0.0000,  0.0000)
   8 H     10.075222    7.000000    7.001052    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    18.062514    0.000000    0.000000   120     0.1505
  2. axis:    no     0.000000   16.244742    0.000000   108     0.1504
  3. axis:    no     0.000000    0.000000   15.773866   104     0.1517

  Lengths:  18.062514  16.244742  15.773866
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:43:00  +0.69          -45.271963    0      1      
iter:   2  23:43:03  -0.17          -45.359872    0      1      
iter:   3  23:43:05  -0.63          -45.370234    0      1      
iter:   4  23:43:06  -2.36          -45.373150    0      1      
iter:   5  23:43:07  -3.25          -45.373245    0      1      
iter:   6  23:43:09  -4.06          -45.373261    0      1      
iter:   7  23:43:10  -4.78          -45.373264    0      1      
iter:   8  23:43:12  -5.55          -45.373264    0      1      
iter:   9  23:43:13  -6.23          -45.373264    0      1      
iter:  10  23:43:15  -6.85          -45.373264    0      1      
iter:  11  23:43:16  -7.99          -45.373264    0      1      
iter:  12  23:43:17  -8.60          -45.373264    0      1      
iter:  13  23:43:19  -9.68          -45.373264    0      1      
iter:  14  23:43:22 -10.15          -45.373264    0      1      

Converged after 14 iterations.

Dipole moment: (-0.010487, 0.282371, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -4171.049506)

Kinetic:        +39.419786
Potential:      -42.823305
External:        +0.000000
XC:             -42.090828
Entropy (-ST):   +0.000000
Local:           +0.121083
--------------------------
Free energy:    -45.373264
Extrapolated:   -45.373264

Fermi levels: -2.18283, -2.18283

 Band  Eigenvalues  Occupancy
    0    -25.07037    2.00000
    1    -18.44629    2.00000
    2    -14.85112    2.00000
    3    -12.48843    2.00000
    4    -11.17002    2.00000
    5     -9.52467    2.00000
    6     -9.13945    2.00000
    7     -8.58661    2.00000
    8     -7.61666    2.00000
    9     -5.75773    2.00000
   10      1.39207    0.00000
   11      2.37791    0.00000
   12      3.31135    0.00000
   13      3.38260    0.00000
   14      3.91203    0.00000
   15      4.71038    0.00000
   16      4.83248    0.00000
   17      5.70678    0.00000
   18      8.84411    0.00000
   19      8.93279    0.00000
   20     10.02615    0.00000
   21     11.36860    0.00000
   22     11.63275    0.00000
   23     12.50129    0.00000
   24     14.13614    0.00000
   25     14.80185    0.00000
   26     15.31980    0.00000
   27     15.88639    0.00000
   28     16.09335    0.00000
   29     16.41389    0.00000
   30     17.71067    0.00000
   31     17.76607    0.00000
   32     20.79087    0.00000
   33     21.08217    0.00000
   34     22.28089    0.00000
   35     24.88124    0.00000
   36     25.38655    0.00000
   37     25.69393    0.00000
   38     26.53132    0.00000
   39     28.40995    0.00000
   40     29.88419    0.00000
   41     32.74195    0.00000
   42     33.37163    0.00000
   43     34.71243    0.00000
   44     34.96932    0.00000
   45     35.82434    0.00000
   46     36.05101    0.00000
   47     36.55152    0.00000
   48     37.10504    0.00000
   49     37.27783    0.00000
   50     39.22678    0.00000
   51     41.18564    0.00000
   52     41.54732    0.00000
   53     43.53105    0.00000
   54     44.85325    0.00000
   55     45.67707    0.00000
   56     46.91972    0.00000
   57     46.98409    0.00000
   58     50.33702    0.00000
   59     51.28968    0.00000
   60     52.28990    0.00000
   61     52.96292    0.00000
   62     53.36626    0.00000
   63     55.46723    0.00000
   64     58.76573    0.00000
   65     62.35143    0.00000
   66     62.53469    0.00000
   67     64.56133    0.00000
   68     74.05407    0.00000

Gap: 7.150 eV
Transition (v -> c):
  (s=0, k=0, n=9, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=10, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.012     0.012   0.0% |
LCAO WFS Initialize:                   1.413     0.043   0.1% |
 Hamiltonian:                          1.370     0.000   0.0% |
  Atomic:                              0.047     0.001   0.0% |
   XC Correction:                      0.046     0.046   0.2% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.007     0.007   0.0% |
  Hartree integrate/restrict:          0.028     0.028   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.300     0.016   0.1% |
   Communicate bwd 0:                  0.021     0.021   0.1% |
   Communicate bwd 1:                  0.080     0.080   0.3% |
   Communicate fwd 0:                  0.077     0.077   0.3% |
   Communicate fwd 1:                  0.026     0.026   0.1% |
   fft:                                0.027     0.027   0.1% |
   fft2:                               0.053     0.053   0.2% |
  XC 3D grid:                          0.976     0.976   3.2% ||
  vbar:                                0.005     0.005   0.0% |
P tci:                                 0.003     0.003   0.0% |
SCF-cycle:                            25.940     0.003   0.0% |
 Direct Minimisation step:            24.496     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.291     0.004   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.079     0.079   0.3% |
   Potential matrix:                   0.203     0.203   0.7% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.003     0.003   0.0% |
  LCAO eigensolver:                    0.018     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.005     0.005   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.008     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.006     0.006   0.0% |
  Update Kohn-Sham energy:            24.173     0.000   0.0% |
   Density:                            0.997     0.000   0.0% |
    Atomic density matrices:           0.092     0.092   0.3% |
    Mix:                               0.687     0.687   2.3% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.004     0.004   0.0% |
    Pseudo density:                    0.211     0.012   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.197     0.197   0.7% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       23.176     0.002   0.0% |
    Atomic:                            0.803     0.017   0.1% |
     XC Correction:                    0.786     0.786   2.6% ||
    Calculate atomic Hamiltonians:     0.041     0.041   0.1% |
    Communicate:                       0.111     0.111   0.4% |
    Hartree integrate/restrict:        0.433     0.433   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.123     0.279   0.9% |
     Communicate bwd 0:                0.391     0.391   1.3% ||
     Communicate bwd 1:                1.384     1.384   4.6% |-|
     Communicate fwd 0:                1.305     1.305   4.3% |-|
     Communicate fwd 1:                0.403     0.403   1.3% ||
     fft:                              0.461     0.461   1.5% ||
     fft2:                             0.900     0.900   3.0% ||
    XC 3D grid:                       16.588    16.588  55.2% |---------------------|
    vbar:                              0.072     0.072   0.2% |
 Get canonical representation:         1.440     0.000   0.0% |
  LCAO eigensolver:                    0.018     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.005     0.005   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.012     0.012   0.0% |
  Update Kohn-Sham energy:             1.422     0.000   0.0% |
   Density:                            0.058     0.000   0.0% |
    Atomic density matrices:           0.006     0.006   0.0% |
    Mix:                               0.040     0.040   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.012     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.011     0.011   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.364     0.000   0.0% |
    Atomic:                            0.048     0.002   0.0% |
     XC Correction:                    0.046     0.046   0.2% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.006     0.006   0.0% |
    Hartree integrate/restrict:        0.027     0.027   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.301     0.016   0.1% |
     Communicate bwd 0:                0.022     0.022   0.1% |
     Communicate bwd 1:                0.081     0.081   0.3% |
     Communicate fwd 0:                0.077     0.077   0.3% |
     Communicate fwd 1:                0.024     0.024   0.1% |
     fft:                              0.027     0.027   0.1% |
     fft2:                             0.053     0.053   0.2% |
    XC 3D grid:                        0.976     0.976   3.2% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.531     0.531   1.8% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.154     2.154   7.2% |--|
-------------------------------------------------------------
Total:                                          30.058 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:43:22 2019
