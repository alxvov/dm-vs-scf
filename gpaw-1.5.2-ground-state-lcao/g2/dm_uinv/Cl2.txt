
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:35:50 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Cl-setup:
  name: Chlorine
  id: 726897f06f34e53cf8e33b5885a02604
  Z: 17
  valence: 7
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.PBE.gz
  cutoffs: 0.79(comp), 1.40(filt), 1.49(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -20.689   0.794
    3p(5.00)    -8.594   0.794
    *s           6.523   0.794
    *p          18.617   0.794
    *d           0.000   0.794

  LCAO basis set for Cl:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Cl.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.1719 Bohr: 3s-sz confined orbital
      l=1, rc=6.2656 Bohr: 3p-sz confined orbital
      l=0, rc=2.8281 Bohr: 3s-dz split-valence wave
      l=1, rc=3.5156 Bohr: 3p-dz split-valence wave
      l=2, rc=6.2656 Bohr: d-type Gaussian polarization

Reference energy: -25109.898791

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*108 grid
  Fine grid: 184*184*216 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 37.78 MiB
    Density: 22.60 MiB
      Arrays: 21.92 MiB
      Localized functions: 0.68 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 14.39 MiB
      Arrays: 14.34 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 0.79 MiB
      C [qnM]: 0.01 MiB
      S, T [2 x qmm]: 0.01 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.78 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 26
Number of bands in calculation: 26
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            Cl                   |  
 |        |                                 |  
 |        |                                 |  
 |        |            Cl                   |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 Cl     7.000000    7.000000    9.015082    ( 0.0000,  0.0000,  0.0000)
   1 Cl     7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   16.015082   108     0.1483

  Lengths:  14.000000  14.000000  16.015082
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1509

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:35:56  +0.21           -2.523552    0      1      
iter:   2  00:35:57  -0.16           -2.529784    0      1      
iter:   3  00:35:58  -3.13           -2.534636    0      1      
iter:   4  00:35:59  -3.99           -2.534649    0      1      
iter:   5  00:36:00  -6.47           -2.534651    0      1      
iter:   6  00:36:01  -7.69           -2.534651    0      1      
iter:   7  00:36:04 -10.08           -2.534651    0      1      

Converged after 7 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -25109.898791)

Kinetic:        +12.603197
Potential:       -9.632854
External:        +0.000000
XC:              -5.525328
Entropy (-ST):   +0.000000
Local:           +0.020335
--------------------------
Free energy:     -2.534651
Extrapolated:    -2.534651

Fermi levels: -5.32599, -5.32599

 Band  Eigenvalues  Occupancy
    0    -22.95837    2.00000
    1    -18.78823    2.00000
    2    -11.45429    2.00000
    3     -9.33231    2.00000
    4     -9.33231    2.00000
    5     -6.77043    2.00000
    6     -6.77043    2.00000
    7     -3.88154    0.00000
    8      8.97923    0.00000
    9     10.81358    0.00000
   10     10.81358    0.00000
   11     12.46285    0.00000
   12     13.43618    0.00000
   13     13.43618    0.00000
   14     13.71749    0.00000
   15     13.71749    0.00000
   16     14.21733    0.00000
   17     14.21827    0.00000
   18     15.68871    0.00000
   19     15.68973    0.00000
   20     16.77708    0.00000
   21     18.91006    0.00000
   22     20.30658    0.00000
   23     20.30658    0.00000
   24     20.48463    0.00000
   25     34.17073    0.00000

Gap: 2.889 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.003     0.003   0.0% |
LCAO WFS Initialize:                   1.071     0.027   0.2% |
 Hamiltonian:                          1.044     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.063     0.063   0.5% |
  Hartree integrate/restrict:          0.021     0.021   0.2% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.297     0.004   0.0% |
   Communicate bwd 0:                  0.057     0.057   0.4% |
   Communicate bwd 1:                  0.055     0.055   0.4% |
   Communicate fwd 0:                  0.053     0.053   0.4% |
   Communicate fwd 1:                  0.058     0.058   0.4% |
   fft:                                0.016     0.016   0.1% |
   fft2:                               0.053     0.053   0.4% |
  XC 3D grid:                          0.659     0.659   4.9% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            10.708     0.002   0.0% |
 Direct Minimisation step:             9.638     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.023     0.001   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.005     0.005   0.0% |
   Potential matrix:                   0.016     0.016   0.1% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.001     0.001   0.0% |
  LCAO eigensolver:                    0.003     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Preconditioning::                    0.000     0.000   0.0% |
  Unitary rotation:                    0.003     0.000   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.003     0.003   0.0% |
  Update Kohn-Sham energy:             9.606     0.000   0.0% |
   Density:                            0.266     0.000   0.0% |
    Atomic density matrices:           0.008     0.008   0.1% |
    Mix:                               0.235     0.235   1.7% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.021     0.004   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.017     0.017   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        9.339     0.001   0.0% |
    Atomic:                            0.015     0.015   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.004     0.004   0.0% |
    Communicate:                       0.555     0.555   4.1% |-|
    Hartree integrate/restrict:        0.186     0.186   1.4% ||
    New Kinetic Energy:                0.001     0.000   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           2.679     0.041   0.3% |
     Communicate bwd 0:                0.522     0.522   3.9% |-|
     Communicate bwd 1:                0.500     0.500   3.7% ||
     Communicate fwd 0:                0.476     0.476   3.5% ||
     Communicate fwd 1:                0.521     0.521   3.9% |-|
     fft:                              0.148     0.148   1.1% |
     fft2:                             0.473     0.473   3.5% ||
    XC 3D grid:                        5.871     5.871  43.4% |----------------|
    vbar:                              0.027     0.027   0.2% |
 Get canonical representation:         1.068     0.000   0.0% |
  LCAO eigensolver:                    0.003     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.002     0.002   0.0% |
  Update Kohn-Sham energy:             1.065     0.000   0.0% |
   Density:                            0.030     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.026     0.026   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.002     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.002     0.002   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.036     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.063     0.063   0.5% |
    Hartree integrate/restrict:        0.021     0.021   0.2% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.298     0.005   0.0% |
     Communicate bwd 0:                0.058     0.058   0.4% |
     Communicate bwd 1:                0.055     0.055   0.4% |
     Communicate fwd 0:                0.053     0.053   0.4% |
     Communicate fwd 1:                0.058     0.058   0.4% |
     fft:                              0.016     0.016   0.1% |
     fft2:                             0.053     0.053   0.4% |
    XC 3D grid:                        0.650     0.650   4.8% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.099     0.099   0.7% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.630     1.630  12.1% |----|
-------------------------------------------------------------
Total:                                          13.515 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:36:04 2019
