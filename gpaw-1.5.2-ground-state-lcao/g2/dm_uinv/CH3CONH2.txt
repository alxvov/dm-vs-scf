
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:54:58 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -5640.280848

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 120*116*104 grid
  Fine grid: 240*232*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 240*232*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 61.57 MiB
    Density: 35.98 MiB
      Arrays: 34.88 MiB
      Localized functions: 1.10 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 22.90 MiB
      Arrays: 22.82 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.09 MiB
    Wavefunctions: 2.69 MiB
      C [qnM]: 0.05 MiB
      S, T [2 x qmm]: 0.09 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 2.55 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 9
Number of atomic orbitals: 77
Number of bands in calculation: 77
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
            .-------------------------------------------.  
           /|                                           |  
          / |                                           |  
         /  |                                           |  
        /   |                                           |  
       /    |                                           |  
      /     |                                           |  
     /      |                                           |  
    /       |                                           |  
   /        |                                           |  
  /         |                                           |  
 *          |                                           |  
 |          |                                           |  
 |          |                                           |  
 |          |           H      O                        |  
 |          |           HC   C   H                      |  
 |          |                H N                        |  
 |          |           H                               |  
 |          |                                           |  
 |          .-------------------------------------------.  
 |         /                                           /   
 |        /                                           /    
 |       /                                           /     
 |      /                                           /      
 |     /                                           /       
 |    /                                           /        
 |   /                                           /         
 |  /                                           /          
 | /                                           /           
 |/                                           /            
 *-------------------------------------------*             

Positions:
   0 O      9.421775   10.151273    7.779720    ( 0.0000,  0.0000,  0.0000)
   1 C      9.074387    8.974038    7.767437    ( 0.0000,  0.0000,  0.0000)
   2 N      9.982747    7.945712    7.722776    ( 0.0000,  0.0000,  0.0000)
   3 C      7.625754    8.535584    7.771542    ( 0.0000,  0.0000,  0.0000)
   4 H      9.705181    7.000000    7.941628    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    9.409171    7.596209    ( 0.0000,  0.0000,  0.0000)
   6 H      7.436387    7.784979    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H      7.365116    8.101242    8.741500    ( 0.0000,  0.0000,  0.0000)
   8 H     10.950362    8.192675    7.883552    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.950362    0.000000    0.000000   120     0.1496
  2. axis:    no     0.000000   17.151273    0.000000   116     0.1479
  3. axis:    no     0.000000    0.000000   15.741500   104     0.1514

  Lengths:  17.950362  17.151273  15.741500
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1496

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:55:08  +0.85          -49.995865    0      1      
iter:   2  23:55:11  -0.22          -50.135679    0      1      
iter:   3  23:55:12  -1.07          -50.157420    0      1      
iter:   4  23:55:14  -1.56          -50.160168    0      1      
iter:   5  23:55:15  -2.28          -50.160788    0      1      
iter:   6  23:55:17  -2.96          -50.160906    0      1      
iter:   7  23:55:19  -3.76          -50.160941    0      1      
iter:   8  23:55:20  -4.36          -50.160947    0      1      
iter:   9  23:55:22  -4.96          -50.160949    0      1      
iter:  10  23:55:23  -5.67          -50.160950    0      1      
iter:  11  23:55:25  -6.01          -50.160950    0      1      
iter:  12  23:55:26  -6.61          -50.160950    0      1      
iter:  13  23:55:28  -7.31          -50.160950    0      1      
iter:  14  23:55:29  -8.11          -50.160950    0      1      
iter:  15  23:55:31  -8.95          -50.160950    0      1      
iter:  16  23:55:33  -9.32          -50.160950    0      1      
iter:  17  23:55:34  -9.73          -50.160950    0      1      
iter:  18  23:55:37 -10.24          -50.160950    0      1      

Converged after 18 iterations.

Dipole moment: (-0.028561, -0.718790, 0.099939) |e|*Ang

Energy contributions relative to reference atoms: (reference = -5640.280848)

Kinetic:        +41.994917
Potential:      -45.417895
External:        +0.000000
XC:             -46.946564
Entropy (-ST):   +0.000000
Local:           +0.208592
--------------------------
Free energy:    -50.160950
Extrapolated:   -50.160950

Fermi levels: -2.76344, -2.76344

 Band  Eigenvalues  Occupancy
    0    -25.49289    2.00000
    1    -22.37475    2.00000
    2    -18.22498    2.00000
    3    -14.38186    2.00000
    4    -13.12877    2.00000
    5    -11.24335    2.00000
    6    -10.80461    2.00000
    7     -9.60798    2.00000
    8     -9.28629    2.00000
    9     -8.68504    2.00000
   10     -6.21383    2.00000
   11     -5.32112    2.00000
   12     -0.20576    0.00000
   13      1.26358    0.00000
   14      2.53349    0.00000
   15      2.97650    0.00000
   16      3.71871    0.00000
   17      3.90398    0.00000
   18      5.18598    0.00000
   19      7.20275    0.00000
   20      8.36768    0.00000
   21      9.39877    0.00000
   22     10.01109    0.00000
   23     10.60498    0.00000
   24     11.07686    0.00000
   25     11.40557    0.00000
   26     12.11004    0.00000
   27     13.83906    0.00000
   28     14.08237    0.00000
   29     14.80773    0.00000
   30     15.33554    0.00000
   31     15.74822    0.00000
   32     15.97110    0.00000
   33     17.49772    0.00000
   34     17.92875    0.00000
   35     19.27286    0.00000
   36     19.95488    0.00000
   37     20.48487    0.00000
   38     21.64594    0.00000
   39     21.77045    0.00000
   40     24.61141    0.00000
   41     25.47272    0.00000
   42     26.18337    0.00000
   43     27.55175    0.00000
   44     27.79252    0.00000
   45     30.15675    0.00000
   46     31.14493    0.00000
   47     32.82258    0.00000
   48     33.31187    0.00000
   49     34.67495    0.00000
   50     36.12620    0.00000
   51     36.63701    0.00000
   52     36.92847    0.00000
   53     37.41803    0.00000
   54     38.43994    0.00000
   55     39.48208    0.00000
   56     41.13397    0.00000
   57     42.44364    0.00000
   58     43.07142    0.00000
   59     44.32622    0.00000
   60     44.80674    0.00000
   61     47.02357    0.00000
   62     47.96906    0.00000
   63     49.26412    0.00000
   64     49.48023    0.00000
   65     50.63457    0.00000
   66     51.40873    0.00000
   67     51.47893    0.00000
   68     53.72186    0.00000
   69     55.01999    0.00000
   70     56.36525    0.00000
   71     58.17372    0.00000
   72     63.72764    0.00000
   73     64.97205    0.00000
   74     68.36800    0.00000
   75     70.30070    0.00000
   76     75.44955    0.00000

Gap: 5.115 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.013     0.013   0.0% |
LCAO WFS Initialize:                   1.522     0.046   0.1% |
 Hamiltonian:                          1.475     0.000   0.0% |
  Atomic:                              0.047     0.001   0.0% |
   XC Correction:                      0.046     0.046   0.1% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.007     0.007   0.0% |
  Hartree integrate/restrict:          0.034     0.034   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.331     0.017   0.0% |
   Communicate bwd 0:                  0.020     0.020   0.1% |
   Communicate bwd 1:                  0.088     0.088   0.2% |
   Communicate fwd 0:                  0.081     0.081   0.2% |
   Communicate fwd 1:                  0.021     0.021   0.1% |
   fft:                                0.029     0.029   0.1% |
   fft2:                               0.075     0.075   0.2% |
  XC 3D grid:                          1.045     1.045   2.7% ||
  vbar:                                0.006     0.006   0.0% |
P tci:                                 0.005     0.005   0.0% |
SCF-cycle:                            34.216     0.004   0.0% |
 Direct Minimisation step:            32.664     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.393     0.005   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.003     0.003   0.0% |
   Distribute overlap matrix:          0.037     0.037   0.1% |
   Potential matrix:                   0.343     0.343   0.9% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.020     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.016     0.016   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.012     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.009     0.009   0.0% |
  Update Kohn-Sham energy:            32.231     0.000   0.0% |
   Density:                            1.327     0.000   0.0% |
    Atomic density matrices:           0.056     0.056   0.1% |
    Mix:                               0.914     0.914   2.3% ||
    Multipole moments:                 0.004     0.004   0.0% |
    Normalize:                         0.005     0.005   0.0% |
    Pseudo density:                    0.347     0.017   0.0% |
     Calculate density matrix:         0.002     0.002   0.0% |
     Construct density:                0.328     0.328   0.8% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       30.904     0.002   0.0% |
    Atomic:                            0.991     0.015   0.0% |
     XC Correction:                    0.976     0.976   2.5% ||
    Calculate atomic Hamiltonians:     0.054     0.054   0.1% |
    Communicate:                       0.139     0.139   0.4% |
    Hartree integrate/restrict:        0.581     0.581   1.5% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           7.062     0.370   0.9% |
     Communicate bwd 0:                0.509     0.509   1.3% ||
     Communicate bwd 1:                1.839     1.839   4.7% |-|
     Communicate fwd 0:                1.704     1.704   4.4% |-|
     Communicate fwd 1:                0.452     0.452   1.2% |
     fft:                              0.610     0.610   1.6% ||
     fft2:                             1.578     1.578   4.0% |-|
    XC 3D grid:                       21.976    21.976  56.2% |---------------------|
    vbar:                              0.095     0.095   0.2% |
 Get canonical representation:         1.548     0.001   0.0% |
  LCAO eigensolver:                    0.020     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.016     0.016   0.0% |
  Update Kohn-Sham energy:             1.528     0.000   0.0% |
   Density:                            0.062     0.000   0.0% |
    Atomic density matrices:           0.002     0.002   0.0% |
    Mix:                               0.043     0.043   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.016     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.015     0.015   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.466     0.000   0.0% |
    Atomic:                            0.047     0.001   0.0% |
     XC Correction:                    0.047     0.047   0.1% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.007     0.007   0.0% |
    Hartree integrate/restrict:        0.027     0.027   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.336     0.018   0.0% |
     Communicate bwd 0:                0.024     0.024   0.1% |
     Communicate bwd 1:                0.087     0.087   0.2% |
     Communicate fwd 0:                0.080     0.080   0.2% |
     Communicate fwd 1:                0.023     0.023   0.1% |
     fft:                              0.029     0.029   0.1% |
     fft2:                             0.075     0.075   0.2% |
    XC 3D grid:                        1.042     1.042   2.7% ||
    vbar:                              0.005     0.005   0.0% |
ST tci:                                0.003     0.003   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.994     0.994   2.5% ||
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.358     2.358   6.0% |-|
-------------------------------------------------------------
Total:                                          39.113 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:55:37 2019
