
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:22:03 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2726.570947

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*92*100 grid
  Fine grid: 184*184*200 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*184*200 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 34.18 MiB
    Density: 20.52 MiB
      Arrays: 20.28 MiB
      Localized functions: 0.24 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 13.29 MiB
      Arrays: 13.27 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.02 MiB
    Wavefunctions: 0.38 MiB
      C [qnM]: 0.00 MiB
      S, T [2 x qmm]: 0.00 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 0.37 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 2
Number of atomic orbitals: 18
Number of bands in calculation: 18
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
          .---------------------------------.  
         /|                                 |  
        / |                                 |  
       /  |                                 |  
      /   |                                 |  
     /    |                                 |  
    /     |                                 |  
   /      |                                 |  
  /       |                                 |  
 *        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        |            F                    |  
 |        |            H                    |  
 |        |                                 |  
 |        |                                 |  
 |        |                                 |  
 |        .---------------------------------.  
 |       /                                 /   
 |      /                                 /    
 |     /                                 /     
 |    /                                 /      
 |   /                                 /       
 |  /                                 /        
 | /                                 /         
 |/                                 /          
 *---------------------------------*           

Positions:
   0 F      7.000000    7.000000    7.933891    ( 0.0000,  0.0000,  0.0000)
   1 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   14.000000    0.000000    92     0.1522
  3. axis:    no     0.000000    0.000000   14.933891   100     0.1493

  Lengths:  14.000000  14.000000  14.933891
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1512

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:22:09  +0.42           -6.994954    0      1      
iter:   2  00:22:10  +0.20           -6.998441    0      1      
iter:   3  00:22:11  -2.53           -7.000595    0      1      
iter:   4  00:22:12  -3.65           -7.000606    0      1      
iter:   5  00:22:13  -4.94           -7.000607    0      1      
iter:   6  00:22:13  -5.68           -7.000607    0      1      
iter:   7  00:22:14  -5.93           -7.000607    0      1      
iter:   8  00:22:15  -8.92           -7.000607    0      1      
iter:   9  00:22:17 -10.05           -7.000607    0      1      

Converged after 9 iterations.

Dipole moment: (-0.000000, -0.000000, -0.351313) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2726.570947)

Kinetic:        +11.398280
Potential:      -10.466060
External:        +0.000000
XC:              -8.035213
Entropy (-ST):   +0.000000
Local:           +0.102386
--------------------------
Free energy:     -7.000607
Extrapolated:    -7.000607

Fermi levels: -3.80793, -3.80793

 Band  Eigenvalues  Occupancy
    0    -29.16783    2.00000
    1    -12.55605    2.00000
    2     -8.64778    2.00000
    3     -8.64778    2.00000
    4      1.03192    0.00000
    5     11.79319    0.00000
    6     19.75024    0.00000
    7     19.75024    0.00000
    8     21.81873    0.00000
    9     25.17192    0.00000
   10     25.17192    0.00000
   11     39.34724    0.00000
   12     47.46274    0.00000
   13     58.95199    0.00000
   14     58.95277    0.00000
   15     67.30437    0.00000
   16     67.30437    0.00000
   17     83.50862    0.00000

Gap: 9.680 eV
Transition (v -> c):
  (s=0, k=0, n=3, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=4, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.002     0.002   0.0% |
LCAO WFS Initialize:                   0.984     0.026   0.2% |
 Hamiltonian:                          0.958     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.048     0.048   0.3% |
  Hartree integrate/restrict:          0.018     0.018   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.273     0.004   0.0% |
   Communicate bwd 0:                  0.053     0.053   0.4% |
   Communicate bwd 1:                  0.050     0.050   0.3% |
   Communicate fwd 0:                  0.049     0.049   0.3% |
   Communicate fwd 1:                  0.053     0.053   0.4% |
   fft:                                0.015     0.015   0.1% |
   fft2:                               0.049     0.049   0.3% |
  XC 3D grid:                          0.612     0.612   4.3% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            11.680     0.002   0.0% |
 Direct Minimisation step:            10.706     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.015     0.001   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.006     0.006   0.0% |
   Potential matrix:                   0.007     0.007   0.0% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.001     0.001   0.0% |
  LCAO eigensolver:                    0.001     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.001     0.001   0.0% |
  Preconditioning::                    0.000     0.000   0.0% |
  Unitary rotation:                    0.004     0.000   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.004     0.004   0.0% |
  Update Kohn-Sham energy:            10.683     0.000   0.0% |
   Density:                            0.285     0.000   0.0% |
    Atomic density matrices:           0.010     0.010   0.1% |
    Mix:                               0.260     0.260   1.8% ||
    Multipole moments:                 0.001     0.001   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.012     0.004   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.008     0.008   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       10.397     0.001   0.0% |
    Atomic:                            0.003     0.003   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
    Communicate:                       0.527     0.527   3.7% ||
    Hartree integrate/restrict:        0.195     0.195   1.4% ||
    New Kinetic Energy:                0.001     0.000   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.019     0.047   0.3% |
     Communicate bwd 0:                0.594     0.594   4.1% |-|
     Communicate bwd 1:                0.551     0.551   3.8% |-|
     Communicate fwd 0:                0.542     0.542   3.8% |-|
     Communicate fwd 1:                0.591     0.591   4.1% |-|
     fft:                              0.163     0.163   1.1% |
     fft2:                             0.532     0.532   3.7% ||
    XC 3D grid:                        6.617     6.617  46.0% |-----------------|
    vbar:                              0.030     0.030   0.2% |
 Get canonical representation:         0.972     0.000   0.0% |
  LCAO eigensolver:                    0.001     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.000     0.000   0.0% |
   Orbital Layouts:                    0.000     0.000   0.0% |
   Potential matrix:                   0.001     0.001   0.0% |
  Update Kohn-Sham energy:             0.970     0.000   0.0% |
   Density:                            0.026     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.024     0.024   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.001     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.001     0.001   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        0.944     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.048     0.048   0.3% |
    Hartree integrate/restrict:        0.018     0.018   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.275     0.004   0.0% |
     Communicate bwd 0:                0.054     0.054   0.4% |
     Communicate bwd 1:                0.051     0.051   0.4% |
     Communicate fwd 0:                0.049     0.049   0.3% |
     Communicate fwd 1:                0.054     0.054   0.4% |
     fft:                              0.015     0.015   0.1% |
     fft2:                             0.049     0.049   0.3% |
    XC 3D grid:                        0.600     0.600   4.2% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.181     0.181   1.3% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.528     1.528  10.6% |---|
-------------------------------------------------------------
Total:                                          14.378 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:22:17 2019
