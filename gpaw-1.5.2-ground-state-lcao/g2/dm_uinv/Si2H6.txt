
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:27:37 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -15847.629474

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*112*116 grid
  Fine grid: 216*224*232 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*224*232 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 61.69 MiB
    Density: 35.86 MiB
      Arrays: 33.80 MiB
      Localized functions: 2.06 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 22.26 MiB
      Arrays: 22.11 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.15 MiB
    Wavefunctions: 3.57 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.05 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 3.50 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 8
Number of atomic orbitals: 56
Number of bands in calculation: 56
Bands to converge: occupied states only
Number of valence electrons: 14

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |               H                       |  
 |         |           H  Si H                     |  
 |         |                                       |  
 |         |                                       |  
 |         |           H  Si H                     |  
 |         |             H                         |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 Si     8.206621    8.393286    9.853703    ( 0.0000,  0.0000,  0.0000)
   1 Si     8.206621    8.393286    7.518337    ( 0.0000,  0.0000,  0.0000)
   2 H      8.206621    9.786572   10.372040    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.696643   10.372040    ( 0.0000,  0.0000,  0.0000)
   4 H      9.413242    7.696643   10.372040    ( 0.0000,  0.0000,  0.0000)
   5 H      8.206621    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   6 H      7.000000    9.089929    7.000000    ( 0.0000,  0.0000,  0.0000)
   7 H      9.413242    9.089929    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.413242    0.000000    0.000000   108     0.1520
  2. axis:    no     0.000000   16.786572    0.000000   112     0.1499
  3. axis:    no     0.000000    0.000000   17.372040   116     0.1498

  Lengths:  16.413242  16.786572  17.372040
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1505

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:27:44  +1.31          -29.898109    0      1      
iter:   2  00:27:46  +0.27          -30.128370    0      1      
iter:   3  00:27:47  -1.11          -30.147739    0      1      
iter:   4  00:27:49  -3.52          -30.149023    0      1      
iter:   5  00:27:51  -4.73          -30.149027    0      1      
iter:   6  00:27:52  -6.03          -30.149027    0      1      
iter:   7  00:27:54  -6.79          -30.149027    0      1      
iter:   8  00:27:56  -8.16          -30.149027    0      1      
iter:   9  00:27:57  -9.65          -30.149027    0      1      
iter:  10  00:28:01 -11.23          -30.149027    0      1      

Converged after 10 iterations.

Dipole moment: (0.000000, 0.000000, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -15847.629474)

Kinetic:        +27.957475
Potential:      -31.232598
External:        +0.000000
XC:             -26.811318
Entropy (-ST):   +0.000000
Local:           -0.062585
--------------------------
Free energy:    -30.149027
Extrapolated:   -30.149027

Fermi levels: -3.68704, -3.68704

 Band  Eigenvalues  Occupancy
    0    -14.04775    2.00000
    1    -12.44208    2.00000
    2     -8.79786    2.00000
    3     -8.79779    2.00000
    4     -8.15640    2.00000
    5     -8.15636    2.00000
    6     -7.22137    2.00000
    7     -0.15272    0.00000
    8     -0.15238    0.00000
    9      0.15535    0.00000
   10      0.84979    0.00000
   11      1.62879    0.00000
   12      1.62889    0.00000
   13      2.05410    0.00000
   14      4.22295    0.00000
   15      5.09759    0.00000
   16      5.09771    0.00000
   17      5.65434    0.00000
   18      5.65438    0.00000
   19      6.66542    0.00000
   20      6.75699    0.00000
   21      6.75714    0.00000
   22      8.56858    0.00000
   23     10.65361    0.00000
   24     10.65380    0.00000
   25     10.71517    0.00000
   26     10.71555    0.00000
   27     11.76495    0.00000
   28     12.48683    0.00000
   29     12.48719    0.00000
   30     13.42739    0.00000
   31     16.50839    0.00000
   32     18.51496    0.00000
   33     18.63957    0.00000
   34     18.63996    0.00000
   35     20.42631    0.00000
   36     20.42684    0.00000
   37     21.64547    0.00000
   38     21.65300    0.00000
   39     21.84533    0.00000
   40     24.60318    0.00000
   41     24.60323    0.00000
   42     25.75518    0.00000
   43     25.75536    0.00000
   44     28.94035    0.00000
   45     30.18361    0.00000
   46     31.71405    0.00000
   47     31.71416    0.00000
   48     32.33063    0.00000
   49     32.33082    0.00000
   50     38.85438    0.00000
   51     40.13427    0.00000
   52     42.22524    0.00000
   53     42.22577    0.00000
   54     44.48399    0.00000
   55     44.48508    0.00000

Gap: 7.069 eV
Transition (v -> c):
  (s=0, k=0, n=6, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=7, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.014     0.014   0.1% |
LCAO WFS Initialize:                   1.637     0.047   0.2% |
 Hamiltonian:                          1.590     0.000   0.0% |
  Atomic:                              0.063     0.001   0.0% |
   XC Correction:                      0.062     0.062   0.3% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.001     0.001   0.0% |
  Hartree integrate/restrict:          0.033     0.033   0.1% |
  Initialize Hamiltonian:              0.004     0.004   0.0% |
  Poisson:                             0.460     0.008   0.0% |
   Communicate bwd 0:                  0.090     0.090   0.4% |
   Communicate bwd 1:                  0.083     0.083   0.3% |
   Communicate fwd 0:                  0.085     0.085   0.4% |
   Communicate fwd 1:                  0.092     0.092   0.4% |
   fft:                                0.044     0.044   0.2% |
   fft2:                               0.059     0.059   0.2% |
  XC 3D grid:                          1.022     1.022   4.2% |-|
  vbar:                                0.006     0.006   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            20.004     0.002   0.0% |
 Direct Minimisation step:            18.339     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.195     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.029     0.029   0.1% |
   Potential matrix:                   0.161     0.161   0.7% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.018     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.015     0.015   0.1% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.004     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.004     0.004   0.0% |
  Update Kohn-Sham energy:            18.117     0.000   0.0% |
   Density:                            0.697     0.000   0.0% |
    Atomic density matrices:           0.040     0.040   0.2% |
    Mix:                               0.483     0.483   2.0% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.003     0.003   0.0% |
    Pseudo density:                    0.169     0.008   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.161     0.161   0.7% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       17.420     0.001   0.0% |
    Atomic:                            0.695     0.006   0.0% |
     XC Correction:                    0.689     0.689   2.9% ||
    Calculate atomic Hamiltonians:     0.021     0.021   0.1% |
    Communicate:                       0.013     0.013   0.1% |
    Hartree integrate/restrict:        0.322     0.322   1.3% ||
    New Kinetic Energy:                0.001     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           5.110     0.086   0.4% |
     Communicate bwd 0:                1.019     1.019   4.2% |-|
     Communicate bwd 1:                0.943     0.943   3.9% |-|
     Communicate fwd 0:                0.935     0.935   3.9% |-|
     Communicate fwd 1:                1.002     1.002   4.2% |-|
     fft:                              0.480     0.480   2.0% ||
     fft2:                             0.645     0.645   2.7% ||
    XC 3D grid:                       11.208    11.208  46.5% |------------------|
    vbar:                              0.048     0.048   0.2% |
 Get canonical representation:         1.663     0.000   0.0% |
  LCAO eigensolver:                    0.018     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.003     0.003   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.015     0.015   0.1% |
  Update Kohn-Sham energy:             1.645     0.000   0.0% |
   Density:                            0.063     0.000   0.0% |
    Atomic density matrices:           0.004     0.004   0.0% |
    Mix:                               0.043     0.043   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.015     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.014     0.014   0.1% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.581     0.000   0.0% |
    Atomic:                            0.063     0.001   0.0% |
     XC Correction:                    0.063     0.063   0.3% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.001     0.001   0.0% |
    Hartree integrate/restrict:        0.029     0.029   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.460     0.008   0.0% |
     Communicate bwd 0:                0.090     0.090   0.4% |
     Communicate bwd 1:                0.085     0.085   0.4% |
     Communicate fwd 0:                0.085     0.085   0.4% |
     Communicate fwd 1:                0.089     0.089   0.4% |
     fft:                              0.044     0.044   0.2% |
     fft2:                             0.059     0.059   0.2% |
    XC 3D grid:                        1.021     1.021   4.2% |-|
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.182     0.182   0.8% |
mktci:                                 0.002     0.002   0.0% |
Other:                                 2.246     2.246   9.3% |---|
-------------------------------------------------------------
Total:                                          24.088 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:28:01 2019
