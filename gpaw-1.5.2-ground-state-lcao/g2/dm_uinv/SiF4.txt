
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:41:32 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Si-setup:
  name: Silicon
  id: ee77bee481871cc2cb65ac61239ccafa
  Z: 14
  valence: 4
  core: 10
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Si.PBE.gz
  cutoffs: 1.06(comp), 1.86(filt), 2.06(core), lmax=2
  valence states:
                energy  radius
    3s(2.00)   -10.812   1.058
    3p(2.00)    -4.081   1.058
    *s          16.399   1.058
    *p          23.130   1.058
    *d           0.000   1.058

  LCAO basis set for Si:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Si.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.8594 Bohr: 3s-sz confined orbital
      l=1, rc=9.0625 Bohr: 3p-sz confined orbital
      l=0, rc=3.8906 Bohr: 3s-dz split-valence wave
      l=1, rc=5.2344 Bohr: 3p-dz split-valence wave
      l=2, rc=9.0625 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -18742.667391

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 104*104*104 grid
  Fine grid: 208*208*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 208*208*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 48.22 MiB
    Density: 28.49 MiB
      Arrays: 27.03 MiB
      Localized functions: 1.45 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 17.80 MiB
      Arrays: 17.69 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.11 MiB
    Wavefunctions: 1.94 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.06 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.84 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 5
Number of atomic orbitals: 65
Number of bands in calculation: 65
Bands to converge: occupied states only
Number of valence electrons: 32

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .--------------------------------------.  
          /|                                      |  
         / |                                      |  
        /  |                                      |  
       /   |                                      |  
      /    |                                      |  
     /     |                                      |  
    /      |                                      |  
   /       |                                      |  
  /        |                                      |  
 *         |                                      |  
 |         |                                      |  
 |         |                                      |  
 |         |                F                     |  
 |         |           F Si                       |  
 |         |            F                         |  
 |         |               F                      |  
 |         |                                      |  
 |         |                                      |  
 |         .--------------------------------------.  
 |        /                                      /   
 |       /                                      /    
 |      /                                      /     
 |     /                                      /      
 |    /                                      /       
 |   /                                      /        
 |  /                                      /         
 | /                                      /          
 |/                                      /           
 *--------------------------------------*            

Positions:
   0 Si     7.912806    7.912806    7.912806    ( 0.0000,  0.0000,  0.0000)
   1 F      8.825612    8.825612    8.825612    ( 0.0000,  0.0000,  0.0000)
   2 F      7.000000    7.000000    8.825612    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    8.825612    7.000000    ( 0.0000,  0.0000,  0.0000)
   4 F      8.825612    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    15.825612    0.000000    0.000000   104     0.1522
  2. axis:    no     0.000000   15.825612    0.000000   104     0.1522
  3. axis:    no     0.000000    0.000000   15.825612   104     0.1522

  Lengths:  15.825612  15.825612  15.825612
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1522

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:41:40  -0.43          -25.179685    0      1      
iter:   2  23:41:41  -0.28          -25.185044    0      1      
iter:   3  23:41:43  -2.99          -25.201584    0      1      
iter:   4  23:41:44  -3.91          -25.201648    0      1      
iter:   5  23:41:45  -5.28          -25.201657    0      1      
iter:   6  23:41:47  -5.63          -25.201657    0      1      
iter:   7  23:41:48  -6.28          -25.201657    0      1      
iter:   8  23:41:49  -8.10          -25.201657    0      1      
iter:   9  23:41:51  -8.86          -25.201657    0      1      
iter:  10  23:41:53 -10.74          -25.201657    0      1      

Converged after 10 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -18742.667391)

Kinetic:        +37.406069
Potential:      -33.701713
External:        +0.000000
XC:             -29.159182
Entropy (-ST):   +0.000000
Local:           +0.253169
--------------------------
Free energy:    -25.201657
Extrapolated:   -25.201657

Fermi levels: -5.04996, -5.04996

 Band  Eigenvalues  Occupancy
    0    -31.37656    2.00000
    1    -30.43838    2.00000
    2    -30.43838    2.00000
    3    -30.43838    2.00000
    4    -14.96205    2.00000
    5    -12.92843    2.00000
    6    -12.92843    2.00000
    7    -12.92843    2.00000
    8    -11.36284    2.00000
    9    -11.36284    2.00000
   10    -11.21409    2.00000
   11    -11.21409    2.00000
   12    -11.21409    2.00000
   13    -10.21841    2.00000
   14    -10.21841    2.00000
   15    -10.21841    2.00000
   16      0.11849    0.00000
   17      1.91794    0.00000
   18      1.91794    0.00000
   19      1.91794    0.00000
   20      6.53102    0.00000
   21      6.53102    0.00000
   22      6.53102    0.00000
   23     10.13576    0.00000
   24     11.04297    0.00000
   25     11.04297    0.00000
   26     16.95957    0.00000
   27     16.95957    0.00000
   28     16.95957    0.00000
   29     19.18985    0.00000
   30     20.09992    0.00000
   31     20.09992    0.00000
   32     21.24185    0.00000
   33     21.24185    0.00000
   34     21.24185    0.00000
   35     22.61785    0.00000
   36     22.61785    0.00000
   37     22.61785    0.00000
   38     26.29000    0.00000
   39     26.29000    0.00000
   40     26.29000    0.00000
   41     47.31188    0.00000
   42     47.31188    0.00000
   43     47.31188    0.00000
   44     53.41021    0.00000
   45     55.42951    0.00000
   46     55.42951    0.00000
   47     55.42951    0.00000
   48     56.40224    0.00000
   49     56.40224    0.00000
   50     56.40224    0.00000
   51     56.66629    0.00000
   52     56.66629    0.00000
   53     56.85547    0.00000
   54     56.85547    0.00000
   55     56.85547    0.00000
   56     64.36305    0.00000
   57     64.36305    0.00000
   58     64.36305    0.00000
   59     71.89970    0.00000
   60     71.89970    0.00000
   61     71.98372    0.00000
   62     74.33876    0.00000
   63     74.33876    0.00000
   64     74.33876    0.00000

Gap: 10.337 eV
Transition (v -> c):
  (s=0, k=0, n=15, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=16, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.008     0.008   0.0% |
LCAO WFS Initialize:                   1.312     0.035   0.2% |
 Hamiltonian:                          1.277     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.065     0.065   0.3% |
  Hartree integrate/restrict:          0.023     0.023   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.366     0.007   0.0% |
   Communicate bwd 0:                  0.073     0.073   0.4% |
   Communicate bwd 1:                  0.069     0.069   0.3% |
   Communicate fwd 0:                  0.067     0.067   0.3% |
   Communicate fwd 1:                  0.074     0.074   0.4% |
   fft:                                0.025     0.025   0.1% |
   fft2:                               0.052     0.052   0.2% |
  XC 3D grid:                          0.817     0.817   3.9% |-|
  vbar:                                0.004     0.004   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            17.138     0.002   0.0% |
 Direct Minimisation step:            15.829     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.094     0.003   0.0% |
   Construct Gradient Matrix:          0.002     0.002   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.025     0.025   0.1% |
   Potential matrix:                   0.063     0.063   0.3% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.008     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.006     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.005     0.005   0.0% |
  Update Kohn-Sham energy:            15.716     0.000   0.0% |
   Density:                            0.510     0.000   0.0% |
    Atomic density matrices:           0.034     0.034   0.2% |
    Mix:                               0.402     0.402   1.9% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.070     0.006   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.063     0.063   0.3% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       15.205     0.001   0.0% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.006     0.006   0.0% |
    Communicate:                       0.775     0.775   3.7% ||
    Hartree integrate/restrict:        0.286     0.286   1.4% ||
    New Kinetic Energy:                0.002     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           4.309     0.072   0.3% |
     Communicate bwd 0:                0.860     0.860   4.2% |-|
     Communicate bwd 1:                0.806     0.806   3.9% |-|
     Communicate fwd 0:                0.795     0.795   3.8% |-|
     Communicate fwd 1:                0.850     0.850   4.1% |-|
     fft:                              0.296     0.296   1.4% ||
     fft2:                             0.628     0.628   3.0% ||
    XC 3D grid:                        9.780     9.780  47.2% |------------------|
    vbar:                              0.042     0.042   0.2% |
 Get canonical representation:         1.307     0.001   0.0% |
  LCAO eigensolver:                    0.008     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Update Kohn-Sham energy:             1.298     0.000   0.0% |
   Density:                            0.041     0.000   0.0% |
    Atomic density matrices:           0.003     0.003   0.0% |
    Mix:                               0.032     0.032   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.006     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.005     0.005   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.257     0.000   0.0% |
    Atomic:                            0.001     0.001   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.064     0.064   0.3% |
    Hartree integrate/restrict:        0.024     0.024   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.356     0.006   0.0% |
     Communicate bwd 0:                0.072     0.072   0.3% |
     Communicate bwd 1:                0.067     0.067   0.3% |
     Communicate fwd 0:                0.065     0.065   0.3% |
     Communicate fwd 1:                0.070     0.070   0.3% |
     fft:                              0.025     0.025   0.1% |
     fft2:                             0.052     0.052   0.3% |
    XC 3D grid:                        0.808     0.808   3.9% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.339     0.339   1.6% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.919     1.919   9.3% |---|
-------------------------------------------------------------
Total:                                          20.720 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:41:53 2019
