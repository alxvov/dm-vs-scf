
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:15:47 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

B-setup:
  name: Boron
  id: 6e91439235a05fd7549e31636e8f777c
  Z: 5
  valence: 3
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/B.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.38(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)    -9.443   0.635
    2p(1.00)    -3.606   0.635
    *s          17.768   0.635
    *p          23.605   0.635
    *d           0.000   0.635

  LCAO basis set for B:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/B.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=6.6719 Bohr: 2s-sz confined orbital
      l=1, rc=8.2188 Bohr: 2p-sz confined orbital
      l=0, rc=3.7500 Bohr: 2s-dz split-valence wave
      l=1, rc=4.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=8.2188 Bohr: d-type Gaussian polarization

F-setup:
  name: Fluorine
  id: 9cd46ba2a61e170ad72278be75b55cc0
  Z: 9
  valence: 7
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/F.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 0.74(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -29.898   0.635
    2p(5.00)   -11.110   0.635
    *s          -2.687   0.635
    *p          16.102   0.635
    *d           0.000   0.635

  LCAO basis set for F:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/F.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=3.8594 Bohr: 2s-sz confined orbital
      l=1, rc=4.8750 Bohr: 2p-sz confined orbital
      l=0, rc=2.0156 Bohr: 2s-dz split-valence wave
      l=1, rc=2.6094 Bohr: 2p-dz split-valence wave
      l=2, rc=4.8750 Bohr: d-type Gaussian polarization

Reference energy: -8811.731923

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 108*108*92 grid
  Fine grid: 216*216*184 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 216*216*184 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 0, 1].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 44.89 MiB
    Density: 26.47 MiB
      Arrays: 25.78 MiB
      Localized functions: 0.70 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 16.92 MiB
      Arrays: 16.86 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 1.50 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.04 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.43 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 4
Number of atomic orbitals: 52
Number of bands in calculation: 52
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------------.  
          /|                                       |  
         / |                                       |  
        /  |                                       |  
       /   |                                       |  
      /    |                                       |  
     /     |                                       |  
    /      |                                       |  
   /       |                                       |  
  /        |                                       |  
 *         |                                       |  
 |         |                                       |  
 |         |                                       |  
 |         |               F                       |  
 |         |           F  B F                      |  
 |         |                                       |  
 |         |                                       |  
 |         .---------------------------------------.  
 |        /                                       /   
 |       /                                       /    
 |      /                                       /     
 |     /                                       /      
 |    /                                       /       
 |   /                                       /        
 |  /                                       /         
 | /                                       /          
 |/                                       /           
 *---------------------------------------*            

Positions:
   0 B      8.144678    7.660880    7.000000    ( 0.0000,  0.0000,  0.0000)
   1 F      8.144678    8.982640    7.000000    ( 0.0000,  0.0000,  0.0000)
   2 F      9.289356    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)
   3 F      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    16.289356    0.000000    0.000000   108     0.1508
  2. axis:    no     0.000000   15.982640    0.000000   108     0.1480
  3. axis:    no     0.000000    0.000000   14.000000    92     0.1522

  Lengths:  16.289356  15.982640  14.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1503

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:15:54  -0.83          -21.563275    0      1      
iter:   2  00:15:56  -1.04          -21.565898    0      1      
iter:   3  00:15:57  -3.30          -21.567132    0      1      
iter:   4  00:15:58  -4.65          -21.567144    0      1      
iter:   5  00:15:59  -6.40          -21.567144    0      1      
iter:   6  00:16:01  -7.03          -21.567144    0      1      
iter:   7  00:16:03  -7.16          -21.567144    0      1      
iter:   8  00:16:04  -9.21          -21.567144    0      1      
iter:   9  00:16:07 -10.05          -21.567144    0      1      

Converged after 9 iterations.

Dipole moment: (0.000000, -0.000023, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -8811.731923)

Kinetic:        +29.294268
Potential:      -28.447605
External:        +0.000000
XC:             -22.728128
Entropy (-ST):   +0.000000
Local:           +0.314321
--------------------------
Free energy:    -21.567144
Extrapolated:   -21.567144

Fermi levels: -4.89511, -4.89511

 Band  Eigenvalues  Occupancy
    0    -31.57965    2.00000
    1    -30.58352    2.00000
    2    -30.58342    2.00000
    3    -14.54869    2.00000
    4    -13.48482    2.00000
    5    -13.48431    2.00000
    6    -12.53514    2.00000
    7    -10.71269    2.00000
    8    -10.71264    2.00000
    9    -10.50806    2.00000
   10    -10.50793    2.00000
   11     -9.58891    2.00000
   12     -0.20132    0.00000
   13      1.90476    0.00000
   14      5.12676    0.00000
   15      5.12696    0.00000
   16      5.77707    0.00000
   17     10.35639    0.00000
   18     10.35778    0.00000
   19     11.75719    0.00000
   20     18.94186    0.00000
   21     18.94365    0.00000
   22     19.64844    0.00000
   23     19.64861    0.00000
   24     21.61225    0.00000
   25     22.80009    0.00000
   26     22.80072    0.00000
   27     23.95212    0.00000
   28     24.85499    0.00000
   29     24.90273    0.00000
   30     24.90353    0.00000
   31     25.59658    0.00000
   32     34.66749    0.00000
   33     34.66775    0.00000
   34     47.06177    0.00000
   35     47.06601    0.00000
   36     52.37209    0.00000
   37     56.72645    0.00000
   38     56.72671    0.00000
   39     57.11321    0.00000
   40     57.91298    0.00000
   41     57.91310    0.00000
   42     58.23528    0.00000
   43     61.77201    0.00000
   44     63.82916    0.00000
   45     70.96733    0.00000
   46     70.96764    0.00000
   47     76.45921    0.00000
   48     76.46309    0.00000
   49     79.92802    0.00000
   50     81.75732    0.00000
   51     81.75880    0.00000

Gap: 9.388 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.006     0.006   0.0% |
LCAO WFS Initialize:                   1.237     0.034   0.2% |
 Hamiltonian:                          1.203     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.001     0.001   0.0% |
  Communicate:                         0.050     0.050   0.3% |
  Hartree integrate/restrict:          0.024     0.024   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.348     0.011   0.1% |
   Communicate bwd 0:                  0.064     0.064   0.3% |
   Communicate bwd 1:                  0.064     0.064   0.3% |
   Communicate fwd 0:                  0.065     0.065   0.3% |
   Communicate fwd 1:                  0.069     0.069   0.4% |
   fft:                                0.020     0.020   0.1% |
   fft2:                               0.055     0.055   0.3% |
  XC 3D grid:                          0.776     0.776   4.0% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            16.146     0.002   0.0% |
 Direct Minimisation step:            14.915     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.076     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.011     0.011   0.1% |
   Potential matrix:                   0.060     0.060   0.3% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.007     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.006     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.005     0.005   0.0% |
  Update Kohn-Sham energy:            14.823     0.000   0.0% |
   Density:                            0.486     0.000   0.0% |
    Atomic density matrices:           0.018     0.018   0.1% |
    Mix:                               0.396     0.396   2.0% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.068     0.006   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.061     0.061   0.3% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       14.336     0.001   0.0% |
    Atomic:                            0.004     0.004   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.006     0.006   0.0% |
    Communicate:                       0.587     0.587   3.0% ||
    Hartree integrate/restrict:        0.266     0.266   1.4% ||
    New Kinetic Energy:                0.001     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           4.122     0.116   0.6% |
     Communicate bwd 0:                0.780     0.780   4.0% |-|
     Communicate bwd 1:                0.772     0.772   3.9% |-|
     Communicate fwd 0:                0.748     0.748   3.8% |-|
     Communicate fwd 1:                0.805     0.805   4.1% |-|
     fft:                              0.241     0.241   1.2% |
     fft2:                             0.660     0.660   3.4% ||
    XC 3D grid:                        9.308     9.308  47.6% |------------------|
    vbar:                              0.040     0.040   0.2% |
 Get canonical representation:         1.228     0.000   0.0% |
  LCAO eigensolver:                    0.007     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.001     0.001   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.005     0.005   0.0% |
  Update Kohn-Sham energy:             1.221     0.000   0.0% |
   Density:                            0.039     0.000   0.0% |
    Atomic density matrices:           0.001     0.001   0.0% |
    Mix:                               0.032     0.032   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.006     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.005     0.005   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.182     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.001     0.001   0.0% |
    Communicate:                       0.049     0.049   0.2% |
    Hartree integrate/restrict:        0.022     0.022   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.341     0.009   0.0% |
     Communicate bwd 0:                0.064     0.064   0.3% |
     Communicate bwd 1:                0.064     0.064   0.3% |
     Communicate fwd 0:                0.061     0.061   0.3% |
     Communicate fwd 1:                0.066     0.066   0.3% |
     fft:                              0.020     0.020   0.1% |
     fft2:                             0.056     0.056   0.3% |
    XC 3D grid:                        0.766     0.766   3.9% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.001     0.001   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.342     0.342   1.8% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.818     1.818   9.3% |---|
-------------------------------------------------------------
Total:                                          19.553 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:16:07 2019
