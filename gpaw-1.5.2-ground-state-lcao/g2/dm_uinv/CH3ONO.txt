
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sat Feb 16 23:47:44 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

N-setup:
  name: Nitrogen
  id: f7500608b86eaa90eef8b1d9a670dc53
  Z: 7
  valence: 5
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/N.PBE.gz
  cutoffs: 0.58(comp), 1.11(filt), 0.96(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -18.583   0.603
    2p(3.00)    -7.089   0.529
    *s           8.629   0.603
    *p          20.123   0.529
    *d           0.000   0.577

  LCAO basis set for N:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/N.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.8594 Bohr: 2s-sz confined orbital
      l=1, rc=6.0625 Bohr: 2p-sz confined orbital
      l=0, rc=2.6094 Bohr: 2s-dz split-valence wave
      l=1, rc=3.2656 Bohr: 2p-dz split-valence wave
      l=2, rc=6.0625 Bohr: d-type Gaussian polarization

Reference energy: -6628.524709

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 112*108*104 grid
  Fine grid: 224*216*208 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 224*216*208 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [2, 1, 0].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 416.48 MiB
  Calculator: 53.05 MiB
    Density: 31.23 MiB
      Arrays: 30.27 MiB
      Localized functions: 0.96 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 19.87 MiB
      Arrays: 19.80 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.07 MiB
    Wavefunctions: 1.95 MiB
      C [qnM]: 0.03 MiB
      S, T [2 x qmm]: 0.07 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.85 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 7
Number of atomic orbitals: 67
Number of bands in calculation: 67
Bands to converge: occupied states only
Number of valence electrons: 24

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .-----------------------------------------.  
          /|                                         |  
         / |                                         |  
        /  |                                         |  
       /   |                                         |  
      /    |                                         |  
     /     |                                         |  
    /      |                                         |  
   /       |                                         |  
  /        |                                         |  
 *         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |                                         |  
 |         |            HC   O                       |  
 |         |                 O N                     |  
 |         |            H                            |  
 |         |                                         |  
 |         |                                         |  
 |         .-----------------------------------------.  
 |        /                                         /   
 |       /                                         /    
 |      /                                         /     
 |     /                                         /      
 |    /                                         /       
 |   /                                         /        
 |  /                                         /         
 | /                                         /          
 |/                                         /           
 *-----------------------------------------*            

Positions:
   0 C      7.669330    8.487663    7.890672    ( 0.0000,  0.0000,  0.0000)
   1 O      8.985538    9.075268    7.890672    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    9.344429    7.890672    ( 0.0000,  0.0000,  0.0000)
   3 H      7.521202    7.873779    8.781344    ( 0.0000,  0.0000,  0.0000)
   4 H      7.521202    7.873779    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 N     10.030872    8.155601    7.890672    ( 0.0000,  0.0000,  0.0000)
   6 O      9.672302    7.000000    7.890672    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    17.030872    0.000000    0.000000   112     0.1521
  2. axis:    no     0.000000   16.344429    0.000000   108     0.1513
  3. axis:    no     0.000000    0.000000   15.781344   104     0.1517

  Lengths:  17.030872  16.344429  15.781344
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1517

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  23:47:53  +1.01          -37.291164    0      1      
iter:   2  23:47:55  +0.14          -37.447907    0      1      
iter:   3  23:47:57  -0.18          -37.465920    0      1      
iter:   4  23:47:58  -1.31          -37.474325    0      1      
iter:   5  23:47:59  -1.67          -37.475120    0      1      
iter:   6  23:48:01  -2.86          -37.475472    0      1      
iter:   7  23:48:02  -3.12          -37.475523    0      1      
iter:   8  23:48:04  -3.71          -37.475546    0      1      
iter:   9  23:48:05  -4.74          -37.475550    0      1      
iter:  10  23:48:06  -4.75          -37.475551    0      1      
iter:  11  23:48:08  -5.40          -37.475551    0      1      
iter:  12  23:48:09  -6.12          -37.475551    0      1      
iter:  13  23:48:10  -7.21          -37.475551    0      1      
iter:  14  23:48:12  -7.38          -37.475551    0      1      
iter:  15  23:48:13  -8.38          -37.475551    0      1      
iter:  16  23:48:14  -8.59          -37.475551    0      1      
iter:  17  23:48:16  -9.67          -37.475551    0      1      
iter:  18  23:48:18 -10.02          -37.475551    0      1      

Converged after 18 iterations.

Dipole moment: (-0.438285, 0.067782, 0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -6628.524709)

Kinetic:        +34.769292
Potential:      -36.998164
External:        +0.000000
XC:             -35.499490
Entropy (-ST):   +0.000000
Local:           +0.252812
--------------------------
Free energy:    -37.475551
Extrapolated:   -37.475551

Fermi levels: -4.52012, -4.52012

 Band  Eigenvalues  Occupancy
    0    -30.17223    2.00000
    1    -26.25803    2.00000
    2    -18.94490    2.00000
    3    -15.34498    2.00000
    4    -13.27856    2.00000
    5    -12.42045    2.00000
    6    -12.10659    2.00000
    7    -10.59235    2.00000
    8     -9.97135    2.00000
    9     -8.73472    2.00000
   10     -7.67626    2.00000
   11     -6.10057    2.00000
   12     -2.93967    0.00000
   13      1.92602    0.00000
   14      2.02570    0.00000
   15      2.72588    0.00000
   16      3.50697    0.00000
   17      3.92352    0.00000
   18      7.46165    0.00000
   19      8.93086    0.00000
   20      9.36467    0.00000
   21     10.78407    0.00000
   22     12.74265    0.00000
   23     13.00820    0.00000
   24     13.33729    0.00000
   25     13.91209    0.00000
   26     14.67073    0.00000
   27     14.76077    0.00000
   28     16.38570    0.00000
   29     16.90239    0.00000
   30     17.15263    0.00000
   31     18.60936    0.00000
   32     19.22344    0.00000
   33     21.01010    0.00000
   34     22.67853    0.00000
   35     23.24029    0.00000
   36     26.13258    0.00000
   37     27.35105    0.00000
   38     29.85955    0.00000
   39     30.66780    0.00000
   40     30.88208    0.00000
   41     31.81360    0.00000
   42     34.72251    0.00000
   43     35.22936    0.00000
   44     35.40249    0.00000
   45     36.90029    0.00000
   46     39.04409    0.00000
   47     40.24562    0.00000
   48     41.14441    0.00000
   49     42.24404    0.00000
   50     43.56395    0.00000
   51     44.92011    0.00000
   52     46.26022    0.00000
   53     46.34800    0.00000
   54     50.20070    0.00000
   55     50.99423    0.00000
   56     51.09161    0.00000
   57     51.80662    0.00000
   58     53.54669    0.00000
   59     56.98029    0.00000
   60     58.10398    0.00000
   61     59.13981    0.00000
   62     60.11588    0.00000
   63     64.39934    0.00000
   64     65.85746    0.00000
   65     66.64128    0.00000
   66     74.32353    0.00000

Gap: 3.161 eV
Transition (v -> c):
  (s=0, k=0, n=11, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=12, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.010     0.010   0.0% |
LCAO WFS Initialize:                   1.314     0.039   0.1% |
 Hamiltonian:                          1.275     0.000   0.0% |
  Atomic:                              0.027     0.027   0.1% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.002     0.002   0.0% |
  Communicate:                         0.021     0.021   0.1% |
  Hartree integrate/restrict:          0.025     0.025   0.1% |
  Initialize Hamiltonian:              0.003     0.003   0.0% |
  Poisson:                             0.281     0.015   0.0% |
   Communicate bwd 0:                  0.019     0.019   0.1% |
   Communicate bwd 1:                  0.073     0.073   0.2% |
   Communicate fwd 0:                  0.072     0.072   0.2% |
   Communicate fwd 1:                  0.024     0.024   0.1% |
   fft:                                0.029     0.029   0.1% |
   fft2:                               0.049     0.049   0.1% |
  XC 3D grid:                          0.911     0.911   2.7% ||
  vbar:                                0.004     0.004   0.0% |
P tci:                                 0.002     0.002   0.0% |
SCF-cycle:                            29.731     0.004   0.0% |
 Direct Minimisation step:            28.394     0.002   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.259     0.005   0.0% |
   Construct Gradient Matrix:          0.003     0.003   0.0% |
   DenseAtomicCorrection:              0.002     0.002   0.0% |
   Distribute overlap matrix:          0.085     0.085   0.2% |
   Potential matrix:                   0.162     0.162   0.5% |
   Residual:                           0.002     0.002   0.0% |
  Get Search Direction:                0.004     0.004   0.0% |
  LCAO eigensolver:                    0.013     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.008     0.008   0.0% |
  Preconditioning::                    0.002     0.002   0.0% |
  Unitary rotation:                    0.011     0.002   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.009     0.009   0.0% |
  Update Kohn-Sham energy:            28.103     0.000   0.0% |
   Density:                            1.074     0.000   0.0% |
    Atomic density matrices:           0.107     0.107   0.3% |
    Mix:                               0.790     0.790   2.3% ||
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.005     0.005   0.0% |
    Pseudo density:                    0.170     0.013   0.0% |
     Calculate density matrix:         0.001     0.001   0.0% |
     Construct density:                0.155     0.155   0.5% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       27.028     0.002   0.0% |
    Atomic:                            0.580     0.579   1.7% ||
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.045     0.045   0.1% |
    Communicate:                       0.440     0.440   1.3% ||
    Hartree integrate/restrict:        0.500     0.500   1.5% ||
    New Kinetic Energy:                0.003     0.001   0.0% |
     Pseudo part:                      0.002     0.002   0.0% |
    Poisson:                           6.322     0.329   1.0% |
     Communicate bwd 0:                0.619     0.619   1.8% ||
     Communicate bwd 1:                1.587     1.587   4.7% |-|
     Communicate fwd 0:                1.500     1.500   4.4% |-|
     Communicate fwd 1:                0.670     0.670   2.0% ||
     fft:                              0.599     0.599   1.8% ||
     fft2:                             1.018     1.018   3.0% ||
    XC 3D grid:                       19.054    19.054  55.9% |---------------------|
    vbar:                              0.082     0.082   0.2% |
 Get canonical representation:         1.333     0.001   0.0% |
  LCAO eigensolver:                    0.013     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.004     0.004   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.008     0.008   0.0% |
  Update Kohn-Sham energy:             1.320     0.000   0.0% |
   Density:                            0.052     0.000   0.0% |
    Atomic density matrices:           0.005     0.005   0.0% |
    Mix:                               0.038     0.038   0.1% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.008     0.001   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.007     0.007   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.268     0.000   0.0% |
    Atomic:                            0.027     0.027   0.1% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.002     0.002   0.0% |
    Communicate:                       0.021     0.021   0.1% |
    Hartree integrate/restrict:        0.026     0.026   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.280     0.015   0.0% |
     Communicate bwd 0:                0.019     0.019   0.1% |
     Communicate bwd 1:                0.074     0.074   0.2% |
     Communicate fwd 0:                0.071     0.071   0.2% |
     Communicate fwd 1:                0.023     0.023   0.1% |
     fft:                              0.028     0.028   0.1% |
     fft2:                             0.049     0.049   0.1% |
    XC 3D grid:                        0.908     0.908   2.7% ||
    vbar:                              0.004     0.004   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.986     0.986   2.9% ||
mktci:                                 0.001     0.001   0.0% |
Other:                                 2.054     2.054   6.0% |-|
-------------------------------------------------------------
Total:                                          34.102 100.0%

Memory usage: 416.48 MiB
Date: Sat Feb 16 23:48:18 2019
