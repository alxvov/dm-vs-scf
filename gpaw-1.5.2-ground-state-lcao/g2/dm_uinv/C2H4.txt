
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-0-35
Date:   Sun Feb 17 00:11:31 2019
Arch:   x86_64
Pid:    22617
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (4f08e4a709)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (c5ed980b6a)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 10000000000.0,
                eigenstates: 1e-10,
                energy: 10000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  h: 0.15
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.0}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

C-setup:
  name: Carbon
  id: 4aa54d4b901d75f77cc0ea3eec22967b
  Z: 6
  valence: 4
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/C.PBE.gz
  cutoffs: 0.64(comp), 1.14(filt), 1.14(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -13.751   0.635
    2p(2.00)    -5.284   0.635
    *s          13.461   0.635
    *p          21.927   0.635
    *d           0.000   0.635

  LCAO basis set for C:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/C.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=5.5469 Bohr: 2s-sz confined orbital
      l=1, rc=6.6719 Bohr: 2p-sz confined orbital
      l=0, rc=3.0312 Bohr: 2s-dz split-valence wave
      l=1, rc=3.7500 Bohr: 2p-dz split-valence wave
      l=2, rc=6.6719 Bohr: d-type Gaussian polarization

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  LCAO basis set for H:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/H.dzp.basis.gz
    Number of radial functions: 3
    Number of spherical harmonics: 5
      l=0, rc=6.4219 Bohr: 1s-sz confined orbital
      l=0, rc=3.6094 Bohr: 1s-dz split-valence wave
      l=1, rc=6.4219 Bohr: p-type Gaussian polarization

Reference energy: -2105.216880

Spin-paired calculation

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.0000 eV

Convergence criteria:
  Maximum total energy change: 1e+10 eV / electron
  Maximum integral of absolute density change: 1e+10 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: float
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 92*104*108 grid
  Fine grid: 184*208*216 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 184*208*216 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 1, 2].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 436.49 MiB
  Calculator: 43.41 MiB
    Density: 25.43 MiB
      Arrays: 24.81 MiB
      Localized functions: 0.62 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 16.28 MiB
      Arrays: 16.23 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.05 MiB
    Wavefunctions: 1.70 MiB
      C [qnM]: 0.02 MiB
      S, T [2 x qmm]: 0.03 MiB
      P [aqMi]: 0.00 MiB
      BasisFunctions: 1.66 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Domain decomposition: 2 x 2 x 2

Number of atoms: 6
Number of atomic orbitals: 46
Number of bands in calculation: 46
Bands to converge: occupied states only
Number of valence electrons: 12

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
           .---------------------------------.  
          /|                                 |  
         / |                                 |  
        /  |                                 |  
       /   |                                 |  
      /    |                                 |  
     /     |                                 |  
    /      |                                 |  
   /       |                                 |  
  /        |                                 |  
 *         |                                 |  
 |         |                                 |  
 |         |                                 |  
 |         |            H                    |  
 |         |          HC                     |  
 |         |                                 |  
 |         |           CH                    |  
 |         |          H                      |  
 |         |                                 |  
 |         |                                 |  
 |         .---------------------------------.  
 |        /                                 /   
 |       /                                 /    
 |      /                                 /     
 |     /                                 /      
 |    /                                 /       
 |   /                                 /        
 |  /                                 /         
 | /                                 /          
 |/                                 /           
 *---------------------------------*            

Positions:
   0 C      7.000000    7.922832    8.905175    ( 0.0000,  0.0000,  0.0000)
   1 C      7.000000    7.922832    7.570215    ( 0.0000,  0.0000,  0.0000)
   2 H      7.000000    8.845664    9.475390    ( 0.0000,  0.0000,  0.0000)
   3 H      7.000000    7.000000    9.475390    ( 0.0000,  0.0000,  0.0000)
   4 H      7.000000    8.845664    7.000000    ( 0.0000,  0.0000,  0.0000)
   5 H      7.000000    7.000000    7.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    14.000000    0.000000    0.000000    92     0.1522
  2. axis:    no     0.000000   15.845664    0.000000   104     0.1524
  3. axis:    no     0.000000    0.000000   16.475390   108     0.1525

  Lengths:  14.000000  15.845664  16.475390
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1524

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:11:38  +0.46          -30.911788    0      1      
iter:   2  00:11:39  +0.03          -30.945617    0      1      
iter:   3  00:11:40  -2.31          -30.954354    0      1      
iter:   4  00:11:41  -3.36          -30.954390    0      1      
iter:   5  00:11:42  -5.11          -30.954394    0      1      
iter:   6  00:11:44  -6.19          -30.954394    0      1      
iter:   7  00:11:45  -7.00          -30.954394    0      1      
iter:   8  00:11:46  -7.63          -30.954394    0      1      
iter:   9  00:11:49 -10.34          -30.954394    0      1      

Converged after 9 iterations.

Dipole moment: (-0.000000, -0.000000, -0.000000) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2105.216880)

Kinetic:        +24.718671
Potential:      -28.513719
External:        +0.000000
XC:             -27.218234
Entropy (-ST):   +0.000000
Local:           +0.058888
--------------------------
Free energy:    -30.954394
Extrapolated:   -30.954394

Fermi levels: -3.62473, -3.62473

 Band  Eigenvalues  Occupancy
    0    -18.74930    2.00000
    1    -14.22077    2.00000
    2    -11.37437    2.00000
    3    -10.09213    2.00000
    4     -8.46938    2.00000
    5     -6.55748    2.00000
    6     -0.69198    0.00000
    7      2.46584    0.00000
    8      2.93989    0.00000
    9      3.24993    0.00000
   10      5.51180    0.00000
   11      8.64925    0.00000
   12      8.88549    0.00000
   13      8.96672    0.00000
   14     10.16276    0.00000
   15     10.76400    0.00000
   16     11.94563    0.00000
   17     12.28426    0.00000
   18     15.86206    0.00000
   19     16.24502    0.00000
   20     17.92450    0.00000
   21     18.28143    0.00000
   22     20.69451    0.00000
   23     21.02969    0.00000
   24     22.02753    0.00000
   25     24.05997    0.00000
   26     26.30357    0.00000
   27     28.53916    0.00000
   28     31.17133    0.00000
   29     31.47277    0.00000
   30     32.21463    0.00000
   31     34.50118    0.00000
   32     34.71239    0.00000
   33     34.78999    0.00000
   34     35.33185    0.00000
   35     35.85475    0.00000
   36     42.63106    0.00000
   37     43.66596    0.00000
   38     45.11065    0.00000
   39     45.78875    0.00000
   40     48.44754    0.00000
   41     50.36261    0.00000
   42     51.16913    0.00000
   43     52.45080    0.00000
   44     67.58641    0.00000
   45     68.46844    0.00000

Gap: 5.866 eV
Transition (v -> c):
  (s=0, k=0, n=5, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=6, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.000     0.000   0.0% |
Basis functions set positions:         0.008     0.008   0.0% |
LCAO WFS Initialize:                   1.197     0.032   0.2% |
 Hamiltonian:                          1.164     0.000   0.0% |
  Atomic:                              0.000     0.000   0.0% |
   XC Correction:                      0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:       0.000     0.000   0.0% |
  Communicate:                         0.048     0.048   0.3% |
  Hartree integrate/restrict:          0.023     0.023   0.1% |
  Initialize Hamiltonian:              0.001     0.001   0.0% |
  Poisson:                             0.340     0.006   0.0% |
   Communicate bwd 0:                  0.067     0.067   0.4% |
   Communicate bwd 1:                  0.063     0.063   0.4% |
   Communicate fwd 0:                  0.065     0.065   0.4% |
   Communicate fwd 1:                  0.067     0.067   0.4% |
   fft:                                0.019     0.019   0.1% |
   fft2:                               0.054     0.054   0.3% |
  XC 3D grid:                          0.748     0.748   4.2% |-|
  vbar:                                0.003     0.003   0.0% |
P tci:                                 0.000     0.000   0.0% |
SCF-cycle:                            14.489     0.002   0.0% |
 Direct Minimisation step:            13.272     0.001   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                 0.103     0.002   0.0% |
   Construct Gradient Matrix:          0.001     0.001   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.019     0.019   0.1% |
   Potential matrix:                   0.080     0.080   0.5% |
   Residual:                           0.001     0.001   0.0% |
  Get Search Direction:                0.002     0.002   0.0% |
  LCAO eigensolver:                    0.010     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.007     0.007   0.0% |
  Preconditioning::                    0.001     0.001   0.0% |
  Unitary rotation:                    0.005     0.001   0.0% |
   Broadcast coefficients:             0.000     0.000   0.0% |
   Eigendecomposition:                 0.004     0.004   0.0% |
  Update Kohn-Sham energy:            13.151     0.000   0.0% |
   Density:                            0.466     0.000   0.0% |
    Atomic density matrices:           0.029     0.029   0.2% |
    Mix:                               0.350     0.350   2.0% ||
    Multipole moments:                 0.002     0.002   0.0% |
    Normalize:                         0.002     0.002   0.0% |
    Pseudo density:                    0.083     0.005   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.077     0.077   0.4% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                       12.684     0.001   0.0% |
    Atomic:                            0.003     0.003   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.005     0.005   0.0% |
    Communicate:                       0.527     0.527   3.0% ||
    Hartree integrate/restrict:        0.234     0.234   1.3% ||
    New Kinetic Energy:                0.001     0.001   0.0% |
     Pseudo part:                      0.001     0.001   0.0% |
    Poisson:                           3.680     0.065   0.4% |
     Communicate bwd 0:                0.735     0.735   4.2% |-|
     Communicate bwd 1:                0.680     0.680   3.8% |-|
     Communicate fwd 0:                0.665     0.665   3.8% |-|
     Communicate fwd 1:                0.732     0.732   4.1% |-|
     fft:                              0.209     0.209   1.2% |
     fft2:                             0.594     0.594   3.4% ||
    XC 3D grid:                        8.197     8.197  46.4% |------------------|
    vbar:                              0.036     0.036   0.2% |
 Get canonical representation:         1.215     0.000   0.0% |
  LCAO eigensolver:                    0.009     0.000   0.0% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.002     0.002   0.0% |
   Orbital Layouts:                    0.001     0.001   0.0% |
   Potential matrix:                   0.007     0.007   0.0% |
  Update Kohn-Sham energy:             1.205     0.000   0.0% |
   Density:                            0.042     0.000   0.0% |
    Atomic density matrices:           0.003     0.003   0.0% |
    Mix:                               0.032     0.032   0.2% |
    Multipole moments:                 0.000     0.000   0.0% |
    Normalize:                         0.000     0.000   0.0% |
    Pseudo density:                    0.007     0.000   0.0% |
     Calculate density matrix:         0.000     0.000   0.0% |
     Construct density:                0.007     0.007   0.0% |
     Symmetrize density:               0.000     0.000   0.0% |
   Hamiltonian:                        1.163     0.000   0.0% |
    Atomic:                            0.000     0.000   0.0% |
     XC Correction:                    0.000     0.000   0.0% |
    Calculate atomic Hamiltonians:     0.000     0.000   0.0% |
    Communicate:                       0.047     0.047   0.3% |
    Hartree integrate/restrict:        0.021     0.021   0.1% |
    New Kinetic Energy:                0.000     0.000   0.0% |
     Pseudo part:                      0.000     0.000   0.0% |
    Poisson:                           0.339     0.006   0.0% |
     Communicate bwd 0:                0.067     0.067   0.4% |
     Communicate bwd 1:                0.062     0.062   0.4% |
     Communicate fwd 0:                0.061     0.061   0.3% |
     Communicate fwd 1:                0.070     0.070   0.4% |
     fft:                              0.019     0.019   0.1% |
     fft2:                             0.054     0.054   0.3% |
    XC 3D grid:                        0.752     0.752   4.3% |-|
    vbar:                              0.003     0.003   0.0% |
ST tci:                                0.002     0.002   0.0% |
Set symmetry:                          0.001     0.001   0.0% |
TCI: Evaluate splines:                 0.202     0.202   1.1% |
mktci:                                 0.001     0.001   0.0% |
Other:                                 1.777     1.777  10.1% |---|
-------------------------------------------------------------
Total:                                          17.677 100.0%

Memory usage: 436.49 MiB
Date: Sun Feb 17 00:11:49 2019
