from ase.build import bulk
from gpaw import GPAW, LCAO, ConvergenceError
from gpaw.occupations import FermiDirac
import numpy as np
from gpaw.mpi import world
from ase.build import sort
from ase.parallel import parprint 
import time
from gpaw.utilities.memory import maxrss

mat = {'NaCl': 5.698, 'NaF': 4.700, 'LiCl': 5.148, 'LiF': 4.062 ,'MgO': 4.242}
struct = {'NaCl': 'rocksalt', 'NaF': 'rocksalt', 'LiCl': 'rocksalt', 'LiF': 'rocksalt' ,'MgO': 'rocksalt'}

mode = LCAO()
xc = 'PBE'

ftow = open('data.txt', 'w')

for b in mat.keys():

    sys = bulk(b, struct[b], cubic=True, a=mat[b]) * (1, 1, 1)
    sys = sort(sys)

    calc = GPAW(xc=xc,
                convergence={'density': 1.0e-4,
                             'eigenstates': 1.0e-10,
                             'energy': 1.0e9,
                            },
                # parallel={'domain': world.size},
                maxiter=1200,
                basis='dzp',
                mode=mode,
                spinpol=False, 
                kpts={'size': (3, 3, 3), 'gamma': True},
                occupations=FermiDirac(width=1.0e-4),
                txt=b + '.txt'
               )

    sys.set_calculator(calc)
    
    try:
        t1 = time.time()
        e = sys.get_potential_energy()
        t2 = time.time()
        steps = sys.calc.get_number_of_iterations()
        iters = steps  # sys.calc.wfs.eigensolver.get_en_and_grad_iters
        memory = maxrss() / 1024.0 ** 2
        parprint(
                 "{}\t{}\t{}\t{}\t{}\t{:.3f}".format(b, steps, e, iters,
                                                     t2 - t1, memory),
                 flush=True, file=ftow)  # s,MB
        # calc.write(str(len(sys) // 3) + '_H2O.gpw')
        del calc
        del sys
    except ConvergenceError:
        parprint(
                 "\t{}\t{}\t{}\t{}\t{}".format(None, None, None, None,
                                               None),
                 flush=True, file=ftow)
        del calc
        del sys

ftow.close()
