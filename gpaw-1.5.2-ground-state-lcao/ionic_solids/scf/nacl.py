from ase.build import bulk
from gpaw import GPAW, LCAO
from gpaw.occupations import FermiDirac
from gpaw.mpi import world
from ase.build import sort

sys = bulk('NaCl', 'rocksalt', cubic=True, a=5.698) * (2, 2, 2)
sys = sort(sys)
calc = GPAW(xc='PBE',
            convergence={'density': 1.0e-4},
            parallel={'domain': world.size},
            basis='dzp',
            mode= LCAO(),
            spinpol=False,
            kpts={'size': (3, 3, 3), 'gamma': True},
            occupations=FermiDirac(width=0.0),
            txt='NaCl_zero_k.txt',
            )

sys.set_calculator(calc)
sys.get_potential_energy()
