
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.5.2b1
 |___|_|             

User:   aleksei@compute-1-6
Date:   Fri Mar  1 11:09:51 2019
Arch:   x86_64
Pid:    15070
Python: 3.6.5
gpaw:   /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/gpaw (0b16ebee44)
_gpaw:  /users/home/aleksei/NEW_PROJECTS/gpaw_lcao/source_code/build/bin.linux-x86_64-3.6/
        gpaw-python (d36f9cbfcd)
ase:    /users/home/aleksei/ase_my_fork/ase (version 3.18.0b1-471271e4ee)
numpy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/numpy (version 1.15.2)
scipy:  /opt/share/python/intel2019/intelpython3/lib/python3.6/site-packages/scipy (version 1.1.0)
libxc:  4.0.4
units:  Angstrom and eV
cores:  8

Input parameters:
  basis: dzp
  convergence: {density: 1000000000.0,
                eigenstates: 1e-10,
                energy: 1000000000.0}
  eigensolver: {initial_orbitals: KS,
                initial_rotation: zero,
                linesearch_algo: {'name': 'SwcAwc', 'method': 'LBFGS_P'},
                matrix_exp: egdecomp2,
                name: direct_min_lcao,
                representation: {'name': 'u_invar'},
                searchdir_algo: {'name': 'LBFGS_P'},
                update_precond_counter: 1000,
                update_ref_orbs_counter: 20,
                use_prec: True}
  kpts: {gamma: True,
         size: (3, 3, 3)}
  maxiter: 1200
  mixer: {method: dummy}
  mode: {interpolation: 3,
         name: lcao}
  nbands: nao
  occupations: {name: fermi-dirac,
                width: 0.0001}
  spinpol: False
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Mg-setup:
  name: Magnesium
  id: b56b6cab5149d5bf22ad04c2f71a3023
  Z: 12
  valence: 10
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/Mg.PBE.gz
  cutoffs: 1.03(comp), 1.86(filt), 0.54(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -79.840   1.090
    3s(2.00)    -4.705   1.090
    2p(6.00)   -46.613   1.085
    3p(0.00)    -1.330   1.085
    *d           0.000   1.037

  LCAO basis set for Mg:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/Mg.dzp.basis.gz
    Number of radial functions: 9
    Number of spherical harmonics: 19
      l=0, rc=2.7969 Bohr: 2s-sz confined orbital
      l=0, rc=9.2344 Bohr: 3s-sz confined orbital
      l=1, rc=3.1406 Bohr: 2p-sz confined orbital
      l=1, rc=12.7188 Bohr: 3p-sz confined orbital
      l=0, rc=1.8125 Bohr: 2s-dz split-valence wave
      l=0, rc=5.5156 Bohr: 3s-dz split-valence wave
      l=1, rc=2.0938 Bohr: 2p-dz split-valence wave
      l=1, rc=7.7969 Bohr: 3p-dz split-valence wave
      l=1, rc=9.2344 Bohr: p-type Gaussian polarization

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /users/home/aleksei/gpaw-setups-0.9.11271/O.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  LCAO basis set for O:
    Name: dzp
    File: /users/home/aleksei/gpaw-setups-0.9.11271/O.dzp.basis.gz
    Number of radial functions: 5
    Number of spherical harmonics: 13
      l=0, rc=4.3438 Bohr: 2s-sz confined orbital
      l=1, rc=5.3906 Bohr: 2p-sz confined orbital
      l=0, rc=2.2969 Bohr: 2s-dz split-valence wave
      l=1, rc=2.8906 Bohr: 2p-dz split-valence wave
      l=2, rc=5.3906 Bohr: d-type Gaussian polarization

Reference energy: -29961.988759

Spin-paired calculation

Occupation numbers:
  Fermi-Dirac: width=0.0001 eV

Convergence criteria:
  Maximum total energy change: 1e+09 eV / electron
  Maximum integral of absolute density change: 1e+09 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 1200

Symmetries present (total): 48

  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)
  ( 0  1  0)  ( 0  1  0)  ( 0  0  1)  ( 0  0  1)  ( 0  0 -1)  ( 0  0 -1)
  ( 0  0  1)  ( 0  0 -1)  ( 0  1  0)  ( 0 -1  0)  ( 0  1  0)  ( 0 -1  0)

  ( 1  0  0)  ( 1  0  0)  ( 0  1  0)  ( 0  1  0)  ( 0  1  0)  ( 0  1  0)
  ( 0 -1  0)  ( 0 -1  0)  ( 1  0  0)  ( 1  0  0)  ( 0  0  1)  ( 0  0  1)
  ( 0  0  1)  ( 0  0 -1)  ( 0  0  1)  ( 0  0 -1)  ( 1  0  0)  (-1  0  0)

  ( 0  1  0)  ( 0  1  0)  ( 0  1  0)  ( 0  1  0)  ( 0  0  1)  ( 0  0  1)
  ( 0  0 -1)  ( 0  0 -1)  (-1  0  0)  (-1  0  0)  ( 1  0  0)  ( 1  0  0)
  ( 1  0  0)  (-1  0  0)  ( 0  0  1)  ( 0  0 -1)  ( 0  1  0)  ( 0 -1  0)

  ( 0  0  1)  ( 0  0  1)  ( 0  0  1)  ( 0  0  1)  ( 0  0  1)  ( 0  0  1)
  ( 0  1  0)  ( 0  1  0)  ( 0 -1  0)  ( 0 -1  0)  (-1  0  0)  (-1  0  0)
  ( 1  0  0)  (-1  0  0)  ( 1  0  0)  (-1  0  0)  ( 0  1  0)  ( 0 -1  0)

  ( 0  0 -1)  ( 0  0 -1)  ( 0  0 -1)  ( 0  0 -1)  ( 0  0 -1)  ( 0  0 -1)
  ( 1  0  0)  ( 1  0  0)  ( 0  1  0)  ( 0  1  0)  ( 0 -1  0)  ( 0 -1  0)
  ( 0  1  0)  ( 0 -1  0)  ( 1  0  0)  (-1  0  0)  ( 1  0  0)  (-1  0  0)

  ( 0  0 -1)  ( 0  0 -1)  ( 0 -1  0)  ( 0 -1  0)  ( 0 -1  0)  ( 0 -1  0)
  (-1  0  0)  (-1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 0  0  1)  ( 0  0  1)
  ( 0  1  0)  ( 0 -1  0)  ( 0  0  1)  ( 0  0 -1)  ( 1  0  0)  (-1  0  0)

  ( 0 -1  0)  ( 0 -1  0)  ( 0 -1  0)  ( 0 -1  0)  (-1  0  0)  (-1  0  0)
  ( 0  0 -1)  ( 0  0 -1)  (-1  0  0)  (-1  0  0)  ( 0  1  0)  ( 0  1  0)
  ( 1  0  0)  (-1  0  0)  ( 0  0  1)  ( 0  0 -1)  ( 0  0  1)  ( 0  0 -1)

  (-1  0  0)  (-1  0  0)  (-1  0  0)  (-1  0  0)  (-1  0  0)  (-1  0  0)
  ( 0  0  1)  ( 0  0  1)  ( 0  0 -1)  ( 0  0 -1)  ( 0 -1  0)  ( 0 -1  0)
  ( 0  1  0)  ( 0 -1  0)  ( 0  1  0)  ( 0 -1  0)  ( 0  0  1)  ( 0  0 -1)

27 k-points: 3 x 3 x 3 Monkhorst-Pack grid
4 k-points in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/27
   1:     0.33333333    0.00000000    0.00000000          6/27
   2:     0.33333333    0.33333333    0.00000000         12/27
   3:     0.33333333    0.33333333    0.33333333          8/27

Wave functions: LCAO
  Diagonalizer: Serial LAPACK
  Atomic Correction: dense with blas
  Datatype: complex
 

Eigensolver
   Direct minimisation using exponential transformation.
       Search direction: LBFGS algorithm with preconditioning
       Line search: Inexact line search based on cubic interpolation,
                    strong and approximate Wolfe conditions
       Preconditioning: True
       WARNING: do not use it for metals as occupation numbers are
                not found variationally
 

Densities:
  Coarse grid: 20*20*20 grid
  Fine grid: 40*40*40 grid
  Total Charge: 0.000000 

No density mixing

Hamiltonian:
  XC and Coulomb potentials evaluated on a 40*40*40 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [0, 1, 2];
    FST axes: [].
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 196.73 MiB
  Calculator: 21.15 MiB
    Density: 7.05 MiB
      Arrays: 0.79 MiB
      Localized functions: 6.26 MiB
      Mixer: 0.00 MiB
    Hamiltonian: 0.95 MiB
      Arrays: 0.52 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.43 MiB
    Wavefunctions: 13.14 MiB
      C [qnM]: 0.25 MiB
      S, T [2 x qmm]: 0.50 MiB
      P [aqMi]: 0.01 MiB
      BasisFunctions: 12.39 MiB
      Eigensolver: 0.00 MiB

Total number of cores used: 8
Parallelization over k-points: 4
Domain decomposition: 2 x 1 x 1

Number of atoms: 8
Number of atomic orbitals: 128
Number of bands in calculation: 128
Bands to converge: occupied states only
Number of valence electrons: 64

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
    .---------.  
   /|         |  
  / |         |  
 *  |         |  
 |Mg|  O      |  
 |  .---------.  
 O /  Mg     /   
 |/    Mg   /    
 Mg---O----*     

Positions:
   0 Mg     0.000000    0.000000    0.000000    ( 0.0000,  0.0000,  0.0000)
   1 Mg     0.000000    2.121000    2.121000    ( 0.0000,  0.0000,  0.0000)
   2 Mg     2.121000    0.000000    2.121000    ( 0.0000,  0.0000,  0.0000)
   3 Mg     2.121000    2.121000    0.000000    ( 0.0000,  0.0000,  0.0000)
   4 O      2.121000    0.000000    0.000000    ( 0.0000,  0.0000,  0.0000)
   5 O      2.121000    2.121000    2.121000    ( 0.0000,  0.0000,  0.0000)
   6 O      0.000000    0.000000    2.121000    ( 0.0000,  0.0000,  0.0000)
   7 O      0.000000    2.121000    0.000000    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    yes    4.242000    0.000000    0.000000    20     0.2121
  2. axis:    yes    0.000000    4.242000    0.000000    20     0.2121
  3. axis:    yes    0.000000    0.000000    4.242000    20     0.2121

  Lengths:   4.242000   4.242000   4.242000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2121

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  11:10:03  -1.84          -45.167400    0      1      
iter:   2  11:10:05  -3.32          -45.172327    0      1      
iter:   3  11:10:08  -4.98          -45.172620    0      1      
iter:   4  11:10:10  -5.60          -45.172620    0      1      
iter:   5  11:10:13  -6.59          -45.172620    0      1      
iter:   6  11:10:26  -6.61          -45.172621    0      1      
iter:   7  11:10:28  -9.10          -45.172620    0      1      
iter:   8  11:10:32 -10.08          -45.172620    0      1      

Converged after 8 iterations.

Dipole moment: (-5.678806, -5.678806, -5.678806) |e|*Ang

Energy contributions relative to reference atoms: (reference = -29961.988759)

Kinetic:        +62.165311
Potential:      -52.179331
External:        +0.000000
XC:             -57.079502
Entropy (-ST):   -0.000000
Local:           +1.920902
--------------------------
Free energy:    -45.172620
Extrapolated:   -45.172620

Fermi level: 5.65032

Showing only first 2 kpts
 Kpt  Band  Eigenvalues  Occupancy
  0    30      4.79379    0.07407
  0    31      4.79379    0.07407
  0    32      9.24397    0.00000
  0    33     14.49920    0.00000

  1    30      4.42078    0.44444
  1    31      4.42078    0.44444
  1    32     11.19651    0.00000
  1    33     13.88029    0.00000


Gap: 4.450 eV
Transition (v -> c):
  (s=0, k=0, n=31, [0.00, 0.00, 0.00]) -> (s=0, k=0, n=32, [0.00, 0.00, 0.00])
Timing:                                incl.     excl.
-------------------------------------------------------------
Basic WFS set positions:               0.012     0.012   0.0% |
Basis functions set positions:         0.080     0.080   0.2% |
LCAO WFS Initialize:                   0.380     0.017   0.0% |
 Hamiltonian:                          0.362     0.000   0.0% |
  Atomic:                              0.321     0.237   0.6% |
   XC Correction:                      0.084     0.084   0.2% |
  Calculate atomic Hamiltonians:       0.003     0.003   0.0% |
  Communicate:                         0.000     0.000   0.0% |
  Hartree integrate/restrict:          0.001     0.001   0.0% |
  Initialize Hamiltonian:              0.000     0.000   0.0% |
  Poisson:                             0.013     0.000   0.0% |
   Communicate bwd 0:                  0.003     0.003   0.0% |
   Communicate bwd 1:                  0.003     0.003   0.0% |
   Communicate fwd 0:                  0.001     0.001   0.0% |
   Communicate fwd 1:                  0.003     0.003   0.0% |
   fft:                                0.000     0.000   0.0% |
   fft2:                               0.002     0.002   0.0% |
  XC 3D grid:                          0.025     0.025   0.1% |
  vbar:                                0.000     0.000   0.0% |
P tci:                                 0.102     0.102   0.2% |
SCF-cycle:                            38.160     0.004   0.0% |
 Direct Minimisation step:            36.847     0.005   0.0% |
  Broadcast gradients:                 0.000     0.000   0.0% |
  Calculate gradients:                18.910     0.640   1.6% ||
   Construct Gradient Matrix:          0.016     0.016   0.0% |
   DenseAtomicCorrection:              0.006     0.006   0.0% |
   Distribute overlap matrix:          0.008     0.008   0.0% |
   Potential matrix:                  17.698    17.698  43.2% |----------------|
   Residual:                           0.007     0.007   0.0% |
   Sum over cells:                     0.535     0.535   1.3% ||
  Get Search Direction:                0.005     0.005   0.0% |
  LCAO eigensolver:                    1.357     0.054   0.1% |
   Calculate projections:              0.000     0.000   0.0% |
   DenseAtomicCorrection:              0.000     0.000   0.0% |
   Distribute overlap matrix:          0.007     0.007   0.0% |
   Orbital Layouts:                    0.006     0.006   0.0% |
   Potential matrix:                   1.251     1.251   3.1% ||
   Sum over cells:                     0.038     0.038   0.1% |
  Preconditioning::                    0.008     0.008   0.0% |
  Unitary rotation:                    0.031     0.013   0.0% |
   Broadcast coefficients:             0.001     0.001   0.0% |
   Eigendecomposition:                 0.017     0.017   0.0% |
  Update Kohn-Sham energy:            16.530     0.000   0.0% |
   Density:                           14.794     0.000   0.0% |
    Atomic density matrices:           0.076     0.076   0.2% |
    Mix:                               0.042     0.042   0.1% |
    Multipole moments:                 0.003     0.003   0.0% |
    Normalize:                         0.001     0.001   0.0% |
    Pseudo density:                   14.671     0.227   0.6% |
     Calculate density matrix:         0.003     0.003   0.0% |
     Construct density:               14.083    14.083  34.3% |-------------|
     Symmetrize density:               0.359     0.359   0.9% |
   Hamiltonian:                        1.736     0.002   0.0% |
    Atomic:                            1.162     0.014   0.0% |
     XC Correction:                    1.148     1.148   2.8% ||
    Calculate atomic Hamiltonians:     0.036     0.036   0.1% |
    Communicate:                       0.002     0.002   0.0% |
    Hartree integrate/restrict:        0.008     0.008   0.0% |
    New Kinetic Energy:                0.006     0.002   0.0% |
     Pseudo part:                      0.004     0.004   0.0% |
    Poisson:                           0.177     0.007   0.0% |
     Communicate bwd 0:                0.039     0.039   0.1% |
     Communicate bwd 1:                0.040     0.040   0.1% |
     Communicate fwd 0:                0.016     0.016   0.0% |
     Communicate fwd 1:                0.041     0.041   0.1% |
     fft:                              0.005     0.005   0.0% |
     fft2:                             0.030     0.030   0.1% |
    XC 3D grid:                        0.342     0.342   0.8% |
    vbar:                              0.002     0.002   0.0% |
 Get canonical representation:         1.309     0.005   0.0% |
  DenseAtomicCorrection:               0.000     0.000   0.0% |
  Distribute overlap matrix:           0.000     0.000   0.0% |
  Potential matrix:                    1.265     1.265   3.1% ||
  Sum over cells:                      0.038     0.038   0.1% |
ST tci:                                0.440     0.440   1.1% |
Set symmetry:                          0.018     0.018   0.0% |
TCI: Evaluate splines:                 0.650     0.650   1.6% ||
mktci:                                 0.180     0.180   0.4% |
Other:                                 0.987     0.987   2.4% ||
-------------------------------------------------------------
Total:                                          41.009 100.0%

Memory usage: 196.73 MiB
Date: Fri Mar  1 11:10:32 2019
